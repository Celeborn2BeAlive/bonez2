#pragma once

#include "VoxelGrid.hpp"
#include "topology/CurvilinearSkeleton.hpp"
#include "topology/SegmentedCurvSkel.hpp"

#include <memory>

namespace BnZ {

typedef std::vector<Vec3f> NodeColors;

class VoxelSpace {
public:
    VoxelSpace(const Vec3u& size,
               const Mat4f& worldToLocalTransform,
               float worldToLocalScale):
        m_WorldToLocal(worldToLocalTransform),
        m_LocalToWorld(inverse(m_WorldToLocal)),
        m_fWorldToLocalScale(worldToLocalScale),
        m_fLocalToWorldScale(1.f / m_fWorldToLocalScale),
        m_Size(size),
        m_nSliceSize(m_Size.x * m_Size.y) {
    }

    const Mat4f& getWorldToLocalTransform() const {
        return m_WorldToLocal;
    }

    const Mat4f& getLocalToWorldTransform() const {
        return m_LocalToWorld;
    }

    float getWorldToLocalScale() const {
        return m_fWorldToLocalScale;
    }

    float getLocalToWorldScale() const {
        return m_fLocalToWorldScale;
    }

    Vec3u getSize() const {
        return m_Size;
    }

    Vec3i getVoxel(Vec3f Pws) const {
        auto P = m_WorldToLocal * Vec4f(Pws, 1);
        return Vec3i(P.x, P.y, P.z);
    }

    float getVoxelSize() const {
        return m_fLocalToWorldScale;
    }

    Vec3f getClusteringNodeColor(GraphNodeIndex idx) const {
        if(idx == UNDEFINED_NODE) return Vec3f(0.f);
        return m_ClusterColors[idx];
    }

    Vec3f getSkeletonNodeColor(GraphNodeIndex idx) const {
        if(idx == UNDEFINED_NODE) return Vec3f(0.f);
        return m_SkelNodeColors[idx];
    }

    const NodeColors& getSkeletonNodeColors() const {
        return m_SkelNodeColors;
    }

    const NodeColors& getClusteringNodeColors() const {
        return m_ClusterColors;
    }

    void setSkeletonNodeColors(NodeColors colors) {
        m_SkelNodeColors = colors;
    }

    void setClusteringNodeColors(NodeColors colors) {
        m_ClusterColors = colors;
    }

private:
    Mat4f m_WorldToLocal;
    Mat4f m_LocalToWorld;
    float m_fWorldToLocalScale;
    float m_fLocalToWorldScale; // It is also the size of a voxel in world space
    Vec3u m_Size; // Dimension of the voxel grid
    size_t m_nSliceSize; // m_Size.x * m_Size.y

    // Only used for analysis
    NodeColors m_SkelNodeColors;
    NodeColors m_ClusterColors;
};

const NodeColors& getNodeColors(const Skeleton& skeleton, const VoxelSpace& voxelSpace);

void setRandomColors(VoxelSpace& voxelSpace);

void setClusteringColors(VoxelSpace& voxelSpace);

void setDegreeColors(VoxelSpace& voxelSpace);

void setMaxballRadiusColors(VoxelSpace& voxelSpace);

void setMaxballRadiusPerLineColors(VoxelSpace& voxelSpace);

void setMaxballVariancePerLineColors(VoxelSpace& voxelSpace);

void setLocalMaxballExtremasColors(VoxelSpace& voxelSpace);

void setLineMaxballExtremasColors(VoxelSpace& voxelSpace);

void setMeyerWatershedColors(VoxelSpace& voxelSpace);

void setVisibilityClusteringTestColors(VoxelSpace& voxelSpace);

void setCurvatureColors(VoxelSpace& voxelSpace);

template<typename Functor>
inline void iterate6neighbouring(int x, int y, int z, Functor f) {
    f(x - 1, y, z);
    f(x, y - 1, z);
    f(x, y, z - 1);
    f(x + 1, y, z);
    f(x, y + 1, z);
    f(x, y, z + 1);
}

template<typename Functor>
inline void iterate6neighbouring(const Vec3i& voxel, Functor f) {
    iterate6neighbouring(voxel.x, voxel.y, voxel.z, f);
}

}
