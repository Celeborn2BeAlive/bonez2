#include "Scene.hpp"
#include "bonez/maths/maths.hpp"
#include "bonez/sampling/distribution1d.h"
#include "bonez/sampling/shapes.hpp"
#include "lights/AreaLight.hpp"
#include <bonez/sys/threads.hpp>

namespace BnZ {

Scene::~Scene() {
    rtcDeleteScene(m_RTCScene);
}

static void fillRTCRay(const Ray& ray, RTCRay& rtcRay) {
    rtcRay.org[0] = ray.org.x;
    rtcRay.org[1] = ray.org.y;
    rtcRay.org[2] = ray.org.z;
    rtcRay.dir[0] = ray.dir.x;
    rtcRay.dir[1] = ray.dir.y;
    rtcRay.dir[2] = ray.dir.z;
    rtcRay.tnear = ray.tnear;
    rtcRay.tfar = ray.tfar;
    rtcRay.geomID = RTC_INVALID_GEOMETRY_ID;
    rtcRay.primID = RTC_INVALID_GEOMETRY_ID;
    rtcRay.instID = RTC_INVALID_GEOMETRY_ID;
    rtcRay.mask = 0xFFFFFFFF;
    rtcRay.time = 0.0f;
}

struct RTCRayHandle {
    RTCRay rtcRay;
    const Ray& ray;

    RTCRayHandle(const Ray& ray) :
        ray(ray) {
        fillRTCRay(ray, rtcRay);
    }
};

static void filterFunc(void* userPtr, RTCRay& ray) {
    RTCRayHandle* handle = (RTCRayHandle*) &ray; // Find the handle containing the rtcRay

    // Test if the intersected primitive is the origin primitive: avoid self intersections of a given triangle
    if ((ray.geomID == handle->ray.orgPrim.x && ray.primID == handle->ray.orgPrim.y)
            || (ray.geomID == handle->ray.dstPrim.x && ray.primID == handle->ray.dstPrim.y)) {
        ray.geomID = RTC_INVALID_GEOMETRY_ID; // According to embree's API, this cancel this intersection
    }
}

void Scene::buildAccel() {
    m_RTCScene = rtcNewScene(RTC_SCENE_STATIC, RTC_INTERSECT1);

    for(const auto& mesh: m_Geometry.getMeshs()) {
        auto geoID = rtcNewTriangleMesh(m_RTCScene, RTC_GEOMETRY_STATIC, mesh.m_Triangles.size(),
                                        mesh.m_Vertices.size());
        rtcSetBuffer(m_RTCScene, geoID, RTC_VERTEX_BUFFER, (void*)(mesh.m_Vertices.data()), 0, sizeof(TriangleMesh::Vertex));
        rtcSetBuffer(m_RTCScene, geoID, RTC_INDEX_BUFFER, (void*)(mesh.m_Triangles.data()), 0, sizeof(TriangleMesh::Triangle)); // Care: this works because uint32 and int32 are compatible in terms of representation
        rtcSetOcclusionFilterFunction(m_RTCScene, geoID, filterFunc);
        rtcSetIntersectionFilterFunction(m_RTCScene, geoID, filterFunc);
    }

    rtcCommit(m_RTCScene);
}

void Scene::extractAreaLights() {
    for(auto meshIdx: m_Geometry.getEmissiveMeshIndices()) {
        auto light = makeShared<AreaLight>(meshIdx,
                                           m_Geometry.getMesh(meshIdx),
                                           m_Geometry.getMaterial(m_Geometry.getMesh(meshIdx).m_MaterialID));
        auto lightID = m_LightContainer.addLight(light);
        m_Geometry.setMeshLight(meshIdx, lightID);
    }
}

void Scene::buildSamplingDistribution() {
    m_fTotalArea = 0.f;

    m_MeshSamplingDistribution.resize(getDistribution1DBufferSize(m_Geometry.getMeshCount()));
    m_MeshDistributionOffsets.reserve(m_Geometry.getMeshCount());
    auto offset = 0u;
    for(const auto& mesh: m_Geometry.getMeshs()) {
        m_MeshDistributionOffsets.emplace_back(offset);
        auto distSize = getDistribution1DBufferSize(mesh.getTriangleCount());
        offset += distSize;
    }
    m_TriangleSamplingDistributions.resize(offset);
    auto i = 0u;
    for(const auto& mesh: m_Geometry.getMeshs()) {
        auto totalArea = 0.f;
        auto offset = m_MeshDistributionOffsets[i];
        auto ptr = m_TriangleSamplingDistributions.data() + offset;
        for(auto triangleIndex = 0u; triangleIndex < mesh.getTriangleCount(); ++triangleIndex) {
            auto area = mesh.getTriangleArea(triangleIndex);
            ptr[triangleIndex] = area;
            totalArea += area;
        }

        buildDistribution1D(
            [ptr](uint32_t triangleIndex) {
                return ptr[triangleIndex];
            },
            ptr,
            mesh.getTriangleCount());
        m_MeshSamplingDistribution[i] = totalArea;
        ++i;

        m_fTotalArea += totalArea;
    }
    auto ptr = m_MeshSamplingDistribution.data();
    buildDistribution1D(
        [ptr](uint32_t meshIndex) {
            return ptr[meshIndex];
        },
        ptr,
        m_Geometry.getMeshCount());

    m_fTotalArea *= 2.f; // Both side of each triangle to take into account
}

Intersection Scene::intersect(const Ray& ray) const {
    Intersection I;
    RTCRayHandle rayHandle(ray);

    rtcIntersect(m_RTCScene, rayHandle.rtcRay);

    if (rayHandle.rtcRay.geomID != RTC_INVALID_GEOMETRY_ID && rayHandle.rtcRay.tfar > 0.f) {
        I.meshID = rayHandle.rtcRay.geomID;
        I.triangleID = rayHandle.rtcRay.primID;
        I.uv = Vec2f(rayHandle.rtcRay.u, rayHandle.rtcRay.v);
        I.Ng = normalize(Vec3f(rayHandle.rtcRay.Ng[0], rayHandle.rtcRay.Ng[1], rayHandle.rtcRay.Ng[2]));
        I.distance = rayHandle.rtcRay.tfar;

        //faceForward(-ray.dir, I.Ng);

        I.P = ray.org + rayHandle.rtcRay.tfar * ray.dir;

        m_Geometry.postIntersect(ray, I);
    }

    return I;
}

bool Scene::occluded(const Ray& ray) const {
    RTCRayHandle rayHandle(ray);
    rtcOccluded(m_RTCScene, rayHandle.rtcRay);
    return rayHandle.rtcRay.geomID == 0;
}

BRDF Scene::shade(const SurfacePoint& point) const {
    const auto& material = m_Geometry.getMaterial(m_Geometry.getMesh(point.meshID));
    return BRDF(point, material);
}

void Scene::uniformSampleSurfacePoints(uint32_t count,
                                       const float* s1DMeshBuffer, // Used to sample a mesh
                                       const float* s1DTriangleBuffer, // Used to sample a triangle
                                       const float* s1DTriangleSideBuffer, // Used to sample the side of the mesh
                                       const Vec2f* s2DTriangleBuffer, // Used to sample 2D area of the chosen triangle
                                       SurfacePointSample* sampledPointsBuffer) const {
    for(auto i = 0u; i < count; ++i) {
        auto sampledMesh = sampleDiscreteDistribution1D(m_MeshSamplingDistribution.data(),
                                                        m_Geometry.getMeshCount(),
                                                        s1DMeshBuffer[i]);
        if(sampledMesh.pdf > 0.f) {
            const auto& mesh = m_Geometry.getMesh(sampledMesh.value);
            auto sampledTriangle = sampleDiscreteDistribution1D(m_TriangleSamplingDistributions.data() + m_MeshDistributionOffsets[sampledMesh.value],
                                                                mesh.getTriangleCount(),
                                                                s1DTriangleBuffer[i]);
            if(sampledTriangle.pdf > 0.f) {
                const auto& triangle = mesh.getTriangle(sampledTriangle.value);
                auto sampledUV = uniformSampleTriangleUVs(s2DTriangleBuffer[i].x, s2DTriangleBuffer[i].y,
                                                          mesh.getVertex(triangle.v0).position,
                                                          mesh.getVertex(triangle.v1).position,
                                                          mesh.getVertex(triangle.v2).position);
                auto area = mesh.getTriangleArea(sampledTriangle.value);

                m_Geometry.getSurfacePoint(sampledMesh.value, sampledTriangle.value,
                                           sampledUV.x, sampledUV.y, sampledPointsBuffer[i].value);
                sampledPointsBuffer[i].pdf = 0.5f * sampledTriangle.pdf * sampledMesh.pdf / area; // 0.5f stands for the side selection

                if(s1DTriangleSideBuffer[i] <= 0.5f) {
                    sampledPointsBuffer[i].value.Ng = -sampledPointsBuffer[i].value.Ng;
                    sampledPointsBuffer[i].value.Ns = -sampledPointsBuffer[i].value.Ns;
                }

            } else {
                sampledPointsBuffer[i].pdf = 0.f;
            }
        } else {
            sampledPointsBuffer[i].pdf = 0.f;
        }
    }

}

}
