#pragma once

#include <vector>
#include <bonez/embree.hpp>

#include "Ray.hpp"
#include "Intersection.hpp"
#include "SceneGeometry.hpp"

#include "topology/CurvilinearSkeleton.hpp"
#include "topology/SegmentedCurvSkel.hpp"

#include "shading/BRDF.hpp"

#include "cameras/Camera.hpp"
#include "lights/LightContainer.hpp"

namespace BnZ {

class Scene {
public:
    Scene(SceneGeometry geometry) :
        m_Geometry(std::move(geometry)) {
        buildAccel();
        extractAreaLights();
        buildSamplingDistribution();
    }

    ~Scene();

    const Material& getMaterial(uint32_t idx) const {
        return m_Geometry.getMaterial(idx);
    }

    const SceneGeometry& getGeometry() const {
        return m_Geometry;
    }

    Intersection intersect(const Ray& ray) const;

    bool occluded(const Ray& ray) const;

    const BBox3f& getBBox() const {
        return m_Geometry.getBBox();
    }

    BRDF shade(const SurfacePoint& point) const;

    const CurvilinearSkeleton* getCurvSkeleton() const {
        return m_pCurvSkel.get();
    }

    void setCurvSkeleton(CurvilinearSkeleton curvSkel) {
        m_pCurvSkel = makeUnique<CurvilinearSkeleton>(std::move(curvSkel));
        m_pSegmentedCurvSkel = makeUnique<SegmentedCurvSkel>(*m_pCurvSkel);
    }

    const SegmentedCurvSkel* getSegmentedCurvSkeleton() const {
        return m_pSegmentedCurvSkel.get();
    }

    const LightContainer& getLightContainer() const {
        return m_LightContainer;
    }

    LightContainer& getLightContainer() {
        return m_LightContainer;
    }

    void addLight(const Shared<Light>& pLight) {
        m_LightContainer.addLight(pLight);
    }

    void clearLights() {
        m_LightContainer.clear();

        extractAreaLights();
    }

    void uniformSampleSurfacePoints(uint32_t count,
                                    const float* s1DMeshBuffer, // Used to sample a mesh
                                    const float* s1DTriangleBuffer, // Used to sample a triangle
                                    const float* s1DTriangleSideBuffer, // Used to sample the side of the mesh
                                    const Vec2f* s2DTriangleBuffer, // Used to sample 2D area of the chosen triangle
                                    SurfacePointSample* sampledPointsBuffer) const;

    float getArea() const {
        return m_fTotalArea;
    }

private:
    void buildAccel();

    void extractAreaLights();

    void buildSamplingDistribution();

    SceneGeometry m_Geometry;
    RTCScene m_RTCScene;
    LightContainer m_LightContainer;

    Unique<CurvilinearSkeleton> m_pCurvSkel;
    Unique<SegmentedCurvSkel> m_pSegmentedCurvSkel;

    std::vector<float> m_MeshSamplingDistribution;
    std::vector<uint32_t> m_MeshDistributionOffsets;
    std::vector<float> m_TriangleSamplingDistributions;

    float m_fTotalArea = 0.f;
};

inline DiscreteDistribution computePowerBasedDistribution(const Scene& scene) {
    const auto& lightContainer = scene.getLightContainer();
    std::vector<float> powers;
    powers.reserve(lightContainer.size());
    for(const auto& pLight: lightContainer) {
        powers.emplace_back(luminance(pLight->getPowerUpperBound(scene)));
    }
    return DiscreteDistribution(powers.size(), powers.data());
}

inline int getMaterialID(const Scene& scene, const SurfacePoint& point) {
    return scene.getGeometry().getMesh(point.meshID).m_MaterialID;
}

inline const Material& getMaterial(const Scene& scene, const SurfacePoint& point) {
    return scene.getGeometry().getMaterial(scene.getGeometry().getMesh(point.meshID));
}

}
