#pragma once

#include <bonez/types.hpp>
#include <bonez/sampling/Sample.hpp>
#include <bonez/scene/SurfacePoint.hpp>

namespace BnZ {

class Material;
class SurfacePoint;

struct LambertBRDF {
    Vec3f m_Kd;

    LambertBRDF() {
    }

    LambertBRDF(const Vec2f& texCoords,
                const Material& material);
};

Vec3f sample(const LambertBRDF& brdf, const Vec3f& normal, const Vec3f& incidentDirection,
             const Vec2f& aRndTuple, float &cosThetaOutDir, Sample3f& outgoingDirection);

Vec3f eval(const LambertBRDF& brdf);

float pdf(const LambertBRDF& brdf, const Vec3f& normal, const Vec3f& direction);

inline Vec3f albedo(const LambertBRDF& brdf) {
    return brdf.m_Kd;
}

}
