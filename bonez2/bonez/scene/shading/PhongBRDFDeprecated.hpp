#pragma once

#include "common.hpp"
#include "Material.hpp"

#include <bonez/maths/maths.hpp>
#include <bonez/maths/BBox.hpp>
#include <bonez/sampling/Sample.hpp>
#include <bonez/scene/SurfacePoint.hpp>

namespace BnZ {

class PhongBRDFDeprecated {
public:
    enum ScatteringEvent {
        DIFFUSE_REFLECTION = 1,
        GLOSSY_REFLECTION = 1 << 1,
        ABSORPTION = 1 << 2
    };

    PhongBRDFDeprecated() {
    }

    PhongBRDFDeprecated(const SurfacePoint& point, const Material& material) {
        auto Kd = material.m_DiffuseReflectance;
        auto Ks = material.m_GlossyReflectance;
        auto shininess = material.m_Shininess;

        if(material.m_DiffuseReflectanceTexture) {
            Kd *= Vec3f(texture(*material.m_DiffuseReflectanceTexture, point.texCoords));
        }

        if(material.m_GlossyReflectanceTexture) {
            Ks *= Vec3f(texture(*material.m_GlossyReflectanceTexture, point.texCoords));
        }

        if(material.m_ShininessTexture) {
            shininess *= texture(*material.m_ShininessTexture, point.texCoords).r;
        }

        m_fShininess = shininess;
        m_Kd = Kd / pi<float>();
        m_Ks = Ks * (shininess + 2) / (2.f * pi<float>());
        m_fKdProb = luminance(m_Kd);
        m_fKsProb = luminance(m_Ks);
        float sum = m_fKdProb + m_fKsProb;
        if(sum > 0.f) {
            m_fKdProb /= sum;
            m_fKsProb /= sum;
        }

        m_N = Vec3f(point.Ns);
    }

    Vec3f sample(const Vec3f& wo, Sample3f& wi,
                 const Vec2f& s2D, ScatteringEvent* pEvent = nullptr) const;
    
    float pdf(const Vec3f& wo, const Vec3f& wi) const;
    
    Vec3f eval(const Vec3f& wi, const Vec3f& wo) const;
    
    Vec3f diffuseTerm() const {
        return m_Kd;
    }

    Vec3f glossyTerm() const {
        return m_Ks;
    }
    
    Vec3f upperBound(const Vec3f& wo, const BBox3f& bbox) const {
        return m_Kd + m_Ks;
    }
    
    Vec3f upperBound(const Vec3f& wo) const {
        return m_Kd + m_Ks;
    }
    
private:
    Vec3f m_N;
    Vec3f m_Kd = Vec3f(1);
    Vec3f m_Ks = Vec3f(1);
    float m_fShininess = 1.f;

    float m_fKdProb, m_fKsProb;
};

}
