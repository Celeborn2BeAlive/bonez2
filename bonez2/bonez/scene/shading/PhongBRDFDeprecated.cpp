#include "BRDF.hpp"

#include <bonez/sampling/shapes.hpp>
#include <iostream>

namespace BnZ {

Vec3f PhongBRDFDeprecated::sample(const Vec3f& wo, Sample3f& wi, const Vec2f& s2D, ScatteringEvent* pEvent) const {

    /*if(debug) {
        std::cerr << "s2D " << s2D << " - m_Kd " << m_Kd << std::endl;
    }*/

    if (s2D.x <= m_fKdProb) {
        // Sample the diffuse component
        Vec2f ss = Vec2f(s2D.x / m_fKdProb, s2D.y);
        
        wi = cosineSampleHemisphere(ss.x, ss.y, m_N);
        wi.pdf *= m_fKdProb;

        if(pEvent) {
            *pEvent = DIFFUSE_REFLECTION;
        }

        return m_Kd;
    }

    if (s2D.x <= (m_fKdProb + m_fKsProb)) {
        // Sample the specular component
        Vec2f ss = Vec2f((s2D.x - m_fKdProb) / m_fKsProb, s2D.y);
        
        Vec3f R = reflect(-wo, m_N); // Perfect reflecting direction

        wi = powerCosineSampleHemisphere(ss.x, ss.y, R, m_fShininess);

        if (dot(m_N, wi.value) <= 0.f) {
            if(pEvent) {
                *pEvent = GLOSSY_REFLECTION;
            }

            wi.pdf = 0.f;
            return Vec3f(0.f);
        }
        wi.pdf *= m_fKsProb;

        if(pEvent) {
            *pEvent = GLOSSY_REFLECTION;
        }

        return m_Ks * pow(dot(wi.value, R), m_fShininess);
    }

    if(pEvent) {
        *pEvent = ABSORPTION;
    }

    wi = Sample3f(Vec3f(0.f), 1 - m_fKdProb - m_fKsProb);
    return Vec3f(0.f);
}

float PhongBRDFDeprecated::pdf(const Vec3f& wo, const Vec3f& wi) const {
    if(dot(wi, m_N) <= 0.f) {
        return 0.f;
    }

    return m_fKdProb * cosineSampleHemispherePDF(wi, m_N) +
        m_fKsProb * powerCosineSampleHemispherePDF(wi, reflect(wo, m_N), m_fShininess);
}

Vec3f PhongBRDFDeprecated::eval(const Vec3f& wi, const Vec3f& wo) const {
    Vec3f R = reflect(wi, m_N);
    float dotProduct = max(0.f, dot(R, wo));
    float glossyPow = (dotProduct <= 0.f) ? 0.f : pow(dotProduct, m_fShininess);
    
    return m_Kd + m_Ks * glossyPow;
}

}
