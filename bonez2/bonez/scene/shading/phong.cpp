#include "phong.hpp"
#include "Material.hpp"

#include <bonez/maths/constants.hpp>
#include <bonez/sampling/shapes.hpp>

namespace BnZ {

PhongBRDF::PhongBRDF(const Vec2f& texCoords,
          const Material& material):
    m_Ks(material.getGlossyReflectance(texCoords)), m_fShininess(material.getShininess(texCoords)) {
    m_Ks = m_Ks * (m_fShininess + 2) * one_over_two_pi<float>();
}

Vec3f sample(const PhongBRDF& brdf, const Vec3f& normal, const Vec3f& incidentDirection,
             const Vec2f& aRndTuple, float &cosThetaOutDir, Sample3f& outgoingDirection) {
    Vec3f R = reflect(incidentDirection, normal);

    outgoingDirection = powerCosineSampleHemisphere(aRndTuple.x, aRndTuple.y, R, brdf.m_fShininess);
    cosThetaOutDir = dot(outgoingDirection.value, normal);

    if(cosThetaOutDir <= 0.f) {
        return zero<Vec3f>();
    }

    return brdf.m_Ks * pow(dot(outgoingDirection.value, R), brdf.m_fShininess);
}

Vec3f eval(const PhongBRDF& brdf, const Vec3f& normal, const Vec3f& incidentDirection, const Vec3f& outgoingDirection) {
    Vec3f R = reflect(incidentDirection, normal);
    return brdf.m_Ks * pow(dot(outgoingDirection, R), brdf.m_fShininess);
}

float pdf(const PhongBRDF& brdf, const Vec3f& normal, const Vec3f& incidentDirection, const Vec3f& outgoingDirection) {
    const Vec3f R = reflect(incidentDirection, normal);
    return powerCosineSampleHemispherePDF(outgoingDirection, R, brdf.m_fShininess);
}

}
