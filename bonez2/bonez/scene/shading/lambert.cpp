#include "lambert.hpp"
#include "Material.hpp"

#include <bonez/maths/constants.hpp>
#include <bonez/sampling/shapes.hpp>

namespace BnZ {

LambertBRDF::LambertBRDF(const Vec2f& texCoords,
            const Material& material):
    m_Kd(material.getDiffuseReflectance(texCoords) * one_over_pi<float>()) {
}

Vec3f sample(const LambertBRDF& brdf, const Vec3f& normal, const Vec3f& incidentDirection,
             const Vec2f& rndTuple, float &cosThetaOutDir, Sample3f& outgoingDirection) {
    outgoingDirection = cosineSampleHemisphere(rndTuple.x, rndTuple.y, normal);
    cosThetaOutDir = pi<float>() * outgoingDirection.pdf;

    return brdf.m_Kd;
}

Vec3f eval(const LambertBRDF& brdf) {
    return brdf.m_Kd;
}

float pdf(const LambertBRDF& brdf, const Vec3f& normal, const Vec3f& direction) {
    return cosineSampleHemispherePDF(direction, normal);
}

}
