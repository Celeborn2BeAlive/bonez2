#pragma once

#include <bonez/types.hpp>
#include <bonez/sampling/Sample.hpp>
#include <bonez/scene/SurfacePoint.hpp>

namespace BnZ {

class Material;
class SurfacePoint;

struct PhongBRDF {
    Vec3f m_Ks;
    float m_fShininess;

    PhongBRDF() {
    }

    PhongBRDF(const Vec2f& texCoords,
              const Material& material);
};

Vec3f sample(const PhongBRDF& brdf, const Vec3f& normal, const Vec3f& incidentDirection,
             const Vec2f& aRndTuple, float &cosThetaOutDir, Sample3f& outgoingDirection);

Vec3f eval(const PhongBRDF& brdf, const Vec3f& normal, const Vec3f& incidentDirection, const Vec3f& outgoingDirection);

float pdf(const PhongBRDF& brdf, const Vec3f& normal, const Vec3f& incidentDirection, const Vec3f& outgoingDirection);

inline Vec3f albedo(const PhongBRDF& brdf) {
    return brdf.m_Ks;
}

}
