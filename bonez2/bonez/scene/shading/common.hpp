#pragma once

#include <cstdint>

namespace BnZ {

enum {
    BRDF_ALL                   = 0xFFFFFFFF,    // all BSDF components for the given surface
    BRDF_DIFFUSE               = 0x000F000F,    // all diffuse BSDFs for the given surface
    BRDF_DIFFUSE_REFLECTION    = 0x00000001,    // fully diffuse reflection BSDF
    BRDF_DIFFUSE_REFRACTION    = 0x00010000,    // fully diffuse refraction BSDF
    BRDF_GLOSSY                = 0x00F000F0,    // all glossy BSDFs for the given surface
    BRDF_GLOSSY_REFLECTION     = 0x00000010,    // semi-specular reflection BSDF
    BRDF_GLOSSY_REFRACTION     = 0x00100000,    // semi-specular transmission BSDF
    BRDF_NONE                  = 0x00000000,    // no BSDF components are set for the given surface
    BRDF_REFLECTION            = 0x0000FFFF,    // all reflection BSDFs for the given surface
    BRDF_SPECULAR              = 0x0F000F00,    // all specular BSDFs for the given surface
    BRDF_SPECULAR_REFLECTION   = 0x00000100,    // perfect specular reflection BSDF
    BRDF_SPECULAR_REFRACTION   = 0x01000000,    // perfect specular transmission BSDF
    BRDF_REFRACTION            = 0xFFFF0000     // all transmission BSDFs for the given surface
};

typedef uint32_t BRDFType;

}
