#pragma once

#include <bonez/utils/Graph.hpp>
#include <bonez/utils/Grid3D.hpp>

#include "bonez/scene/Intersection.hpp"
#include "bonez/scene/VoxelGrid.hpp"

#include "Skeleton.hpp"

namespace BnZ {

class VoxelSpace;

class CurvilinearSkeleton: public Skeleton {
public:
    typedef Grid3D<GraphNodeIndex> GridType;
    typedef std::vector<Skeleton::Node> NodeArray;

    CurvilinearSkeleton() {
    }

    void addNode(const Vec3f& position, float maxBall) {
        m_Nodes.push_back({position, maxBall});
    }

    void setGraph(Graph graph) {
        m_Graph = graph;
    }

    virtual const Graph& getGraph() const {
        return m_Graph;
    }
    
    void setGrid(GridType grid) {
        m_Grid = std::move(grid);
    }

    void setWorldToGrid(const Mat4f& worldToGrid, float worldToGridScale) {
        m_WorldToLocal = worldToGrid;
        m_LocalToWorld = inverse(worldToGrid);
        m_fWorldToLocalScale = worldToGridScale;
        m_fLocalToWorldScale = 1.f / m_fWorldToLocalScale;
    }

    const Node& operator[](GraphNodeIndex idx) const {
        assert(idx != UNDEFINED_NODE);
        return m_Nodes[idx];
    }

    virtual const Skeleton::Node getNode(GraphNodeIndex idx) const {
        assert(idx != UNDEFINED_NODE);
        return { m_Nodes[idx].P, sqrt(m_Nodes[idx].maxball) };
    }

    virtual size_t size() const {
        return m_Nodes.size();
    }

    virtual bool empty() const {
        return m_Nodes.empty();
    }

    const GridType& getGrid() const {
        return m_Grid;
    }

    Vec3f getVoxelSelector(const Vec3f& position, const Vec3f& normal) const;

    Vec3f getVoxelSelector(const SurfacePoint& point) const {
        return getVoxelSelector(Vec3f(point.P), Vec3f(point.Ns));
    }

    Vec3i getVoxel(const Vec3f& position) const;

    virtual GraphNodeIndex getNearestNode(const Vec3f& position) const {
        auto voxel = getVoxel(position);
        return m_Grid(voxel.x, voxel.y, voxel.z);
    }

    virtual GraphNodeIndex getNearestNode(const Vec3f& position, const Vec3f& normal) const {
        return getNearestNode(getVoxelSelector(position, normal));
    }
 
    virtual GraphNodeIndex getNearestNode(const SurfacePoint& point) const {
        return getNearestNode(getVoxelSelector(point));
    }
    
    virtual const GraphAdjacencyList& neighbours(GraphNodeIndex idx) const {
        assert(idx != UNDEFINED_NODE);
        return m_Graph[idx];
    }

    const Mat4f& getWorldToGridMatrix() const {
        return m_WorldToLocal;
    }

    const Mat4f& getGridToWorldMatrix() const {
        return m_LocalToWorld;
    }

    float getWorldToGridScale() const {
        return m_fWorldToLocalScale;
    }

    float getGridToWorldScale() const {
        return m_fLocalToWorldScale;
    }

    void blur();

    void blurMaxballs();

private:
    Mat4f m_WorldToLocal;
    Mat4f m_LocalToWorld;
    float m_fWorldToLocalScale;
    float m_fLocalToWorldScale; // It is also the size of a voxel in world space

    NodeArray m_Nodes;
    Graph m_Graph;
    GridType m_Grid;
};

typedef std::vector<GraphNodeIndex> NodeVector;

/**
 * Compute a list of node which are local minimas in their neighbour for the maxball radius.
 */
NodeVector computeLocalMinimas(const CurvilinearSkeleton& skeleton);

CurvilinearSkeleton::GridType computeGridWrtVoxelGrid(const VoxelGrid& voxelGrid,
                                                      const CurvilinearSkeleton& skeleton);

}
