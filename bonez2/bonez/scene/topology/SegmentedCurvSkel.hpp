#pragma once

#include "CurvilinearSkeleton.hpp"
#include "Skeleton.hpp"

#include "bonez/scene/Intersection.hpp"

namespace BnZ {

class Scene;

class SegmentedCurvSkel: public Skeleton {
public:
    enum class ClusterFlag {
        NO_FLAG,
        HIGH_CURVATURE_FLAG,
        COVERAGE_FLAG,
        TOPOLOGY_FLAG
    };

    struct Cluster {
        GraphNodeIndex representative; // The representative node of the cluster
        ClusterFlag flag;
        std::vector<GraphNodeIndex> nodes; // The nodes in the cluster
        Vec3f center; // Center of the cluster
        float maxball; // Mean of the maxball contained in the cluster

        Cluster(ClusterFlag flag): flag(flag) {
        }
    };

    // A pair of clusters associated to a node of the original skeleton, with a weight
    // for each one
    // We have weight1 > weight2
    struct NodeClusterPair {
        GraphNodeIndex cluster1;
        float weight1;
        GraphNodeIndex cluster2;
        float weight2;

        NodeClusterPair():
            cluster1(UNDEFINED_NODE), weight1(0.f),
            cluster2(UNDEFINED_NODE), weight2(0.f) {
        }

        explicit NodeClusterPair(GraphNodeIndex cluster):
            cluster1(cluster), weight1(1.f),
            cluster2(UNDEFINED_NODE), weight2(0.f) {
        }

        NodeClusterPair(GraphNodeIndex c1, float w1, GraphNodeIndex c2, float w2):
            cluster1(c1), weight1(w1),
            cluster2(c2), weight2(w2) {
            // Swap to get the cluster with highest weight first
            if(w2 > w1) {
                std::swap(cluster1, cluster2);
                std::swap(weight1, weight2);
            }

            // Normalize the weights to get a sum of 1
            weight1 = weight1 / (weight1 + weight2);
            weight2 = weight2 / (weight1 + weight2);
        }

        explicit operator bool() const {
            return cluster1 != UNDEFINED_NODE;
        }
    };

    void correctGridPostProcess();

    const Cluster& operator [](GraphNodeIndex idx) const {
        assert(idx != UNDEFINED_NODE);
        return m_Clusters[idx];
    }

    virtual const Skeleton::Node getNode(GraphNodeIndex idx) const {
        return { m_Clusters[idx].center, m_Clusters[idx].maxball };
    }

    Vec3f getVoxelSelector(const Vec3f& position, const Vec3f& normal) const;

    Vec3f getVoxelSelector(const SurfacePoint& point) const {
        return getVoxelSelector(Vec3f(point.P), Vec3f(point.Ns));
    }

    Vec3i getVoxel(const Vec3f& position) const;

    virtual GraphNodeIndex getNearestNode(const Vec3f& P) const {
        assert(m_pSkeleton);

        Vec3i voxel = getVoxel(P);
        if(!m_Grid.contains(voxel)) {
            return UNDEFINED_NODE;
        }
        return m_Grid(voxel.x, voxel.y, voxel.z);
    }

    float nodeCoherence(const Vec3f& P, const Vec3f& N, GraphNodeIndex node) const {
        Vec3f wi = m_Clusters[node].center - P;
        float distance = length(wi);
        wi /= distance;
        float d1 = dot(wi, N);
        if(d1 <= 0.f) {
            return 0.f;
        }
        float distanceFactor = sqr(m_Clusters[node].maxball / distance);
        return distanceFactor;
    }

    virtual GraphNodeIndex getNearestNode(const Vec3f& P, const Vec3f& N) const {
        assert(m_pSkeleton);
        return getNearestNode(getVoxelSelector(P, N));
    }

    virtual GraphNodeIndex getNearestNode(const SurfacePoint& point) const {
        return getNearestNode(Vec3f(point.P), Vec3f(point.Ns));
    }
    
    const Cluster& getCluster(GraphNodeIndex idx) const {
        assert(idx != UNDEFINED_NODE);
        return m_Clusters[idx];
    }
    
    const Cluster& getNodeCluster(GraphNodeIndex node) const {
        assert(node != UNDEFINED_NODE);
        return m_Clusters[getNodeClusterIndex(node)];
    }
    
    GraphNodeIndex getNodeClusterIndex(GraphNodeIndex node) const {
        if(node == UNDEFINED_NODE) {
            return UNDEFINED_NODE;
        }
        return m_NodeClusters[node].cluster1;
    }

    NodeClusterPair getNodeClusterPair(GraphNodeIndex node) const {
        if(node == UNDEFINED_NODE) {
            return NodeClusterPair();
        }
        return m_NodeClusters[node];
    }
    
    virtual const GraphAdjacencyList& neighbours(GraphNodeIndex idx) const {
        assert(idx != UNDEFINED_NODE);
        return m_Graph[idx];
    }
    
    virtual size_t size() const {
        return m_Clusters.size();
    }
    
    virtual bool empty() const {
        return m_Clusters.empty();
    }

    bool build(const CurvilinearSkeleton& skeleton);

    bool buildWRTCurvature(const CurvilinearSkeleton& skeleton);

    /*
    void buildVisibilityGraph(const Scene& scene);

    bool checkVisibility(GraphNodeIndex cluster1, GraphNodeIndex cluster2) const {
        assert(!m_VisibilityGraph.empty());
        if(cluster1 == cluster2) {
            return true;
        }
        GraphNodeIndex first = std::min(cluster1, cluster2);
        GraphNodeIndex second = std::max(cluster1, cluster2);
        return m_VisibilityGraph[first][second - (first + 1)];
    }

    float getVisibilityFactor(GraphNodeIndex cluster1, GraphNodeIndex cluster2) const {
        assert(!m_VisibilityFactors.empty());
        if(cluster1 == cluster2) {
            return true;
        }
        GraphNodeIndex first = std::min(cluster1, cluster2);
        GraphNodeIndex second = std::max(cluster1, cluster2);
        return m_VisibilityFactors[first][second - (first + 1)];
    }
*/
    virtual const Graph& getGraph() const {
        return m_Graph;
    }

    SegmentedCurvSkel():
        m_pSkeleton(nullptr) {
    }

    SegmentedCurvSkel(const CurvilinearSkeleton& pSkeleton) {
        build(pSkeleton);
    }

    struct Edge {
        float minMaxball;
        Vec3f minMaxballPosition;

        Edge():
            minMaxball(std::numeric_limits<float>::max()), minMaxballPosition(0.f) {
        }

        Edge(float minMaxball, Vec3f minMaxballPosition):
            minMaxball(minMaxball), minMaxballPosition(minMaxballPosition) {
        }
    };

    const Edge& getEdge(GraphNodeIndex cluster1, GraphNodeIndex cluster2) const {
        return m_Edges[cluster1][cluster2];
    }

private:
    // m_NodeClusters[i] is the cluster index of the node i
    std::vector<NodeClusterPair> m_NodeClusters;

    std::vector<Cluster> m_Clusters;
    Graph m_Graph; // Graph of the clusters

    std::vector<std::vector<Edge>> m_Edges;

    const CurvilinearSkeleton* m_pSkeleton;

    std::vector<std::vector<bool>> m_VisibilityGraph;
    std::vector<std::vector<float>> m_VisibilityFactors;

    Grid3D<GraphNodeIndex> m_Grid;

    Mat4f m_WorldToLocal;
    Mat4f m_LocalToWorld;
    float m_fWorldToLocalScale;
    float m_fLocalToWorldScale; // It is also the size of a voxel in world space


public:
    const Mat4f& getWorldToGridMatrix() const {
        return m_WorldToLocal;
    }

    const Mat4f& getGridToWorldMatrix() const {
        return m_LocalToWorld;
    }

    float getWorldToGridScale() const {
        return m_fWorldToLocalScale;
    }

    float getGridToWorldScale() const {
        return m_fLocalToWorldScale;
    }

    void setGrid(Grid3D<GraphNodeIndex> grid) {
        m_Grid = grid;
    }

    const Grid3D<GraphNodeIndex>& getGrid() const {
        return m_Grid;
    }
};

typedef std::vector<GraphNodeIndex> ClusterVector;

/**
 * Compute a list of node which are local minimas in their neighbour for the mean maxball radius.
 */
ClusterVector computeLocalMinimas(const SegmentedCurvSkel& clustering);

/*
Grid3D<GraphNodeIndex> computeGridWrtVoxelGrid(const VoxelGrid& voxelGrid,
                                               const CurvSkelClustering& clustering);

Grid3D<GraphNodeIndex> computeGridWrtVoxelGridOF(const VoxelGrid& voxelGrid,
                                                 const CurvSkelClustering& clustering);

Grid3D<GraphNodeIndex> computeGridWithFMM(const VoxelGrid& voxelGrid,
                                          const CurvSkelClustering& clustering);

Grid3D<GraphNodeIndex> computeGridWrtVoxelGridUsingCoherence(const VoxelGrid& voxelGrid,
                                                const CurvSkelClustering& clustering);
*/

}
