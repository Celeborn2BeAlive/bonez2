#include "SegmentedCurvSkel.hpp"
#include "bonez/scene/Scene.hpp"

#include "bonez/maths/maths.hpp"

#include "bonez/scene/VoxelSpace.hpp"

#include <queue>

#include <set>

#include "bonez/itertools/itertools.hpp"

namespace BnZ {

#if 0

typedef std::tuple<Vec3i, GraphNodeIndex, float> CompareNode2;
struct CompareNode2Functor {
    bool operator ()(const CompareNode2& lhs,
                     const CompareNode2& rhs) {
        return std::get<2>(lhs) < std::get<2>(rhs);
    }
};

// This version use the radius of the maxball as priority value
// for expension; each time it advances, the priority decrease
// of 1
Grid3D<GraphNodeIndex> computeGridWrtVoxelGrid2(const VoxelGrid& voxelGrid,
                                                const CurvSkelClustering& clustering) {
    Progress progress;
    progress.init("computeGridWrtVoxelGrid", 1);

    auto size = clustering.getVoxelSpace()->getSize();
    Grid3D<GraphNodeIndex> grid(size.x, size.y, size.z);
    std::fill(begin(grid), end(grid), UNDEFINED_NODE);

    float scale = clustering.getVoxelSpace()->getWorldToLocalScale();

    std::priority_queue<CompareNode2, std::vector<CompareNode2>, CompareNode2Functor>
            queue;
    for(auto nodeIndex: range(clustering.size())) {
        float radius = clustering.getNode(nodeIndex).maxball * scale;
        Vec3i center = clustering.getVoxelSpace()->getVoxel(clustering.getNode(nodeIndex).P);
        queue.push(std::make_tuple(center, nodeIndex, radius));
    }

    while(!queue.empty()) {
        auto t = queue.top();
        queue.pop();

        Vec3i p = std::get<0>(t);
        if(grid(p.x, p.y, p.z) == UNDEFINED_NODE) {
            grid(p.x, p.y, p.z) = std::get<1>(t);

            if(!voxelGrid(p.x, p.y, p.z)) {
                iterate6neighbouring(p.x, p.y, p.z, [&](int x, int y, int z) {
                    if(grid.contains(x, y, z)) {
                        Vec3i p(x, y, z);
                        queue.push(std::make_tuple(p, std::get<1>(t), std::get<2>(t) - 1));
                    }
                });
            }
        }
    }

    progress.end();

    return grid;
}

struct CompareNodeCoherence {
    Vec3i voxel;
    GraphNodeIndex nodeIdx;
    float geoDist;
    float coherence;
};

bool operator <(const CompareNodeCoherence& lhs, const CompareNodeCoherence& rhs) {
    if(lhs.coherence < rhs.coherence) {
        return true;
    } else if(lhs.coherence == rhs.coherence) {
        return lhs.geoDist < rhs.geoDist;
    }

    return false;
}

Grid3D<GraphNodeIndex> computeGridWrtVoxelGridUsingCoherence(const VoxelGrid& voxelGrid,
                                                const CurvSkelClustering& clustering) {
    Progress progress;
    progress.init("computeGridWrtVoxelGridUsingCoherence", 1);

    auto size = clustering.getVoxelSpace()->getSize();
    Grid3D<GraphNodeIndex> grid(size.x, size.y, size.z);
    std::fill(begin(grid), end(grid), UNDEFINED_NODE);

    VoxelGrid voxelGridCopy = voxelGrid;
    std::queue<Vec3i> normalAffectationQueue;
    Grid3D<Vec3f> normalGrid(size.x, size.y, size.z, Vec3f(0.f));

    for(auto pair: index(voxelGrid)) {
        if(pair.second) {
            auto voxel = voxelGrid.coords(pair.first);
            iterate6neighbouring(voxel, [&](int x, int y, int z) {
                auto neighbourVoxel = Vec3i(x, y, z);
                if(voxelGrid.contains(neighbourVoxel) && !voxelGrid(neighbourVoxel)) {
                    normalGrid(neighbourVoxel) += Vec3f(neighbourVoxel) - Vec3f(voxel);
                    normalAffectationQueue.push(Vec3i(x, y, z));
                }
            });
        }
    }

    while(!normalAffectationQueue.empty()) {
        auto voxel = normalAffectationQueue.front();
        normalAffectationQueue.pop();

        if(!voxelGridCopy(voxel)) {
            normalGrid(voxel) = embree::normalize(normalGrid(voxel));
            voxelGridCopy(voxel) = 1;

//            iterate6neighbouring(voxel, [&](int x, int y, int z) {
//                auto neighbourVoxel = Vec3i(x, y, z);
//                if(voxelGridCopy.contains(neighbourVoxel) && !voxelGridCopy(neighbourVoxel)) {
//                    normalGrid(neighbourVoxel) += Vec3f(neighbourVoxel) - Vec3f(voxel);
//                    normalAffectationQueue.push(Vec3i(x, y, z));
//                }
//            });
        }
    }

    float scale = clustering.getVoxelSpace()->getWorldToLocalScale();

    std::priority_queue<CompareNodeCoherence> queue;
    for(auto nodeIndex: range(clustering.size())) {
        float radius = clustering.getNode(nodeIndex).maxball * scale;
        Vec3i center = clustering.getVoxelSpace()->getVoxel(clustering.getNode(nodeIndex).P);
        queue.push({ center, GraphNodeIndex(nodeIndex), 0, radius });
    }

    auto computeCoherence = [&](const Vec3i& voxel, float geoDist, GraphNodeIndex nodeIdx) -> float {
        auto node = clustering.getNode(nodeIdx);
        float maxBall = scale * node.maxball;

        float orientationFactor = 1.f;
        if(voxelGridCopy(voxel)) {
            Vec3f normal = normalGrid(voxel);
            Vec3f wi = embree::normalize(Vec3f(clustering.getVoxelSpace()->getVoxel(node.P) - voxel));
            orientationFactor = embree::dot(normal, wi);
        }

        return orientationFactor * maxBall / (geoDist * geoDist);
    };

    Grid3D<float> coherenceGrid(size.x, size.y, size.z, 0);

    while(!queue.empty()) {
        auto elt = queue.top();
        queue.pop();

        auto currentNodeIdx = grid(elt.voxel);

        if(currentNodeIdx == UNDEFINED_NODE) {
            grid(elt.voxel) = elt.nodeIdx;
            coherenceGrid(elt.voxel) = elt.coherence;

            if(!voxelGrid(elt.voxel)) {
                iterate6neighbouring(elt.voxel, [&](int x, int y, int z) {
                    Vec3i neighbourVoxel(x, y, z);
                    if(grid.contains(neighbourVoxel) && grid(neighbourVoxel) == UNDEFINED_NODE) {
                        float geoDist = elt.geoDist + embree::distance(Vec3f(elt.voxel), Vec3f(neighbourVoxel));
                        float coherence = computeCoherence(neighbourVoxel, geoDist, elt.nodeIdx);

                        queue.push({ neighbourVoxel, elt.nodeIdx, geoDist, coherence});
                    }
                });
            }
        } else if (currentNodeIdx != elt.nodeIdx) {
            float coherence = computeCoherence(elt.voxel, elt.geoDist, elt.nodeIdx);

            if(coherence > coherenceGrid(elt.voxel)) {
                grid(elt.voxel) = elt.nodeIdx;
                coherenceGrid(elt.voxel) = elt.coherence;

                if(!voxelGrid(elt.voxel)) {
                    iterate6neighbouring(elt.voxel, [&](int x, int y, int z) {
                        Vec3i neighbourVoxel(x, y, z);
                        if(grid.contains(neighbourVoxel) && grid(neighbourVoxel) == UNDEFINED_NODE) {
                            float geoDist = elt.geoDist + embree::distance(Vec3f(elt.voxel), Vec3f(neighbourVoxel));
                            float coherence = computeCoherence(neighbourVoxel, geoDist, elt.nodeIdx);

                            queue.push({ neighbourVoxel, elt.nodeIdx, geoDist, coherence});
                        }
                    });
                }
            }
        }
    }

    progress.end();

    return grid;
}

// This version use the distance to the border as priority values
// for expension;
Grid3D<GraphNodeIndex> computeGridWrtVoxelGridWithWatershed(const VoxelGrid& voxelGrid,
                                                            const CurvSkelClustering& clustering) {
    Progress progress;
    progress.init("computeGridWrtVoxelGridWithWatershed", 1);

    auto size = clustering.getVoxelSpace()->getSize();
    Grid3D<GraphNodeIndex> grid(size.x, size.y, size.z);
    std::fill(begin(grid), end(grid), UNDEFINED_NODE);

    Progress dprogress;
    dprogress.init("Pink::distanceGrid", 1);
    Grid3D<uint32_t> distanceGrid = Pink::distanceGrid(voxelGrid, 6);
    dprogress.end();

    std::priority_queue<CompareNode2, std::vector<CompareNode2>, CompareNode2Functor>
            queue;
    for(auto nodeIndex: range(clustering.size())) {
        //float radius = clustering.getNode(nodeIndex).maxball * scale;
        Vec3i center = clustering.getVoxelSpace()->getVoxel(clustering.getNode(nodeIndex).P);
        std::cerr << distanceGrid(center.x, center.y, center.z) << std::endl;
        queue.push(std::make_tuple(center, nodeIndex, distanceGrid(center.x, center.y, center.z)));
    }

    while(!queue.empty()) {
        auto t = queue.top();
        queue.pop();

        Vec3i p = std::get<0>(t);
        if(grid(p.x, p.y, p.z) == UNDEFINED_NODE) {
            grid(p.x, p.y, p.z) = std::get<1>(t);

            if(!voxelGrid(p.x, p.y, p.z)) {
                iterate6neighbouring(p.x, p.y, p.z, [&](int x, int y, int z) {
                    if(grid.contains(x, y, z)) {
                        Vec3i p(x, y, z);
                        queue.push(std::make_tuple(p, std::get<1>(t), distanceGrid(p.x, p.y, p.z)));
                    }
                });
            }
        }
    }

    progress.end();

    return grid;
}

// This version fill the maxballs (biggest first) and then fill the holes
// with a queue
Grid3D<GraphNodeIndex> computeGridWrtVoxelGrid(const VoxelGrid& voxelGrid,
                                               const CurvSkelClustering& clustering) {
    Progress progress;
    progress.init("computeGridWrtVoxelGrid2", 1);

    std::vector<GraphNodeIndex> nodes(clustering.size());
    std::vector<uint32_t> degrees(clustering.size());

    auto size = clustering.getVoxelSpace()->getSize();
    Grid3D<GraphNodeIndex> grid(size.x, size.y, size.z);
    std::fill(begin(grid), end(grid), UNDEFINED_NODE);

    float scale = clustering.getVoxelSpace()->getWorldToLocalScale();

    for(auto i: range(clustering.size())) {
        nodes[i] = i;
        degrees[i] = clustering.neighbours(i).size();
    }
    std::sort(begin(nodes), end(nodes), [&clustering, &degrees](GraphNodeIndex i, GraphNodeIndex j) {
        if(clustering.getNode(i).maxball != clustering.getNode(j).maxball) {
            return clustering.getNode(i).maxball > clustering.getNode(j).maxball;
        }
        return degrees[i] > degrees[j];
    });

    /*
    std::priority_queue<CompareNode2, std::vector<CompareNode2>, CompareNode2Functor>
            queue;*/
    std::queue<CompareNode2> queue;

    for(auto nodeIndex: nodes) {
        float radius = clustering.getNode(nodeIndex).maxball * scale;
        float sqrRadius = radius * radius;
        Vec3i center = clustering.getVoxelSpace()->getVoxel(clustering.getNode(nodeIndex).P);
        std::vector<Vec3i> stack;
        stack.emplace_back(center);
        while(!stack.empty()) {
            Vec3i p = stack.back();
            stack.pop_back();

            if(grid(p.x, p.y, p.z) == UNDEFINED_NODE) {
                grid(p.x, p.y, p.z) = nodeIndex;

                if(!voxelGrid(p.x, p.y, p.z)) {
                    iterate6neighbouring(p.x, p.y, p.z, [&](int x, int y, int z) {
                        if(grid.contains(x, y, z)) {
                            Vec3i p(x, y, z);
                            Vec3i v = p - center;
                            if(embree::dot(v, v) <= sqrRadius) {
                                stack.emplace_back(p);
                            } else {
                                queue.push(std::make_tuple(p, nodeIndex, radius));
                            }
                        }
                    });
                }
            }
        }
    }

    while(!queue.empty()) {
        auto t = queue.front();
        queue.pop();

        Vec3i p = std::get<0>(t);
        if(grid(p.x, p.y, p.z) == UNDEFINED_NODE) {
            grid(p.x, p.y, p.z) = std::get<1>(t);

            if(!voxelGrid(p.x, p.y, p.z)) {
                iterate6neighbouring(p.x, p.y, p.z, [&](int x, int y, int z) {
                    if(grid.contains(x, y, z)) {
                        Vec3i p(x, y, z);
                        queue.push(std::make_tuple(p, std::get<1>(t), std::get<2>(t) - 1));
                    }
                });
            }
        }
    }

    progress.end();

    return grid;
}

static Grid3D<uint32_t> getSeeds(const VoxelGrid& voxelGrid, const CurvSkelClustering& clustering) {
    Grid3D<uint32_t> grid(voxelGrid.width(), voxelGrid.height(), voxelGrid.depth(), 0);
    for(auto nodeIdx: range(clustering.size())) {
        Vec3i voxel = clustering.getVoxelSpace()->getVoxel(clustering.getNode(nodeIdx).P);
        grid(voxel) = nodeIdx + 1;
    }
    return grid;
}

static Grid3D<float> getMetric(const VoxelGrid& voxelGrid, const CurvSkelClustering& clustering) {
    Grid3D<float> metric { voxelGrid.width(), voxelGrid.height(), voxelGrid.depth(), float(embree::inf) };

    std::vector<GraphNodeIndex> nodes(clustering.size());
    std::vector<uint32_t> degrees(clustering.size());

    auto size = clustering.getVoxelSpace()->getSize();
    Grid3D<GraphNodeIndex> grid(size.x, size.y, size.z);
    std::fill(begin(grid), end(grid), UNDEFINED_NODE);

    for(auto i: range(clustering.size())) {
        nodes[i] = i;
        degrees[i] = clustering.neighbours(i).size();
    }
    std::sort(begin(nodes), end(nodes), [&clustering, &degrees](GraphNodeIndex i, GraphNodeIndex j) {
        if(clustering.getNode(i).maxball != clustering.getNode(j).maxball) {
            return clustering.getNode(i).maxball > clustering.getNode(j).maxball;
        }
        return degrees[i] > degrees[j];
    });

    float scale = clustering.getVoxelSpace()->getWorldToLocalScale();

    for(auto nodeIdx: nodes) {
        auto node = clustering.getNode(nodeIdx);

        Vec3i seedVoxel = clustering.getVoxelSpace()->getVoxel(node.P);

        std::queue<Vec3i> voxelQueue;
        voxelQueue.push(seedVoxel);

        while(!voxelQueue.empty()) {
            Vec3i voxel = voxelQueue.front();
            voxelQueue.pop();

            if(metric(voxel) == float(embree::inf)) {
                metric(voxel) = 1.f / node.maxball;

                iterate6neighbouring(voxel, [&](int x, int y, int z) {
                    Vec3i p(x, y, z);
                    if(voxelGrid.contains(x, y, z) && !voxelGrid(p) && metric(p) == float(embree::inf)) {
                        float d = embree::distance(Vec3f(seedVoxel), Vec3f(p));

                        if(d < node.maxball * scale) {
                            voxelQueue.push(p);
                        }
                    }
                });
            }
        }
    }

    for(auto pair: index(voxelGrid)) {
        if(!pair.second && metric[pair.first] == float(embree::inf)) {
            metric[pair.first] = float(embree::inf) - 1;
        }
    }

    return metric;
}

void getMetricAndSeeds(
        const VoxelGrid& voxelGrid, const CurvSkelClustering& clustering,
        Grid3D<uint32_t>& seeds, Grid3D<float>& metric) {
    seeds = Grid3D<uint32_t> { voxelGrid.width(), voxelGrid.height(), voxelGrid.depth(), 0 };
    metric = Grid3D<float> { voxelGrid.width(), voxelGrid.height(), voxelGrid.depth(), float(embree::inf) };

    std::vector<GraphNodeIndex> nodes(clustering.size());
    std::vector<uint32_t> degrees(clustering.size());

    auto size = clustering.getVoxelSpace()->getSize();
    Grid3D<GraphNodeIndex> grid(size.x, size.y, size.z);
    std::fill(begin(grid), end(grid), UNDEFINED_NODE);

    for(auto i: range(clustering.size())) {
        nodes[i] = i;
        degrees[i] = clustering.neighbours(i).size();
    }
    std::sort(begin(nodes), end(nodes), [&clustering, &degrees](GraphNodeIndex i, GraphNodeIndex j) {
        if(clustering.getNode(i).maxball != clustering.getNode(j).maxball) {
            return clustering.getNode(i).maxball > clustering.getNode(j).maxball;
        }
        return degrees[i] > degrees[j];
    });

    float scale = clustering.getVoxelSpace()->getWorldToLocalScale();

    for(auto nodeIdx: nodes) {
        auto node = clustering.getNode(nodeIdx);

        Vec3i seedVoxel = clustering.getVoxelSpace()->getVoxel(node.P);

        std::queue<Vec3i> voxelQueue;
        voxelQueue.push(seedVoxel);

        while(!voxelQueue.empty()) {
            Vec3i voxel = voxelQueue.front();
            voxelQueue.pop();

            if(metric(voxel) == float(embree::inf)) {
                metric(voxel) = 1.f / node.maxball;
                seeds(voxel) = nodeIdx + 1;

                iterate6neighbouring(voxel, [&](int x, int y, int z) {
                    Vec3i p(x, y, z);
                    if(voxelGrid.contains(x, y, z) && !voxelGrid(p) && metric(p) == float(embree::inf)) {
                        float d = embree::distance(Vec3f(seedVoxel), Vec3f(p));

                        if(d < node.maxball * scale) {
                            voxelQueue.push(p);
                        }
                    }
                });
            }
        }
    }

    for(auto pair: index(voxelGrid)) {
        if(!pair.second && metric[pair.first] == float(embree::inf)) {
            metric[pair.first] = 1;
        }
    }
}

Grid3D<GraphNodeIndex> computeGridWithFMM(const VoxelGrid& voxelGrid,
                                          const CurvSkelClustering& clustering) {
    Progress progress;
    progress.init("computeGridWithFMM", 1);

    Grid3D<GraphNodeIndex> newGrid(voxelGrid.width(), voxelGrid.height(), voxelGrid.depth());

//    transform(Pink::fmm(voxelGrid, getSeeds(voxelGrid, clustering), getMetric(voxelGrid, clustering)), newGrid,
//        [](uint32_t seed) {
//            if(seed) { return seed - 1; }
//            return UNDEFINED_NODE;
//    });

    Grid3D<uint32_t> seeds;
    Grid3D<float> metric;

    getMetricAndSeeds(voxelGrid, clustering, seeds, metric);

    transform(Pink::fmm(voxelGrid, seeds, metric), newGrid,
        [](uint32_t seed) {
            if(seed) { return seed - 1; }
            return UNDEFINED_NODE;
    });

    progress.end();

    return newGrid;
}

Grid3D<GraphNodeIndex> computeGridWrtVoxelGrid3(const VoxelGrid& voxelGrid,
                                               const CurvSkelClustering& clustering) {
    Progress progress;
    progress.init("computeGridWrtVoxelGrid3", 1);

    /*
    Progress dprogress;
    dprogress.init("Pink::distanceGrid", 1);
    Grid3D<uint32_t> distanceGrid = Pink::distanceGrid(voxelGrid, 6);
    dprogress.end();
*/
    std::vector<GraphNodeIndex> nodes(clustering.size());
    std::vector<uint32_t> degrees(clustering.size());

    auto size = clustering.getVoxelSpace()->getSize();
    Grid3D<GraphNodeIndex> grid(size.x, size.y, size.z);
    std::fill(begin(grid), end(grid), UNDEFINED_NODE);

    float scale = clustering.getVoxelSpace()->getWorldToLocalScale();

    for(auto i: range(clustering.size())) {
        nodes[i] = i;
        degrees[i] = clustering.neighbours(i).size();
    }
    std::sort(begin(nodes), end(nodes), [&clustering, &degrees](GraphNodeIndex i, GraphNodeIndex j) {
        if(clustering.getNode(i).maxball != clustering.getNode(j).maxball) {
            return clustering.getNode(i).maxball > clustering.getNode(j).maxball;
        }
        return degrees[i] > degrees[j];
    });

    std::queue<CompareNode2> queue;

    for(auto nodeIndex: nodes) {
        float radius = clustering.getNode(nodeIndex).maxball * scale;
        float sqrRadius = radius * radius;
        Vec3i center = clustering.getVoxelSpace()->getVoxel(clustering.getNode(nodeIndex).P);
        std::vector<Vec3i> stack;
        stack.emplace_back(center);

        // fill the ball
        while(!stack.empty()) {
            Vec3i p = stack.back();
            stack.pop_back();

            if(grid(p.x, p.y, p.z) == UNDEFINED_NODE) {
                grid(p.x, p.y, p.z) = nodeIndex;

                if(!voxelGrid(p.x, p.y, p.z)) {
                    iterate6neighbouring(p.x, p.y, p.z, [&](int x, int y, int z) {
                        if(grid.contains(x, y, z)) {
                            Vec3i p(x, y, z);
                            Vec3i v = p - center;
                            if(embree::dot(v, v) <= sqrRadius) {
                                stack.emplace_back(p);
                            } else {
                                queue.push(std::make_tuple(p, nodeIndex, radius));
                            }
                        }
                    });
                }
            }
        }
    }

    /*
    while(!queue.empty()) {
        auto t = queue.front();
        queue.pop();

        Vec3i p = std::get<0>(t);
        if(grid(p.x, p.y, p.z) == UNDEFINED_NODE &&
                distanceGrid(p.x, p.y, p.z) <= std::get<2>(t)) {
            grid(p.x, p.y, p.z) = std::get<1>(t);

            if(!voxelGrid(p.x, p.y, p.z)) {
                iterate6neighbouring(p.x, p.y, p.z, [&](int x, int y, int z) {
                    if(grid.contains(x, y, z)) {
                        Vec3i p(x, y, z);
                        queue.push(std::make_tuple(p, std::get<1>(t), std::get<2>(t)));
                    }
                });
            }
        }
    }*/

    progress.end();

    return grid;
}

Grid3D<GraphNodeIndex> computeGridWrtVoxelGridOF(const VoxelGrid& voxelGrid,
                                                 const CurvSkelClustering& clustering) {
    Progress progress;
    progress.init("computeGridWrtVoxelGridOF", 1);

    auto size = clustering.getVoxelSpace()->getSize();
    Grid3D<GraphNodeIndex> grid(size.x, size.y, size.z);
    std::fill(begin(grid), end(grid), UNDEFINED_NODE);

    Progress oprogress;
    oprogress.init("Opening function", 1);

    Grid3D<uint32_t> openingFunction = Pink::openingFunction(clustering.getVoxelSpace()->getVoxelGrid(),
                                                           Pink::distanceGrid(clustering.getVoxelSpace()->getVoxelGrid(), 26));

    oprogress.end();

    std::priority_queue<CompareNode2, std::vector<CompareNode2>, CompareNode2Functor>
            queue;
    for(auto nodeIndex: range(clustering.size())) {
        //float radius = clustering.getNode(nodeIndex).maxball * scale;
        Vec3i center = clustering.getVoxelSpace()->getVoxel(clustering.getNode(nodeIndex).P);
        std::cerr << openingFunction(center.x, center.y, center.z) << std::endl;
        queue.push(std::make_tuple(center, nodeIndex, openingFunction(center.x, center.y, center.z)));
    }

    while(!queue.empty()) {
        auto t = queue.top();
        queue.pop();

        Vec3i p = std::get<0>(t);
        if(grid(p.x, p.y, p.z) == UNDEFINED_NODE) {
            grid(p.x, p.y, p.z) = std::get<1>(t);

            if(!voxelGrid(p.x, p.y, p.z)) {
                iterate6neighbouring(p.x, p.y, p.z, [&](int x, int y, int z) {
                    if(grid.contains(x, y, z)) {
                        Vec3i p(x, y, z);
                        queue.push(std::make_tuple(p, std::get<1>(t), openingFunction(p.x, p.y, p.z)));
                    }
                });
            }
        }
    }

    progress.end();

    return grid;
}

typedef std::tuple<Vec3i, GraphNodeIndex, uint32_t, uint32_t> CompareNode3;
struct CompareNode3Functor {
    bool operator ()(const CompareNode3& lhs,
                     const CompareNode3& rhs) {
        return std::get<2>(lhs) < std::get<2>(rhs);
    }
};

Grid3D<GraphNodeIndex> computeGridWrtVoxelGridOF2(const VoxelGrid& voxelGrid,
                                                 const CurvSkelClustering& clustering) {
    Progress progress;
    progress.init("computeGridWrtVoxelGridOF", 1);

    auto size = clustering.getVoxelSpace()->getSize();
    Grid3D<GraphNodeIndex> grid(size.x, size.y, size.z);
    std::fill(begin(grid), end(grid), UNDEFINED_NODE);

    Progress oprogress;
    oprogress.init("Opening function", 1);

    Grid3D<uint32_t> openingFunction = Pink::openingFunction(clustering.getVoxelSpace()->getVoxelGrid(),
                                                           Pink::distanceGrid(clustering.getVoxelSpace()->getVoxelGrid(), 26));

    oprogress.end();

    float scale = clustering.getVoxelSpace()->getWorldToLocalScale();

    std::priority_queue<CompareNode3, std::vector<CompareNode3>, CompareNode3Functor>
            queue;
    for(auto nodeIndex: range(clustering.size())) {
        float radius = clustering.getNode(nodeIndex).maxball * scale;
        Vec3i center = clustering.getVoxelSpace()->getVoxel(clustering.getNode(nodeIndex).P);
        queue.push(std::make_tuple(center, nodeIndex, radius, openingFunction(center)));
    }

    while(!queue.empty()) {
        auto t = queue.top();
        queue.pop();

        Vec3i p = std::get<0>(t);
        if(grid(p.x, p.y, p.z) == UNDEFINED_NODE) {
            grid(p.x, p.y, p.z) = std::get<1>(t);

            if(!voxelGrid(p.x, p.y, p.z)) {
                iterate6neighbouring(p.x, p.y, p.z, [&](int x, int y, int z) {
                    if(grid.contains(x, y, z)) {
                        float radius = clustering.getNode(std::get<1>(t)).maxball * scale;

                        Vec3i p(x, y, z);
                        if(std::get<3>(t) >= openingFunction(p)) {
                            queue.push(std::make_tuple(p, std::get<1>(t), std::get<2>(t) - 1, std::get<3>(t)));
                        }
                    }
                });
            }
        }
    }

    progress.end();

    return grid;
}
#endif

Vec3i SegmentedCurvSkel::getVoxel(const Vec3f& position) const {
    auto P = m_WorldToLocal * Vec4f(position, 1);
    return Vec3i(P.x, P.y, P.z);
}

Vec3f SegmentedCurvSkel::getVoxelSelector(const Vec3f& position, const Vec3f& normal) const {
    return position + 2.f * m_fLocalToWorldScale * normal;
}

#if 0

void CurvSkelClustering::correctGridPostProcess() {
    const VoxelGrid& voxelGrid = m_pVoxelSpace->getVoxelGrid();

    std::set<Vec3i> voxelsToCorrect;

    // Compute the set of all voxels to possibly correct: they are
    // the voxels at distance 1 from the surfaces
    for(auto voxel: index(voxelGrid)) {
        if(voxel.second) {
            Vec3i coords = voxelGrid.coords(voxel.first);

            auto process = [&voxelsToCorrect, &voxelGrid](int x, int y, int z) {
                if(voxelGrid.contains(x, y, z) && !voxelGrid(x, y, z)) {
                    voxelsToCorrect.insert(Vec3i(x, y, z));
                }
            };

            iterate6neighbouring(coords, process);
        }
    }

    std::queue<std::tuple<Vec3i, Vec3f, GraphNodeIndex>> queue;

    // For each voxel to correct, estimates the normal of that voxel and try
    // to improve the association of cluster
    for(auto voxel: voxelsToCorrect) {
        Vec3i coords = voxel;
        Vec3f N(0.f);
        size_t count = 0;

        auto approxNormal = [&coords, &N, &count, &voxelGrid](int x, int y, int z) {
            if(voxelGrid.contains(x, y, z) && voxelGrid(x, y, z)) {
                Vec3f Nn = Vec3f(coords.x - x, coords.y - y, coords.z - z);
                N += Nn;
                count += 1;
            }
        };

        iterate6neighbouring(coords.x, coords.y, coords.z, approxNormal);

        if(count > 0) {
            Vec3f P = embree::xfmPoint(m_pVoxelSpace->getLocalToWorldTransform(), Vec3f(coords.x, coords.y, coords.z));
            GraphNodeIndex nodeIdx = m_Grid(coords);
            if(nodeIdx != UNDEFINED_NODE) {
                N = embree::normalize(N / (float) count);
                N = embree::normalize(
                            embree::xfmNormal(m_pVoxelSpace->getLocalToWorldTransform(), N)
                        );

                Skeleton::Node node = getNode(nodeIdx);
                Vec3f wi = node.P - P;
                float d = embree::length(wi);
                wi /= d;
                float validity = embree::dot(wi, N) * node.maxball / d;
                bool hasChanged = false;

                for(auto neighbour: neighbours(nodeIdx)) {
                    node = getNode(neighbour);
                    wi = node.P - P;
                    d = embree::length(wi);
                    wi /= d;
                    float v = embree::dot(wi, N) * node.maxball / d;

                    if(v > validity) {
                        validity = v;
                        nodeIdx = neighbour;
                        hasChanged = true;
                    }
                }

                if(hasChanged) {
                    m_Grid(coords) = nodeIdx;
                    auto process = [&queue, &voxelGrid, this, nodeIdx, N, coords](int x, int y, int z) {
                        if(voxelGrid.contains(x, y, z) && !voxelGrid(x, y, z)
                                && m_Grid(x, y, z) != nodeIdx) {
                            Vec3f D(x - coords.x, y - coords.y, z - coords.z);
                            if(embree::dot(D, N) > 0) {
                                queue.push(std::make_tuple(Vec3i(x, y, z), N, nodeIdx));
                            }
                        }
                    };

                    iterate6neighbouring(coords, process);
                }
            }
        }
    }

    /*
    while(!queue.empty()) {
        auto p = queue.front();
        queue.pop();

        Vec3i coords = std::get<0>(p);
        Vec3f N = std::get<1>(p);
        GraphNodeIndex candidateNode = std::get<2>(p);

        GraphNodeIndex nodeIdx = m_Grid(coords);
        if(nodeIdx == candidateNode) {
            continue;
        }

        Vec3f P = embree::xfmPoint(m_pVoxelSpace->getLocalToWorldTransform(), Vec3f(coords.x, coords.y, coords.z));

        Skeleton::Node node1 = getNode(nodeIdx);
        Vec3f wi1 = node1.P - P;
        float d1 = embree::length(wi1);
        wi1 /= d1;
        float validity1 = embree::dot(wi1, N) * node1.maxball / d1;

        Skeleton::Node node2 = getNode(candidateNode);
        Vec3f wi2 = node2.P - P;
        float d2 = embree::length(wi2);
        wi2 /= d2;
        float validity2 = embree::dot(wi2, N) * node2.maxball / d2;

        if(validity2 > validity1) {
            m_Grid(coords) = candidateNode;

            auto process = [&queue, &voxelGrid, this, N, candidateNode, coords](int x, int y, int z) {
                if(voxelGrid.contains(x, y, z) && !voxelGrid(x, y, z)
                        && m_Grid(x, y, z) != candidateNode) {
                    Vec3f D(x - coords.x, y - coords.y, z - coords.z);
                    if(embree::dot(D, N) > 0) {
                        queue.push(std::make_tuple(Vec3i(x, y, z), N, candidateNode));
                    }
                }
            };

            iterate6neighbouring(coords, process);
        }
    }*/

    // Remove small artefacts
    /*
    GraphNodeIndex buffer[27];
    int idx = 0;

    for(auto voxel: index(voxelGrid)) {
        Vec3i coords = voxelGrid.coords(voxel.first);
        for(int i = -1; i <= 1; ++i) {
            for(int j = -1; j <= 1; ++j) {
                for(int k = -1; k <= 1; ++k) {
                    Vec3i voxel(coords.x + i, coords.y + j, coords.z + k);
                    if(voxelGrid.contains(voxel)) {
                        buffer[idx++] = m_Grid(voxel);
                    }
                }
            }
        }

        int currentMaxCount = 0;
        GraphNodeIndex bestNode = UNDEFINED_NODE;
        for(int i = 0; i < 27; ++i) {
            int count = 1;
            for(int j = i + 1; j < 27; ++j) {
                if(buffer[i] == buffer[j]) {
                    ++count;
                }
            }
            if(count > currentMaxCount) {
                currentMaxCount = count;
                bestNode = buffer[i];
            }
        }

        m_Grid(coords) = bestNode;

        idx = 0;
    }
    */

    progress.end();
}

#endif

bool SegmentedCurvSkel::build(const CurvilinearSkeleton& skeleton) {
    if(skeleton.empty()) {
        return false;
    }

    if(buildWRTCurvature(skeleton)) {
        m_WorldToLocal = skeleton.getWorldToGridMatrix();
        m_LocalToWorld = skeleton.getGridToWorldMatrix();
        m_fWorldToLocalScale = skeleton.getWorldToGridScale();
        m_fLocalToWorldScale = skeleton.getGridToWorldScale();

//        if(!m_pVoxelSpace->getVoxelGrid().empty()) {
//            m_Grid = computeGridWrtVoxelGrid(m_pVoxelSpace->getVoxelGrid(), *this);
//        } else {
            m_Grid = skeleton.getGrid();
            for(GraphNodeIndex& node: m_Grid) {
                node = getNodeClusterIndex(node);
            }
//          }

        //correctGridPostProcess();

        return true;
    }
    return false;
}

bool SegmentedCurvSkel::buildWRTCurvature(const CurvilinearSkeleton& skeleton) {
    m_pSkeleton = &skeleton;

    // Build the line set of the skeleton
    SkeletonLineSet lines(skeleton);
    //std::cerr << lines.size() << std::endl;

    lines.segmentWrtMaxballCurvature(skeleton, 0.7f);
    //std::cerr << lines.size() << std::endl;

    lines.segmentWrtMaxballCoverage(skeleton);
    //lines.segmentWrtMaxballVariance(skeleton);
    //std::cerr << lines.size() << std::endl;

    std::vector<std::vector<Edge>> edges;

    // Build the clusters
    m_NodeClusters.resize(skeleton.size());
    for(const auto& i: index(lines)) {
        const auto& line = i.second;

        GraphNodeIndex first = line.extremums[0];
        GraphNodeIndex second = line.extremums[1];

        if(!m_NodeClusters[first]) {
            GraphNodeIndex newClusterIndex = m_Clusters.size();
            ClusterFlag flag = ClusterFlag::NO_FLAG;
            if(lines.getLineOf(first) == SkeletonLineSet::CURVATURE_NODE) {
                flag = ClusterFlag::HIGH_CURVATURE_FLAG;
            } else if(lines.getLineOf(first) == SkeletonLineSet::COVERAGE_NODE) {
                flag = ClusterFlag::COVERAGE_FLAG;
            }
            m_Clusters.emplace_back(flag);
            Cluster& cluster = m_Clusters.back();

            cluster.representative = first;
            cluster.nodes.push_back(first);
            cluster.center = skeleton[first].P;
            cluster.maxball = sqrt(skeleton[first].maxball);

            m_NodeClusters[first] = NodeClusterPair(newClusterIndex);
            m_Graph.emplace_back();
            edges.emplace_back();
        }

        if(!m_NodeClusters[second]) {
            GraphNodeIndex newClusterIndex = m_Clusters.size();
            ClusterFlag flag = ClusterFlag::NO_FLAG;
            if(lines.getLineOf(second) == SkeletonLineSet::CURVATURE_NODE) {
                flag = ClusterFlag::HIGH_CURVATURE_FLAG;
            } else if(lines.getLineOf(second) == SkeletonLineSet::COVERAGE_NODE) {
                flag = ClusterFlag::COVERAGE_FLAG;
            }
            m_Clusters.emplace_back(flag);
            Cluster& cluster = m_Clusters.back();

            cluster.representative = second;
            cluster.nodes.push_back(second);
            cluster.center = skeleton[second].P;
            cluster.maxball = sqrt(skeleton[second].maxball);

            m_NodeClusters[second] = NodeClusterPair(newClusterIndex);
            m_Graph.emplace_back();
            edges.emplace_back();
        }

        GraphNodeIndex firstCluster = m_NodeClusters[first].cluster1;
        GraphNodeIndex secondCluster = m_NodeClusters[second].cluster1;
        m_Graph[firstCluster].push_back(secondCluster);
        m_Graph[secondCluster].push_back(firstCluster);

        float minMaxball = std::numeric_limits<float>::max();
        Vec3f minMaxballPosition(0.f);
        for(auto node: line.nodes) {
            float d1 = distanceSquared(skeleton[node].P, line.extremumPositions[0]);
            float d2 = distanceSquared(skeleton[node].P, line.extremumPositions[1]);

            float b1 = skeleton[line.extremums[0]].maxball;
            float b2 = skeleton[line.extremums[1]].maxball;

            float c1 = b1 / d1;
            float c2 = b2 / d2;

            /*GraphNodeIndex cluster;
            if(c1 > c2) {
                cluster = firstCluster;
            } else {
                cluster = secondCluster;
            }*/
            m_NodeClusters[node] = NodeClusterPair(firstCluster, c1, secondCluster, c2);
            m_Clusters[m_NodeClusters[node].cluster1].nodes.push_back(node);

            if(skeleton.getNode(node).maxball < minMaxball) {
                minMaxball = skeleton.getNode(node).maxball;
                minMaxballPosition = skeleton.getNode(node).P;
            }
        }

        edges[firstCluster].push_back(Edge {minMaxball, minMaxballPosition});
        edges[secondCluster].push_back(Edge {minMaxball, minMaxballPosition});
    }

    // Link neighbours that are not
    for(auto i: range(skeleton.size())) {
        if(lines.getLineOf(i) < 0 && m_NodeClusters[i]) {
            for(auto neighbour: skeleton.neighbours(i)) {
                if(lines.getLineOf(neighbour) < 0 && m_NodeClusters[neighbour]) {
                    m_Graph[m_NodeClusters[i].cluster1].emplace_back(m_NodeClusters[neighbour].cluster1);
                    edges[m_NodeClusters[i].cluster1].push_back(Edge {std::numeric_limits<float>::max(), Vec3f(0.f)});
                }
            }
        }
    }

    m_Edges.resize(m_Clusters.size());
    for(auto i: range(m_Clusters.size())) {
        m_Edges[i].resize(m_Clusters.size());

        for(auto neighbour: index(neighbours(i))) {
            m_Edges[i][neighbour.second] = edges[i][neighbour.first];
        }
    }




    // Sort the new nodes (according to maxball radius, biggest first)
    std::vector<GraphNodeIndex> sortedNodes;
    sortedNodes.reserve(size());
    for(auto i = 0u; i < size(); ++i) {
        sortedNodes.emplace_back(i);
    }
    std::sort(begin(sortedNodes), end(sortedNodes), [this](GraphNodeIndex n1, GraphNodeIndex n2) {
        return m_Clusters[n1].maxball > m_Clusters[n2].maxball;
    });


    // For each node (in maxball radius order), merge it with nodes contained in its maxball
    std::vector<GraphNodeIndex> flags(sortedNodes.size(), UNDEFINED_NODE);
    std::vector<std::vector<GraphNodeIndex>> newClusterLists;

    for(auto i = 0u; i < sortedNodes.size(); ++i) {
        if(flags[i] == UNDEFINED_NODE) {
            auto newClusterIdx = newClusterLists.size();
            newClusterLists.emplace_back();
            newClusterLists.back().emplace_back(i);
            flags[i] = newClusterIdx;

            for(auto j = 0u; j < size(); ++j) {
                if(flags[j] == UNDEFINED_NODE) {
                    float d = distance(m_Clusters[i].center, m_Clusters[j].center);
                    if(d < m_Clusters[i].maxball) {
                        flags[j] = newClusterIdx;
                        newClusterLists.back().emplace_back(j);
                    }
                }
            }
        }
    }


    // Build the new list of clusters and the new adjacency graph
    std::vector<Cluster> newClusters;
    newClusters.reserve(newClusterLists.size());
    Graph newGraph;
    newGraph.reserve(newClusterLists.size());

    for(auto i = 0u; i < newClusterLists.size(); ++i) {
        newClusters.emplace_back(m_Clusters[newClusterLists[i][0]]);
        auto& nodes = newClusters.back().nodes;
        for(auto j = 1u; j < newClusterLists[i].size(); ++j) {
            const auto& cluster = m_Clusters[newClusterLists[i][j]];
            nodes.insert(end(nodes), begin(cluster.nodes), end(cluster.nodes));
        }
        newGraph.emplace_back();
        for(auto clusterIdx: newClusterLists[i]) {
            for(auto neighbour: neighbours(clusterIdx)) {
                if(flags[neighbour] != i) {
                    newGraph.back().emplace_back(flags[neighbour]);
                }
            }
        }
    }

    swap(newClusters, m_Clusters);
    swap(newGraph, m_Graph);

    for(auto i = 0u; i < m_Clusters.size(); ++i) {
        const auto& cluster = m_Clusters[i];
        for(const auto& node: cluster.nodes) {
            m_NodeClusters[node].cluster1 = i;
            m_NodeClusters[node].weight1 = 1.f;
            m_NodeClusters[node].weight2 = 0.f;
        }
    }

    std::cerr << "Number of clusters = " << size() << std::endl;

    return true;
}

#if 0

void CurvSkelClustering::buildVisibilityGraph(const Scene& scene) {
    Progress progress;

    progress.init("Visibility graph computation", size());

    m_VisibilityGraph.resize(size());
    m_VisibilityFactors.resize(size());

    static const size_t nbSamples = 0;
    Random rng;

    static const size_t nbSensors = 2048;

    RayCounter rayCounter;
    for(auto i: range(size())) {
        m_VisibilityGraph[i].resize(size() - (i + 1));
        m_VisibilityFactors[i].resize(size() - (i + 1));

        Skeleton::Node ni = getNode(i);
        for(auto j: range(i + 1, size())) {
            m_VisibilityFactors[i][j - (i + 1)] = 0.f;

            Skeleton::Node nj = getNode(j);
            Vec3f w = nj.P - ni.P;
            float l = embree::length(w);
            w /= l;
            Ray ray(ni.P, w, 0, l);
            if(!occluded(ray, scene, rayCounter)) {
                m_VisibilityGraph[i][j - (i + 1)] = true;

               m_VisibilityFactors[i][j - (i + 1)] += 1.f;
            } else {
                m_VisibilityGraph[i][j - (i + 1)] = false;
            }


            AffineSpace3f framei = coordinateSystem(ni.P, w);
            AffineSpace3f framej = coordinateSystem(nj.P, -w);

            for(auto _: range(nbSamples)) {
                Vec2f si = embree::uniformSampleDisk(Vec2f(rng.getFloat(), rng.getFloat()), ni.maxball);
                Vec2f sj = embree::uniformSampleDisk(Vec2f(rng.getFloat(), rng.getFloat()), nj.maxball);
                Vec3f Pi = embree::xfmPoint(framei, Vec3f(si.x, 0, si.y));
                Vec3f Pj = embree::xfmPoint(framej, Vec3f(sj.x, 0, sj.y));

                Vec3f w = Pj - Pi;
                float l = embree::length(w);
                w /= l;
                Ray ray(Pi, w, 0, l);
                if(!occluded(ray, scene, rayCounter)) {
                    m_VisibilityFactors[i][j - (i + 1)] += 1.f;
                }
            }
            m_VisibilityFactors[i][j - (i + 1)] /= (nbSamples + 1);
        }

    /*
        for(auto j: range(nbSensors)) {
            Sample3f direction = embree::uniformSampleSphere(rng.getFloat(), rng.getFloat());
            Intersection I = intersect(Ray(ni.P, direction.value), scene);
            if(I) {
                m_Sensors[i].emplace_back(I);
            }
        }*/
    }




    progress.end();

    /*
    progress.init("Sensors computation", size());

    static const size_t nbSensors = 2048;

    m_Sensors.resize(size());
    m_SensorTrees.resize(size());

    for(auto i: range(size())) {
        Skeleton::Node ni = getNode(i);

        for(auto j: range(nbSensors)) {
            Sample3f direction = embree::uniformSampleSphere(rng.getFloat(), rng.getFloat());
            Intersection I = intersect(Ray(ni.P, direction.value), scene);
            if(I) {
                m_Sensors[i].emplace_back(I);
            }
        }

        m_SensorTrees[i].build(m_Sensors[i].size(), [this, i](uint32_t j) { return m_Sensors[i][j].P; });
    }

    progress.end();
    */
}

#endif

/**
 * Compute a list of node which are local minimas in their neighbour for the mean maxball radius.
 */
ClusterVector computeLocalMinimas(const SegmentedCurvSkel& clustering) {
    ClusterVector clusters;
    
    for(GraphNodeIndex idx = 0, end = clustering.size(); idx != end; ++idx) {
        bool isMinima = true;
        for(GraphNodeIndex c: clustering.neighbours(idx)) {
            if(clustering.getCluster(c).maxball < clustering.getCluster(idx).maxball) {
                isMinima = false;
                break;
            }
        }
        if(isMinima) {
            clusters.push_back(idx);
        }
    }
    
    return clusters;
}

}
