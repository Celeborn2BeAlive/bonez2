#pragma once

#include "bonez/atb.hpp"
#include "bonez/common.hpp"
#include "bonez/sys/FileSystem.hpp"
#include "bonez/parsing/parsing.hpp"

#include "cameras/ProjectiveCamera.hpp"
#include "lights/LightContainer.hpp"

namespace BnZ {

class ConfigManager {
public:
    ConfigManager() = default;

    void setPath(FilePath path);

    const FilePath& getPath() const {
        return m_Path;
    }

    void storeConfig(const std::string& name, const ProjectiveCamera& camera,
                     const LightContainer& lights);

    FilePath loadConfig(const std::string& name, ProjectiveCamera& camera,
                    Scene& scene, float resX, float resY, float zNear, float zFar,
                    tinyxml2::XMLDocument* pDoc = nullptr) const;

    FilePath loadConfig(uint32_t index, ProjectiveCamera& camera,
                    Scene& scene, float resX, float resY, float zNear, float zFar,
                    tinyxml2::XMLDocument* pDoc = nullptr) const;

    const std::vector<std::string>& getConfigs() const {
        return m_Configs;
    }

private:
    FilePath m_Path;
    std::vector<std::string> m_Configs;
};

}
