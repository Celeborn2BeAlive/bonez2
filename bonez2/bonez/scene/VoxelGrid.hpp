#pragma once

#include <vector>
#include <cstdint>

#include "bonez/sys/FileSystem.hpp"
#include <bonez/utils/Grid3D.hpp>

namespace BnZ {

typedef Grid3D<uint8_t> VoxelGrid;

VoxelGrid loadVoxelGridFromPGM(const FilePath& filepath);

}
