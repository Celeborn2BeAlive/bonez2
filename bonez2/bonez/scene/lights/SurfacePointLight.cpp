#include "SurfacePointLight.hpp"

namespace BnZ {

DiffuseSurfacePointLight::DiffuseSurfacePointLight(
        const Vec3f& position, const Vec3f& normal, const Vec3f& Le):
    m_fLe(length(Le)),
    m_Color(m_fLe ? Le / m_fLe : Le),
    m_Position(position),
    m_Normal(normalize(normal)),
    m_Le(Le) {
}

Vec3f DiffuseSurfacePointLight::getPowerUpperBound(const Scene& scene) const {
    return pi<float>() * Vec3f(m_Le);
}

void DiffuseSurfacePointLight::exposeIO(TwBar* bar, UpdateFlag& flag) {
    auto cb = [&flag]() { flag.addUpdate(); };
    atb::addVarRWCB(bar, ATB_VAR(m_Position.x), cb);
    atb::addVarRWCB(bar, ATB_VAR(m_Position.y), cb);
    atb::addVarRWCB(bar, ATB_VAR(m_Position.z), cb);
    atb::addVarRWCB(bar, ATB_VAR(m_Normal), cb);

    auto cb2 = [this, &flag]() {
        m_Le = m_fLe * m_Color;
        flag.addUpdate();
    };

    atb::addVarRWCB(bar, ATB_VAR(m_Color), cb2);
    atb::addVarRWCB(bar, ATB_VAR(m_fLe), cb2);
}

void DiffuseSurfacePointLight::accept(LightVisitor& visitor) {
    visitor.visit(*this);
}



DiffuseSurfacePointLightVertex::DiffuseSurfacePointLightVertex(const DiffuseSurfacePointLight& light, float pdf):
    EmissionVertex(&light, pi<float>() * light.m_Le, pdf),
    m_Light(light) {
}

Vec3f DiffuseSurfacePointLightVertex::sampleWi(RaySample& ray, const Vec2f& s2D, float* cosLight) const {
    auto dirSample = cosineSampleHemisphere(s2D.x, s2D.y, m_Light.m_Normal);
    ray = RaySample(Ray(m_Light.m_Position, dirSample.value), dirSample.pdf);

    if(cosLight) {
        *cosLight = dot(dirSample.value, m_Light.m_Normal);
    }

    return dot(dirSample.value, m_Light.m_Normal) * Vec3f(one_over_pi<float>());
}

float DiffuseSurfacePointLightVertex::pdfWi(const Vec3f& wo) const {
    return cosineSampleHemispherePDF(wo, m_Light.m_Normal);
}

Vec3f DiffuseSurfacePointLightVertex::Le(const Vec3f& wo) const {
    return Vec3f(one_over_pi<float>());
}

bool DiffuseSurfacePointLightVertex::isDelta() const {
    return true;
}

bool DiffuseSurfacePointLightVertex::isFinite() const {
    return true;
}

float DiffuseSurfacePointLightVertex::G(const SurfacePoint& point, Ray& rShadowRay) const {
    auto wi = m_Light.m_Position - point.P;
    auto d = length(wi);
    if(d == 0.f) {
        return 0.f;
    }
    wi /= d;
    float G = abs(dot(point.Ns, wi)) * max(0.f, dot(m_Light.m_Normal, -wi)) / sqr(d);
    rShadowRay = Ray(point, wi, d);
    return G;
}

float DiffuseSurfacePointLightVertex::pdfWrtArea(const SurfacePoint& point) const {
    auto wi = m_Light.m_Position - point.P;
    auto d = length(wi);
    if(d == 0.f) {
        return 0.f;
    }
    wi /= d;
    float g = abs(dot(point.Ns, wi)) / sqr(d);
    return g * pdfWi(-wi);
}

float DiffuseSurfacePointLightVertex::g(const SurfacePoint& point) const {
    return 0.f;
}

}
