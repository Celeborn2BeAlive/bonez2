#include "PointLight.hpp"

namespace BnZ {

PointLight::PointLight(const Vec3f& position,
           const Vec3f& intensity):
    m_fIntensity(length(intensity)),
    m_Color(m_fIntensity ? intensity / m_fIntensity : intensity),
    m_Position(position),
    m_Intensity(intensity) {
}

Vec3f PointLight::getPowerUpperBound(const Scene& scene) const {
    return four_pi<float>() * Vec3f(m_Intensity);
}

void PointLight::exposeIO(TwBar* bar, UpdateFlag& flag) {
    auto cb = [&flag]() { flag.addUpdate(); };
    atb::addVarRWCB(bar, ATB_VAR(m_Position.x), cb);
    atb::addVarRWCB(bar, ATB_VAR(m_Position.y), cb);
    atb::addVarRWCB(bar, ATB_VAR(m_Position.z), cb);

    auto cb2 = [this, &flag]() {
        m_Intensity = m_fIntensity * m_Color;
        flag.addUpdate();
    };

    atb::addVarRWCB(bar, ATB_VAR(m_Color), cb2);
    atb::addVarRWCB(bar, ATB_VAR(m_fIntensity), cb2);
}

void PointLight::accept(LightVisitor& visitor) {
    visitor.visit(*this);
}



PointLightVertex::PointLightVertex(const PointLight& light, float pdf):
    EmissionVertex(&light, four_pi<float>() * light.m_Intensity, pdf),
    m_Light(light) {
}

Vec3f PointLightVertex::sampleWi(RaySample& ray, const Vec2f& s2D, float* cosLight) const {
    auto dirSample = uniformSampleSphere(s2D.x, s2D.y);
    ray = RaySample(Ray(m_Light.m_Position, dirSample.value), dirSample.pdf);

    if(cosLight) {
        *cosLight = 1.f;
    }

    return Vec3f(one_over_four_pi<float>());
}

float PointLightVertex::pdfWi(const Vec3f& wo) const {
    return uniformSampleSpherePDF();
}

Vec3f PointLightVertex::Le(const Vec3f& wo) const {
    return Vec3f(one_over_four_pi<float>());
}

bool PointLightVertex::isDelta() const {
    return true;
}

bool PointLightVertex::isFinite() const {
    return true;
}

float PointLightVertex::G(const SurfacePoint& point, Ray& rShadowRay) const {
    auto wi = m_Light.m_Position - point.P;
    auto d = length(wi);
    if(d == 0.f) {
        return 0.f;
    }
    wi /= d;
    float G = abs(dot(point.Ns, wi)) / sqr(d);
    rShadowRay = Ray(point, wi, d);
    return G;
}

float PointLightVertex::pdfWrtArea(const SurfacePoint& point) const {
    auto wi = m_Light.m_Position - point.P;
    auto d = length(wi);
    if(d == 0.f) {
        return 0.f;
    }
    wi /= d;
    float g = abs(dot(point.Ns, wi)) / sqr(d);
    return g * pdfWi(-wi);
}

float PointLightVertex::g(const SurfacePoint& point) const {
    return 0.f;
}

}
