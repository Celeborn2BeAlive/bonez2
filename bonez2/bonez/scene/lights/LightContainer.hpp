#pragma once

#include <bonez/sampling/DiscreteDistribution.hpp>
#include <bonez/common.hpp>
#include <bonez/atb.hpp>
#include <bonez/sys/memory.hpp>

#include "Light.hpp"

namespace BnZ {

class LightVisitor;

class LightContainer {
public:
private:
    void setUpdate() {
        m_UpdateFlag.addUpdate();
        if(m_pBar) {
            exposeIO(m_pBar);
        }
    }

    using LightVector = std::vector<Shared<Light>>;
public:
    using const_iterator = LightVector::const_iterator;

    bool hasChanged(UpdateFlag& flag) const {
        if(flag != m_UpdateFlag) {
            flag = m_UpdateFlag;
            return true;
        }
        return false;
    }

    void clear() {
        m_Lights.clear();
        setUpdate();
    }

    bool empty() const {
        return m_Lights.empty();
    }

    uint32_t addLight(const Shared<Light>& pLight) {
        m_Lights.emplace_back(pLight);
        setUpdate();
        return m_Lights.size() - 1;
    }

    void removeLight(uint32_t idx) {
        m_Lights.erase(std::begin(m_Lights) + idx);
        setUpdate();
    }

    size_t size() const {
        return m_Lights.size();
    }

    const Shared<Light>& getLight(uint32_t idx) const {
        return m_Lights[idx];
    }

    int getLightID(const Light* pLight) const {
        auto it = std::find_if(std::begin(m_Lights), std::end(m_Lights), [pLight](const Shared<Light>& pCandidate) {
            return pCandidate.get() == pLight;
        });
        if(it == std::end(m_Lights)) {
            return -1;
        }
        return it - std::begin(m_Lights);
    }

    void exposeIO(TwBar* bar) {
        m_pBar = bar;
        atb::removeAllVars(bar);
        atb::addVarROCB(bar, "lightCount", [this]() -> uint32_t {
                            return size();
                        });
        atb::addVarRWCB(bar, "selectedLight", m_nSelectedLight, [this, bar]() {
            exposeIO(bar);
        });
        atb::addButton(bar, "remove", [this, bar]() {
            removeLight(m_nSelectedLight);
        });

        TwAddSeparator(bar, nullptr, nullptr);

        if(m_nSelectedLight < size()) {
            const auto& pLight = getLight(m_nSelectedLight);
            pLight->exposeIO(bar, m_UpdateFlag);
        }
    }

    void accept(LightVisitor& visitor) const {
        for(const auto& pLight: m_Lights) {
            pLight->accept(visitor);
        }
    }

    const_iterator begin() const {
        return std::begin(m_Lights);
    }

    const_iterator end() const {
        return std::end(m_Lights);
    }

private:
    TwBar* m_pBar = nullptr;
    UpdateFlag m_UpdateFlag;

    uint32_t m_nSelectedLight = 0;

    LightVector m_Lights;
};

}
