#pragma once

#include <bonez/maths/maths.hpp>
#include "EmissionVertex.hpp"

#include "bonez/scene/Scene.hpp"
#include "Light.hpp"
#include "LightVisitor.hpp"

namespace BnZ {

class DirectionalLight: public Light {
    float m_fLe;
    Col3f m_Color;
public:
    Vec3f m_IncidentDirection;
    Vec3f m_Le;

    DirectionalLight(const Vec3f& wi,
                     const Vec3f& Le);

    Vec3f getPowerUpperBound(const Scene& scene) const override;

    void exposeIO(TwBar* bar, UpdateFlag& flag) override;

    void accept(LightVisitor& visitor) override;
};

class DirectionalLightVertex: public EmissionVertex {
    static Sample3f computeSampledPosition(const DirectionalLight& light, const Vec2f& s2D, const Scene& scene);
public:
    DirectionalLight m_Light;
    const Scene* m_pScene = nullptr;

    DirectionalLightVertex(const DirectionalLight& light, float pdf, const Scene& scene);

    Vec3f sampleWi(RaySample& ray, const Vec2f& s2D, float* cosLight) const override;

    float pdfWi(const Vec3f& wo) const override;

    Vec3f Le(const Vec3f& wo) const override;

    bool isDelta() const override;

    bool isFinite() const override;

    float G(const SurfacePoint& point, Ray& shadowRay) const override;

    float pdfWrtArea(const SurfacePoint& point) const override;

    float g(const SurfacePoint& point) const override;
};

}
