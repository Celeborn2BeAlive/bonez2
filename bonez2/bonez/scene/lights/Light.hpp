#pragma once

#include <bonez/atb.hpp>
#include <bonez/common.hpp>
#include <bonez/types.hpp>
#include <bonez/maths/maths.hpp>

namespace BnZ {

class Scene;
class DirectLightSampler;
class LightVisitor;

class Light {
public:
    virtual ~Light() {
    }

    virtual Vec3f getPowerUpperBound(const Scene& scene) const = 0;

    virtual void exposeIO(TwBar* bar, UpdateFlag& flag) = 0;

    virtual void accept(LightVisitor& visitor) = 0;
};

}
