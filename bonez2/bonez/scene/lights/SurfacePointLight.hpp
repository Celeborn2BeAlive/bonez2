#pragma once

#include <bonez/maths/constants.hpp>
#include "EmissionVertex.hpp"
#include <bonez/sampling/shapes.hpp>

#include "Light.hpp"
#include "LightVisitor.hpp"

namespace BnZ {

class DiffuseSurfacePointLight: public Light {
    float m_fLe;
    Col3f m_Color;
public:
    Vec3f m_Position;
    Vec3f m_Normal;
    Vec3f m_Le;

    DiffuseSurfacePointLight(
            const Vec3f& position, const Vec3f& normal, const Vec3f& Le);

    Vec3f getPowerUpperBound(const Scene& scene) const override;

    void exposeIO(TwBar* bar, UpdateFlag& flag) override;

    void accept(LightVisitor& visitor) override;
};

class DiffuseSurfacePointLightVertex: public EmissionVertex {
public:
    DiffuseSurfacePointLight m_Light;

    DiffuseSurfacePointLightVertex(const DiffuseSurfacePointLight& light, float pdf);

    Vec3f sampleWi(RaySample& ray, const Vec2f& s2D, float* cosLight) const override;

    float pdfWi(const Vec3f& wo) const override;

    Vec3f Le(const Vec3f& wo) const override;

    bool isDelta() const override;

    float G(const SurfacePoint& point, Ray& rShadowRay) const override;

    float pdfWrtArea(const SurfacePoint& point) const override;

    float g(const SurfacePoint& point) const override;

    bool isFinite() const override;
};

}
