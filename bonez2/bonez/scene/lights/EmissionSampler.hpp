#pragma once

#include <bonez/types.hpp>
#include <bonez/scene/Scene.hpp>
#include <bonez/scene/SurfacePoint.hpp>
#include <bonez/scene/Ray.hpp>

#include "EmissionVertex.hpp"

namespace BnZ {

class EmissionSampler {
public:
    virtual ~EmissionSampler() {
    }

    void initFrame(const Scene& scene);

    Unique<EmissionVertex> sample(const Scene& scene, float sLightIdx, float s1D, const Vec2f& s2D) const;

    float pdf(uint32_t lightID) const;

private:
    UpdateFlag m_UpdateFlag;
    DiscreteDistribution m_PowerBasedDistribution;
};

}
