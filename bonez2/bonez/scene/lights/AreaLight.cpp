#include "AreaLight.hpp"
#include <bonez/scene/SceneGeometry.hpp>
#include <bonez/maths/maths.hpp>
#include <bonez/scene/shading/Material.hpp>
#include <bonez/scene/Scene.hpp>
#include <bonez/sampling/shapes.hpp>
#include "LightVisitor.hpp"

namespace BnZ {

AreaLight::AreaLight(uint32_t meshIdx, const TriangleMesh& mesh, const Material& material):
    m_nMeshIdx(meshIdx) {
    m_Power = material.m_EmittedRadiance * pi<float>() * mesh.getArea();
}

Vec3f AreaLight::getPowerUpperBound(const Scene& scene) const {
    return m_Power;
}

void AreaLight::exposeIO(TwBar* bar, UpdateFlag& flag) {
    // Nothing to expose -> materials are not mutable
}

void AreaLight::accept(LightVisitor& visitor) {
    visitor.visit(*this);
}

Vec3f AreaLight::sample(const Scene& scene, float s1D, const Vec2f& s2D,
             SurfacePointSample& surfacePoint) const {
    const auto& mesh = scene.getGeometry().getMesh(m_nMeshIdx);

    auto triangleIdx = clamp(uint32_t(s1D * mesh.getTriangleCount()),
                             0u, uint32_t(mesh.getTriangleCount() - 1));

    const auto& triangle = mesh.getTriangle(triangleIdx);
    auto sampledUV = uniformSampleTriangleUVs(s2D.x, s2D.y,
                                              mesh.getVertex(triangle.v0).position,
                                              mesh.getVertex(triangle.v1).position,
                                              mesh.getVertex(triangle.v2).position);

    scene.getGeometry().getSurfacePoint(m_nMeshIdx, triangleIdx,
                               sampledUV.x, sampledUV.y, surfacePoint.value);
    auto area = mesh.getTriangleArea(triangleIdx);
    surfacePoint.pdf = 1.f / (mesh.getTriangleCount() * area);

    auto Le = scene.getGeometry().getMaterial(mesh.m_MaterialID).m_EmittedRadiance;
    if(scene.getGeometry().getMaterial(mesh.m_MaterialID).m_EmittedRadianceTexture) {
        Le *= texture(*scene.getGeometry().getMaterial(mesh.m_MaterialID).m_EmittedRadianceTexture,
                      surfacePoint.value.texCoords).xyz();
    }

    return Le;
}

float AreaLight::pdf(const SurfacePoint& surfacePoint, const Scene& scene) const {
    const auto& mesh = scene.getGeometry().getMesh(m_nMeshIdx);
    auto area = mesh.getTriangleArea(surfacePoint.triangleID);
    return 1.f / (mesh.getTriangleCount() * area);
}

void AreaLight::pdf(const SurfacePoint& surfacePoint, const Vec3f& outgoingDirection,
            const Scene& scene, float& pointPdf, float& directionPdf) const {
    pointPdf = pdf(surfacePoint, scene);
    directionPdf = cosineSampleHemispherePDF(outgoingDirection, surfacePoint.Ns);
}

AreaLightVertex::AreaLightVertex(const AreaLight& light, const SurfacePoint& sampledPoint, float pdf, const Vec3f& Le):
    EmissionVertex(&light, pi<float>() * Le, pdf),
    m_SampledPoint(sampledPoint) {
}

Vec3f AreaLightVertex::sampleWi(RaySample& ray, const Vec2f& s2D, float* cosLight) const {
    auto dirSample = cosineSampleHemisphere(s2D.x, s2D.y, m_SampledPoint.Ns);
    ray = RaySample(Ray(m_SampledPoint.P, dirSample.value), dirSample.pdf);

    if(cosLight) {
        *cosLight = dot(dirSample.value, m_SampledPoint.Ns);
    }

    return dot(dirSample.value, m_SampledPoint.Ns) * Vec3f(one_over_pi<float>());
}

float AreaLightVertex::pdfWi(const Vec3f& wo) const {
    return cosineSampleHemispherePDF(wo, m_SampledPoint.Ns);
}

Vec3f AreaLightVertex::Le(const Vec3f& wo) const {
    return Vec3f(one_over_pi<float>());
}

bool AreaLightVertex::isDelta() const {
    return false;
}

bool AreaLightVertex::isFinite() const {
    return true;
}

float AreaLightVertex::G(const SurfacePoint& point, Ray& shadowRay) const {
    auto wo = point.P - m_SampledPoint.P;
    auto dist = length(wo);
    if(dist == 0.f) {
        return 0.f;
    }
    wo /= dist;
    shadowRay = Ray(point, m_SampledPoint, -wo, dist);
    return max(0.f, dot(wo, m_SampledPoint.Ns)) * abs(dot(-wo, point.Ns)) / sqr(dist);
}

float AreaLightVertex::pdfWrtArea(const SurfacePoint& point) const {
    auto wi = m_SampledPoint.P - point.P;
    auto d = length(wi);
    if(d == 0.f) {
        return 0.f;
    }
    wi /= d;
    float g = abs(dot(point.Ns, wi)) / sqr(d);
    return g * pdfWi(-wi);
}

float AreaLightVertex::g(const SurfacePoint& point) const {
    auto wo = point.P - m_SampledPoint.P;
    auto dist = length(wo);
    if(dist == 0.f) {
        return 0.f;
    }
    wo /= dist;
    return max(0.f, dot(wo, m_SampledPoint.Ns)) / sqr(dist);
}

}
