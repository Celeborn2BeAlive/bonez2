#pragma once

#include <bonez/common.hpp>
#include <bonez/types.hpp>
#include <bonez/scene/Ray.hpp>

namespace BnZ {

class Light;
class Camera;

class EmissionVertex {
public:
    const Light* m_pLight = nullptr;

    Vec3f m_Power;
    Vec3f m_RadiantExitance;
    float m_fPdf;

    EmissionVertex(const Light* pLight, const Vec3f& radiantExitance, float pdf):
        m_pLight(pLight), m_Power(radiantExitance / pdf), m_RadiantExitance(radiantExitance), m_fPdf(pdf) {
    }

    // Sample incoming importance field
    virtual Vec3f sampleWi(RaySample& ray, const Vec2f& s2D, float* cosLight = nullptr) const = 0;

    // Return the pdf of computing an incoming direction with sampleWi
    virtual float pdfWi(const Vec3f& wo) const = 0;

    const Vec3f& getRadiantExitance() const {
        return m_RadiantExitance;
    }

    // Return the pdf of sampling this vertex
    float pdf() const {
        return m_fPdf;
    }

    Vec3f power() const {
        return m_Power;
    }

    // Evaluate directional emitted radiance from this vertex toward an outgoing direction
    virtual Vec3f Le(const Vec3f& wo) const = 0;

    // Return true if the node cannot be reached by a ray (for exemple point lights or directional lights)
    virtual bool isDelta() const = 0;

    virtual bool isFinite() const = 0;

    // Compute the geometric factor between this vertex and a surface point.
    // Also compute a shadow ray STARTING at the surface point (so shadowRay.dir is an incident direction
    // of the surface point)
    virtual float G(const SurfacePoint& point, Ray& shadowRay) const = 0;

    // Compute the pdf wrt. area of sampling a a direction toward a point
    // with sampleWi.
    // The result of this function is valid only if point is "visible"
    // from this vertex.
    // This function allows to compute the pdf wrt to path space
    // of the path [ this, point ] by using pdf() * pdfWrtArea(point)
    virtual float pdfWrtArea(const SurfacePoint& point) const = 0;

    // Compute the conversion factor required to transform a probability density wrt. solid angle at
    // the point to a probability density wrt. area.
    // So if p(w) is the probability density of sampling the direction going from point to the emission vertex,
    // then p(w) * g(point) is the probability density of sampling the emission vertex wrt. area (provided that
    // the emission vertex is visible from point).
    virtual float g(const SurfacePoint& point) const = 0;
};

}
