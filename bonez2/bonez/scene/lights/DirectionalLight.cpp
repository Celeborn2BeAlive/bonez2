#include "DirectionalLight.hpp"

#include "bonez/scene/Scene.hpp"

#include <bonez/sampling/shapes.hpp>

namespace BnZ {

DirectionalLight::DirectionalLight(const Vec3f& wi,
                 const Vec3f& Le):
    m_fLe(length(Le)),
    m_Color(m_fLe ? Le / m_fLe : Le),
    m_IncidentDirection(normalize(wi)), m_Le(Le) {
}

Vec3f DirectionalLight::getPowerUpperBound(const Scene& scene) const {
    Vec3f center;
    float radius;
    boundingSphere(scene.getBBox(), center, radius);
    return Vec3f(m_Le) * pi<float>() * radius * radius;
}

void DirectionalLight::exposeIO(TwBar* bar, UpdateFlag& flag) {
    auto cb = [&flag]() { flag.addUpdate(); };
    atb::addVarRWCB(bar, ATB_VAR(m_IncidentDirection), cb);

    auto cb2 = [this, &flag]() {
        m_Le = m_fLe * m_Color;
        flag.addUpdate();
    };

    atb::addVarRWCB(bar, ATB_VAR(m_Color), cb2);
    atb::addVarRWCB(bar, ATB_VAR(m_fLe), cb2);
}

void DirectionalLight::accept(LightVisitor& visitor) {
    visitor.visit(*this);
}

Sample3f DirectionalLightVertex::computeSampledPosition(const DirectionalLight& light, const Vec2f& s2D, const Scene& scene) {
    Vec3f center;
    float radius;
    boundingSphere(scene.getBBox(), center, radius);

    // Center of the disk
    center += light.m_IncidentDirection * radius;

    Vec2f pos2D = uniformSampleDisk(s2D, radius);
    float pdf = 1.f / (pi<float>() * radius * radius);

    auto matrix = frameY(center, light.m_IncidentDirection);
    return Sample3f(matrix * Vec4f(pos2D.x, 0, pos2D.y, 1.f), pdf);
}

DirectionalLightVertex::DirectionalLightVertex(const DirectionalLight& light, float pdf, const Scene& scene):
    EmissionVertex(&light, light.m_Le, pdf),
    m_Light(light),
    m_pScene(&scene) {
}

Vec3f DirectionalLightVertex::sampleWi(RaySample& ray, const Vec2f& s2D, float* cosLight) const {
    auto sampledPos = computeSampledPosition(m_Light, s2D, *m_pScene);
    ray = RaySample(Ray(sampledPos.value, -m_Light.m_IncidentDirection), sampledPos.pdf);

    if(cosLight) {
        *cosLight = 1.f;
    }

    return Vec3f(1.f);
}

float DirectionalLightVertex::pdfWi(const Vec3f& wo) const {
    Vec3f center;
    float radius;
    boundingSphere(m_pScene->getBBox(), center, radius);
    return 1.f / (pi<float>() * radius * radius);
}

Vec3f DirectionalLightVertex::Le(const Vec3f& wo) const {
    return Vec3f(1.f);
}

bool DirectionalLightVertex::isDelta() const {
    return true;
}

bool DirectionalLightVertex::isFinite() const {
    return false;
}

float DirectionalLightVertex::G(const SurfacePoint& point, Ray& shadowRay) const {
    auto wi = m_Light.m_IncidentDirection;
    shadowRay = Ray(point, wi);
    return abs(dot(point.Ns, wi));
}

float DirectionalLightVertex::pdfWrtArea(const SurfacePoint& point) const {
    auto wi = m_Light.m_IncidentDirection;
    float g = max(0.f, dot(point.Ns, wi));
    return g * pdfWi(-wi);
}

float DirectionalLightVertex::g(const SurfacePoint& point) const {
    return 0.f; // A directional light cannot be intersected
}

}
