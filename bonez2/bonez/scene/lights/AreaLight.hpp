#pragma once

#include "Light.hpp"
#include "EmissionVertex.hpp"

namespace BnZ {

class Scene;
class TriangleMesh;
class Material;

class AreaLight: public Light {
public:
    AreaLight(uint32_t meshIdx, const TriangleMesh& mesh, const Material& material);

    Vec3f getPowerUpperBound(const Scene& scene) const override;

    void exposeIO(TwBar* bar, UpdateFlag& flag) override;

    void accept(LightVisitor& visitor) override;

    uint32_t getMeshIdx() const {
        return m_nMeshIdx;
    }

    // Sample a point and return the emitted exitant radiance at that point
    Vec3f sample(const Scene& scene, float s1D, const Vec2f& s2D,
                 SurfacePointSample& surfacePoint) const;

    float pdf(const SurfacePoint& surfacePoint, const Scene& scene) const;

    void pdf(const SurfacePoint& surfacePoint, const Vec3f& outgoingDirection,
                const Scene& scene, float& pointPdf, float& directionPdf) const;
private:
    uint32_t m_nMeshIdx;
    Vec3f m_Power;
};

class AreaLightVertex: public EmissionVertex {
public:
    AreaLightVertex(const AreaLight& light, const SurfacePoint& sampledPoint, float pdf, const Vec3f& Le);

    Vec3f sampleWi(RaySample& ray, const Vec2f& s2D, float* cosLight) const override;

    float pdfWi(const Vec3f& wo) const override;

    Vec3f Le(const Vec3f& wo) const override;

    bool isDelta() const override;

    bool isFinite() const override;

    float G(const SurfacePoint& point, Ray& shadowRay) const override;

    float pdfWrtArea(const SurfacePoint& point) const override;

    float g(const SurfacePoint& point) const override;

private:
    SurfacePoint m_SampledPoint;
};

}
