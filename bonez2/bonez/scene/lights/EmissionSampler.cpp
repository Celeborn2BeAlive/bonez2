#include "EmissionSampler.hpp"

#include "AreaLight.hpp"
#include "PointLight.hpp"
#include "EnvironmentLight.hpp"
#include "DirectionalLight.hpp"
#include "LightContainer.hpp"
#include "SurfacePointLight.hpp"

#include "Light.hpp"

namespace BnZ {

void EmissionSampler::initFrame(const Scene& scene) {
    if(scene.getLightContainer().hasChanged(m_UpdateFlag)) {
        m_PowerBasedDistribution = computePowerBasedDistribution(scene);
    }
}

struct EmissionSamplerVisitor: public LightVisitor {
    const EmissionSampler& m_Sampler;
    const Scene& m_Scene;
    Sample1u m_Light;
    float m_fS1D;
    Vec2f m_fS2D;
    Unique<EmissionVertex> m_pVertex;

    EmissionSamplerVisitor(const EmissionSampler& sampler,
                           const Scene& scene,
                           const Sample1u& light, float s1D, const Vec2f& s2D):
        m_Sampler(sampler),
        m_Scene(scene),
        m_Light(light), m_fS1D(s1D), m_fS2D(s2D) {
    }

    void visit(const Light& light) override {
        std::cerr << "EmissionSamplerVisitor: unknown light type." << std::endl;
    }

    void visit(const PointLight& light) {
        m_pVertex = makeUnique<PointLightVertex>(light, m_Light.pdf);
    }

    void visit(const DirectionalLight& light) {
        m_pVertex = makeUnique<DirectionalLightVertex>(light, m_Light.pdf, m_Scene);
    }

    void visit(const DiffuseSurfacePointLight& light) {
        m_pVertex = makeUnique<DiffuseSurfacePointLightVertex>(light, m_Light.pdf);
    }

    void visit(const AreaLight& light) {
        SurfacePointSample sampledPoint;
        auto Le = light.sample(m_Scene, m_fS1D, m_fS2D, sampledPoint);
        m_pVertex = makeUnique<AreaLightVertex>(light, sampledPoint.value, m_Light.pdf * sampledPoint.pdf, Le);
    }
};

Unique<EmissionVertex> EmissionSampler::sample(const Scene& scene, float sLightIdx, float s1D, const Vec2f& s2D) const {
    const auto& lights = scene.getLightContainer();
    if(lights.empty()) {
        return nullptr;
    }

    auto s = m_PowerBasedDistribution.sample(sLightIdx);
    auto pLight = lights.getLight(s.value);
    assert(pLight);

    EmissionSamplerVisitor visitor(*this, scene, s, s1D, s2D);
    pLight->accept(visitor);

    return std::move(visitor.m_pVertex);
}

float EmissionSampler::pdf(uint32_t lightID) const {
    return m_PowerBasedDistribution.pdf(lightID);
}

}
