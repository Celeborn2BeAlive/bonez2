#include "VoxelGrid.hpp"

#include <fstream>
#include <stdexcept>
#include <sstream>

using namespace std;

namespace BnZ {

VoxelGrid loadVoxelGridFromPGM(const FilePath& filepath) {
    ifstream in(filepath.c_str());

    if (!in) {
        throw runtime_error("Unable to open the file " + filepath.str() + ".");
    }

    std::string ext = filepath.ext();

    if (ext != "ppm" && ext != "pgm") {
        throw runtime_error("Invalid format for the file " + filepath.str() +
            ". Recognized formats are .ppm / .pgm");
    }

    string line;
    getline(in, line, '\n');

    // Read file code
    if ((line.find("P5") == string::npos)) {
        throw std::runtime_error("PPM / PGM invalid file (error in the code).");
    }

    // Skip commentaries
    do {
        getline(in, line, '\n');
    }
    while (line.at(0) == '#');

    size_t w, h, d;
    {
        istringstream iss(line);

        // Read the size of the grid
        iss >> w;
        iss >> h;
        iss >> d;
    }
    getline(in, line, '\n'); // contains max grey value = 255
    if(line != "255") {
        throw std::runtime_error("PPM / PGM invalid file (doesn't contains chars').");
    }

    VoxelGrid grid(w, h, d);
    in.read((char*) grid.data(), grid.size() * sizeof(VoxelGrid::value_type));

    return grid;
}

}
