#include "ConfigManager.hpp"

namespace BnZ {

void ConfigManager::setPath(FilePath path) {
    m_Path = std::move(path);
    createDirectory(m_Path);
    Directory dir(m_Path);
    if(!dir) {
        throw std::runtime_error("Unable to open configurations directory");
    }
    for(const auto& config: dir.files()) {
        m_Configs.emplace_back(config.str());
    }
}

void ConfigManager::storeConfig(const std::string& name,
                                const ProjectiveCamera& camera,
                                const LightContainer& lights) {
    auto path = m_Path + name;
    createDirectory(path);
    tinyxml2::XMLDocument doc;
    auto pRoot = doc.NewElement("Config");
    doc.InsertFirstChild(pRoot);

    auto pCamera = doc.NewElement("ProjectiveCamera");
    pRoot->InsertEndChild(pCamera);

    storePerspectiveCamera(*pCamera, camera);

    auto pLights = doc.NewElement("Lights");
    pRoot->InsertEndChild(pLights);

    storeLights(lights, *pLights);

    doc.SaveFile((path + "config.bnz.xml").c_str());

    m_Configs.emplace_back(name);
}

FilePath ConfigManager::loadConfig(const std::string& name,
                               ProjectiveCamera& camera,
                               Scene& scene,
                               float resX, float resY, float zNear, float zFar,
                               tinyxml2::XMLDocument* pDoc) const {
    auto path = m_Path + name;
    tinyxml2::XMLDocument doc;
    if(!pDoc) {
        pDoc = &doc;
    }

    if(tinyxml2::XML_NO_ERROR != pDoc->LoadFile(
                (path + "config.bnz.xml").c_str())) {
        throw std::runtime_error("ConfigManager: Unable to load config " + name);
    }
    auto pRoot = pDoc->RootElement();
    auto pCamera = pRoot->FirstChildElement("ProjectiveCamera");

    Vec3f eye, point, up;
    float fovY;

    loadPerspectiveCamera(*pCamera, eye, point, up, fovY);

    camera = ProjectiveCamera(lookAt(eye, point, up), fovY, resX, resY, zNear, zFar);
    /*
    camera.lookAt(eye, point, up);
    camera.setPerspective(fovY, ratio, zNear, zFar);*/

    scene.clearLights();
    loadLights(scene, pRoot->FirstChildElement("Lights"));

    return path;
}

FilePath ConfigManager::loadConfig(uint32_t index, ProjectiveCamera& camera,
                Scene& scene, float resX, float resY, float zNear, float zFar, tinyxml2::XMLDocument* pDoc) const {
    return loadConfig(m_Configs[index], camera, scene, resX, resY, zNear, zFar, pDoc);
}

}
