#include "ProjectiveCamera.hpp"

namespace BnZ {

ProjectiveCamera::ProjectiveCamera(const Mat4f& viewMatrix,
                 float fovy, float resX, float resY,
                 float zNear, float zFar):
    Camera(viewMatrix),
    m_ProjMatrix(perspective(fovy, resX / resY, zNear, zFar)),
    m_RcpProjMatrix(inverse(m_ProjMatrix)),
    m_fFovY(fovy),
    m_fResX(resX),
    m_fResY(resY),
    m_fZFar(zFar) {
    m_fFocalDist = m_fResY / (2.f * tan(0.5f * fovy));
}

Ray ProjectiveCamera::getRay(const Vec2f& ndcPosition) const {
    auto pixelWP = getRcpViewProjMatrix() * Vec4f(ndcPosition, 0.f, 1.f);
    pixelWP /= pixelWP.w;
    auto org = getPosition();
    return Ray(org, normalize(Vec3f(pixelWP) - org));
}

bool ProjectiveCamera::getNDC(const Vec3f& P, Vec2f& ndc) const {
    auto P_clip = getViewProjMatrix() * Vec4f(P, 1.f);
    auto P_ndc = P_clip.xyz() / P_clip.w;
    if(abs(P_ndc).x > 1.f || abs(P_ndc.y) > 1.f || abs(P_ndc.z) > 1.f) {
        return false;
    }
    ndc = P_ndc.xy();

    return true;
}

float ProjectiveCamera::surfaceToImageFactor(const SurfacePoint& point,
                           Ray& shadowRay) const {
    auto directionToCamera = getPosition() - point.P;
    auto dist = length(directionToCamera);
    if(dist == 0.f) {
        return 0.f;
    }
    directionToCamera /= dist;
    const float cosToCamera = abs(dot(directionToCamera, point.Ns));
    const float cosAtCamera = dot(getFrontVector(), -directionToCamera);
    if(cosAtCamera <= 0.f) {
        return 0.f;
    }
    const float imagePointToCameraDist = m_fFocalDist / cosAtCamera;
    const float imageToSolidAngleFactor = sqr(imagePointToCameraDist) / cosAtCamera;

    shadowRay = Ray(point, directionToCamera, dist);
    return sqr(dist) / (imageToSolidAngleFactor * cosToCamera);
}


}
