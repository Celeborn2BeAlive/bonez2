#pragma once

#include "Camera.hpp"

namespace BnZ {

class ParaboloidCamera: public Camera {
public:
    ParaboloidCamera() = default;

    ParaboloidCamera(const Mat4f& viewMatrix): Camera(viewMatrix) {
    }

    Ray getRay(const Vec2f& ndc) const override {
        if(dot(ndc, ndc) > 1.f) {
            // Return a bad ray (tnear = tfar)
            return Ray(Vec3f(0.f), Vec3f(0.f), 0.f, 0.f);
        }

        // N is the normal vector of the point (x, y, z) of the paraboloid
        Vec3f N = Vec3f(ndc.x, ndc.y, -1.f);
        float scale = 2.f / dot(N, N);
        // We want D such that -scale * N = D + (0,0,-1) (D is the reflection or (0,0,1) wrt. N)
        Vec3f D = scale * N - Vec3f(0.f, 0.f, -1.f);

        return Ray(getPosition(), Vec3f(getRcpViewMatrix() * Vec4f(D, 0.f)));
    }

    bool getNDC(const Vec3f& P, Vec2f& ndc) const override {
        auto globalDir = normalize(P - getPosition());
        auto localDir = Vec3f(getViewMatrix() * Vec4f(globalDir, 0.f));

        if(localDir.z >= 0.f) {
            return false;
        }

        auto scaledN = localDir + Vec3f(0, 0, -1);
        auto N = -scaledN / scaledN.z;
        ndc = N.xy();

        return true;
    }

    float surfaceToImageFactor(const SurfacePoint& point,
                               Ray& shadowRay) const override {
        return 0.f;
    }
};

class DualParaboloidCamera: public Camera {
public:
    DualParaboloidCamera() = default;

    DualParaboloidCamera(const Mat4f& viewMatrix): Camera(viewMatrix) {
    }

    virtual Ray getRay(const Vec2f& ndc) const {
        auto uv = ndcToUV(ndc);

        auto N = getDualParaboloidNormal(uv);
        auto dualNDC = N.xy();

        if(dot(dualNDC, dualNDC) > 1.f) {
            return Ray(Vec3f(0.f), Vec3f(0.f), 0.f, 0.f);
        }

        return Ray(getPosition(), Vec3f(getRcpViewMatrix() * Vec4f(dualParaboloidMapping(uv), 0.f)));
    }

    bool getNDC(const Vec3f& P, Vec2f& ndc) const override {
        auto globalDir = normalize(P - getPosition());
        auto localDir = Vec3f(getViewMatrix() * Vec4f(globalDir, 0.f));

        auto uv = rcpDualParaboloidMapping(localDir);
        ndc = uvToNDC(uv);

        return true;
    }

    float surfaceToImageFactor(const SurfacePoint& point,
                               Ray& shadowRay) const override {
        return 0.f;
    }
};

}
