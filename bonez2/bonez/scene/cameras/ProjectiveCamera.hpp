#pragma once

#include <bonez/common.hpp>
#include "Camera.hpp"
#include <bonez/scene/Ray.hpp>

namespace BnZ {

class ProjectiveCamera: public Camera {
public:
    ProjectiveCamera() = default;

    ProjectiveCamera(const Mat4f& viewMatrix,
                     float fovy, float resX, float resY,
                     float zNear, float zFar);

    const Mat4f& getProjMatrix() const {
        return m_ProjMatrix;
    }

    const Mat4f& getRcpProjMatrix() const {
        return m_RcpProjMatrix;
    }

    Mat4f getViewProjMatrix() const {
        return m_ViewProjMatrix;
    }

    Mat4f getRcpViewProjMatrix() const {
        return m_RcpViewProjMatrix;
    }

    float getFovY() const {
        return m_fFovY;
    }

    Ray getRay(const Vec2f& ndcPosition) const override;

    bool getNDC(const Vec3f& P, Vec2f& ndc) const override;

    float getZFar() const {
        return m_fZFar;
    }

    float getFocalDistance() const {
        return m_fFocalDist;
    }

    float surfaceToImageFactor(const SurfacePoint& point,
                               Ray& shadowRay) const override;

private:
    Mat4f m_ProjMatrix = Mat4f(1.f);
    Mat4f m_RcpProjMatrix = Mat4f(1.f);
    float m_fFovY = 0.f;
    float m_fResX = 0.f, m_fResY = 0.f;
    float m_fZNear = 0.f;
    float m_fZFar = 0.f;
    float m_fFocalDist = 0.f;
    Mat4f m_ViewProjMatrix = getProjMatrix() * getViewMatrix();
    Mat4f m_RcpViewProjMatrix = inverse(m_ViewProjMatrix);
};

}
