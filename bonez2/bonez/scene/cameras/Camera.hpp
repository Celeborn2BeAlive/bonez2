#pragma once

#include <bonez/common.hpp>
#include <bonez/maths/maths.hpp>
#include <bonez/scene/Ray.hpp>

namespace BnZ {

class SurfacePoint;

class Camera {
public:
    virtual ~Camera() {

    }

    Camera() = default;

    Camera(const Mat4f& viewMatrix):
        m_ViewMatrix(viewMatrix), m_RcpViewMatrix(inverse(viewMatrix)) {
    }

    Camera(const Vec3f& eye, const Vec3f& point, const Vec3f& up):
        Camera(lookAt(eye, point, up)) {
    }

    Vec3f getPosition() const {
        return Vec3f(m_RcpViewMatrix[3]);
    }

    Vec3f getFrontVector() const {
        return -Vec3f(m_RcpViewMatrix[2]);
    }

    Vec3f getLeftVector() const {
        return -Vec3f(m_RcpViewMatrix[0]);
    }

    Vec3f getUpVector() const {
        return Vec3f(m_RcpViewMatrix[1]);
    }

    const Mat4f& getViewMatrix() const {
        return m_ViewMatrix;
    }

    const Mat4f& getRcpViewMatrix() const {
        return m_RcpViewMatrix;
    }

    virtual Ray getRay(const Vec2f& ndcPosition) const = 0;

    // Project a point and returns its NDC
    virtual bool getNDC(const Vec3f& P, Vec2f& ndc) const = 0;

    // Compute the factor to convert a probability density expressed wrt. area
    // to a pdf expressed wrt. image plane of the camera
    // Return in shadowRay a ray to test for occlusion
    virtual float surfaceToImageFactor(const SurfacePoint& point,
                                       Ray& shadowRay) const = 0;


private:
    Mat4f m_ViewMatrix = Mat4f(1.f);
    Mat4f m_RcpViewMatrix = Mat4f(1.f);
};

}
