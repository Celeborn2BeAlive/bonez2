#include "SceneGeometry.hpp"

#include <fstream>
#include <unordered_set>
#include <unordered_map>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "bonez/common.hpp"
#include "bonez/sys/FileSystem.hpp"
#include "bonez/maths/maths.hpp"


#include <bonez/parsing/parsing.hpp>

namespace BnZ {

static const aiVector3D aiZERO(0.f, 0.f, 0.f);

static void loadMaterial(const aiMaterial* aimaterial, const FilePath& basePath, SceneGeometry& geometry) {
    aiColor3D color;

    aiString ainame;
    aimaterial->Get(AI_MATKEY_NAME, ainame);
    std::string name = ainame.C_Str();
    std::clog << "Load material " << name << std::endl;

    Material material(name);

    if (AI_SUCCESS == aimaterial->Get(AI_MATKEY_COLOR_DIFFUSE, color)) {
        material.m_DiffuseReflectance = Vec3f(color.r, color.g, color.b);
    }

    aiString path;

    if (AI_SUCCESS == aimaterial->GetTexture(aiTextureType_DIFFUSE, 0, &path,
                                                nullptr, nullptr, nullptr, nullptr, nullptr)) {
        std::clog << "Loading texture " << (basePath + path.data) << std::endl;
        material.m_DiffuseReflectanceTexture = loadImage(basePath + path.data, true);
    }

    if (AI_SUCCESS == aimaterial->Get(AI_MATKEY_COLOR_SPECULAR, color)) {
        material.m_GlossyReflectance = Vec3f(color.r, color.g, color.b);
    }

    if (AI_SUCCESS == aimaterial->GetTexture(aiTextureType_SPECULAR, 0, &path,
                                              nullptr, nullptr, nullptr, nullptr, nullptr)) {
        std::clog << "Loading texture " << (basePath + path.data) << std::endl;
        material.m_GlossyReflectanceTexture = loadImage(basePath + path.data, true);
    }

    aimaterial->Get(AI_MATKEY_SHININESS, material.m_Shininess);

    if (AI_SUCCESS == aimaterial->GetTexture(aiTextureType_SHININESS, 0, &path,
                                               nullptr, nullptr, nullptr, nullptr, nullptr)) {
        std::clog << "Loading texture " << (basePath + path.data) << std::endl;
        material.m_ShininessTexture = loadImage(basePath + path.data, true);
    }

    geometry.addMaterial(std::move(material));
}

static void loadMesh(const aiMesh* aimesh, uint32_t materialOffset, SceneGeometry& geometry) {
    TriangleMesh mesh;

#ifdef _DEBUG
    mesh.m_MaterialID = 0;
#else
    mesh.m_MaterialID = materialOffset + aimesh->mMaterialIndex;
#endif

    mesh.m_Vertices.reserve(aimesh->mNumVertices);
    for (size_t vertexIdx = 0; vertexIdx < aimesh->mNumVertices; ++vertexIdx) {
        const aiVector3D* pPosition = &aimesh->mVertices[vertexIdx];
        const aiVector3D* pNormal = &aimesh->mNormals[vertexIdx];
        const aiVector3D* pTexCoords = aimesh->HasTextureCoords(0) ? &aimesh->mTextureCoords[0][vertexIdx] : &aiZERO;
        mesh.m_Vertices.emplace_back(
                    Vec3f(pPosition->x, pPosition->y, pPosition->z),
                    Vec3f(pNormal->x, pNormal->y, pNormal->z),
                    Vec2f(pTexCoords->x, pTexCoords->y));

        if(vertexIdx == 0) {
            mesh.m_BBox = BBox3f(Vec3f(mesh.m_Vertices.back().position));
        } else {
            mesh.m_BBox.grow(Vec3f(mesh.m_Vertices.back().position));
        }
    }

    mesh.m_Triangles.reserve(aimesh->mNumFaces);
    for (size_t triangleIdx = 0; triangleIdx < aimesh->mNumFaces; ++triangleIdx) {
        const aiFace& face = aimesh->mFaces[triangleIdx];
        mesh.m_Triangles.emplace_back(face.mIndices[0], face.mIndices[1], face.mIndices[2]);
    }

    /*
    if(geometry.m_TriangleMeshs.empty()) {
        geometry.m_BBox = mesh.m_BBox;
    }  else {
        geometry.m_BBox.grow(mesh.m_BBox);
    }

    geometry.m_TriangleMeshs.emplace_back(std::move(mesh));*/
    geometry.append(mesh);
}

void loadAssimpScene(const aiScene* aiscene, const std::string& filepath, SceneGeometry& geometry) {
    auto materialOffset = geometry.getMaterialCount();

    FilePath path(filepath);

#ifdef _DEBUG
    //geometry.m_Materials.reserve(1);
    geometry.m_Materials.emplace_back(Material());
#else
    //geometry.m_Materials.reserve(materialOffset + aiscene->mNumMaterials);
    for (size_t materialIdx = 0; materialIdx < aiscene->mNumMaterials; ++materialIdx) {
        loadMaterial(aiscene->mMaterials[materialIdx], path.path(), geometry);
    }
#endif

    //geometry.m_TriangleMeshs.reserve(geometry.m_TriangleMeshs.size() + aiscene->mNumMeshes);
    for (size_t meshIdx = 0; meshIdx < aiscene->mNumMeshes; ++meshIdx) {
        loadMesh(aiscene->mMeshes[meshIdx], materialOffset, geometry);
    }
}

SceneGeometry loadModel(const std::string& filepath) {
#ifndef _WIN32
//    if (exists(filepath + ".bnzmodel")) {
//        return loadBnZModel(filepath + ".bnzmodel");
//    }
#endif

    Assimp::Importer importer;
    std::clog << "Loading geometry of " << filepath << std::endl;
    const aiScene* aiscene = importer.ReadFile(filepath.c_str(), aiProcess_Triangulate | aiProcess_GenNormals | aiProcess_FlipUVs);

    if (aiscene) {
        try {
            SceneGeometry geometry;
            loadAssimpScene(aiscene, filepath, geometry);

//            if (!exists(filepath + ".bnzmodel")) {
//                storeBnZModel(filepath + ".bnzmodel", geometry);
//            }

            return geometry;
        }
        catch (const std::runtime_error& e) {
            throw std::runtime_error("Assimp loading error on file " + filepath + ": " + e.what());
        }
    }
    else {
        throw std::runtime_error("Assimp loading error on file " + filepath + ": " + importer.GetErrorString());
    }
}

void SceneGeometry::postIntersect(const Ray& ray, Intersection& I) const {
    float u = I.uv.x;
    float v = I.uv.y;
    float w = 1.f - u - v;

    const auto& mesh = m_TriangleMeshs[I.meshID];
    const auto& triangle = mesh.m_Triangles[I.triangleID];

    const auto& v0 = mesh.m_Vertices[triangle.v0];
    const auto& v1 = mesh.m_Vertices[triangle.v1];
    const auto& v2 = mesh.m_Vertices[triangle.v2];

    I.Ns = normalize(w * v0.normal + u * v1.normal + v * v2.normal);

    if(glm::dot(-ray.dir, I.Ns) > 0.f) {
        I.Le = m_Materials[mesh.m_MaterialID].m_EmittedRadiance;
        if(m_Materials[mesh.m_MaterialID].m_EmittedRadianceTexture) {
            I.Le *= texture(*m_Materials[mesh.m_MaterialID].m_EmittedRadianceTexture, I.texCoords).xyz();
        }
    }

    //faceForward(-ray.dir, I.Ns);

    I.texCoords = w * v0.texCoords + u * v1.texCoords + v * v2.texCoords;
}

void SceneGeometry::getSurfacePoint(uint32_t meshID, uint32_t triangleID,
                                    float u, float v, SurfacePoint& point) const {
    float w = 1.f - u - v;
    const auto& mesh = m_TriangleMeshs[meshID];
    const auto& triangle = mesh.m_Triangles[triangleID];

    const auto& v0 = mesh.m_Vertices[triangle.v0];
    const auto& v1 = mesh.m_Vertices[triangle.v1];
    const auto& v2 = mesh.m_Vertices[triangle.v2];

    auto e0 = v1.position - v0.position;
    auto e1 = v2.position - v0.position;

    point.meshID = meshID;
    point.triangleID = triangleID;
    point.P = w * v0.position + u * v1.position + v * v2.position;
    point.Ng = normalize(cross(e0, e1));
    point.uv = Vec2f(u, v);
    point.Ns = normalize(w * v0.normal + u * v1.normal + v * v2.normal);
    faceForward(point.Ns, point.Ng);
    point.texCoords = w * v0.texCoords + u * v1.texCoords + v * v2.texCoords;
}

//void storeMesh(std::ofstream& out, const TriangleMesh& mesh) {
//    auto vertexCount = mesh.m_Vertices.size();
//    //std::clog << "write " << vertexCount << " vertices " << std::endl;
//    out.write((const char*) &vertexCount, sizeof(vertexCount));
//    out.write((const char*)mesh.m_Vertices.data(), sizeof(mesh.m_Vertices[0]) * vertexCount);
//    auto triangleCount = mesh.m_Triangles.size();
//    //std::clog << "write " << triangleCount << " triangles " << std::endl;
//    out.write((const char*)&triangleCount, sizeof(triangleCount));
//    out.write((const char*)mesh.m_Triangles.data(), sizeof(mesh.m_Triangles[0]) * triangleCount);
//    out.write((const char*)&mesh.m_MaterialID, sizeof(mesh.m_MaterialID));
//    out.write((const char*)&mesh.m_BBox, sizeof(mesh.m_BBox));
//}

//void storeMaterial(std::ofstream& out, const Material& material) {
//    out.write((const char*)&material.m_DiffuseReflectance, sizeof(material.m_DiffuseReflectance));
//    out.write((const char*)&material.m_GlossyReflectance, sizeof(material.m_GlossyReflectance));
//    out.write((const char*)&material.m_Shininess, sizeof(material.m_Shininess));
//    auto ptr = material.m_DiffuseReflectanceTexture.get();
//    //std::clog << "write texture address " << ptr << std::endl;
//    out.write((const char*)&ptr, sizeof(ptr));
//    ptr = material.m_GlossyReflectanceTexture.get();
//    //std::clog << "write texture address " << ptr << std::endl;
//    out.write((const char*)&ptr, sizeof(ptr));
//    ptr = material.m_ShininessTexture.get();
//    //std::clog << "write texture address " << ptr << std::endl;
//    out.write((const char*)&ptr, sizeof(ptr));
//}

//void storeImage(std::ofstream& out, const Image* pImage) {
//    //std::clog << "write image " << pImage << std::endl;
//    out.write((const char*)&pImage, sizeof(pImage));
//    auto size = pImage->getSize();
//    out.write((const char*)&size, sizeof(size));
//    auto ptr = pImage->getPixels();
//    out.write((const char*)ptr, sizeof(ptr[0]) * size.x * size.y);
//}

//void storeBnZModel(const std::string& filepath, const SceneGeometry& geometry) {
//    std::ofstream out(filepath, std::ios::binary);

//    std::clog << "Store bbox..." << std::endl;
//    out.write((const char*)&geometry.getBBox(), sizeof(geometry.getBBox()));

//    std::clog << "Store geometry..." << std::endl;
//    auto meshCount = geometry.getMeshCount();
//    out.write((const char*)&meshCount, sizeof(meshCount));
//    std::clog << "write " << meshCount << " meshes" << std::endl;
//    for (auto i = 0u; i < meshCount; ++i) {
//        storeMesh(out, geometry.getMesh(i));
//    }
//    std::unordered_set<const Image*> imageSet;
//    imageSet.insert(nullptr);
//    for (const auto& material : geometry.getMaterials()) {
//        imageSet.insert(material.m_DiffuseReflectanceTexture.get());
//        imageSet.insert(material.m_GlossyReflectanceTexture.get());
//        imageSet.insert(material.m_ShininessTexture.get());
//    }
//    std::clog << "Store images..." << std::endl;
//    auto imageCount = imageSet.size() - 1; // Minus nullptr
//    std::clog << "write " << imageCount << " images" << std::endl;
//    out.write((const char*)&imageCount, sizeof(imageCount));
//    for (auto pImage : imageSet) {
//        if (pImage) {
//            storeImage(out, pImage);
//        }
//    }

//    std::clog << "Store materials..." << std::endl;
//    auto materialCount = geometry.getMaterialCount();
//    std::clog << "write " << materialCount << " materials" << std::endl;
//    out.write((const char*)&materialCount, sizeof(materialCount));
//    for (const auto& material : geometry.getMaterials()) {
//        storeMaterial(out, material);
//    }
//}

//void loadBnZMesh(std::ifstream& in, TriangleMesh& mesh) {
//    decltype(mesh.m_Vertices.size()) vertexCount;
//    in.read((char*)&vertexCount, sizeof(vertexCount));
//    //std::clog << "read " << vertexCount << " vertices " << std::endl;
//    mesh.m_Vertices.resize(vertexCount);
//    in.read((char*)mesh.m_Vertices.data(), sizeof(mesh.m_Vertices[0]) * vertexCount);
//    decltype(mesh.m_Triangles.size()) triangleCount;
//    in.read((char*)&triangleCount, sizeof(triangleCount));
//    //std::clog << "read " << triangleCount << " triangles " << std::endl;
//    mesh.m_Triangles.resize(triangleCount);
//    in.read((char*)mesh.m_Triangles.data(), sizeof(mesh.m_Triangles[0]) * triangleCount);
//    in.read((char*)&mesh.m_MaterialID, sizeof(mesh.m_MaterialID));
//    in.read((char*)&mesh.m_BBox, sizeof(mesh.m_BBox));
//}

//Shared<Image> loadBnZImage(std::ifstream& in, const Image*& oldPtr) {
//    in.read((char*)&oldPtr, sizeof(oldPtr));
//    auto pImage = makeShared<Image>();
//    decltype(pImage->getSize()) size;
//    in.read((char*)&size, sizeof(size));
//    pImage->setSize(size.x, size.y);
//    auto ptr = pImage->getPixels();
//    in.read((char*)ptr, sizeof(ptr[0]) * size.x * size.y);

//    return pImage;
//}

//void loadBnZMaterial(std::ifstream& in, Material& material, const std::unordered_map<const Image*, Shared<Image>>& imageMap) {
//    in.read((char*)&material.m_DiffuseReflectance, sizeof(material.m_DiffuseReflectance));
//    in.read((char*)&material.m_GlossyReflectance, sizeof(material.m_GlossyReflectance));
//    in.read((char*)&material.m_Shininess, sizeof(material.m_Shininess));

//    const Image* ptr = nullptr;

//    in.read((char*)&ptr, sizeof(ptr));
//    //std::clog << "read texture address " << ptr << std::endl;
//    if (ptr) {
//        auto it = imageMap.find(ptr);
//        if(it == std::end(imageMap)) {
//            throw std::runtime_error("loadBnZMaterial: unable to find texture " + toString(ptr) + " in map");
//        }
//        material.m_DiffuseReflectanceTexture = (*it).second;
//    }

//    in.read((char*)&ptr, sizeof(ptr));
//    //std::clog << "read texture address " << ptr << std::endl;
//    if (ptr) {
//        auto it = imageMap.find(ptr);
//        if(it == std::end(imageMap)) {
//            throw std::runtime_error("loadBnZMaterial: unable to find texture " + toString(ptr) + " in map");
//        }
//        material.m_GlossyReflectanceTexture = (*it).second;
//    }

//    in.read((char*)&ptr, sizeof(ptr));
//    //std::clog << "read texture address " << ptr << std::endl;
//    if (ptr) {
//        auto it = imageMap.find(ptr);
//        if(it == std::end(imageMap)) {
//            throw std::runtime_error("loadBnZMaterial: unable to find texture " + toString(ptr) + " in map");
//        }
//        material.m_ShininessTexture = (*it).second;
//    }
//}

//SceneGeometry loadBnZModel(const std::string& filepath) {
//    SceneGeometry geometry;

//    std::clog << "Load BnZ Model..." << std::endl;

//    std::ifstream in(filepath);
//    if (!in) {
//        throw std::runtime_error("Unable to open " + filepath);
//    }

//    std::clog << "Load bbox..." << std::endl;
//    in.read((char*)&geometry.getBBox(), sizeof(geometry.getBBox()));

//    std::clog << "Load geometry..." << std::endl;
//    decltype(geometry.getMeshCount()) meshCount;
//    in.read((char*)&meshCount, sizeof(meshCount));
//    std::clog << "read " << meshCount << " meshes" << std::endl;
//    auto triangleCount = 0u;
//    for (auto i = 0u; i < meshCount; ++i) {
//        //geometry.m_TriangleMeshs.emplace_back();
//        TriangleMesh mesh;
//        loadBnZMesh(in, mesh);
//        geometry.append(mesh);
//        triangleCount += mesh.getTriangleCount();
//    }
//    std::clog << "Number of triangles = " << triangleCount << std::endl;

//    std::clog << "Load images..." << std::endl;
//    std::unordered_set<const Image*> imageSet;
//    std::unordered_map<const Image*, Shared<Image>> imageMap;
//    decltype(imageSet.size()) imageCount;
//    in.read((char*)&imageCount, sizeof(imageCount));
//    std::clog << "read " << imageCount << " images" << std::endl;
//    for (auto i = 0u; i < imageCount; ++i) {
//        const Image* oldPtr;
//        auto pImage = loadBnZImage(in, oldPtr);
//        //std::clog << "Image " << oldPtr << " map to " << pImage.get() << std::endl;
//        imageMap[oldPtr] = pImage;
//    }

//    std::clog << "Load materials..." << std::endl;
//    decltype(geometry.getMaterialCount()) materialCount;
//    in.read((char*)&materialCount, sizeof(materialCount));
//    std::clog << "read " << materialCount << " materials" << std::endl;
//    for (auto i = 0u; i < materialCount; ++i) {
//        //geometry.m_Materials.emplace_back();
//        Material material;
//        loadBnZMaterial(in, material, imageMap);
//        geometry.addMaterial(material);
//    }

//    return geometry;
//}

static void updateMaterial(const FilePath& path, const tinyxml2::XMLElement& materialDescription,
                           Material& material) {
    auto pDiffuse = materialDescription.FirstChildElement("Diffuse");
    if(pDiffuse) {
        material.m_DiffuseReflectance = loadColor(*pDiffuse, zero<Vec3f>());
        std::string texturePath;
        if(getAttribute(*pDiffuse, "textureDiffuse", texturePath)) {
            material.m_DiffuseReflectanceTexture = loadImage(path + texturePath);
        }
    }

    auto pGlossy = materialDescription.FirstChildElement("Glossy");
    if(pGlossy) {
        material.m_GlossyReflectance = loadColor(*pGlossy, zero<Vec3f>());
        if(!getAttribute(*pGlossy, "shininess", material.m_Shininess)) {
            material.m_Shininess = 0.f;
        }
        std::string texturePath;
        if(getAttribute(*pGlossy, "textureGlossy", texturePath)) {
            material.m_GlossyReflectanceTexture = loadImage(path + texturePath);
        }
        if(getAttribute(*pGlossy, "textureShininess", texturePath)) {
            material.m_ShininessTexture = loadImage(path + texturePath);
        }
    }

    auto pEmission = materialDescription.FirstChildElement("Emission");
    if(pEmission) {
        material.m_EmittedRadiance = loadColor(*pEmission, zero<Vec3f>());
        std::string texturePath;
        if(getAttribute(*pEmission, "textureEmission", texturePath)) {
            material.m_EmittedRadianceTexture = loadImage(path + texturePath);
        }
    }

    auto pSpecular = materialDescription.FirstChildElement("Specular");
    if(pSpecular) {
        getAttribute(*pSpecular, "indexOfRefraction", material.m_fIndexOfRefraction);
        std::string texturePath;
        if(getAttribute(*pSpecular, "textureIndexOfRefraction", texturePath)) {
            material.m_IndexOfRefractionTexture = loadImage(path + texturePath);
        }

        auto pReflection = pSpecular->FirstChildElement("Reflection");

        if(pReflection) {
            material.m_SpecularReflectance = loadColor(*pReflection, zero<Vec3f>());
            if(getAttribute(*pReflection, "textureReflection", texturePath)) {
                material.m_SpecularReflectanceTexture = loadImage(path + texturePath);
            }

            getAttribute(*pSpecular, "absorption", material.m_fSpecularAbsorption);
            if(getAttribute(*pSpecular, "textureAbsorption", texturePath)) {
                material.m_SpecularAbsorptionTexture = loadImage(path + texturePath);
            }
        }

        auto pTransmission = pSpecular->FirstChildElement("Transmission");

        if(pTransmission) {
            material.m_SpecularTransmittance = loadColor(*pTransmission, zero<Vec3f>());
            if(getAttribute(*pTransmission, "textureTransmission", texturePath)) {
                material.m_SpecularTransmittanceTexture = loadImage(path + texturePath);
            }
        }
    }
}

static Material loadMaterial(const FilePath& path, const tinyxml2::XMLElement& materialDescription) {
    std::string name;
    if(!getAttribute(materialDescription, "name", name)) {
        name = getUniqueName();
    }

    Material material(name);
    updateMaterial(path, materialDescription, material);

    return material;
}

SceneGeometry loadMesh(const FilePath& path, const tinyxml2::XMLElement& meshDescription) {
    SceneGeometry geometry;
    auto materialID = geometry.getMaterialCount();

    auto pMaterial = meshDescription.FirstChildElement("Material");
    if(!pMaterial) {
        geometry.addMaterial(Material(getUniqueName()));
    } else {
        geometry.m_Materials.emplace_back(loadMaterial(path, *pMaterial));
    }

    auto pVertexList = meshDescription.FirstChildElement("VertexList");
    if(!pVertexList) {
        throw std::runtime_error("No VertexList in a mesh description.");
    }

    auto pTriangleList = meshDescription.FirstChildElement("TriangleList");
    if(!pTriangleList) {
        throw std::runtime_error("No TriangleList in a mesh description.");
    }

    TriangleMesh mesh;

    mesh.m_MaterialID = materialID;

    auto pVertex = pVertexList->FirstChildElement("Vertex");
    auto vertexIdx = 0u;
    while(pVertex) {
        Vec3f position(0.f), normal(0.f);
        Vec2f texCoords(0.f);

        getAttribute(*pVertex, "x", position.x);
        getAttribute(*pVertex, "y", position.y);
        getAttribute(*pVertex, "z", position.z);
        getAttribute(*pVertex, "nx", normal.x);
        getAttribute(*pVertex, "ny", normal.y);
        getAttribute(*pVertex, "nz", normal.z);
        getAttribute(*pVertex, "u", texCoords.x);
        getAttribute(*pVertex, "v", texCoords.y);

        mesh.m_Vertices.emplace_back(
                    position,
                    normal,
                    texCoords);

        if(vertexIdx == 0) {
            mesh.m_BBox = BBox3f(Vec3f(mesh.m_Vertices.back().position));
        } else {
            mesh.m_BBox.grow(Vec3f(mesh.m_Vertices.back().position));
        }
        ++vertexIdx;

        pVertex = pVertex->NextSiblingElement("Vertex");
    }

    auto pTriangle = pTriangleList->FirstChildElement("Triangle");
    while(pTriangle) {
        uint32_t v0, v1, v2;
        getAttribute(*pTriangle, "a", v0);
        getAttribute(*pTriangle, "b", v1);
        getAttribute(*pTriangle, "c", v2);
        if(v0 >= vertexIdx || v1 >= vertexIdx || v2 >= vertexIdx) {
            throw std::runtime_error("Vertex index out of range");
        }
        mesh.m_Triangles.emplace_back(v0, v1, v2);

        pTriangle = pTriangle->NextSiblingElement("Triangle");
    }

    geometry.append(mesh);

    return geometry;
}

static TriangleMesh buildSphere(const Vec3f& center, float r, uint32_t discLat, uint32_t discLong) {
    // Equation paramétrique en (r, phi, theta) de la sphère
    // avec r >= 0, -PI / 2 <= theta <= PI / 2, 0 <= phi <= 2PI
    //
    // x(r, phi, theta) = r sin(phi) cos(theta)
    // y(r, phi, theta) = r sin(theta)
    // z(r, phi, theta) = r cos(phi) cos(theta)
    //
    // Discrétisation:
    // dPhi = 2PI / discLat, dTheta = PI / discLong
    //
    // x(r, i, j) = r * sin(i * dPhi) * cos(-PI / 2 + j * dTheta)
    // y(r, i, j) = r * sin(-PI / 2 + j * dTheta)
    // z(r, i, j) = r * cos(i * dPhi) * cos(-PI / 2 + j * dTheta)

    float rcpLat = 1.f / discLat, rcpLong = 1.f / discLong;
    float dPhi = 2 * pi<float>() * rcpLat, dTheta = pi<float>() * rcpLong;

    TriangleMesh mesh;

    // Construit l'ensemble des vertex
    for(uint32_t j = 0; j <= discLong; ++j) {
        float cosTheta = cos(-pi<float>() * 0.5f + j * dTheta);
        float sinTheta = sin(-pi<float>() * 0.5f + j * dTheta);

        for(uint32_t i = 0; i <= discLat; ++i) {
            TriangleMesh::Vertex vertex;

            vertex.texCoords.x = i * rcpLat;
            vertex.texCoords.y = 1.f - j * rcpLong;

            vertex.normal.x = sin(i * dPhi) * cosTheta;
            vertex.normal.y = sinTheta;
            vertex.normal.z = cos(i * dPhi) * cosTheta;

            vertex.position = center + r * vertex.normal;

            mesh.m_Vertices.emplace_back(vertex);

            if(mesh.m_Vertices.size() == 1u) {
                mesh.m_BBox = BBox3f(Vec3f(mesh.m_Vertices.back().position));
            } else {
                mesh.m_BBox.grow(Vec3f(mesh.m_Vertices.back().position));
            }
        }
    }

    // Construit les vertex finaux en regroupant les données en triangles:
    // Pour une longitude donnée, les deux triangles formant une face sont de la forme:
    // (i, i + 1, i + discLat + 1), (i, i + discLat + 1, i + discLat)
    // avec i sur la bande correspondant à la longitude
    for(uint32_t j = 0; j < discLong; ++j) {
        uint32_t offset = j * (discLat + 1);
        for(uint32_t i = 0; i < discLat; ++i) {
            mesh.m_Triangles.emplace_back(offset + i, offset + (i + 1), offset + discLat + 1 + (i + 1));
            mesh.m_Triangles.emplace_back(offset + i, offset + discLat + 1 + (i + 1), offset + i + discLat + 1);
        }
    }

    return mesh;
}

static TriangleMesh buildQuad() {
    TriangleMesh mesh;

    {
        TriangleMesh::Vertex vertex;

        vertex.texCoords.x = 0;
        vertex.texCoords.y = 0;

        vertex.normal.x = 0;
        vertex.normal.y = -1;
        vertex.normal.z = 0;

        vertex.position = Vec3f(0.25, 1.28002, 0.25);

        mesh.m_Vertices.emplace_back(vertex);

        if(mesh.m_Vertices.size() == 1u) {
            mesh.m_BBox = BBox3f(Vec3f(mesh.m_Vertices.back().position));
        } else {
            mesh.m_BBox.grow(Vec3f(mesh.m_Vertices.back().position));
        }
    }

    {
        TriangleMesh::Vertex vertex;

        vertex.texCoords.x = 1;
        vertex.texCoords.y = 0;

        vertex.normal.x = 0;
        vertex.normal.y = -1;
        vertex.normal.z = 0;

        vertex.position = Vec3f(0.25, 1.28002, -0.25);

        mesh.m_Vertices.emplace_back(vertex);

        if(mesh.m_Vertices.size() == 1u) {
            mesh.m_BBox = BBox3f(Vec3f(mesh.m_Vertices.back().position));
        } else {
            mesh.m_BBox.grow(Vec3f(mesh.m_Vertices.back().position));
        }
    }

    {
        TriangleMesh::Vertex vertex;

        vertex.texCoords.x = 1;
        vertex.texCoords.y = 1;

        vertex.normal.x = 0;
        vertex.normal.y = -1;
        vertex.normal.z = 0;

        vertex.position = Vec3f(-0.25, 1.28002, -0.25);

        mesh.m_Vertices.emplace_back(vertex);

        if(mesh.m_Vertices.size() == 1u) {
            mesh.m_BBox = BBox3f(Vec3f(mesh.m_Vertices.back().position));
        } else {
            mesh.m_BBox.grow(Vec3f(mesh.m_Vertices.back().position));
        }
    }

    {
        TriangleMesh::Vertex vertex;

        vertex.texCoords.x = 0;
        vertex.texCoords.y = 1;

        vertex.normal.x = 0;
        vertex.normal.y = -1;
        vertex.normal.z = 0;

        vertex.position = Vec3f(-0.25, 1.28002, 0.25);

        mesh.m_Vertices.emplace_back(vertex);

        if(mesh.m_Vertices.size() == 1u) {
            mesh.m_BBox = BBox3f(Vec3f(mesh.m_Vertices.back().position));
        } else {
            mesh.m_BBox.grow(Vec3f(mesh.m_Vertices.back().position));
        }
    }

    mesh.m_Triangles.emplace_back(2, 1, 0);
    mesh.m_Triangles.emplace_back(2, 0, 3);

    return mesh;
}

SceneGeometry loadQuad(const FilePath& path, const tinyxml2::XMLElement& quadDescription) {
    SceneGeometry geometry;
    auto materialID = geometry.getMaterialCount();

    auto pMaterial = quadDescription.FirstChildElement("Material");
    if(!pMaterial) {
        geometry.addMaterial(Material(getUniqueName()));
    } else {
        geometry.m_Materials.emplace_back(loadMaterial(path, *pMaterial));
    }

    TriangleMesh mesh = buildQuad();

    mesh.m_MaterialID = materialID;

    geometry.append(mesh);

    return geometry;
}

SceneGeometry loadSphere(const FilePath& path, const tinyxml2::XMLElement& sphereDescription) {
    SceneGeometry geometry;
    auto materialID = geometry.getMaterialCount();

    auto pMaterial = sphereDescription.FirstChildElement("Material");
    if(!pMaterial) {
        geometry.addMaterial(Material(getUniqueName()));
    } else {
        geometry.m_Materials.emplace_back(loadMaterial(path, *pMaterial));
    }

    float radius = 1;
    uint32_t discLat = 8, discLong = 4;
    Vec3f center;

    center = loadVector(sphereDescription);

    if(!getAttribute(sphereDescription, "radius", radius)) {
        std::cerr << "Sphere description error: no radius specified" << std::endl;
    }
    if(!getAttribute(sphereDescription, "discLat", discLat)) {
        std::cerr << "Sphere description error: no discLat specified" << std::endl;
    }
    if(!getAttribute(sphereDescription, "discLong", discLong)) {
        std::cerr << "Sphere description error: no discLong specified" << std::endl;
    }

    TriangleMesh mesh = buildSphere(center, radius, discLat, discLong);

    mesh.m_MaterialID = materialID;

    geometry.append(mesh);

    return geometry;
}

SceneGeometry loadGeometry(const FilePath& path, const tinyxml2::XMLElement& geometryDescription) {
    SceneGeometry geometry;

    auto pModel = geometryDescription.FirstChildElement("Model");
    while(pModel) {
        geometry.append(loadModel(path + pModel->Attribute("path")));
        pModel = pModel->NextSiblingElement("Model");
    }

    auto pMesh = geometryDescription.FirstChildElement("Mesh");
    while(pMesh) {
        geometry.append(loadMesh(path, *pMesh));
        pMesh = pMesh->NextSiblingElement("Mesh");
    }


    for(auto pShape = geometryDescription.FirstChildElement("Shape");
            pShape; pShape = pShape->NextSiblingElement("Shape")) {
        std::string type;
        if(!getAttribute(*pShape, "type", type)) {
            std::cerr << "Error in geometry description: Shape specified without type" << std::endl;
            continue;
        }

        if(type == "sphere") {
            std::clog << "Load a sphere" << std::endl;
            geometry.append(loadSphere(path, *pShape));
        } else if(type == "quad") {
            std::clog << "Load a quad" << std::endl;
            geometry.append(loadQuad(path, *pShape));
        } else {
            std::cerr << "Error in geometry description: unknown shape type '" << type << "'" << std::endl;
            continue;
        }
    }

    auto pMaterialUpdates = geometryDescription.FirstChildElement("MaterialUpdates");
    if(pMaterialUpdates) {
        for(auto pMaterial = pMaterialUpdates->FirstChildElement("Material"); pMaterial; pMaterial = pMaterial->NextSiblingElement("Material")) {
            std::string name;
            if(!getAttribute(*pMaterial, "name", name)) {
                std::cerr << "Error in geometry description: material update with no name provided." << std::endl;
                continue;
            }
            auto materialID = geometry.getMaterialID(name);
            if(materialID < 0) {
                std::cerr << "Error in geometry description: material '" << name << "' not found for update" << std::endl;
                continue;
            }

            auto& material = geometry.getMaterial(materialID);
            updateMaterial(path, *pMaterial, material);
        }
    }

    return geometry;
}

}
