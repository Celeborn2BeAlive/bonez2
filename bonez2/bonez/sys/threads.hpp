#pragma once

#include <vector>
#include <thread>
#include <atomic>
#include <algorithm>
#include <mutex>

namespace BnZ {

class ParallelProcessor {
    uint32_t m_nThreadCount = std::thread::hardware_concurrency();
    std::vector<std::thread> m_Threads;
    std::mutex m_DebugMutex;

    ParallelProcessor(): m_Threads(m_nThreadCount) {
    }

public:
    static ParallelProcessor s_Instance;

    uint32_t getThreadCount() const {
        return m_nThreadCount;
    }

    template<typename TaskFunctor>
    void launchThreads(const TaskFunctor& task, bool debug = false) {
        if(debug) {
            task(0);
        } else {
            for(auto i = 0u; i < m_nThreadCount; ++i) {
                m_Threads[i] = std::thread(task, i);
            }

            for(auto i = 0u; i < m_nThreadCount; ++i) {
                m_Threads[i].join();
            }
        }
    }

    // Lock the debug mutex, use this function in a scope
    friend std::unique_lock<std::mutex> debugLock();
};

inline uint32_t getThreadCount() {
    return ParallelProcessor::s_Instance.getThreadCount();
}

template<typename TaskFunctor>
inline void launchThreads(const TaskFunctor& task,
                          bool debug = false) {
    ParallelProcessor::s_Instance.launchThreads(task, debug);
}

template<typename TaskFunctor>
inline void processTasks(uint32_t taskCount,
                         const TaskFunctor& task,
                         bool debug = false) {
    auto blockSize = std::max(1u, taskCount / getThreadCount());

    std::atomic_uint nextBatch;
    nextBatch.store(0);

    auto batchProcess = [&](uint32_t threadID) {
        while(true) {
            auto batchID = nextBatch++;
            auto taskID = batchID * blockSize;

            if(taskID >= taskCount) {
                break;
            }

            auto end = std::min(taskID + blockSize, taskCount);

            while (taskID < end) {
                task(taskID, threadID);
                ++taskID;
            }
        }
    };

    launchThreads(batchProcess, debug);
}

template<typename TaskFunctor>
inline void processTiles(uint32_t tileWidth, uint32_t tileHeight,
                         uint32_t nbTileX, uint32_t nbTileY,
                         const TaskFunctor& task,
                         bool debug = false) {
    uint32_t totalTileCount = nbTileX * nbTileY;

    std::atomic_uint nextTile;
    nextTile.store(0);

    auto tileProcess = [&](uint32_t threadID) {
        while(true) {
            auto tileID = nextTile++;

            if(tileID >= totalTileCount) {
                return;
            }

            uint32_t tileX = tileID % nbTileX;
            uint32_t tileY = tileID / nbTileX;

            uint32_t x_start = tileX * tileWidth;
            uint32_t y_start = tileY * tileHeight;
            uint32_t x_end = x_start + tileWidth;
            uint32_t y_end = y_start + tileHeight;

            for(auto y = y_start; y < y_end; ++y) {
                for(auto x = x_start; x < x_end; ++x) {
                    task(x, y, threadID);
                }
            }
        }
    };

    launchThreads(tileProcess, debug);
}

inline std::unique_lock<std::mutex> debugLock() {
    return std::unique_lock<std::mutex>(ParallelProcessor::s_Instance.m_DebugMutex);
}

// A Simple class to manipulate statistics computed over multiple
// threads
template<typename T, size_t MaxThreadCount>
struct DistributedStatistic {
    T m_Values[MaxThreadCount];
public:
    void init(const T& value) {
        std::fill(std::begin(m_Values), std::end(m_Values), value);
    }

    const T& operator [](uint32_t threadIndex) const {
        return m_Values[threadIndex];
    }

    T& operator [](uint32_t threadIndex) {
        return m_Values[threadIndex];
    }

    T sum() const {
        T result = m_Values[0];
        for (auto i = 1u; i < MaxThreadCount; ++i) {
            result += m_Values[i];
        }
        return result;
    }
};

}
