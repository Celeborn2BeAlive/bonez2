#pragma once

#include <cstdint>

namespace BnZ {

inline double ns2sec(uint64_t t) {
    return t * 0.000000001;
}

inline double ns2ms(uint64_t t) {
    return t * 0.000001;
}

inline double us2ms(uint64_t t) {
    return t * 0.001;
}

inline double us2sec(uint64_t t) {
    return t * 0.000001;
}

inline double ms2sec(double t) {
    return t * 0.001;
}

uint64_t getMicroseconds();

class Timer {
    uint64_t m_Start;

public:
    Timer(): m_Start(getMicroseconds()) {
    }

    uint64_t getEllapsedTime() const {
        return getMicroseconds() - m_Start;
    }
};

}
