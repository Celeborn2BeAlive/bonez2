#include "time.hpp"
#include <chrono>

namespace BnZ {

uint64_t getMicroseconds() {
    typedef std::chrono::microseconds us;
    return std::chrono::duration_cast<us>(
                std::chrono::high_resolution_clock::now().time_since_epoch()
           ).count();
}

}
