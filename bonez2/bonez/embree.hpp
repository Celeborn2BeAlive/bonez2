#pragma once

#include <cstdint>

typedef uint32_t int32;
#include <embree2/rtcore.h>
#include <embree2/rtcore_ray.h>
