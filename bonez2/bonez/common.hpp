#pragma once

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <iomanip>
#include "sys/time.hpp"
#include "itertools/itertools.hpp"

namespace BnZ {

template<typename T>
inline std::string toString6(T&& value) {
    std::stringstream ss;
    ss << std::setfill('0');
    ss << std::setw(6) << value;
    return ss.str();
}

template<typename T>
inline std::string toString3(T&& value) {
    std::stringstream ss;
    ss << std::setfill('0');
    ss << std::setw(3) << value;
    return ss.str();
}

template<typename T>
inline std::string toString(T&& value) {
    std::stringstream ss;
    ss << value;
    return ss.str();
}

inline std::string getUniqueName() {
    return toString(getMicroseconds());
}

template<typename T>
std::vector<T> concat(std::vector<T> v1, const std::vector<T>& v2) {
    v1.insert(end(v1), begin(v2), end(v2));
    return v1;
}

class UpdateFlag {
public:
    uint64_t m_nCount = 0;

    bool operator ==(const UpdateFlag& flag) const {
        return m_nCount == flag.m_nCount;
    }

    bool operator !=(const UpdateFlag& flag) const {
        return !(*this == flag);
    }

    void addUpdate() {
        ++m_nCount;
    }
};

}
