#pragma once

#include "Renderer.hpp"

#include <bonez/common.hpp>
#include <bonez/sys/memory.hpp>

namespace BnZ {

// This class manages a list of named renderer and their settings
class RendererManager {
public:
    using RendererFactoryFunction = std::function<Unique<Renderer>()>;
    using RendererPair = std::pair<Shared<Renderer>, RendererFactoryFunction>;

    void addDefaultRenderers();

    void addRenderer(const std::string& name,
                     const RendererFactoryFunction& factoryFunction);

    template<typename RendererType>
    void addRenderer(const std::string& name) {
        addRenderer(name, [] { return makeUnique<RendererType>(); });
    }

    Unique<Renderer> createRenderer(const std::string& name) const;

    void exposeIO();

    const Shared<Renderer>& getCurrentRenderer() const {
        return m_pCurrentRenderer;
    }

    Shared<Renderer> getRenderer(const std::string& name);

    void setSettingsFile(const FilePath& file);

    void storeSettings();

private:
    FilePath m_RenderersSettingsFile;
    tinyxml2::XMLDocument m_RenderersSettings;
    tinyxml2::XMLElement* m_pRenderersElement = nullptr;
    Shared<Renderer> m_pCurrentRenderer;
    uint32_t m_nCurrentRendererIdx = 0;
    std::vector<std::string> m_RendererNames;
    std::unordered_map<std::string, RendererPair> m_RendererMap;
    TwBar* m_pBar = nullptr;
};

}
