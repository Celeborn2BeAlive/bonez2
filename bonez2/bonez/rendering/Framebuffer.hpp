#pragma once

#include <bonez/maths/maths.hpp>
#include <bonez/image/Image.hpp>
#include <bonez/sys/threads.hpp>

namespace BnZ {

class Framebuffer {
    Vec2u m_Size;
    std::vector<Image> m_Images;
public:
    Framebuffer() = default;

    Framebuffer(const Vec2u& size, uint32_t nbChannels):
        Framebuffer(size.x, size.y, nbChannels) {
    }

    Framebuffer(uint32_t width, uint32_t height, uint32_t nbChannels):
        m_Size(width, height), m_Images(max(1u, nbChannels), Image(width, height)) {
    }

    void setChannelCount(uint32_t nbChannels) {
        m_Images.resize(nbChannels, Image(m_Size.x, m_Size.y));
    }

    const Image& getChannel(uint32_t idx) const {
        return m_Images[idx];
    }

    uint32_t getWidth() const {
        return m_Size.x;
    }

    uint32_t getHeight() const {
        return m_Size.y;
    }

    uint32_t getChannelCount() const {
        return m_Images.size();
    }

    uint32_t getPixelCount() const {
        return m_Size.x * m_Size.y;
    }

    const Vec2u& getSize() const {
        return m_Size;
    }

    void accumulate(uint32_t channelIdx, uint32_t pixelIdx, const Vec4f& value) {
        if(channelIdx < m_Images.size()) {
            m_Images[channelIdx][pixelIdx] += value;
        }
    }

    // Accumulate on default channel (0)
    void accumulate(uint32_t pixelIdx, const Vec4f& value) {
        m_Images[0][pixelIdx] += value;
    }

    void clear() {
        processTasks(getChannelCount(), [this](uint32_t taskID, uint32_t threadID) {
            m_Images[taskID].fill(Vec4f(0.f));
        });
    }
};

}
