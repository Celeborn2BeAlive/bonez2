#pragma once

#include <bonez/types.hpp>
#include <bonez/atb.hpp>
#include <bonez/parsing/parsing.hpp>
#include <bonez/sys/memory.hpp>
#include <bonez/sys/threads.hpp>
#include <bonez/sampling/Random.hpp>

#include "Framebuffer.hpp"

namespace BnZ {

class Context;
class Image;
class EmissionSampler;

Vec3f estimateDirectLight(
        const Scene& scene,
        const EmissionSampler& sampler,
        const SurfacePoint& point, const Vec3f& wo,
        const BRDF& brdf,
        float sLightIdx, float s1D, const Vec2f& s2D);

Vec3f estimateDirectLightAndDirectIrradiance(
        const Scene& scene,
        const EmissionSampler& sampler,
        const SurfacePoint& point, const Vec3f& wo,
        const BRDF& brdf,
        Vec3f& directIrradiance,
        float sLightIdx, float s1D, const Vec2f& s2D);

Vec3f estimateDirectIrradiance(
        const Scene& scene,
        const EmissionSampler& sampler,
        const SurfacePoint& point,
        float sLightIdx, float s1D, const Vec2f& s2D);

class Renderer {
private:
    const Scene* m_pScene = nullptr;
    const Camera* m_pCamera = nullptr;
    Framebuffer* m_pFramebuffer = nullptr;
    tinyxml2::XMLElement* m_pStats = nullptr; // If set, the renderer can output some statistics

    Vec2f m_fFramebufferSize;

    int32_t m_nDebugPixelX = 536;
    int32_t m_nDebugPixelY = 136;
protected:

    uint32_t m_nPathDepthMask = 0u;

    uint32_t getPixelIndex(uint32_t x, uint32_t y) const {
        return BnZ::getPixelIndex(x, y, getFramebuffer().getSize());
    }

    bool isDebugPixel(uint32_t pixelIdx) const {
        return pixelIdx == getPixelIndex(m_nDebugPixelX, m_nDebugPixelY);
    }

    bool isDebugPixel(uint32_t x, uint32_t y) const {
        return isDebugPixel(getPixelIndex(x, y));
    }

    const Scene& getScene() const {
        assert(m_pScene);
        return *m_pScene;
    }

    const Camera& getCamera() const {
        assert(m_pCamera);
        return *m_pCamera;
    }

    Framebuffer& getFramebuffer() const {
        assert(m_pFramebuffer);
        return *m_pFramebuffer;
    }

    uint32_t getPathDepthMask() const {
        return m_nPathDepthMask;
    }

    bool acceptPathDepth(uint32_t depth) const {
        return ~m_nPathDepthMask & (1 << depth);
    }

    Ray getPrimaryRay(uint32_t x, uint32_t y, const Vec2f& s2D) const {
        return getCamera().getRay(getNDC(Vec2f(x, y) + s2D, m_fFramebufferSize));
    }

    tinyxml2::XMLElement* getStatisticsOutput() const {
        return m_pStats;
    }

    virtual void doInit() = 0;

    virtual void doRender() = 0;

public:
    virtual ~Renderer() {
    }

    void init(const Scene& scene,
              const Camera& camera,
              Framebuffer& framebuffer,
              const tinyxml2::XMLElement* pSettings = nullptr) {
        m_pScene = &scene;
        m_pCamera = &camera;
        m_pFramebuffer = &framebuffer;
        m_fFramebufferSize = Vec2f(framebuffer.getSize());

        if(pSettings) {
            loadSettings(*pSettings);
        }

        doInit();
    }

    void render() {
        doRender();
    }

    void setStatisticsOutput(tinyxml2::XMLElement* pStats) {
        m_pStats = pStats;
    }

    virtual void storeStatistics() {
        // Do nothing by default
    }

    virtual void loadSettings(const tinyxml2::XMLElement& xml) {
        // Do nothing by default
    }

    virtual void storeSettings(tinyxml2::XMLElement& xml) const {
        // Do nothing by default
    }

    virtual void exposeIO(TwBar* pBar) {
        // Do nothing by default
    }
};

}
