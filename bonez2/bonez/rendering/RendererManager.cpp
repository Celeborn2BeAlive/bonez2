#include "RendererManager.hpp"
#include "renderers/PathtraceRenderer.hpp"
#include "renderers/PathtraceMISRenderer.hpp"
#include "renderers/PrimaryRaysRenderer.hpp"
#include "renderers/LighttraceRenderer.hpp"
#include "renderers/IGIRenderer.hpp"
#include "renderers/VeachBDPTRenderer.hpp"
#include "renderers/RecursiveMISBDPTRenderer.hpp"
#include "renderers/RecursiveMISBDPTWithBSDFRenderer.hpp"

#include "renderers/VCMRenderer.hpp"

#include "renderers/PathtraceWithBSDFRenderer.hpp"

#include "renderers/EG15/SkelBDPTEG15Renderer.hpp"

#include "renderers/VCMWithBSDFRenderer.hpp"

namespace BnZ {

void RendererManager::addDefaultRenderers() {
    addRenderer<PathtraceRenderer>("PathtraceRenderer");
    addRenderer<PrimaryRaysRenderer>("PrimaryRaysRenderer");
    addRenderer<LighttraceRenderer>("LighttraceRenderer");
    addRenderer<IGIRenderer>("IGIRenderer");
    addRenderer<VeachBDPTRenderer>("VeachBDPTRenderer");
    addRenderer<RecursiveMISBDPTRenderer>("RecursiveMISBDPTRenderer");
    addRenderer<VCMRenderer>("VCMRenderer");
    addRenderer<PathtraceMISRenderer>("PathtraceMISRenderer");

    addRenderer<PathtraceWithBSDFRenderer>("PathtraceWithBSDFRenderer");
    addRenderer<RecursiveMISBDPTWithBSDFRenderer>("RecursiveMISBDPTWithBSDFRenderer");
    addRenderer<VCMWithBSDFRenderer>("VCMWithBSDFRenderer");

    addRenderer<SkelBDPTEG15Renderer>("SkelBDPTEG15Renderer");

    m_nCurrentRendererIdx = 0u;
    m_pCurrentRenderer = getRenderer(m_RendererNames[0u]);
    exposeIO();
}

void RendererManager::addRenderer(const std::string& name, const RendererFactoryFunction& factoryFunction) {
    auto it = m_RendererMap.find(name);
    if(end(m_RendererMap) == it) {
        auto idx = m_RendererNames.size();

        //Shared<Renderer> pRenderer = factoryFunction();

        Shared<Renderer> pRenderer = nullptr;

        m_RendererMap[name] = std::make_pair(pRenderer, factoryFunction);
        m_RendererNames.emplace_back(name);

        /*
        if(m_pRenderersElement) {
            auto pSettings = m_pRenderersElement->
                    FirstChildElement(name.c_str());
            if(pSettings) {
                pRenderer->loadSettings(*pSettings);
            }
        }

        if(idx == m_nCurrentRendererIdx) {
            m_pCurrentRenderer = pRenderer;
        }
*/
        if(m_pBar) {
            exposeIO();
        }
    } else {
        throw std::runtime_error("RendererManager::addRenderer - Can't add a renderer with the same name");
    }
}

Unique<Renderer> RendererManager::createRenderer(const std::string& name) const {
    auto it = m_RendererMap.find(name);
    if(end(m_RendererMap) == it) {
        throw std::runtime_error("Unable to create the renderer " + name);
    }
    return (*it).second.second();
}

void RendererManager::exposeIO() {
    if(!m_pBar) {
        m_pBar = TwNewBar("Renderers");
    }

    atb::removeAllVars(m_pBar);

    std::string rendererNames;
    for(const auto& name: m_RendererNames) {
        rendererNames += name + ",";
    }

    auto setRendererCB = [this,rendererNames](uint32_t idx) {
        m_nCurrentRendererIdx = idx;
        m_pCurrentRenderer = getRenderer(m_RendererNames[idx]);

        exposeIO();
    };

    auto getRendererCB =[&]() -> uint32_t {
        return m_nCurrentRendererIdx;
    };

    atb::addVarCB(m_pBar, "Current Renderer", rendererNames,
                  setRendererCB, getRendererCB);
    if(m_pCurrentRenderer) {
        m_pCurrentRenderer->exposeIO(m_pBar);
    }
}

Shared<Renderer> RendererManager::getRenderer(const std::string& name) {
    auto it = m_RendererMap.find(name);
    if(end(m_RendererMap) == it) {
        return nullptr;
    }

    if(!(*it).second.first) {
        (*it).second.first = (*it).second.second();

        if(m_pRenderersElement) {
            auto pSettings = m_pRenderersElement->
                    FirstChildElement(name.c_str());
            if(pSettings) {
                (*it).second.first->loadSettings(*pSettings);
            }
        }

//        if(idx == m_nCurrentRendererIdx) {
//            m_pCurrentRenderer = pRenderer;
//        }
    }

    return (*it).second.first;
}

void RendererManager::setSettingsFile(const FilePath& file) {
    m_RenderersSettings.Clear();

    if(tinyxml2::XML_NO_ERROR !=
            m_RenderersSettings.LoadFile(file.c_str())) {
        m_pRenderersElement = m_RenderersSettings.NewElement("Renderers");
        m_RenderersSettings.InsertFirstChild(m_pRenderersElement);
        m_RenderersSettings.SaveFile(file.c_str());
    } else {
        m_pRenderersElement = m_RenderersSettings.RootElement();
        if(!m_pRenderersElement) {
            throw std::runtime_error("No root element in renderers' settings file");
        }
        getAttribute(*m_pRenderersElement, "currentRenderer", m_nCurrentRendererIdx);
    }
    m_RenderersSettingsFile = file;
}

void RendererManager::storeSettings() {
    if(m_pRenderersElement) {
        m_pRenderersElement->DeleteChildren();
        for(const auto& pair: m_RendererMap) {
            auto pElement = m_RenderersSettings.NewElement(pair.first.c_str());
            m_pRenderersElement->InsertEndChild(pElement);
            if(pair.second.first) {
                pair.second.first->storeSettings(*pElement);
            }
        }
        setAttribute(*m_pRenderersElement, "currentRenderer", m_nCurrentRendererIdx);
        m_RenderersSettings.SaveFile(m_RenderersSettingsFile.c_str());
    }
}

}
