#pragma once

//#include "viewer/Viewer.hpp"
#include "RendererManager.hpp"
#include <bonez/opengl/GLImageRenderer.hpp>
#include <bonez/scene/ConfigManager.hpp>

#include <bonez/scene/cameras/ParaboloidCamera.hpp>

namespace BnZ {

class RenderStatistics;

struct RenderModule {
    RendererManager m_RendererManager;
    Framebuffer m_CPUFramebuffer;
    UpdateFlag m_CameraFlag, m_LightFlag;
    Shared<Renderer> m_CurrentRenderer;

    std::vector<Framebuffer> m_FramebufferList; // Saved framebuffers
    uint32_t m_nFramebufferCount = 0;
    int m_nSelectedFramebuffer = -1;

    bool m_bCPURendering = false;
    bool m_bPause = false;
    float m_fGamma = 1.f;
    uint32_t m_nChannelIdx = 0;

    bool m_bRenderFlag = false;

    void setUp(const FilePath& path, const tinyxml2::XMLElement* pViewerSettings, const Vec2u& framebufferSize) {
        // Init RendererManager
        auto pRenderers = pViewerSettings->FirstChildElement("Renderers");
        if(!pRenderers) {
            throw std::runtime_error("No Renderers tag in viewer settings");
        }
        auto rendererSettings = path + pRenderers->Attribute("path");

        m_RendererManager.setSettingsFile(rendererSettings);
        m_RendererManager.addDefaultRenderers();

        // Init Framebuffer
        m_CPUFramebuffer = Framebuffer(framebufferSize, 8);

        auto pModuleSettings = pViewerSettings->FirstChildElement("RenderModule");
        if(pModuleSettings) {
            getAttribute(*pModuleSettings, "gamma", m_fGamma);
        }
    }

    void tearDown(tinyxml2::XMLElement* pViewerSettings) {
        // Store renderers settings
        m_RendererManager.storeSettings();

        auto pModuleSettings = pViewerSettings->FirstChildElement("RenderModule");
        if(!pModuleSettings) {
            pModuleSettings = pViewerSettings->GetDocument()->NewElement("RenderModule");
            pViewerSettings->InsertEndChild(pModuleSettings);
        }
        setAttribute(*pModuleSettings, "gamma", m_fGamma);
    }

    void exposeIO(TwBar* pBar) {
        atb::addVarRW(pBar, ATB_VAR(m_bCPURendering));
        atb::addVarRW(pBar, ATB_VAR(m_bPause));
        atb::addVarRW(pBar, ATB_VAR(m_fGamma));
        atb::addVarRW(pBar, ATB_VAR(m_nChannelIdx), "min=0 max=8");
        atb::addButton(pBar, "Render", [this]() { m_bRenderFlag = true; });
        atb::addButton(pBar, "Clear", [this]() { clearCPUFramebuffer(); });

        atb::addButton(pBar, "Store on list", [this]() {
            m_FramebufferList.emplace_back(m_CPUFramebuffer);
            m_nFramebufferCount = m_FramebufferList.size();
        });

        atb::addVarRO(pBar, ATB_VAR(m_nFramebufferCount));
        atb::addVarRW(pBar, ATB_VAR(m_nSelectedFramebuffer));

        atb::addButton(pBar, "Remove", [this]() {
            if(m_nSelectedFramebuffer < m_nFramebufferCount) {
                auto it = begin(m_FramebufferList) + m_nSelectedFramebuffer;
                m_FramebufferList.erase(it);
                m_nFramebufferCount = m_FramebufferList.size();
            }
        });

        m_RendererManager.exposeIO();
    }

    void clearCPUFramebuffer() {
        m_CPUFramebuffer.clear();
    }

    void doCPURendering(UpdateFlag cameraUpdateFlag, const Camera& camera, const Scene& scene, GLImageRenderer& imageRenderer) {
        auto pRenderer = m_RendererManager.getCurrentRenderer();

        if(pRenderer) {
            if(pRenderer != m_CurrentRenderer || cameraUpdateFlag != m_CameraFlag || scene.getLightContainer().hasChanged(m_LightFlag)) {
                clearCPUFramebuffer();
                m_CurrentRenderer = pRenderer;
                m_CameraFlag = cameraUpdateFlag;

                m_CurrentRenderer->init(scene, camera, m_CPUFramebuffer);
            }

            if(m_nSelectedFramebuffer < 0 && (!m_bPause || m_bRenderFlag)) {
                m_CurrentRenderer->render();
                m_bRenderFlag = false;
            }
         
            glViewport(0, 0, m_CPUFramebuffer.getWidth(), m_CPUFramebuffer.getHeight());
            if(m_nSelectedFramebuffer < 0) {
                imageRenderer.drawFramebuffer(m_fGamma, m_CPUFramebuffer, m_nChannelIdx);
            } else if(m_nSelectedFramebuffer < m_nFramebufferCount) {
                imageRenderer.drawFramebuffer(m_fGamma, m_FramebufferList[m_nSelectedFramebuffer], m_nChannelIdx);
            }
        }
    }

    void renderConfig(const FilePath& basePath, const std::string& name, const ConfigManager& configManager,
                      Scene& scene, float zNear, float zFar);
private:
    void runCompareRenderGroup(
            const std::string& resultPrefix,
            const FilePath& resultPath,
            const FilePath& resultDocumentPath,
            tinyxml2::XMLDocument& resultsDocument,
            tinyxml2::XMLElement& pGroupElement,
            const tinyxml2::XMLElement* pHeritedSettings,
            const Scene& scene,
            const Camera& camera,
            int iterCount,
            int storeInterval,
            float processingTime,
            float storeStep,
            float minRMSE,
            uint32_t& renderCount,
            const Image& referenceImage);

    void renderCompare(
            const std::string& resultPrefix,
            const FilePath& resultPath,
            const FilePath& resultDocumentPath,
            tinyxml2::XMLDocument& resultsDocument,
            const tinyxml2::XMLElement& pRendererSettings,
            const Scene& scene,
            const Camera& camera,
            int iterCount,
            int storeInterval,
            float processingTime,
            float storeStep,
            float minRMSE,
            uint32_t index,
            const Image& referenceImage);

    void renderReference(const tinyxml2::XMLDocument& configDocument,
                         const FilePath& resultPath,
                         const Scene& scene, const Camera& camera);

    void storeResult(const FilePath& resultDir,
                     const std::string& resultPrefix,
                     uint32_t index,
                     const RenderStatistics& stats,
                     float gamma,
                     const Framebuffer& framebuffer);

    void storeResultStep(const FilePath& resultDir,
                         const std::string& resultPrefix,
                     uint32_t index,
                     const RenderStatistics& stats,
                     float gamma,
                     const Framebuffer& framebuffer,
                     const Image& refImage);
};

}
