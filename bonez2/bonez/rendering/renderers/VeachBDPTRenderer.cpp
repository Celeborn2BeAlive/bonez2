#include "VeachBDPTRenderer.hpp"
#include <bonez/scene/lights/AreaLight.hpp>

namespace BnZ {

VeachBDPTRenderer::VeachBDPTRenderer() {
}

std::string VeachBDPTRenderer::getName() const {
    return "VeachBDPTRenderer";
}

uint32_t VeachBDPTRenderer::getMaxLightPathDepth() const {
    return m_nMaxDepth - 2;
}

uint32_t VeachBDPTRenderer::getMaxEyePathDepth() const {
    return m_nMaxDepth;
}

float computeLightPathWeight(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                             VeachBDPTRenderer::PathVertex* eyePathVertex, VeachBDPTRenderer::PathVertex* lightPathVertex, const EmissionVertex* lightNode) {
    auto sum = 0.f;
    auto p = 1.f;
    auto lightPathLength = lightPathVertex->depth;

    auto currentVertex = eyePathVertex;
    auto nextVertex = lightPathVertex;
    auto incidentDir = currentVertex->incidentDirection; // Incident direction on the currentVertex when going toward the light node

    auto exitantDir = nextVertex->lastVertex.P - currentVertex->lastVertex.P; // Exitent direction on the currentVertex when going toward the light node
    auto l = length(exitantDir);
    if(l == 0.f) {
        return 0.f; // avoid NaN bugs
    }
    exitantDir /= l;
    auto sqrDist = sqr(l);

    for(auto i = 0u; i < lightPathLength; ++i) {
        auto pdfWrtSolidAngle = currentVertex->lastVertexBRDF.pdf(incidentDir, exitantDir);
        auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(nextVertex->lastVertex.Ns, -exitantDir)) / sqrDist;

        p *= pdfWrtArea / nextVertex->pdfLastVertex;
        sum += p;

        currentVertex = nextVertex;
        --nextVertex;
        incidentDir = -exitantDir;
        exitantDir = currentVertex->incidentDirection; // The incident direction of the next current vertex goes toward the light node
        sqrDist = sqr(currentVertex->lastVertex.distance);
    }

    p *= currentVertex->lastVertexBRDF.pdf(incidentDir, currentVertex->incidentDirection) * lightNode->g(currentVertex->lastVertex) / lightNode->pdf();
    sum += p;

    return sum;
}

float computeEyePathWeight(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                           VeachBDPTRenderer::PathVertex* eyePathVertex, VeachBDPTRenderer::PathVertex* lightPathVertex) {
    auto sum = 0.f;
    auto p = 1.f;
    auto eyePathLength = eyePathVertex->depth;

    auto currentVertex = lightPathVertex;
    auto nextVertex = eyePathVertex;
    auto incidentDir = currentVertex->incidentDirection; // Incident direction on the currentVertex when going toward the eye node

    auto exitantDir = nextVertex->lastVertex.P - currentVertex->lastVertex.P;  // Exitent direction on the currentVertex when going toward the light node
    auto l = length(exitantDir);
    if(l == 0.f) {
        return 0.f; // avoid NaN bugs
    }
    exitantDir /= l;
    auto sqrDist = sqr(l);

    for(auto i = 0u; i < eyePathLength - 1; ++i) {
        auto pdfWrtSolidAngle = currentVertex->lastVertexBRDF.pdf(incidentDir, exitantDir);
        auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(nextVertex->lastVertex.Ns, -exitantDir)) / sqrDist;

        p *= pdfWrtArea / nextVertex->pdfLastVertex;
        sum += p;

        currentVertex = nextVertex;
        --nextVertex;
        incidentDir = -exitantDir;
        exitantDir = currentVertex->incidentDirection; // The incident direction of the next current vertex goes toward the eye node
        sqrDist = sqr(currentVertex->lastVertex.distance);
    }

    return sum;
}

float computeMISWeight(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       VeachBDPTRenderer::PathVertex* eyePathVertex, VeachBDPTRenderer::PathVertex* lightPathVertex, const EmissionVertex* lightNode) {
    auto sum = 1.f;

    sum += computeLightPathWeight(threadID, pixelID, sampleID, eyePathVertex, lightPathVertex, lightNode);

    sum += computeEyePathWeight(threadID, pixelID, sampleID, eyePathVertex, lightPathVertex);

    return 1.f / sum;
}

float computeMISWeight(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       VeachBDPTRenderer::PathVertex* eyePathVertex,
                       const EmissionVertex* lightNode,
                       const Vec3f& dirToLight) {
    auto sum = 1.f;

    { // Eval p_0
       auto pdfWrtSolidAngle = eyePathVertex->lastVertexBRDF.pdf(eyePathVertex->incidentDirection, dirToLight);
       auto pdfWrtArea = pdfWrtSolidAngle * lightNode->g(eyePathVertex->lastVertex);

       sum += pdfWrtArea / lightNode->pdf();
    }

    if(eyePathVertex->depth > 1u) {
        auto p = 1.f;

        auto pdfWrtArea = lightNode->pdfWrtArea(eyePathVertex->lastVertex);
        p *= pdfWrtArea / eyePathVertex->pdfLastVertex;

        sum += p;

        auto currentVertex = eyePathVertex;
        auto nextVertex = eyePathVertex - 1;
        auto incidentDir = dirToLight;

        auto pEyePathLength = eyePathVertex->depth;

        if(pEyePathLength > 1) {
            for(auto i = 0u; i < pEyePathLength - 2; ++i) {
                auto exitantDir = currentVertex->incidentDirection;
                auto pdfWrtSolidAngle = currentVertex->lastVertexBRDF.pdf(incidentDir, exitantDir);
                auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(nextVertex->lastVertex.Ns, -exitantDir)) / sqr(currentVertex->lastVertex.distance);

                p *= pdfWrtArea / nextVertex->pdfLastVertex;
                sum += p;

                currentVertex = nextVertex;
                --nextVertex;
                incidentDir = -exitantDir;
            }
        }
    }

    return 1.f / sum;
}

float computeMISWeight(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       VeachBDPTRenderer::PathVertex* eyePathVertex, const Scene& scene, const EmissionSampler& sampler) {
    if(eyePathVertex->lastVertex.Le == zero<Vec3f>()) {
        return 0.f;
    }

    if(eyePathVertex->depth == 1u) {
        return 1.f;
    }

    if(!eyePathVertex->lastVertex) {
        // Hit on environment map (if present) should be handled here
        return 0.f;
    }

    auto meshID = eyePathVertex->lastVertex.meshID;
    auto lightID = scene.getGeometry().getMesh(meshID).m_nLightID;

    const auto& pLight = scene.getLightContainer().getLight(lightID);
    const auto* pAreaLight = static_cast<const AreaLight*>(pLight.get());

    auto pdfWrtArea = sampler.pdf(lightID) * pAreaLight->pdf(eyePathVertex->lastVertex, scene);

    auto sum = 1.f;
    auto p = 1.f;

    p *= pdfWrtArea / eyePathVertex->pdfLastVertex;

    sum += p;

    if(eyePathVertex->depth > 2u) {
        auto currentVertex = eyePathVertex;
        auto nextVertex = eyePathVertex - 1;

        {
            auto exitantDir = currentVertex->incidentDirection;
            auto pdfWrtSolidAngle = cosineSampleHemispherePDF(eyePathVertex->incidentDirection, eyePathVertex->lastVertex.Ns);
            auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(nextVertex->lastVertex.Ns, -exitantDir)) / sqr(currentVertex->lastVertex.distance);

            p *= pdfWrtArea / eyePathVertex->pdfLastVertex;

            sum += p;
        }

        currentVertex = nextVertex;
        --nextVertex;
        auto incidentDir = -eyePathVertex->incidentDirection;
        while(currentVertex->depth > 2u) {
            auto exitantDir = currentVertex->incidentDirection;
            auto pdfWrtSolidAngle = currentVertex->lastVertexBRDF.pdf(incidentDir, exitantDir);
            auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(nextVertex->lastVertex.Ns, -exitantDir)) / sqr(currentVertex->lastVertex.distance);

            p *= pdfWrtArea / nextVertex->pdfLastVertex;
            sum += p;

            currentVertex = nextVertex;
            --nextVertex;
            incidentDir = -exitantDir;
        }
    }

    return 1.f / sum;
}

Unique<EmissionVertex> VeachBDPTRenderer::sampleLightPath(
        const Scene& scene, const EmissionSampler& sampler,
        uint32_t threadID, uint32_t pixelID, PathVertex* pBuffer,
        uint32_t& lightPathLength,
        uint32_t maxLightPathDepth) const {
    auto pEmissionVertex = sampler.sample(scene,
                                          getFloat(threadID),
                                          getFloat(threadID),
                                          getFloat2(threadID));

    if(!pEmissionVertex || !pEmissionVertex->pdf()) {
        return nullptr;
    }

    if(maxLightPathDepth == 0u) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    RaySample ray;
    auto s2D = getFloat2(threadID);
    auto Le_dir = pEmissionVertex->sampleWi(ray, s2D);

    if(!ray.pdf) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    auto I = scene.intersect(ray.value);

    if(!I) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    auto power = pEmissionVertex->getRadiantExitance() * Le_dir / (pEmissionVertex->pdf() * ray.pdf);
    auto pdfLastVertex = pEmissionVertex->pdfWrtArea(I);
    auto pdf = pEmissionVertex->pdf() * pdfLastVertex;
    auto incidentDirection = -ray.value.dir;
    auto brdf = scene.shade(I);
    pBuffer[0].lastVertex = I;
    pBuffer[0].lastVertexBRDF = brdf;
    pBuffer[0].incidentDirection = incidentDirection;
    pBuffer[0].pdf = pdf;
    pBuffer[0].pdfLastVertex = pdfLastVertex;
    pBuffer[0].power = power;
    pBuffer[0].depth = 1u;

    lightPathLength = 1u;

    for(auto length = 2u; length <= maxLightPathDepth; ++length) {
        auto idx = length - 1;

        Sample3f woSample;
        auto fr = brdf.sample(incidentDirection, woSample, getFloat2(threadID));
        if(woSample.pdf > 0.f && fr != zero<Vec3f>()) {
            auto Itmp = scene.intersect(Ray(I, woSample.value));

            if(Itmp) {
                incidentDirection = -woSample.value;
                auto sqrLength = sqr(Itmp.distance);
                pdfLastVertex = woSample.pdf  * max(0.f, dot(Itmp.Ns, incidentDirection)) / sqrLength;
                pdf *= pdfLastVertex;
                power *= max(0.f, dot(I.Ns, woSample.value)) * fr / woSample.pdf;
            } else {
                break;
            }

            if(pdfLastVertex == 0.f) {
                break;
            }

            I = Itmp;
            brdf = scene.shade(I);

            lightPathLength = length;
            pBuffer[idx].lastVertex = I;
            pBuffer[idx].lastVertexBRDF = brdf;
            pBuffer[idx].incidentDirection = incidentDirection;
            pBuffer[idx].pdf = pdf;
            pBuffer[idx].pdfLastVertex = pdfLastVertex;
            pBuffer[idx].power = power;
            pBuffer[idx].depth = length;
        } else {
            break;
        }
    }

    return pEmissionVertex;
}

void VeachBDPTRenderer::Li(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       uint32_t x, uint32_t y, Vec4f values[8]) const {
    for(auto i = 0u; i < 8; ++i) {
        values[i] += Vec4f(0.f, 0.f, 0.f, 1.f);
    }

    auto s2D = getPixelSample(threadID, sampleID);
    auto primRay = getPrimaryRay(x, y, s2D);

    auto pLightPath = m_PathArray.get() + threadID * (getMaxLightPathDepth() + getMaxEyePathDepth());
    auto pEyePath = pLightPath + getMaxLightPathDepth();

    {
        auto I = getScene().intersect(primRay);

        if(!I) {
            values[0] += Vec4f(I.Le, 1.f);
            values[1] += Vec4f(I.Le, 1.f);
            return;
        }

        pEyePath[0].lastVertex = I;
        pEyePath[0].lastVertexBRDF = getScene().shade(pEyePath[0].lastVertex);
        pEyePath[0].incidentDirection = -primRay.dir.xyz();
        pEyePath[0].depth = 1;
        pEyePath[0].pdf = 1.f;
        pEyePath[0].pdfLastVertex = 1.f;
        pEyePath[0].power = Vec3f(1.f);
    }

    uint32_t lightPathLength;
    auto lightNode = sampleLightPath(getScene(), m_EmissionSampler,
                                     threadID, pixelID, pLightPath,
                                     lightPathLength, getMaxLightPathDepth());

    auto L = zero<Vec3f>();

    if(lightNode) {
        auto maxEyePathDepth = getMaxEyePathDepth();
        // trace an eye pathpEyePath[0].lastVertex
        for(auto i = 1u; i <= maxEyePathDepth; ++i) {
            auto k = i - 1u; // index of vertex in pEyePath;

            { // Intersection with light source
                auto totalLength = i;
                if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                    auto weight = computeMISWeight(threadID, pixelID, sampleID, &pEyePath[k], getScene(), m_EmissionSampler);
                    auto contrib = pEyePath[k].power * pEyePath[k].lastVertex.Le;
                    L += weight * contrib;
                    values[totalLength] += Vec4f(weight * contrib, 0.f);
                }
            }

            { // Direct illumination
                auto totalLength = i + 1;
                if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                    Ray shadowRay;
                    auto G = lightNode->G(pEyePath[k].lastVertex, shadowRay);
                    if(G > 0.f && !getScene().occluded(shadowRay)) {
                        auto contrib = pEyePath[k].power * lightNode->power() * lightNode->Le(-shadowRay.dir)
                                * G * pEyePath[k].lastVertexBRDF.eval(shadowRay.dir, pEyePath[k].incidentDirection);
                        auto weight = computeMISWeight(threadID, pixelID, sampleID, &pEyePath[k], lightNode.get(), shadowRay.dir);
                        L += weight * contrib;
                        values[totalLength] += Vec4f(weight * contrib, 0.f);
                    }
                }
            }

            for(auto j = 0u; j < lightPathLength; ++j) {
                auto lightPath = pLightPath + j;
                auto totalLength = i + lightPath->depth + 1;

                if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                    Vec3f incidentDirection;
                    float dist;
                    auto G = geometricFactor(pEyePath[k].lastVertex, lightPath->lastVertex, incidentDirection, dist);
                    Ray incidentRay(pEyePath[k].lastVertex, lightPath->lastVertex, incidentDirection, dist);

                    if(G > 0.f) {
                        bool isVisible = !getScene().occluded(incidentRay);

                        if(isVisible) {
                            auto weight = computeMISWeight(threadID, pixelID, sampleID, &pEyePath[k], lightPath, lightNode.get());

                            auto contrib = pEyePath[k].power * lightPath->power * lightPath->lastVertexBRDF.eval(lightPath->incidentDirection, -incidentDirection)
                                    * G * pEyePath[k].lastVertexBRDF.eval(incidentDirection, pEyePath[k].incidentDirection);

                            L += weight * contrib;

                            values[totalLength] += Vec4f(weight * contrib, 0.f);
                        }
                    }
                }
            }

            if(i < maxEyePathDepth) {
                Sample3f wiSample;
                auto fr = pEyePath[k].lastVertexBRDF.sample(pEyePath[k].incidentDirection, wiSample, getFloat2(threadID));

                if(wiSample.pdf > 0.f) {
                    auto throughput = fr * max(0.f, dot(pEyePath[k].lastVertex.Ns, wiSample.value)) / wiSample.pdf;
                    auto I = getScene().intersect(Ray(pEyePath[k].lastVertex, wiSample.value));
                    if(!I) {
                        break;
                    }

                    pEyePath[k + 1].lastVertex = I;
                    pEyePath[k + 1].lastVertexBRDF = getScene().shade(pEyePath[k + 1].lastVertex);
                    pEyePath[k + 1].incidentDirection = -wiSample.value;
                    pEyePath[k + 1].depth = pEyePath[k].depth + 1;
                    pEyePath[k + 1].pdfLastVertex = wiSample.pdf * max(0.f, dot(I.Ns, -wiSample.value)) / sqr(I.distance);
                    pEyePath[k + 1].pdf = pEyePath[k].pdf * pEyePath[k + 1].pdfLastVertex;
                    pEyePath[k + 1].power = pEyePath[k].power * throughput;

                    if(pEyePath[k + 1].pdfLastVertex == 0.f) {
                        break;
                    }

                } else {
                    break;
                }
            }
        }
    }

    values[0] += Vec4f(L, 0.f);
}

void VeachBDPTRenderer::preprocess() {
    m_EmissionSampler.initFrame(getScene());

    m_PathArray = makeUniqueArray<PathVertex>(getThreadCount() * (getMaxLightPathDepth() + getMaxEyePathDepth()));
}

void VeachBDPTRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    auto spp = getSppCount();
    Vec4f L[8];

    TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
        auto pixelID = getPixelIndex(x, y);
        std::fill(L, L + 8, Vec4f(0.f));
        for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
            Li(threadID, pixelID, sampleID, x, y, L);
        }
        for(auto i = 0u; i < 8; ++i) {
            getFramebuffer().accumulate(i, pixelID, L[i]);
        }
    });

    if(m_bDisplayProgress) {
        auto l = debugLock();
        std::cerr << "Tile " << tileID << " / " << getTileCount() << " (" << (tileID * 100.f / getTileCount()) << " %)" << std::endl;
    }
}

void VeachBDPTRenderer::doExposeIO(TwBar* bar) {
    atb::addVarRW(bar, ATB_VAR(m_nMaxDepth));
    atb::addVarRW(bar, ATB_VAR(m_bDisplayProgress));
}

void VeachBDPTRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
    getAttribute(xml, "maxDepth", m_nMaxDepth);
    getAttribute(xml, "displayProgress", m_bDisplayProgress);
}

void VeachBDPTRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
    setAttribute(xml, "maxDepth", m_nMaxDepth);
    setAttribute(xml, "displayProgress", m_bDisplayProgress);
}

}
