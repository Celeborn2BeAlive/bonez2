#include "PathtraceWithBSDFRenderer.hpp"
#include <bonez/scene/shading/BSDF.hpp>

namespace BnZ {

std::string PathtraceWithBSDFRenderer::getName() const {
    return "PathtraceWithBSDFRenderer";
}

void PathtraceWithBSDFRenderer::preprocess() {
    m_EmissionSampler.initFrame(getScene());
}

void PathtraceWithBSDFRenderer::processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const {
    for(auto i = 0u; i <= m_nMaxPathDepth; ++i) {
        getFramebuffer().accumulate(i, pixelID, Vec4f(0.f, 0.f, 0.f, 1.f));
    }

    auto s2D = getPixelSample(threadID, sampleID);
    auto ray = getPrimaryRay(x, y, s2D);

    Vec3f throughput = Vec3f(1.f);
    Vec3f L = Vec3f(0.f);

    auto I = getScene().intersect(ray);
    Vec3f wo = -ray.dir.xyz();

    if(acceptPathDepth(1)) {
        getFramebuffer().accumulate(1, pixelID, Vec4f(I.Le, 0.f));
        L += I.Le;
    }

    auto lightNode = m_EmissionSampler.sample(getScene(), getFloat(threadID), getFloat(threadID), getFloat2(threadID));

    uint32_t sampledEvent = ScatteringEvent::Absorption;

    for(auto i = 1u; I && i <= m_nMaxPathDepth; ++i) {
        BSDF bsdf(wo, I, getScene());
        // Next event estimation
        if(i < m_nMaxPathDepth && acceptPathDepth(i + 1)) {
            Ray shadowRay;
            auto G = lightNode->G(I, shadowRay);
            if(G > 0.f && !getScene().occluded(shadowRay)) {
                float cosThetaOutDir;
                auto contrib = throughput * lightNode->power() * lightNode->Le(-shadowRay.dir)
                        * G * bsdf.eval(shadowRay.dir, cosThetaOutDir);
                getFramebuffer().accumulate(i + 1, pixelID, Vec4f(contrib, 0.f));
                L += contrib;
            }
        }

        // If the scattering was specular, add Le
        if((sampledEvent & ScatteringEvent::Specular) && acceptPathDepth(i)) {
            auto contrib = throughput * I.Le;
            getFramebuffer().accumulate(i, pixelID, Vec4f(contrib, 0.f));
            L += contrib;
        }

        if(i < m_nMaxPathDepth) {
            Sample3f wi;
            float cosThetaOutDir;

            auto fr = bsdf.sample(Vec3f(getFloat(threadID), getFloat2(threadID)), wi, cosThetaOutDir, &sampledEvent);

            if(fr != zero<Vec3f>() && wi.pdf > 0.f) {
                throughput *= fr * abs(cosThetaOutDir) / wi.pdf;
                I = getScene().intersect(secondaryRay(I, wi.value));
                wo = -wi.value;
            } else {
                break;
            }
        }
    }

    getFramebuffer().accumulate(0, pixelID, Vec4f(L, 0.f));
}

void PathtraceWithBSDFRenderer::doExposeIO(TwBar* bar) {
    atb::addVarRW(bar, ATB_VAR(m_nMaxPathDepth));
}

void PathtraceWithBSDFRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
    getAttribute(xml, "maxDepth", m_nMaxPathDepth);
}

void PathtraceWithBSDFRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
    setAttribute(xml, "maxDepth", m_nMaxPathDepth);
}

void PathtraceWithBSDFRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    processTileSamples(viewport, [this, threadID](uint32_t x, uint32_t y, uint32_t pixelID, uint32_t sampleID) {
        processSample(threadID, pixelID, sampleID, x, y);
    });
}

}
