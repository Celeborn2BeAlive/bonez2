#pragma once

#include "TileProcessingRenderer.hpp"

namespace BnZ {

class PrimaryRaysRenderer: public TileProcessingRenderer {
public:
    std::string getName() const override {
        return "PrimaryRaysRenderer";
    }

    void processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       uint32_t x, uint32_t y) const {
        auto s2D = getPixelSample(threadID, sampleID);
        auto ray = getPrimaryRay(x, y, s2D);

        getFramebuffer().accumulate(0, pixelID, Vec4f(ray.dir, 1.f));
    }

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override {
        processTileSamples(viewport, [this, threadID](uint32_t x, uint32_t y, uint32_t pixelID, uint32_t sampleID) {
            processSample(threadID, pixelID, sampleID, x, y);
        });
    }
};


}
