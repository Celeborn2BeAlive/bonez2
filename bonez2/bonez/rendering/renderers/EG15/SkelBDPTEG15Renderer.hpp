#pragma once

#include <bonez/rendering/renderers/TileProcessingRenderer.hpp>
#include <bonez/sampling/multiple_importance_sampling.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/sampling/distribution1d.h>
#include <bonez/sampling/shapes.hpp>
#include <bonez/sampling/patterns.hpp>
#include <bonez/scene/lights/EmissionSampler.hpp>
#include <bonez/scene/shading/BSDF.hpp>

#include <bonez/opengl/debug/GLDebugRenderer.hpp>
#include <bonez/opengl/debug/GLDebugOutputStream.hpp>

namespace BnZ {

class SkelBDPTEG15Renderer: public TileProcessingRenderer {
    uint32_t m_nMaxDepth = 3;

    uint32_t m_nLightPathCount = 1024;
    float m_fPowerSkelFactor = 0.1f;
    uint32_t m_nMaxNodeCount = 4;
    bool m_bUseSkelGridMapping = false;
    bool m_bUseSkeleton = true;
    bool m_bUniformResampling = false;

    bool m_bDisplayProgress = false;

    EmissionSampler m_EmissionSampler;

    enum RenderTarget {
        FINAL_RENDER = 0
    };

    struct PathVertex {
        Intersection lastVertex;
        BSDF lastVertexBSDF;
        float pdf; // pdf of the complete path
        float pdfLastVertex; // pdf of the last vertex, conditional to the previous
        Vec3f power;
        uint32_t depth; // number of edges of the path

        float dVCM;
        float dVC;
    };

    Unique<EmissionVertex> sampleLightPath(
            const Scene& scene, const EmissionSampler& sampler,
            uint32_t threadID, uint32_t pixelID, PathVertex* pBuffer,
            uint32_t& lightPathLength,
            uint32_t maxLightPathDepth) const;

    bool sampleScattering(uint32_t threadID, uint32_t pixelID,
                          const PathVertex& currentVertex, PathVertex& nextVertex,
                          bool sampleAjdoint) const;

    Unique<PathVertex[]> m_EyePathArray;
    Unique<PathVertex[]> m_LightPathArray;
    Unique<std::vector<PathVertex*>[]> m_LightVertexPerTile; // For each tile, contains the light vertices that project on that tile
    uint32_t m_nLightVertexCount;

    float Mis(float pdf) const {
        return pdf; // Balance heuristic
    }

    struct SkelMappingData {
        std::vector<Vec4i> nodes;
        std::vector<Vec4f> weights;

        void resizeBuffers(uint32_t size) {
            nodes.resize(size);
            weights.resize(size);
        }
    };

    SkelMappingData m_SkelMappingData;

    Unique<float[]> m_SkelNodeLightVertexDistributionsArray; // For each node, contains a discrete distribution among light paths
    Unique<float[]> m_DefaultLightVertexDistributionsArray; // When a point is not associated with any node, use this distribution
    uint32_t m_nDistributionSize;

    void skeletonMapping(const SurfacePoint& I, uint32_t pixelID,
                         Vec4i& nearestNodes, Vec4f& weights) const;

    void computeSkeletonMapping();

    void computeLightVertexDistributions();

    uint32_t getLightPathVertexPerNode() const;

    void connectLightVerticesToCamera(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const;

    void computeEmittedRadiance(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                           uint32_t x, uint32_t y, const PathVertex& eyeVertex) const;

    void computeDirectIllumination(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                   uint32_t x, uint32_t y, const PathVertex& eyeVertex,
                                   const EmissionVertex& lightVertex) const;

    void connectVertices(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                         uint32_t x, uint32_t y, const PathVertex& eyeVertex,
                         const PathVertex& lightVertex, float lightVertexResamplingPdf) const;

    bool samplePrimaryEyeVertex(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                uint32_t x, uint32_t y, PathVertex& eyeVertex) const;

public:
    SkelBDPTEG15Renderer();

    virtual std::string getName() const;

    uint32_t getMaxLightPathDepth() const;

    uint32_t getMaxEyePathDepth() const;

    void processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const;

    void preprocess() override;

    void beginFrame() override;

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override;

    virtual void doExposeIO(TwBar* bar);

    virtual void doLoadSettings(const tinyxml2::XMLElement& xml);

    virtual void doStoreSettings(tinyxml2::XMLElement& xml) const;
};

}
