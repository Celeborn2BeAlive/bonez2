#include "SkelBDPTEG15Renderer.hpp"
#include <bonez/scene/lights/AreaLight.hpp>

namespace BnZ {

inline float computeNodeWeightDistCosMaxball(float dist, Vec3f wi, Vec3f N_ws, float r) {
    float c = max(0.0001f, dot(wi, N_ws)); // Works because node view matrix are only translations
    return r * c / (dist * dist);
}

template<typename SkelNodeOcclusionFunction>
Vec4i getNearestNodes(const Skeleton& skel, const Vec3f& P, const Vec3f& N, Vec4f& weights, const SkelNodeOcclusionFunction& occluded) {
    Vec4i nearestNodes = Vec4i(-1);
    Vec4f currentWeights = Vec4f(0);

    for(auto i = 0u; i < skel.size(); ++i) {
        auto nodeP = skel.getNode(i).P;

        auto dir = P - nodeP;
        auto dist = length(dir);
        dir /= dist;

        auto wi = -dir;
        float weight = computeNodeWeightDistCosMaxball(dist, wi, N, skel.getNode(i).maxball);
        if(weight > currentWeights.x || weight > currentWeights.y || weight > currentWeights.z || currentWeights.w) {
            bool occ= occluded(i, dir, dist);

            if(!occ) {
                if(weight > currentWeights.x) {
                    for(int i = 3; i > 0; --i) {
                        currentWeights[i] = currentWeights[i - 1];
                        nearestNodes[i] = nearestNodes[i - 1];
                    }
                    currentWeights.x = weight;
                    nearestNodes.x = i;
                } else if(weight > currentWeights.y) {
                    for(int i = 3; i > 1; --i) {
                        currentWeights[i] = currentWeights[i - 1];
                        nearestNodes[i] = nearestNodes[i - 1];
                    }
                    currentWeights.y = weight;
                    nearestNodes.y = i;
                } else if(weight > currentWeights.z) {
                    for(int i = 3; i > 2; --i) {
                        currentWeights[i] = currentWeights[i - 1];
                        nearestNodes[i] = nearestNodes[i - 1];
                    }
                    currentWeights.z = weight;
                    nearestNodes.z = i;
                } else if(weight > currentWeights.w) {
                    currentWeights.w = weight;
                    nearestNodes.w = i;
                }
            }
        }
    }

    weights = currentWeights;
    return nearestNodes;
}

template<typename SkelNodeOcclusionFunction>
int getNearestNode(const Skeleton& skel, const Vec3f& P, const Vec3f& N, const SkelNodeOcclusionFunction& occluded) {
    int nearestNode = -1;
    float currentWeight = 0;

    for(auto i = 0u; i < skel.size(); ++i) {
        auto nodeP = skel.getNode(i).P;

        auto dir = P - nodeP;
        auto dist = length(dir);
        dir /= dist;

        auto wi = -dir;
        float weight = computeNodeWeightDistCosMaxball(dist, wi, N, skel.getNode(i).maxball);
        if(weight > currentWeight) {
            bool occ = occluded(i, dir, dist);

            if(!occ) {
                if(weight > currentWeight) {
                    currentWeight = weight;
                    nearestNode = i;
                }
            }
        }
    }
    return nearestNode;
}

inline Sample1u sampleNodeFromWeights(
        const Vec4i& nodeIndices,
        const Vec4f& nodeWeights,
        uint32_t maxNodeCount,
        float s1D) {
    auto size = 0u;
    Vec4f weights = zero<Vec4f>();
    for (size = 0u;
         size < min(maxNodeCount, 4u)
         && nodeIndices[size] >= 0;
         ++size) {
        weights[size] = nodeWeights[size];
    }

    if(size > 0) {
        DiscreteDistribution4f dist(weights);
        return dist.sample(s1D);
    }

    return Sample1u();
}

SkelBDPTEG15Renderer::SkelBDPTEG15Renderer() {
}

std::string SkelBDPTEG15Renderer::getName() const {
    return "SkelBDPTEG15Renderer";
}

uint32_t SkelBDPTEG15Renderer::getMaxLightPathDepth() const {
    return m_nMaxDepth - 1;
}

uint32_t SkelBDPTEG15Renderer::getMaxEyePathDepth() const {
    return m_nMaxDepth;
}

bool SkelBDPTEG15Renderer::sampleScattering(uint32_t threadID, uint32_t pixelID,
                                            const PathVertex& currentVertex, PathVertex& nextVertex,
                                            bool sampleAjdoint) const {
    Sample3f woSample;
    float cosThetaOutDir;
    uint32_t sampledEvent;
    auto fs = currentVertex.lastVertexBSDF.sample(Vec3f(getFloat(threadID), getFloat2(threadID)), woSample, cosThetaOutDir, &sampledEvent, sampleAjdoint);

    if(woSample.pdf == 0.f || fs == zero<Vec3f>()) {
        return false;
    }

    auto I = getScene().intersect(Ray(currentVertex.lastVertex, woSample.value));

    if(!I) {
        return false;
    }

    auto incidentDirection = -woSample.value;
    auto sqrLength = sqr(I.distance);
    auto pdfLastVertex = woSample.pdf  * abs(dot(I.Ns, incidentDirection)) / sqrLength;

    if(pdfLastVertex == 0.f) {
        return false;
    }

    nextVertex.lastVertex = I;
    nextVertex.lastVertexBSDF = BSDF(incidentDirection, I, getScene());
    nextVertex.pdf = currentVertex.pdf * pdfLastVertex;
    nextVertex.pdfLastVertex = pdfLastVertex;
    nextVertex.power = currentVertex.power * abs(cosThetaOutDir) * fs / woSample.pdf;
    nextVertex.depth = currentVertex.depth + 1;

    auto g = abs(cosThetaOutDir) / sqrLength;

    if((sampledEvent & ScatteringEvent::Specular) && currentVertex.lastVertexBSDF.isDelta()) {
        nextVertex.dVCM = 0.f;
        nextVertex.dVC = Mis(g) * currentVertex.dVC;
    } else {
        float reversePdf = currentVertex.lastVertexBSDF.pdf(woSample.value, true);

        nextVertex.dVCM = Mis(1.f / nextVertex.pdfLastVertex);
        nextVertex.dVC = Mis(g / nextVertex.pdfLastVertex) * (currentVertex.dVCM + Mis(reversePdf) * currentVertex.dVC);
    }

    return true;
}

Unique<EmissionVertex> SkelBDPTEG15Renderer::sampleLightPath(
        const Scene& scene, const EmissionSampler& sampler,
        uint32_t threadID, uint32_t pixelID, PathVertex* pBuffer,
        uint32_t& lightPathLength,
        uint32_t maxLightPathDepth) const {
    for(PathVertex *it = pBuffer, *end = pBuffer + maxLightPathDepth; it != end; ++it) {
        it->pdf = 0.f;
    }

    auto pEmissionVertex = sampler.sample(scene,
                                          getFloat(threadID),
                                          getFloat(threadID),
                                          getFloat2(threadID));

    if(!pEmissionVertex || !pEmissionVertex->pdf()) {
        return nullptr;
    }

    if(maxLightPathDepth == 0u) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    RaySample ray;
    auto s2D = getFloat2(threadID);
    auto Le_dir = pEmissionVertex->sampleWi(ray, s2D);

    if(!ray.pdf) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    auto I = scene.intersect(ray.value);

    if(!I) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    auto power = pEmissionVertex->getRadiantExitance() * Le_dir / (pEmissionVertex->pdf() * ray.pdf);
    auto pdfLastVertex = pEmissionVertex->pdfWrtArea(I);
    auto pdf = pEmissionVertex->pdf() * pdfLastVertex;
    auto incidentDirection = -ray.value.dir;
    auto bsdf = BSDF(incidentDirection, I, getScene());
    pBuffer[0].lastVertex = I;
    pBuffer[0].lastVertexBSDF = bsdf;
    pBuffer[0].pdf = pdf;
    pBuffer[0].pdfLastVertex = pdfLastVertex;
    pBuffer[0].power = power;
    pBuffer[0].depth = 1u;

    pBuffer[0].dVCM = Mis(1.f / pBuffer[0].pdfLastVertex);
    pBuffer[0].dVC = Mis(pEmissionVertex->g(pBuffer[0].lastVertex) / (pEmissionVertex->pdf() * pBuffer[0].pdfLastVertex));

    lightPathLength = 1u;

    for(auto length = 2u; length <= maxLightPathDepth; ++length) {
        auto idx = length - 1;

        if(!sampleScattering(threadID, pixelID, pBuffer[idx - 1], pBuffer[idx], true)) {
            break;
        }

        ++lightPathLength;
    }

    return pEmissionVertex;
}

void SkelBDPTEG15Renderer::computeEmittedRadiance(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                                              uint32_t x, uint32_t y, const PathVertex& eyeVertex) const {
    if(eyeVertex.lastVertex.Le == zero<Vec3f>()) {
        return;
    }

    auto contrib = eyeVertex.power * eyeVertex.lastVertex.Le;

    auto rcpWeight = 1.f;
    if(eyeVertex.depth > 1u) {
        if(!eyeVertex.lastVertex) {
            // Hit on environment map (if present) should be handled here

        } else {
            auto meshID = eyeVertex.lastVertex.meshID;
            auto lightID = getScene().getGeometry().getMesh(meshID).m_nLightID;

            const auto& pLight = getScene().getLightContainer().getLight(lightID);
            const auto* pAreaLight = static_cast<const AreaLight*>(pLight.get());

            float pointPdfWrtArea, directionPdfWrtSolidAngle;
            pAreaLight->pdf(eyeVertex.lastVertex, eyeVertex.lastVertexBSDF.getIncidentDirection(), getScene(),
                            pointPdfWrtArea, directionPdfWrtSolidAngle);

            pointPdfWrtArea *= m_EmissionSampler.pdf(lightID);

            rcpWeight += Mis(pointPdfWrtArea) * (eyeVertex.dVCM + Mis(directionPdfWrtSolidAngle) * eyeVertex.dVC);
        }
    }

    auto weight = 1.f / rcpWeight;

    auto weightedContrib = weight * contrib;
    getFramebuffer().accumulate(0, pixelID, Vec4f(weightedContrib, 0));
    getFramebuffer().accumulate(eyeVertex.depth, pixelID, Vec4f(weightedContrib, 0));
}

void SkelBDPTEG15Renderer::computeDirectIllumination(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                                     uint32_t x, uint32_t y, const PathVertex& eyeVertex,
                                                     const EmissionVertex& lightVertex) const {
    Ray shadowRay;
    auto G = lightVertex.G(eyeVertex.lastVertex, shadowRay);

    float cosAtEyeVertex;
    float eyeDirPdf, eyeRevPdf;

    auto fr = eyeVertex.lastVertexBSDF.eval(shadowRay.dir, cosAtEyeVertex, &eyeDirPdf, &eyeRevPdf);
    if(G > 0.f && fr != zero<Vec3f>() && !getScene().occluded(shadowRay)) {
        auto contrib = eyeVertex.power * lightVertex.power() * lightVertex.Le(-shadowRay.dir) * G * fr;

        auto rcpWeight = 1.f;

        {
           auto pdfWrtArea = eyeDirPdf * lightVertex.g(eyeVertex.lastVertex);
           rcpWeight += Mis(pdfWrtArea / lightVertex.pdf());
        }

        {
            auto pdfWrtArea = lightVertex.pdfWrtArea(eyeVertex.lastVertex);
            rcpWeight += Mis(pdfWrtArea) * (eyeVertex.dVCM + Mis(eyeRevPdf) * eyeVertex.dVC);
        }

        auto weight = 1.f / rcpWeight;

        auto weightedContrib = weight * contrib;
        getFramebuffer().accumulate(0, pixelID, Vec4f(weightedContrib, 0));
        getFramebuffer().accumulate(eyeVertex.depth + 1, pixelID, Vec4f(weightedContrib, 0));
    }
}

void SkelBDPTEG15Renderer::connectVertices(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                           uint32_t x, uint32_t y, const PathVertex& eyeVertex,
                                           const PathVertex& lightVertex, float lightVertexResamplingPdf) const {
    Vec3f incidentDirection;
    float dist;
    auto G = geometricFactor(eyeVertex.lastVertex, lightVertex.lastVertex, incidentDirection, dist);
    Ray incidentRay(eyeVertex.lastVertex, lightVertex.lastVertex, incidentDirection, dist);

    float cosAtLightVertex, cosAtEyeVertex;
    float lightDirPdf, lightRevPdf;
    float eyeDirPdf, eyeRevPdf;

    auto M = lightVertex.lastVertexBSDF.eval(-incidentDirection, cosAtLightVertex, &lightDirPdf, &lightRevPdf) *
            eyeVertex.lastVertexBSDF.eval(incidentDirection, cosAtEyeVertex, &eyeDirPdf, &eyeRevPdf);

    if(G > 0.f && M != zero<Vec3f>()) {
        bool isVisible = !getScene().occluded(incidentRay);

        if(isVisible) {
            auto rcpWeight = 1.f;

            {
                auto pdfWrtArea = eyeDirPdf * abs(cosAtLightVertex) / sqr(dist);
                rcpWeight += Mis(pdfWrtArea) * (lightVertex.dVCM + Mis(lightRevPdf) * lightVertex.dVC);
            }

            {
                auto pdfWrtArea = lightDirPdf * abs(cosAtEyeVertex) / sqr(dist);
                rcpWeight += Mis(pdfWrtArea) * (eyeVertex.dVCM + Mis(eyeRevPdf) * eyeVertex.dVC);
            }

            auto weight = 1.f / rcpWeight;

            auto contrib = (1.f / (lightVertexResamplingPdf * m_nLightPathCount)) * eyeVertex.power * lightVertex.power * G * M;

            auto weightedContrib = weight * contrib;
            getFramebuffer().accumulate(0, pixelID, Vec4f(weightedContrib, 0));
            getFramebuffer().accumulate(eyeVertex.depth + 1 + lightVertex.depth, pixelID, Vec4f(weightedContrib, 0));
        }
    }
}

bool SkelBDPTEG15Renderer::samplePrimaryEyeVertex(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                            uint32_t x, uint32_t y, PathVertex& eyeVertex) const {
    auto s2D = getPixelSample(threadID, sampleID);
    auto primRay = getPrimaryRay(x, y, s2D);

    auto I = getScene().intersect(primRay);

    if(!I) {
        // Hit on environment light, if exists
        getFramebuffer().accumulate(0, pixelID, Vec4f(I.Le, 0));
        getFramebuffer().accumulate(1, pixelID, Vec4f(I.Le, 0));
        return false;
    }

    eyeVertex.lastVertex = I;
    eyeVertex.lastVertexBSDF = BSDF(-primRay.dir.xyz(), eyeVertex.lastVertex, getScene());
    eyeVertex.depth = 1;

    Ray shadowRay;
    auto surfaceToImageFactor = getCamera().surfaceToImageFactor(eyeVertex.lastVertex, shadowRay);
    auto imageToSurfaceFactor = 1.f / surfaceToImageFactor;

    eyeVertex.pdf = imageToSurfaceFactor;
    eyeVertex.pdfLastVertex = imageToSurfaceFactor;
    eyeVertex.power = Vec3f(1.f);

    eyeVertex.dVCM = Mis(m_nLightPathCount / imageToSurfaceFactor);
    eyeVertex.dVC = 0.f;

    return true;
}

void SkelBDPTEG15Renderer::skeletonMapping(const SurfacePoint& I, uint32_t pixelID,
                                           Vec4i& nearestNodes, Vec4f& weights) const {
    if(m_bUseSkelGridMapping) {
        nearestNodes = Vec4i(getScene().getSegmentedCurvSkeleton()->getNearestNode(I), -1, -1, -1);
        weights = Vec4f(1, 0, 0, 0);
    } else {
        nearestNodes = m_SkelMappingData.nodes[pixelID];
        weights = m_SkelMappingData.weights[pixelID];
    }
}

void SkelBDPTEG15Renderer::processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       uint32_t x, uint32_t y) const {
    for(auto i = 0u; i <= m_nMaxDepth; ++i) {
        getFramebuffer().accumulate(i, pixelID, Vec4f(0, 0, 0, 1));
    }

    auto pEyePath = m_EyePathArray.get() + threadID * getMaxEyePathDepth();

    auto lightNode = m_EmissionSampler.sample(getScene(), getFloat(threadID), getFloat(threadID), getFloat2(threadID));

    auto maxEyePathDepth = getMaxEyePathDepth();
    auto maxLightPathDepth = getMaxLightPathDepth();

    if(!samplePrimaryEyeVertex(threadID, pixelID, sampleID, x, y, pEyePath[0])) {
        return;
    }

    for(auto i = 1u; i <= maxEyePathDepth; ++i) {
        auto k = i - 1u; // index of vertex in pEyePath;

        Vec4i nodes;
        Vec4f nodeWeights;
        DiscreteDistribution4f nodeDist;

        if(m_bUseSkeleton) {
            if(i > 1u) {
                nodes = Vec4i(getScene().getSegmentedCurvSkeleton()->getNearestNode(pEyePath[k].lastVertex), -1, -1, -1);
                nodeWeights = Vec4f(1, 0, 0, 0);
            } else {
                skeletonMapping(pEyePath[k].lastVertex, pixelID, nodes, nodeWeights);

                auto size = 0u;
                Vec4f weights = zero<Vec4f>();
                for (size = 0u;
                     size < min(m_nMaxNodeCount, 4u)
                     && nodes[size] >= 0;
                     ++size) {
                    weights[size] = nodeWeights[size];
                }

                nodeDist = DiscreteDistribution4f(weights);
            }
        }

        { // Intersection with light source
            auto totalLength = i;
            if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                computeEmittedRadiance(threadID, pixelID, sampleID, x, y, pEyePath[k]);
            }
        }

        { // Direct illumination
            auto totalLength = i + 1;
            if(lightNode && lightNode->pdf() > 0.f && acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                computeDirectIllumination(threadID, pixelID, sampleID, x, y, pEyePath[k], *lightNode);
            }
        }

        for(auto j = 0u; j < maxLightPathDepth; ++j) {
            auto lightPathDepth = j + 1;
            auto totalLength = i + lightPathDepth + 1;

            if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                //auto lightPath = pLightPath + j;

                float* pDistribution = nullptr;

                auto firstPathOffset = lightPathDepth - 1;

                pDistribution = m_DefaultLightVertexDistributionsArray.get() + (lightPathDepth - 1) * m_nDistributionSize;

                if(m_bUseSkeleton) {
                    Sample1u sampledNodeChannel;
                    if(i == 1u) {
                        sampledNodeChannel = nodeDist.sample(getFloat(threadID));
                    } else {
                        sampledNodeChannel = Sample1u(0, 1.f);
                    }

                    if(sampledNodeChannel.pdf > 0.f) {
                        auto sampledNodeIdx = nodes[sampledNodeChannel.value];
                        if(sampledNodeIdx >= 0) {
                            auto distribOffset = sampledNodeIdx * m_nDistributionSize * getMaxLightPathDepth() +
                                    (lightPathDepth - 1) * m_nDistributionSize;
                            pDistribution = m_SkelNodeLightVertexDistributionsArray.get() + distribOffset;
                        }
                    }
                }

                Sample1u lightPathDiscreteSample;

                if(m_bUniformResampling) {
                    uint32_t idx = clamp(uint32_t(getFloat(threadID) * m_nLightPathCount), 0u, m_nLightPathCount - 1);
                    lightPathDiscreteSample = Sample1u(idx, 1.f / m_nLightPathCount);
                } else {
                    lightPathDiscreteSample =
                        sampleDiscreteDistribution1D(pDistribution, m_nLightPathCount, getFloat(threadID));
                }

                auto lightPath = &m_LightPathArray[firstPathOffset + lightPathDiscreteSample.value * getMaxLightPathDepth()];

                if(lightPathDiscreteSample.pdf > 0.f && lightPath->pdf > 0.f) {
                    connectVertices(threadID, pixelID, sampleID, x, y, pEyePath[k], *lightPath, lightPathDiscreteSample.pdf);
                }
            }
        }

        if(i < maxEyePathDepth) {
            auto idx = k + 1;
            if(!sampleScattering(threadID, pixelID, pEyePath[idx - 1], pEyePath[idx], false)) {
                break;
            }
        }
    }
}

uint32_t SkelBDPTEG15Renderer::getLightPathVertexPerNode() const {
    return m_nLightPathCount * getMaxLightPathDepth();
}

void SkelBDPTEG15Renderer::computeLightVertexDistributions() {
    auto maxLightPathDepth = getMaxLightPathDepth();
    auto nbVertexPerNode = getLightPathVertexPerNode();
    auto pSkel = getScene().getSegmentedCurvSkeleton();
    auto completeSize = nbVertexPerNode * pSkel->size();

    if(!m_SkelNodeLightVertexDistributionsArray) {
        m_nDistributionSize = getDistribution1DBufferSize(m_nLightPathCount);
        // A distribution contains all sub-paths of the same depth
        // There is maxLightPathDepth possible depth and m_nLightPathCount paths of a given depth
        m_SkelNodeLightVertexDistributionsArray = makeUniqueArray<float>(maxLightPathDepth * m_nDistributionSize * pSkel->size());
        m_DefaultLightVertexDistributionsArray = makeUniqueArray<float>(maxLightPathDepth * m_nDistributionSize);
    }

    // Build the default distribution for each light sub-path depth
    for(auto depth = 1u; depth <= maxLightPathDepth; ++depth) {
        auto distribOffset = (depth - 1) * m_nDistributionSize;
        auto firstPathOffset = depth - 1;

        buildDistribution1D([this, firstPathOffset, maxLightPathDepth](uint32_t i) {
            auto pathIdx = firstPathOffset + i * maxLightPathDepth;
            if(m_LightPathArray[pathIdx].pdf == 0.f) {
                return 0.f;
            }
            return luminance(m_LightPathArray[pathIdx].power);
        }, m_DefaultLightVertexDistributionsArray.get() + distribOffset, m_nLightPathCount);
    }

    if(m_bUseSkeleton) {
        // For each node, compute intersections, irradiance values and distributions
        launchThreads([pSkel, nbVertexPerNode, maxLightPathDepth, this](uint32_t threadID) {
            uint32_t loopID = 0u;

            while(true) {
                auto nodeIndex = loopID * getThreadCount() + threadID;
                loopID++;

                if(nodeIndex >= pSkel->size()) {
                    break;
                }

                auto nodePos = pSkel->getNode(nodeIndex).P;
                auto nodeRadius = pSkel->getNode(nodeIndex).maxball;

                for(auto depth = 1u; depth <= maxLightPathDepth; ++depth) {
                    auto distribOffset = nodeIndex * m_nDistributionSize * maxLightPathDepth + (depth - 1) * m_nDistributionSize;
                    auto firstPathOffset = depth - 1;
                    auto visibilityOffset = nodeIndex * m_nLightPathCount * maxLightPathDepth;

                    buildDistribution1D([this, visibilityOffset, firstPathOffset, maxLightPathDepth, nodePos, nodeRadius, nodeIndex](uint32_t i) {
                        auto pathIdx = firstPathOffset + i * maxLightPathDepth;

                        if(m_LightPathArray[pathIdx].pdf == 0.f) {
                            return 0.f;
                        }

                        auto& I = m_LightPathArray[pathIdx].lastVertex;
                        auto dir = nodePos - I.P;
                        auto l = BnZ::length(dir);
                        dir /= l;

                        float weight = sqr(nodeRadius) * (1.f / sqr(l)) * luminance(m_LightPathArray[pathIdx].power);

                        if(dot(I.Ns, dir) <= 0.f || getScene().occluded(Ray(I, dir, l))) {
                            return weight * m_fPowerSkelFactor;
                        }

                        return weight;
                    }, m_SkelNodeLightVertexDistributionsArray.get() + distribOffset, m_nLightPathCount);
                }
            }
        });
    }
}

void SkelBDPTEG15Renderer::computeSkeletonMapping() {
    auto framebufferSize = getFramebuffer().getSize();
    m_SkelMappingData.resizeBuffers(framebufferSize.x * framebufferSize.y);

    Vec2u tileCount = framebufferSize / getTileSize() +
            Vec2u(framebufferSize % getTileSize() != zero<Vec2u>());

    auto totalCount = tileCount.x * tileCount.y;

    auto pSkel = getScene().getSegmentedCurvSkeleton();

    auto task = [&](uint32_t threadID) {
        auto loopID = 0u;
        while(true) {
            auto tileID = loopID * getThreadCount() + threadID;
            ++loopID;

            if(tileID >= totalCount) {
                return;
            }

            uint32_t tileX = tileID % tileCount.x;
            uint32_t tileY = tileID / tileCount.x;

            Vec2u tileOrg = Vec2u(tileX, tileY) * getTileSize();
            auto viewport = Vec4u(tileOrg, getTileSize());

            if(viewport.x + viewport.z > framebufferSize.x) {
                viewport.z = framebufferSize.x - viewport.x;
            }

            if(viewport.y + viewport.w > framebufferSize.y) {
                viewport.w = framebufferSize.y - viewport.y;
            }

            auto xEnd = viewport.x + viewport.z;
            auto yEnd = viewport.y + viewport.w;

            for(auto y = viewport.y; y < yEnd; ++y) {
                for(auto x = viewport.x; x < xEnd; ++x) {
                    auto pixelIdx = getPixelIndex(x, y);
                    auto s2D = Vec2f(0.5f, 0.5f);
                    auto ray = getPrimaryRay(x, y, s2D);
                    auto I = getScene().intersect(ray);
                    if(I) {
                        m_SkelMappingData.nodes[pixelIdx] = getNearestNodes(*pSkel, I.P, I.Ns, m_SkelMappingData.weights[pixelIdx],
                                            [](uint32_t nodeIdx, Vec3f dir, float dist) { return false; });
                    } else {
                        m_SkelMappingData.nodes[pixelIdx] = Vec4i(-1);
                    }
                }
            }
        }
    };

    launchThreads(task);
}

void SkelBDPTEG15Renderer::preprocess() {
    m_EmissionSampler.initFrame(getScene());

    m_EyePathArray = makeUniqueArray<PathVertex>(getThreadCount() * getMaxEyePathDepth());
    m_LightPathArray = makeUniqueArray<PathVertex>(m_nLightPathCount * getMaxLightPathDepth());

    m_LightVertexPerTile = makeUniqueArray<std::vector<PathVertex*>>(getTileCount());

    getFramebuffer().setChannelCount(1 + m_nMaxDepth);
    getFramebuffer().clear();

    if(m_bUseSkeleton && !m_bUseSkelGridMapping) {
        computeSkeletonMapping();
    }
}

void SkelBDPTEG15Renderer::beginFrame() {
    auto maxLightPathDepth = getMaxLightPathDepth();

    m_nLightVertexCount = m_nLightPathCount * maxLightPathDepth;

    auto batchSize = m_nLightPathCount / getThreadCount();

    launchThreads([&](uint32_t threadID) {
        auto loopID = 0u;
        while(true) {
            auto batchID = loopID * getThreadCount() + threadID;
            ++loopID;

            auto pathIdx = batchID * batchSize;

            if(pathIdx >= m_nLightPathCount) {
                break;
            }

            auto pathEnd = min(m_nLightPathCount, pathIdx + batchSize);
            for(; pathIdx < pathEnd; ++pathIdx) {
                auto offset = pathIdx * maxLightPathDepth;
                uint32_t lightPathLength;
                sampleLightPath(getScene(), m_EmissionSampler, threadID, 0u, &m_LightPathArray[offset], lightPathLength, maxLightPathDepth);
            }
        }
    });

    auto tileSize = getTileSize();

    std::for_each(m_LightVertexPerTile.get(), m_LightVertexPerTile.get() + getTileCount(), [](std::vector<PathVertex*>& v) {
        v.clear();
    });

    const auto& framebuffer = getFramebuffer();

    auto pEnd = m_LightPathArray.get() + m_nLightVertexCount;
    for(auto pLightVertex = m_LightPathArray.get(); pLightVertex != pEnd; ++pLightVertex) {
        Vec2f ndc;
        if(getCamera().getNDC(pLightVertex->lastVertex.P, ndc)) {
            auto uv = ndcToUV(ndc);
            auto pixel = getPixel(uv, framebuffer.getSize());

            auto tile = pixel / tileSize;
            auto tileID = getTileID(tile);

            m_LightVertexPerTile[tileID].emplace_back(pLightVertex);
        }
    }

    if(m_bUseSkeleton || !m_bUniformResampling) {
        computeLightVertexDistributions();
    }
}

void SkelBDPTEG15Renderer::connectLightVerticesToCamera(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    const auto& camera = getCamera();
    const auto& scene = getScene();
    auto& framebuffer = getFramebuffer();

    auto rcpPathCount = 1.f / m_nLightPathCount;

    for(auto pLightVertex: m_LightVertexPerTile[tileID]) {
        if(!pLightVertex->pdf) {
            continue;
        }

        auto totalDepth = 1 + pLightVertex->depth;
        if(!acceptPathDepth(totalDepth) || totalDepth > m_nMaxDepth) {
            continue;
        }

        Vec2f ndc;
        if(camera.getNDC(pLightVertex->lastVertex.P, ndc)) {
            auto uv = ndcToUV(ndc);
            auto pixel = getPixel(uv, framebuffer.getSize());

            auto pixelID = BnZ::getPixelIndex(pixel, framebuffer.getSize());

            Ray shadowRay;
            auto surfaceToImageFactor =
                    camera.surfaceToImageFactor(pLightVertex->lastVertex,
                                                shadowRay);
            float costThetaOurDir;
            float bsdfRevPdfW;
            auto fr = pLightVertex->lastVertexBSDF.eval(shadowRay.dir, costThetaOurDir, nullptr, &bsdfRevPdfW);

            const float imageToSurfaceFactor = 1.f / surfaceToImageFactor;

            const float cameraPdfA = imageToSurfaceFactor;
            const float wLight = Mis(cameraPdfA * rcpPathCount) *
                    (pLightVertex->dVCM + Mis(bsdfRevPdfW) * pLightVertex->dVC);

            const float misWeight = 1.f / (wLight + 1.f);

            if(surfaceToImageFactor > 0.f && fr != zero<Vec3f>()) {
                if(!scene.occluded(shadowRay)) {
                    auto imageToSurfaceFactor = 1.f / surfaceToImageFactor;
                    framebuffer.accumulate(0u, pixelID, Vec4f(misWeight * rcpPathCount * pLightVertex->power * fr * imageToSurfaceFactor, 0.f));
                    framebuffer.accumulate(totalDepth, pixelID, Vec4f(misWeight * rcpPathCount * pLightVertex->power * fr * imageToSurfaceFactor, 0.f));
                }
            }

        }
    }
}

void SkelBDPTEG15Renderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    auto spp = getSppCount();

    TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
        auto pixelID = getPixelIndex(x, y);
        for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
            processSample(threadID, pixelID, sampleID, x, y);
        }
    });

    connectLightVerticesToCamera(threadID, tileID, viewport);

    if(m_bDisplayProgress) {
        auto l = debugLock();
        std::cerr << "Tile " << tileID << " / " << getTileCount() << " (" << (tileID * 100.f / getTileCount()) << " %)" << std::endl;
    }
}

void SkelBDPTEG15Renderer::doExposeIO(TwBar* bar) {
    atb::addVarRW(bar, ATB_VAR(m_nMaxDepth));
    atb::addVarRW(bar, ATB_VAR(m_bDisplayProgress));
    atb::addVarRW(bar, ATB_VAR(m_nLightPathCount));
    atb::addVarRW(bar, ATB_VAR(m_fPowerSkelFactor));
    atb::addVarRW(bar, ATB_VAR(m_nMaxNodeCount));
    atb::addVarRW(bar, ATB_VAR(m_bUseSkelGridMapping));
    atb::addVarRW(bar, ATB_VAR(m_bUseSkeleton));
    atb::addVarRW(bar, ATB_VAR(m_bUniformResampling));
}

void SkelBDPTEG15Renderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
    getAttribute(xml, "maxDepth", m_nMaxDepth);
    getAttribute(xml, "displayProgress", m_bDisplayProgress);
    getAttribute(xml, "lightPathCount", m_nLightPathCount);
    getAttribute(xml, "visibilityPunishment", m_fPowerSkelFactor);
    getAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
    getAttribute(xml, "useSkelGridMapping", m_bUseSkelGridMapping);
    getAttribute(xml, "useSkeleton", m_bUseSkeleton);
    getAttribute(xml, "uniformResampling", m_bUniformResampling);
}

void SkelBDPTEG15Renderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
    setAttribute(xml, "maxDepth", m_nMaxDepth);
    setAttribute(xml, "displayProgress", m_bDisplayProgress);
    setAttribute(xml, "lightPathCount", m_nLightPathCount);
    setAttribute(xml, "visibilityPunishment", m_fPowerSkelFactor);
    setAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
    setAttribute(xml, "useSkelGridMapping", m_bUseSkelGridMapping);
    setAttribute(xml, "useSkeleton", m_bUseSkeleton);
    setAttribute(xml, "uniformResampling", m_bUniformResampling);
}

}
