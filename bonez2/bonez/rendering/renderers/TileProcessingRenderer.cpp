#include "TileProcessingRenderer.hpp"

namespace BnZ {

void TileProcessingRenderer::resetStats() {
    m_fInitFrameTime = 0.f;
    m_fEndFrameTime = 0.f;
    m_fTileProcessingTime = 0.f;
    m_fMeanProcessingTime = 0.f;
    m_fSumProcessingTime = 0.f;
    m_fWorstProcessingTime = 0.f;
    m_fBestProcessingTime = 0.f;
    m_nFrameCount = 0;
    m_nTotalSpp = 0;
}

void TileProcessingRenderer::doInit() {
    resetStats();

    m_Rngs.resize(m_nThreadCount);
    for(auto i = 0u; i < m_nThreadCount; ++i) {
        m_Rngs[i].setSeed(getSeed() * m_nThreadCount + i);
    }

    m_nSpp = m_Spp.x * m_Spp.y;
    m_FramebufferSize = getFramebuffer().getSize();
    m_TileCount = m_FramebufferSize / m_TileSize +
            Vec2u(m_FramebufferSize % m_TileSize != zero<Vec2u>());
    m_nTileCount = m_TileCount.x * m_TileCount.y;

    preprocess();
}

void TileProcessingRenderer::doRender() {
    auto start = getMicroseconds();

    beginFrame();

    auto end = getMicroseconds();

    m_fInitFrameTime += us2ms(end - start);

    start = getMicroseconds();

    processTiles([&](uint32_t threadID, uint32_t tileID, const Vec4u& viewport) {
        processTile(threadID, tileID, viewport);
    });

    end = getMicroseconds();

    m_fTileProcessingTime += us2ms(end - start);

    m_fSumProcessingTime += m_fTileProcessingTime;
    ++m_nFrameCount;
    m_fMeanProcessingTime = m_fSumProcessingTime / m_nFrameCount;
    m_fWorstProcessingTime = max(m_fWorstProcessingTime, m_fTileProcessingTime);
    m_fBestProcessingTime = min(m_fBestProcessingTime, m_fTileProcessingTime);

    m_nTotalSpp += m_Spp.x * m_Spp.y;

    start = getMicroseconds();

    endFrame();

    end = getMicroseconds();

    m_fEndFrameTime = us2ms(end - start);
}

void TileProcessingRenderer::exposeIO(TwBar* bar) {
    atb::addVarRW(bar, ATB_VAR(m_Spp.x), " min=1 ");
    atb::addVarRW(bar, ATB_VAR(m_Spp.y), " min=1 ");
    atb::addVarRW(bar, ATB_VAR(m_nPathDepthMask));

    atb::addSeparator(bar);

    atb::addVarRO(bar, ATB_VAR(m_nTotalSpp));
    atb::addVarRO(bar, "InitFrame (ms)", m_fInitFrameTime);
    atb::addVarRO(bar, "EndFrame (ms)", m_fEndFrameTime);
    atb::addVarRO(bar, "TileProcessing (ms)", m_fTileProcessingTime);
    atb::addVarRO(bar, ATB_VAR(m_fMeanProcessingTime));
    atb::addVarRO(bar, ATB_VAR(m_fWorstProcessingTime));
    atb::addVarRO(bar, ATB_VAR(m_fBestProcessingTime));
    atb::addButton(bar, "Reset timings", [this]() {
        resetStats();
    });

    atb::addSeparator(bar);

    doExposeIO(bar);
}

void TileProcessingRenderer::loadSettings(const tinyxml2::XMLElement& xml) {
    getAttribute(xml, "sppX", m_Spp.x);
    getAttribute(xml, "sppY", m_Spp.y);
    getAttribute(xml, "seed", m_nSeed);
    getAttribute(xml, "pathDepthMask", m_nPathDepthMask);

    doLoadSettings(xml);
}

void TileProcessingRenderer::storeSettings(tinyxml2::XMLElement& xml) const {
    setAttribute(xml, "name", getName());

    setAttribute(xml, "sppX", m_Spp.x);
    setAttribute(xml, "sppY", m_Spp.y);
    setAttribute(xml, "seed", m_nSeed);
    setAttribute(xml, "pathDepthMask", m_nPathDepthMask);

    doStoreSettings(xml);
}

void TileProcessingRenderer::storeStatistics() {
    if(auto pStats = getStatisticsOutput()) {
        setAttribute(*pStats, "totalSpp", m_nTotalSpp);
        setAttribute(*pStats, "initFrameTotalTime", m_fInitFrameTime);
        setAttribute(*pStats, "tileProcessingTotalTime", m_fTileProcessingTime);
    }

    doStoreStatistics();
}

}
