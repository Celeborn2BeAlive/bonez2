#include "RecursiveMISBDPTRenderer.hpp"
#include <bonez/scene/lights/AreaLight.hpp>

namespace BnZ {

RecursiveMISBDPTRenderer::RecursiveMISBDPTRenderer() {
}

std::string RecursiveMISBDPTRenderer::getName() const {
    return "RecursiveMISBDPTRenderer";
}

uint32_t RecursiveMISBDPTRenderer::getMaxLightPathDepth() const {
    return m_nMaxDepth - 1;
}

uint32_t RecursiveMISBDPTRenderer::getMaxEyePathDepth() const {
    return m_nMaxDepth;
}

Unique<EmissionVertex> RecursiveMISBDPTRenderer::sampleLightPath(
        const Scene& scene, const EmissionSampler& sampler,
        uint32_t threadID, uint32_t pixelID, PathVertex* pBuffer,
        uint32_t& lightPathLength,
        uint32_t maxLightPathDepth) const {
    auto pEmissionVertex = sampler.sample(scene,
                                          getFloat(threadID),
                                          getFloat(threadID),
                                          getFloat2(threadID));

    if(!pEmissionVertex || !pEmissionVertex->pdf()) {
        return nullptr;
    }

    if(maxLightPathDepth == 0u) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    RaySample ray;
    auto s2D = getFloat2(threadID);
    auto Le_dir = pEmissionVertex->sampleWi(ray, s2D);

    if(!ray.pdf) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    auto I = scene.intersect(ray.value);

    if(!I) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    auto power = pEmissionVertex->getRadiantExitance() * Le_dir / (pEmissionVertex->pdf() * ray.pdf);
    auto pdfLastVertex = pEmissionVertex->pdfWrtArea(I);
    auto pdf = pEmissionVertex->pdf() * pdfLastVertex;
    auto incidentDirection = -ray.value.dir;
    auto brdf = scene.shade(I);
    pBuffer[0].lastVertex = I;
    pBuffer[0].lastVertexBRDF = brdf;
    pBuffer[0].incidentDirection = incidentDirection;
    pBuffer[0].pdf = pdf;
    pBuffer[0].pdfLastVertex = pdfLastVertex;
    pBuffer[0].power = power;
    pBuffer[0].depth = 1u;

    pBuffer[0].dVCM = 1.f / pBuffer[0].pdfLastVertex;
    pBuffer[0].dVC = pEmissionVertex->g(pBuffer[0].lastVertex) / (pEmissionVertex->pdf() * pBuffer[0].pdfLastVertex);

    lightPathLength = 1u;

    for(auto length = 2u; length <= maxLightPathDepth; ++length) {
        auto idx = length - 1;

        Sample3f woSample;
        auto fr = brdf.sample(incidentDirection, woSample, getFloat2(threadID));
        if(woSample.pdf > 0.f && fr != zero<Vec3f>()) {
            auto Itmp = scene.intersect(Ray(I, woSample.value));

            if(Itmp) {
                incidentDirection = -woSample.value;
                auto sqrLength = sqr(Itmp.distance);
                pdfLastVertex = woSample.pdf  * max(0.f, dot(Itmp.Ns, incidentDirection)) / sqrLength;
                pdf *= pdfLastVertex;
                power *= max(0.f, dot(I.Ns, woSample.value)) * fr / woSample.pdf;
            } else {
                break;
            }

            if(pdfLastVertex == 0.f) {
                break;
            }

            I = Itmp;
            brdf = scene.shade(I);

            lightPathLength = length;
            pBuffer[idx].lastVertex = I;
            pBuffer[idx].lastVertexBRDF = brdf;
            pBuffer[idx].incidentDirection = incidentDirection;
            pBuffer[idx].pdf = pdf;
            pBuffer[idx].pdfLastVertex = pdfLastVertex;
            pBuffer[idx].power = power;
            pBuffer[idx].depth = length;

            auto g = max(0.f, dot(pBuffer[idx - 1].lastVertex.Ns, woSample.value)) / sqr(pBuffer[idx].lastVertex.distance);
            float reversePdf = pBuffer[idx - 1].lastVertexBRDF.pdf(woSample.value, pBuffer[idx - 1].incidentDirection);

            pBuffer[idx].dVCM = 1.f / pBuffer[idx].pdfLastVertex;
            pBuffer[idx].dVC = g * (pBuffer[idx - 1].dVCM + pBuffer[idx - 1].dVC * reversePdf) / pBuffer[idx].pdfLastVertex;

        } else {
            break;
        }
    }

    return pEmissionVertex;
}

float RecursiveMISBDPTRenderer::computeMISWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                PathVertex* eyePathVertex,
                                const EmissionVertex* lightNode,
                                const Vec3f& dirToLight) const {
    auto sum = 1.f;

    { // Eval p_0
       auto pdfWrtSolidAngle = eyePathVertex->lastVertexBRDF.pdf(eyePathVertex->incidentDirection, dirToLight);
       auto pdfWrtArea = pdfWrtSolidAngle * lightNode->g(eyePathVertex->lastVertex);

       sum += pdfWrtArea / lightNode->pdf();
    }

    //if(eyePathVertex->depth > 1u) {
        auto pdfWrtArea = lightNode->pdfWrtArea(eyePathVertex->lastVertex);

        auto p1 = pdfWrtArea;

        auto p2 = eyePathVertex->lastVertexBRDF.pdf(dirToLight, eyePathVertex->incidentDirection);

        sum += p1 * (eyePathVertex->dVCM + p2 * eyePathVertex->dVC);
    //}

    return 1.f / sum;
}

float RecursiveMISBDPTRenderer::computeMISWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       PathVertex* eyePathVertex, const Scene& scene, const EmissionSampler& sampler) const {
    if(eyePathVertex->lastVertex.Le == zero<Vec3f>()) {
        return 0.f;
    }

    if(eyePathVertex->depth == 1u) {
        return 1.f;
    }

    if(!eyePathVertex->lastVertex) {
        // Hit on environment map (if present) should be handled here
        return 0.f;
    }

    auto meshID = eyePathVertex->lastVertex.meshID;
    auto lightID = scene.getGeometry().getMesh(meshID).m_nLightID;

    const auto& pLight = scene.getLightContainer().getLight(lightID);
    const auto* pAreaLight = static_cast<const AreaLight*>(pLight.get());

    auto pdfWrtArea = sampler.pdf(lightID) * pAreaLight->pdf(eyePathVertex->lastVertex, scene);

    auto p1 = pdfWrtArea;

    auto p2 = cosineSampleHemispherePDF(eyePathVertex->incidentDirection, eyePathVertex->lastVertex.Ns);

    auto sum = 1 + p1 * (eyePathVertex->dVCM + p2 * eyePathVertex->dVC);
    return 1.f / sum;
}

float RecursiveMISBDPTRenderer::computeLightPathWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                             PathVertex* eyePathVertex, PathVertex* lightPathVertex) const {
    auto exitantDir = lightPathVertex->lastVertex.P - eyePathVertex->lastVertex.P; // Exitent direction on the currentVertex when going toward the light node
    auto l = length(exitantDir);
    if(l == 0.f) {
        return 0.f; // avoid NaN bugs
    }
    exitantDir /= l;
    auto sqrDist = sqr(l);

    auto pdfWrtSolidAngle = eyePathVertex->lastVertexBRDF.pdf(eyePathVertex->incidentDirection, exitantDir);
    auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(lightPathVertex->lastVertex.Ns, -exitantDir)) / sqrDist;

    auto p1 = pdfWrtArea;

    auto p2 = lightPathVertex->lastVertexBRDF.pdf(-exitantDir, lightPathVertex->incidentDirection);

    return p1 * (lightPathVertex->dVCM + p2 * lightPathVertex->dVC);
}


float RecursiveMISBDPTRenderer::computeEyePathWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                           PathVertex* eyePathVertex, PathVertex* lightPathVertex) const {
    if(eyePathVertex->depth == 1u) {
        return 0.f;
    }

    auto exitantDir = eyePathVertex->lastVertex.P - lightPathVertex->lastVertex.P; // Exitent direction on the currentVertex when going toward the light node
    auto l = length(exitantDir);
    if(l == 0.f) {
        return 0.f; // avoid NaN bugs
    }
    exitantDir /= l;
    auto sqrDist = sqr(l);

    auto pdfWrtSolidAngle = lightPathVertex->lastVertexBRDF.pdf(lightPathVertex->incidentDirection, exitantDir);
    auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(eyePathVertex->lastVertex.Ns, -exitantDir)) / sqrDist;

    auto p1 = pdfWrtArea;

    auto p2 = eyePathVertex->lastVertexBRDF.pdf(-exitantDir, eyePathVertex->incidentDirection);

    return p1 * (eyePathVertex->dVCM + p2 * eyePathVertex->dVC);
}

float RecursiveMISBDPTRenderer::computeMISWeightRecursive(uint threadID, uint pixelID, uint32_t sampleID,
                       PathVertex* eyePathVertex, PathVertex* lightPathVertex) const {
    auto sum = 1.f;

    sum += computeLightPathWeightRecursive(threadID, pixelID, sampleID, eyePathVertex, lightPathVertex);

    sum += computeEyePathWeightRecursive(threadID, pixelID, sampleID, eyePathVertex, lightPathVertex);

    return 1.f / sum;
}

void RecursiveMISBDPTRenderer::Li(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       uint32_t x, uint32_t y, Vec4f values[8]) const {
    for(auto i = 0u; i < 8; ++i) {
        values[i] += Vec4f(0.f, 0.f, 0.f, 1.f);
    }

    auto s2D = getPixelSample(threadID, sampleID);
    auto primRay = getPrimaryRay(x, y, s2D);

    auto pLightPath = m_PathArray.get() + threadID * (getMaxLightPathDepth() + getMaxEyePathDepth());
    auto pEyePath = pLightPath + getMaxLightPathDepth();

    {
        auto I = getScene().intersect(primRay);

        if(!I) {
            values[FINAL_RENDER] += Vec4f(I.Le, 0.f);
            values[1] += Vec4f(I.Le, 0.f);
            return;
        }

        pEyePath[0].lastVertex = I;
        pEyePath[0].lastVertexBRDF = getScene().shade(pEyePath[0].lastVertex);
        pEyePath[0].incidentDirection = -primRay.dir.xyz();
        pEyePath[0].depth = 1;
        pEyePath[0].pdf = 1.f;
        pEyePath[0].pdfLastVertex = 1.f;
        pEyePath[0].power = Vec3f(1.f);
        //pEyePath[0].dVCM = 1.f / pEyePath[0].pdfLastVertex;
        pEyePath[0].dVCM = 0.f; // put this to 0 because we dont connect to camera
        pEyePath[0].dVC = 0.f;
    }

    uint32_t lightPathLength;
    auto lightNode = sampleLightPath(getScene(), m_EmissionSampler,
                                     threadID, pixelID, pLightPath,
                                     lightPathLength, getMaxLightPathDepth());

    auto L = zero<Vec3f>();

    if(lightNode) {
        auto maxEyePathDepth = getMaxEyePathDepth();
        // trace an eye pathpEyePath[0].lastVertex
        for(auto i = 1u; i <= maxEyePathDepth; ++i) {
            auto k = i - 1u; // index of vertex in pEyePath;

            { // Intersection with light source
                auto totalLength = i;
                if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                    auto weight = computeMISWeightRecursive(threadID, pixelID, sampleID, &pEyePath[k], getScene(), m_EmissionSampler);
                    auto contrib = pEyePath[k].power * pEyePath[k].lastVertex.Le;
                    L += weight * contrib;
                    values[totalLength] += Vec4f(weight * contrib, 0.f);
                }
            }

            { // Direct illumination
                auto totalLength = i + 1;
                if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                    Ray shadowRay;
                    auto G = lightNode->G(pEyePath[k].lastVertex, shadowRay);
                    if(G > 0.f && !getScene().occluded(shadowRay)) {
                        auto contrib = pEyePath[k].power * lightNode->power() * lightNode->Le(-shadowRay.dir)
                                * G * pEyePath[k].lastVertexBRDF.eval(shadowRay.dir, pEyePath[k].incidentDirection);
                        auto weight = computeMISWeightRecursive(threadID, pixelID, sampleID, &pEyePath[k], lightNode.get(), shadowRay.dir);
                        L += weight * contrib;
                        values[totalLength] += Vec4f(weight * contrib, 0.f);
                    }
                }
            }

            for(auto j = 0u; j < lightPathLength; ++j) {
                auto lightPath = pLightPath + j;
                auto totalLength = i + lightPath->depth + 1;

                if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                    Vec3f incidentDirection;
                    float dist;
                    auto G = geometricFactor(pEyePath[k].lastVertex, lightPath->lastVertex, incidentDirection, dist);
                    Ray incidentRay(pEyePath[k].lastVertex, lightPath->lastVertex, incidentDirection, dist);

                    if(G > 0.f) {
                        bool isVisible = !getScene().occluded(incidentRay);

                        if(isVisible) {
                            auto weight = computeMISWeightRecursive(threadID, pixelID, sampleID, &pEyePath[k], lightPath);

                            auto contrib = pEyePath[k].power * lightPath->power * lightPath->lastVertexBRDF.eval(lightPath->incidentDirection, -incidentDirection)
                                    * G * pEyePath[k].lastVertexBRDF.eval(incidentDirection, pEyePath[k].incidentDirection);

                            L += weight * contrib;

                            values[totalLength] += Vec4f(weight * contrib, 0.f);
                        }
                    }
                }
            }

            if(i < maxEyePathDepth) {
                Sample3f wiSample;
                auto fr = pEyePath[k].lastVertexBRDF.sample(pEyePath[k].incidentDirection, wiSample, getFloat2(threadID));

                if(wiSample.pdf > 0.f) {
                    auto throughput = fr * max(0.f, dot(pEyePath[k].lastVertex.Ns, wiSample.value)) / wiSample.pdf;
                    auto I = getScene().intersect(Ray(pEyePath[k].lastVertex, wiSample.value));
                    if(!I) {
                        break;
                    }

                    auto idx = k + 1;

                    pEyePath[idx].lastVertex = I;
                    pEyePath[idx].lastVertexBRDF = getScene().shade(pEyePath[idx].lastVertex);
                    pEyePath[idx].incidentDirection = -wiSample.value;
                    pEyePath[idx].depth = pEyePath[idx - 1].depth + 1;
                    pEyePath[idx].pdfLastVertex = wiSample.pdf * max(0.f, dot(I.Ns, -wiSample.value)) / sqr(I.distance);
                    pEyePath[idx].pdf = pEyePath[idx - 1].pdf * pEyePath[idx].pdfLastVertex;
                    pEyePath[idx].power = pEyePath[idx - 1].power * throughput;

                    if(pEyePath[idx].pdfLastVertex == 0.f) {
                        break;
                    }

                    //if(pEyePath[idx].depth > 1) {
                        auto g = max(0.f, dot(pEyePath[idx - 1].lastVertex.Ns, wiSample.value)) / sqr(pEyePath[idx].lastVertex.distance);
                        float reversePdf = pEyePath[idx - 1].lastVertexBRDF.pdf(wiSample.value, pEyePath[idx - 1].incidentDirection);

                        pEyePath[idx].dVCM = 1.f / pEyePath[idx].pdfLastVertex;
                        pEyePath[idx].dVC = g * (pEyePath[idx - 1].dVCM + pEyePath[idx - 1].dVC * reversePdf) / pEyePath[idx].pdfLastVertex;
//                    } else {
//                        pEyePath[idx].dVCM = 1.f / pEyePath[idx].pdfLastVertex;
//                        pEyePath[idx].dVC = 0.f;
//                    }

                } else {
                    break;
                }
            }
        }
    }


    values[FINAL_RENDER] += Vec4f(L, 0.f);
}

void RecursiveMISBDPTRenderer::preprocess() {
    m_EmissionSampler.initFrame(getScene());

    m_PathArray = makeUniqueArray<PathVertex>(getThreadCount() * (getMaxLightPathDepth() + getMaxEyePathDepth()));
}

void RecursiveMISBDPTRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    auto spp = getSppCount();
    Vec4f L[8];

    TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
        auto pixelID = getPixelIndex(x, y);
        std::fill(L, L + 8, Vec4f(0.f));
        for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
            Li(threadID, pixelID, sampleID, x, y, L);
        }
        for(auto i = 0u; i < 8; ++i) {
            getFramebuffer().accumulate(i, pixelID, L[i]);
        }
    });

    if(m_bDisplayProgress) {
        auto l = debugLock();
        std::cerr << "Tile " << tileID << " / " << getTileCount() << " (" << (tileID * 100.f / getTileCount()) << " %)" << std::endl;
    }
}

void RecursiveMISBDPTRenderer::doExposeIO(TwBar* bar) {
    atb::addVarRW(bar, ATB_VAR(m_nMaxDepth));
    atb::addVarRW(bar, ATB_VAR(m_bDisplayProgress));
}

void RecursiveMISBDPTRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
    getAttribute(xml, "maxDepth", m_nMaxDepth);
    getAttribute(xml, "displayProgress", m_bDisplayProgress);
}

void RecursiveMISBDPTRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
    setAttribute(xml, "maxDepth", m_nMaxDepth);
    setAttribute(xml, "displayProgress", m_bDisplayProgress);
}

}
