#pragma once

#include <bonez/rendering/renderers/TileProcessingRenderer.hpp>
#include <bonez/scene/lights/EmissionSampler.hpp>
#include <bonez/sampling/Random.hpp>
#include <bonez/sys/time.hpp>

namespace BnZ {

class LighttraceRenderer: public TileProcessingRenderer {
public:
    struct PathVertex {
        Intersection lastVertex;
        BRDF lastVertexBRDF;
        Vec3f incidentDirection;
        float pdf; // pdf of the complete path
        float pdfLastVertex; // pdf of the last vertex, conditional to the previous
        Vec3f power;
        uint32_t depth; // number of edges of the path
    };

    std::string getName() const;

    Unique<EmissionVertex> sampleLightPath(
            const Scene& scene, const EmissionSampler& sampler,
            uint32_t threadID, uint32_t pixelID, PathVertex* pBuffer,
            uint32_t& lightPathLength,
            uint32_t maxLightPathDepth) const;

    void beginFrame() override;

    void preprocess() override;

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override;

    void doExposeIO(TwBar* bar) override;

    void doLoadSettings(const tinyxml2::XMLElement& xml) override;

    void doStoreSettings(tinyxml2::XMLElement& xml) const override;

private:
    EmissionSampler m_EmissionSampler;

    Unique<PathVertex[]> m_LightVertexArray;
    Unique<std::vector<PathVertex*>[]> m_LightVertexPerTile; // For each tile, contains the light vertices that project on that tile

    uint32_t m_nMaxPathDepth = 2u;
    uint32_t m_nPathCount = 0u;
    uint32_t m_nLightVertexCount = 0u;
};

}
