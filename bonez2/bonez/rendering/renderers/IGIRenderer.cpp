#include "IGIRenderer.hpp"

namespace BnZ {

std::string IGIRenderer::getName() const {
    return "IGIRenderer";
}

void IGIRenderer::preprocess() {
    m_EmissionSampler.initFrame(getScene());

    getFramebuffer().setChannelCount(1 + m_nMaxPathDepth);
    getFramebuffer().clear();
}

uint32_t IGIRenderer::getMaxLightPathDepth() const {
    return m_nMaxPathDepth - 2;
}

Unique<EmissionVertex> IGIRenderer::sampleLightPath(
        const Scene& scene, const EmissionSampler& sampler,
        uint32_t threadID, uint32_t pixelID, PathVertex* pBuffer,
        uint32_t& lightPathLength,
        uint32_t maxLightPathDepth) const {
    for(PathVertex *it = pBuffer, *end = pBuffer + maxLightPathDepth; it != end; ++it) {
        it->pdf = 0.f;
    }

    auto pEmissionVertex = sampler.sample(scene,
                                          getFloat(threadID),
                                          getFloat(threadID),
                                          getFloat2(threadID));

    if(!pEmissionVertex || !pEmissionVertex->pdf()) {
        return nullptr;
    }

    if(maxLightPathDepth == 0u) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    RaySample ray;
    auto s2D = getFloat2(threadID);
    auto Le_dir = pEmissionVertex->sampleWi(ray, s2D);

    if(!ray.pdf) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    auto I = scene.intersect(ray.value);

    if(!I) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    auto power = pEmissionVertex->getRadiantExitance() * Le_dir / (pEmissionVertex->pdf() * ray.pdf);
    auto pdfLastVertex = pEmissionVertex->pdfWrtArea(I);
    auto pdf = pEmissionVertex->pdf() * pdfLastVertex;
    auto incidentDirection = -ray.value.dir;
    auto brdf = scene.shade(I);
    pBuffer[0].lastVertex = I;
    pBuffer[0].lastVertexBRDF = brdf;
    pBuffer[0].incidentDirection = incidentDirection;
    pBuffer[0].pdf = pdf;
    pBuffer[0].pdfLastVertex = pdfLastVertex;
    pBuffer[0].power = power;
    pBuffer[0].depth = 1u;

    lightPathLength = 1u;

    for(auto length = 2u; length <= maxLightPathDepth; ++length) {
        auto idx = length - 1;

        Sample3f woSample;
        auto fr = brdf.sample(incidentDirection, woSample, getFloat2(threadID));
        if(woSample.pdf > 0.f && fr != zero<Vec3f>()) {
            auto Itmp = scene.intersect(Ray(I, woSample.value));

            if(Itmp) {
                incidentDirection = -woSample.value;
                auto sqrLength = sqr(Itmp.distance);
                pdfLastVertex = woSample.pdf  * max(0.f, dot(Itmp.Ns, incidentDirection)) / sqrLength;
                pdf *= pdfLastVertex;
                power *= max(0.f, dot(I.Ns, woSample.value)) * fr / woSample.pdf;
            } else {
                break;
            }

            if(pdfLastVertex == 0.f) {
                break;
            }

            I = Itmp;
            brdf = scene.shade(I);

            lightPathLength = length;
            pBuffer[idx].lastVertex = I;
            pBuffer[idx].lastVertexBRDF = brdf;
            pBuffer[idx].incidentDirection = incidentDirection;
            pBuffer[idx].pdf = pdf;
            pBuffer[idx].pdfLastVertex = pdfLastVertex;
            pBuffer[idx].power = power;
            pBuffer[idx].depth = length;
        } else {
            break;
        }
    }

    return pEmissionVertex;
}

void IGIRenderer::beginFrame() {
    // Sample all VPLs, allocs buffers

    if(m_nPathCount > 0u && m_nMaxPathDepth > 2u) {
        auto maxLightPathDepth = getMaxLightPathDepth();
        m_VertexBuffer.resize(getMaxLightPathDepth() * m_nPathCount);
        m_LightNodeBuffer.resize(m_nPathCount);

        float scaleCoeff = 1.f / m_nPathCount;

        for(auto i = 0u; i < m_nPathCount; ++i) {
            uint32_t lightPathLength;
            auto pLightPath = m_VertexBuffer.data() + i * maxLightPathDepth;
            m_LightNodeBuffer[i] = sampleLightPath(getScene(), m_EmissionSampler,
                                 0u, 0u, pLightPath,
                                 lightPathLength, maxLightPathDepth);

            for(auto depth = 1u; depth <= maxLightPathDepth; ++depth) {
                auto idx = depth - 1;
                pLightPath[idx].power *= scaleCoeff;
            }
        }
    }
}

void IGIRenderer::processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const {
    for(auto i = 1u; i <= m_nMaxPathDepth; ++i) {
        getFramebuffer().accumulate(i, pixelID, Vec4f(0.f, 0.f, 0.f, 1.f));
    }

    auto s2D = getPixelSample(threadID, sampleID);
    auto ray = getPrimaryRay(x, y, s2D);

    Vec3f L = Vec3f(0.f);

    auto I = getScene().intersect(ray);
    Vec3f wo = -ray.dir.xyz();

    if(!I) {
        L += I.Le;
    } else {
        auto brdf = getScene().shade(I);

        if(m_nMaxPathDepth >= 1u && acceptPathDepth(1u)) {
            L += I.Le;
            getFramebuffer().accumulate(1, pixelID, Vec4f(I.Le, 0.f));
        }

        if(m_nMaxPathDepth >= 2u && acceptPathDepth(2u)) {
            float scaleCoeff = 1.f / m_nPathCount;
            for(const auto& lightNode: m_LightNodeBuffer) {
                Ray shadowRay;
                auto G = lightNode->G(I, shadowRay);
                if(G > 0.f && !getScene().occluded(shadowRay)) {
                    auto contrib = scaleCoeff * lightNode->power() * lightNode->Le(-shadowRay.dir)
                            * G * brdf.eval(shadowRay.dir, wo);
                    L += contrib;
                    getFramebuffer().accumulate(2, pixelID, Vec4f(contrib, 0.f));
                }
            }
        }

        for(const auto& vpl: m_VertexBuffer) {
            if(vpl.pdf) {
                auto pathDepth = vpl.depth + 2u;

                if(acceptPathDepth(pathDepth)) {
                    Vec3f wi;
                    float dist;
                    auto G = geometricFactor(I, vpl.lastVertex, wi, dist);
                    if(G > 0.f) {
                        auto M = brdf.eval(wi, wo) * vpl.lastVertexBRDF.eval(vpl.incidentDirection, -wi);
                        if(M != zero<Vec3f>()) {
                            Ray shadowRay(I, vpl.lastVertex, wi, dist);
                            if(!getScene().occluded(shadowRay)) {
                                auto contrib = M * G * vpl.power;
                                L += M * G * vpl.power;
                                getFramebuffer().accumulate(pathDepth, pixelID, Vec4f(contrib, 0.f));
                            }
                        }
                    }
                }
            }
        }
    }

    getFramebuffer().accumulate(0, pixelID, Vec4f(L, 1.f));
}

void IGIRenderer::doExposeIO(TwBar* bar) {
    atb::addVarRW(bar, ATB_VAR(m_nMaxPathDepth));
    atb::addVarRW(bar, ATB_VAR(m_nPathCount));
}

void IGIRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
    getAttribute(xml, "maxDepth", m_nMaxPathDepth);
    getAttribute(xml, "pathCount", m_nPathCount);
}

void IGIRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
    setAttribute(xml, "maxDepth", m_nMaxPathDepth);
    setAttribute(xml, "pathCount", m_nPathCount);
}

void IGIRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    processTileSamples(viewport, [this, threadID](uint32_t x, uint32_t y, uint32_t pixelID, uint32_t sampleID) {
        processSample(threadID, pixelID, sampleID, x, y);
    });
}

}
