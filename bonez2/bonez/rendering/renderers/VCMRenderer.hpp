#pragma once

#include <bonez/rendering/renderers/TileProcessingRenderer.hpp>
#include <bonez/sampling/multiple_importance_sampling.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/sampling/distribution1d.h>
#include <bonez/sampling/shapes.hpp>
#include <bonez/sampling/patterns.hpp>
#include <bonez/scene/lights/EmissionSampler.hpp>

#include <bonez/utils/HashGrid.hpp>

namespace BnZ {

class VCMRenderer: public TileProcessingRenderer {
    struct PathVertex {
        Intersection lastVertex;
        BRDF lastVertexBRDF;
        Vec3f incidentDirection;
        float pdf; // pdf of the complete path
        float pdfLastVertex; // pdf of the last vertex, conditional to the previous
        Vec3f power;
        uint32_t depth; // number of edges of the path

        float dVCM;
        float dVC;
        float dVM;
    };
public:
    VCMRenderer();

    virtual std::string getName() const;

    uint32_t getMaxLightPathDepth() const;

    uint32_t getMaxEyePathDepth() const;

    std::string getAlgorithmType() const;

    void setAlgorithmType(const std::string& algorithmType);

    void processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       uint32_t x, uint32_t y) const;

    void preprocess() override;

    void beginFrame() override;

    void endFrame() override;

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override;

    virtual void doExposeIO(TwBar* bar);

    virtual void doLoadSettings(const tinyxml2::XMLElement& xml);

    virtual void doStoreSettings(tinyxml2::XMLElement& xml) const;

    inline float Mis(float pdf) const {
        return pdf; // balance heuristic
    }

private:
    EmissionSampler m_EmissionSampler;

    Unique<Unique<EmissionVertex>[]> m_LightNodeArray;
    Unique<PathVertex[]> m_LightPathArray;
    Unique<std::vector<PathVertex*>[]> m_LightVertexPerTile; // For each tile, contains the light vertices that project on that tile
    uint32_t m_nLightPathVertexCount;

    Unique<PathVertex[]> m_EyePathArray;

    HashGrid m_HashGrid;

    enum AlgorithmType
    {
        // light vertices contribute to camera,
        // No MIS weights (dVCM, dVM, dVC all ignored)
        kLightTrace = 0,

        // Camera and light vertices merged on first non-specular surface from camera.
        // Cannot handle mixed specular + non-specular materials.
        // No MIS weights (dVCM, dVM, dVC all ignored)
        kPpm,

        // Camera and light vertices merged on along full path.
        // dVCM and dVM used for MIS
        kBpm,

        // Standard bidirectional path tracing
        // dVCM and dVC used for MIS
        kBpt,

        // Vertex connection and mering
        // dVCM, dVM, and dVC used for MIS
        kVcm
    };

    // User parameters
    AlgorithmType m_AlgorithmType = kVcm;
    uint32_t m_nMaxDepth = 3;
    bool m_bDisplayProgress = false;

    // Precomputed parameters
    bool  m_UseVM;             // Vertex merging (of some form) is used
    bool  m_UseVC;             // Vertex connection (BPT) is used
    bool  m_LightTraceOnly;    // Do only light tracing
    bool  m_Ppm;               // Do PPM, same terminates camera after first merge

    float m_SceneRadius;
    float m_RadiusAlpha;       // Radius reduction rate parameter
    float m_RadiusFactor;
    float m_BaseRadius;        // Initial merging radius
    float m_MisVmWeightFactor; // Weight of vertex merging (used in VC)
    float m_MisVcWeightFactor; // Weight of vertex connection (used in VM)
    float m_ScreenPixelCount;  // Number of pixels
    float m_LightSubPathCount; // Number of light sub-paths
    float m_VmNormalization;   // 1 / (Pi * radius^2 * light_path_count)

    uint32_t m_nIterationCount = 0u;

    void connectLightVerticesToCamera(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const;

    void vertexMerging(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, const PathVertex& eyeVertex) const;

    void vertexConnection(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                          const Unique<EmissionVertex>& pEmissionVertex, const PathVertex* pLightPath,
                          const PathVertex& eyeVertex) const;
    float computeLightPathWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                 const PathVertex& eyePathVertex, const PathVertex& lightPathVertex) const;

    float computeEyePathWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                               const PathVertex& eyePathVertex, const PathVertex& lightPathVertex) const;

    float computeMISWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                           const PathVertex& eyePathVertex, const PathVertex& lightPathVertex) const;

    float computeMISWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                           const PathVertex& eyePathVertex, const Scene& scene, const EmissionSampler& sampler) const;

    float computeMISWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                    const PathVertex& eyePathVertex,
                                    const EmissionVertex* lightNode,
                                    const Vec3f& dirToLight) const;

    Unique<EmissionVertex> sampleLightPath(
            const Scene& scene, const EmissionSampler& sampler,
            uint32_t threadID, uint32_t pixelID, PathVertex* pBuffer,
            uint32_t& lightPathLength,
            uint32_t maxLightPathDepth);

public:
    friend const Vec3f& getPosition(const PathVertex& pathVertex) {
        return pathVertex.lastVertex.P;
    }

    friend bool isValid(const PathVertex& pathVertex) {
        return pathVertex.pdf > 0.f;
    }
};

}
