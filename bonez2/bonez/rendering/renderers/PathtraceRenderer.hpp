#pragma once

#include "TileProcessingRenderer.hpp"
#include <bonez/scene/lights/EmissionSampler.hpp>

namespace BnZ {

class PathtraceRenderer: public TileProcessingRenderer {
public:
    EmissionSampler m_EmissionSampler;

    uint32_t m_nMaxPathDepth = 2;

    std::string getName() const override {
        return "PathtraceRenderer";
    }

    void preprocess() override {
        m_EmissionSampler.initFrame(getScene());
    }

    void processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const {
        for(auto i = 0u; i <= m_nMaxPathDepth; ++i) {
            getFramebuffer().accumulate(i, pixelID, Vec4f(0.f, 0.f, 0.f, 1.f));
        }

        auto s2D = getPixelSample(threadID, sampleID);
        auto ray = getPrimaryRay(x, y, s2D);

        Vec3f throughput = Vec3f(1.f);
        Vec3f L = Vec3f(0.f);

        auto I = getScene().intersect(ray);
        Vec3f wo = -ray.dir.xyz();

        if(acceptPathDepth(1)) {
            getFramebuffer().accumulate(1, pixelID, Vec4f(I.Le, 0.f));
            L += I.Le;
        }

        auto lightNode = m_EmissionSampler.sample(getScene(), getFloat(threadID), getFloat(threadID), getFloat2(threadID));

        for(auto i = 1u; I && i < m_nMaxPathDepth; ++i) {
            auto brdf = getScene().shade(I);
            if(acceptPathDepth(i + 1)) {
                Ray shadowRay;
                auto G = lightNode->G(I, shadowRay);
                if(G > 0.f && !getScene().occluded(shadowRay)) {
                    auto contrib = throughput * lightNode->power() * lightNode->Le(-shadowRay.dir)
                            * G * brdf.eval(shadowRay.dir, wo);
                    getFramebuffer().accumulate(i + 1, pixelID, Vec4f(contrib, 0.f));
                    L += contrib;
                }
            }

            if(i < m_nMaxPathDepth - 1) {
                Sample3f wi;
                auto fr = brdf.sample(wo, wi, getFloat2(threadID));
                if(fr != zero<Vec3f>() && wi.pdf > 0.f) {
                    throughput *= fr * dot(I.Ns.xyz(), wi.value) / wi.pdf;
                    I = getScene().intersect(secondaryRay(I, wi.value));
                    wo = -wi.value;
                }
            }
        }

        getFramebuffer().accumulate(0, pixelID, Vec4f(L, 0.f));
    }

    void doExposeIO(TwBar* bar) override {
        atb::addVarRW(bar, ATB_VAR(m_nMaxPathDepth));
    }

    void doLoadSettings(const tinyxml2::XMLElement& xml) override {
        getAttribute(xml, "maxDepth", m_nMaxPathDepth);
    }

    void doStoreSettings(tinyxml2::XMLElement& xml) const override {
        setAttribute(xml, "maxDepth", m_nMaxPathDepth);
    }

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override {
        processTileSamples(viewport, [this, threadID](uint32_t x, uint32_t y, uint32_t pixelID, uint32_t sampleID) {
            processSample(threadID, pixelID, sampleID, x, y);
        });
    }
};


}
