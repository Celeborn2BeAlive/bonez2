#include "LighttraceRenderer.hpp"

namespace BnZ {

std::string LighttraceRenderer::getName() const {
    return "LighttraceRenderer";
}

Unique<EmissionVertex> LighttraceRenderer::sampleLightPath(
        const Scene& scene, const EmissionSampler& sampler,
        uint32_t threadID, uint32_t pixelID, PathVertex* pBuffer,
        uint32_t& lightPathLength,
        uint32_t maxLightPathDepth) const {
    for(PathVertex *it = pBuffer, *end = pBuffer + maxLightPathDepth; it != end; ++it) {
        it->pdf = 0.f;
    }

    auto pEmissionVertex = sampler.sample(scene,
                                          getFloat(threadID),
                                          getFloat(threadID),
                                          getFloat2(threadID));

    if(!pEmissionVertex || !pEmissionVertex->pdf()) {
        return nullptr;
    }

    if(maxLightPathDepth == 0u) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    RaySample ray;
    auto s2D = getFloat2(threadID);
    auto Le_dir = pEmissionVertex->sampleWi(ray, s2D);

    if(!ray.pdf) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    auto I = scene.intersect(ray.value);

    if(!I) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    auto power = pEmissionVertex->getRadiantExitance() * Le_dir / (pEmissionVertex->pdf() * ray.pdf);
    auto pdfLastVertex = pEmissionVertex->pdfWrtArea(I);
    auto pdf = pEmissionVertex->pdf() * pdfLastVertex;
    auto incidentDirection = -ray.value.dir;
    auto brdf = scene.shade(I);
    pBuffer[0].lastVertex = I;
    pBuffer[0].lastVertexBRDF = brdf;
    pBuffer[0].incidentDirection = incidentDirection;
    pBuffer[0].pdf = pdf;
    pBuffer[0].pdfLastVertex = pdfLastVertex;
    pBuffer[0].power = power;
    pBuffer[0].depth = 1u;

    lightPathLength = 1u;

    for(auto length = 2u; length <= maxLightPathDepth; ++length) {
        auto idx = length - 1;

        Sample3f woSample;
        auto fr = brdf.sample(incidentDirection, woSample, getFloat2(threadID));
        if(woSample.pdf > 0.f && fr != zero<Vec3f>()) {
            auto Itmp = scene.intersect(Ray(I, woSample.value));

            if(Itmp) {
                incidentDirection = -woSample.value;
                auto sqrLength = sqr(Itmp.distance);
                pdfLastVertex = woSample.pdf  * max(0.f, dot(Itmp.Ns, incidentDirection)) / sqrLength;
                pdf *= pdfLastVertex;
                power *= max(0.f, dot(I.Ns, woSample.value)) * fr / woSample.pdf;



            } else {
                break;
            }

            if(pdfLastVertex == 0.f) {
                break;
            }

            I = Itmp;
            brdf = scene.shade(I);

            lightPathLength = length;
            pBuffer[idx].lastVertex = I;
            pBuffer[idx].lastVertexBRDF = brdf;
            pBuffer[idx].incidentDirection = incidentDirection;
            pBuffer[idx].pdf = pdf;
            pBuffer[idx].pdfLastVertex = pdfLastVertex;
            pBuffer[idx].power = power;
            pBuffer[idx].depth = length;
        } else {
            break;
        }
    }

    return pEmissionVertex;
}

void LighttraceRenderer::preprocess() {
    m_nPathCount = getFramebuffer().getPixelCount() * getSppCount();
    m_nLightVertexCount = m_nPathCount * m_nMaxPathDepth;
    m_LightVertexArray = makeUniqueArray<PathVertex>(m_nLightVertexCount);
    m_LightVertexPerTile = makeUniqueArray<std::vector<PathVertex*>>(getTileCount());

    getFramebuffer().setChannelCount(1 + m_nMaxPathDepth);
}

void LighttraceRenderer::beginFrame() {
    m_EmissionSampler.initFrame(getScene());

    // Sample a light path for each pixel
    auto spp = getSppCount();
    TileProcessingRenderer::processTiles([&](uint32_t threadID, uint32_t tileID, const Vec4u& viewport) {
        TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
            auto pixelID = getPixelIndex(x, y);
            auto lightPathOffset = pixelID * spp * m_nMaxPathDepth;
            for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
                auto pLightPath = m_LightVertexArray.get() + lightPathOffset + sampleID * m_nMaxPathDepth;
                uint32_t lightPathDepth;
                sampleLightPath(getScene(), m_EmissionSampler,
                                threadID, pixelID, pLightPath,
                                lightPathDepth, m_nMaxPathDepth);
            }
        });
    });

    auto tileSize = getTileSize();

    std::for_each(m_LightVertexPerTile.get(), m_LightVertexPerTile.get() + getTileCount(), [](std::vector<PathVertex*>& v) {
        v.clear();
    });

    const auto& framebuffer = getFramebuffer();

    auto pEnd = m_LightVertexArray.get() + m_nLightVertexCount;
    for(auto pLightVertex = m_LightVertexArray.get(); pLightVertex != pEnd; ++pLightVertex) {
        Vec2f ndc;
        if(getCamera().getNDC(pLightVertex->lastVertex.P, ndc)) {
            auto uv = ndcToUV(ndc);
            auto pixel = getPixel(uv, framebuffer.getSize());

            auto tile = pixel / tileSize;
            auto tileID = getTileID(tile);

            m_LightVertexPerTile[tileID].emplace_back(pLightVertex);
        }
    }
}

void LighttraceRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    const auto& camera = getCamera();
    const auto& scene = getScene();
    auto& framebuffer = getFramebuffer();

    auto viewportRange = Vec4u(viewport.xy(), viewport.xy() + viewport.zw());
    auto rcpPathCount = 1.f / m_nPathCount;

    TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
        auto pixelID = getPixelIndex(x, y);

        for(auto i = 0u; i < getFramebuffer().getChannelCount(); ++i) {
            framebuffer.accumulate(i, pixelID, Vec4f(0.f, 0.f, 0.f, 1));
        }

        if(acceptPathDepth(1u) && 1u <= m_nMaxPathDepth) {
            for(auto sampleID = 0u; sampleID < getSppCount(); ++sampleID) {
                auto s2D = getPixelSample(threadID, sampleID);
                auto primaryRay = getPrimaryRay(x, y, s2D);

                auto I = scene.intersect(primaryRay);

                if(I) {
                    framebuffer.accumulate(0u, pixelID, Vec4f(I.Le, 0));
                    framebuffer.accumulate(1u, pixelID, Vec4f(I.Le, 0));
                }
            }
        }
    });

    for(auto pLightVertex: m_LightVertexPerTile[tileID]) {
        if(!pLightVertex->pdf) {
            continue;
        }

        auto totalDepth = 1 + pLightVertex->depth;
        if(!acceptPathDepth(totalDepth) || totalDepth > m_nMaxPathDepth) {
            continue;
        }

        Vec2f ndc;
        if(camera.getNDC(pLightVertex->lastVertex.P, ndc)) {
            auto uv = ndcToUV(ndc);
            auto pixel = getPixel(uv, framebuffer.getSize());

            auto pixelID = BnZ::getPixelIndex(pixel, framebuffer.getSize());

            Ray shadowRay;
            auto surfaceToImageFactor =
                    camera.surfaceToImageFactor(pLightVertex->lastVertex,
                                                shadowRay);
            auto fr = pLightVertex->lastVertexBRDF.eval(pLightVertex->incidentDirection, shadowRay.dir);

            if(surfaceToImageFactor > 0.f && fr != zero<Vec3f>()) {
                if(!scene.occluded(shadowRay)) {
                    auto imageToSurfaceFactor = 1.f / surfaceToImageFactor;
                    framebuffer.accumulate(0u, pixelID, Vec4f(rcpPathCount * pLightVertex->power * fr * imageToSurfaceFactor, 0.f));
                    framebuffer.accumulate(totalDepth, pixelID, Vec4f(rcpPathCount * pLightVertex->power * fr * imageToSurfaceFactor, 0.f));
                }
            }
        }
    }
}

void LighttraceRenderer::doExposeIO(TwBar* bar) {
    atb::addVarRW(bar, ATB_VAR(m_nPathDepthMask));
    atb::addVarRW(bar, ATB_VAR(m_nMaxPathDepth));
    atb::addVarRO(bar, ATB_VAR(m_nPathCount));
}

void LighttraceRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
    getAttribute(xml, "maxDepth", m_nMaxPathDepth);
}

void LighttraceRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
    setAttribute(xml, "maxDepth", m_nMaxPathDepth);
}

}
