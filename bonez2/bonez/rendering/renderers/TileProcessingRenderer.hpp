#pragma once

#include <bonez/rendering/Renderer.hpp>

namespace BnZ {

class TileProcessingRenderer: public Renderer {
private:
    static const uint32_t s_nTileWidth = 32u;
    static const uint32_t s_nTileHeight = 32u;
    static const uint32_t s_nTileSize = s_nTileWidth * s_nTileHeight;

    Vec2u m_TileSize = Vec2u(32u, 32u);
    Vec2u m_TileCount;

    float m_fInitFrameTime = 0.f;
    float m_fTileProcessingTime = 0.f;
    float m_fEndFrameTime = 0.f;
    float m_fSumProcessingTime = 0.f;
    float m_fMeanProcessingTime = 0.f;
    float m_fWorstProcessingTime = 0.f;
    float m_fBestProcessingTime = 0.f;
    uint32_t m_nFrameCount = 0;

    uint32_t m_nSeed = 0u;
    Vec2u m_Spp = Vec2u(1);
    uint32_t m_nSpp = 0;

    Vec2u m_FramebufferSize;

    uint32_t m_nThreadCount = BnZ::getThreadCount();
    mutable std::vector<RandomGenerator> m_Rngs; // For each thread, a random sequence

    void resetStats();

    uint32_t m_nTileCount = 0u;

protected:
    uint32_t m_nTotalSpp = 0;

    const Vec2u getTileSize() const {
        return m_TileSize;
    }

    uint32_t getThreadCount() const {
        return m_nThreadCount;
    }

    uint32_t getSeed() const {
        return m_nSeed;
    }

    Vec2u getSpp() const {
        return m_Spp;
    }

    uint32_t getSppCount() const {
        return m_nSpp;
    }

    uint32_t getTileCount() const {
        return m_nTileCount;
    }

    // Process each sample of each pixel of a tile with a specific task
    template<typename Task>
    void processTileSamples(const Vec4u& viewport, Task&& task) const {
        auto xEnd = viewport.x + viewport.z;
        auto yEnd = viewport.y + viewport.w;

        for(auto y = viewport.y; y < yEnd; ++y) {
            for(auto x = viewport.x; x < xEnd; ++x) {
                uint32_t pixelID = getPixelIndex(x, y);
                for(auto sampleID = 0u; sampleID < m_nSpp; ++sampleID) {
                    task(x, y, pixelID, sampleID);
                }
            }
        }
    }

    Vec2f getPixelSample(uint32_t threadID, uint32_t sampleID) const {
        return getFloat2(threadID);
    }

    Vec2f getFloat2(uint32_t threadID) const {
        return Vec2f(getFloat(threadID), getFloat(threadID));
    }

//    mutable std::vector<float> m_RngF = {
//        0.218091875, 0.159590751, 0.182594478, 0.227520332, 0.848888338, 0.343986630, 0.490491748, 0.464008540, 0.038450919, 0.544758141, 0.738217175, 0.335322112, 0.009103555, 0.776305377, 0.412564307, 0.476443708, 0.985185742, 0.501401484, 0.378091156, 0.104217850, 0.935098946, 0.119338825
//    };

//    mutable std::vector<float> m_RngF;
//    mutable uint32_t IDX = 0u;

    float getFloat(uint32_t threadID) const {
//        return m_RngF[IDX++];

//        auto v = m_Rngs[threadID].getFloat();
//        m_RngF.push_back(v);
//        return v;

        return m_Rngs[threadID].getFloat();
    }

    uint64_t getRandomCallCount(uint32_t threadID) const {
        return m_Rngs[threadID].getCallCount();
    }

    void discardRandom(uint32_t threadID, uint64_t callCount) const {
        m_Rngs[threadID].discard(callCount);
    }

    uint32_t getRandomSeed(uint32_t threadID) const {
        return m_Rngs[threadID].getSeed();
    }

    void setRandomSeed(uint32_t threadID, uint32_t seed) const {
        return m_Rngs[threadID].setSeed(seed);
    }

    // Do once during initialization
    // Scene, Camera and Framebuffer are not supposed to change until next call to this method
    virtual void preprocess() {
    }

    // Called before the processing of a frame
    virtual void beginFrame() {
    }

    // Process a tile
    virtual void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const = 0;

    // Called after the processing of a frame
    virtual void endFrame() {
    }

    virtual void doExposeIO(TwBar* bar) {
    }

    virtual void doLoadSettings(const tinyxml2::XMLElement& xml) {
    }

    virtual void doStoreSettings(tinyxml2::XMLElement& xml) const {
    }

    void doInit() override;

    void doRender() override;

    virtual void doStoreStatistics() const {
        // Do nothing by default
    }

    uint32_t getTileID(const Vec2u& tile) const {
        return tile.x + tile.y * m_TileCount.x;
    }

    template<typename TileProcessingFunc>
    void processTiles(const TileProcessingFunc& fun) {
        auto task = [&](uint32_t threadID) {
            auto loopID = 0u;
            while(true) {
                auto tileID = loopID * m_nThreadCount + threadID;
                ++loopID;

                if(tileID >= m_nTileCount) {
                    return;
                }

                uint32_t tileX = tileID % m_TileCount.x;
                uint32_t tileY = tileID / m_TileCount.x;

                Vec2u tileOrg = Vec2u(tileX, tileY) * m_TileSize;
                auto viewport = Vec4u(tileOrg, m_TileSize);

                if(viewport.x + viewport.z > m_FramebufferSize.x) {
                    viewport.z = m_FramebufferSize.x - viewport.x;
                }

                if(viewport.y + viewport.w > m_FramebufferSize.y) {
                    viewport.w = m_FramebufferSize.y - viewport.y;
                }

                fun(threadID, tileID, viewport);
            }
        };

        launchThreads(task);
    }

public:
    // Process each pixel of a tile with a specific task
    template<typename Task>
    void processTilePixels(const Vec4u& viewport, Task&& task) const {
        auto xEnd = viewport.x + viewport.z;
        auto yEnd = viewport.y + viewport.w;

        for(auto y = viewport.y; y < yEnd; ++y) {
            for(auto x = viewport.x; x < xEnd; ++x) {
                task(x, y);
            }
        }
    }

    virtual ~TileProcessingRenderer() {
    }

    virtual std::string getName() const = 0;

    void exposeIO(TwBar* bar) override;

    void loadSettings(const tinyxml2::XMLElement& xml) override;

    void storeSettings(tinyxml2::XMLElement& xml) const override;

    void storeStatistics() override final;
};

}
