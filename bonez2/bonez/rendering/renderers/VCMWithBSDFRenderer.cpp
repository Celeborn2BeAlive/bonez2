#include "VCMWithBSDFRenderer.hpp"
#include <bonez/scene/lights/AreaLight.hpp>

namespace BnZ {

VCMWithBSDFRenderer::VCMWithBSDFRenderer() {
}

std::string VCMWithBSDFRenderer::getName() const {
    return "VCMWithBSDFRenderer";
}

uint32_t VCMWithBSDFRenderer::getMaxLightPathDepth() const {
    return m_nMaxDepth - 1;
}

uint32_t VCMWithBSDFRenderer::getMaxEyePathDepth() const {
    return m_nMaxDepth;
}

bool VCMWithBSDFRenderer::sampleScattering(uint32_t threadID, uint32_t pixelID,
                                                        const PathVertex& currentVertex, PathVertex& nextVertex,
                                                        bool sampleAdjoint) const {
    Sample3f woSample;
    float cosThetaOutDir;
    uint32_t sampledEvent;
    auto fs = currentVertex.lastVertexBSDF.sample(Vec3f(getFloat(threadID), getFloat2(threadID)), woSample, cosThetaOutDir, &sampledEvent, sampleAdjoint);

    if(woSample.pdf == 0.f || fs == zero<Vec3f>()) {
        return false;
    }

    auto I = getScene().intersect(Ray(currentVertex.lastVertex, woSample.value));

    if(!I) {
        return false;
    }

    auto incidentDirection = -woSample.value;
    auto sqrLength = sqr(I.distance);
    auto pdfLastVertex = woSample.pdf  * abs(dot(I.Ns, incidentDirection)) / sqrLength;

    if(pdfLastVertex == 0.f) {
        return false;
    }

    nextVertex.lastVertex = I;
    nextVertex.lastVertexBSDF = BSDF(incidentDirection, I, getScene());
    nextVertex.pdf = currentVertex.pdf * pdfLastVertex;
    nextVertex.pdfLastVertex = pdfLastVertex;
    nextVertex.power = currentVertex.power * abs(cosThetaOutDir) * fs / woSample.pdf;
    nextVertex.depth = currentVertex.depth + 1;

    auto g = abs(cosThetaOutDir) / sqrLength;

    if((sampledEvent & ScatteringEvent::Specular) && currentVertex.lastVertexBSDF.isDelta()) {
        nextVertex.dVCM = 0.f;
        nextVertex.dVC = Mis(g) * currentVertex.dVC;
        nextVertex.dVC = Mis(g) * currentVertex.dVM;
    } else {
        float reversePdf = currentVertex.lastVertexBSDF.pdf(woSample.value, true);

        nextVertex.dVCM = Mis(1.f / nextVertex.pdfLastVertex);
        nextVertex.dVC = Mis(g / nextVertex.pdfLastVertex) *
                (m_MisVmWeightFactor +
                 currentVertex.dVCM +
                 Mis(reversePdf) * currentVertex.dVC);
        nextVertex.dVM = Mis(g / nextVertex.pdfLastVertex) *
                (1.f +
                 currentVertex.dVCM * m_MisVcWeightFactor +
                 Mis(reversePdf) * currentVertex.dVM);
    }

    return true;
}

Unique<EmissionVertex> VCMWithBSDFRenderer::sampleLightPath(
        const Scene& scene, const EmissionSampler& sampler,
        uint32_t threadID, uint32_t pixelID, PathVertex* pBuffer,
        uint32_t& lightPathLength,
        uint32_t maxLightPathDepth) {
    for(PathVertex *it = pBuffer, *end = pBuffer + maxLightPathDepth; it != end; ++it) {
        it->pdf = 0.f;
    }

    auto pEmissionVertex = sampler.sample(scene,
                                          getFloat(threadID),
                                          getFloat(threadID),
                                          getFloat2(threadID));

    if(!pEmissionVertex || !pEmissionVertex->pdf()) {
        return nullptr;
    }

    if(maxLightPathDepth == 0u) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    RaySample ray;
    auto s2D = getFloat2(threadID);
    auto Le_dir = pEmissionVertex->sampleWi(ray, s2D);

    if(!ray.pdf) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    auto I = scene.intersect(ray.value);

    if(!I) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    auto power = pEmissionVertex->getRadiantExitance() * Le_dir / (pEmissionVertex->pdf() * ray.pdf);
    auto pdfLastVertex = pEmissionVertex->pdfWrtArea(I);
    auto pdf = pEmissionVertex->pdf() * pdfLastVertex;
    auto incidentDirection = -ray.value.dir;

    pBuffer[0].lastVertex = I;
    pBuffer[0].lastVertexBSDF = BSDF(incidentDirection, pBuffer[0].lastVertex, getScene());
    pBuffer[0].pdf = pdf;
    pBuffer[0].pdfLastVertex = pdfLastVertex;
    pBuffer[0].power = power;
    pBuffer[0].depth = 1u;

    pBuffer[0].dVCM = Mis(1.f / pBuffer[0].pdfLastVertex);
    pBuffer[0].dVC = Mis(pEmissionVertex->g(pBuffer[0].lastVertex) / (pEmissionVertex->pdf() * pBuffer[0].pdfLastVertex));
    pBuffer[0].dVM = pBuffer[0].dVC * m_MisVcWeightFactor;

    lightPathLength = 1u;

    for(auto length = 2u; length <= maxLightPathDepth; ++length) {
        auto idx = length - 1;

        if(!sampleScattering(threadID, pixelID, pBuffer[idx - 1], pBuffer[idx], true)) {
            break;
        }

        ++lightPathLength;
    }

    return pEmissionVertex;
}

void VCMWithBSDFRenderer::vertexMerging(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, const PathVertex& eyeVertex) const {
    auto& framebuffer = getFramebuffer();

    auto vmContrib = zero<Vec3f>();
    auto query = [&](const PathVertex& lightPathVertex) {
        // Reject if full path length below/above min/max path length
        auto totalDepth = lightPathVertex.depth + eyeVertex.depth;
        if(!acceptPathDepth(totalDepth) || totalDepth > m_nMaxDepth) {
             return;
        }

        // Retrieve light incoming direction in world coordinates
        const Vec3f lightDirection = lightPathVertex.lastVertexBSDF.getIncidentDirection();

//        if(dot(lightDirection, pEyePath[k].incidentDirection) < 0.f) {
//            return;
//        }

        if(dot(lightDirection, eyeVertex.lastVertex.Ns) < 0.f) {
            return;
        }

        const Vec3f fr = eyeVertex.lastVertexBSDF.eval(lightDirection);

        auto brdfDirPdf = eyeVertex.lastVertexBSDF.pdf(lightDirection);
        auto brdfRevPdf = eyeVertex.lastVertexBSDF.pdf(lightDirection, true);

        if(fr == zero<Vec3f>())
            return;

        // Partial light sub-path MIS weight [tech. rep. (38)]
        const float wLight = lightPathVertex.dVCM * m_MisVcWeightFactor +
            lightPathVertex.dVM * Mis(brdfDirPdf);

        // Partial eye sub-path MIS weight [tech. rep. (39)]
        const float wCamera = eyeVertex.dVCM * m_MisVcWeightFactor +
            eyeVertex.dVM * Mis(brdfRevPdf);

        // Full path MIS weight [tech. rep. (37)]. No MIS for PPM
        const float misWeight = m_Ppm ?
            1.f :
            1.f / (wLight + 1.f + wCamera);

        auto contrib = misWeight * fr * lightPathVertex.power;
        vmContrib += contrib;

        framebuffer.accumulate(totalDepth, pixelID, Vec4f(eyeVertex.power * m_VmNormalization * contrib, 0.f));
    };

    m_HashGrid.Process(m_LightPathArray.get(), getPosition(eyeVertex), query);
    framebuffer.accumulate(0u, pixelID, Vec4f(eyeVertex.power * m_VmNormalization * vmContrib, 0.f));
}

void VCMWithBSDFRenderer::vertexConnection(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y,
                                const Unique<EmissionVertex>& pEmissionVertex, const PathVertex* pLightPath,
                                const PathVertex& eyeVertex) const {
    auto& framebuffer = getFramebuffer();

    {   // Direct illumination
        auto totalLength = eyeVertex.depth + 1;
        if(pEmissionVertex && acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
            computeDirectIllumination(threadID, pixelID, sampleID, x, y, eyeVertex, *pEmissionVertex);
        }
    }

    auto lightPathDepth = getMaxLightPathDepth();
    // Connection with light vertices
    for(auto j = 0u; j < lightPathDepth; ++j) {
        auto lightPath = pLightPath + j;
        auto totalLength = eyeVertex.depth + lightPath->depth + 1;

        if(lightPath->pdf > 0.f && acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
            connectVertices(threadID, pixelID, sampleID, x, y, eyeVertex, *lightPath);
        }
    }
}

void VCMWithBSDFRenderer::computeEmittedRadiance(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                                              uint32_t x, uint32_t y, const PathVertex& eyeVertex) const {
    if(eyeVertex.lastVertex.Le == zero<Vec3f>()) {
        return;
    }

    auto contrib = eyeVertex.power * eyeVertex.lastVertex.Le;

    auto rcpWeight = 1.f;
    if(eyeVertex.depth > 1u) {
        if(!eyeVertex.lastVertex) {
            // Hit on environment map (if present) should be handled here

        } else {
            auto meshID = eyeVertex.lastVertex.meshID;
            auto lightID = getScene().getGeometry().getMesh(meshID).m_nLightID;

            const auto& pLight = getScene().getLightContainer().getLight(lightID);
            const auto* pAreaLight = static_cast<const AreaLight*>(pLight.get());

            float pointPdfWrtArea, directionPdfWrtSolidAngle;
            pAreaLight->pdf(eyeVertex.lastVertex, eyeVertex.lastVertexBSDF.getIncidentDirection(), getScene(),
                            pointPdfWrtArea, directionPdfWrtSolidAngle);

            pointPdfWrtArea *= m_EmissionSampler.pdf(lightID);

            rcpWeight += Mis(pointPdfWrtArea) * (eyeVertex.dVCM + Mis(directionPdfWrtSolidAngle) * eyeVertex.dVC);
        }
    }

    auto weight = 1.f / rcpWeight;

    auto weightedContrib = weight * contrib;
    getFramebuffer().accumulate(0, pixelID, Vec4f(weightedContrib, 0));
    getFramebuffer().accumulate(eyeVertex.depth, pixelID, Vec4f(weightedContrib, 0));
}

void VCMWithBSDFRenderer::computeDirectIllumination(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                                                 uint32_t x, uint32_t y, const PathVertex& eyeVertex,
                                                                 const EmissionVertex& lightVertex) const {
    Ray shadowRay;
    auto G = lightVertex.G(eyeVertex.lastVertex, shadowRay);

    float cosAtEyeVertex;
    float eyeDirPdf, eyeRevPdf;

    auto fr = eyeVertex.lastVertexBSDF.eval(shadowRay.dir, cosAtEyeVertex, &eyeDirPdf, &eyeRevPdf);
    if(G > 0.f && fr != zero<Vec3f>() && !getScene().occluded(shadowRay)) {
        auto contrib = eyeVertex.power * lightVertex.power() * lightVertex.Le(-shadowRay.dir) * G * fr;

        auto rcpWeight = 1.f;

        {
           auto pdfWrtArea = eyeDirPdf * lightVertex.g(eyeVertex.lastVertex);
           rcpWeight += Mis(pdfWrtArea / lightVertex.pdf());
        }

        {
            auto pdfWrtArea = lightVertex.pdfWrtArea(eyeVertex.lastVertex);
            rcpWeight += Mis(pdfWrtArea) * (m_MisVmWeightFactor + eyeVertex.dVCM + Mis(eyeRevPdf) * eyeVertex.dVC);
        }

        auto weight = 1.f / rcpWeight;

        auto weightedContrib = weight * contrib;
        getFramebuffer().accumulate(0, pixelID, Vec4f(weightedContrib, 0));
        getFramebuffer().accumulate(eyeVertex.depth + 1, pixelID, Vec4f(weightedContrib, 0));
    }
}

void VCMWithBSDFRenderer::connectVertices(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                                       uint32_t x, uint32_t y, const PathVertex& eyeVertex,
                                                       const PathVertex& lightVertex) const {
    Vec3f incidentDirection;
    float dist;
    auto G = geometricFactor(eyeVertex.lastVertex, lightVertex.lastVertex, incidentDirection, dist);
    Ray incidentRay(eyeVertex.lastVertex, lightVertex.lastVertex, incidentDirection, dist);

    float cosAtLightVertex, cosAtEyeVertex;
    float lightDirPdf, lightRevPdf;
    float eyeDirPdf, eyeRevPdf;

    auto M = lightVertex.lastVertexBSDF.eval(-incidentDirection, cosAtLightVertex, &lightDirPdf, &lightRevPdf) *
            eyeVertex.lastVertexBSDF.eval(incidentDirection, cosAtEyeVertex, &eyeDirPdf, &eyeRevPdf);

    if(G > 0.f && M != zero<Vec3f>()) {
        bool isVisible = !getScene().occluded(incidentRay);

        if(isVisible) {
            auto rcpWeight = 1.f;

            {
                auto pdfWrtArea = eyeDirPdf * abs(cosAtLightVertex) / sqr(dist);
                rcpWeight += Mis(pdfWrtArea) * (m_MisVmWeightFactor + lightVertex.dVCM + Mis(lightRevPdf) * lightVertex.dVC);
            }

            {
                auto pdfWrtArea = lightDirPdf * abs(cosAtEyeVertex) / sqr(dist);
                rcpWeight += Mis(pdfWrtArea) * (m_MisVmWeightFactor + eyeVertex.dVCM + Mis(eyeRevPdf) * eyeVertex.dVC);
            }

            auto weight = 1.f / rcpWeight;

            auto contrib = eyeVertex.power * lightVertex.power * G * M;

            auto weightedContrib = weight * contrib;
            getFramebuffer().accumulate(0, pixelID, Vec4f(weightedContrib, 0));
            getFramebuffer().accumulate(eyeVertex.depth + 1 + lightVertex.depth, pixelID, Vec4f(weightedContrib, 0));
        }
    }
}

bool VCMWithBSDFRenderer::samplePrimaryEyeVertex(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                            uint32_t x, uint32_t y, PathVertex& eyeVertex) const {
    auto s2D = getPixelSample(threadID, sampleID);
    auto primRay = getPrimaryRay(x, y, s2D);

    auto I = getScene().intersect(primRay);

    if(!I) {
        // Hit on environment light, if exists
        getFramebuffer().accumulate(0, pixelID, Vec4f(I.Le, 0));
        getFramebuffer().accumulate(1, pixelID, Vec4f(I.Le, 0));
        return false;
    }

    eyeVertex.lastVertex = I;
    eyeVertex.lastVertexBSDF = BSDF(-primRay.dir.xyz(), eyeVertex.lastVertex, getScene());
    eyeVertex.depth = 1;

    Ray shadowRay;
    auto surfaceToImageFactor = getCamera().surfaceToImageFactor(eyeVertex.lastVertex, shadowRay);
    auto imageToSurfaceFactor = 1.f / surfaceToImageFactor;

    eyeVertex.pdf = imageToSurfaceFactor;
    eyeVertex.pdfLastVertex = imageToSurfaceFactor;
    eyeVertex.power = Vec3f(1.f);

    eyeVertex.dVCM = Mis(m_nLightPathCount / imageToSurfaceFactor);
    eyeVertex.dVC = 0.f;
    eyeVertex.dVM = 0.f;

    return true;
}

void VCMWithBSDFRenderer::processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                             uint32_t x, uint32_t y) const {
    auto& framebuffer = getFramebuffer();

    auto spp = getSppCount();
    auto lightPathLength = getMaxLightPathDepth();
    auto pLightPath = m_LightPathArray.get() + pixelID * spp * lightPathLength;
    auto pEyePath = m_EyePathArray.get() + threadID * getMaxEyePathDepth();

    if(!samplePrimaryEyeVertex(threadID, pixelID, sampleID, x, y, pEyePath[0])) {
        return;
    }

    auto pEmissionVertex = m_EmissionSampler.sample(
                getScene(), getFloat(threadID), getFloat(threadID), getFloat2(threadID));

    auto maxEyePathDepth = getMaxEyePathDepth();

    for(auto i = 1u; i <= maxEyePathDepth; ++i) {
        auto k = i - 1u; // index of vertex in pEyePath;

        { // Intersection with light source
            auto totalLength = i;
            if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                computeEmittedRadiance(threadID, pixelID, sampleID, x, y, pEyePath[k]);
            }
        }

        if(m_UseVC) {
            vertexConnection(threadID, pixelID, sampleID, x, y, pEmissionVertex, pLightPath, pEyePath[k]);
        }

        // Vertex merging
        if(m_UseVM) {
            vertexMerging(threadID, pixelID, sampleID, pEyePath[k]);

            // PPM merges only at the first non-specular surface from camera
            if(m_Ppm) break;
        }

        if(i < maxEyePathDepth) {
            auto idx = k + 1;
            if(!sampleScattering(threadID, pixelID, pEyePath[k], pEyePath[idx], false)) {
                break;
            }
        }
    }
}

void VCMWithBSDFRenderer::preprocess() {
    m_EmissionSampler.initFrame(getScene());

    auto& fb = getFramebuffer();
    fb.setChannelCount(1 + m_nMaxDepth);
    fb.clear();

    auto size = getFramebuffer().getSize();
    auto spp = getSppCount();
    //m_LightNodeArray = makeUniqueArray<Unique<EmissionVertex>>(size.x * size.y * spp);

    auto lightVertexCount = size.x * size.y * spp * getMaxLightPathDepth();
    m_LightPathArray = makeUniqueArray<PathVertex>(lightVertexCount);
    m_LightVertexPerTile = makeUniqueArray<std::vector<PathVertex*>>(getTileCount());

    m_EyePathArray = makeUniqueArray<PathVertex>(getMaxEyePathDepth() * getThreadCount());

    m_LightTraceOnly = false;
    m_UseVC = false;
    m_UseVM = false;
    m_Ppm = false;

    switch(m_AlgorithmType)
    {
    case kLightTrace:
        m_LightTraceOnly = true;
        break;
    case kPpm:
        m_Ppm   = true;
        m_UseVM = true;
        break;
    case kBpm:
        m_UseVM = true;
        break;
    case kBpt:
        m_UseVC = true;
        break;
    case kVcm:
        m_UseVC = true;
        m_UseVM = true;
        break;
    default:
        std::cerr << "Unknown algorithm requested" << std::endl;
        break;
    }

    Vec3f sceneCenter;
    boundingSphere(getScene().getBBox(), sceneCenter, m_SceneRadius);

    m_BaseRadius  = m_RadiusFactor * m_SceneRadius;

    m_nIterationCount = 0u;
}

void VCMWithBSDFRenderer::beginFrame() {
    auto spp = getSppCount();
    auto maxLightPathDepth = getMaxLightPathDepth();

    auto size = getFramebuffer().getSize();
    auto pathCount = size.x * size.y * spp;
    m_nLightPathVertexCount = pathCount * getMaxLightPathDepth();
    m_nLightPathCount = pathCount;

    // Sample light paths for each pixel
    TileProcessingRenderer::processTiles([&](uint32_t threadID, uint32_t tileID, const Vec4u& viewport) {
        TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
            auto pixelID = getPixelIndex(x, y);
            auto lightPathOffset = pixelID * spp * maxLightPathDepth;
            for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
                auto pLightPath = m_LightPathArray.get() + lightPathOffset + sampleID * maxLightPathDepth;
                uint32_t lightPathDepth;
                sampleLightPath(getScene(), m_EmissionSampler,
                                threadID, pixelID, pLightPath,
                                lightPathDepth, maxLightPathDepth);
            }
        });
    });

    // Cluster light vertices per tile
    auto tileSize = getTileSize();

    std::for_each(m_LightVertexPerTile.get(), m_LightVertexPerTile.get() + getTileCount(), [](std::vector<PathVertex*>& v) {
        v.clear();
    });

    const auto& framebuffer = getFramebuffer();

    auto pEnd = m_LightPathArray.get() + m_nLightPathVertexCount;
    for(auto pLightVertex = m_LightPathArray.get(); pLightVertex != pEnd; ++pLightVertex) {
        Vec2f ndc;
        if(getCamera().getNDC(pLightVertex->lastVertex.P, ndc)) {
            auto uv = ndcToUV(ndc);
            auto pixel = getPixel(uv, framebuffer.getSize());

            auto tile = pixel / tileSize;
            auto tileID = getTileID(tile);

            m_LightVertexPerTile[tileID].emplace_back(pLightVertex);
        }
    }

    // Setup our radius, 1st iteration has aIteration == 0, thus offset
    float radius = m_BaseRadius;
    radius /= pow(float(m_nIterationCount + 1), 0.5f * (1 - m_RadiusAlpha));
    // Purely for numeric stability
    radius = max(radius, 1e-7f);
    const float radiusSqr = sqr(radius);

    // Factor used to normalise vertex merging contribution.
    // We divide the summed up energy by disk radius and number of light paths
    m_VmNormalization = 1.f / (radiusSqr * pi<float>() * m_nLightPathCount);

    // MIS weight constant [tech. rep. (20)], with n_VC = 1 and n_VM = mLightPathCount
    const float etaVCM = (pi<float>() * radiusSqr) * m_nLightPathCount;

    m_MisVmWeightFactor = m_UseVM ? Mis(etaVCM)       : 0.f;
    m_MisVcWeightFactor = m_UseVC ? Mis(1.f / etaVCM) : 0.f;

    if(m_UseVM)
    {
        m_HashGrid.Reserve(pathCount);
        m_HashGrid.Build(m_LightPathArray.get(), m_nLightPathVertexCount, radius);
    }
}

void VCMWithBSDFRenderer::endFrame() {
    ++m_nIterationCount;
}

void VCMWithBSDFRenderer::connectLightVerticesToCamera(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    const auto& camera = getCamera();
    const auto& scene = getScene();
    auto& framebuffer = getFramebuffer();

    auto viewportRange = Vec4u(viewport.xy(), viewport.xy() + viewport.zw());
    auto rcpPathCount = 1.f / m_nLightPathCount;

    for(auto pLightVertex: m_LightVertexPerTile[tileID]) {
        if(!pLightVertex->pdf) {
            continue;
        }

        auto totalDepth = 1 + pLightVertex->depth;
        if(!acceptPathDepth(totalDepth) || totalDepth > m_nMaxDepth) {
            continue;
        }

        Vec2f ndc;
        if(camera.getNDC(pLightVertex->lastVertex.P, ndc)) {
            auto uv = ndcToUV(ndc);
            auto pixel = getPixel(uv, framebuffer.getSize());

            auto pixelID = BnZ::getPixelIndex(pixel, framebuffer.getSize());

            Ray shadowRay;
            auto surfaceToImageFactor =
                    camera.surfaceToImageFactor(pLightVertex->lastVertex,
                                                shadowRay);
            auto fr = pLightVertex->lastVertexBSDF.eval(shadowRay.dir);
            auto bsdfRevPdfW = pLightVertex->lastVertexBSDF.pdf(shadowRay.dir, true);

            const float imageToSurfaceFactor = 1.f / surfaceToImageFactor;

            const float cameraPdfA = imageToSurfaceFactor;
            const float wLight = Mis(cameraPdfA * rcpPathCount) * (
                m_MisVmWeightFactor + pLightVertex->dVCM + pLightVertex->dVC * Mis(bsdfRevPdfW));

            const float misWeight = m_LightTraceOnly ? 1.f : (1.f / (wLight + 1.f));

            if(surfaceToImageFactor > 0.f && fr != zero<Vec3f>()) {
                if(!scene.occluded(shadowRay)) {
                    auto imageToSurfaceFactor = 1.f / surfaceToImageFactor;
                    framebuffer.accumulate(0u, pixelID, Vec4f(misWeight * rcpPathCount * pLightVertex->power * fr * imageToSurfaceFactor, 0.f));
                    framebuffer.accumulate(totalDepth, pixelID, Vec4f(misWeight * rcpPathCount * pLightVertex->power * fr * imageToSurfaceFactor, 0.f));
                }
            }

        }
    }
}

void VCMWithBSDFRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    auto spp = getSppCount();

    auto& framebuffer = getFramebuffer();

    TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
        auto pixelID = getPixelIndex(x, y);

        for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
            for(auto i = 0u; i < framebuffer.getChannelCount(); ++i) {
                framebuffer.accumulate(i, pixelID, Vec4f(0.f, 0.f, 0.f, 1));
            }

            processSample(threadID, pixelID, sampleID, x, y);
        }
    });

    if(m_UseVC) {
        connectLightVerticesToCamera(threadID, tileID, viewport);
    }

    if(m_bDisplayProgress) {
        auto l = debugLock();
        std::cerr << "Tile " << tileID << " / " << getTileCount() << " (" << (tileID * 100.f / getTileCount()) << " %)" << std::endl;
    }
}

std::string VCMWithBSDFRenderer::getAlgorithmType() const {
    const std::string mapping[] = {
        "lt", "ppm", "bpm", "bpt", "vcm"
    };
    return mapping[m_AlgorithmType];
}

void VCMWithBSDFRenderer::setAlgorithmType(const std::string& algorithmType) {
    const std::string mapping[] = {
        "lt", "ppm", "bpm", "bpt", "vcm"
    };
    auto it = std::find(std::begin(mapping), std::end(mapping), algorithmType);
    if(it == std::end(mapping)) {
        std::cerr << "Unrecognized algorithm type for VCMWithBSDFRenderer" << std::endl;
        m_AlgorithmType = kVcm;
        return;
    }
    m_AlgorithmType = AlgorithmType(it - std::begin(mapping));
}

void VCMWithBSDFRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
    getAttribute(xml, "maxDepth", m_nMaxDepth);
    getAttribute(xml, "displayProgress", m_bDisplayProgress);
    std::string algorithmType;
    getAttribute(xml, "algorithmType", algorithmType);
    setAlgorithmType(algorithmType);
    getAttribute(xml, "radiusFactor", m_RadiusFactor);
    getAttribute(xml, "radiusAlpha", m_RadiusAlpha);
}

void VCMWithBSDFRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
    setAttribute(xml, "maxDepth", m_nMaxDepth);
    setAttribute(xml, "displayProgress", m_bDisplayProgress);
    setAttribute(xml, "algorithmType", getAlgorithmType());
    setAttribute(xml, "radiusFactor", m_RadiusFactor);
    setAttribute(xml, "radiusAlpha", m_RadiusAlpha);
}

void VCMWithBSDFRenderer::doExposeIO(TwBar* bar) {
    atb::addVarRW(bar, ATB_VAR(m_nMaxDepth));
    atb::addVarRW(bar, ATB_VAR(m_RadiusFactor));
    atb::addVarRW(bar, ATB_VAR(m_RadiusAlpha));
    atb::addVarRW(bar, "algorithm", "kLightTrace,kPpm,kBpm,kBpt,kVcm", m_AlgorithmType);
}

}

