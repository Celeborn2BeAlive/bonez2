#pragma once

#include "TileProcessingRenderer.hpp"
#include <bonez/scene/lights/EmissionSampler.hpp>
#include <bonez/sampling/Random.hpp>

namespace BnZ {

class IGIRenderer: public TileProcessingRenderer {
    EmissionSampler m_EmissionSampler;

    uint32_t m_nMaxPathDepth = 2u;
    uint32_t m_nPathCount = 1u;

    struct PathVertex {
        Intersection lastVertex;
        BRDF lastVertexBRDF;
        Vec3f incidentDirection;
        float pdf; // pdf of the complete path
        float pdfLastVertex; // pdf of the last vertex, conditional to the previous
        Vec3f power;
        uint32_t depth; // number of edges of the path
    };

    Unique<EmissionVertex> sampleLightPath(
            const Scene& scene, const EmissionSampler& sampler,
            uint32_t threadID, uint32_t pixelID, PathVertex* pBuffer,
            uint32_t& lightPathLength,
            uint32_t maxLightPathDepth) const;

    std::vector<Unique<EmissionVertex>> m_LightNodeBuffer;
    std::vector<PathVertex> m_VertexBuffer;
public:
    std::string getName() const;

    void preprocess() override;

    uint32_t getMaxLightPathDepth() const;

    void beginFrame() override;

    void processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const;

    void doExposeIO(TwBar* bar) override;

    void doLoadSettings(const tinyxml2::XMLElement& xml) override;

    void doStoreSettings(tinyxml2::XMLElement& xml) const override;

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override;
};

}
