#include "RecursiveMISBDPTWithBSDFRenderer.hpp"
#include <bonez/scene/lights/AreaLight.hpp>

namespace BnZ {

RecursiveMISBDPTWithBSDFRenderer::RecursiveMISBDPTWithBSDFRenderer() {
}

std::string RecursiveMISBDPTWithBSDFRenderer::getName() const {
    return "RecursiveMISBDPTWithBSDFRenderer";
}

uint32_t RecursiveMISBDPTWithBSDFRenderer::getMaxLightPathDepth() const {
    return m_nMaxDepth - 1;
}

uint32_t RecursiveMISBDPTWithBSDFRenderer::getMaxEyePathDepth() const {
    return m_nMaxDepth;
}

bool RecursiveMISBDPTWithBSDFRenderer::sampleScattering(uint32_t threadID, uint32_t pixelID,
                                                        const PathVertex& currentVertex, PathVertex& nextVertex,
                                                        bool sampleAdjoint) const {
    Sample3f woSample;
    float cosThetaOutDir;
    uint32_t sampledEvent;
    auto fs = currentVertex.lastVertexBSDF.sample(Vec3f(getFloat(threadID), getFloat2(threadID)), woSample, cosThetaOutDir, &sampledEvent, sampleAdjoint);

    if(woSample.pdf == 0.f || fs == zero<Vec3f>()) {
        return false;
    }

    auto I = getScene().intersect(Ray(currentVertex.lastVertex, woSample.value));

    if(!I) {
        return false;
    }

    auto incidentDirection = -woSample.value;
    auto sqrLength = sqr(I.distance);
    auto pdfLastVertex = woSample.pdf  * abs(dot(I.Ns, incidentDirection)) / sqrLength;

    if(pdfLastVertex == 0.f) {
        return false;
    }

    nextVertex.lastVertex = I;
    nextVertex.lastVertexBSDF = BSDF(incidentDirection, I, getScene());
    nextVertex.pdf = currentVertex.pdf * pdfLastVertex;
    nextVertex.pdfLastVertex = pdfLastVertex;
    nextVertex.power = currentVertex.power * abs(cosThetaOutDir) * fs / woSample.pdf;
    nextVertex.depth = currentVertex.depth + 1;

    auto g = abs(cosThetaOutDir) / sqrLength;

    if((sampledEvent & ScatteringEvent::Specular) && currentVertex.lastVertexBSDF.isDelta()) {
        nextVertex.dVCM = 0.f;
        nextVertex.dVC = Mis(g) * currentVertex.dVC;
    } else {
        float reversePdf = currentVertex.lastVertexBSDF.pdf(woSample.value, true);

        nextVertex.dVCM = Mis(1.f / nextVertex.pdfLastVertex);
        nextVertex.dVC = Mis(g / nextVertex.pdfLastVertex) * (currentVertex.dVCM + Mis(reversePdf) * currentVertex.dVC);
    }

    return true;
}

Unique<EmissionVertex> RecursiveMISBDPTWithBSDFRenderer::sampleLightPath(
        const Scene& scene, const EmissionSampler& sampler,
        uint32_t threadID, uint32_t pixelID, PathVertex* pBuffer,
        uint32_t& lightPathLength,
        uint32_t maxLightPathDepth) const {
    for(PathVertex *it = pBuffer, *end = pBuffer + maxLightPathDepth; it != end; ++it) {
        it->pdf = 0.f;
    }

    auto pEmissionVertex = sampler.sample(scene,
                                          getFloat(threadID),
                                          getFloat(threadID),
                                          getFloat2(threadID));

    if(!pEmissionVertex || !pEmissionVertex->pdf()) {
        return nullptr;
    }

    if(maxLightPathDepth == 0u) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    RaySample ray;
    auto s2D = getFloat2(threadID);
    auto Le_dir = pEmissionVertex->sampleWi(ray, s2D);

    if(!ray.pdf) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    auto I = scene.intersect(ray.value);

    if(!I) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    auto power = pEmissionVertex->getRadiantExitance() * Le_dir / (pEmissionVertex->pdf() * ray.pdf);
    auto pdfLastVertex = pEmissionVertex->pdfWrtArea(I);
    auto pdf = pEmissionVertex->pdf() * pdfLastVertex;
    auto incidentDirection = -ray.value.dir;
    auto bsdf = BSDF(incidentDirection, I, getScene());
    pBuffer[0].lastVertex = I;
    pBuffer[0].lastVertexBSDF = bsdf;
    pBuffer[0].pdf = pdf;
    pBuffer[0].pdfLastVertex = pdfLastVertex;
    pBuffer[0].power = power;
    pBuffer[0].depth = 1u;

    pBuffer[0].dVCM = Mis(1.f / pBuffer[0].pdfLastVertex);
    pBuffer[0].dVC = Mis(pEmissionVertex->g(pBuffer[0].lastVertex) / (pEmissionVertex->pdf() * pBuffer[0].pdfLastVertex));

    lightPathLength = 1u;

    for(auto length = 2u; length <= maxLightPathDepth; ++length) {
        auto idx = length - 1;

        if(!sampleScattering(threadID, pixelID, pBuffer[idx - 1], pBuffer[idx], true)) {
            break;
        }

        ++lightPathLength;
    }

    return pEmissionVertex;
}

void RecursiveMISBDPTWithBSDFRenderer::computeEmittedRadiance(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                                              uint32_t x, uint32_t y, const PathVertex& eyeVertex) const {
    if(eyeVertex.lastVertex.Le == zero<Vec3f>()) {
        return;
    }

    auto contrib = eyeVertex.power * eyeVertex.lastVertex.Le;

    auto rcpWeight = 1.f;
    if(eyeVertex.depth > 1u) {
        if(!eyeVertex.lastVertex) {
            // Hit on environment map (if present) should be handled here

        } else {
            auto meshID = eyeVertex.lastVertex.meshID;
            auto lightID = getScene().getGeometry().getMesh(meshID).m_nLightID;

            const auto& pLight = getScene().getLightContainer().getLight(lightID);
            const auto* pAreaLight = static_cast<const AreaLight*>(pLight.get());

            float pointPdfWrtArea, directionPdfWrtSolidAngle;
            pAreaLight->pdf(eyeVertex.lastVertex, eyeVertex.lastVertexBSDF.getIncidentDirection(), getScene(),
                            pointPdfWrtArea, directionPdfWrtSolidAngle);

            pointPdfWrtArea *= m_EmissionSampler.pdf(lightID);

            rcpWeight += Mis(pointPdfWrtArea) * (eyeVertex.dVCM + Mis(directionPdfWrtSolidAngle) * eyeVertex.dVC);
        }
    }

    auto weight = 1.f / rcpWeight;

    auto weightedContrib = weight * contrib;
    getFramebuffer().accumulate(0, pixelID, Vec4f(weightedContrib, 0));
    getFramebuffer().accumulate(eyeVertex.depth, pixelID, Vec4f(weightedContrib, 0));
}

void RecursiveMISBDPTWithBSDFRenderer::computeDirectIllumination(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                                                 uint32_t x, uint32_t y, const PathVertex& eyeVertex,
                                                                 const EmissionVertex& lightVertex) const {
    Ray shadowRay;
    auto G = lightVertex.G(eyeVertex.lastVertex, shadowRay);

    float cosAtEyeVertex;
    float eyeDirPdf, eyeRevPdf;

    auto fr = eyeVertex.lastVertexBSDF.eval(shadowRay.dir, cosAtEyeVertex, &eyeDirPdf, &eyeRevPdf);
    if(G > 0.f && fr != zero<Vec3f>() && !getScene().occluded(shadowRay)) {
        auto contrib = eyeVertex.power * lightVertex.power() * lightVertex.Le(-shadowRay.dir) * G * fr;

        auto rcpWeight = 1.f;

        {
           auto pdfWrtArea = eyeDirPdf * lightVertex.g(eyeVertex.lastVertex);
           rcpWeight += Mis(pdfWrtArea / lightVertex.pdf());
        }

        {
            auto pdfWrtArea = lightVertex.pdfWrtArea(eyeVertex.lastVertex);
            rcpWeight += Mis(pdfWrtArea) * (eyeVertex.dVCM + Mis(eyeRevPdf) * eyeVertex.dVC);
        }

        auto weight = 1.f / rcpWeight;

        auto weightedContrib = weight * contrib;
        getFramebuffer().accumulate(0, pixelID, Vec4f(weightedContrib, 0));
        getFramebuffer().accumulate(eyeVertex.depth + 1, pixelID, Vec4f(weightedContrib, 0));
    }
}

void RecursiveMISBDPTWithBSDFRenderer::connectVertices(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                                       uint32_t x, uint32_t y, const PathVertex& eyeVertex,
                                                       const PathVertex& lightVertex) const {
    Vec3f incidentDirection;
    float dist;
    auto G = geometricFactor(eyeVertex.lastVertex, lightVertex.lastVertex, incidentDirection, dist);
    Ray incidentRay(eyeVertex.lastVertex, lightVertex.lastVertex, incidentDirection, dist);

    float cosAtLightVertex, cosAtEyeVertex;
    float lightDirPdf, lightRevPdf;
    float eyeDirPdf, eyeRevPdf;

    auto M = lightVertex.lastVertexBSDF.eval(-incidentDirection, cosAtLightVertex, &lightDirPdf, &lightRevPdf) *
            eyeVertex.lastVertexBSDF.eval(incidentDirection, cosAtEyeVertex, &eyeDirPdf, &eyeRevPdf);

    if(G > 0.f && M != zero<Vec3f>()) {
        bool isVisible = !getScene().occluded(incidentRay);

        if(isVisible) {
            auto rcpWeight = 1.f;

            {
                auto pdfWrtArea = eyeDirPdf * abs(cosAtLightVertex) / sqr(dist);
                rcpWeight += Mis(pdfWrtArea) * (lightVertex.dVCM + Mis(lightRevPdf) * lightVertex.dVC);
            }

            {
                auto pdfWrtArea = lightDirPdf * abs(cosAtEyeVertex) / sqr(dist);
                rcpWeight += Mis(pdfWrtArea) * (eyeVertex.dVCM + Mis(eyeRevPdf) * eyeVertex.dVC);
            }

            auto weight = 1.f / rcpWeight;

            auto contrib = eyeVertex.power * lightVertex.power * G * M;

            auto weightedContrib = weight * contrib;
            getFramebuffer().accumulate(0, pixelID, Vec4f(weightedContrib, 0));
            getFramebuffer().accumulate(eyeVertex.depth + 1 + lightVertex.depth, pixelID, Vec4f(weightedContrib, 0));
        }
    }
}

bool RecursiveMISBDPTWithBSDFRenderer::samplePrimaryEyeVertex(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                            uint32_t x, uint32_t y, PathVertex& eyeVertex) const {
    auto s2D = getPixelSample(threadID, sampleID);
    auto primRay = getPrimaryRay(x, y, s2D);

    auto I = getScene().intersect(primRay);

    if(!I) {
        // Hit on environment light, if exists
        getFramebuffer().accumulate(0, pixelID, Vec4f(I.Le, 0));
        getFramebuffer().accumulate(1, pixelID, Vec4f(I.Le, 0));
        return false;
    }

    eyeVertex.lastVertex = I;
    eyeVertex.lastVertexBSDF = BSDF(-primRay.dir.xyz(), eyeVertex.lastVertex, getScene());
    eyeVertex.depth = 1;

    Ray shadowRay;
    auto surfaceToImageFactor = getCamera().surfaceToImageFactor(eyeVertex.lastVertex, shadowRay);
    auto imageToSurfaceFactor = 1.f / surfaceToImageFactor;

    eyeVertex.pdf = imageToSurfaceFactor;
    eyeVertex.pdfLastVertex = imageToSurfaceFactor;
    eyeVertex.power = Vec3f(1.f);

    eyeVertex.dVCM = Mis(m_nLightPathCount / imageToSurfaceFactor);
    eyeVertex.dVC = 0.f;

    return true;
}

void RecursiveMISBDPTWithBSDFRenderer::processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       uint32_t x, uint32_t y) const {
    for(auto i = 0u; i <= m_nMaxDepth; ++i) {
        getFramebuffer().accumulate(i, pixelID, Vec4f(0, 0, 0, 1));
    }

    auto pLightPath = m_LightPathArray.get() + pixelID * getMaxLightPathDepth();
    auto pEyePath = m_EyePathArray.get() + threadID * getMaxEyePathDepth();

    if(!samplePrimaryEyeVertex(threadID, pixelID, sampleID, x, y, pEyePath[0])) {
        return;
    }

    auto emissionVertex = m_EmissionSampler.sample(getScene(), getFloat(threadID), getFloat(threadID), getFloat2(threadID));

    auto maxEyePathDepth = getMaxEyePathDepth();
    auto maxLightPathDepth = getMaxLightPathDepth();

    for(auto eyePathDepth = 1u; eyePathDepth <= maxEyePathDepth; ++eyePathDepth) {
        auto eyeVertexIndex = eyePathDepth - 1u; // index of vertex in pEyePath;

        { // Intersection with light source
            auto totalLength = eyePathDepth;
            if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                computeEmittedRadiance(threadID, pixelID, sampleID, x, y, pEyePath[eyeVertexIndex]);
            }
        }

        { // Direct illumination
            auto totalLength = eyePathDepth + 1;
            if(emissionVertex && acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                computeDirectIllumination(threadID, pixelID, sampleID, x, y, pEyePath[eyeVertexIndex], *emissionVertex);
            }
        }

        // Connection with each light vertex
        for(auto j = 0u; j < maxLightPathDepth; ++j) {
            auto lightPath = pLightPath + j;
            auto totalLength = eyePathDepth + lightPath->depth + 1;

            if(lightPath->pdf > 0.f && acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                connectVertices(threadID, pixelID, sampleID, x, y, pEyePath[eyeVertexIndex], *lightPath);
            }
        }

        if(eyePathDepth < maxEyePathDepth) {
            auto nextEyeVertexIndex = eyeVertexIndex + 1;
            if(!sampleScattering(threadID, pixelID, pEyePath[eyeVertexIndex], pEyePath[nextEyeVertexIndex], false)) {
                break;
            }
        }
    }
}

void RecursiveMISBDPTWithBSDFRenderer::preprocess() {
    m_EmissionSampler.initFrame(getScene());

    m_nLightPathCount = getFramebuffer().getPixelCount();
    m_nLightVertexCount = m_nLightPathCount * getMaxLightPathDepth();

    m_LightVertexPerTile = makeUniqueArray<std::vector<PathVertex*>>(getTileCount());

    m_LightPathArray = makeUniqueArray<PathVertex>(m_nLightVertexCount);
    m_EyePathArray = makeUniqueArray<PathVertex>(getThreadCount() * getMaxEyePathDepth());

    getFramebuffer().setChannelCount(1 + m_nMaxDepth);
    getFramebuffer().clear();
}

void RecursiveMISBDPTWithBSDFRenderer::connectLightVerticesToCamera(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    const auto& camera = getCamera();
    const auto& scene = getScene();
    auto& framebuffer = getFramebuffer();

    auto rcpPathCount = 1.f / m_nLightPathCount;

    for(auto pLightVertex: m_LightVertexPerTile[tileID]) {
        if(!pLightVertex->pdf) {
            continue;
        }

        auto totalDepth = 1 + pLightVertex->depth;
        if(!acceptPathDepth(totalDepth) || totalDepth > m_nMaxDepth) {
            continue;
        }

        Vec2f ndc;
        if(camera.getNDC(pLightVertex->lastVertex.P, ndc)) {
            auto uv = ndcToUV(ndc);
            auto pixel = getPixel(uv, framebuffer.getSize());

            auto pixelID = BnZ::getPixelIndex(pixel, framebuffer.getSize());

            Ray shadowRay;
            auto surfaceToImageFactor =
                    camera.surfaceToImageFactor(pLightVertex->lastVertex,
                                                shadowRay);
            float costThetaOurDir;
            float bsdfRevPdfW;
            auto fr = pLightVertex->lastVertexBSDF.eval(shadowRay.dir, costThetaOurDir, nullptr, &bsdfRevPdfW);

            const float imageToSurfaceFactor = 1.f / surfaceToImageFactor;

            const float cameraPdfA = imageToSurfaceFactor;
            const float wLight = Mis(cameraPdfA * rcpPathCount) *
                    (pLightVertex->dVCM + Mis(bsdfRevPdfW) * pLightVertex->dVC);

            const float misWeight = 1.f / (wLight + 1.f);

            if(surfaceToImageFactor > 0.f && fr != zero<Vec3f>()) {
                if(!scene.occluded(shadowRay)) {
                    auto imageToSurfaceFactor = 1.f / surfaceToImageFactor;
                    framebuffer.accumulate(0u, pixelID, Vec4f(misWeight * rcpPathCount * pLightVertex->power * fr * imageToSurfaceFactor, 0.f));
                    framebuffer.accumulate(totalDepth, pixelID, Vec4f(misWeight * rcpPathCount * pLightVertex->power * fr * imageToSurfaceFactor, 0.f));
                }
            }

        }
    }
}

void RecursiveMISBDPTWithBSDFRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    auto spp = getSppCount();

    TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
        auto pixelID = getPixelIndex(x, y);
        for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
            processSample(threadID, pixelID, sampleID, x, y);
        }
    });

    connectLightVerticesToCamera(threadID, tileID, viewport);

    if(m_bDisplayProgress) {
        auto l = debugLock();
        std::cerr << "Tile " << tileID << " / " << getTileCount() << " (" << (tileID * 100.f / getTileCount()) << " %)" << std::endl;
    }
}

void RecursiveMISBDPTWithBSDFRenderer::beginFrame() {
    auto maxLightPathDepth = getMaxLightPathDepth();

    // Sample light paths for each pixel
    TileProcessingRenderer::processTiles([&](uint32_t threadID, uint32_t tileID, const Vec4u& viewport) {
        TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
            auto pixelID = getPixelIndex(x, y);
            auto lightPathOffset = pixelID * maxLightPathDepth;

            auto pLightPath = m_LightPathArray.get() + lightPathOffset;
            uint32_t lightPathDepth;
            sampleLightPath(getScene(), m_EmissionSampler,
                            threadID, pixelID, pLightPath,
                            lightPathDepth, maxLightPathDepth);
        });
    });

    // Cluster light vertices per tile
    auto tileSize = getTileSize();

    std::for_each(m_LightVertexPerTile.get(), m_LightVertexPerTile.get() + getTileCount(), [](std::vector<PathVertex*>& v) {
        v.clear();
    });

    const auto& framebuffer = getFramebuffer();

    auto pEnd = m_LightPathArray.get() + m_nLightVertexCount;
    for(auto pLightVertex = m_LightPathArray.get(); pLightVertex != pEnd; ++pLightVertex) {
        Vec2f ndc;
        if(getCamera().getNDC(pLightVertex->lastVertex.P, ndc)) {
            auto uv = ndcToUV(ndc);
            auto pixel = getPixel(uv, framebuffer.getSize());

            auto tile = pixel / tileSize;
            auto tileID = getTileID(tile);

            m_LightVertexPerTile[tileID].emplace_back(pLightVertex);
        }
    }
}

void RecursiveMISBDPTWithBSDFRenderer::doExposeIO(TwBar* bar) {
    atb::addVarRW(bar, ATB_VAR(m_nMaxDepth));
    atb::addVarRW(bar, ATB_VAR(m_bDisplayProgress));
}

void RecursiveMISBDPTWithBSDFRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
    getAttribute(xml, "maxDepth", m_nMaxDepth);
    getAttribute(xml, "displayProgress", m_bDisplayProgress);
}

void RecursiveMISBDPTWithBSDFRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
    setAttribute(xml, "maxDepth", m_nMaxDepth);
    setAttribute(xml, "displayProgress", m_bDisplayProgress);
}

}
