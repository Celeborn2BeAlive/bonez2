#pragma once

#include <bonez/rendering/renderers/TileProcessingRenderer.hpp>
#include <bonez/sampling/multiple_importance_sampling.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/sampling/distribution1d.h>
#include <bonez/sampling/shapes.hpp>
#include <bonez/sampling/patterns.hpp>
#include <bonez/scene/lights/EmissionSampler.hpp>

#include <bonez/opengl/debug/GLDebugRenderer.hpp>
#include <bonez/opengl/debug/GLDebugOutputStream.hpp>

namespace BnZ {

class RecursiveMISBDPTRenderer: public TileProcessingRenderer {
    uint32_t m_nMaxDepth = 3;
    bool m_bDisplayProgress = false;

    EmissionSampler m_EmissionSampler;

    enum RenderTarget {
        FINAL_RENDER = 0
    };

    struct PathVertex {
        Intersection lastVertex;
        BRDF lastVertexBRDF;
        Vec3f incidentDirection;
        float pdf; // pdf of the complete path
        float pdfLastVertex; // pdf of the last vertex, conditional to the previous
        Vec3f power;
        uint32_t depth; // number of edges of the path

        float dVCM;
        float dVC;
    };

    Unique<EmissionVertex> sampleLightPath(
            const Scene& scene, const EmissionSampler& sampler,
            uint32_t threadID, uint32_t pixelID, PathVertex* pBuffer,
            uint32_t& lightPathLength,
            uint32_t maxLightPathDepth) const;

    Unique<PathVertex[]> m_PathArray;

    float computeLightPathWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                 PathVertex* eyePathVertex, PathVertex* lightPathVertex) const;

    float computeEyePathWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                               PathVertex* eyePathVertex, PathVertex* lightPathVertex) const;

    float computeMISWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                           PathVertex* eyePathVertex, PathVertex* lightPathVertex) const;

    float computeMISWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                           PathVertex* eyePathVertex, const Scene& scene, const EmissionSampler& sampler) const;

    float computeMISWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                    PathVertex* eyePathVertex,
                                    const EmissionVertex* lightNode,
                                    const Vec3f& dirToLight) const;

public:
    RecursiveMISBDPTRenderer();

    virtual std::string getName() const;

    uint32_t getMaxLightPathDepth() const;

    uint32_t getMaxEyePathDepth() const;

    void Li(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y, Vec4f values[8]) const;

    void preprocess() override;

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override;

    virtual void doExposeIO(TwBar* bar);

    virtual void doLoadSettings(const tinyxml2::XMLElement& xml);

    virtual void doStoreSettings(tinyxml2::XMLElement& xml) const;
};

}
