#pragma once

#include <bonez/rendering/renderers/TileProcessingRenderer.hpp>
#include <bonez/sampling/multiple_importance_sampling.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/sampling/distribution1d.h>
#include <bonez/sampling/shapes.hpp>
#include <bonez/sampling/patterns.hpp>
#include <bonez/scene/lights/EmissionSampler.hpp>
#include <bonez/scene/shading/BSDF.hpp>

#include <bonez/opengl/debug/GLDebugRenderer.hpp>
#include <bonez/opengl/debug/GLDebugOutputStream.hpp>

namespace BnZ {

class RecursiveMISBDPTWithBSDFRenderer: public TileProcessingRenderer {
    uint32_t m_nMaxDepth = 3;
    bool m_bDisplayProgress = false;

    EmissionSampler m_EmissionSampler;

    struct PathVertex {
        Intersection lastVertex;
        BSDF lastVertexBSDF;
        //Vec3f incidentDirection;
        float pdf; // pdf of the complete path
        float pdfLastVertex; // pdf of the last vertex, conditional to the previous
        Vec3f power;
        uint32_t depth; // number of edges of the path

        float dVCM;
        float dVC;
    };

    Unique<EmissionVertex> sampleLightPath(
            const Scene& scene, const EmissionSampler& sampler,
            uint32_t threadID, uint32_t pixelID, PathVertex* pBuffer,
            uint32_t& lightPathLength,
            uint32_t maxLightPathDepth) const;

    bool sampleScattering(uint32_t threadID, uint32_t pixelID,
                          const PathVertex& currentVertex, PathVertex& nextVertex,
                          bool sampleAdjoint) const;

    Unique<PathVertex[]> m_LightPathArray;
    Unique<PathVertex[]> m_EyePathArray;
    Unique<std::vector<PathVertex*>[]> m_LightVertexPerTile;

    uint32_t m_nLightPathCount;
    uint32_t m_nLightVertexCount;

    float Mis(float pdf) const {
        return pdf; // Balance heuristic
    }

    void connectLightVerticesToCamera(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const;

    void computeEmittedRadiance(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                           uint32_t x, uint32_t y, const PathVertex& eyeVertex) const;

    void computeDirectIllumination(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                   uint32_t x, uint32_t y, const PathVertex& eyeVertex,
                                   const EmissionVertex& lightVertex) const;

    void connectVertices(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                         uint32_t x, uint32_t y, const PathVertex& eyeVertex,
                         const PathVertex& lightVertex) const;

    bool samplePrimaryEyeVertex(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                uint32_t x, uint32_t y, PathVertex& eyeVertex) const;

public:
    RecursiveMISBDPTWithBSDFRenderer();

    virtual std::string getName() const;

    uint32_t getMaxLightPathDepth() const;

    uint32_t getMaxEyePathDepth() const;

    void processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const;

    void beginFrame() override;

    void preprocess() override;

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override;

    virtual void doExposeIO(TwBar* bar);

    virtual void doLoadSettings(const tinyxml2::XMLElement& xml);

    virtual void doStoreSettings(tinyxml2::XMLElement& xml) const;
};

}
