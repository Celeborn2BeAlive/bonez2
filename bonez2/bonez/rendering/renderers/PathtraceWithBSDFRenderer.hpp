#pragma once

#include "TileProcessingRenderer.hpp"
#include <bonez/scene/lights/EmissionSampler.hpp>

namespace BnZ {

class PathtraceWithBSDFRenderer: public TileProcessingRenderer {
public:
    EmissionSampler m_EmissionSampler;

    uint32_t m_nMaxPathDepth = 2;

    std::string getName() const override;

    void preprocess() override;

    void processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const;

    void doExposeIO(TwBar* bar) override;

    void doLoadSettings(const tinyxml2::XMLElement& xml) override;

    void doStoreSettings(tinyxml2::XMLElement& xml) const override;

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override;
};


}
