#include "VCMRenderer.hpp"
#include <bonez/scene/lights/AreaLight.hpp>

namespace BnZ {

VCMRenderer::VCMRenderer() {
}

std::string VCMRenderer::getName() const {
    return "VCMRenderer";
}

uint32_t VCMRenderer::getMaxLightPathDepth() const {
    return m_nMaxDepth;
}

uint32_t VCMRenderer::getMaxEyePathDepth() const {
    return m_nMaxDepth;
}

float VCMRenderer::computeMISWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                const PathVertex& eyePathVertex,
                                const EmissionVertex* lightNode,
                                const Vec3f& dirToLight) const {
    auto sum = 1.f;

    { // Eval w(VC,0)(y)
       auto pdfWrtSolidAngle = eyePathVertex.lastVertexBRDF.pdf(eyePathVertex.incidentDirection, dirToLight);
       auto pdfWrtArea = pdfWrtSolidAngle * lightNode->g(eyePathVertex.lastVertex);

       sum += Mis(pdfWrtArea / lightNode->pdf());
    }

    // Eval w(VC,t-1)(z)
    auto pdfWrtArea = lightNode->pdfWrtArea(eyePathVertex.lastVertex);

    auto revPdfWrtSolidAngle = eyePathVertex.lastVertexBRDF.pdf(dirToLight, eyePathVertex.incidentDirection);

    sum += Mis(pdfWrtArea) * (m_MisVmWeightFactor + eyePathVertex.dVCM + Mis(revPdfWrtSolidAngle) * eyePathVertex.dVC);

    return 1.f / sum;
}

float VCMRenderer::computeMISWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       const PathVertex& eyePathVertex, const Scene& scene, const EmissionSampler& sampler) const {
    if(eyePathVertex.lastVertex.Le == zero<Vec3f>()) {
        return 0.f;
    }

    if(eyePathVertex.depth == 1u) {
        return 1.f;
    }

    if(!eyePathVertex.lastVertex) {
        // Hit on environment map (if present) should be handled here
        return 0.f;
    }

    auto meshID = eyePathVertex.lastVertex.meshID;
    auto lightID = scene.getGeometry().getMesh(meshID).m_nLightID;

    const auto& pLight = scene.getLightContainer().getLight(lightID);
    const auto* pAreaLight = static_cast<const AreaLight*>(pLight.get());

    auto pdfWrtArea = sampler.pdf(lightID) * pAreaLight->pdf(eyePathVertex.lastVertex, scene);

    auto p1 = Mis(pdfWrtArea);

    auto p2 = Mis(cosineSampleHemispherePDF(eyePathVertex.incidentDirection, eyePathVertex.lastVertex.Ns));

    auto sum = 1 + p1 * (eyePathVertex.dVCM + p2 * eyePathVertex.dVC);
    return 1.f / sum;
}

float VCMRenderer::computeLightPathWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                             const PathVertex& eyePathVertex, const PathVertex& lightPathVertex) const {
    auto exitantDir = lightPathVertex.lastVertex.P - eyePathVertex.lastVertex.P; // Exitent direction on the currentVertex when going toward the light node
    auto l = length(exitantDir);
    if(l == 0.f) {
        return 0.f; // avoid NaN bugs
    }
    exitantDir /= l;
    auto sqrDist = sqr(l);

    auto pdfWrtSolidAngle = eyePathVertex.lastVertexBRDF.pdf(eyePathVertex.incidentDirection, exitantDir);
    auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(lightPathVertex.lastVertex.Ns, -exitantDir)) / sqrDist;

    auto p1 = Mis(pdfWrtArea);

    auto p2 = Mis(lightPathVertex.lastVertexBRDF.pdf(-exitantDir, lightPathVertex.incidentDirection));

    return p1 * (m_MisVmWeightFactor + lightPathVertex.dVCM + p2 * lightPathVertex.dVC);
}


float VCMRenderer::computeEyePathWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                           const PathVertex& eyePathVertex, const PathVertex& lightPathVertex) const {
//    if(eyePathVertex.depth == 1u) {
//        return 0.f;
//    }

    auto exitantDir = eyePathVertex.lastVertex.P - lightPathVertex.lastVertex.P; // Exitent direction on the currentVertex when going toward the light node
    auto l = length(exitantDir);
    if(l == 0.f) {
        return 0.f; // avoid NaN bugs
    }
    exitantDir /= l;
    auto sqrDist = sqr(l);

    auto pdfWrtSolidAngle = lightPathVertex.lastVertexBRDF.pdf(lightPathVertex.incidentDirection, exitantDir);
    auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(eyePathVertex.lastVertex.Ns, -exitantDir)) / sqrDist;

    auto p1 = Mis(pdfWrtArea);

    auto p2 = Mis(eyePathVertex.lastVertexBRDF.pdf(-exitantDir, eyePathVertex.incidentDirection));

    return p1 * (m_MisVmWeightFactor + eyePathVertex.dVCM + p2 * eyePathVertex.dVC);
}

float VCMRenderer::computeMISWeightRecursive(uint threadID, uint pixelID, uint32_t sampleID,
                       const PathVertex& eyePathVertex, const PathVertex& lightPathVertex) const {
    auto sum = 1.f;

    sum += computeLightPathWeightRecursive(threadID, pixelID, sampleID, eyePathVertex, lightPathVertex);

    sum += computeEyePathWeightRecursive(threadID, pixelID, sampleID, eyePathVertex, lightPathVertex);

    return 1.f / sum;
}

Unique<EmissionVertex> VCMRenderer::sampleLightPath(
        const Scene& scene, const EmissionSampler& sampler,
        uint32_t threadID, uint32_t pixelID, PathVertex* pBuffer,
        uint32_t& lightPathLength,
        uint32_t maxLightPathDepth) {
    for(PathVertex *it = pBuffer, *end = pBuffer + maxLightPathDepth; it != end; ++it) {
        it->pdf = 0.f;
    }

    auto pEmissionVertex = sampler.sample(scene,
                                          getFloat(threadID),
                                          getFloat(threadID),
                                          getFloat2(threadID));

    if(!pEmissionVertex || !pEmissionVertex->pdf()) {
        return nullptr;
    }

    if(maxLightPathDepth == 0u) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    RaySample ray;
    auto s2D = getFloat2(threadID);
    auto Le_dir = pEmissionVertex->sampleWi(ray, s2D);

    if(!ray.pdf) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    auto I = scene.intersect(ray.value);

    if(!I) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    auto power = pEmissionVertex->getRadiantExitance() * Le_dir / (pEmissionVertex->pdf() * ray.pdf);
    auto pdfLastVertex = pEmissionVertex->pdfWrtArea(I);
    auto pdf = pEmissionVertex->pdf() * pdfLastVertex;
    auto incidentDirection = -ray.value.dir;
    auto brdf = scene.shade(I);
    pBuffer[0].lastVertex = I;
    pBuffer[0].lastVertexBRDF = brdf;
    pBuffer[0].incidentDirection = incidentDirection;
    pBuffer[0].pdf = pdf;
    pBuffer[0].pdfLastVertex = pdfLastVertex;
    pBuffer[0].power = power;
    pBuffer[0].depth = 1u;

    pBuffer[0].dVCM = Mis(1.f / pBuffer[0].pdfLastVertex);
    pBuffer[0].dVC = Mis(pEmissionVertex->g(pBuffer[0].lastVertex) / (pEmissionVertex->pdf() * pBuffer[0].pdfLastVertex));
    pBuffer[0].dVM = pBuffer[0].dVC * m_MisVcWeightFactor;

    lightPathLength = 1u;

    for(auto length = 2u; length <= maxLightPathDepth; ++length) {
        auto idx = length - 1;

        Sample3f woSample;
        auto fr = brdf.sample(incidentDirection, woSample, getFloat2(threadID));
        if(woSample.pdf > 0.f && fr != zero<Vec3f>()) {
            auto Itmp = scene.intersect(Ray(I, woSample.value));

            if(Itmp) {
                incidentDirection = -woSample.value;
                auto sqrLength = sqr(Itmp.distance);
                pdfLastVertex = woSample.pdf  * max(0.f, dot(Itmp.Ns, incidentDirection)) / sqrLength;
                pdf *= pdfLastVertex;
                power *= max(0.f, dot(I.Ns, woSample.value)) * fr / woSample.pdf;
            } else {
                break;
            }

            if(pdfLastVertex == 0.f) {
                break;
            }

            I = Itmp;
            brdf = scene.shade(I);

            lightPathLength = length;
            pBuffer[idx].lastVertex = I;
            pBuffer[idx].lastVertexBRDF = brdf;
            pBuffer[idx].incidentDirection = incidentDirection;
            pBuffer[idx].pdf = pdf;
            pBuffer[idx].pdfLastVertex = pdfLastVertex;
            pBuffer[idx].power = power;
            pBuffer[idx].depth = length;

            auto g = max(0.f, dot(pBuffer[idx - 1].lastVertex.Ns, woSample.value)) / sqr(pBuffer[idx].lastVertex.distance);
            float reversePdf = pBuffer[idx - 1].lastVertexBRDF.pdf(woSample.value, pBuffer[idx - 1].incidentDirection);

            pBuffer[idx].dVCM = Mis(1.f / pBuffer[idx].pdfLastVertex);
            pBuffer[idx].dVC = Mis(g / pBuffer[idx].pdfLastVertex) *
                    (m_MisVmWeightFactor +
                     pBuffer[idx - 1].dVCM +
                     pBuffer[idx - 1].dVC * Mis(reversePdf));
            pBuffer[idx].dVM = Mis(g / pBuffer[idx].pdfLastVertex) *
                    (1.f +
                     pBuffer[idx - 1].dVCM * m_MisVcWeightFactor +
                     pBuffer[idx - 1].dVM * Mis(reversePdf));

        } else {
            break;
        }
    }

    return pEmissionVertex;
}

void VCMRenderer::vertexMerging(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, const PathVertex& eyeVertex) const {
    auto& framebuffer = getFramebuffer();

    auto vmContrib = zero<Vec3f>();
    auto query = [&](const PathVertex& lightPathVertex) {
        // Reject if full path length below/above min/max path length
        auto totalDepth = lightPathVertex.depth + eyeVertex.depth;
        if(!acceptPathDepth(totalDepth) || totalDepth > m_nMaxDepth) {
             return;
        }

        // Retrieve light incoming direction in world coordinates
        const Vec3f lightDirection = lightPathVertex.incidentDirection;

//        if(dot(lightDirection, pEyePath[k].incidentDirection) < 0.f) {
//            return;
//        }

        if(dot(lightDirection, eyeVertex.lastVertex.Ns) < 0.f) {
            return;
        }

        const Vec3f fr = eyeVertex.lastVertexBRDF.eval(lightDirection,
                                                       eyeVertex.incidentDirection);

        auto brdfDirPdf = eyeVertex.lastVertexBRDF.pdf(eyeVertex.incidentDirection, lightDirection);
        auto brdfRevPdf = eyeVertex.lastVertexBRDF.pdf(lightDirection, eyeVertex.incidentDirection);

        if(fr == zero<Vec3f>())
            return;

        // Partial light sub-path MIS weight [tech. rep. (38)]
        const float wLight = lightPathVertex.dVCM * m_MisVcWeightFactor +
            lightPathVertex.dVM * Mis(brdfDirPdf);

        // Partial eye sub-path MIS weight [tech. rep. (39)]
        const float wCamera = eyeVertex.dVCM * m_MisVcWeightFactor +
            eyeVertex.dVM * Mis(brdfRevPdf);

        // Full path MIS weight [tech. rep. (37)]. No MIS for PPM
        const float misWeight = m_Ppm ?
            1.f :
            1.f / (wLight + 1.f + wCamera);

        auto contrib = misWeight * fr * lightPathVertex.power;
        vmContrib += contrib;

        framebuffer.accumulate(totalDepth, pixelID, Vec4f(eyeVertex.power * m_VmNormalization * contrib, 0.f));
    };

    m_HashGrid.Process(m_LightPathArray.get(), getPosition(eyeVertex), query);
    framebuffer.accumulate(0u, pixelID, Vec4f(eyeVertex.power * m_VmNormalization * vmContrib, 0.f));
}

void VCMRenderer::vertexConnection(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                const Unique<EmissionVertex>& pEmissionVertex, const PathVertex* pLightPath,
                                const PathVertex& eyeVertex) const {
    auto& framebuffer = getFramebuffer();

    {   // Direct illumination
        auto totalLength = eyeVertex.depth + 1;
        if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
            Ray shadowRay;
            auto G = pEmissionVertex->G(eyeVertex.lastVertex, shadowRay);
            if(G > 0.f && !getScene().occluded(shadowRay)) {
                auto contrib = eyeVertex.power * pEmissionVertex->power() * pEmissionVertex->Le(-shadowRay.dir)
                        * G * eyeVertex.lastVertexBRDF.eval(shadowRay.dir, eyeVertex.incidentDirection);

                auto misWeight = computeMISWeightRecursive(threadID, pixelID, sampleID, eyeVertex, pEmissionVertex.get(), shadowRay.dir);
                auto weightedContrib = misWeight * contrib;

                framebuffer.accumulate(0u, pixelID, Vec4f(weightedContrib, 0.f));
                framebuffer.accumulate(totalLength, pixelID, Vec4f(weightedContrib, 0.f));
            }
        }
    }

    auto lightPathDepth = getMaxLightPathDepth();
    // Connection with light vertices
    for(auto j = 0u; j < lightPathDepth; ++j) {
        auto lightPath = pLightPath + j;
        auto totalLength = eyeVertex.depth + lightPath->depth + 1;

        if(lightPath->pdf > 0.f && acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
            Vec3f incidentDirection;
            float dist;
            auto G = geometricFactor(eyeVertex.lastVertex, lightPath->lastVertex, incidentDirection, dist);
            Ray incidentRay(eyeVertex.lastVertex, lightPath->lastVertex, incidentDirection, dist);

            if(G > 0.f) {
                bool isVisible = !getScene().occluded(incidentRay);

                if(isVisible) {
                    auto misWeight = computeMISWeightRecursive(threadID, pixelID, sampleID, eyeVertex, *lightPath);

                    auto contrib = eyeVertex.power * lightPath->power * lightPath->lastVertexBRDF.eval(lightPath->incidentDirection, -incidentDirection)
                            * G * eyeVertex.lastVertexBRDF.eval(incidentDirection, eyeVertex.incidentDirection);

                    auto weightedContrib = misWeight * contrib;

                    framebuffer.accumulate(0u, pixelID, Vec4f(weightedContrib, 0.f));
                    framebuffer.accumulate(totalLength, pixelID, Vec4f(weightedContrib, 0.f));
                }
            }
        }
    }
}

void VCMRenderer::processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                             uint32_t x, uint32_t y) const {
    auto& framebuffer = getFramebuffer();

    auto s2D = getPixelSample(threadID, sampleID);
    auto primRay = getPrimaryRay(x, y, s2D);

    auto spp = getSppCount();
    auto lightPathLength = getMaxLightPathDepth();
    auto pLightPath = m_LightPathArray.get() + pixelID * spp * lightPathLength;
    auto pEyePath = m_EyePathArray.get() + threadID * getMaxEyePathDepth();

    const auto& pEmissionVertex = m_LightNodeArray[pixelID * spp];

    {
        auto I = getScene().intersect(primRay);

        if(!I && acceptPathDepth(1u) && 1u <= m_nMaxDepth) {
            framebuffer.accumulate(0u, pixelID, Vec4f(I.Le, 0.f));
            framebuffer.accumulate(1u, pixelID, Vec4f(I.Le, 0.f));
            return;
        }

        pEyePath[0].lastVertex = I;
        pEyePath[0].lastVertexBRDF = getScene().shade(pEyePath[0].lastVertex);
        pEyePath[0].incidentDirection = -primRay.dir.xyz();
        pEyePath[0].depth = 1;

        Ray shadowRay;
        auto surfaceToImageFactor = getCamera().surfaceToImageFactor(pEyePath[0].lastVertex, shadowRay);
        auto imageToSurfaceFactor = 1.f / surfaceToImageFactor;

        pEyePath[0].pdf = imageToSurfaceFactor;
        pEyePath[0].pdfLastVertex = imageToSurfaceFactor;
        pEyePath[0].power = Vec3f(1.f);

        pEyePath[0].dVCM = Mis(m_LightSubPathCount / imageToSurfaceFactor);
        pEyePath[0].dVC =  0.f;
        pEyePath[0].dVM = 0.f;
    }

    auto maxEyePathDepth = getMaxEyePathDepth();

    for(auto i = 1u; i <= maxEyePathDepth; ++i) {
        auto k = i - 1u; // index of vertex in pEyePath;

        { // Intersection with light source
            auto totalLength = i;
            if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                auto misWeight = computeMISWeightRecursive(threadID, pixelID, sampleID, pEyePath[k], getScene(), m_EmissionSampler);

                auto contrib = pEyePath[k].power * pEyePath[k].lastVertex.Le;
                auto weightedContrib = misWeight * contrib;

                framebuffer.accumulate(0u, pixelID, Vec4f(weightedContrib, 0.f));
                framebuffer.accumulate(totalLength, pixelID, Vec4f(weightedContrib, 0.f));
            }
        }

        if(pEmissionVertex && m_UseVC) {
            vertexConnection(threadID, pixelID, sampleID, pEmissionVertex, pLightPath, pEyePath[k]);
        }

        // Vertex merging
        if(m_UseVM) {
            vertexMerging(threadID, pixelID, sampleID, pEyePath[k]);

            // PPM merges only at the first non-specular surface from camera
            if(m_Ppm) break;
        }

        if(i < maxEyePathDepth) {
            Sample3f wiSample;
            auto fr = pEyePath[k].lastVertexBRDF.sample(pEyePath[k].incidentDirection, wiSample, getFloat2(threadID));

            if(wiSample.pdf > 0.f) {
                auto throughput = fr * max(0.f, dot(pEyePath[k].lastVertex.Ns, wiSample.value)) / wiSample.pdf;
                auto I = getScene().intersect(Ray(pEyePath[k].lastVertex, wiSample.value));
                if(!I) {
                    break;
                }

                auto idx = k + 1;

                pEyePath[idx].lastVertex = I;
                pEyePath[idx].lastVertexBRDF = getScene().shade(pEyePath[idx].lastVertex);
                pEyePath[idx].incidentDirection = -wiSample.value;
                pEyePath[idx].depth = pEyePath[idx - 1].depth + 1;
                pEyePath[idx].pdfLastVertex = wiSample.pdf * max(0.f, dot(I.Ns, -wiSample.value)) / sqr(I.distance);
                pEyePath[idx].pdf = pEyePath[idx - 1].pdf * pEyePath[idx].pdfLastVertex;
                pEyePath[idx].power = pEyePath[idx - 1].power * throughput;

                if(pEyePath[idx].pdfLastVertex == 0.f) {
                    break;
                }

                auto g = max(0.f, dot(pEyePath[idx - 1].lastVertex.Ns, wiSample.value)) / sqr(pEyePath[idx].lastVertex.distance);
                float reversePdf = pEyePath[idx - 1].lastVertexBRDF.pdf(wiSample.value, pEyePath[idx - 1].incidentDirection);

                pEyePath[idx].dVCM = Mis(1.f / pEyePath[idx].pdfLastVertex);
                pEyePath[idx].dVC = Mis(g / pEyePath[idx].pdfLastVertex) * (
                            m_MisVmWeightFactor +
                            pEyePath[idx - 1].dVCM +
                            pEyePath[idx - 1].dVC * Mis(reversePdf));
                pEyePath[idx].dVM = Mis(g / pEyePath[idx].pdfLastVertex) * (
                            1.f +
                            pEyePath[idx - 1].dVCM * m_MisVcWeightFactor +
                            pEyePath[idx - 1].dVM * Mis(reversePdf));

            } else {
                break;
            }
        }
    }
}

void VCMRenderer::preprocess() {
    m_EmissionSampler.initFrame(getScene());

    auto& fb = getFramebuffer();
    fb.setChannelCount(1 + m_nMaxDepth);
    fb.clear();

    auto size = getFramebuffer().getSize();
    auto spp = getSppCount();
    m_LightNodeArray = makeUniqueArray<Unique<EmissionVertex>>(size.x * size.y * spp);
    auto lightVertexCount = size.x * size.y * spp * getMaxLightPathDepth();
    m_LightPathArray = makeUniqueArray<PathVertex>(lightVertexCount);
    m_LightVertexPerTile = makeUniqueArray<std::vector<PathVertex*>>(getTileCount());

    m_EyePathArray = makeUniqueArray<PathVertex>(getMaxEyePathDepth() * getThreadCount());

    m_LightTraceOnly = false;
    m_UseVC = false;
    m_UseVM = false;
    m_Ppm = false;

    switch(m_AlgorithmType)
    {
    case kLightTrace:
        m_LightTraceOnly = true;
        break;
    case kPpm:
        m_Ppm   = true;
        m_UseVM = true;
        break;
    case kBpm:
        m_UseVM = true;
        break;
    case kBpt:
        m_UseVC = true;
        break;
    case kVcm:
        m_UseVC = true;
        m_UseVM = true;
        break;
    default:
        std::cerr << "Unknown algorithm requested" << std::endl;
        break;
    }

    Vec3f sceneCenter;
    boundingSphere(getScene().getBBox(), sceneCenter, m_SceneRadius);

    m_BaseRadius  = m_RadiusFactor * m_SceneRadius;

    m_nIterationCount = 0u;
}

void VCMRenderer::beginFrame() {
    auto spp = getSppCount();
    auto maxLightPathDepth = getMaxLightPathDepth();

    auto size = getFramebuffer().getSize();
    auto pathCount = size.x * size.y * spp;
    m_nLightPathVertexCount = pathCount * getMaxLightPathDepth();
    m_LightSubPathCount = pathCount;

    // Sample light paths for each pixel
    TileProcessingRenderer::processTiles([&](uint32_t threadID, uint32_t tileID, const Vec4u& viewport) {
        TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
            auto pixelID = getPixelIndex(x, y);
            auto lightNodeOffset = pixelID * spp;
            auto lightPathOffset = pixelID * spp * maxLightPathDepth;
            for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
                auto pLightPath = m_LightPathArray.get() + lightPathOffset + sampleID * maxLightPathDepth;
                uint32_t lightPathDepth;
                m_LightNodeArray[lightNodeOffset + sampleID] = sampleLightPath(getScene(), m_EmissionSampler,
                                                                              threadID, pixelID, pLightPath,
                                                                              lightPathDepth, maxLightPathDepth);
            }
        });
    });

    // Cluster light vertices per tile
    auto tileSize = getTileSize();

    std::for_each(m_LightVertexPerTile.get(), m_LightVertexPerTile.get() + getTileCount(), [](std::vector<PathVertex*>& v) {
        v.clear();
    });

    const auto& framebuffer = getFramebuffer();

    auto pEnd = m_LightPathArray.get() + m_nLightPathVertexCount;
    for(auto pLightVertex = m_LightPathArray.get(); pLightVertex != pEnd; ++pLightVertex) {
        Vec2f ndc;
        if(getCamera().getNDC(pLightVertex->lastVertex.P, ndc)) {
            auto uv = ndcToUV(ndc);
            auto pixel = getPixel(uv, framebuffer.getSize());

            auto tile = pixel / tileSize;
            auto tileID = getTileID(tile);

            m_LightVertexPerTile[tileID].emplace_back(pLightVertex);
        }
    }

    // Setup our radius, 1st iteration has aIteration == 0, thus offset
    float radius = m_BaseRadius;
    radius /= pow(float(m_nIterationCount + 1), 0.5f * (1 - m_RadiusAlpha));
    // Purely for numeric stability
    radius = max(radius, 1e-7f);
    const float radiusSqr = sqr(radius);

    // Factor used to normalise vertex merging contribution.
    // We divide the summed up energy by disk radius and number of light paths
    m_VmNormalization = 1.f / (radiusSqr * pi<float>() * m_LightSubPathCount);

    // MIS weight constant [tech. rep. (20)], with n_VC = 1 and n_VM = mLightPathCount
    const float etaVCM = (pi<float>() * radiusSqr) * m_LightSubPathCount;

    m_MisVmWeightFactor = m_UseVM ? Mis(etaVCM)       : 0.f;
    m_MisVcWeightFactor = m_UseVC ? Mis(1.f / etaVCM) : 0.f;

    if(m_UseVM)
    {
        m_HashGrid.Reserve(pathCount);
        m_HashGrid.Build(m_LightPathArray.get(), m_nLightPathVertexCount, radius);
    }
}

void VCMRenderer::endFrame() {
    ++m_nIterationCount;
}

void VCMRenderer::connectLightVerticesToCamera(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    const auto& camera = getCamera();
    const auto& scene = getScene();
    auto& framebuffer = getFramebuffer();

    auto viewportRange = Vec4u(viewport.xy(), viewport.xy() + viewport.zw());
    auto rcpPathCount = 1.f / m_LightSubPathCount;

    for(auto pLightVertex: m_LightVertexPerTile[tileID]) {
        if(!pLightVertex->pdf) {
            continue;
        }

        auto totalDepth = 1 + pLightVertex->depth;
        if(!acceptPathDepth(totalDepth) || totalDepth > m_nMaxDepth) {
            continue;
        }

        Vec2f ndc;
        if(camera.getNDC(pLightVertex->lastVertex.P, ndc)) {
            auto uv = ndcToUV(ndc);
            auto pixel = getPixel(uv, framebuffer.getSize());

            auto pixelID = BnZ::getPixelIndex(pixel, framebuffer.getSize());

            Ray shadowRay;
            auto surfaceToImageFactor =
                    camera.surfaceToImageFactor(pLightVertex->lastVertex,
                                                shadowRay);
            auto fr = pLightVertex->lastVertexBRDF.eval(pLightVertex->incidentDirection, shadowRay.dir);
            auto bsdfRevPdfW = pLightVertex->lastVertexBRDF.pdf(shadowRay.dir, pLightVertex->incidentDirection);

            const float imageToSurfaceFactor = 1.f / surfaceToImageFactor;

            const float cameraPdfA = imageToSurfaceFactor;
            const float wLight = Mis(cameraPdfA * rcpPathCount) * (
                m_MisVmWeightFactor + pLightVertex->dVCM + pLightVertex->dVC * Mis(bsdfRevPdfW));

            const float misWeight = m_LightTraceOnly ? 1.f : (1.f / (wLight + 1.f));

            if(surfaceToImageFactor > 0.f && fr != zero<Vec3f>()) {
                if(!scene.occluded(shadowRay)) {
                    auto imageToSurfaceFactor = 1.f / surfaceToImageFactor;
                    framebuffer.accumulate(0u, pixelID, Vec4f(misWeight * rcpPathCount * pLightVertex->power * fr * imageToSurfaceFactor, 0.f));
                    framebuffer.accumulate(totalDepth, pixelID, Vec4f(misWeight * rcpPathCount * pLightVertex->power * fr * imageToSurfaceFactor, 0.f));
                }
            }

        }
    }
}

void VCMRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    auto spp = getSppCount();

    auto& framebuffer = getFramebuffer();

    TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
        auto pixelID = getPixelIndex(x, y);

        for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
            for(auto i = 0u; i < framebuffer.getChannelCount(); ++i) {
                framebuffer.accumulate(i, pixelID, Vec4f(0.f, 0.f, 0.f, 1));
            }

            processSample(threadID, pixelID, sampleID, x, y);
        }
    });

    connectLightVerticesToCamera(threadID, tileID, viewport);

    if(m_bDisplayProgress) {
        auto l = debugLock();
        std::cerr << "Tile " << tileID << " / " << getTileCount() << " (" << (tileID * 100.f / getTileCount()) << " %)" << std::endl;
    }
}

std::string VCMRenderer::getAlgorithmType() const {
    const std::string mapping[] = {
        "lt", "ppm", "bpm", "bpt", "vcm"
    };
    return mapping[m_AlgorithmType];
}

void VCMRenderer::setAlgorithmType(const std::string& algorithmType) {
    const std::string mapping[] = {
        "lt", "ppm", "bpm", "bpt", "vcm"
    };
    auto it = std::find(std::begin(mapping), std::end(mapping), algorithmType);
    if(it == std::end(mapping)) {
        std::cerr << "Unrecognized algorithm type for VCMRenderer" << std::endl;
        m_AlgorithmType = kVcm;
        return;
    }
    m_AlgorithmType = AlgorithmType(it - std::begin(mapping));
}

void VCMRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
    getAttribute(xml, "maxDepth", m_nMaxDepth);
    getAttribute(xml, "displayProgress", m_bDisplayProgress);
    std::string algorithmType;
    getAttribute(xml, "algorithmType", algorithmType);
    setAlgorithmType(algorithmType);
    getAttribute(xml, "radiusFactor", m_RadiusFactor);
    getAttribute(xml, "radiusAlpha", m_RadiusAlpha);
}

void VCMRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
    setAttribute(xml, "maxDepth", m_nMaxDepth);
    setAttribute(xml, "displayProgress", m_bDisplayProgress);
    setAttribute(xml, "algorithmType", getAlgorithmType());
    setAttribute(xml, "radiusFactor", m_RadiusFactor);
    setAttribute(xml, "radiusAlpha", m_RadiusAlpha);
}

void VCMRenderer::doExposeIO(TwBar* bar) {
    atb::addVarRW(bar, ATB_VAR(m_nMaxDepth));
    atb::addVarRW(bar, ATB_VAR(m_RadiusFactor));
    atb::addVarRW(bar, ATB_VAR(m_RadiusAlpha));
    atb::addVarRW(bar, "algorithm", "kLightTrace,kPpm,kBpm,kBpt,kVcm", m_AlgorithmType);
}

}

