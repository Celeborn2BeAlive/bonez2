#include "PathtraceMISRenderer.hpp"
#include <bonez/scene/lights/AreaLight.hpp>

namespace BnZ {

std::string PathtraceMISRenderer::getName() const {
    return "PathtraceMISRenderer";
}

void PathtraceMISRenderer::preprocess() {
    m_EmissionSampler.initFrame(getScene());

    getFramebuffer().setChannelCount(1 + m_nMaxPathDepth);
}

//void PathtraceMISRenderer::processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const {
//    for(auto i = 0u; i <= m_nMaxPathDepth; ++i) {
//        getFramebuffer().accumulate(i, pixelID, Vec4f(0.f, 0.f, 0.f, 1.f));
//    }

//    auto s2D = getPixelSample(threadID, sampleID);
//    auto ray = getPrimaryRay(x, y, s2D);

//    Vec3f throughput = Vec3f(1.f);
//    Vec3f L = Vec3f(0.f);

//    auto I = getScene().intersect(ray);

//    if(acceptPathDepth(1)) {
//        getFramebuffer().accumulate(1, pixelID, Vec4f(I.Le, 1.f));
//        L += I.Le;
//    }

//    if(I) {
//        auto nextI = I;
//        Vec3f wo = -ray.dir.xyz();
//        float wiPdf = 1.f;

//        auto lightNode = m_EmissionSampler.sample(getScene(), getFloat(threadID), getFloat(threadID), getFloat2(threadID));

//        for(auto i = 1u; i < m_nMaxPathDepth; ++i) {
//            I = nextI;
//            auto brdf = getScene().shade(I);

//            Sample3f wi;
//            auto fr = brdf.sample(wo, wi, getFloat2(threadID));

//            nextI = getScene().intersect(secondaryRay(I, wi.value));
//            bool hasHit = false;

//            if(acceptPathDepth(i + 1)) {
//                // Hit the light source
////                if(nextI && nextI.Le != zero<Vec3f>()) {
////                    hasHit = false;

////                    auto contrib = throughput * fr * dot(I.Ns, wi.value) * nextI.Le / wi.pdf;

////                    auto meshID = nextI.meshID;
////                    auto lightID = getScene().getGeometry().getMesh(meshID).m_nLightID;

////                    const auto& pLight = getScene().getLightContainer().getLight(lightID);
////                    const auto* pAreaLight = static_cast<const AreaLight*>(pLight.get());

////                    auto pdf2 = m_EmissionSampler.pdf(lightID) * pAreaLight->pdf(nextI, getScene());
////                    auto pdf1 = wiPdf * max(0.f, dot(nextI.Ns, wo)) / sqr(nextI.distance);

////                    auto misWeight = Mis(pdf1) / (Mis(pdf1) + Mis(pdf2));
////                    getFramebuffer().accumulate(i + 1, pixelID, Vec4f(misWeight * contrib, 1.f));
////                    L += misWeight * contrib;
////                }

//                // Direct illumination
//                Ray shadowRay;
//                auto G = lightNode->G(I, shadowRay);
//                if(G > 0.f && !getScene().occluded(shadowRay)) {
//                    auto contrib = throughput * lightNode->power() * lightNode->Le(-shadowRay.dir)
//                            * G * brdf.eval(shadowRay.dir, wo);

//                    auto g = lightNode->g(I);
//                    auto pdf1 = lightNode->pdf();
//                    auto pdf2 = brdf.pdf(wo, shadowRay.dir) * g;

////                    const auto* pAreaLight = static_cast<const AreaLight*>(lightNode->m_pLight);
////                    auto lightID = getScene().getLightContainer().getLightID(pAreaLight);

//                    auto misWeight = 1.f;

//                    if(hasHit) {
//                        misWeight = Mis(pdf1) / (Mis(pdf1) + Mis(pdf2));
//                    }

//                    getFramebuffer().accumulate(i + 1, pixelID, Vec4f(misWeight * contrib, 1.f));
//                    L += misWeight * contrib;
//                }

//                if(fr != zero<Vec3f>() && wi.pdf > 0.f) {
//                    throughput *= fr * dot(I.Ns.xyz(), wi.value) / wi.pdf;
//                    wiPdf = wi.pdf;
//                    I = getScene().intersect(secondaryRay(I, wi.value));
//                    wo = -wi.value;
//                }
//            }
//        }
//    }

//    getFramebuffer().accumulate(0, pixelID, Vec4f(L, 1.f));
//}

void PathtraceMISRenderer::processSample(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y) const {
    for(auto i = 0u; i <= m_nMaxPathDepth; ++i) {
        getFramebuffer().accumulate(i, pixelID, Vec4f(0.f, 0.f, 0.f, 1.f));
    }

    auto s2D = getPixelSample(threadID, sampleID);
    auto ray = getPrimaryRay(x, y, s2D);

    Vec3f throughput = Vec3f(1.f);
    Vec3f L = Vec3f(0.f);

    auto I = getScene().intersect(ray);
    Vec3f wo = -ray.dir.xyz();

    if(acceptPathDepth(1)) {
        getFramebuffer().accumulate(1, pixelID, Vec4f(I.Le, 1.f));
        L += I.Le;
    }

    auto lightNode = m_EmissionSampler.sample(getScene(), getFloat(threadID), getFloat(threadID), getFloat2(threadID));

    for(auto i = 1u; I && i < m_nMaxPathDepth; ++i) {
        auto brdf = getScene().shade(I);

        Sample3f wi;
        auto fr = brdf.sample(wo, wi, getFloat2(threadID));
        auto nextI = getScene().intersect(secondaryRay(I, wi.value));

        if(acceptPathDepth(i + 1)) {
            bool hasHitLight = false;
            if(nextI && nextI.Le != zero<Vec3f>()) {
                hasHitLight = true;

                auto contrib = throughput * fr * dot(I.Ns, wi.value) * nextI.Le / wi.pdf;

                auto meshID = nextI.meshID;
                auto lightID = getScene().getGeometry().getMesh(meshID).m_nLightID;

                const auto& pLight = getScene().getLightContainer().getLight(lightID);
                const auto* pAreaLight = static_cast<const AreaLight*>(pLight.get());

                auto pdf2 = m_EmissionSampler.pdf(lightID) * pAreaLight->pdf(nextI, getScene());
                auto pdf1 = wi.pdf * max(0.f, dot(nextI.Ns, -wi.value)) / sqr(nextI.distance);

                auto misWeight = Mis(pdf1) / (Mis(pdf1) + Mis(pdf2));

                getFramebuffer().accumulate(i + 1, pixelID, Vec4f(misWeight * contrib, 0.f));
                L += misWeight * contrib;
            }

            Ray shadowRay;
            auto G = lightNode->G(I, shadowRay);
            if(G > 0.f && !getScene().occluded(shadowRay)) {
                auto contrib = throughput * lightNode->power() * lightNode->Le(-shadowRay.dir)
                        * G * brdf.eval(shadowRay.dir, wo);

                auto misWeight = 1.f;

                if(hasHitLight) {
                    auto g = lightNode->g(I);
                    auto pdf1 = lightNode->pdf();
                    auto pdf2 = brdf.pdf(wo, shadowRay.dir) * g;

                    misWeight = Mis(pdf1) / (Mis(pdf1) + Mis(pdf2));
                }

                getFramebuffer().accumulate(i + 1, pixelID, Vec4f(misWeight * contrib, 0.f));
                L += misWeight * contrib;
            }
        }

        if(i < m_nMaxPathDepth - 1) {
            if(fr != zero<Vec3f>() && wi.pdf > 0.f) {
                throughput *= fr * dot(I.Ns, wi.value) / wi.pdf;
                I = nextI;
                wo = -wi.value;
            }
        }
    }

    getFramebuffer().accumulate(0, pixelID, Vec4f(L, 0.f));
}

void PathtraceMISRenderer::doExposeIO(TwBar* bar) {
    atb::addVarRW(bar, ATB_VAR(m_nMaxPathDepth));
}

void PathtraceMISRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
    getAttribute(xml, "maxDepth", m_nMaxPathDepth);
}

void PathtraceMISRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
    setAttribute(xml, "maxDepth", m_nMaxPathDepth);
}

void PathtraceMISRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    processTileSamples(viewport, [this, threadID](uint32_t x, uint32_t y, uint32_t pixelID, uint32_t sampleID) {
        processSample(threadID, pixelID, sampleID, x, y);
    });
}

}
