#include "Renderer.hpp"

#include <bonez/sys/threads.hpp>
#include <bonez/image/Image.hpp>

#include <bonez/sampling/patterns.hpp>

#include <bonez/scene/lights/EmissionSampler.hpp>

namespace BnZ {

Vec3f estimateDirectLight(
        const Scene& scene,
        const EmissionSampler& sampler,
        const SurfacePoint& point, const Vec3f& wo,
        const BRDF& brdf,
        float sLightIdx, float s1D, const Vec2f& s2D) {
    auto lightNode = sampler.sample(scene, sLightIdx, s1D, s2D);
    Ray shadowRay;
    auto G = lightNode->G(point, shadowRay);

    if(G < 0.f || scene.occluded(shadowRay)) {
        return zero<Vec3f>();
    }

    auto L = lightNode->power() * lightNode->Le(-shadowRay.dir);
    auto wi = Vec3f(shadowRay.dir);
    return brdf.eval(wi, wo) * L * G / lightNode->pdf();

//    RaySample r;
//    auto L = sampler.sample(scene, point, r, sLightIdx, s1D, s2D);

//    if(r.pdf == 0.f || scene.occluded(r.value)) {
//        return zero<Vec3f>();
//    }

//    auto wi = Vec3f(r.value.dir);
//    return brdf.eval(wi, wo) * L * dot(point.Ns, r.value.dir) / r.pdf;
}

Vec3f estimateDirectLightAndDirectIrradiance(
        const Scene& scene,
        const EmissionSampler& sampler,
        const SurfacePoint& point, const Vec3f& wo,
        const BRDF& brdf,
        Vec3f& directIrradiance,
        float sLightIdx, float s1D, const Vec2f& s2D) {
    auto lightNode = sampler.sample(scene, sLightIdx, s1D, s2D);
    Ray shadowRay;
    auto G = lightNode->G(point, shadowRay);

    if(G < 0.f || scene.occluded(shadowRay)) {
        return zero<Vec3f>();
    }

    auto L = lightNode->power() * lightNode->Le(-shadowRay.dir);
    auto wi = Vec3f(shadowRay.dir);

    directIrradiance = L * G / lightNode->pdf();

    return brdf.eval(wi, wo) * directIrradiance;

//    RaySample r;
//    auto L = sampler.sample(scene, point, r, sLightIdx, s1D, s2D);

//    if(r.pdf == 0.f || scene.occluded(r.value)) {
//        return zero<Vec3f>();
//    }

//    directIrradiance = L * dot(point.Ns, r.value.dir) / r.pdf;

//    auto wi = Vec3f(r.value.dir);
//    return brdf.eval(wi, wo) * directIrradiance;
}

Vec3f estimateDirectIrradiance(
        const Scene& scene,
        const EmissionSampler& sampler,
        const SurfacePoint& point,
        float sLightIdx, float s1D, const Vec2f& s2D) {

    auto lightNode = sampler.sample(scene, sLightIdx, s1D, s2D);
    Ray shadowRay;
    auto G = lightNode->G(point, shadowRay);

    if(G < 0.f || scene.occluded(shadowRay)) {
        return zero<Vec3f>();
    }

    auto L = lightNode->power() * lightNode->Le(-shadowRay.dir);

    return L * G / lightNode->pdf();

//    RaySample r;
//    auto L = sampler.sample(scene, point, r, sLightIdx, s1D, s2D);

//    if(r.pdf == 0.f || scene.occluded(r.value)) {
//        return zero<Vec3f>();
//    }

//    return L * dot(point.Ns, r.value.dir) / r.pdf;
}

}

