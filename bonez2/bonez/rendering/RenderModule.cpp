#include "RenderModule.hpp"
#include <bonez/utils/ColorMap.hpp>

namespace BnZ {

static const std::string RES_IMAGE_EXT = "png";
static const std::string RES_FRAMEBUFFER_EXT = "raw";
static const std::string RES_STATS_DIR = "stats";

template<typename T>
static void storeArray(const FilePath& path, const std::vector<T>& values) {
    std::ofstream file(path.c_str());
    for(const auto& v: values) {
        file << v << " ";
    }
}

struct RenderStatistics {
    // All timings are in milliseconds
    float totalTime;
    float initFrameTime;
    uint32_t iterCount;

    // For each spp, contains the TOTAL rendering time to reach it
    // (ie. initFrameTime + processing time of all spp):
    std::vector<float> processingTimes;

    std::vector<Vec3f> rmse; // Relative root mean square error for each spp and each channel
    std::vector<float> rmseFloat; // Relative root mean square error for each spp
};

static void storeFramebuffer(const FilePath& pngDir,
                             const FilePath& rawDir,
                             const FilePath& baseName,
                             float gamma,
                             const Framebuffer& framebuffer,
                             bool storeRaw = false) {
    createDirectory(pngDir.str());
    createDirectory(rawDir.str());

    FilePath pngFile = pngDir + baseName.addExt("." + RES_IMAGE_EXT);
    FilePath rawFile = rawDir + baseName.addExt("." + RES_FRAMEBUFFER_EXT);

    Image copy = framebuffer.getChannel(0);
    copy.flipY();
    copy.divideByAlpha();

    if(storeRaw) {
        storeRawImage(rawFile.str(), copy);
    }

    copy.applyGamma(gamma);
    storeImage(pngFile.str(), copy);

    for(auto i = 0u; i < framebuffer.getChannelCount(); ++i) {
        FilePath pngFile = pngDir + baseName.addExt("." + toString3(i) + "." + RES_IMAGE_EXT);

        Image copy = framebuffer.getChannel(i);
        copy.flipY();
        copy.divideByAlpha();

        copy.applyGamma(gamma);
        storeImage(pngFile.str(), copy);
    }

    {
        // #CodePourri
        FilePath pngFile = pngDir + baseName.addExt("." + toString3(1) + ".occlusionMap." + RES_IMAGE_EXT);

        Image copy = framebuffer.getChannel(1);
        copy.flipY();
        copy.divideByAlpha();
        //copy.applyGamma(gamma);

        auto maxValue = 0.f;
        for(const auto& color: copy) {
            maxValue = max(maxValue, color.x);
        }

        for(auto& color: copy) {
            auto value = sampleColorMap(HEAT_MAP, 1 - color.x / maxValue);
            color = Vec4f(value, 1.f);
        }

        storeImage(pngFile.str(), copy);

        Image bar(256, 1);
        for(auto x = 0u; x < bar.getWidth(); ++x) {
            bar(x, 0) = Vec4f(sampleColorMap(HEAT_MAP, float(x) / bar.getWidth()), 1.f);
        }
        storeImage("colorMap.png", bar);
    }
}

void RenderModule::storeResult(const FilePath& resultDir, const std::string& resultPrefix, uint32_t index, const RenderStatistics& stats, float gamma, const Framebuffer& framebuffer) {
    FilePath baseName(resultPrefix + "." + toString3(index));

    FilePath pngDir = resultDir + RES_IMAGE_EXT;
    FilePath rawDir = resultDir + RES_FRAMEBUFFER_EXT;
    FilePath statsDir = resultDir + RES_STATS_DIR;

    storeFramebuffer(pngDir, rawDir, baseName, gamma, framebuffer);

    createDirectory(statsDir.str());

    storeArray(statsDir + baseName.addExt(".rmse"), stats.rmse);
    storeArray(statsDir + baseName.addExt(".rmseFloat"), stats.rmseFloat);
    storeArray(statsDir + baseName.addExt(".processingTimes"), stats.processingTimes);
}

void RenderModule::storeResultStep(
        const FilePath& resultDir,
        const std::string& resultPrefix,
        uint32_t index,
        const RenderStatistics& stats,
        float gamma,
        const Framebuffer& framebuffer,
        const Image& refImage) {
    FilePath baseName(resultPrefix + "." + toString3(index) + "." + toString6(stats.iterCount) + "." + toString(uint64_t(stats.totalTime)));

    FilePath dir = resultDir + "storeStep";
    createDirectory(dir);

    FilePath file = dir + baseName.addExt("." + RES_IMAGE_EXT);

    Vec3f rmse;
    auto relativeDiffImage = compateRelativeSqrDiffImage(refImage, framebuffer.getChannel(0), rmse);
    float fRmse = (rmse.r + rmse.g + rmse.b) / 3.f;

    Image copy = framebuffer.getChannel(0);
    copy.flipY();
    copy.divideByAlpha();
    copy.applyGamma(gamma);
    storeImage(file.str(), copy);

    // STORE SPECKLES DETECT
//    copy = framebuffer.getChannel(4);
//    copy.flipY();
//    copy.divideByAlpha();
//    storeImage((dir + FilePath("speckles." + baseName.str()).addExt("." + RES_IMAGE_EXT)).str(), copy);

    relativeDiffImage.flipY();
    FilePath diffDir = dir + "diff";
    createDirectory(diffDir);
    storeImage((diffDir + baseName.addExt("-" + toString(fRmse) + "." + RES_IMAGE_EXT)).c_str(), relativeDiffImage);
}

void RenderModule::runCompareRenderGroup(
        const std::string& resultPrefix,
        const FilePath& resultPath,
        const FilePath& resultDocumentPath,
        tinyxml2::XMLDocument& resultsDocument,
        tinyxml2::XMLElement& pGroupElement,
        const tinyxml2::XMLElement* pHeritedSettings,
        const Scene& scene,
        const Camera& camera,
        int iterCount,
        int storeInterval,
        float processingTime,
        float storeStep,
        float minRMSE,
        uint32_t& renderCount,
        const Image& referenceImage) {
    auto pNewSettings = pGroupElement.GetDocument()->NewElement("Settings");
    // Copy existing settings
    if(pHeritedSettings) {
        for(auto pAttr = pHeritedSettings->FirstAttribute();
            pAttr;
            pAttr = pAttr->Next()) {
            pNewSettings->SetAttribute(pAttr->Name(), pAttr->Value());
        }
    }
    // Copy group settings
    for(auto pAttr = pGroupElement.FirstAttribute();
        pAttr;
        pAttr = pAttr->Next()) {
        pNewSettings->SetAttribute(pAttr->Name(), pAttr->Value());
    }

    for(auto pChildElement = pGroupElement.FirstChildElement();
        pChildElement; pChildElement = pChildElement->NextSiblingElement()) {
        auto name = std::string(pChildElement->Name());
        if(name == "RenderGroup") {
            runCompareRenderGroup(resultPrefix,
                                  resultPath,
                                  resultDocumentPath,
                                  resultsDocument,
                                  *pChildElement,
                                  pNewSettings,
                                  scene,
                                  camera,
                                  iterCount,
                                  storeInterval,
                                  processingTime,
                                  storeStep,
                                  minRMSE,
                                  renderCount,
                                  referenceImage);
        } else if(name == "Renderer") {
            auto pRendererSettings = pGroupElement.GetDocument()->NewElement("Renderer");

            // copy herited settings
            for(auto pAttr = pNewSettings->FirstAttribute();
                pAttr;
                pAttr = pAttr->Next()) {
                pRendererSettings->SetAttribute(pAttr->Name(), pAttr->Value());
            }

            // copy herited settings
            for(auto pAttr = pChildElement->FirstAttribute();
                pAttr;
                pAttr = pAttr->Next()) {
                pRendererSettings->SetAttribute(pAttr->Name(), pAttr->Value());
            }

            renderCompare(resultPrefix,
                          resultPath,
                          resultDocumentPath,
                          resultsDocument,
                          *pRendererSettings,
                          scene,
                          camera,
                          iterCount,
                          storeInterval,
                          processingTime,
                          storeStep,
                          minRMSE,
                          renderCount,
                          referenceImage);
            ++renderCount;
        }
    }
}

void RenderModule::renderCompare(
        const std::string& resultPrefix,
        const FilePath& resultPath,
        const FilePath& resultDocumentPath,
        tinyxml2::XMLDocument& resultsDocument,
        const tinyxml2::XMLElement& rendererSettings,
        const Scene& scene,
        const Camera& camera,
        int iterCount,
        int storeInterval,
        float processingTime,
        float storeStep,
        float minRMSE,
        uint32_t index,
        const Image& referenceImage)
{
    clearCPUFramebuffer();
    auto& framebuffer = m_CPUFramebuffer;

    auto pRendererStats = resultsDocument.NewElement("Stats");
    auto pResult = resultsDocument.NewElement("Result");
    auto pRendererSettings = resultsDocument.NewElement("Renderer");

    RenderStatistics stats;

    std::cerr << "GPU Memory1 = " << getGPUMemoryInfoCurrentAvailable() << std::endl;
    {
        auto pRenderer = m_RendererManager.createRenderer(rendererSettings.Attribute("name"));

        pRenderer->setStatisticsOutput(pRendererStats);

        std::clog << "Render " << index << "..." << std::endl;

        float storeStepAccum = 0.f;

        auto start = getMicroseconds();

        pRenderer->init(scene, camera, framebuffer, &rendererSettings);

        auto end = getMicroseconds();

        stats.initFrameTime = us2ms(end - start);
        stats.totalTime = stats.initFrameTime;
        stats.iterCount = 0u;

        storeStepAccum = stats.totalTime;

        auto rmse = computeRMSE(referenceImage, framebuffer.getChannel(0));
        auto rmseFloat = (rmse.r + rmse.g + rmse.b) / 3.f;
        std::cerr << "RMSE = " << rmse << " | " << rmseFloat << " 0 SPP " <<
                    stats.totalTime << " / " << processingTime << std::endl;

        stats.processingTimes.emplace_back(stats.initFrameTime);
        stats.rmse.emplace_back(rmse);
        stats.rmseFloat.emplace_back(rmseFloat);

        if(storeStep > 0.f && storeStepAccum > storeStep) {
            storeResultStep(resultPath, resultPrefix, index, stats, m_fGamma, framebuffer, referenceImage);
            while(storeStepAccum > storeStep) {
                storeStepAccum -= storeStep;
            }
        }


        while((iterCount < 0 && stats.totalTime < processingTime && rmseFloat > minRMSE) || (iterCount >= 0 && iterCount--)) {
            start = getMicroseconds();

            pRenderer->render();

            auto ellapsedTime = us2ms(getMicroseconds() - start);

            stats.iterCount += 1u;
            stats.totalTime += ellapsedTime;
            storeStepAccum += ellapsedTime;

            rmse = computeRMSE(referenceImage, framebuffer.getChannel(0));
            rmseFloat = (rmse.r + rmse.g + rmse.b) / 3.f;

            std::cerr << "RMSE = " << rmse << " | " << rmseFloat << " "  << stats.totalTime << " / " <<
                         processingTime << " - " << stats.iterCount << std::endl;

            stats.processingTimes.emplace_back(stats.totalTime);
            stats.rmse.emplace_back(rmse);
            stats.rmseFloat.emplace_back(rmseFloat);

            if(iterCount >= 0 && storeInterval > 0 && stats.iterCount % storeInterval == 0) {
                storeResultStep(resultPath, resultPrefix, index, stats, m_fGamma, framebuffer, referenceImage);
            } else if(iterCount < 0 && storeStep > 0.f && storeStepAccum > storeStep) {
                storeResultStep(resultPath, resultPrefix, index, stats, m_fGamma, framebuffer, referenceImage);
                while(storeStepAccum > storeStep) {
                    storeStepAccum -= storeStep;
                }
            }
        }

        storeResult(resultPath, resultPrefix, index, stats, m_fGamma, framebuffer);

        pRenderer->storeStatistics();
        pRenderer->storeSettings(*pRendererSettings);
    }
    std::cerr << "GPU Memory2 = " << getGPUMemoryInfoCurrentAvailable() << std::endl;

    resultsDocument.RootElement()->InsertEndChild(pResult);

    setAttribute(*pResult, "index", index);
    setAttribute(*pResult, "gamma", m_fGamma);

    setAttribute(*pResult, "totalTime_ms", stats.totalTime);
    setAttribute(*pResult, "totalTime_sec", ms2sec(stats.totalTime));
    setAttribute(*pResult, "initTime_ms", stats.initFrameTime);
    setAttribute(*pResult, "initTime_sec", ms2sec(stats.initFrameTime));
    setAttribute(*pResult, "RMSE", stats.rmseFloat.back());
    setAttribute(*pResult, "frameRenderCount", stats.iterCount);
    setAttribute(*pResult, "meanTimePerFrame_ms", stats.totalTime / stats.iterCount);

    pResult->InsertEndChild(pRendererSettings);

    pResult->InsertEndChild(pRendererStats);

    resultsDocument.SaveFile(resultDocumentPath.c_str());
}

static const char* REFERENCE_FILENAME = "reference.bnz.xml";
static const char* RENDER_FILENAME = "render.bnz.xml";

void RenderModule::renderReference(const tinyxml2::XMLDocument& configDocument, const FilePath& resultPath,
                                   const Scene& scene, const Camera& camera) {
    auto pReference = configDocument.RootElement()->FirstChildElement("Reference");
    if(pReference) {
        auto pRendererSettings = pReference->FirstChildElement("Renderer");
        if(pRendererSettings) {
            bool forceCompute = true;
            FilePath referencePath = resultPath + REFERENCE_FILENAME;
            if((getAttribute(*pReference, "forceCompute", forceCompute) && forceCompute) || !exists(referencePath)) {
                std::clog << "Render the reference..." << std::endl;

                getAttribute(*pReference, "gamma", m_fGamma);

                auto pRenderer = m_RendererManager.createRenderer(pRendererSettings->Attribute("name"));

                clearCPUFramebuffer();
                auto startTime = getMicroseconds();

                pRenderer->init(scene, camera, m_CPUFramebuffer, pRendererSettings);
                pRenderer->render();

                auto renderTime = us2ms(getMicroseconds() - startTime);

                storeFramebuffer(resultPath, resultPath, "reference", m_fGamma, m_CPUFramebuffer, true);

                tinyxml2::XMLDocument referenceDoc;
                auto pResult = referenceDoc.NewElement("Reference");
                referenceDoc.InsertFirstChild(pResult);

                setAttribute(*pResult, "gamma", m_fGamma);

                setAttribute(*pResult, "renderTime_ms", renderTime);
                setAttribute(*pResult, "renderTime_sec", ms2sec(renderTime));

                auto pRendererSettings = referenceDoc.NewElement("Renderer");
                pRenderer->storeSettings(*pRendererSettings);
                pResult->InsertEndChild(pRendererSettings);

                referenceDoc.SaveFile(referencePath.c_str());
            }
        }
    }
}

void RenderModule::renderConfig(const FilePath& basePath, const std::string& name, const ConfigManager& configManager,
                                Scene& scene, float zNear, float zFar) {
    tinyxml2::XMLDocument configDoc;

    ProjectiveCamera camera;
    FilePath path = configManager.loadConfig(name, camera, scene,
                                             m_CPUFramebuffer.getWidth(), m_CPUFramebuffer.getHeight(),
                                             zNear, zFar, &configDoc);

    auto resultPath = basePath + FilePath("results") + name;
    createDirectory(basePath + FilePath("results"));
    createDirectory(resultPath);

    renderReference(configDoc, resultPath, scene, camera);

    Shared<Image> referenceImage = loadRawImage(resultPath + "reference.raw");
    if(!referenceImage) {
        std::cerr << "No reference" << std::endl;
        return;
    }

    referenceImage->flipY();

    {
        tinyxml2::XMLDocument rendersDoc;
        if(tinyxml2::XML_NO_ERROR != rendersDoc.LoadFile(
                    (path + RENDER_FILENAME).c_str())) {
            std::clog << "No " << RENDER_FILENAME << " for the config." << std::endl;
            return;
        }

        auto pRenders = rendersDoc.RootElement();
        if(!pRenders) {
            std::clog << "Invalide file format (no root element in " << RENDER_FILENAME << ")" << std::endl;
            return;
        }

        float processingTime = 0;
        float storeStep = -1;

        auto resultPrefix = toString(getMicroseconds());
        resultPath = resultPath + resultPrefix;
        createDirectory(resultPath);

        FilePath resultDocumentPath = resultPath + "results.bnz.xml";

        rendersDoc.SaveFile((resultPath + RENDER_FILENAME).c_str());

        tinyxml2::XMLDocument resultsDocument;
        auto pResults = resultsDocument.NewElement("Results");
        resultsDocument.InsertFirstChild(pResults);

        uint32_t renderCount = 0;
        int iterCount = -1;
        int storeInterval = -1;
        float minRMSE = 0.f;

        if(getAttribute(*pRenders, "processingTime", processingTime)) {
            getAttribute(*pRenders, "storeStep", storeStep);
            getAttribute(*pRenders, "gamma", m_fGamma);
            getAttribute(*pRenders, "iterCount", iterCount);
            getAttribute(*pRenders, "storeInterval", storeInterval);
            getAttribute(*pRenders, "minRMSE", minRMSE);

            runCompareRenderGroup(resultPrefix,
                                  resultPath,
                                  resultDocumentPath,
                                  resultsDocument,
                                  *pRenders,
                                  nullptr,
                                  scene,
                                  camera,
                                  iterCount,
                                  storeInterval,
                                  processingTime,
                                  storeStep,
                                  minRMSE,
                                  renderCount,
                                  *referenceImage);
        } else {
            std::cerr << "No processingTime attribute" << std::endl;
        }
    }
}

}
