#pragma once

#include <cmath>
#include <bonez/types.hpp>

#include "constants.hpp"

namespace BnZ {

using glm::distance;
using glm::length;
using glm::radians;
using glm::dot;
using glm::cross;
using glm::cos;
using glm::sin;
using glm::tan;
using glm::abs;
using glm::floor;
using glm::sign;
using glm::max;
using glm::min;
using glm::translate;
using glm::rotate;
using glm::scale;
using glm::perspective;
using glm::ortho;
using glm::lookAt;
using glm::clamp;
using glm::normalize;
using glm::inverse;
using glm::fract;
using glm::pow;
using glm::sqrt;

using glm::isPowerOfTwo;

inline Vec3f getColor(uint32_t n) {
    return fract(
                sin(
                    float(n) * Vec3f(12.9898, 78.233, 56.128)
                    )
                * 43758.5453f
                );
}

template<typename T>
inline T sqr(const T& value) {
    return value * value;
}

inline float distanceSquared(const Vec3f& A, const Vec3f& B) {
    auto v = B - A;
    return dot(v, v);
}

inline float lengthSquared(const Vec3f& v) {
    return dot(v, v);
}

/**
 * @brief cartesianToSpherical
 * @param direction a unit length 3D vector
 * @return Spherical coordinates (phi, theta) of direction with phi in [0, 2pi[ and theta in [-pi, pi]
 */
inline Vec2f cartesianToSpherical(const Vec3f& direction) {
    return Vec2f(atan2(direction.x, direction.z), asin(direction.y));
}

/**
 * @brief sphericalToCartesian
 * @param angles The spherical angles: angles.x is phi and angles.y is theta
 * @return Cartesian coordinates
 */
inline Vec3f sphericalToCartesian(const Vec2f& angles) {
    auto cosTheta = cos(angles.y);
    return Vec3f(sin(angles.x) * cosTheta,
                     sin(angles.y),
                     cos(angles.x) * cosTheta);
}

inline Vec2f getSphericalAngles(const Vec2f& uv) {
    return uv * Vec2f(two_pi<float>(), pi<float>());
}

// Standard spherical mapping
inline Vec3f sphericalMapping(const Vec2f& uv, float& sinTheta) {
    auto phiTheta = getSphericalAngles(uv);
    sinTheta = sin(phiTheta.y);

    return Vec3f(cos(phiTheta.x) * sinTheta,
                 sin(phiTheta.x) * sinTheta,
                 cos(phiTheta.y));
}

inline Vec3f sphericalMapping(const Vec2f& uv) {
    float sinTheta;
    return sphericalMapping(uv, sinTheta);
}

inline Vec2f rcpSphericalMapping(const Vec3f& wi, float& sinTheta) {
    sinTheta = sqrt(sqr(wi.x) + sqr(wi.y));
    Vec2f phiTheta;

    phiTheta.x = atan2(wi.y / sinTheta, wi.x / sinTheta);
    phiTheta.y = atan2(sinTheta, wi.z);

    if(phiTheta.x < 0.f) {
        phiTheta.x += two_pi<float>();
    }

    if(phiTheta.y < 0.f) {
        phiTheta.y += two_pi<float>();
    }

    return phiTheta * Vec2f(one_over_two_pi<float>(), one_over_pi<float>());
}

inline Vec2f rcpSphericalMapping(const Vec3f& wi) {
    float sinTheta;
    return rcpSphericalMapping(wi, sinTheta);
}

inline Vec2f getNDC(const Vec2f rasterPosition, Vec2f imageSize) {
    return Vec2f(-1) + 2.f * rasterPosition / imageSize;
}

inline Vec3f getDualParaboloidNormal(const Vec2f& uv) {
    if(uv.x < 0.5f) {
        return Vec3f(getNDC(uv, Vec2f(0.5f, 1.f)), 1.f);
    }
    auto ndc = getNDC(uv - Vec2f(0.5f, 0.f), Vec2f(0.5f, 1.f));
    ndc.x = -ndc.x;
    return Vec3f(ndc, -1.f);
}

inline Vec3f dualParaboloidMapping(const Vec2f& uv) {
    auto N = getDualParaboloidNormal(uv);
    auto scale = 2.f / dot(N, N);
    return scale * N - Vec3f(0, 0, N.z);
}

inline Vec2f rcpDualParaboloidMapping(const Vec3f& wi) {
    if(wi.z > 0.f) {
        // left
        auto scaledN = wi + Vec3f(0, 0, 1);
        auto N = scaledN / scaledN.z;
        return Vec2f(0.25f * (N.x + 1.f), 0.5f * (N.y + 1.f));
    }
    //right
    auto scaledN = wi + Vec3f(0, 0, -1);
    auto N = -scaledN / scaledN.z;
    return Vec2f(0.5f + 0.25f * (-N.x + 1.f), 0.5f * (N.y + 1.f));
}

inline Vec3f getOrthogonalUnitVector(const Vec3f& v) {
    if(abs(v.y) > abs(v.x)) {
        float rcpLength = 1.f / length(Vec2f(v.x, v.y));
        return rcpLength * Vec3f(v.y, -v.x, 0.f);
    }
    float rcpLength = 1.f / length(Vec2f(v.x, v.z));
    return rcpLength * Vec3f(v.z, 0.f, -v.x);
}

inline Mat3f frameY(const Vec3f& yAxis) {
    Mat3f matrix;

    matrix[1] = Vec3f(yAxis);
    if(abs(yAxis.y) > abs(yAxis.x)) {
        float rcpLength = 1.f / length(Vec2f(yAxis.x, yAxis.y));
        matrix[0] = rcpLength * Vec3f(yAxis.y, -yAxis.x, 0.f);
    } else {
        float rcpLength = 1.f / length(Vec2f(yAxis.x, yAxis.z));
        matrix[0] = rcpLength * Vec3f(yAxis.z, 0.f, -yAxis.x);
    }
    matrix[2] = cross(matrix[0], yAxis);
    return matrix;
}

// zAxis should be normalized
inline Mat3f frameZ(const Vec3f& zAxis) {
    Mat3f matrix;

    matrix[2] = Vec3f(zAxis);
    if(abs(zAxis.y) > abs(zAxis.x)) {
        float rcpLength = 1.f / length(Vec2f(zAxis.x, zAxis.y));
        matrix[0] = rcpLength * Vec3f(zAxis.y, -zAxis.x, 0.f);
    } else {
        float rcpLength = 1.f / length(Vec2f(zAxis.x, zAxis.z));
        matrix[0] = rcpLength * Vec3f(zAxis.z, 0.f, -zAxis.x);
    }
    matrix[1] = cross(zAxis, matrix[0]);
    return matrix;
}

inline Mat4f frameY(const Vec3f& origin, const Vec3f& yAxis) {
    Mat4f matrix;
    matrix[3] = Vec4f(origin, 1);

    matrix[1] = Vec4f(yAxis, 0);
    if(abs(yAxis.y) > abs(yAxis.x)) {
        float rcpLength = 1.f / length(Vec2f(yAxis.x, yAxis.y));
        matrix[0] = rcpLength * Vec4f(yAxis.y, -yAxis.x, 0.f, 0.f);
    } else {
        float rcpLength = 1.f / length(Vec2f(yAxis.x, yAxis.z));
        matrix[0] = rcpLength * Vec4f(yAxis.z, 0.f, -yAxis.x, 0.f);
    }
    matrix[2] = Vec4f(cross(Vec3f(matrix[0]), yAxis), 0);
    return matrix;
}

inline void faceForward(const Vec3f& wi, Vec3f& N) {
    if(dot(wi, N) < 0.f) {
        N = -N;
    }
}

inline uint32_t getPixelIndex(uint32_t x, uint32_t y, const Vec2u& imageSize) {
    return x + y * imageSize.x;
}

inline uint32_t getPixelIndex(const Vec2u& pixel, const Vec2u& imageSize) {
    return getPixelIndex(pixel.x, pixel.y, imageSize);
}

inline Vec2u getPixel(uint32_t index, const Vec2u& imageSize) {
    return Vec2u(index % imageSize.x, index / imageSize.x);
}

inline Vec2u getPixel(const Vec2f& uv, const Vec2u& imageSize) {
    return clamp(Vec2u(uv * Vec2f(imageSize)),
                 Vec2u(0),
                 Vec2u(imageSize.x - 1, imageSize.y - 1));
}

inline Vec2u rasterPositionToPixel(const Vec2f& rasterPosition,
                                   const Vec2u& imageSize) {
    return clamp(Vec2u(rasterPosition),
                 Vec2u(0),
                 Vec2u(imageSize.x - 1, imageSize.y - 1));
}

inline Vec2f getUV(const Vec2f& rasterPosition,
                   const Vec2f& imageSize) {
    return rasterPosition / imageSize;
}

inline Vec2f getUV(const Vec2f& rasterPosition,
                   const Vec2u& imageSize) {
    return getUV(rasterPosition, Vec2f(imageSize));
}

inline Vec2f getUV(uint32_t i, uint32_t j,
                   const Vec2u& imageSize) {
    return getUV(Vec2f(i, j) + Vec2f(0.5f),
                 imageSize);
}

inline Vec2f getUV(const Vec2u& pixel,
                   const Vec2u& imageSize) {
    return getUV(Vec2f(pixel) + Vec2f(0.5f),
                 imageSize);
}

inline Vec2f ndcToUV(const Vec2f& ndc) {
    return 0.5f * (ndc + Vec2f(1));
}

inline Vec2f uvToNDC(const Vec2f& uv) {
    return 2.f * uv - Vec2f(1);
}

inline float reduceMax(const Vec3f& v) {
    return max(v.x, max(v.y, v.z));
}

inline float luminance(const Vec3f& color) {
    return 0.212671f * color.r + 0.715160f * color.g + 0.072169f * color.b;
}

template<typename T> inline T sin2cos ( const T& x )  {
    return sqrt(max(T(0.f), T(1.f) - x * x));
}

template<typename T> inline T cos2sin ( const T& x )  {
    return sin2cos(x);
}

inline Mat4f getViewMatrix(const Vec3f& origin) {
    return glm::translate(glm::mat4(1.f), -origin);
}

inline bool viewportContains(const Vec2f& position, const Vec4f& viewport) {
    return position.x >= viewport.x && position.y >= viewport.y &&
            position.x < viewport.x + viewport.z && position.y < viewport.y + viewport.w;
}

inline Vec3f refract(const Vec3f& I, const Vec3f& N, float eta) {
    auto dotI = dot(I, N);

    auto sqrDotO = 1 - sqr(eta) * (1 - sqr(dotI));
    if(sqrDotO < 0.f) {
        return zero<Vec3f>();
    }
    auto k = sqrt(sqrDotO);
    return -eta * I + (eta * dotI - sign(dotI) * k) * N;
}

inline Vec3f reflect(const Vec3f& I, const Vec3f& N) {
    return glm::reflect(-I, N);
}

}
