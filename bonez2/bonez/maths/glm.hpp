#pragma once

#define GLM_SWIZZLE
#define GLM_SIMD_ENABLE_XYZW_UNION
#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtx/io.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/compatibility.hpp>
#include <glm/gtx/simd_vec4.hpp>
#include <glm/gtx/bit.hpp>

namespace BnZ {

template<typename T>
struct VecTraits;

template<typename T, glm::precision p>
struct VecTraits<glm::detail::tvec1<T, p>> {
    static const auto dim = 1u;
    typedef T Scalar;
};

template<typename T, glm::precision p>
struct VecTraits<glm::detail::tvec2<T, p>> {
    static const auto dim = 2u;
    typedef T Scalar;
};

template<typename T, glm::precision p>
struct VecTraits<glm::detail::tvec3<T, p>> {
    static const auto dim = 3u;
    typedef T Scalar;
};

template<typename T, glm::precision p>
struct VecTraits<glm::detail::tvec4<T, p>> {
    static const auto dim = 4u;
    typedef T Scalar;
};

}
