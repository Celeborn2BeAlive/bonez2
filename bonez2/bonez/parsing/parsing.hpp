#pragma once

#include <bonez/types.hpp>
#include <bonez/scene/lights/LightContainer.hpp>
#include <bonez/scene/cameras/ProjectiveCamera.hpp>

#include <bonez/scene/lights/PointLight.hpp>
#include <bonez/scene/lights/SurfacePointLight.hpp>
#include <bonez/scene/lights/DirectionalLight.hpp>

#include "tinyxml/tinyxml2.h"

namespace BnZ {

inline bool getAttribute(const tinyxml2::XMLElement& elt, const char* name,
                         float& value) {
    return tinyxml2::XML_NO_ERROR == elt.QueryFloatAttribute(name, &value);
}

inline bool getAttribute(const tinyxml2::XMLElement& elt, const char* name,
                         double& value) {
    return tinyxml2::XML_NO_ERROR == elt.QueryDoubleAttribute(name, &value);
}

inline bool getAttribute(const tinyxml2::XMLElement& elt, const char* name,
                         int& value) {
    return tinyxml2::XML_NO_ERROR == elt.QueryIntAttribute(name, &value);
}

inline bool getAttribute(const tinyxml2::XMLElement& elt, const char* name,
                         uint32_t& value) {
    return tinyxml2::XML_NO_ERROR == elt.QueryUnsignedAttribute(name, &value);
}

inline bool getAttribute(const tinyxml2::XMLElement& elt, const char* name,
                         bool& value) {
    return tinyxml2::XML_NO_ERROR == elt.QueryBoolAttribute(name, &value);
}

inline bool getAttribute(const tinyxml2::XMLElement& elt, const char* name,
                         std::string& value) {
    auto ptr = elt.Attribute(name);
    if(ptr) {
        value = ptr;
        return true;
    }
    return false;
}

template<typename T>
inline void setAttribute(tinyxml2::XMLElement& elt, const char* name, const T& value) {
    elt.SetAttribute(name, value);
}

inline void setAttribute(tinyxml2::XMLElement& elt, const char* name,
                         const std::string& value) {
    elt.SetAttribute(name, value.c_str());
}

Vec3f loadVector(const tinyxml2::XMLElement& elt);

Vec3f loadColor(const tinyxml2::XMLElement& elt, const Vec3f& defaultColor = zero<Vec3f>());

void storeVector(tinyxml2::XMLElement& elt, const Vec3f& v);

void storeColor(tinyxml2::XMLElement& elt, const Vec3f& c);

PointLight loadPointLight(
        const tinyxml2::XMLElement& elt);

DiffuseSurfacePointLight loadDiffuseSurfacePointLight(
        const tinyxml2::XMLElement& elt);

DirectionalLight loadDirectionalLight(
        const tinyxml2::XMLElement& elt);

void loadLights(Scene& scene, const tinyxml2::XMLElement* pLightElt);

void storeLight(
        tinyxml2::XMLElement& elt,
        const PointLight& light);

void storeLight(
        tinyxml2::XMLElement& elt,
        const DirectionalLight& light);

void storeLight(
        tinyxml2::XMLElement& elt,
        const DiffuseSurfacePointLight& light);

void storeLights(const LightContainer& lightContainer, tinyxml2::XMLElement& lightElt);

void loadPerspectiveCamera(
        const tinyxml2::XMLElement& elt,
        Vec3f& eye, Vec3f& point, Vec3f& up,
        float& fovY);

void storePerspectiveCamera(
        tinyxml2::XMLElement& elt,
        const ProjectiveCamera& camera);

}
