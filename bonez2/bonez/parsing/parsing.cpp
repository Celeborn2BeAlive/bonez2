#include "parsing.hpp"
#include <bonez/scene/Scene.hpp>

namespace BnZ {

Vec3f loadVector(const tinyxml2::XMLElement& elt) {
    Vec3f v;
    getAttribute(elt, "x", v.x);
    getAttribute(elt, "y", v.y);
    getAttribute(elt, "z", v.z);
    return v;
}

Vec3f loadColor(const tinyxml2::XMLElement& elt, const Vec3f& defaultColor) {
    Vec3f c;
    if(!getAttribute(elt, "r", c.r)) {
        c.r = defaultColor.r;
    }
    if(!getAttribute(elt, "g", c.g)) {
        c.g = defaultColor.g;
    }
    if(!getAttribute(elt, "b", c.b)) {
        c.b = defaultColor.b;
    }
    return c;
}

void storeVector(tinyxml2::XMLElement& elt, const Vec3f& v) {
    setAttribute(elt, "x", v.x);
    setAttribute(elt, "y", v.y);
    setAttribute(elt, "z", v.z);
}

void storeColor(tinyxml2::XMLElement& elt, const Vec3f& c) {
    setAttribute(elt, "r", c.r);
    setAttribute(elt, "g", c.g);
    setAttribute(elt, "b", c.b);
}

PointLight loadPointLight(
        const tinyxml2::XMLElement& elt) {
    return { loadVector(elt), loadColor(elt) };
}

DirectionalLight loadDirectionalLight(
        const tinyxml2::XMLElement& elt) {
    return { loadVector(elt), loadColor(elt) };
}

DiffuseSurfacePointLight loadDiffuseSurfacePointLight(
        const tinyxml2::XMLElement& elt) {
    auto P = loadVector(elt);
    auto Le = loadColor(elt);
    Vec3f N;
    getAttribute(elt, "nx", N.x);
    getAttribute(elt, "ny", N.y);
    getAttribute(elt, "nz", N.z);
    return { P, N, Le };
}

void loadLights(Scene& scene, const tinyxml2::XMLElement* pLightElt) {
    if(pLightElt) {
        for(auto pLight = pLightElt->FirstChildElement();
            pLight;
            pLight = pLight->NextSiblingElement()) {
            auto lightType = toString(pLight->Name());
            if(lightType == "PointLight") {
                scene.addLight(makeShared<PointLight>(loadPointLight(*pLight)));
            } else if(lightType == "DirectionalLight") {
                scene.addLight(makeShared<DirectionalLight>(loadDirectionalLight(*pLight)));
            } else if(lightType == "DiffuseSurfacePointLight") {
                scene.addLight(makeShared<DiffuseSurfacePointLight>(loadDiffuseSurfacePointLight(*pLight)));
            } else {
                std::cerr << "Unrecognized light type" << lightType << std::endl;
            }
        }
    }
}

class LightStoreVisitor: public LightVisitor {
    tinyxml2::XMLElement& m_LightElmt;
public:
    LightStoreVisitor(tinyxml2::XMLElement& lightElmt): m_LightElmt(lightElmt) {
    }

    virtual void visit(const Light& light) {
        std::cerr << "LightStoreVisitor: unknown light type" << std::endl;
    }

    template<typename LightType>
    void doVisit(const LightType& light) {
        auto elt = m_LightElmt.GetDocument()->NewElement("Unknown");
        m_LightElmt.InsertEndChild(elt);
        storeLight(*elt, light);
    }

    virtual void visit(const PointLight& light) override {
        doVisit(light);
    }

    virtual void visit(const DirectionalLight& light) {
        doVisit(light);
    }

    virtual void visit(const DiffuseSurfacePointLight& light) {
        doVisit(light);
    }
};

void storeLights(const LightContainer& lightContainer, tinyxml2::XMLElement& lightElt) {
    LightStoreVisitor lightStore(lightElt);
    lightContainer.accept(lightStore);

/*
    auto size = lightContainer.size();
    auto doc = lightElt.GetDocument();
    for(auto i = 0u; i < size; ++i) {
        auto elt = doc->NewElement("Unknown");
        lightElt.InsertEndChild(elt);
        LightContainer::LightType type;
        const auto& light = lightContainer.getLight(i, type);
        switch(type) {
        case LightContainer::POINT_LIGHT:
            storePointLight(*elt, static_cast<const PointLight&>(light));
            break;
        case LightContainer::DIFFUSE_SURFACE_POINT_LIGHT:
            storeDiffuseSurfacePointLight(*elt, static_cast<const DiffuseSurfacePointLight&>(light));
            break;
        case LightContainer::DIRECTIONAL_LIGHT:
            storeDirectionalLight(*elt, static_cast<const DirectionalLight&>(light));
            break;
        default:
            break;
        }
    }*/
}

void storeLight(
        tinyxml2::XMLElement& elt,
        const PointLight& light) {
    storeVector(elt, light.m_Position);
    storeColor(elt, light.m_Intensity);
    elt.SetName("PointLight");
}

void storeLight(
        tinyxml2::XMLElement& elt,
        const DirectionalLight& light) {
    storeVector(elt, light.m_IncidentDirection);
    storeColor(elt, light.m_Le);
    elt.SetName("DirectionalLight");
}

void storeLight(
        tinyxml2::XMLElement& elt,
        const DiffuseSurfacePointLight& light) {
    storeVector(elt, light.m_Position);
    storeColor(elt, light.m_Le);
    setAttribute(elt, "nx", light.m_Normal.x);
    setAttribute(elt, "ny", light.m_Normal.y);
    setAttribute(elt, "nz", light.m_Normal.z);
    elt.SetName("DiffuseSurfacePointLight");
}

void loadPerspectiveCamera(
        const tinyxml2::XMLElement& elt,
        Vec3f& eye, Vec3f& point, Vec3f& up,
        float& fovY) {
    getAttribute(elt, "fovY", fovY);
    getAttribute(elt, "eyeX", eye.x);
    getAttribute(elt, "eyeY", eye.y);
    getAttribute(elt, "eyeZ", eye.z);
    getAttribute(elt, "pointX", point.x);
    getAttribute(elt, "pointY", point.y);
    getAttribute(elt, "pointZ", point.z);
    getAttribute(elt, "upX", up.x);
    getAttribute(elt, "upY", up.y);
    getAttribute(elt, "upZ", up.z);
}

void storePerspectiveCamera(
        tinyxml2::XMLElement& elt,
        const Vec3f& eye, const Vec3f& point, const Vec3f& up,
        float fovY) {
    setAttribute(elt, "fovY", fovY);
    setAttribute(elt, "eyeX", eye.x);
    setAttribute(elt, "eyeY", eye.y);
    setAttribute(elt, "eyeZ", eye.z);
    setAttribute(elt, "pointX", point.x);
    setAttribute(elt, "pointY", point.y);
    setAttribute(elt, "pointZ", point.z);
    setAttribute(elt, "upX", up.x);
    setAttribute(elt, "upY", up.y);
    setAttribute(elt, "upZ", up.z);
}

void storePerspectiveCamera(
        tinyxml2::XMLElement& elt,
        const ProjectiveCamera& camera) {
    auto eye = camera.getPosition();
    storePerspectiveCamera(
                elt,
                eye,
                eye + camera.getFrontVector(),
                camera.getUpVector(),
                camera.getFovY());
}

}
