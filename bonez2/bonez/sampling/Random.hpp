#pragma once

#include <random>
#include <algorithm>
#include <bonez/types.hpp>

namespace BnZ {

// Transform a float into a random generator; useful
// to sample standard library distribution with arbitrary floating points
struct RandomFloat {
    typedef float result_type;

    float value = 0.f;

    RandomFloat() = default;

    RandomFloat(float v): value(v) {
    }

    operator float() const {
        return value;
    }

    result_type min() {
        return 0.f;
    }

    result_type max() {
        return 1.f;
    }

    result_type operator()() {
        return value;
    }
};

class RandomGenerator {
    typedef std::mt19937 Generator;
public:
    typedef float result_type;

    RandomGenerator(uint32_t seed = 0u):
        m_Generator(seed), m_Distribution(0, 1), m_nSeed(seed) {
    }

    void setSeed(uint32_t seed) {
        m_Generator.seed(seed);
        m_nSeed = seed;
        m_nCallCount = 0u;
    }

    uint32_t getSeed() const {
        return m_nSeed;
    }

    uint32_t getUInt() {
        return m_Generator();
    }

    RandomFloat getFloat() {
        ++m_nCallCount;
        // doesn't work on windows: returns number greater than 1...
        //return std::generate_canonical<float, std::numeric_limits<float>::digits>(m_Generator);
        return m_Distribution(m_Generator);
    }

    Vec2f getFloat2() {
        return Vec2f(getFloat(), getFloat());
    }

    result_type min() {
        return 0.f;
    }

    result_type max() {
        return 1.f;
    }

    result_type operator()() {
        return getFloat();
    }

    uint64_t getCallCount() const {
        return m_nCallCount;
    }

    void discard(uint64_t callCount) {
        m_Generator.discard(callCount);
        m_nCallCount += callCount;
    }

private:
    Generator m_Generator;
    std::uniform_real_distribution<float> m_Distribution;
    uint32_t m_nSeed;
    uint64_t m_nCallCount = 0u;
};

template<typename Distrib>
typename Distrib::result_type sampleDistribution(RandomFloat r, Distrib& d) {
    return d(r);
}

inline uint32_t getTileSeed(uint32_t frameID, const Vec4u& tileViewport,
                            const Vec2u& imageSize) {
    return tileViewport.x + tileViewport.y * imageSize.x +
            frameID * imageSize.x * imageSize.y;
}

}
