#pragma once

#include <bonez/sampling/Sample.hpp>
#include <iostream>

namespace BnZ {

inline size_t getDistribution1DBufferSize(size_t size) {
    return size + 1;
}

// Build a 1D distribution for sampling
// - function(i) must returns the weight associating to the i-th element
// - size must be the number of elements
// - pCDF must point to a buffer containing getDistribution1DBufferSize(size) values and will
// be fill with the CDF of the distribution
// - if pSum != nullptr, then *pSum will be equal to the sum of the weights
//
// note: it is safe for function(i) to returns pCDF[i], so the buffer can be used to
// contain preprocessed weights as long as function(i) doesn't used pCDF[j] for j < i
template<typename Functor>
void buildDistribution1D(const Functor& function, float* pCDF, size_t size,
                         float* pSum = nullptr, bool debug = false) {
    // Compute CDF
    float sum = 0.f;
    for(auto i = 0u; i < size; ++i) {
        float tmp = function(i);
        pCDF[i] = sum;
        sum += tmp;
    }
    pCDF[size] = sum;

    if(sum > 0.f) {
        // Normalize the CDF
        for (auto i = 0u; i <= size; ++i) {
            pCDF[i] = pCDF[i] / sum; // DON'T MULTIPLY BY rcpSum => can produce numerical errors
        }
    } else {
        pCDF[size] = 0.f;
    }

    if(pSum) {
        *pSum = sum;
    }
}

Sample1f sampleContinuousDistribution1D(const float* pCDF, size_t size, float s1D);

Sample1u sampleDiscreteDistribution1D(const float* pCDF, size_t size, float s1D);

float pdfContinuousDistribution1D(const float* pCDF, size_t size, float x);

float pdfDiscreteDistribution1D(const float* pCDF, uint32_t idx);

}
