#pragma once

#include <bonez/maths/maths.hpp>

namespace BnZ {

struct JitteredDistribution2D {
    uint32_t m_nGridWidth;
    Vec2f m_fDelta;

    JitteredDistribution2D(uint32_t gridWidth, uint32_t gridHeight):
        m_nGridWidth(gridWidth),
        m_fDelta(1.f / gridWidth, 1.f / gridHeight) {
    }

    // Return the i-th sample of the distribution for a given random value
    Vec2f getSample(uint32_t i, const Vec2f& s2D) const {
        auto x = i % m_nGridWidth;
        auto y = i / m_nGridWidth;
        return (Vec2f(x, y) + s2D) * m_fDelta;
    }
};

}
