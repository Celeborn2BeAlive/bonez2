#pragma once

#include <cstdint>
#include <bonez/types.hpp>
#include <functional>
#include <vector>

struct GLFWwindow;

namespace BnZ {
    class WindowManager {
    public:
        WindowManager(uint32_t width, uint32_t height, const char* title);

        ~WindowManager();

        bool isOpen() const;

        void close();

        void swapBuffers();

        void handleEvents();

        bool hasClicked() {
            return m_bClick;
        }

        bool drawGUI();

        Vec2u getSize() const;

        float getRatio() const {
            auto size = getSize();
            return float(size.x) / size.y;
        }

        GLFWwindow* getWindow() const {
            return m_pWindow;
        }

        Vec2u getCursorPosition(const Vec2f& relativeTo = Vec2f(0)) const;

        Vec2f getCursorNDC() const;

        Vec2f getCursorNDC(const Vec4f& relativeTo) const;

        double getSeconds() const;

    private:
        GLFWwindow* m_pWindow = nullptr;
        bool m_bButtonState = false;
        bool m_bClick = false;
    };
}
