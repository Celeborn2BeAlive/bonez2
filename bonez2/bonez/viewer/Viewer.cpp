#include "Viewer.hpp"

#include <bonez/scene/topology/VSkelLoader.hpp>

namespace BnZ {

Viewer::Settings::Settings(const FilePath& filepath):
    m_FilePath(filepath) {
    if(tinyxml2::XML_NO_ERROR != m_Document.LoadFile(filepath.c_str())) {
        throw std::runtime_error("Unable to load viewer settings file");
    }

    if(nullptr == (m_pRoot = m_Document.RootElement())) {
        throw std::runtime_error("Invalid viewer settings file format (no root element)");
    }

    auto pWindow = m_pRoot->FirstChildElement("Window");
    if(!pWindow) {
        throw std::runtime_error("Invalid viewer settings file format (no Window element)");
    }

    if(!getAttribute(*pWindow, "width", m_WindowSize.x) ||
            !getAttribute(*pWindow, "height", m_WindowSize.y)) {
        throw std::runtime_error("Invalid viewer settings file format (no width/height params)");
    }

    auto pViewController = m_pRoot->FirstChildElement("ViewController");
    if(pViewController) {
        if(!getAttribute(*pViewController, "speedFarRatio", m_fSpeedFarRatio)) {
            throw std::runtime_error("Invalid viewer settings file format (no speedFarRatio params in ViewController)");
        }
    }

    auto pFramebuffer = m_pRoot->FirstChildElement("Framebuffer");
    if(!getAttribute(*pFramebuffer, "width", m_FramebufferSize.x) ||
            !getAttribute(*pFramebuffer, "height", m_FramebufferSize.y)) {
        throw std::runtime_error("Invalid viewer settings file format (no width/height params for the framebuffer)");
    }
    m_fFramebufferRatio = float(m_FramebufferSize.x) / m_FramebufferSize.y;
}

void Viewer::initShaderManager() {
    auto pShaders = m_Settings.m_pRoot->FirstChildElement("Shaders");
    if(!pShaders) {
        std::clog << "No Shaders tag found in viewer settings file" << std::endl;
        return;
    }

    auto shadersPath = m_Path + pShaders->Attribute("path");

    tinyxml2::XMLDocument shadersDoc;
    if(tinyxml2::XML_NO_ERROR != shadersDoc.LoadFile(shadersPath.c_str())) {
        throw std::runtime_error("Unable to load shaders file");
    }
    auto pRoot = shadersDoc.RootElement();
    if(!pRoot) {
        throw std::runtime_error("No root element in shaders' XML file");
    }
    auto path = shadersPath.path();
    for(auto pDir = pRoot->FirstChildElement("Dir"); pDir; pDir = pDir->NextSiblingElement("Dir")) {
        auto dirPath = FilePath(pDir->Attribute("path"));
        if(!dirPath.isAbsolute()) {
            dirPath = path + dirPath;
        }
        m_ShaderManager.addDirectory(dirPath);
    }
}

void Viewer::initScene() {
    auto pScene = m_Settings.m_pRoot->FirstChildElement("Scene");
    if(!pScene) {
        std::clog << "No Scene  tag found in viewer settings file" << std::endl;
        return;
    }

    m_SceneDocumentFilePath = m_Path + pScene->Attribute("path");

    if(tinyxml2::XML_NO_ERROR != m_SceneDocument.LoadFile(m_SceneDocumentFilePath.c_str())) {
        throw std::runtime_error("Unable to load scene file");
    }
    pScene = m_SceneDocument.RootElement();
    if(!pScene) {
        throw std::runtime_error("No root element in scene's XML file");
    }
    auto path = m_SceneDocumentFilePath.path();

    // Load geometry
    auto pGeometry = pScene->FirstChildElement("Geometry");
    if(!pGeometry) {
        throw std::runtime_error("No Geometry tag in scene setting file");
    }

    SceneGeometry geometry = loadGeometry(path, *pGeometry);

    m_ZNearFar.y = length(geometry.getBBox().size());
    getAttribute(*pScene, "far", m_ZNearFar.y);
    float nearFarRatio = pScene->FloatAttribute("nearFarRatio");
    m_ZNearFar.x = m_ZNearFar.y * nearFarRatio;

    auto pCamera = pScene->FirstChildElement("Camera");
    Vec3f eye(0), point(0, 0, -1), up(0, 1, 0);
    float fovY = 45.f;
    if(pCamera) {
        loadPerspectiveCamera(*pCamera, eye, point, up, fovY);
    }

    m_Camera = ProjectiveCamera(lookAt(eye, point, up), fovY, m_Settings.m_FramebufferSize.x, m_Settings.m_FramebufferSize.y, m_ZNearFar.x, m_ZNearFar.y);
/*
    m_Camera.lookAt(eye, point, up);
    m_Camera.setPerspective(fovY, m_Settings.m_fFramebufferRatio, m_ZNearFar.x, m_ZNearFar.y);*/

    m_pScene = makeUnique<Scene>(std::move(geometry));
    if(auto pLights = pScene->FirstChildElement("Lights")) {
        loadLights(*m_pScene, pLights);
    }

    // Load skeleton
    if(auto pCurvSkel = pScene->FirstChildElement("CurvSkel")) {
        VSkelLoader loader(path);
        m_pScene->setCurvSkeleton(loader.loadCurvilinearSkeleton(pCurvSkel->Attribute("path")));
    }
}

void Viewer::storeScene() {
    // Store scene settings
    auto pScene = m_SceneDocument.RootElement();
    auto pCamera = pScene->FirstChildElement("Camera");
    if(!pCamera) {
        pCamera = m_SceneDocument.NewElement("Camera");
        pScene->InsertEndChild(pCamera);
    }
    storePerspectiveCamera(*pCamera, m_Camera);

    auto pLights = pScene->FirstChildElement("Lights");
    if(!pLights) {
        pLights = m_SceneDocument.NewElement("Lights");
        pScene->InsertEndChild(pLights);
    }

    pLights->DeleteChildren();

    storeLights(m_pScene->getLightContainer(), *pLights);

    m_SceneDocument.SaveFile(m_SceneDocumentFilePath.c_str());
}

void Viewer::initConfigManager() {
    // Init ConfigManager
    auto pConfigs = m_SceneDocument.RootElement()->FirstChildElement("Configs");
    if(!pConfigs || ! pConfigs->Attribute("path")) {
        throw std::runtime_error("No Configs path specified in scene settings");
    }
    auto configPath = m_SceneDocumentFilePath.path() + pConfigs->Attribute("path");
    m_ConfigManager.setPath(configPath);
}

void Viewer::exposeConfigIO() {
    auto pConfigsBar = TwGetBarByName("Configs");

    if(!pConfigsBar) {
        pConfigsBar = TwNewBar("Configs");
    }

    atb::removeAllVars(pConfigsBar);

    atb::addButton(pConfigsBar, "Save Current Config", [&]() {
        auto name = toString(getMicroseconds());
        m_ConfigManager.storeConfig(name, m_Camera, m_pScene->getLightContainer());
        exposeConfigIO();
    });

    atb::addSeparator(pConfigsBar);

    const auto& configs = m_ConfigManager.getConfigs();
    for (auto i = 0u; i < configs.size(); ++i) {
        atb::addButton(pConfigsBar, configs[i].c_str(), [i, this]() {
            m_ConfigManager.loadConfig(i, m_Camera, *m_pScene,
                                       m_Settings.m_FramebufferSize.x, m_Settings.m_FramebufferSize.y,
                                       m_ZNearFar.x, m_ZNearFar.y);
        });
    }
}

void Viewer::exposeLightsIO() {
    auto pLightBar = TwNewBar("Lights");
    m_pScene->getLightContainer().exposeIO(pLightBar);
}

void Viewer::renderBatch(bool& quitAfter) {
    auto pRenders = m_Settings.m_pRoot->FirstChildElement("Renders");
    if(pRenders) {
        getAttribute(*pRenders, "quitAfter", quitAfter);
        for(auto pRender = pRenders->FirstChildElement("Render");
            pRender;
            pRender = pRender->NextSiblingElement("Render")) {
            m_RenderModule.renderConfig(m_Settings.m_FilePath.path(), pRender->Attribute("config"), m_ConfigManager, *m_pScene,
                                        m_ZNearFar.x, m_ZNearFar.y);
        }
    }
}

Viewer::Viewer(const char* title, const char* settingsFilePath):
    m_Path(FilePath(settingsFilePath).path()),
    m_Settings(settingsFilePath),
    m_WindowManager(m_Settings.m_WindowSize.x, m_Settings.m_WindowSize.y,
                    title),
    m_ViewController(m_WindowManager.getWindow()) {

    initShaderManager();
    initImageIO();
    rtcInit(); // Init embree

    initScene();
    initConfigManager();

    m_ViewController.setSpeed(m_Settings.m_fSpeedFarRatio * m_ZNearFar.y);

    std::clog << "Number of threads = " << getThreadCount() << std::endl;
}

Viewer::~Viewer() {
    rtcExit(); // Exit embree

    storeScene();

    m_Settings.m_Document.SaveFile(m_Settings.m_FilePath.c_str());
}

void Viewer::run() {
    exposeConfigIO();
    exposeLightsIO();

    m_RenderModule.setUp(m_Path,
                         m_Settings.m_pRoot,
                         m_Settings.m_FramebufferSize);

    setUp();

    bool quitAfter = false;
    renderBatch(quitAfter);

    if(!quitAfter) {
        m_nFrameID = 0;
        while (m_WindowManager.isOpen()) {
            auto startTime = m_WindowManager.getSeconds();

            m_ViewController.setViewMatrix(m_Camera.getViewMatrix());

            drawFrame();

            m_bGUIHasFocus = m_WindowManager.drawGUI();

            m_WindowManager.handleEvents();

            m_WindowManager.swapBuffers();

            if (!m_bGUIHasFocus) {
                float ellapsedTime = float(m_WindowManager.getSeconds() - startTime);
                if(m_ViewController.update(ellapsedTime)) {
                    m_Camera = ProjectiveCamera(m_ViewController.getViewMatrix(), m_Camera.getFovY(),
                                                m_Settings.m_FramebufferSize.x, m_Settings.m_FramebufferSize.y,
                                                getZNear(), getZFar());
                    m_CameraUpdateFlag.addUpdate();
                }
            }

            ++m_nFrameID;
        }
    }

    tearDown();

    m_RenderModule.tearDown(m_Settings.m_pRoot);
}

}
