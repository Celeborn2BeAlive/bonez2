#include "WindowManager.hpp"

#include <bonez/opengl/opengl.hpp>
#include <GLFW/glfw3.h>
#include <AntTweakBar/atb.hpp>

#include <iostream>
#include <stdexcept>

#ifndef _WIN32
    #define sprintf_s snprintf
#endif

static void formatDebugOutput(char outStr[], size_t outStrSize, GLenum source, GLenum type,
    GLuint id, GLenum severity, const char *msg)
{
    char sourceStr[32];
    const char *sourceFmt = "UNDEFINED(0x%04X)";
    switch (source)

    {
    case GL_DEBUG_SOURCE_API_ARB:             sourceFmt = "API"; break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB:   sourceFmt = "WINDOW_SYSTEM"; break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER_ARB: sourceFmt = "SHADER_COMPILER"; break;
    case GL_DEBUG_SOURCE_THIRD_PARTY_ARB:     sourceFmt = "THIRD_PARTY"; break;
    case GL_DEBUG_SOURCE_APPLICATION_ARB:     sourceFmt = "APPLICATION"; break;
    case GL_DEBUG_SOURCE_OTHER_ARB:           sourceFmt = "OTHER"; break;
    }

    sprintf_s(sourceStr, 32, sourceFmt, source);

    char typeStr[32];
    const char *typeFmt = "UNDEFINED(0x%04X)";
    switch (type)
    {

    case GL_DEBUG_TYPE_ERROR_ARB:               typeFmt = "ERROR"; break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB: typeFmt = "DEPRECATED_BEHAVIOR"; break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB:  typeFmt = "UNDEFINED_BEHAVIOR"; break;
    case GL_DEBUG_TYPE_PORTABILITY_ARB:         typeFmt = "PORTABILITY"; break;
    case GL_DEBUG_TYPE_PERFORMANCE_ARB:         typeFmt = "PERFORMANCE"; break;
    case GL_DEBUG_TYPE_OTHER_ARB:               typeFmt = "OTHER"; break;
    }
    sprintf_s(typeStr, 32, typeFmt, type);


    char severityStr[32];
    const char *severityFmt = "UNDEFINED";
    switch (severity)
    {
    case GL_DEBUG_SEVERITY_HIGH_ARB:   severityFmt = "HIGH";   break;
    case GL_DEBUG_SEVERITY_MEDIUM_ARB: severityFmt = "MEDIUM"; break;
    case GL_DEBUG_SEVERITY_LOW_ARB:    severityFmt = "LOW"; break;
    }

    sprintf_s(severityStr, 32, severityFmt, severity);

    sprintf_s(outStr, outStrSize, "OpenGL: %s [source=%s type=%s severity=%s id=%d]",
        msg, sourceStr, typeStr, severityStr, id);
}

static void foo() {
    std::cerr << "foo" << std::endl;
}

static void debugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, 
    GLsizei length, const GLchar* message, GLvoid* userParam)
{
    if (131218 == id) { // "OpenGL: Program/shader state performance warning: Fragment Shader is going to be recompiled because the shader key based on GL state mismatches."
        return;
    }
    else if (131186 == id) { // "Buffer performance warning: Buffer object 24 (bound to GL_SHADER_STORAGE_BUFFER, usage hint is GL_STATIC_DRAW) is being copied/moved from VIDEO memory to HOST memory."
        return;
    }
    else {
        if(severity != GL_DEBUG_SEVERITY_HIGH_ARB) {
            return;
        }

        char finalMessage[256];
        formatDebugOutput(finalMessage, 256, source, type, id, severity, message);
        std::cerr << finalMessage << std::endl;
        foo();
        finalMessage[0] = 0;
    }
}

static void errorCallback(int code, const char * desc) {
    std::cerr << "GLFW error code = " << code << " desc = " << desc << std::endl;
}

static void copyStdStringToClient(std::string& destinationClientString, const std::string& sourceLibraryString)
{
    // Copy the content of souceString handled by the AntTweakBar library to destinationClientString handled by your application
    destinationClientString = sourceLibraryString;
}

static int g_KMod = 0;
static bool g_EventCharTw = false;

static void twKeyCallback(GLFWwindow* pWindow, int glfwKey, int scancode,
    int glfwAction, int mods)
{
    // Register of modifiers state
    if (glfwAction == GLFW_PRESS)
    {
        switch (glfwKey)
        {
        case GLFW_KEY_LEFT_SHIFT:
        case GLFW_KEY_RIGHT_SHIFT:
            g_KMod |= TW_KMOD_SHIFT;
            break;
        case GLFW_KEY_LEFT_CONTROL:
        case GLFW_KEY_RIGHT_CONTROL:
            g_KMod |= TW_KMOD_CTRL;
            break;
        case GLFW_KEY_LEFT_ALT:
        case GLFW_KEY_RIGHT_ALT:
            g_KMod |= TW_KMOD_ALT;
            break;
        }
    }
    else
    {
        switch (glfwKey)
        {
        case GLFW_KEY_LEFT_SHIFT:
        case GLFW_KEY_RIGHT_SHIFT:
            g_KMod &= ~TW_KMOD_SHIFT;
            break;
        case GLFW_KEY_LEFT_CONTROL:
        case GLFW_KEY_RIGHT_CONTROL:
            g_KMod &= ~TW_KMOD_CTRL;
            break;
        case GLFW_KEY_LEFT_ALT:
        case GLFW_KEY_RIGHT_ALT:
            g_KMod &= ~TW_KMOD_ALT;
            break;
        }
    }

    // Process key pressed
    if (glfwAction == GLFW_PRESS)
    {
        int mod = g_KMod;
        int testkp = ((mod&TW_KMOD_CTRL) || (mod&TW_KMOD_ALT)) ? 1 : 0;

        if ((mod&TW_KMOD_CTRL) && glfwKey>0/* && glfwKey<GLFW_KEY_SPECIAL*/)   // CTRL cases
            TwKeyPressed(glfwKey, mod);
        else/* if( glfwKey>=GLFW_KEY_SPECIAL )*/
        {
            int k = 0;

            if (glfwKey >= GLFW_KEY_F1 && glfwKey <= GLFW_KEY_F15)
                k = TW_KEY_F1 + (glfwKey - GLFW_KEY_F1);
            else if (testkp && glfwKey >= GLFW_KEY_KP_0 && glfwKey <= GLFW_KEY_KP_9)
                k = '0' + (glfwKey - GLFW_KEY_KP_0);
            else
            {
                switch (glfwKey)
                {
                case GLFW_KEY_ESCAPE:
                    k = TW_KEY_ESCAPE;
                    glfwSetWindowShouldClose(pWindow, 1);
                    break;
                case GLFW_KEY_UP:
                    k = TW_KEY_UP;
                    break;
                case GLFW_KEY_DOWN:
                    k = TW_KEY_DOWN;
                    break;
                case GLFW_KEY_LEFT:
                    k = TW_KEY_LEFT;
                    break;
                case GLFW_KEY_RIGHT:
                    k = TW_KEY_RIGHT;
                    break;
                case GLFW_KEY_TAB:
                    k = TW_KEY_TAB;
                    break;
                case GLFW_KEY_ENTER:
                    k = TW_KEY_RETURN;
                    break;
                case GLFW_KEY_BACKSPACE:
                    k = TW_KEY_BACKSPACE;
                    break;
                case GLFW_KEY_INSERT:
                    k = TW_KEY_INSERT;
                    break;
                case GLFW_KEY_DELETE:
                    k = TW_KEY_DELETE;
                    break;
                case GLFW_KEY_PAGE_UP:
                    k = TW_KEY_PAGE_UP;
                    break;
                case GLFW_KEY_PAGE_DOWN:
                    k = TW_KEY_PAGE_DOWN;
                    break;
                case GLFW_KEY_HOME:
                    k = TW_KEY_HOME;
                    break;
                case GLFW_KEY_END:
                    k = TW_KEY_END;
                    break;
                case GLFW_KEY_KP_ENTER:
                    k = TW_KEY_RETURN;
                    break;
                case GLFW_KEY_KP_DIVIDE:
                    if (testkp)
                        k = '/';
                    break;
                case GLFW_KEY_KP_MULTIPLY:
                    if (testkp)
                        k = '*';
                    break;
                case GLFW_KEY_KP_SUBTRACT:
                    if (testkp)
                        k = '-';
                    break;
                case GLFW_KEY_KP_ADD:
                    if (testkp)
                        k = '+';
                    break;
                case GLFW_KEY_KP_DECIMAL:
                    if (testkp)
                        k = '.';
                    break;
                case GLFW_KEY_KP_EQUAL:
                    if (testkp)
                        k = '=';
                    break;
                }
            }

            if (k>0)
                TwKeyPressed(k, mod);
        }
    }
}

static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE) {
        glfwSetWindowShouldClose(window, true);
    }
    twKeyCallback(window, key, scancode, action, mods);
}

static void twCharCallback(GLFWwindow* window, uint32_t glfwChar)
{
    if ((glfwChar & 0xff00) == 0) {
        g_EventCharTw = TwKeyPressed(glfwChar, g_KMod);
    }
}

static void twMouseButtonCallback(GLFWwindow* pWindow, int button, int action, int mods) {
    TwMouseAction a = (action == GLFW_PRESS) ? TW_MOUSE_PRESSED : TW_MOUSE_RELEASED;
    if (button == GLFW_MOUSE_BUTTON_LEFT)
        TwMouseButton(a, TW_MOUSE_LEFT);
    else if (button == GLFW_MOUSE_BUTTON_RIGHT)
        TwMouseButton(a, TW_MOUSE_RIGHT);
    else if (button == GLFW_MOUSE_BUTTON_MIDDLE)
        TwMouseButton(a, TW_MOUSE_MIDDLE);
}

namespace BnZ {
    WindowManager::WindowManager(uint32_t width, uint32_t height, const char* title) {
        glfwSetErrorCallback(errorCallback);
        
        int major, minor, rev;
        glfwGetVersion(&major, &minor, &rev);
        std::clog << "GLFW version " << major << "." << minor << "." << rev << std::endl;

        if (!glfwInit()) {
            throw std::runtime_error("glfwInit() error");
        }

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
        glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
        if (!(m_pWindow = glfwCreateWindow(width, height, title, nullptr, nullptr))) {
            glfwTerminate();
            throw std::runtime_error("glfwCreateWindow() error");
        }
        glfwMakeContextCurrent(m_pWindow);

        glfwSetKeyCallback(m_pWindow, keyCallback);
        glfwSetCharCallback(m_pWindow, twCharCallback);
        glfwSetMouseButtonCallback(m_pWindow, twMouseButtonCallback);

        if (!BnZ::initGLEW()) {
            throw std::runtime_error("initGLEW() error");
        }

        GLint glMajor, glMinor;
        glGetIntegerv(GL_MAJOR_VERSION, &glMajor);
        glGetIntegerv(GL_MINOR_VERSION, &glMinor);
        std::clog << "OpenGL version " << glMajor << "." << glMinor << std::endl;

        if (glewIsSupported("GL_ARB_debug_output")) {
            glDebugMessageCallbackARB((GLDEBUGPROCARB) debugCallback, 0); // print debug output to stderr
            glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
        }
        else {
            std::cerr << "GL_ARB_debug_output not supported" << std::endl;
        }

        TwInit(TW_OPENGL_CORE, nullptr);
        TwCopyStdStringToClientFunc(copyStdStringToClient);
    }

    WindowManager::~WindowManager() {
        TwTerminate();
        glfwDestroyWindow(m_pWindow);
        glfwTerminate();
    }

    bool WindowManager::isOpen() const {
        return !glfwWindowShouldClose(m_pWindow);
    }

    void WindowManager::close() {
        glfwSetWindowShouldClose(m_pWindow, true);
    }

    void WindowManager::swapBuffers() {
        glfwSwapBuffers(m_pWindow);
    }

    void WindowManager::handleEvents() {
        glfwPollEvents();

        if(glfwGetKey(m_pWindow, GLFW_KEY_F1)) {
            for(auto i = 0; i < TwGetBarCount(); ++i) {
                auto pBar = TwGetBarByIndex(i);
                if(pBar) {
                    int32_t value = 1;
                    TwSetParam(pBar, nullptr, "iconified", TW_PARAM_INT32, 1, &value);
                }
            }
        }

        if(glfwGetKey(m_pWindow, GLFW_KEY_F2)) {
            for(auto i = 0; i < TwGetBarCount(); ++i) {
                auto pBar = TwGetBarByIndex(i);
                if(pBar) {
                    int32_t value = 0;
                    TwSetParam(pBar, nullptr, "iconified", TW_PARAM_INT32, 1, &value);
                }
            }
        }

        bool newState = glfwGetMouseButton(m_pWindow, GLFW_MOUSE_BUTTON_RIGHT);
        m_bClick = !newState && m_bButtonState;
        m_bButtonState = newState;
    }

    bool WindowManager::drawGUI() {
        int width, height;
        glfwGetFramebufferSize(m_pWindow, &width, &height);
        glViewport(0, 0, width, height);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        double fMouseX, fMouseY;
        glfwGetCursorPos(m_pWindow, &fMouseX, &fMouseY);

        int mouseX = std::floor(fMouseX);
        int mouseY = height - std::floor(fMouseY);

        bool hasFocus = TwMouseMotion(mouseX, height - mouseY);

        TwWindowSize(width, height);
        TwDraw();

        glDisable(GL_BLEND);

        return hasFocus || g_EventCharTw;
    }

    Vec2u WindowManager::getSize() const {
        int width, height;
        glfwGetFramebufferSize(m_pWindow, &width, &height);
        return Vec2u(width, height);
    }

    Vec2u WindowManager::getCursorPosition(const Vec2f& relativeTo) const {
        Vec2u size = getSize();
        double fMouseX, fMouseY;
        glfwGetCursorPos(m_pWindow, &fMouseX, &fMouseY);
        return Vec2u(fMouseX - relativeTo.x, size.y - 1 - std::floor(fMouseY) - relativeTo.y);
    }

    Vec2f WindowManager::getCursorNDC() const {
        Vec2u cursorPosition = getCursorPosition();
        Vec2u size = getSize();
        return Vec2f(-1.f) + 2.f * Vec2f(cursorPosition) / Vec2f(size);
    }

    Vec2f WindowManager::getCursorNDC(const Vec4f& relativeTo) const {
        Vec2u cursorPosition = getCursorPosition(Vec2f(relativeTo));
        return Vec2f(-1.f) + 2.f * Vec2f(cursorPosition) / Vec2f(relativeTo.z, relativeTo.w);
    }

    double WindowManager::getSeconds() const {
        return glfwGetTime();
    }
}
