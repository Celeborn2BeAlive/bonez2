#pragma once

#include "WindowManager.hpp"
#include "ViewController.hpp"

#include <bonez/scene/ConfigManager.hpp>
#include <bonez/opengl/GLShaderManager.hpp>
#include <bonez/opengl/GLImageRenderer.hpp>
#include <bonez/parsing/parsing.hpp>
#include <bonez/rendering/RenderModule.hpp>

namespace BnZ {

class Viewer {
    void initShaderManager();

    void initScene();

    void storeScene();

    void initConfigManager();

    void exposeConfigIO();

    void exposeLightsIO();

    void exposeRenderIO();

    void renderBatch(bool& quitAfter);
protected:
    struct Settings {
        FilePath m_FilePath;
        tinyxml2::XMLDocument m_Document;
        tinyxml2::XMLElement* m_pRoot;
        Vec2u m_WindowSize;
        Vec2u m_FramebufferSize;
        float m_fFramebufferRatio;
        float m_fSpeedFarRatio = 1.f;

        Settings(const FilePath& filepath);
    };

    FilePath m_Path;

    // Viewing settings
    Settings m_Settings;

    WindowManager m_WindowManager;
    GLShaderManager m_ShaderManager;

    // Scene
    ConfigManager m_ConfigManager;

    FilePath m_SceneDocumentFilePath;
    tinyxml2::XMLDocument m_SceneDocument;
    Unique<Scene> m_pScene;
    ProjectiveCamera m_Camera;
    Vec2f m_ZNearFar; // Computed from the scene bounds

    ViewController m_ViewController;
    UpdateFlag m_CameraUpdateFlag;

    // For CPU rendering
    RenderModule m_RenderModule;

    bool m_bGUIHasFocus = false;
    uint32_t m_nFrameID = 0;

    // Called before the render loop
    virtual void setUp() = 0;

    virtual void drawFrame() = 0;

    // Called after the render loop
    virtual void tearDown() = 0;

public:
    float getZNear() const {
        return m_ZNearFar.x;
    }

    float getZFar() const {
        return m_ZNearFar.y;
    }

    Viewer(const char* title, const char* settingsFilePath);

    ~Viewer();

    void run();
};

}
