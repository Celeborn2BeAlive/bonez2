#include "ViewController.hpp"

#include <bonez/maths/maths.hpp>

#include <GLFW/glfw3.h>

namespace BnZ {

bool ViewController::update(float elapsedTime) {

    Vec3f m_FrontVector = -Vec3f(m_RcpViewMatrix[2]);
    Vec3f m_LeftVector = -Vec3f(m_RcpViewMatrix[0]);
    Vec3f m_UpVector = Vec3f(m_RcpViewMatrix[1]);
    Vec3f position = Vec3f(m_RcpViewMatrix[3]);

    Vec2f m_SphericalAngles = cartesianToSpherical(m_FrontVector);

    bool hasMoved = false;
    Vec3f localTranslationVector(0.f);

#ifdef _WIN32
    if (glfwGetKey(m_pWindow, GLFW_KEY_Z)) {
        localTranslationVector += m_fSpeed * elapsedTime * m_FrontVector;
    }

    if (glfwGetKey(m_pWindow, GLFW_KEY_Q)) {
        localTranslationVector += m_fSpeed * elapsedTime * m_LeftVector;
    }
#else
    if (glfwGetKey(m_pWindow, GLFW_KEY_W)) {
        localTranslationVector += m_fSpeed * elapsedTime * m_FrontVector;
    }

    if (glfwGetKey(m_pWindow, GLFW_KEY_A)) {
        localTranslationVector += m_fSpeed * elapsedTime * m_LeftVector;
    }
#endif
    if (glfwGetKey(m_pWindow, GLFW_KEY_S)) {
        localTranslationVector -= m_fSpeed * elapsedTime * m_FrontVector;
    }
    
    if (glfwGetKey(m_pWindow, GLFW_KEY_D)) {
        localTranslationVector -= m_fSpeed * elapsedTime * m_LeftVector;
    }
    if (glfwGetKey(m_pWindow, GLFW_KEY_UP)) {
        localTranslationVector += m_fSpeed * elapsedTime * m_UpVector;
    }
    if (glfwGetKey(m_pWindow, GLFW_KEY_DOWN)) {
        localTranslationVector -= m_fSpeed * elapsedTime * m_UpVector;
    }

    position += localTranslationVector;

    if (localTranslationVector != Vec3f(0.f)) {
        hasMoved = true;
    }

    if(glfwGetMouseButton(m_pWindow, GLFW_MOUSE_BUTTON_LEFT) && !m_LeftButtonPressed) {
        m_LeftButtonPressed = true;
        glfwGetCursorPos(m_pWindow, &m_LastCursorPosition.x, &m_LastCursorPosition.y);
    } else if(!glfwGetMouseButton(m_pWindow, GLFW_MOUSE_BUTTON_LEFT) && m_LeftButtonPressed) {
        m_LeftButtonPressed = false;
    }

    if(m_LeftButtonPressed) {
        Vec2d cursorPosition;
        glfwGetCursorPos(m_pWindow, &cursorPosition.x, &cursorPosition.y);
        Vec2d delta = cursorPosition - m_LastCursorPosition;
        m_SphericalAngles -= delta * 0.01;
        m_LastCursorPosition = cursorPosition;

        if(delta.x || delta.y) {
            m_FrontVector = sphericalToCartesian(m_SphericalAngles);
            m_LeftVector = sphericalToCartesian(Vec2f(m_SphericalAngles.x + pi<float>() * 0.5f, 0.f));
            m_UpVector = cross(m_FrontVector, m_LeftVector);
            hasMoved = true;
        }
    }

    if(hasMoved) {
        setViewMatrix(lookAt(position, position + m_FrontVector, m_UpVector));
    }

    return hasMoved;
}

}
