#pragma once

#include <GL/glew.h>
#include "GLObject.hpp"
#include <iostream>
#include "memory.hpp"

namespace BnZ {

template<GLenum textureType>
class GLTextureHandle {
    GLuint64 m_nHandle;
public:
    explicit GLTextureHandle(GLuint64 handle = 0u) : m_nHandle(handle) {
    }

    explicit operator bool() const {
        return m_nHandle != 0u;
    }

    explicit operator GLuint64() const {
        return m_nHandle;
    }

    bool isResident() const {
        assert(m_nHandle);
        return glIsTextureHandleResidentNV(m_nHandle);
    }

    void makeResident() const {
        assert(m_nHandle);
        return glMakeTextureHandleResidentNV(m_nHandle);
    }

    void makeNonResident() const {
        assert(m_nHandle);
        return glMakeTextureHandleNonResidentNV(m_nHandle);
    }
};

template<GLenum textureType>
class GLImageHandle {
    GLuint64 m_nHandle;
public:
    explicit GLImageHandle(GLuint64 handle = 0u) : m_nHandle(handle) {
    }

    explicit operator bool() const {
        return m_nHandle != 0u;
    }

    explicit operator GLuint64() const {
        return m_nHandle;
    }

    void makeResident(GLenum access) const {
        glMakeImageHandleResidentNV(m_nHandle, access);
    }

    bool isResident() const {
        return glIsImageHandleResidentNV(m_nHandle);
    }

    void makeNonResident() const {
        glMakeImageHandleNonResidentNV(m_nHandle);
    }
};

template<GLenum textureType>
class GLTextureBase;

// This class use the CRTP (https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern) to extend its functionalities
// based on the texture type. The class GLTextureBase<T> contains the extended functionalities.
template<GLenum textureType>
class GLTexture : GLTextureObject, public GLTextureBase<textureType> {
    //GLuint64 m_nTextureHandle = 0;
    //GLuint64 m_nImageHandle = 0;
    GLTextureHandle<textureType> m_nTextureHandle;
public:
    using GLTextureObject::glId;

    static const GLenum TEXTURE_TYPE = textureType;
    using TextureHandle = GLTextureHandle<textureType>;

    explicit GLTexture(GLuint id) : GLTextureObject(id) {
    }

    ~GLTexture() {
        if(m_nTextureHandle && m_nTextureHandle.isResident()) {
            //std::cerr << "unresidentise" << std::endl;
            m_nTextureHandle.makeNonResident();
        }
    }

    GLTexture() = default;

    GLTexture(GLTexture&& rvalue) :
        GLTextureObject(std::move(rvalue)),
        m_nTextureHandle(rvalue.m_nTextureHandle) {
        rvalue.m_nTextureHandle = GLTextureHandle<textureType>(0);
    }

    GLTexture& operator =(GLTexture&& rvalue) {
        GLTextureObject::operator=(std::move(rvalue));
        std::swap(m_nTextureHandle, rvalue.m_nTextureHandle);
        return *this;
    }

    void getImage(GLint level, GLenum format, GLenum type, GLvoid *pixels) const {
        glGetTextureImageEXT(glId(), TEXTURE_TYPE, level, format, type, pixels);
    }

    void setParameter(GLenum pname, GLfloat param) {
        glTextureParameterfEXT(glId(), TEXTURE_TYPE, pname, param);
    }

    void setParameter(GLenum pname, GLint param) {
        glTextureParameteriEXT(glId(), TEXTURE_TYPE, pname, param);
    }

    void setMinFilter(GLint param) {
        setParameter(GL_TEXTURE_MIN_FILTER, param);
    }

    void setMagFilter(GLint param) {
        setParameter(GL_TEXTURE_MAG_FILTER, param);
    }

    void setWrapS(GLint param) {
        setParameter(GL_TEXTURE_WRAP_S, param);
    }

    void setWrapT(GLint param) {
        setParameter(GL_TEXTURE_WRAP_T, param);
    }

    void generateMipmap() {
        glGenerateTextureMipmapEXT(glId(), textureType);
    }

    void setCompareMode(GLint param) {
        setParameter(GL_TEXTURE_COMPARE_MODE, param);
    }

    void setCompareFunc(GLint param) {
        setParameter(GL_TEXTURE_COMPARE_FUNC, param);
    }

    void clear(GLint level, GLenum format, GLenum type, const void* data) {
        glClearTexImage(glId(), level, format, type, data);
    }

    GLTextureHandle<textureType> getTextureHandle() const {
        return m_nTextureHandle;
    }

    GLTextureHandle<textureType> makeTextureHandleResident() {
        if(!m_nTextureHandle) {
            m_nTextureHandle = GLTextureHandle<textureType>(glGetTextureHandleNV(glId()));
        }
        m_nTextureHandle.makeResident();
        return m_nTextureHandle;
    }

    void makeTextureHandleNonResident() {
        if(!m_nTextureHandle) {
            return;
        }
        m_nTextureHandle.makeNonResident();
    }

    GLImageHandle<textureType> getImageHandle(GLint level, GLboolean layered, GLint layer, GLenum format) const {
        return GLImageHandle<textureType>(glGetImageHandleNV(glId(), level, layered, layer, format));
    }

    void bind() const {
        glBindTexture(TEXTURE_TYPE, glId());
    }
};

class GLResidentTextureScope {
    GLResidentTextureScope(const GLResidentTextureScope&) = delete;
    GLResidentTextureScope& operator =(const GLResidentTextureScope&) = delete;
public:
    template<GLenum textureType>
    GLResidentTextureScope(GLTexture<textureType>& texture):
        m_nHandle(GLuint64(texture.makeTextureHandleResident())) {
    }

    ~GLResidentTextureScope() {
        if(m_nHandle) {
            glMakeTextureHandleNonResidentNV(m_nHandle);
        }
    }

private:
    GLuint64 m_nHandle = 0;
};

using GLTexture2D = GLTexture<GL_TEXTURE_2D>;
using GLTextureCubeMap = GLTexture<GL_TEXTURE_CUBE_MAP>;
using GLTexture2DArray = GLTexture<GL_TEXTURE_2D_ARRAY>;
using GLTextureCubeMapArray = GLTexture<GL_TEXTURE_CUBE_MAP_ARRAY>;

using GLTexture2DHandle = GLTextureHandle<GL_TEXTURE_2D>;
using GLTextureCubeMapHandle = GLTextureHandle<GL_TEXTURE_CUBE_MAP>;
using GLTexture2DArrayHandle = GLTextureHandle<GL_TEXTURE_2D_ARRAY>;
using GLTextureCubeMapArrayHandle = GLTextureHandle<GL_TEXTURE_CUBE_MAP_ARRAY>;

using GLImage2DHandle = GLImageHandle<GL_TEXTURE_2D>;
using GLImageCubeMapHandle = GLImageHandle<GL_TEXTURE_CUBE_MAP>;
using GLImage2DArrayHandle = GLImageHandle<GL_TEXTURE_2D_ARRAY>;
using GLImageCubeMapArrayHandle = GLImageHandle<GL_TEXTURE_CUBE_MAP_ARRAY>;

template<>
class GLTextureBase<GL_TEXTURE_2D> {
    GLsizei m_nWidth = 0;
    GLsizei m_nHeight = 0;
public:
    GLuint glId() const {
        return ((const GLTexture<GL_TEXTURE_2D>*) this)->glId();
    }

    void setImage(GLint level, GLenum internalFormat, GLsizei width, GLsizei height,
        GLint border, GLenum format, GLenum type, const GLvoid* data) {
        glTextureImage2DEXT(glId(), GL_TEXTURE_2D, level, internalFormat, width, height,
            border, format, type, data);
        m_nWidth = width;
        m_nHeight = height;
    }

    void setStorage(GLint levels, GLenum internalFormat, GLsizei width, GLsizei height) {
        glTextureStorage2DEXT(glId(), GL_TEXTURE_2D, levels, internalFormat, width, height);
        m_nWidth = width;
        m_nHeight = height;
    }

    GLsizei getWidth() const {
        return m_nWidth;
    }

    GLsizei getHeight() const {
        return m_nHeight;
    }
};

template<>
class GLTextureBase<GL_TEXTURE_CUBE_MAP> {
public:
    GLuint glId() const {
        return ((const GLTexture<GL_TEXTURE_CUBE_MAP>*) this)->glId();
    }

    void setImage(GLint face, GLint level, GLenum internalFormat, GLsizei width, GLsizei height,
        GLint border, GLenum format, GLenum type, const GLvoid* data) {
        glTextureImage2DEXT(glId(), GL_TEXTURE_CUBE_MAP_POSITIVE_X + face, level, internalFormat, width, height,
            border, format, type, data);
    }

    void setStorage(GLint face, GLint levels, GLenum internalFormat, GLsizei width, GLsizei height) {
        glTextureStorage2DEXT(glId(), GL_TEXTURE_CUBE_MAP_POSITIVE_X + face, levels, internalFormat, width, height);
    }

    void setStorage(GLint levels, GLenum internalFormat, GLsizei width, GLsizei height) {
        glTextureStorage2DEXT(glId(), GL_TEXTURE_CUBE_MAP, levels, internalFormat, width, height);
    }
};

template<>
class GLTextureBase<GL_TEXTURE_2D_ARRAY> {
    GLsizei m_nWidth = 0;
    GLsizei m_nHeight = 0;
    GLsizei m_nLayerCount = 0;
public:
    GLuint glId() const {
        return ((const GLTexture<GL_TEXTURE_2D_ARRAY>*) this)->glId();
    }
    
    void setImage(GLint level, GLenum internalFormat, GLsizei width, GLsizei height,
        GLsizei nbLayers, GLint border, GLenum format, GLenum type, const GLvoid* data) {
        glTextureImage3DEXT(glId(), GL_TEXTURE_2D_ARRAY, level, internalFormat, width, height, nbLayers,
            border, format, type, data);
        m_nWidth = width;
        m_nHeight = height;
        m_nLayerCount = nbLayers;
    }

    void setSubImage(GLint level,
        GLint xoffset,
        GLint yoffset,
        GLint zoffset,
        GLsizei width,
        GLsizei height,
        GLsizei nbLayers,
        GLenum format,
        GLenum type,
        const GLvoid * data) {
        glTextureSubImage3DEXT(glId(), GL_TEXTURE_2D_ARRAY, level, xoffset, yoffset, zoffset, width, height, nbLayers,
            format, type, data);
    }

    void setSubImage(
        GLint level,
        GLenum format,
        GLenum type,
        const GLvoid * data) {
        setSubImage(level, 0, 0, 0, m_nWidth, m_nHeight, m_nLayerCount, format, type, data);
    }

    void setStorage(GLint levels, GLenum internalFormat, GLsizei width, GLsizei height,
        GLsizei nbLayers) {
        glTextureStorage3DEXT(glId(), GL_TEXTURE_2D_ARRAY, levels, internalFormat, width, height, nbLayers);
        m_nWidth = width;
        m_nHeight = height;
        m_nLayerCount = nbLayers;
    }

    GLsizei getWidth() const {
        return m_nWidth;
    }

    GLsizei getHeight() const {
        return m_nHeight;
    }

    GLsizei getLayerCount() const {
        return m_nLayerCount;
    }
};

template<>
class GLTextureBase<GL_TEXTURE_CUBE_MAP_ARRAY> {
    GLsizei m_nWidth = 0;
    GLsizei m_nHeight = 0;
    GLsizei m_nLayerCount = 0;
public:
    GLuint glId() const {
        return ((const GLTexture<GL_TEXTURE_CUBE_MAP_ARRAY>*) this)->glId();
    }

    // data must contain 6 * nbLayers images
    void setImage(GLint level, GLenum internalFormat, GLsizei width, GLsizei height,
        GLsizei nbLayers, GLint border, GLenum format, GLenum type, const GLvoid* data) {
        glTextureImage3DEXT(glId(), GL_TEXTURE_CUBE_MAP_ARRAY, level, internalFormat, width, height, 6 * nbLayers,
            border, format, type, data);
        m_nWidth = width;
        m_nHeight = height;
        m_nLayerCount = nbLayers;
    }

    // The total depth of the underlying cube map array is 6 * nbLayers
    void setStorage(GLint levels, GLenum internalFormat, GLsizei width, GLsizei height,
        GLsizei nbLayers) {
        glTextureStorage3DEXT(glId(), GL_TEXTURE_CUBE_MAP_ARRAY, levels, internalFormat, width, height, 6 * nbLayers);
        m_nWidth = width;
        m_nHeight = height;
        m_nLayerCount = nbLayers;
    }

    GLsizei getWidth() const {
        return m_nWidth;
    }

    GLsizei getHeight() const {
        return m_nHeight;
    }

    GLsizei getLayerCount() const {
        return m_nLayerCount;
    }

    GLsizei getLayerFaceCount() const {
        return 6 * m_nLayerCount;
    }
};

}
