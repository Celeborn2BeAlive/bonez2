#pragma once

#include "GLObject.hpp"

namespace BnZ {

class GLQuery : GLQueryObject {
public:
    using GLQueryObject::glId;

    bool isResultAvailable() const {
        GLint done;
        glGetQueryObjectiv(glId(), GL_QUERY_RESULT_AVAILABLE, &done);
        return done == GL_TRUE;
    }

    GLuint64 getResult() const {
        GLuint64 result;
        glGetQueryObjectui64v(glId(), GL_QUERY_RESULT, &result);
        return result;
    }

    GLuint64 waitResult() const {
        GLint done = false;
        while(!done) {
            glGetQueryObjectiv(glId(), GL_QUERY_RESULT_AVAILABLE, &done);
        }
        return getResult();
    }

    void queryCounter() const {
        glQueryCounter(glId(), GL_TIMESTAMP);
    }

    void begin(GLenum type) {
        glBeginQuery(type, glId());
    }
};

template<GLenum type>
class GLQueryScope {
public:
    GLQueryScope(GLQuery& query) {
        query.begin(type);
    }

    ~GLQueryScope() {
        glEndQuery(type);
    }
};

using GLTimerScope = GLQueryScope<GL_TIME_ELAPSED>;

}
