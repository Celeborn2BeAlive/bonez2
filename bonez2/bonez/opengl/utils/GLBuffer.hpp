#pragma once

#include <vector>
#include <cassert>
#include "GLObject.hpp"

namespace BnZ {

template<typename T>
class GLBuffer: GLBufferObject {
    GLBufferAddress<T> m_nGPUAddress;
    GLenum m_Access;
    size_t m_Size = 0;
public:
    using GLBufferObject::glId;

    GLBuffer() = default;

    GLBuffer(GLBuffer&& rvalue) : GLBufferObject(std::move(rvalue)),
        m_nGPUAddress(rvalue.m_nGPUAddress), m_Access(rvalue.m_Access), m_Size(rvalue.m_Size) {
        rvalue.m_nGPUAddress = GLBufferAddress<T>();
    }

    ~GLBuffer() {
        makeNonResident();
    }

    GLBuffer& operator =(GLBuffer&& rvalue) {
        GLBufferObject::operator=(std::move(rvalue));
        m_nGPUAddress = rvalue.m_nGPUAddress;
        m_Access = rvalue.m_Access;
        m_Size = rvalue.m_Size;
        rvalue.m_nGPUAddress = GLBufferAddress<T>();
        return *this;
    }

    size_t size() const {
        return m_Size;
    }

    void setStorage(size_t count, GLenum usage) {
        bool wasResident = false;
        if(m_nGPUAddress) {
            wasResident = true;
            makeNonResident();
        }

        m_Size = count;
        glNamedBufferDataEXT(glId(), m_Size * sizeof(T), nullptr, usage);
        
        if(wasResident) {
            m_nGPUAddress = makeResident(m_Access);
        }
    }

    void setData(size_t count, const T* data, GLenum usage) {
        bool wasResident = false;
        if(m_nGPUAddress) {
            wasResident = true;
            makeNonResident();
        }

        m_Size = count;
        glNamedBufferDataEXT(glId(), m_Size * sizeof(T), data, usage);

        if(wasResident) {
            m_nGPUAddress = makeResident(m_Access);
        }
    }

    void setData(const std::vector<T>& data, GLenum usage) {
        setData(data.size(), data.data(), usage);
    }

    void setSubData(uint32_t offset, size_t count, const T* data) {
        assert(offset + count <= m_Size);
        glNamedBufferSubDataEXT(glId(), offset * sizeof(T), count * sizeof(T), data);
    }

    void setSubData(uint32_t offset, const std::vector<T>& data) {
        setSubData(offset, data.size(), data.data());
    }

    void setSubData(const std::vector<T>& data) {
        setSubData(0, data);
    }

    void bind(GLenum target) const {
        glBindBuffer(target, glId());
    }

    void bindBase(GLuint index, GLenum target) const {
        glBindBufferBase(target, index, glId());
    }

    GLBufferAddress<T> getGPUAddress() const {
        return m_nGPUAddress;
    }

    GLBufferAddress<T> makeResident(GLenum access) {
        if(!m_nGPUAddress) {
            glMakeNamedBufferResidentNV(glId(), access);
            GLuint64 address;
            glGetNamedBufferParameterui64vNV(glId(),
                                             GL_BUFFER_GPU_ADDRESS_NV,
                                             &address);
            m_nGPUAddress = GLBufferAddress<T>(address);
            m_Access = access;
        }
        else if (access != m_Access) {
            makeNonResident();
            makeResident(access);
        }
        return m_nGPUAddress;
    }

    void makeNonResident() {
        if(m_nGPUAddress) {
            glMakeNamedBufferNonResidentNV(glId());
            m_nGPUAddress = GLBufferAddress<T>();
        }
    }

    T* map(GLenum access) {
        return (T*) glMapNamedBufferEXT(glId(), access);
    }

    void unmap() {
        glUnmapNamedBufferEXT(glId());
    }

    void getData(size_t offset, size_t count, T* data) {
        glGetNamedBufferSubDataEXT(glId(), offset * sizeof(T), count * sizeof(T), data);
    }

    void getData(size_t count, T* data) {
        getData(0, count, data);
    }

    void getData(size_t offset, std::vector<T>& data) {
        getData(offset, data.size(), data.data());
    }

    void getData(std::vector<T>& data) {
        getData(0, data);
    }
};

}
