#pragma once

#include "bonez/opengl/opengl.hpp"

namespace BnZ {

template<typename Factory>
class GLObject {
    GLuint m_GLId;
public:
    explicit GLObject(GLuint id) :
        m_GLId(id) {
    }

    GLObject() {
        Factory::genObjects(1, &m_GLId);
    }

    ~GLObject() {
        Factory::deleteObjects(1, &m_GLId);
    }

    GLObject(const GLObject&) = delete;

    GLObject& operator =(const GLObject&) = delete;

    GLObject(GLObject&& rvalue): m_GLId(rvalue.m_GLId) {
        rvalue.m_GLId = 0;
    }

    GLObject& operator =(GLObject&& rvalue) {
        this->~GLObject();
        m_GLId = rvalue.m_GLId;
        rvalue.m_GLId = 0;
        return *this;
    }

    explicit operator bool() const {
        return m_GLId != 0;
    }

    GLuint glId() const {
        return m_GLId;
    }
};

#define GL_OBJECT(OBJECT) \
struct GL##OBJECT##Factory { \
    static void genObjects(GLsizei n, GLuint* objects) { \
        glGen##OBJECT##s(n, objects); \
    } \
    static void deleteObjects(GLsizei n, GLuint* objects) { \
        glDelete##OBJECT##s(n, objects); \
    } \
}; \
using GL##OBJECT##Object = GLObject<GL##OBJECT##Factory>

GL_OBJECT(Buffer);
GL_OBJECT(Texture);
GL_OBJECT(Framebuffer);
GL_OBJECT(TransformFeedback);
GL_OBJECT(VertexArray);

struct GLQueryFactory {
    static void genObjects(GLsizei n, GLuint* objects) {
        glGenQueries(n, objects);
    }
    static void deleteObjects(GLsizei n, GLuint* objects) {
        glDeleteQueries(n, objects);
    }
};
using GLQueryObject = GLObject<GLQueryFactory>;

#undef GL_OBJECT

template<typename T>
class GLBufferAddress {
    GLuint64 m_nAddress;
public:
    explicit GLBufferAddress(GLuint64 address = 0u):
        m_nAddress(address) {
    }

    explicit operator GLuint64() const {
        return m_nAddress;
    }

    explicit operator bool() const {
        return m_nAddress != 0;
    }

    template<typename Integer>
    GLBufferAddress& operator +=(Integer offset) {
        m_nAddress += sizeof(T) * offset;
        return *this;
    }

    template<typename Integer>
    friend GLBufferAddress operator +(GLBufferAddress addr, Integer offset) {
        return addr += offset;
    }

    template<typename Integer>
    friend GLBufferAddress operator +(Integer offset, GLBufferAddress addr) {
        return addr + offset;
    }
};

}
