#pragma once

#include <bonez/opengl/opengl.hpp>
#include <vector>
#include "GLObject.hpp"
#include "memory.hpp"
#include <iostream>

namespace BnZ {

template<typename T>
class GLImmutableBuffer {
    GLImmutableBuffer(const GLImmutableBuffer&) = default;

    GLImmutableBuffer& operator =(const GLImmutableBuffer&) = default;
public:
    GLImmutableBuffer() = default;

    ~GLImmutableBuffer() {
        if(m_nGLId) {
            makeNonResident();
            glDeleteBuffers(1, &m_nGLId);
        }
    }

    GLImmutableBuffer(size_t size, const T* data, GLbitfield flags = 0):
        m_nSize(size) {
        glGenBuffers(1, &m_nGLId);
        glNamedBufferStorageEXT(m_nGLId, m_nSize * sizeof(T), data, flags);
    }

    GLImmutableBuffer(size_t size, const T* data, GLbitfield flags, GLenum residentAccess):
        GLImmutableBuffer(size, data, flags) {
        makeResident(residentAccess);
    }

    GLImmutableBuffer(const std::vector<T>& data, GLbitfield flags = 0):
        GLImmutableBuffer(data.size(), data.data(), flags) {
    }

    GLImmutableBuffer(const std::vector<T>& data, GLbitfield flags, GLenum residentAccess):
        GLImmutableBuffer(data.size(), data.data(), flags, residentAccess) {
    }

    GLImmutableBuffer(GLImmutableBuffer&& rvalue):
        GLImmutableBuffer((const GLImmutableBuffer&) rvalue) {
        rvalue.m_nGLId = 0;
        rvalue.m_nAddress = GLBufferAddress<T>();
    }

    GLImmutableBuffer& operator =(GLImmutableBuffer&& rvalue) {
        this->~GLImmutableBuffer();
        *this = (const GLImmutableBuffer&) rvalue;
        rvalue.m_nGLId = 0;
        rvalue.m_nAddress = GLBufferAddress<T>();
        return *this;
    }

    GLuint glId() const {
        return m_nGLId;
    }

    explicit operator bool() const {
        return m_nGLId != 0;
    }

    size_t size() const {
        return m_nSize;
    }

    size_t byteSize() const {
        return m_nSize * sizeof(T);
    }

    void bind(GLenum target) const {
        glBindBuffer(target, glId());
    }

    void bindBase(GLuint index, GLenum target) const {
        glBindBufferBase(target, index, glId());
    }

    GLBufferAddress<T> makeResident(GLenum access) {
        if(!m_nAddress) {
            glMakeNamedBufferResidentNV(m_nGLId, access);
            GLuint64 address;
            glGetNamedBufferParameterui64vNV(m_nGLId,
                                             GL_BUFFER_GPU_ADDRESS_NV,
                                             &address);
            m_nAddress = GLBufferAddress<T>(address);
            m_Access = access;
        }
        else if (access != m_Access) {
            makeNonResident();
            makeResident(access);
        }
        return m_nAddress;
    }

    void makeNonResident() {
        if(m_nAddress) {
            glMakeNamedBufferNonResidentNV(m_nGLId);
            m_nAddress = GLBufferAddress<T>();
            m_Access = 0;
        }
    }

    GLBufferAddress<T> getGPUAddress() const {
        return m_nAddress;
    }

    T* map(size_t offset, size_t length, GLbitfield access) {
        return (T*) glMapNamedBufferRangeEXT(m_nGLId, offset * sizeof(T),
                                     length * sizeof(T), access);
    }

    T* map(GLbitfield access) {
        return (T*) glMapNamedBufferRangeEXT(m_nGLId, 0,
                                     m_nSize * sizeof(T), access);
    }

    void unmap() {
        glUnmapNamedBufferEXT(m_nGLId);
    }

    void getData(size_t offset, size_t count, T* data) {
        glGetNamedBufferSubDataEXT(glId(), offset * sizeof(T), count * sizeof(T), data);
    }

    /*
    void getData(size_t count, T* data) {
        getData(0, count, data);
    }

    void getData(size_t offset, std::vector<T>& data) {
        getData(offset, data.size(), data.data());
    }*/

    void getData(std::vector<T>& data) {
        getData(0, data.size(), data.data());
    }

private:
    GLuint m_nGLId = 0;
    size_t m_nSize = 0;
    GLBufferAddress<T> m_nAddress;
    GLenum m_Access = 0;
};

template<typename T>
inline GLImmutableBuffer<T> genValueStorage(const T& value, GLbitfield flags = 0) {
    return GLImmutableBuffer<T>(sizeof(value), &value, flags);
}

template<typename T>
inline GLImmutableBuffer<T> genValueStorage(const T& value, GLbitfield flags, GLenum residentAccess) {
    return GLImmutableBuffer<T>(sizeof(value), &value, flags, residentAccess);
}

template<typename T>
inline GLImmutableBuffer<T> genBufferStorage(size_t size, const T* data,
                                             GLbitfield flags = 0) {
    return GLImmutableBuffer<T>(size, data, flags);
}

template<typename T>
inline GLImmutableBuffer<T> genBufferStorage(size_t size, const T* data,
                                             GLbitfield flags,
                                             GLenum residentAccess) {
    return GLImmutableBuffer<T>(size, data, flags, residentAccess);
}

template<typename T>
inline GLImmutableBuffer<T> genBufferStorage(const std::vector<T>& data, GLbitfield flags = 0) {
    return GLImmutableBuffer<T>(data, flags);
}

template<typename T>
inline GLImmutableBuffer<T> genBufferStorage(const std::vector<T>& data, GLbitfield flags,
                                             GLenum residentAccess) {
    return GLImmutableBuffer<T>(data, flags, residentAccess);
}

}
