#pragma once

#include <bonez/types.hpp>
#include "GLShader.hpp"
#include "GLProgram.hpp"
#include "GLFramebuffer.hpp"
#include "GLCubeFramebuffer.hpp"
#include "GLQuery.hpp"
#include "GLBuffer.hpp"
#include "GLImmutableBuffer.hpp"
#include "GLState.hpp"
#include "GLUniform.hpp"
#include "GLVertexArray.hpp"
#include "memory.hpp"

namespace BnZ {

// A full screen triangle
struct GLScreenTriangle {
    GLImmutableBuffer<float> vbo;
    GLVertexArray vao;

    GLScreenTriangle(GLuint attribLocation = 0):
        vbo({ -1, -1, 3, -1, -1, 3 }, 0, GL_READ_ONLY) {
        vao.enableVertexAttrib(attribLocation);
        vao.vertexAttribOffset(vbo.glId(), attribLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);
    }

    void render() const {
        vao.bind();
        glDrawArrays(GL_TRIANGLES, 0, 3);
    }
};

inline GLintptr bufferOffset(size_t offset) {
    return GLintptr(offset);
}

#define BNZ_OFFSETOF(TYPE, MEMBER) BnZ::bufferOffset(offsetof(TYPE, MEMBER))

}
