#pragma once

#include <bonez/opengl/opengl.hpp>

namespace BnZ {

inline GLint getGPUMemoryInfoTotalAvailable() {
    GLint total_mem_kb = 0;
    glGetIntegerv(GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX,
                  &total_mem_kb);
    return total_mem_kb;
}

inline GLint getGPUMemoryInfoCurrentAvailable() {
    GLint cur_avail_mem_kb = 0;
    glGetIntegerv(GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX,
                  &cur_avail_mem_kb);
    return cur_avail_mem_kb;
}

}
