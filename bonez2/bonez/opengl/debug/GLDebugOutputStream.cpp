#include "GLDebugOutputStream.hpp"

namespace BnZ {

GLDebugOutputStream::~GLDebugOutputStream() {
    if(m_pRenderer) {
        m_pRenderer->deleteStream(m_sName);
        m_pRenderer = nullptr;
    }
}

void GLDebugOutputStream::clearObjects() {
    if(m_pRenderer) {
        m_pRenderer->clearObjects(m_sName);
    }
}

void GLDebugOutputStream::addSphere(const Vec3f& center, float radius,
               const Vec3f& color, const Vec4u& objectID) {
    if(m_pRenderer) {
        m_pRenderer->addSphere(m_sName, center, radius, color, objectID);
    }
}

void GLDebugOutputStream::addArrow(const Vec3f& position, const Vec3f& direction, float length, float baseRadius,
              const Vec3f& color, const Vec4u& objectID) {
    if(m_pRenderer) {
        m_pRenderer->addArrow(m_sName, position, direction, length, baseRadius, color, objectID);
    }
}

void GLDebugOutputStream::addLine(const Vec3f& org, const Vec3f& dst,
              const Vec3f& orgColor, const Vec3f& dstColor, float lineWidth,
              const Vec4u& objectID) {
    if(m_pRenderer) {
        m_pRenderer->addLine(m_sName, org, dst, orgColor, dstColor, lineWidth,
                             objectID);
    }
}

}
