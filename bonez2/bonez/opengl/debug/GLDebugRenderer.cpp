#include "GLDebugRenderer.hpp"
#include <bonez/maths/maths.hpp>

namespace BnZ {

Vec4u GLDebugRenderer::NULL_ID ( 0.f );

GLDebugRenderer::Object::Object() {
    VAO.enableVertexAttrib(0);
    VAO.enableVertexAttrib(1);
    VAO.vertexAttribOffset(VBO.glId(), 0, 3, GL_FLOAT, GL_FALSE,
                           sizeof(Vertex), BNZ_OFFSETOF(Vertex, position));
    VAO.vertexAttribOffset(VBO.glId(), 1, 3, GL_FLOAT, GL_FALSE,
                           sizeof(Vertex), BNZ_OFFSETOF(Vertex, normal));
}

void GLDebugRenderer::setOutputColorTexture(GLTexture2D& outputColorTexture) {
    auto width = outputColorTexture.getWidth();
    auto height = outputColorTexture.getHeight();

    glNamedFramebufferTexture2DEXT(m_FBO.glId(), GL_COLOR_ATTACHMENT0,
                                 GL_TEXTURE_2D, outputColorTexture.glId(), 0);

    m_ObjectIDTexture.setImage(0, GL_RGBA32UI, width, height, 0,
                               GL_RGBA_INTEGER, GL_UNSIGNED_INT, nullptr);
    m_ObjectIDTexture.setMinFilter(GL_NEAREST);
    m_ObjectIDTexture.setMagFilter(GL_NEAREST);

    glNamedFramebufferTexture2DEXT(m_FBO.glId(), GL_COLOR_ATTACHMENT1,
                                 GL_TEXTURE_2D, m_ObjectIDTexture.glId(), 0);

    m_DepthTexture.setImage(0, GL_DEPTH_COMPONENT32F, width, height, 0,
                            GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
    m_DepthTexture.setMinFilter(GL_NEAREST);
    m_DepthTexture.setMagFilter(GL_NEAREST);

    glNamedFramebufferTexture2DEXT(m_FBO.glId(), GL_DEPTH_ATTACHMENT,
                                 GL_TEXTURE_2D, m_DepthTexture.glId(), 0);

    m_DrawBuffers[0] = GL_COLOR_ATTACHMENT0;
    m_DrawBuffers[1] = GL_COLOR_ATTACHMENT1;

    // check that the FBO is complete
    GLenum status = glCheckNamedFramebufferStatusEXT(m_FBO.glId(), GL_DRAW_FRAMEBUFFER);
    if(GL_FRAMEBUFFER_COMPLETE != status) {
        std::cerr << GLFramebufferErrorString(status) << std::endl;
        throw std::runtime_error(GLFramebufferErrorString(status));
    }
}

void GLDebugRenderer::Object::setInstanceBuffer(GLuint instanceBuffer) {
    VAO.bind();

    VAO.enableVertexAttrib(2);
    VAO.vertexAttribOffset(instanceBuffer, 2, 3, GL_FLOAT, GL_FALSE,
                           sizeof(ObjectInstance), BNZ_OFFSETOF(ObjectInstance, color));
    glVertexAttribDivisor(2, 1);

    VAO.enableVertexAttrib(3);
    VAO.vertexAttribOffset(instanceBuffer, 3, 4, GL_FLOAT, GL_FALSE,
                           sizeof(ObjectInstance), BNZ_OFFSETOF(ObjectInstance, mvpMatrix) + 0 * sizeof(Vec4f));
    glVertexAttribDivisor(3, 1);

    VAO.enableVertexAttrib(4);
    VAO.vertexAttribOffset(instanceBuffer, 4, 4, GL_FLOAT, GL_FALSE,
                           sizeof(ObjectInstance), BNZ_OFFSETOF(ObjectInstance, mvpMatrix) + 1 * sizeof(Vec4f));
    glVertexAttribDivisor(4, 1);

    VAO.enableVertexAttrib(5);
    VAO.vertexAttribOffset(instanceBuffer, 5, 4, GL_FLOAT, GL_FALSE,
                           sizeof(ObjectInstance), BNZ_OFFSETOF(ObjectInstance, mvpMatrix) + 2 * sizeof(Vec4f));
    glVertexAttribDivisor(5, 1);

    VAO.enableVertexAttrib(6);
    VAO.vertexAttribOffset(instanceBuffer, 6, 4, GL_FLOAT, GL_FALSE,
                           sizeof(ObjectInstance), BNZ_OFFSETOF(ObjectInstance, mvpMatrix) + 3 * sizeof(Vec4f));
    glVertexAttribDivisor(6, 1);

    VAO.enableVertexAttrib(7);
    VAO.vertexAttribOffset(instanceBuffer, 7, 4, GL_FLOAT, GL_FALSE,
                           sizeof(ObjectInstance), BNZ_OFFSETOF(ObjectInstance, mvMatrix) + 0 * sizeof(Vec4f));
    glVertexAttribDivisor(7, 1);

    VAO.enableVertexAttrib(8);
    VAO.vertexAttribOffset(instanceBuffer, 8, 4, GL_FLOAT, GL_FALSE,
                           sizeof(ObjectInstance), BNZ_OFFSETOF(ObjectInstance, mvMatrix) + 1 * sizeof(Vec4f));
    glVertexAttribDivisor(8, 1);

    VAO.enableVertexAttrib(9);
    VAO.vertexAttribOffset(instanceBuffer, 9, 4, GL_FLOAT, GL_FALSE,
                           sizeof(ObjectInstance), BNZ_OFFSETOF(ObjectInstance, mvMatrix) + 2 * sizeof(Vec4f));
    glVertexAttribDivisor(9, 1);

    VAO.enableVertexAttrib(10);
    VAO.vertexAttribOffset(instanceBuffer, 10, 4, GL_FLOAT, GL_FALSE,
                           sizeof(ObjectInstance), BNZ_OFFSETOF(ObjectInstance, mvMatrix) + 3 * sizeof(Vec4f));
    glVertexAttribDivisor(10, 1);

    VAO.enableVertexAttrib(11);
    VAO.vertexAttribIOffset(instanceBuffer, 11, 4, GL_UNSIGNED_INT,
                           sizeof(ObjectInstance), BNZ_OFFSETOF(ObjectInstance, objectID));
    glVertexAttribDivisor(11, 1);
}

void GLDebugRenderer::Object::draw() {
    VAO.bind();
    IBO.bind(GL_ELEMENT_ARRAY_BUFFER);
    glDrawElements(primType, IBO.size(), GL_UNSIGNED_SHORT, 0);
}

void GLDebugRenderer::Object::draw(uint32_t nbInstances) {
    VAO.bind();
    IBO.bind(GL_ELEMENT_ARRAY_BUFFER);
    glDrawElementsInstanced(primType, IBO.size(), GL_UNSIGNED_SHORT, 0, nbInstances);
}

void GLDebugRenderer::buildSphere(GLsizei discLat, GLsizei discLong, Object& model) {
    GLfloat rcpLat = 1.f / discLat, rcpLong = 1.f / discLong;
    GLfloat dPhi = pi<float>() * 2.f * rcpLat, dTheta = pi<float>() * rcpLong;

    std::vector<Vertex> vertices;

    float pi_over_two = pi<float>() / 2.f;

    // Construit l'ensemble des vertex
    for(GLsizei j = 0; j <= discLong; ++j) {
        GLfloat cosTheta = cos(-pi_over_two + j * dTheta);
        GLfloat sinTheta = sin(-pi_over_two + j * dTheta);

        for(GLsizei i = 0; i <= discLat; ++i) {
            Vec3f coords;

            coords.x = sin(i * dPhi) * cosTheta;
            coords.y = sinTheta;
            coords.z = cos(i * dPhi) * cosTheta;

            vertices.emplace_back(coords, coords);
        }
    }

    std::vector<uint16_t> indices;

    for(GLsizei j = 0; j < discLong; ++j) {
        GLsizei offset = j * (discLat + 1);
        for(GLsizei i = 0; i < discLat; ++i) {
            indices.push_back(offset + i);
            indices.push_back(offset + (i + 1));
            indices.push_back(offset + discLat + 1 + (i + 1));

            indices.push_back(offset + i);
            indices.push_back(offset + discLat + 1 + (i + 1));
            indices.push_back(offset + i + discLat + 1);
        }
    }

    model.VBO.setData(vertices, GL_STATIC_DRAW);
    model.IBO.setData(indices, GL_STATIC_DRAW);
    model.primType = GL_TRIANGLES;
}

void GLDebugRenderer::buildDisk(GLsizei discBase, Object& model) {
    GLfloat rcpBase = 1.f / discBase;
    GLfloat dPhi = pi<float>() * 2.f * rcpBase;

    std::vector<Vertex> vertices;

    // Construit l'ensemble des vertex

    Vec3f normal(0, 1, 0);

    // Origin
    vertices.emplace_back(Vec3f(0), normal);

    for(GLsizei i = 0; i <= discBase; ++i) {
        vertices.emplace_back(Vec3f(sin(i * dPhi), 0, cos(i * dPhi)), normal);
    }

    std::vector<uint16_t> indices;

    for(GLsizei i = 0; i < discBase; ++i) {
        indices.push_back(0);
        indices.push_back(i + 1);
        indices.push_back(i + 2);
    }

    model.VBO.setData(vertices, GL_STATIC_DRAW);
    model.IBO.setData(indices, GL_STATIC_DRAW);
    model.primType = GL_TRIANGLES;
}

void GLDebugRenderer::buildCircle(GLsizei discBase, Object& model) {
    GLfloat rcpBase = 1.f / discBase;
    GLfloat dPhi = pi<float>() * 2.f * rcpBase;

    std::vector<Vertex> vertices;

    // Construit l'ensemble des vertex

    Vec3f normal(0, 1, 0);

    for(GLsizei i = 0; i <= discBase; ++i) {
        vertices.emplace_back(Vec3f(sin(i * dPhi), 0, cos(i * dPhi)),
                                         normal);
    }

    std::vector<uint16_t> indices;

    for(GLsizei i = 0; i < discBase; ++i) {
        indices.push_back(i);
        indices.push_back(i + 1);
    }

    model.VBO.setData(vertices, GL_STATIC_DRAW);
    model.IBO.setData(indices, GL_STATIC_DRAW);
    model.primType = GL_LINES;
}

void GLDebugRenderer::buildCone(GLsizei discLat, GLsizei discHeight, Object& model) {
    GLfloat rcpLat = 1.f / discLat, rcpH = 1.f / discHeight;
    GLfloat dPhi = pi<float>() * 2.f * rcpLat, dH = rcpH;

    std::vector<Vertex> vertices;

    // Construit l'ensemble des vertex
    for(GLsizei j = 0; j <= discHeight; ++j) {
        for(GLsizei i = 0; i < discLat; ++i) {
            Vec3f position, normal;

            position.x = (1.f - j * dH) * sin(i * dPhi);
            position.y = j * dH;
            position.z = (1.f - j * dH) * cos(i * dPhi);

            normal.x = sin(i * dPhi);
            normal.y = 1.f;
            normal.z = cos(i * dPhi);

            vertices.emplace_back(position, normalize(normal));
        }
    }

    std::vector<uint16_t> indices;

    // Construit les vertex finaux en regroupant les données en triangles:
    // Pour une longitude donnée, les deux triangles formant une face sont de la forme:
    // (i, i + 1, i + discLat + 1), (i, i + discLat + 1, i + discLat)
    // avec i sur la bande correspondant à la longitude
    for(GLsizei j = 0; j < discHeight; ++j) {
        GLsizei offset = j * discLat;
        for(GLsizei i = 0; i < discLat; ++i) {
            indices.push_back(offset + i);
            indices.push_back(offset + (i + 1) % discLat);
            indices.push_back(offset + discLat + (i + 1) % discLat);

            indices.push_back(offset + i);
            indices.push_back(offset + discLat + (i + 1) % discLat);
            indices.push_back(offset + i + discLat);
        }
    }

    model.VBO.setData(vertices, GL_STATIC_DRAW);
    model.IBO.setData(indices, GL_STATIC_DRAW);
    model.primType = GL_TRIANGLES;
}

void GLDebugRenderer::buildCylinder(GLsizei discLat, GLsizei discHeight, Object& model) {
    GLfloat rcpLat = 1.f / discLat, rcpH = 1.f / discHeight;
    GLfloat dPhi = pi<float>() * 2.f * rcpLat, dH = rcpH;

    std::vector<Vertex> vertices;

    // Construit l'ensemble des vertex
    for(GLsizei j = 0; j <= discHeight; ++j) {
        for(GLsizei i = 0; i < discLat; ++i) {
            Vec3f position, normal;

            normal.x = sin(i * dPhi);
            normal.y = 0;
            normal.z = cos(i * dPhi);

            position = normal;
            position.y = j * dH;

            vertices.emplace_back(position, normal);
        }
    }

    std::vector<uint16_t> indices;

    for(GLsizei j = 0; j < discHeight; ++j) {
        GLsizei offset = j * discLat;
        for(GLsizei i = 0; i < discLat; ++i) {
            indices.push_back(offset + i);
            indices.push_back(offset + (i + 1) % discLat);
            indices.push_back(offset + discLat + (i + 1) % discLat);

            indices.push_back(offset + i);
            indices.push_back(offset + discLat + (i + 1) % discLat);
            indices.push_back(offset + i + discLat);
        }
    }

    model.VBO.setData(vertices, GL_STATIC_DRAW);
    model.IBO.setData(indices, GL_STATIC_DRAW);
    model.primType = GL_TRIANGLES;
}

GLDebugRenderer::GLDebugRenderer(const GLShaderManager &shaderManager):
    m_Program(shaderManager.buildProgram({ "debug.vs", "debug.fs" })),
    m_DrawLinesPass(shaderManager) {
    buildSphere(16, 8, m_Sphere);
    buildDisk(8, m_Disk);
    buildCone(8, 1, m_Cone);
    buildCylinder(8, 1, m_Cylinder);
    buildCircle(32, m_Circle);

    for(auto object: { &m_Sphere, &m_Disk, &m_Cone, &m_Cylinder, &m_Circle }) {
        object->setInstanceBuffer(m_InstanceBuffer.glId());
    }
}

GLDebugRenderer::StreamData::StreamData() {
    m_LineVAO.enableVertexAttrib(0);
    m_LineVAO.enableVertexAttrib(1);
    m_LineVAO.vertexAttribOffset(m_LineVBO.glId(), 0, 3, GL_FLOAT, GL_FALSE,
                                  sizeof(LineVertex), BNZ_OFFSETOF(LineVertex, position));
    m_LineVAO.vertexAttribOffset(m_LineVBO.glId(), 1, 3, GL_FLOAT, GL_FALSE,
                                  sizeof(LineVertex), BNZ_OFFSETOF(LineVertex, color));
}

GLDebugRenderer::DrawLinesPass::DrawLinesPass(const GLShaderManager &shaderManager):
    m_Program(shaderManager.buildProgram({ "debugLines.vs", "debugLines.fs" })) {
}

/*
void GLDebugRenderer::DrawLinesPass::clear() {
    m_Vertices.clear();
    m_Instances.clear();
}
*/
/*
void GLDebugRenderer::clearObjects() {

    m_SphereInstances.clear();
    m_ArrowInstances.clear();

    m_DrawLinesPass.clear();
}
*/
void GLDebugRenderer::StreamData::addSphere(const Vec3f& center, float radius,
                                const Vec3f& color, const Vec4u& objectID) {
    m_SphereInstances.emplace_back(scale(translate(Mat4f(1.f), center), Vec3f(radius)),
                                   color, objectID);
    //m_bNeedSphereUpdate = true;
}

void GLDebugRenderer::StreamData::addArrow(const Vec3f& position, const Vec3f& normal, float length, float baseRadius,
              const Vec3f& color, const Vec4u& objectID) {

    Mat4f modelMatrix = scale(frameY(position, normal), Vec3f(baseRadius, length, baseRadius));

    m_ArrowInstances.emplace_back(modelMatrix, color, objectID);
    //m_bNeedArrowUpdate = true;
}

void GLDebugRenderer::StreamData::addLine(const Vec3f& org, const Vec3f& dst,
             const Vec3f& orgColor, const Vec3f& dstColor, float lineWidth,
             const Vec4u& objectID) {
    m_LineVertices.emplace_back(org, orgColor);
    m_LineVertices.emplace_back(dst, dstColor);
    m_LineInstances.emplace_back(lineWidth, objectID);
}

void GLDebugRenderer::addSphere(const std::string& stream, const Vec3f& center, float radius,
                                const Vec3f& color, const Vec4u& objectID) {
    auto it = m_Streams.find(stream);
    if(end(m_Streams) == it) {
        throw std::runtime_error("Stream " + stream + " doesn't exist");
    }
    (*it).second.addSphere(center, radius, color, objectID);
}

void GLDebugRenderer::addArrow(const std::string& stream, const Vec3f& position, const Vec3f& normal, float length, float baseRadius,
              const Vec3f& color, const Vec4u& objectID) {

    auto it = m_Streams.find(stream);
    if(end(m_Streams) == it) {
        throw std::runtime_error("Stream " + stream + " doesn't exist");
    }
    (*it).second.addArrow(position, normal, length, baseRadius, color, objectID);
}

void GLDebugRenderer::addLine(const std::string& stream, const Vec3f& org, const Vec3f& dst,
             const Vec3f& orgColor, const Vec3f& dstColor, float lineWidth,
             const Vec4u& objectID) {
    auto it = m_Streams.find(stream);
    if(end(m_Streams) == it) {
        throw std::runtime_error("Stream " + stream + " doesn't exist");
    }
    (*it).second.addLine(org, dst, orgColor, dstColor, lineWidth, objectID);
}

void GLDebugRenderer::clearObjects(const std::string &stream) {
    auto it = m_Streams.find(stream);
    if(end(m_Streams) == it) {
        throw std::runtime_error("Stream " + stream + " doesn't exist");
    }
    (*it).second.clearObjects();
}

void GLDebugRenderer::render(const GLGBuffer& gBuffer,
                             const ProjectiveCamera& camera) {
    auto readFBO = pushGLState<GL_READ_FRAMEBUFFER_BINDING>();

    auto depthTest = pushGLState<GL_DEPTH_TEST>();
    depthTest.set(true);

    auto viewport = pushGLState<GL_VIEWPORT>();
    viewport.set(Vec4f(0, 0, gBuffer.getWidth(), gBuffer.getHeight()));

    auto drawFBO = pushGLState<GL_DRAW_FRAMEBUFFER_BINDING>();
    drawFBO.set(m_FBO.glId());

    gBuffer.bindForReading();
    glBlitFramebuffer(0, 0, gBuffer.getWidth(), gBuffer.getHeight(),
                      0, 0, gBuffer.getWidth(), gBuffer.getHeight(),
                      GL_DEPTH_BUFFER_BIT, GL_NEAREST);

    m_DrawBuffers[0] = GL_NONE;
    m_DrawBuffers[1] = GL_COLOR_ATTACHMENT1;
    glDrawBuffers(2, m_DrawBuffers);

    glClear(GL_COLOR_BUFFER_BIT);

    m_DrawBuffers[0] = GL_COLOR_ATTACHMENT0;
    m_DrawBuffers[1] = GL_COLOR_ATTACHMENT1;
    glDrawBuffers(2, m_DrawBuffers);

    auto vpMatrix = camera.getViewProjMatrix();
    auto vMatrix = camera.getViewMatrix();

    m_Program.use();

    for(auto& pair: m_Streams) {
        std::vector<ObjectInstance> copy = pair.second.m_SphereInstances;
        for(auto& inst: copy) {
            inst.mvpMatrix = vpMatrix * inst.mvpMatrix;
            inst.mvMatrix = vMatrix * inst.mvMatrix;
        }

        m_InstanceBuffer.setData(copy, GL_STREAM_DRAW);
        m_Sphere.draw(m_InstanceBuffer.size());

        copy = pair.second.m_ArrowInstances;
        for(auto& inst: copy) {
            inst.mvpMatrix = vpMatrix * inst.mvpMatrix;
            inst.mvMatrix = vMatrix * inst.mvMatrix;
        }

        m_InstanceBuffer.setData(copy, GL_STREAM_DRAW);
        m_Cylinder.draw(m_InstanceBuffer.size());

        for(auto& inst: copy) {
            inst.mvpMatrix = translate(scale(inst.mvpMatrix, Vec3f(3.f, 1.f, 3.f)), Vec3f(0, 1, 0));
            inst.mvMatrix = translate(scale(inst.mvMatrix, Vec3f(3.f, 1.f, 3.f)), Vec3f(0, 1, 0));
        }

        m_InstanceBuffer.setData(copy, GL_STREAM_DRAW);
        m_Cone.draw(m_InstanceBuffer.size());
    }

    m_DrawLinesPass.m_Program.use();

    m_DrawLinesPass.uMVPMatrix.set(vpMatrix);

    for(auto& pair: m_Streams) {
        pair.second.m_LineVBO.setData(pair.second.m_LineVertices, GL_STREAM_DRAW);

        uint32_t offset = 0;
        pair.second.m_LineVAO.bind();
        for(const auto& instance: pair.second.m_LineInstances) {
            glLineWidth(instance.lineWidth);
            m_DrawLinesPass.uObjectID.set(instance.objectID);
            glDrawArrays(GL_LINES, offset, 2);
            offset += 2;
        }
    }

    /*
    for(auto& inst: m_SphereInstances) {
        inst.mvpMatrix = vpMatrix * inst.mvpMatrix;
        inst.mvMatrix = vMatrix * inst.mvMatrix;
    }

    m_InstanceBuffer.setData(m_SphereInstances, GL_STREAM_DRAW);
    m_Sphere.draw(m_SphereInstances.size());

    for(auto& inst: m_ArrowInstances) {
        inst.mvpMatrix = scale(vpMatrix * inst.mvpMatrix, Vec3f(0.2, 1.f, 0.2));
        inst.mvMatrix = scale(vMatrix * inst.mvMatrix, Vec3f(0.2, 1.f, 0.2));
    }

    m_InstanceBuffer.setData(m_ArrowInstances, GL_STREAM_DRAW);
    m_Cylinder.draw(m_ArrowInstances.size());

    for(auto& inst: m_ArrowInstances) {
        inst.mvpMatrix = translate(scale(inst.mvpMatrix, Vec3f(4.f, 1.f, 4.f)), Vec3f(0, 1, 0));
        inst.mvMatrix = translate(scale(inst.mvMatrix, Vec3f(4.f, 1.f, 4.f)), Vec3f(0, 1, 0));
    }

    m_InstanceBuffer.setData(m_ArrowInstances, GL_STREAM_DRAW);
    m_Cone.draw(m_ArrowInstances.size());

    m_DrawLinesPass.m_Program.use();

    m_DrawLinesPass.uMVPMatrix.set(vpMatrix);

    m_DrawLinesPass.m_VBO.setData(m_DrawLinesPass.m_Vertices, GL_STREAM_DRAW);
    uint32_t offset = 0;
    m_DrawLinesPass.m_VAO.bind();
    for(const auto& instance: m_DrawLinesPass.m_Instances) {
        glLineWidth(instance.lineWidth);
        m_DrawLinesPass.uObjectID.set(instance.objectID);
        glDrawArrays(GL_LINES, offset, 2);
        offset += 2;
    }*/
}

Vec4u GLDebugRenderer::getObjectID(const Vec2u& pixel) {
    auto readFBO = pushGLState<GL_READ_FRAMEBUFFER_BINDING>();
    readFBO.set(m_FBO.glId());

    glReadBuffer(GL_COLOR_ATTACHMENT1);

    Vec4u value;
    glReadPixels(pixel.x, pixel.y, 1, 1, GL_RGBA_INTEGER, GL_UNSIGNED_INT, value_ptr(value));

    glReadBuffer(GL_NONE);

    return value;
}

}
