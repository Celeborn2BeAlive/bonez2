#pragma once

#include <bonez/types.hpp>
#include <bonez/sys/memory.hpp>
#include <bonez/scene/cameras/ProjectiveCamera.hpp>

#include "GLDebugRenderer.hpp"

namespace BnZ {

class GLDebugRenderer;

class GLDebugOutputStream {
    Shared<GLDebugRenderer> m_pRenderer;
    std::string m_sName;

    GLDebugOutputStream(GLDebugOutputStream&) = delete;
    GLDebugOutputStream& operator =(GLDebugOutputStream&) = delete;

    GLDebugOutputStream() = default;

    GLDebugOutputStream(const Shared<GLDebugRenderer>& renderer,
        const std::string& name) :
        m_pRenderer(renderer), m_sName(name) {
    }

public:
    friend GLDebugOutputStream newGLDebugStream(const Shared<GLDebugRenderer>& renderer, const std::string& name);

    GLDebugOutputStream(GLDebugOutputStream&& rvalue):
        m_pRenderer(rvalue.m_pRenderer), m_sName(rvalue.m_sName) {
        rvalue.m_pRenderer = nullptr;
    }

    GLDebugOutputStream& operator =(GLDebugOutputStream&& rvalue) {
        m_pRenderer = rvalue.m_pRenderer;
        m_sName = rvalue.m_sName;
        rvalue.m_pRenderer = nullptr;
        return *this;
    }

    ~GLDebugOutputStream();

    void clearObjects();

    void addSphere(const Vec3f& center, float radius,
                   const Vec3f& color, const Vec4u& objectID = zero<Vec4u>());

    void addArrow(const Vec3f& position, const Vec3f& direction, float length, float baseRadius,
                  const Vec3f& color, const Vec4u& objectID = zero<Vec4u>());

    void addLine(const Vec3f& org, const Vec3f& dst,
                  const Vec3f& orgColor, const Vec3f& dstColor, float lineWidth,
                  const Vec4u& objectID = zero<Vec4u>());
};

inline GLDebugOutputStream newGLDebugStream(const Shared<GLDebugRenderer>& renderer, const std::string& name) {
    renderer->addStream(name);
    return GLDebugOutputStream(renderer, name);
}

}
