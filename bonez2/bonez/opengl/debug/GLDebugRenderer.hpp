#pragma once

#include <bonez/types.hpp>
#include <bonez/scene/cameras/ProjectiveCamera.hpp>
#include <bonez/opengl/utils/GLutils.hpp>
#include <bonez/opengl/GLShaderManager.hpp>
#include <bonez/opengl/GLGBuffer.hpp>

namespace BnZ {

class GLDebugRenderer {
public:
    static Vec4u NULL_ID;

    GLDebugRenderer(const GLShaderManager& shaderManager);

    void setOutputColorTexture(GLTexture2D& outputColorTexture);

    //void clearObjects();

    void addSphere(const std::string& stream, const Vec3f& center, float radius,
                   const Vec3f& color, const Vec4u& objectID = NULL_ID);

    void addArrow(const std::string& stream, const Vec3f& position, const Vec3f& direction,
                  float length, float baseRadius,
                  const Vec3f& color, const Vec4u& objectID = NULL_ID);

    void addLine(const std::string& stream, const Vec3f& org, const Vec3f& dst,
                 const Vec3f& orgColor, const Vec3f& dstColor, float lineWidth,
                 const Vec4u& objectID = NULL_ID);

    void clearObjects(const std::string& stream);

    void render(const GLGBuffer& gBuffer, const ProjectiveCamera& camera);

    Vec4u getObjectID(const Vec2u& pixel);

    void addStream(const std::string& name) {
        if (end(m_Streams) != m_Streams.find(name)) {
            throw std::runtime_error("Stream " + name + " already exists.");
        }
        m_Streams.emplace(name, StreamData());
    }

    void deleteStream(const std::string& name) {
        m_Streams.erase(name);
    }

private:
    struct Vertex {
        Vec3f position;
        Vec3f normal;

        Vertex(Vec3f p, Vec3f n): position(p), normal(n) {
        }
    };

    struct Object {
        GLBuffer<Vertex> VBO;
        GLBuffer<uint16_t> IBO;
        GLVertexArray VAO;
        GLenum primType;

        Object();

        void setInstanceBuffer(GLuint instanceBuffer);

        void draw();

        void draw(uint32_t nbInstances);
    };

    Object m_Sphere;
    Object m_Disk;
    Object m_Circle;
    Object m_Cone;
    Object m_Cylinder;

    static void buildSphere(GLsizei discLat, GLsizei discLong, Object& model);

    static void buildDisk(GLsizei discBase, Object& model);

    static void buildCircle(GLsizei discBase, Object& model);

    static void buildCone(GLsizei discLat, GLsizei discHeight, Object& model);

    static void buildCylinder(GLsizei discLat, GLsizei discHeight, Object& model);

    struct ObjectInstance {
        Vec3f color;
        Mat4f mvpMatrix;
        Mat4f mvMatrix;
        Vec4u objectID;

        ObjectInstance(const Mat4f& modelMatrix, const Vec3f& color,
                       const Vec4u& objectID):
            color(color), mvpMatrix(modelMatrix), mvMatrix(modelMatrix), objectID(objectID) {
        }
    };

    /*
    std::vector<ObjectInstance> m_SphereInstances;
    std::vector<ObjectInstance> m_ArrowInstances;
*/
    GLBuffer<ObjectInstance> m_InstanceBuffer;

    GLProgram m_Program;

    struct LineInstance {
        float lineWidth;
        Vec4u objectID;

        LineInstance(float lineWidth, const Vec4u& objectID):
            lineWidth(lineWidth), objectID(objectID) {
        }
    };

    struct LineVertex {
        Vec3f position;
        Vec3f color;
        LineVertex(const Vec3f& pos, const Vec3f& col):
            position(pos), color(col) {
        }
    };

    struct StreamData {
        std::vector<ObjectInstance> m_SphereInstances;
        std::vector<ObjectInstance> m_ArrowInstances;

        GLBuffer<LineVertex> m_LineVBO;
        GLVertexArray m_LineVAO;
        std::vector<LineVertex> m_LineVertices;
        std::vector<LineInstance> m_LineInstances;

        StreamData();

        StreamData(StreamData&& rvalue) :
            m_SphereInstances(std::move(rvalue.m_SphereInstances)),
            m_ArrowInstances(std::move(rvalue.m_ArrowInstances)),
            m_LineVBO(std::move(rvalue.m_LineVBO)),
            m_LineVAO(std::move(rvalue.m_LineVAO)),
            m_LineVertices(std::move(rvalue.m_LineVertices)),
            m_LineInstances(std::move(rvalue.m_LineInstances)) {
        }

        StreamData& operator =(StreamData&& rvalue) {
            m_SphereInstances = std::move(rvalue.m_SphereInstances);
            m_ArrowInstances = std::move(rvalue.m_ArrowInstances);
            m_LineVBO = std::move(rvalue.m_LineVBO);
            m_LineVAO = std::move(rvalue.m_LineVAO);
            m_LineVertices = std::move(rvalue.m_LineVertices);
            m_LineInstances = std::move(rvalue.m_LineInstances);
            return *this;
        }

        void clearObjects() {
            m_SphereInstances.clear();
            m_ArrowInstances.clear();
            m_LineVertices.clear();
            m_LineInstances.clear();
        }

        void addSphere(const Vec3f& center, float radius,
                       const Vec3f& color, const Vec4u& objectID = zero<Vec4u>());

        void addArrow(const Vec3f& position, const Vec3f& direction, float length, float baseRadius,
                      const Vec3f& color, const Vec4u& objectID = zero<Vec4u>());

        void addLine(const Vec3f& org, const Vec3f& dst,
                      const Vec3f& orgColor, const Vec3f& dstColor, float lineWidth,
                      const Vec4u& objectID = zero<Vec4u>());
    };

    std::unordered_map<std::string, StreamData> m_Streams;

    struct DrawLinesPass {
        GLProgram m_Program;
        BNZ_GLUNIFORM(m_Program, Mat4f, uMVPMatrix);
        BNZ_GLUNIFORM(m_Program, Vec4u, uObjectID);

        /*
        GLBuffer<LineVertex> m_VBO;
        GLVertexArray m_VAO;
        std::vector<LineVertex> m_Vertices;
        std::vector<LineInstance> m_Instances;*/

        DrawLinesPass(const GLShaderManager& shaderManager);

        //void clear();
    };

    DrawLinesPass m_DrawLinesPass;

    GLFramebufferObject m_FBO;
    GLTexture2D m_ObjectIDTexture;
    GLTexture2D m_DepthTexture;
    GLenum m_DrawBuffers[2];
};

}
