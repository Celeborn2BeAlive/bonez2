#include "GLImageRenderer.hpp"

#include <bonez/image/Image.hpp>

namespace BnZ {

GLImageRenderer::GLImageRenderer(const GLShaderManager& shaderManager):
    m_Program(shaderManager.buildProgram({ "image.vs", "image.fs" })),
    m_DrawTexArrayProgram(shaderManager.buildProgram({ "image.vs", "imageArray.fs", "utils.fs" })) {
}

/*
void GLImageRenderer::drawImage(float gamma, const Image& image) {
    glDisable(GL_DEPTH_TEST);

    m_Program.use();

    GLTexture2D m_Texture;
    fillTexture(m_Texture, image);
    
    m_Texture.setMinFilter(GL_LINEAR);
    m_Texture.setMagFilter(GL_LINEAR);

    uGamma.set(gamma);
    uImage.set(m_Texture.makeTextureHandleResident());
    uDisplayAlphaChannel.set(false);
    uDivideByAlpha.set(false);
    uVerticalScale.set(-1.f);

    m_ScreenTriangle.render();
}*/

void GLImageRenderer::drawNormalTexture(const GLGBuffer& gBuffer) {
    glDisable(GL_DEPTH_TEST);

    m_Program.use();

    uGamma.set(1.f);
    uImage.set(gBuffer.getColorBuffer(GBUFFER_NORMAL_DEPTH).getTextureHandle());
    uDisplayAlphaChannel.set(false);
    uDivideByAlpha.set(false);
    uVerticalScale.set(1.f);

    m_ScreenTriangle.render();
}

void GLImageRenderer::drawDepthTexture(const GLGBuffer& gBuffer) {
    glDisable(GL_DEPTH_TEST);

    m_Program.use();

    uGamma.set(1.f);
    uImage.set(gBuffer.getColorBuffer(GBUFFER_NORMAL_DEPTH).getTextureHandle());
    uDisplayAlphaChannel.set(true);
    uDivideByAlpha.set(false);
    uVerticalScale.set(1.f);

    m_ScreenTriangle.render();
}

void GLImageRenderer::drawDiffuseTexture(const GLGBuffer& gBuffer) {
    glDisable(GL_DEPTH_TEST);

    m_Program.use();

    uGamma.set(1.f);
    uImage.set(gBuffer.getColorBuffer(GBUFFER_DIFFUSE).getTextureHandle());
    uDisplayAlphaChannel.set(false);
    uDivideByAlpha.set(false);
    uVerticalScale.set(1.f);

    m_ScreenTriangle.render();
}

void GLImageRenderer::drawGlossyTexture(const GLGBuffer& gBuffer) {
    glDisable(GL_DEPTH_TEST);

    m_Program.use();

    uGamma.set(1.f);
    uImage.set(gBuffer.getColorBuffer(GBUFFER_GLOSSY_SHININESS).getTextureHandle());
    uDisplayAlphaChannel.set(false);
    uDivideByAlpha.set(false);
    uVerticalScale.set(1.f);

    m_ScreenTriangle.render();
}

void GLImageRenderer::drawShininessTexture(const GLGBuffer& gBuffer) {
    glDisable(GL_DEPTH_TEST);

    m_Program.use();

    uGamma.set(1.f);
    uImage.set(gBuffer.getColorBuffer(GBUFFER_GLOSSY_SHININESS).getTextureHandle());
    uDisplayAlphaChannel.set(true);
    uDivideByAlpha.set(false);
    uVerticalScale.set(1.f);

    m_ScreenTriangle.render();
}

void GLImageRenderer::drawImage(float gamma, const Image& image) {
    glDisable(GL_DEPTH_TEST);

    m_Program.use();

    GLTexture2D m_Texture;
    fillTexture(m_Texture, image);

    m_Texture.setMinFilter(GL_LINEAR);
    m_Texture.setMagFilter(GL_LINEAR);

    uGamma.set(gamma);

    //glActiveTexture(GL_TEXTURE0);
    //m_Texture.bind();

    //auto u = m_Program.getUniformLocation("uImage");
    //glUniform1i(u, 0);
    uImage.set(m_Texture.makeTextureHandleResident());
    uDisplayAlphaChannel.set(false);
    uDivideByAlpha.set(true);
    uVerticalScale.set(1.f);

    m_ScreenTriangle.render();
}

void GLImageRenderer::drawFramebuffer(float gamma, const Framebuffer& framebuffer) {
    drawFramebuffer(gamma, framebuffer, 0);
}

void GLImageRenderer::drawFramebuffer(float gamma, const Framebuffer& framebuffer, uint32_t channelIdx) {
    drawImage(gamma, framebuffer.getChannel(channelIdx));
}

void GLImageRenderer::drawTexture(uint32_t index, const GLTexture2DArray& textureArray) {
    glDisable(GL_DEPTH_TEST);

    m_DrawTexArrayProgram.use();

    uLayer.set(index);
    uTextureArray.set(textureArray.getTextureHandle());
    uDrawDepth.set(false);

    m_ScreenTriangle.render();
}

void GLImageRenderer::drawDepthTexture(uint32_t index, const GLTexture2DArray& textureArray, float near, float far) {
    glDisable(GL_DEPTH_TEST);

    m_DrawTexArrayProgram.use();

    uLayer.set(index);
    uTextureArray.set(textureArray.getTextureHandle());
    uDrawDepth.set(true);
    uNear.set(near);
    uFar.set(far);

    m_ScreenTriangle.render();
}


}
