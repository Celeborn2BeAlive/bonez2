#version 430 core
#extension GL_NV_gpu_shader5: enable

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec2 aTexCoords;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCoords;
flat out int vInstanceID;

uniform mat4* uMVMatrixBuffer;

void main() {
    gl_Position = vec4(aPosition, 1);

    vPosition = (uMVMatrixBuffer[gl_InstanceID] * gl_Position).xyz;
    vNormal = aNormal;
    vTexCoords = aTexCoords;
    vInstanceID = gl_InstanceID;
}
