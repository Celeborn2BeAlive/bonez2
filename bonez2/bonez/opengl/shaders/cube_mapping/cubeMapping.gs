#version 430 core
#extension GL_NV_gpu_shader5: enable

layout(triangles) in;
layout(triangle_strip, max_vertices = 18) out;

in vec3 vPosition[];
in vec3 vNormal[];
in vec2 vTexCoords[];
flat in int vInstanceID[];

out vec3 gPosition;
out vec3 gNormal;
out vec2 gTexCoords;

uniform mat4* uMVPMatrixBuffer; // Contains 6 * number of layer matrices

void main() {
    for(int face = 0; face < 6; ++face) {
        int mvpMatrix = 6 * vInstanceID[0] + face;
        gl_Layer = mvpMatrix;
        gl_ViewportIndex = face;
        for(int i = 0; i < 3; ++i) {
            gl_Position = uMVPMatrixBuffer[mvpMatrix] * gl_in[i].gl_Position;

            gPosition = vPosition[i];
            gNormal = vNormal[i];
            gTexCoords = vTexCoords[i];

            EmitVertex();
        }
        EndPrimitive();
    }
}
