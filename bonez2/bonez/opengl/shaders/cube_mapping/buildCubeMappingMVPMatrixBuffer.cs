#version 430
#extension GL_NV_shader_buffer_load: enable
#extension GL_NV_gpu_shader5: enable

layout(local_size_x = 1024) in;

uniform mat4 uFaceProjMatrix[6];
uniform const mat4* uMVMatrixBuffer; // input
uniform uint uMVCount; // number of matrix to process

uniform mat4* uMVPMatrixBuffer; // output

void main() {
    uint threadID = gl_GlobalInvocationID.x;
    if(threadID >= uMVCount) {
        return;
    }
    uint offset = threadID * 6;
    for (uint f = 0u; f < 6u; ++f) {
        uMVPMatrixBuffer[offset] = uFaceProjMatrix[f] * uMVMatrixBuffer[threadID];
        ++offset;
    }
}
