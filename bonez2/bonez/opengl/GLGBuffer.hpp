#pragma once

#include "bonez/opengl/utils/GLFramebuffer.hpp"
#include "bonez/opengl/utils/GLUniform.hpp"

namespace BnZ {

enum GBufferTextureType {
    GBUFFER_NORMAL_DEPTH,
    GBUFFER_DIFFUSE,
    GBUFFER_GLOSSY_SHININESS,
    GBUFFER_TEXTURE_COUNT
};

struct GLGBuffer: public GLFramebuffer<GBUFFER_TEXTURE_COUNT> {
    void init(uint32_t width, uint32_t height) {
        GLenum internalFormats[] = { GL_RGBA32F, GL_RGBA32F, GL_RGBA32F };
        GLFramebuffer<GBUFFER_TEXTURE_COUNT>::init(width, height,
            internalFormats,
            GL_DEPTH_COMPONENT32F);
    }

    void init(const Vec2u& size) {
        return init(size.x, size.y);
    }

    void setShadingViewport() const {
        glViewport(0, 0, getWidth(), getHeight());
    }
};

struct GLGBufferUniform {
    GLUniform<GLTexture2DHandle> normalDepthSampler;
    GLUniform<GLTexture2DHandle> diffuseSampler;
    GLUniform<GLTexture2DHandle> glossyShininessSampler;

    GLGBufferUniform(const GLProgram& program):
        normalDepthSampler(program, "uGBuffer.normalDepthSampler"),
        diffuseSampler(program, "uGBuffer.diffuseSampler"),
        glossyShininessSampler(program, "uGBuffer.glossyShininessSampler") {
    }

    void set(const GLGBuffer& gbuffer) {
        normalDepthSampler.set(gbuffer.getColorBuffer(GBUFFER_NORMAL_DEPTH).getTextureHandle());
        diffuseSampler.set(gbuffer.getColorBuffer(GBUFFER_DIFFUSE).getTextureHandle());
        glossyShininessSampler.set(gbuffer.getColorBuffer(GBUFFER_GLOSSY_SHININESS).getTextureHandle());
    }
};

}
