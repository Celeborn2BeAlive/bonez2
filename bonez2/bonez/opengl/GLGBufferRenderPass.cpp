#include "GLGBufferRenderPass.hpp"

namespace BnZ {

GLGBufferRenderPass::GLGBufferRenderPass(const GLShaderManager& shaderManager):
    m_Program(shaderManager.buildProgram({ "geometryPass.vs", "geometryPass.fs" })){
}

void GLGBufferRenderPass::render(const Mat4f& projMatrix, const Mat4f& viewMatrix, float zFar,
            const GLScene& scene, GLGBuffer& gbuffer) {
    auto depthTest = pushGLState<GL_DEPTH_TEST>();
    depthTest.set(true);

    auto viewport = pushGLState<GL_VIEWPORT>();
    viewport.set(Vec4f(0, 0, gbuffer.getWidth(), gbuffer.getHeight()));

    auto fbo = pushGLState<GL_DRAW_FRAMEBUFFER_BINDING>();
    gbuffer.bindForDrawing();

    m_Program.use();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    Mat4f MVPMatrix = projMatrix * viewMatrix;
    Mat4f MVMatrix = viewMatrix;

    uMVPMatrix.set(MVPMatrix);
    uMVMatrix.set(MVMatrix);

    uZFar.set(zFar);

    scene.render(m_MaterialUniforms);
}

}
