#pragma once

#include <unordered_map>
#include <bonez/sys/FileSystem.hpp>

#include "utils/GLShader.hpp"
#include "utils/GLProgram.hpp"

namespace BnZ {

class GLShaderManager {
public:
    GLShaderManager();

    void addDirectory(const FilePath& dirPath);

    const GLShader& getShader(const std::string& name) const;

    GLProgram buildProgram(const std::vector<std::string>& shaders) const;

private:
    void recursiveCompileShaders(const FilePath& relativePath, const Directory& dir);

    std::unordered_map<std::string, GLenum> m_ExtToType;
    std::unordered_map<std::string, GLShader> m_ShadersMap;
};

}
