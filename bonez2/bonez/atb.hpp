#pragma once

#include "types.hpp"
#include <AntTweakBar/atb.hpp>

namespace atb {

ATB_GEN_TYPE_HELPER(BnZ::Vec3f, TW_TYPE_DIR3F);
ATB_GEN_TYPE_HELPER(BnZ::Col3f, TW_TYPE_COLOR3F);
ATB_GEN_TYPE_HELPER(BnZ::Col4f, TW_TYPE_COLOR4F);

}
