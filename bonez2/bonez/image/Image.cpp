#include "Image.hpp"

#include <iostream>
#include <fstream>
#include <unordered_map>
#include <FreeImage/FreeImage.h>

#include <bonez/maths/maths.hpp>

namespace BnZ {
    static bool s_bIsInit = false;

    static std::unordered_map<std::string, Shared<Image>> s_ImageCache;

    void initImageIO() {
        s_bIsInit = true;
        std::clog << "FreeImage " << FreeImage_GetVersion() << std::endl;
    }

    FIBITMAP* loadFreeImage(const char* lpszPathName) {
        FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
        // check the file signature and deduce its format
        // (the second argument is currently not used by FreeImage)
        fif = FreeImage_GetFileType(lpszPathName, 0);
        if (fif == FIF_UNKNOWN) {
            // no signature ?
            // try to guess the file format from the file extension
            fif = FreeImage_GetFIFFromFilename(lpszPathName);
        }
        // check that the plugin has reading capabilities ...
        if ((fif != FIF_UNKNOWN) && FreeImage_FIFSupportsReading(fif)) {
            // ok, let's load the file
            FIBITMAP *dib = FreeImage_Load(fif, lpszPathName, 0);
            // unless a bad file format, we are done !
            return dib;
        }
        return NULL;
    }

    Shared<Image> loadImage(const std::string& filepath, bool useCache) {
        if (!s_bIsInit) {
            throw std::runtime_error("IO API not initialized");
        }

        if (useCache) {
            auto it = s_ImageCache.find(filepath);
            if (it != end(s_ImageCache)) {
                return (*it).second;
            }
        }

        auto image = loadFreeImage(filepath.c_str());
        if (!image) {
            throw std::runtime_error("Unable to load");
        }

        unsigned width = FreeImage_GetWidth(image);
        unsigned height = FreeImage_GetHeight(image);

        Shared<Image> img = makeShared<Image>(width, height);

        auto ptr = img->getPixels();
        float scale = 1.f / 255.f;

        for (int j = height - 1; j >= 0; --j) {
            for (auto i = 0u; i < width; ++i) {
                RGBQUAD quad;
                FreeImage_GetPixelColor(image, i, j, &quad);
                (*ptr++) = scale * Vec4f(quad.rgbRed, quad.rgbGreen, quad.rgbBlue, 255.f);
            }
        }

        FreeImage_Unload(image);


        if (useCache) {
            s_ImageCache[filepath] = img;
        }

        return img;
    }

    void storeImage(const std::string& filepath, const Image& image) {
        if (!s_bIsInit) {
            throw std::runtime_error("IO API not initialized");
        }

        auto fif = FreeImage_GetFIFFromFilename(filepath.c_str());
        if (FIF_UNKNOWN == fif) {
            std::cerr << "storeImage: Format unrecognized" << std::endl;
            throw std::runtime_error("storeImage: Format unrecognized");
        }

        auto dib = FreeImage_Allocate(image.getWidth(), image.getHeight(), 32);
        float scale = 255.f;

        auto ptr = image.getPixels();
        for (int j = image.getHeight() - 1; j >= 0; --j) {
            for (auto i = 0u; i < image.getWidth(); ++i) {
                RGBQUAD quad;
                quad.rgbRed = min(255.f, ptr->r * scale);
                quad.rgbBlue = min(255.f, ptr->b * scale);
                quad.rgbGreen = min(255.f, ptr->g * scale);
                quad.rgbReserved = min(255.f, ptr->a * scale);

                FreeImage_SetPixelColor(dib, i, j, &quad);
                ++ptr;
            }
        }

        FreeImage_Save(fif, dib, filepath.c_str());

        FreeImage_Unload(dib);
    }

    Shared<Image> loadRawImage(const std::string& filepath, bool useCache) {
        std::ifstream in(filepath, std::ios_base::binary);
        if(!in) {
            throw std::runtime_error("Unable to open file " + filepath);
        }

        uint32_t w;
        uint32_t h;

        in.read((char*) &w, sizeof(w));
        in.read((char*) &h, sizeof(h));

        Shared<Image> img = makeShared<Image>(w, h);

        in.read((char*) img->getPixels(), sizeof(img->getPixels()[0]) * w * h);

        if (useCache) {
            s_ImageCache[filepath] = img;
        }

        return img;
    }

    void storeRawImage(const std::string& filepath, const Image& image) {
        std::ofstream out(filepath, std::ios_base::binary);

        uint32_t w = image.getWidth();
        uint32_t h = image.getHeight();

        out.write((const char*) &w, sizeof(w));
        out.write((const char*) &h, sizeof(h));
        out.write((const char*) image.getPixels(), sizeof(image.getPixels()[0]) * w * h);
    }

    Vec4f texture(const Image& image, const Vec2f& texCoords, ImageFilter filter) {
        Vec2f st = texCoords - floor(texCoords);
        auto bounds = Vec2i(image.getWidth() - 1, image.getHeight() - 1);
        Vec2i pixel = Vec2i(st * Vec2f(bounds));
        pixel = clamp(pixel, Vec2i(0), bounds);
        return image(pixel.x, pixel.y);
    }

    Vec3f computeRMSE(const Image& reference, const Image& framebuffer) {
        auto size = reference.getPixelCount();
        auto pRef = reference.getPixels();
        auto pFB = framebuffer.getPixels();
        Vec3f sum = zero<Vec3f>();
        for(auto i = 0u; i < size; ++i) {
            auto v1 = *pRef;
            auto v2 = *pFB;
            if(v1.a) v1 /= v1.a;
            if(v2.a) v2 /= v2.a;
            auto diff = v1.rgb() - v2.rgb();
            auto m = max(v1, v2);
            for(auto j = 0; j < 3; ++j) {
                if(m[j]) {
                    diff[j] /= m[j];
                }
            }
            sum += diff * diff;
            ++pRef;
            ++pFB;
        }
        return sqrt(sum / float(size));
    }

    Image compateRelativeSqrDiffImage(const Image& reference, const Image& framebuffer, Vec3f& rmse) {
        Image relativeSqrDiffImage(reference.getWidth(), reference.getHeight());
        auto size = reference.getPixelCount();
        auto pRef = reference.getPixels();
        auto pFB = framebuffer.getPixels();
        Vec3f sum = zero<Vec3f>();
        for(auto i = 0u; i < size; ++i) {
            auto v1 = *pRef;
            auto v2 = *pFB;
            if(v1.a) v1 /= v1.a;
            if(v2.a) v2 /= v2.a;
            auto diff = v1.rgb() - v2.rgb();
            auto m = max(v1, v2);
            for(auto j = 0; j < 3; ++j) {
                if(m[j]) {
                    diff[j] /= m[j];
                }
            }
            relativeSqrDiffImage[i] = Vec4f(diff * diff, 1.f);
            sum += diff * diff;
            ++pRef;
            ++pFB;
        }
        rmse = sqrt(sum / float(size));
        return relativeSqrDiffImage;
    }
}
