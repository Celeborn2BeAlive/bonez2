#pragma once

#include <bonez/viewer/Viewer.hpp>

#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/opengl/GLGBufferRenderPass.hpp>
#include <bonez/opengl/GLFlatShadingPass.hpp>
#include <bonez/opengl/GLScene.hpp>
#include <bonez/opengl/debug/GLDebugRenderer.hpp>
#include <bonez/opengl/debug/GLDebugOutputStream.hpp>

#include <bonez/maths/maths.hpp>

#include "SkelNodeDebugModule.hpp"

#include <bonez/opengl/cube_mapping/GLCubeMapContainer.hpp>
#include <bonez/opengl/cube_mapping/GLCubeMapRenderPass.hpp>

#include <bonez/opengl/lighting/GLPointLightsData.hpp>

#include <bonez/rendering/renderers/PathtraceRenderer.hpp>

#include <bonez/scene/cameras/ParaboloidCamera.hpp>
#include <bonez/scene/cameras/EnvironmentCamera.hpp>

#include <bonez/sampling/distribution2d.h>

#include <bonez/rendering/RenderModule.hpp>

#include "cpu_skel_mapping.hpp"

namespace BnZ {

class SceneViewer: public Viewer {
public:
    GLGBuffer m_GBuffer;
    Unique<GLScene> m_pGLScene;
    GLGBufferRenderPass m_GBufferRenderPass;
    GLFlatShadingPass m_FlatShadingPass;

    Shared<GLDebugRenderer> m_pGLDebugRenderer;
    GLImageRenderer m_GLImageRenderer;

    GLScreenTriangle m_ScreenTriangle;

    GLFramebuffer<1> m_GLFramebuffer;

    // On window viewports
    Vec4f m_FinalRenderViewport;
    Vec2f m_GBufferTexScreenSize;
    Vec4f m_GBufferViewport;
    Vec4f m_SideViewport;

    TwBar* m_pBar = nullptr;

    Vec4u m_SelectedObjectID = GLDebugRenderer::NULL_ID;
    Intersection m_PickedIntersection;
    Vec2f m_SelectedPixel = Vec2f(-1);

    SkelNodeDebugModule m_SkelNodeDebugModule;

    enum { TOPO_NONE, TOPO_CURVSKEL, TOPO_SEGCURVSKEL } m_TopologyDisplayMode = TOPO_SEGCURVSKEL;
    bool m_bHighlightSkeleton = false;

    std::string m_ScreenshotPrefix = toString(getMicroseconds());
    bool m_bDoScreenshot = false;

    bool m_bUseGridSkelMapping = false;
    uint32_t m_nSelectedNodeIdx = -1;

    SceneViewer(const char* settingsFilePath):
        Viewer("Skeleton Sampling", settingsFilePath),
        m_GBufferRenderPass(m_ShaderManager),
        m_FlatShadingPass(m_ShaderManager),
        m_pGLDebugRenderer(makeShared<GLDebugRenderer>(m_ShaderManager)),
        m_GLImageRenderer(m_ShaderManager),
        m_SkelNodeDebugModule(m_ShaderManager) {
    }

    const Scene& getScene() const {
        return *m_pScene;
    }

    void initGUI() {
        m_pBar = TwNewBar("Render Settings");

        atb::addButton(m_pBar, "Screenshot", [this]() {
            m_bDoScreenshot = true;
        });

        atb::addSeparator(m_pBar);

        // Scene rendering
        m_FlatShadingPass.exposeIO(m_pBar);

        atb::addSeparator(m_pBar);

        // CPU Rendering
        m_RenderModule.exposeIO(m_pBar);

        atb::addSeparator(m_pBar);

        // Skeleton display
        atb::addVarRW(m_pBar, "Topology display mode", "None,CurvSkel,SegmentedCurvSkel",
                      m_TopologyDisplayMode);
        atb::addVarRW(m_pBar, ATB_VAR(m_bHighlightSkeleton));

        atb::addSeparator(m_pBar);

        // Selected node
        m_SkelNodeDebugModule.exposeIO(m_pBar);

        atb::addSeparator(m_pBar);

        atb::addButton(m_pBar, "Add Point Light", [&]() {
            m_pScene->addLight(makeShared<PointLight>(m_SkelNodeDebugModule.m_Node.P, zero<Vec3f>()));
        });
        atb::addButton(m_pBar, "Add Diffuse Surface Point Light", [&]() {
            m_pScene->addLight(makeShared<DiffuseSurfacePointLight>(m_SkelNodeDebugModule.m_Node.P, Vec3f(0, 1, 0), zero<Vec3f>()));
        });

        atb::addSeparator(m_pBar);

        atb::addVarRW(m_pBar, ATB_VAR(m_SelectedPixel.x));
        atb::addVarRW(m_pBar, ATB_VAR(m_SelectedPixel.y));
    }

    void setUp() override {
        initGUI();

        m_GBuffer.init(m_Settings.m_FramebufferSize);
        m_pGLScene = makeUnique<GLScene>(m_pScene->getGeometry());

        GLenum format { GL_RGBA32F };
        m_GLFramebuffer.init(m_Settings.m_FramebufferSize,
                             &format, GL_DEPTH_COMPONENT32F);

        m_pGLDebugRenderer->setOutputColorTexture(m_GLFramebuffer.getColorBuffer(0));

        // Setup the viewports depending on the window size and the framebuffer size
        float W = 0.75 * m_Settings.m_WindowSize.x;

        m_GBufferTexScreenSize.x = W / 5;
        m_GBufferTexScreenSize.y = m_GBufferTexScreenSize.x / m_Settings.m_fFramebufferRatio;

        m_GBufferViewport = Vec4f(0, 0, W, m_GBufferTexScreenSize.y);

        Vec4f mainViewport(0, m_GBufferTexScreenSize.y, W, m_Settings.m_WindowSize.y - m_GBufferTexScreenSize.y);

        m_FinalRenderViewport =
                Vec4f(mainViewport.x + 0.5 * (mainViewport.z - m_Settings.m_FramebufferSize.x),
                          mainViewport.y + 0.5 * (mainViewport.w - m_Settings.m_FramebufferSize.y),
                          m_Settings.m_FramebufferSize);

        m_SideViewport = Vec4f(mainViewport.x + mainViewport.z,
                                   0.f,
                                   m_Settings.m_WindowSize.x - mainViewport.z,
                                   mainViewport.w);

        m_pSkel = m_pScene->getSegmentedCurvSkeleton();
        if(!m_pSkel) {
            throw std::runtime_error("No Segmented Skeleton attached to the scene");
        }

        addRenderers();

        exposeIO();
    }

    void tearDown() override {
    }

    void drawGBuffer() {
        glViewport(m_GBufferViewport.x, m_GBufferViewport.y,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);
        m_GLImageRenderer.drawNormalTexture(m_GBuffer);

        glViewport(m_GBufferViewport.x + m_GBufferTexScreenSize.x, m_GBufferViewport.y,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);
        m_GLImageRenderer.drawDepthTexture(m_GBuffer);

        glViewport(m_GBufferViewport.x + 2 * m_GBufferTexScreenSize.x, m_GBufferViewport.y,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);;
        m_GLImageRenderer.drawDiffuseTexture(m_GBuffer);

        glViewport(m_GBufferViewport.x + 3 * m_GBufferTexScreenSize.x, m_GBufferViewport.y,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);
        m_GLImageRenderer.drawGlossyTexture(m_GBuffer);

        glViewport(m_GBufferViewport.x + 4 * m_GBufferTexScreenSize.x, m_GBufferViewport.y,
                   m_GBufferTexScreenSize.x, m_GBufferTexScreenSize.y);
        m_GLImageRenderer.drawShininessTexture(m_GBuffer);
    }

    void drawFinalRender() {
        m_GLFramebuffer.bindForReading();
        m_GLFramebuffer.setReadBuffer(0);

        glBlitFramebuffer(0, 0, m_GBuffer.getWidth(), m_GBuffer.getHeight(),
                          m_FinalRenderViewport.x, m_FinalRenderViewport.y, m_FinalRenderViewport.x + m_FinalRenderViewport.z, m_FinalRenderViewport.y + m_FinalRenderViewport.w,
                          GL_COLOR_BUFFER_BIT, GL_NEAREST);

        glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
    }

    void drawCurvSkel(GLDebugOutputStream& stream) {
        if(m_TopologyDisplayMode == TOPO_NONE) {
            return;
        }

        const Skeleton* pSkel = nullptr;
        uint32_t objectTypeID = 0;

        if(m_TopologyDisplayMode == TOPO_CURVSKEL) {
            pSkel = m_pScene->getCurvSkeleton();
            objectTypeID = SKEL_NODE_ID;
        } else {
            pSkel = m_pScene->getSegmentedCurvSkeleton();
            objectTypeID = SEG_SKEL_NODE_ID;
        }

        if(pSkel) {
            float lineWidth = 2.f;
            std::function<Vec3f(uint32_t)> getNodeColor = [](uint32_t i) {
                return getColor(i);
            };
            float scale = 0.0005f;

            if(m_bHighlightSkeleton) {
                lineWidth = 5.f;
                getNodeColor = [](uint32_t i) { return Vec3f(1, 0, 0); };
                scale = 0.001f;
            }

            for(auto i = 0u; i < pSkel->size(); ++i) {
                auto node = pSkel->getNode(i);
                stream.addSphere(node.P, m_ZNearFar.y * scale,
                                 getNodeColor(i), Vec4u(objectTypeID, i, 0, 0));

                for(auto j: pSkel->neighbours(i)) {
                    auto neighbour = pSkel->getNode(j);
                    stream.addLine(node.P, neighbour.P,
                                   getNodeColor(i), getNodeColor(j), lineWidth);
                }
            }
        }
    }

    void setSelectedPixel(const Vec2u& pixel) {
        m_SelectedPixel = pixel;
    }

    float getArrowLength() {
        return m_ZNearFar.y * 0.005f;
    }

    void doPicking(GLDebugOutputStream& stream) {
        if(m_WindowManager.hasClicked()) {
            auto ndcPosition = m_WindowManager.getCursorNDC(m_FinalRenderViewport);
            if(viewportContains(ndcPosition, Vec4f(-1, -1, 2, 2))) {
                m_SelectedObjectID = m_pGLDebugRenderer->getObjectID(m_WindowManager.getCursorPosition(Vec2f(m_FinalRenderViewport)));

                if(m_SelectedObjectID == GLDebugRenderer::NULL_ID) {
                    m_PickedIntersection = m_pScene->intersect(m_Camera.getRay(ndcPosition));
                    if (m_PickedIntersection) {
                        onIntersectionPicked();
                    }
                } else {
                    m_SkelNodeDebugModule.setObject(m_SelectedObjectID, *m_pScene);
                    onObjectPicked();
                    m_PickedIntersection = Intersection();
                }
            }

            m_SelectedPixel = Vec2f(m_WindowManager.getCursorPosition());
        }

        if (m_PickedIntersection) {
            stream.addArrow(m_PickedIntersection.P, m_PickedIntersection.Ns, getArrowLength(), 0.3f * getArrowLength(), Vec3f(1.f));
        }
    }

    void drawSideView();

    void doScreenshot() {
        FilePath screenshotsPath = m_Path + "screenshots";
        createDirectory(screenshotsPath);

        FilePath baseName = m_ScreenshotPrefix + "screenshot." + toString(getMicroseconds());
        FilePath pngFile = screenshotsPath + baseName.addExt(".png");

        Image image;
        fillImage(image, m_GLFramebuffer.getColorBuffer(0));
        image.flipY();

        storeImage(pngFile, image);
    }

    void drawFrame() override ;

    void addRenderers();

    void exposeIO();

    void initGPUBuffers(GLQuery& timeQuery);

    void initCPUBuffers(float& time);

    const SegmentedCurvSkel* m_pSkel = nullptr;

    void preprocess();

    void render();

    void onIntersectionPicked();

    void onObjectPicked();

    Vec4i getNearestNodes(const SurfacePoint& point, Vec4f& weights) {
        if(m_bUseGridSkelMapping) {
            weights = Vec4i(1, 0, 0, 0);
            return Vec4i(m_pSkel->getNearestNode(point), -1, -1, -1);
        }

        return BnZ::getNearestNodes(*m_pSkel, point.P, point.Ns, weights,
                                    [this, point](uint32_t nodeIdx, const Vec3f& dir, float l) {
                                        return false;
                                    });
    }
};

}
