#pragma once

#include <bonez/types.hpp>
#include <bonez/common.hpp>
#include <bonez/scene/lights/EmissionVertex.hpp>
#include <bonez/scene/Intersection.hpp>
#include <bonez/scene/shading/BRDF.hpp>
#include <bonez/scene/Scene.hpp>
#include <bonez/scene/lights/EmissionSampler.hpp>
#include <bonez/sys/threads.hpp>

namespace BnZ {

struct PathVertex {
    Intersection lastVertex;
    BRDF lastVertexBRDF;
    Vec3f incidentDirection;
    float pdf; // pdf of the complete path
    float pdfLastVertex; // pdf of the last vertex, conditional to the previous
    Vec3f power;
    uint32_t depth; // number of edges of the path
    float recursiveMISFactor; // factor used to compute MIS Weights (balance heuristic)
};

float computeMISWeight(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       PathVertex* eyePathVertex, PathVertex* lightPathVertex, const EmissionVertex* lightNode);

float computeMISWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       PathVertex* eyePathVertex, PathVertex* lightPathVertex);

float computeMISWeight(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       PathVertex* eyePathVertex, const Scene& scene, const EmissionSampler& sampler);

float computeMISWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       PathVertex* eyePathVertex, const Scene& scene, const EmissionSampler& sampler);

float computeMISWeight(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       PathVertex* pEyePathVertex,
                       const EmissionVertex* lightNode,
                       const Vec3f& dirToLight);

float computeMISWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                PathVertex* eyePathVertex,
                                const EmissionVertex* lightNode,
                                const Vec3f& dirToLight);

template<typename FloatGenerator, typename Float2Generator>
Unique<EmissionVertex> sampleLightPath(
        const Scene& scene, const EmissionSampler& sampler,
        uint32_t threadID, uint32_t pixelID, PathVertex* pBuffer,
        uint32_t& lightPathLength,
        uint32_t maxLightPathDepth,
        FloatGenerator getFloat, Float2Generator getFloat2) {
    auto pEmissionVertex = sampler.sample(scene,
                                          getFloat(),
                                          getFloat(),
                                          getFloat2());

    if(!pEmissionVertex || !pEmissionVertex->pdf()) {
        return nullptr;
    }

    if(maxLightPathDepth == 0u) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    RaySample ray;
    auto s2D = getFloat2();
    auto Le_dir = pEmissionVertex->sampleWi(ray, s2D);

    if(!ray.pdf) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    auto I = scene.intersect(ray.value);

    if(!I) {
        lightPathLength = 0u;
        return pEmissionVertex;
    }

    auto power = pEmissionVertex->getRadiantExitance() * Le_dir / (pEmissionVertex->pdf() * ray.pdf);
    auto pdfLastVertex = pEmissionVertex->pdfWrtArea(I);
    auto pdf = pEmissionVertex->pdf() * pdfLastVertex;
    auto incidentDirection = -ray.value.dir;
    auto brdf = scene.shade(I);
    pBuffer[0].lastVertex = I;
    pBuffer[0].lastVertexBRDF = brdf;
    pBuffer[0].incidentDirection = incidentDirection;
    pBuffer[0].pdf = pdf;
    pBuffer[0].pdfLastVertex = pdfLastVertex;
    pBuffer[0].power = power;
    pBuffer[0].depth = 1u;
    pBuffer[0].recursiveMISFactor = pEmissionVertex->g(pBuffer[0].lastVertex) / pEmissionVertex->pdf();

    lightPathLength = 1u;

    for(auto length = 2u; length <= maxLightPathDepth; ++length) {
        auto idx = length - 1;

        Sample3f woSample;
        auto fr = brdf.sample(incidentDirection, woSample, getFloat2());
        if(woSample.pdf > 0.f && fr != zero<Vec3f>()) {
            auto Itmp = scene.intersect(Ray(I, woSample.value));

            if(Itmp) {
                incidentDirection = -woSample.value;
                auto sqrLength = sqr(Itmp.distance);
                pdfLastVertex = woSample.pdf  * max(0.f, dot(Itmp.Ns, incidentDirection)) / sqrLength;
                pdf *= pdfLastVertex;
                power *= max(0.f, dot(I.Ns, woSample.value)) * fr / woSample.pdf;
            } else {
                break;
            }

            if(pdfLastVertex == 0.f) {
                break;
            }

            I = Itmp;
            brdf = scene.shade(I);

            lightPathLength = length;
            pBuffer[idx].lastVertex = I;
            pBuffer[idx].lastVertexBRDF = brdf;
            pBuffer[idx].incidentDirection = incidentDirection;
            pBuffer[idx].pdf = pdf;
            pBuffer[idx].pdfLastVertex = pdfLastVertex;
            pBuffer[idx].power = power;
            pBuffer[idx].depth = length;

            auto g = max(0.f, dot(pBuffer[idx - 1].lastVertex.Ns, woSample.value)) / sqr(pBuffer[idx].lastVertex.distance);
            float reversePdf = pBuffer[idx - 1].lastVertexBRDF.pdf(woSample.value, pBuffer[idx - 1].incidentDirection);

            pBuffer[idx].recursiveMISFactor = g * (1 + pBuffer[idx - 1].recursiveMISFactor * reversePdf) / pBuffer[idx - 1].pdfLastVertex;

        } else {
            break;
        }
    }

    return pEmissionVertex;
}



}
