#pragma once

#include "SceneViewer.hpp"

#include <bonez/rendering/Renderer.hpp>
#include <bonez/sampling/multiple_importance_sampling.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/sampling/distribution1d.h>
#include <bonez/sampling/shapes.hpp>
#include <bonez/sampling/patterns.hpp>
#include <bonez/scene/lights/EmissionSampler.hpp>

#include <bonez/opengl/debug/GLDebugRenderer.hpp>
#include <bonez/opengl/debug/GLDebugOutputStream.hpp>

namespace BnZ {

class SkelBidirRenderer: public TileProcessingRenderer {
public:
    UpdateFlag m_Flag;
    uint32_t m_nMaxNodeCount = 4;
    bool m_bUseSkelGridMapping = false;
    bool m_bBiasedSkelSampling = true;
    float m_fPowerSkelFactor = 0.1f;
    uint32_t m_nMaxDepth = 3;
    bool m_bUseSkeleton = true;

    EmissionSampler m_EmissionSampler;

    struct MappingData {
        std::vector<Vec4i> nodes;
        std::vector<Vec4f> weights;

        void resizeBuffers(uint32_t size) {
            nodes.resize(size);
            weights.resize(size);
        }
    };

    MappingData m_MappingData;

    uint32_t m_nNodeLightPathCount = 1024;

    struct PathVertex {
        SurfacePoint lastVertex;
        BRDF lastVertexBRDF;
        Vec3f incidentDirection;
        float pdf; // pdf of the complete path wrt. area product
        float pdfLastVertex; // pdf of the last vertex, conditional to the previous
        Vec3f power;
        uint32_t length;
    };

    Unique<PathVertex[]> m_SkelNodeLightPathArray;
    Unique<float[]> m_SkelNodeDistributionsArray; // For each node, contains a discrete distribution among its light paths
    Unique<float[]> m_SkelNodePowerArray;
    uint32_t m_nDistributionSize;

    Unique<PathVertex[]> m_NoNodeLightPathArray;
    Unique<float[]> m_NoNodeDistributionsArray;

    Shared<GLDebugRenderer> m_pDebugRenderer;
    mutable GLDebugOutputStream m_DebugStream;

    SkelBidirRenderer(const Shared<GLDebugRenderer>& debugRenderer):
        m_pDebugRenderer(debugRenderer), m_DebugStream(newGLDebugStream(m_pDebugRenderer, "SkelBidirRenderer" + toString(getMicroseconds()))) {
    }

    virtual std::string getName() const {
        return "SkelBidirRenderer";
    }

    uint32_t getLightPathCountPerNode() const {
        return m_nNodeLightPathCount;
    }

    uint32_t getMaxLightPathDepth() const {
        return m_nMaxDepth - 2;
    }

    uint32_t getLightPathVertexPerNode() const {
        return getLightPathCountPerNode() * getMaxLightPathDepth();
    }

    void skeletonMapping(const SurfacePoint& I, uint32_t pixelIdx, Vec4i& nearestNodes, Vec4f& weights) const {
        if(m_bUseSkelGridMapping) {
            nearestNodes = Vec4i(getScene().getSegmentedCurvSkeleton()->getNearestNode(I), -1, -1, -1);
            weights = Vec4f(1, 0, 0, 0);
        } else {
            nearestNodes = m_MappingData.nodes[pixelIdx];
            weights = m_MappingData.weights[pixelIdx];
        }
    }

    // Sample a light path from a skeleton node
    bool sampleSkeletonNode(uint32_t threadID,
                            uint32_t pixelIdx,
                            uint32_t nodeIdx,
                            PathVertex** lightPath,
                            Sample1u& lightPathDiscreteSample) const {
        auto offset = nodeIdx * getLightPathVertexPerNode();
        auto distribOffset = nodeIdx * m_nDistributionSize;

        // Resampling of a point based on precomputed radiosities
        lightPathDiscreteSample = sampleDiscreteDistribution1D(m_SkelNodeDistributionsArray.get() + distribOffset,
                                                               getLightPathVertexPerNode(),
                                                               getFloat(threadID));
        if(lightPathDiscreteSample.pdf > 0.f) {
            *lightPath = &m_SkelNodeLightPathArray[offset + lightPathDiscreteSample.value];
            return true;
        }

        return false;
    }

    bool sampleSkeleton(uint32_t threadID,
                        uint32_t pixelIdx,
                        Vec4i pixelNodes,
                        Vec4f pixelNodeWeights,
                        PathVertex** lightPath,
                        Sample1u& lightPathDiscreteSample,
                        Sample1u& sampledNodeChannel,
                        int& sampledNodeIdx) const {
        Vec4f weights = pixelNodeWeights;
        sampledNodeChannel = sampleNodeFromWeights(pixelNodes,
                                                weights,
                                                m_nMaxNodeCount,
                                                getFloat(threadID));

        if(sampledNodeChannel.pdf > 0.f) {
            sampledNodeIdx = pixelNodes[sampledNodeChannel.value];

            if(sampledNodeIdx >= 0) {
                return sampleSkeletonNode(threadID, pixelIdx, sampledNodeIdx, lightPath, lightPathDiscreteSample);
            }
        }

        return false;
    }

    enum RenderTarget {
        FINAL_RENDER = 0,
        SKEL_CONTRIB = 1,
        BRDF_CONTRIB = 2,
        SKEL_WEIGHT = 3,
        BRDF_WEIGHT = 4,
        SKEL_COHERENCY = 5,
        VARIANCE = 6,
        SAMPLED_NODE = 7
    };

    float computeMISWeight(PathVertex* eyePathVertex, PathVertex* lightPathVertex) const {
        auto sum = 1.f;
        auto p = 1.f;

        auto eyePathLength = eyePathVertex->length;
        auto lightPathLength = lightPathVertex->length;

        PathVertex* currentVertex = eyePathVertex;
        PathVertex* nextVertex = lightPathVertex;
        auto incidentDir = currentVertex->incidentDirection;

        for(auto i = 0u; i < lightPathLength; ++i) {
            auto dir = nextVertex->lastVertex.P - currentVertex->lastVertex.P;
            auto l = length(dir);
            dir /= l;
            auto pdfWrtSolidAngle = currentVertex->lastVertexBRDF.pdf(incidentDir, dir);
            auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(nextVertex->lastVertex.Ns, -dir)) / sqr(l);

            p *= pdfWrtArea / nextVertex->pdfLastVertex;
            sum += p;
//            sum += 1;

            currentVertex = nextVertex;
            --nextVertex;
            incidentDir = -dir;
        }

        currentVertex = lightPathVertex;
        nextVertex = eyePathVertex;
        incidentDir = currentVertex->incidentDirection;

        for(auto i = 0u; i < eyePathLength - 1; ++i) {
            auto dir = nextVertex->lastVertex.P - currentVertex->lastVertex.P;
            auto l = length(dir);
            dir /= l;
            auto pdfWrtSolidAngle = currentVertex->lastVertexBRDF.pdf(incidentDir, dir);
            auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(nextVertex->lastVertex.Ns, -dir)) / sqr(l);

            p *= pdfWrtArea / nextVertex->pdfLastVertex;
            sum += p;
//            sum += 1;

            currentVertex = nextVertex;
            --nextVertex;
            incidentDir = -dir;
        }

        if(sum > 0.f) {
            return 1.f / sum;
        }

        return 0.f;
    }

    float computeMISWeight(PathVertex* eyePathVertex,
                           const EmissionVertex* lightNode,
                           const Vec3f& incidentDirection) const {
        auto sum = 1.f;
        auto p = 1.f;

        auto pdfWrtArea = lightNode->pdfWrtArea(eyePathVertex->lastVertex);
        p *= pdfWrtArea / eyePathVertex->pdfLastVertex;

        sum += p;
//        sum += 1;

        auto currentVertex = eyePathVertex;
        auto nextVertex = eyePathVertex - 1;
        auto incidentDir = incidentDirection;

        auto eyePathLength = eyePathVertex->length;

        for(auto i = 0u; i < eyePathLength - 2; ++i) {
            auto dir = nextVertex->lastVertex.P - currentVertex->lastVertex.P;
            auto l = length(dir);
            dir /= l;
            auto pdfWrtSolidAngle = currentVertex->lastVertexBRDF.pdf(incidentDir, dir);
            auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(nextVertex->lastVertex.Ns, -dir)) / sqr(l);

            p *= pdfWrtArea / nextVertex->pdfLastVertex;
            sum += p;
//            sum += 1;

            currentVertex = nextVertex;
            --nextVertex;
            incidentDir = -dir;
        }

        if(sum > 0.f) {
            return 1.f / sum;
        }

        return 0.f;
    }

    void Li(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx, Vec4f values[]) const {
        PathVertex eyePath[8];

        {
            auto I = getScene().intersect(primRay);

            if(!I) {
                values[FINAL_RENDER] += Vec4f(badColor(), 1.f);
                return;
            }

            eyePath[0].lastVertex = I;
            eyePath[0].lastVertexBRDF = getScene().shade(eyePath[0].lastVertex);
            eyePath[0].incidentDirection = -primRay.dir.xyz();
            eyePath[0].length = 1;
            eyePath[0].pdf = 1.f;
            eyePath[0].pdfLastVertex = 1.f;
            eyePath[0].power = Vec3f(1.f);
        }
        auto L = zero<Vec3f>();

        // trace an eye patheyePath[0].lastVertex
        for(auto i = 1u; i < m_nMaxDepth; ++i) {
            auto k = i - 1u; // index of vertex in eyePath;

            Vec4i nodes;
            Vec4f nodeWeights;

            if(m_bUseSkeleton) {
                if(i > 1u) {
                    nodes = Vec4i(getScene().getSegmentedCurvSkeleton()->getNearestNode(eyePath[k].lastVertex), -1, -1, -1);
                    nodeWeights = Vec4f(1, 0, 0, 0);
                } else {
    //                nodes = Vec4i(getScene().getSegmentedCurvSkeleton()->getNearestNode(eyePath[k].lastVertex), -1, -1, -1);
    //                nodeWeights = Vec4f(1, 0, 0, 0);
                    skeletonMapping(eyePath[k].lastVertex, pixelIdx, nodes, nodeWeights);
                }
            }

            if(!m_bUseSkeleton || nodes[0] < 0) {
                Sample1u lightPathDiscreteSample =
                        sampleDiscreteDistribution1D(m_NoNodeDistributionsArray.get(), getLightPathVertexPerNode(), getFloat(threadID));

                if(lightPathDiscreteSample.pdf > 0.f) {
                    PathVertex* lightPath = &m_NoNodeLightPathArray[lightPathDiscreteSample.value];
                    auto totalLength = i + lightPath->length + 1;

                    if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                        Vec3f incidentDirection;
                        float dist;
                        auto G = geometricFactor(eyePath[k].lastVertex, lightPath->lastVertex, incidentDirection, dist);
                        Ray incidentRay(eyePath[k].lastVertex, lightPath->lastVertex, incidentDirection, dist);

                        if(G > 0.f && !getScene().occluded(incidentRay)) {
                            auto weight = computeMISWeight(&eyePath[k], lightPath);

                            auto contrib = eyePath[k].power * lightPath->power * lightPath->lastVertexBRDF.eval(lightPath->incidentDirection, -incidentDirection)
                                    * G * eyePath[k].lastVertexBRDF.eval(incidentDirection, eyePath[k].incidentDirection) / lightPathDiscreteSample.pdf;

                            //weight = 1./ (totalLength - 2);
                            L += weight * contrib;
                            values[totalLength] += Vec4f(weight * contrib, 1.f);
                        } else {
                            values[totalLength] += Vec4f(Vec3f(0.f), 1.f);
                        }
                    }
                }
            } else {
                PathVertex* lightPath;
                Sample1u lightPathDiscreteSample;
                Sample1u sampledNodeChannel;
                int sampledNodeIdx;

                if(sampleSkeleton(threadID, pixelIdx, nodes, nodeWeights, &lightPath, lightPathDiscreteSample,
                                  sampledNodeChannel, sampledNodeIdx)) {
                    if(i == 1u) {
                        values[SAMPLED_NODE] += Vec4f(getColor(sampledNodeIdx), 1.f);
                    }

                    auto totalLength = i + lightPath->length + 1;

                    if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                        Vec3f incidentDirection;
                        float dist;
                        auto G = geometricFactor(eyePath[k].lastVertex, lightPath->lastVertex, incidentDirection, dist);
                        Ray incidentRay(eyePath[k].lastVertex, lightPath->lastVertex, incidentDirection, dist);

                        if(G > 0.f && !getScene().occluded(incidentRay)) {
                            auto weight = computeMISWeight(&eyePath[k], lightPath);

                            auto contrib = eyePath[k].power * lightPath->power * lightPath->lastVertexBRDF.eval(lightPath->incidentDirection, -incidentDirection)
                                    * G * eyePath[k].lastVertexBRDF.eval(incidentDirection, eyePath[k].incidentDirection) / lightPathDiscreteSample.pdf;
                            //weight = 1./ (totalLength - 2);
                            L += weight * contrib;
                            values[totalLength] += Vec4f(weight * contrib, 1.f);
                        } else {
                            values[totalLength] += Vec4f(Vec3f(0.f), 1.f);
                        }
                    }
                }
            }

            if(i >= 2u) {
                auto totalLength = i + 1;
                if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                    auto lightNode = m_EmissionSampler.sample(getScene(), getFloat(threadID),
                                                              getFloat(threadID), getFloat2(threadID));
                    Ray shadowRay;
                    auto G = lightNode->G(eyePath[k].lastVertex, shadowRay);
                    if(G > 0.f && !getScene().occluded(shadowRay)) {
                        auto power = lightNode->getRadiantExitance() / lightNode->pdf();
                        auto contrib = eyePath[k].power * power * lightNode->Le(-shadowRay.dir)
                                * G * eyePath[k].lastVertexBRDF.eval(shadowRay.dir, eyePath[k].incidentDirection);
                        auto weight = computeMISWeight(&eyePath[k], lightNode.get(), shadowRay.dir);
                        //weight = 1./ (totalLength - 2);
                        L += weight * contrib;
                        values[totalLength] += Vec4f(weight * contrib, 1.f);
                    } else {
                        values[totalLength] += Vec4f(Vec3f(0.f), 1.f);
                    }
                }
            }

            if(i < m_nMaxDepth - 1) {
                Sample3f wiSample;
                auto fr = eyePath[k].lastVertexBRDF.sample(eyePath[k].incidentDirection, wiSample, getFloat2(threadID));

                if(wiSample.pdf > 0.f) {
                    auto throughput = fr * max(0.f, dot(eyePath[k].lastVertex.Ns, wiSample.value)) / wiSample.pdf;
                    auto I = getScene().intersect(Ray(eyePath[k].lastVertex, wiSample.value));
                    if(!I) {
                        break;
                    }

                    auto dir = I.P - eyePath[k].lastVertex.P;
                    auto sqrLength = dot(dir, dir);

                    eyePath[k + 1].lastVertex = I;
                    eyePath[k + 1].lastVertexBRDF = getScene().shade(eyePath[k + 1].lastVertex);
                    eyePath[k + 1].incidentDirection = -wiSample.value;
                    eyePath[k + 1].length = eyePath[k].length + 1;
                    eyePath[k + 1].pdfLastVertex = wiSample.pdf * max(0.f, dot(I.Ns, -wiSample.value)) / sqrLength;
                    eyePath[k + 1].pdf = eyePath[k].pdf * eyePath[k + 1].pdfLastVertex;
                    eyePath[k + 1].power = eyePath[k].power * throughput;

                } else {
                    break;
                }
            }
        }

        values[FINAL_RENDER] += Vec4f(L, 1.f);
    }

    void sampleLightPaths(uint32_t threadID, PathVertex* pBuffer) const {
        auto powerScale = 1.f / getLightPathCountPerNode();
        auto maxLightPathDepth = getMaxLightPathDepth();

        for(auto k = 0u; k < getLightPathCountPerNode(); ++k) {
            auto pathOffset = k * maxLightPathDepth;

            auto pEmissionVertex = m_EmissionSampler.sample(getScene(),
                                                            getFloat(threadID),
                                                            getFloat(threadID),
                                                            getFloat2(threadID));

            RaySample ray;
            auto Le_dir = pEmissionVertex->sampleWi(ray, getFloat2(threadID));
            auto I = getScene().intersect(ray.value);

            auto power = powerScale * pEmissionVertex->getRadiantExitance() * Le_dir / (pEmissionVertex->pdf() * ray.pdf);
            auto pdfLastVertex = pEmissionVertex->pdfWrtArea(I);
            auto pdf = pEmissionVertex->pdf() * pdfLastVertex;
            auto incidentDirection = -ray.value.dir;

            for(auto length = 1u; length <= maxLightPathDepth; ++length) {
                auto idx = pathOffset + length - 1;
                if(I) {
                    auto brdf = getScene().shade(I);
                    pBuffer[idx].lastVertex = I;
                    pBuffer[idx].lastVertexBRDF = brdf;
                    pBuffer[idx].incidentDirection = incidentDirection;
                    pBuffer[idx].pdf = pdf;
                    pBuffer[idx].pdfLastVertex = pdfLastVertex;
                    pBuffer[idx].power = power;
                    pBuffer[idx].length = length;

                    if(length < maxLightPathDepth) {
                        Sample3f woSample;
                        auto fr = brdf.sample(incidentDirection, woSample, getFloat2(threadID));
                        if(woSample.pdf > 0.f && fr != zero<Vec3f>()) {
                            auto Itmp = getScene().intersect(Ray(I, woSample.value));
                            if(Itmp) {
                                incidentDirection = -woSample.value;
                                auto dir = Itmp.P - I.P;
                                auto sqrLength = dot(dir, dir);
                                pdfLastVertex = woSample.pdf  * max(0.f, dot(Itmp.Ns, incidentDirection)) / sqrLength;
                                pdf *= pdfLastVertex;
                                power *= max(0.f, dot(I.Ns, woSample.value)) * fr / woSample.pdf;
                            }
                            I = Itmp;
                        }
                    }
                } else {
                    pBuffer[idx].pdf = 0.f;
                    pBuffer[idx].pdfLastVertex = 0.f;
                    pBuffer[idx].power = Vec3f(0.f);
                    pBuffer[idx].length = length;
                }
            }
        }
    }

    void computeSkelNodeIntersections() {
        auto maxLightPathDepth = getMaxLightPathDepth();
        auto nbVertexPerNode = getLightPathVertexPerNode();
        auto pSkel = getScene().getSegmentedCurvSkeleton();
        auto completeSize = nbVertexPerNode * pSkel->size();

        if(!m_SkelNodeLightPathArray) {
            m_SkelNodeLightPathArray = makeUniqueArray<PathVertex>(completeSize);
            m_nDistributionSize = getDistribution1DBufferSize(nbVertexPerNode);
            m_SkelNodeDistributionsArray = makeUniqueArray<float>(m_nDistributionSize * pSkel->size());
            m_SkelNodePowerArray = makeUniqueArray<float>(pSkel->size());
            m_NoNodeLightPathArray = makeUniqueArray<PathVertex>(nbVertexPerNode);
            m_NoNodeDistributionsArray = makeUniqueArray<float>(m_nDistributionSize);
        }

        sampleLightPaths(0u, m_NoNodeLightPathArray.get());
        buildDistribution1D([this](uint32_t i) {
            return luminance(m_NoNodeLightPathArray[i].lastVertexBRDF.diffuseTerm() * m_NoNodeLightPathArray[i].power);
        }, m_NoNodeDistributionsArray.get(), nbVertexPerNode);

        if(m_bUseSkeleton) {
            // For each node, compute intersections, irradiance values and distributions
            launchThreads([pSkel, nbVertexPerNode, maxLightPathDepth, this](uint32_t threadID) {
                uint32_t loopID = 0u;

                while(true) {
                    auto nodeIndex = loopID * getThreadCount() + threadID;
                    loopID++;

                    if(nodeIndex >= pSkel->size()) {
                        break;
                    }

                    auto nodePos = pSkel->getNode(nodeIndex).P;
                    auto nodeRadius = pSkel->getNode(nodeIndex).maxball;
                    auto offset = nodeIndex * nbVertexPerNode;
                    auto distribOffset = nodeIndex * m_nDistributionSize;

                    sampleLightPaths(threadID, m_SkelNodeLightPathArray.get() + offset);

                    m_SkelNodePowerArray[nodeIndex] = 0.f;
                    for(auto i = 0u; i < nbVertexPerNode; ++i) {
                        if(m_bBiasedSkelSampling) {
                            // If biased sampling, ignore non visible light path vertices
                            auto& I = m_SkelNodeLightPathArray[offset + i].lastVertex;
                            auto dir = nodePos - I.P;
                            auto l = length(dir);
                            dir /= l;
                            if(dot(I.Ns, dir) <= 0.f || getScene().occluded(Ray(I, dir, l))) {
                                m_SkelNodeLightPathArray[offset + i].pdf = 0.f;
                                m_SkelNodeLightPathArray[offset + i].pdfLastVertex = 0.f;
                                m_SkelNodeLightPathArray[offset + i].power = Vec3f(0.f);
                            }
                        }

                        m_SkelNodePowerArray[nodeIndex] += luminance(m_SkelNodeLightPathArray[offset + i].power);
                    }

                    m_SkelNodePowerArray[nodeIndex] /= float(getLightPathCountPerNode());

                    buildDistribution1D([this, offset, nodePos, nodeRadius, nodeIndex](uint32_t i) {
                        auto& I = m_SkelNodeLightPathArray[offset + i].lastVertex;
                        auto dir = nodePos - I.P;
                        auto l = length(dir);
                        dir /= l;

                        if(!m_bBiasedSkelSampling) {
                            // If unbiased sampling, give less sampling probability to non-visible points
                            if(dot(I.Ns, dir) <= 0.f || getScene().occluded(Ray(I, dir, l))) {
                                return sqr(nodeRadius) * (max(0.f, dot(I.Ns, dir)) / sqr(l)) * m_fPowerSkelFactor *
                                        luminance(m_SkelNodeLightPathArray[offset + i].lastVertexBRDF.diffuseTerm() *
                                            m_SkelNodeLightPathArray[offset + i].power);
                            }
                        }
                        return sqr(nodeRadius) * (1.f / sqr(l)) *
                                luminance(m_SkelNodeLightPathArray[offset + i].lastVertexBRDF.diffuseTerm() *
                                    m_SkelNodeLightPathArray[offset + i].power);
                    }, m_SkelNodeDistributionsArray.get() + distribOffset, nbVertexPerNode);
                }
            });
        }
    }

    void computeSkeletonMapping() {
        auto framebufferSize = getFramebuffer().getSize();
        m_MappingData.resizeBuffers(framebufferSize.x * framebufferSize.y);

        Vec2u tileCount = framebufferSize / getTileSize() +
                Vec2u(framebufferSize % getTileSize() != zero<Vec2u>());

        auto totalCount = tileCount.x * tileCount.y;

        auto pSkel = getScene().getSegmentedCurvSkeleton();

        auto task = [&](uint32_t threadID) {
            auto loopID = 0u;
            while(true) {
                auto tileID = loopID * getThreadCount() + threadID;
                ++loopID;

                if(tileID >= totalCount) {
                    return;
                }

                uint32_t tileX = tileID % tileCount.x;
                uint32_t tileY = tileID / tileCount.x;

                Vec2u tileOrg = Vec2u(tileX, tileY) * getTileSize();
                auto viewport = Vec4u(tileOrg, getTileSize());

                if(viewport.x + viewport.z > framebufferSize.x) {
                    viewport.z = framebufferSize.x - viewport.x;
                }

                if(viewport.y + viewport.w > framebufferSize.y) {
                    viewport.w = framebufferSize.y - viewport.y;
                }

                auto xEnd = viewport.x + viewport.z;
                auto yEnd = viewport.y + viewport.w;

                for(auto y = viewport.y; y < yEnd; ++y) {
                    for(auto x = viewport.x; x < xEnd; ++x) {
                        auto pixelIdx = getPixelIndex(x, y);
                        auto s2D = Vec2f(0.5f, 0.5f);
                        auto ray = getPrimaryRay(x, y, s2D);
                        auto I = getScene().intersect(ray);
                        if(I) {
                            m_MappingData.nodes[pixelIdx] = getNearestNodes(*pSkel, I.P, I.Ns, m_MappingData.weights[pixelIdx],
                                                [](uint32_t nodeIdx, Vec3f dir, float dist) { return false; });
                        } else {
                            m_MappingData.nodes[pixelIdx] = Vec4i(-1);
                        }
                    }
                }
            }
        };

        launchThreads(task);
    }

    void preprocess() override {
        m_EmissionSampler.initFrame(getScene());

        if(m_bUseSkeleton && !m_bUseSkelGridMapping) {
            computeSkeletonMapping();
        }
    }

    void beginFrame() override {
        computeSkelNodeIntersections();
    }

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override {
        auto spp = getSppCount();
        TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
            Vec4f L[8];
            std::fill(L, L + 8, Vec4f(0.f));
            auto pixelIdx = getPixelIndex(x, y);
            for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
                auto s2D = getPixelSample(threadID, sampleID);
                auto ray = getPrimaryRay(x, y, s2D);
                Li(ray, threadID, pixelIdx, L);
            }
            for(auto i = 0u; i < 8; ++i) {
                getFramebuffer().accumulate(i, pixelIdx, L[i]);
            }
        });
    }

    virtual void doExposeIO(TwBar* bar) {
        atb::addVarRW(bar, "Max node count", m_nMaxNodeCount);
        atb::addVarRW(bar, ATB_VAR(m_bUseSkelGridMapping));
        atb::addVarRW(bar, ATB_VAR(m_nNodeLightPathCount));
        atb::addVarRW(bar, ATB_VAR(m_bBiasedSkelSampling));
        atb::addVarRW(bar, ATB_VAR(m_fPowerSkelFactor));
        atb::addVarRW(bar, ATB_VAR(m_nMaxDepth));
        atb::addVarRW(bar, ATB_VAR(m_bUseSkeleton));
        atb::addButton(bar, "clear", [this]() {
           m_DebugStream.clearObjects();
        });
    }

    virtual void doLoadSettings(const tinyxml2::XMLElement& xml) {
        getAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
        getAttribute(xml, "useGridSkelMapping", m_bUseSkelGridMapping);
        getAttribute(xml, "nodeLightPathCount", m_nNodeLightPathCount);
        getAttribute(xml, "biasedSkelSampling", m_bBiasedSkelSampling);
        getAttribute(xml, "powerSkelFactor", m_fPowerSkelFactor);
        getAttribute(xml, "maxDepth", m_nMaxDepth);
        getAttribute(xml, "useSkeleton", m_bUseSkeleton);
    }

    virtual void doStoreSettings(tinyxml2::XMLElement& xml) const {
        setAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
        setAttribute(xml, "useGridSkelMapping", m_bUseSkelGridMapping);
        setAttribute(xml, "nodeLightPathCount", m_nNodeLightPathCount);
        setAttribute(xml, "biasedSkelSampling", m_bBiasedSkelSampling);
        setAttribute(xml, "powerSkelFactor", m_fPowerSkelFactor);
        setAttribute(xml, "maxDepth", m_nMaxDepth);
        setAttribute(xml, "useSkeleton", m_bUseSkeleton);
    }
};

}
