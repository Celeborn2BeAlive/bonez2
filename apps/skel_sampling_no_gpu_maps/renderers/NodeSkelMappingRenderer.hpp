#pragma once

#include <bonez/rendering/Renderer.hpp>

namespace BnZ {

class NodeSkelMappingRenderer: public TileProcessingRenderer {
public:
    using GetNearestNodeFunction = std::function<Vec4i (const SurfacePoint& point, Vec4f& weights)>;
    GetNearestNodeFunction m_GetNearestNode;

    NodeSkelMappingRenderer(const GetNearestNodeFunction& getNearestNode):
        m_GetNearestNode(getNearestNode) {
    }

    virtual std::string getName() const {
        return "NodeSkelMappingRenderer";
    }

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override {
        auto spp = getSppCount();
        TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
            auto pixelIdx = getPixelIndex(x, y);
            Vec4f L(0.f);
            for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
                auto s2D = getPixelSample(threadID, sampleID);
                auto ray = getPrimaryRay(x, y, s2D);
                auto I = getScene().intersect(ray);

                Vec4f weights;
                auto nodes = m_GetNearestNode(I, weights);
                L += Vec4f(getColor(nodes[0]), 1.f);
            }
            getFramebuffer().accumulate(pixelIdx, L);
        });
    }
};

}
