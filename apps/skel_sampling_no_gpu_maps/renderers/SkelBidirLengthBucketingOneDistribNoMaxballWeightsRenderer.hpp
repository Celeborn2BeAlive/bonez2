#pragma once

#include "SceneViewer.hpp"

#include <bonez/rendering/Renderer.hpp>
#include <bonez/sampling/multiple_importance_sampling.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/sampling/distribution1d.h>
#include <bonez/sampling/shapes.hpp>
#include <bonez/sampling/patterns.hpp>
#include <bonez/scene/lights/EmissionSampler.hpp>

#include <bonez/opengl/debug/GLDebugRenderer.hpp>
#include <bonez/opengl/debug/GLDebugOutputStream.hpp>

#include "bdpt.hpp"

namespace BnZ {

class SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer: public TileProcessingRenderer {
    UpdateFlag m_Flag;
    uint32_t m_nMaxNodeCount = 4;
    bool m_bUseSkelGridMapping = false;
    float m_fPowerSkelFactor = 0.1f;
    uint32_t m_nMaxDepth = 3;
    bool m_bUseSkeleton = true;

    float m_fBeginTileTime = 0.f;

    EmissionSampler m_EmissionSampler;

    struct MappingData {
        std::vector<Vec4i> nodes;
        std::vector<Vec4f> weights;

        void resizeBuffers(uint32_t size) {
            nodes.resize(size);
            weights.resize(size);
        }
    };

    MappingData m_MappingData;

    uint32_t m_nNodeLightPathCount = 1024;

    Unique<float[]> m_SkelNodeDistributionsArray; // For each node, contains a discrete distribution among its light paths
    uint32_t m_nDistributionSize;

    Unique<PathVertex[]> m_NoNodeLightPathArray;
    Unique<float[]> m_NoNodeDistributionsArray;

    Shared<GLDebugRenderer> m_pDebugRenderer;
    mutable GLDebugOutputStream m_DebugStream;

    enum RenderTarget {
        FINAL_RENDER = 0,
        SKEL_CONTRIB = 1,
        BRDF_CONTRIB = 2,
        SKEL_WEIGHT = 3,
        BRDF_WEIGHT = 4,
        SKEL_COHERENCY = 5,
        VARIANCE = 6,
        SAMPLED_NODE = 7
    };

public:
    SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer(const Shared<GLDebugRenderer>& debugRenderer);

    std::string getName() const override;

    uint32_t getLightPathCountPerNode() const;

    uint32_t getMaxLightPathDepth() const;

    uint32_t getLightPathVertexPerNode() const;

    void skeletonMapping(const SurfacePoint& I, uint32_t pixelIdx, Vec4i& nearestNodes, Vec4f& weights) const;

    // Sample a light path from a skeleton node
    bool sampleSkeletonNode(uint32_t threadID,
                            uint32_t pixelIdx,
                            uint32_t nodeIdx,
                            uint32_t lightPathLength,
                            PathVertex** lightPath,
                            Sample1u& lightPathDiscreteSample) const;

    void Li(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx, uint32_t sampleID, Vec4f values[]) const;

    void sampleLightPaths(uint32_t threadID, PathVertex* pBuffer) const;

    void computeSkelNodeLightPathsDistributions();

    void computeSkeletonMapping();

    void preprocess() override;

    void beginFrame() override;

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override;

    virtual void doExposeIO(TwBar* bar);

    virtual void doLoadSettings(const tinyxml2::XMLElement& xml);

    virtual void doStoreSettings(tinyxml2::XMLElement& xml) const;

    virtual void doStoreStatistics() const override;
};

}
