#pragma once

#include "SceneViewer.hpp"

#include <bonez/rendering/Renderer.hpp>
#include <bonez/sampling/multiple_importance_sampling.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/sampling/distribution1d.h>
#include <bonez/sampling/shapes.hpp>
#include <bonez/sampling/patterns.hpp>
#include <bonez/scene/lights/EmissionSampler.hpp>

#include <bonez/opengl/debug/GLDebugRenderer.hpp>
#include <bonez/opengl/debug/GLDebugOutputStream.hpp>

namespace BnZ {

class SkelBidirLengthBucketingNoAlphaRenderer: public TileProcessingRenderer {
    UpdateFlag m_Flag;
    uint32_t m_nMaxNodeCount = 4;
    bool m_bUseSkelGridMapping = false;
    uint32_t m_nMaxDepth = 3;
    bool m_bUseSkeleton = true;

    EmissionSampler m_EmissionSampler;

    struct MappingData {
        std::vector<Vec4i> nodes;
        std::vector<Vec4f> weights;

        void resizeBuffers(uint32_t size) {
            nodes.resize(size);
            weights.resize(size);
        }
    };

    MappingData m_MappingData;

    uint32_t m_nNodeLightPathCount = 1024;

    struct PathVertex {
        Intersection lastVertex;
        BRDF lastVertexBRDF;
        Vec3f incidentDirection;
        float pdf; // pdf of the complete path wrt. area product
        float pdfLastVertex; // pdf of the last vertex, conditional to the previous
        Vec3f power;
        uint32_t length;
    };

    Unique<PathVertex[]> m_SkelNodeLightPathArray;
    Unique<float[]> m_SkelNodeDistributionsArray; // For each node, contains a discrete distribution among its light paths
    uint32_t m_nDistributionSize;

    Unique<PathVertex[]> m_NoNodeLightPathArray;
    Unique<float[]> m_NoNodeDistributionsArray;

    Shared<GLDebugRenderer> m_pDebugRenderer;
    mutable GLDebugOutputStream m_DebugStream;

    enum RenderTarget {
        FINAL_RENDER = 0,
        SKEL_CONTRIB = 1,
        BRDF_CONTRIB = 2,
        SKEL_WEIGHT = 3,
        BRDF_WEIGHT = 4,
        SKEL_COHERENCY = 5,
        VARIANCE = 6,
        SAMPLED_NODE = 7
    };

public:
    SkelBidirLengthBucketingNoAlphaRenderer(const Shared<GLDebugRenderer>& debugRenderer);

    std::string getName() const override;

    uint32_t getLightPathCountPerNode() const;

    uint32_t getMaxLightPathDepth() const;

    uint32_t getLightPathVertexPerNode() const;

    void skeletonMapping(const SurfacePoint& I, uint32_t pixelIdx, Vec4i& nearestNodes, Vec4f& weights) const;

    // Sample a light path from a skeleton node
    bool sampleSkeletonNode(uint32_t threadID,
                            uint32_t pixelIdx,
                            uint32_t nodeIdx,
                            uint32_t lightPathLength,
                            PathVertex** lightPath,
                            Sample1u& lightPathDiscreteSample,
                            bool visible) const;

    bool sampleSkeleton(uint32_t threadID,
                        uint32_t pixelIdx,
                        Vec4i pixelNodes,
                        Vec4f pixelNodeWeights,
                        uint32_t lightPathLength,
                        PathVertex** lightPath,
                        Sample1u& lightPathDiscreteSample,
                        Sample1u& sampledNodeChannel,
                        int& sampledNodeIdx,
                        bool visible) const;

    float computeMISWeight(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                           PathVertex* eyePathVertex, PathVertex* lightPathVertex) const;

    float computeMISWeight(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                           PathVertex* pEyePathVertex,
                           const EmissionVertex* lightNode,
                           const Vec3f& dirToLight) const;

    void Li(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx, uint32_t sampleID, Vec4f values[]) const;

    void sampleLightPaths(uint32_t threadID, PathVertex* pBuffer) const;

    void computeSkelNodeLightPathsDistributions();

    void computeSkeletonMapping();

    void preprocess() override;

    void beginFrame() override;

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override;

    virtual void doExposeIO(TwBar* bar);

    virtual void doLoadSettings(const tinyxml2::XMLElement& xml);

    virtual void doStoreSettings(tinyxml2::XMLElement& xml) const;
};

}
