#include "SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer.hpp"

namespace BnZ {

SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer(const Shared<GLDebugRenderer>& debugRenderer):
    m_pDebugRenderer(debugRenderer), m_DebugStream(newGLDebugStream(m_pDebugRenderer, "SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer" + toString(getMicroseconds()))) {
}

std::string SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::getName() const {
    return "SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer";
}

uint32_t SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::getLightPathCountPerNode() const {
    return m_nNodeLightPathCount;
}

uint32_t SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::getMaxLightPathDepth() const {
    return m_nMaxDepth - 2;
}

uint32_t SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::getLightPathVertexPerNode() const {
    return getLightPathCountPerNode() * getMaxLightPathDepth();
}

void SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::skeletonMapping(const SurfacePoint& I, uint32_t pixelIdx, Vec4i& nearestNodes, Vec4f& weights) const {
    if(m_bUseSkelGridMapping) {
        nearestNodes = Vec4i(getScene().getSegmentedCurvSkeleton()->getNearestNode(I), -1, -1, -1);
        weights = Vec4f(1, 0, 0, 0);
    } else {
        nearestNodes = m_MappingData.nodes[pixelIdx];
        weights = m_MappingData.weights[pixelIdx];
    }
}

// Sample a light path from a skeleton node
bool SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::sampleSkeletonNode(uint32_t threadID,
                        uint32_t pixelIdx,
                        uint32_t nodeIdx,
                        uint32_t lightPathLength,
                        PathVertex** lightPath,
                        Sample1u& lightPathDiscreteSample,
                        bool visible) const {
    auto distribOffset = nodeIdx * 2 * m_nDistributionSize * getMaxLightPathDepth() + (lightPathLength - 1) * 2 * m_nDistributionSize + int(visible) * m_nDistributionSize;
    auto firstPathOffset = lightPathLength - 1;

    // Resampling of a point based on precomputed radiosities
    lightPathDiscreteSample = sampleDiscreteDistribution1D(m_SkelNodeDistributionsArray.get() + distribOffset,
                                                           getLightPathCountPerNode(),
                                                           getFloat(threadID));

    if(lightPathDiscreteSample.pdf > 0.f) {
        *lightPath = &m_NoNodeLightPathArray[firstPathOffset + lightPathDiscreteSample.value * getMaxLightPathDepth()];
        return true;
    }

    return false;
}

bool SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::sampleSkeleton(uint32_t threadID,
                    uint32_t pixelIdx,
                    Vec4i pixelNodes,
                    Vec4f pixelNodeWeights,
                    uint32_t lightPathLength,
                    PathVertex** lightPath,
                    Sample1u& lightPathDiscreteSample,
                    Sample1u& sampledNodeChannel,
                    int& sampledNodeIdx,
                    bool visible) const {
    Vec4f weights = pixelNodeWeights;
    sampledNodeChannel = sampleNodeFromWeights(pixelNodes,
                                            weights,
                                            m_nMaxNodeCount,
                                            getFloat(threadID));

    if(sampledNodeChannel.pdf > 0.f) {
        sampledNodeIdx = pixelNodes[sampledNodeChannel.value];

        if(sampledNodeIdx >= 0) {
            return sampleSkeletonNode(threadID, pixelIdx, sampledNodeIdx, lightPathLength, lightPath, lightPathDiscreteSample, visible);
        }
    }

    return false;
}

float SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::computeMISWeight(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       PathVertex* eyePathVertex, PathVertex* lightPathVertex) const {
    auto sum = 1.f;
    auto p = 1.f;

    auto eyePathLength = eyePathVertex->length;
    auto lightPathLength = lightPathVertex->length;

    {
        auto currentVertex = eyePathVertex;
        auto nextVertex = lightPathVertex;
        auto incidentDir = currentVertex->incidentDirection; // Incident direction on the currentVertex when going toward the light node

        auto exitantDir = nextVertex->lastVertex.P - currentVertex->lastVertex.P; // Exitent direction on the currentVertex when going toward the light node
        auto l = length(exitantDir);
        if(l == 0.f) {
            return 0.f; // avoid NaN bugs
        }
        exitantDir /= l;
        auto sqrDist = sqr(l);

        for(auto i = 0u; i < lightPathLength; ++i) {
            auto pdfWrtSolidAngle = currentVertex->lastVertexBRDF.pdf(incidentDir, exitantDir);
            auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(nextVertex->lastVertex.Ns, -exitantDir)) / sqrDist;

            p *= pdfWrtArea / nextVertex->pdfLastVertex;
            sum += p;

            currentVertex = nextVertex;
            --nextVertex;
            incidentDir = -exitantDir;
            exitantDir = currentVertex->incidentDirection; // The incident direction of the next current vertex goes toward the light node
            sqrDist = sqr(currentVertex->lastVertex.distance);
        }
    }

    p = 1.f;

    {
        auto currentVertex = lightPathVertex;
        auto nextVertex = eyePathVertex;
        auto incidentDir = currentVertex->incidentDirection; // Incident direction on the currentVertex when going toward the eye node

        auto exitantDir = nextVertex->lastVertex.P - currentVertex->lastVertex.P;  // Exitent direction on the currentVertex when going toward the light node
        auto l = length(exitantDir);
        if(l == 0.f) {
            return 0.f; // avoid NaN bugs
        }
        exitantDir /= l;
        auto sqrDist = sqr(l);

        for(auto i = 0u; i < eyePathLength - 1; ++i) {
            auto pdfWrtSolidAngle = currentVertex->lastVertexBRDF.pdf(incidentDir, exitantDir);
            auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(nextVertex->lastVertex.Ns, -exitantDir)) / sqrDist;

            p *= pdfWrtArea / nextVertex->pdfLastVertex;
            sum += p;

            currentVertex = nextVertex;
            --nextVertex;
            incidentDir = -exitantDir;
            exitantDir = currentVertex->incidentDirection; // The incident direction of the next current vertex goes toward the eye node
            sqrDist = currentVertex->lastVertex.distance;
        }
    }

    return 1.f / sum;
}

float SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::computeMISWeight(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       PathVertex* pEyePathVertex,
                       const EmissionVertex* lightNode,
                       const Vec3f& dirToLight) const {
    auto sum = 1.f;
    auto p = 1.f;

    auto pdfWrtArea = lightNode->pdfWrtArea(pEyePathVertex->lastVertex);
    p *= pdfWrtArea / pEyePathVertex->pdfLastVertex;

    sum += p;

    p = 1.f;

    auto currentVertex = pEyePathVertex;
    auto nextVertex = pEyePathVertex - 1;
    auto incidentDir = dirToLight;

    auto pEyePathLength = pEyePathVertex->length;

    for(auto i = 0u; i < pEyePathLength - 2; ++i) {
        auto exitantDir = currentVertex->incidentDirection;
        auto pdfWrtSolidAngle = currentVertex->lastVertexBRDF.pdf(incidentDir, exitantDir);
        auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(nextVertex->lastVertex.Ns, -exitantDir)) / sqr(currentVertex->lastVertex.distance);

        p *= pdfWrtArea / nextVertex->pdfLastVertex;
        sum += p;

        currentVertex = nextVertex;
        --nextVertex;
        incidentDir = -exitantDir;
    }

    return 1.f / sum;
}

void SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::Li(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                                                          uint32_t x, uint32_t y, Vec4f values[8]) const {
    for(auto i = 0u; i < 8; ++i) {
        values[i] += Vec4f(0.f, 0.f, 0.f, 1.f);
    }

    auto c = getRandomCallCount(threadID);

    auto s2D = getPixelSample(threadID, sampleID);
    auto primRay = getPrimaryRay(x, y, s2D);

    PathVertex eyePath[8];

    {
        auto I = getScene().intersect(primRay);

        if(!I) {
            values[FINAL_RENDER] += Vec4f(badColor(), 1.f);
            return;
        }

        eyePath[0].lastVertex = I;
        eyePath[0].lastVertexBRDF = getScene().shade(eyePath[0].lastVertex);
        eyePath[0].incidentDirection = -primRay.dir.xyz();
        eyePath[0].length = 1;
        eyePath[0].pdf = 1.f;
        eyePath[0].pdfLastVertex = 1.f;
        eyePath[0].power = Vec3f(1.f);
    }
    auto L = zero<Vec3f>();

    auto lightNode = m_EmissionSampler.sample(getScene(), getFloat(threadID),
                                              getFloat(threadID), getFloat2(threadID));

    // trace an eye patheyePath[0].lastVertex
    for(auto i = 1u; i < m_nMaxDepth; ++i) {
        auto k = i - 1u; // index of vertex in eyePath;

        Vec4i nodes;
        Vec4f nodeWeights;

        if(m_bUseSkeleton) {
            if(i > 1u) {
                nodes = Vec4i(getScene().getSegmentedCurvSkeleton()->getNearestNode(eyePath[k].lastVertex), -1, -1, -1);
                nodeWeights = Vec4f(1, 0, 0, 0);
            } else {
//                nodes = Vec4i(getScene().getSegmentedCurvSkeleton()->getNearestNode(eyePath[k].lastVertex), -1, -1, -1);
//                nodeWeights = Vec4f(1, 0, 0, 0);
                skeletonMapping(eyePath[k].lastVertex, pixelID, nodes, nodeWeights);
            }
        }

        for(auto lightPathLength = 1u; lightPathLength <= getMaxLightPathDepth(); ++lightPathLength) {
            if(!m_bUseSkeleton || nodes[0] < 0) {
                auto distribOffset = (lightPathLength - 1) * m_nDistributionSize;
                auto firstPathOffset = lightPathLength - 1;

                Sample1u lightPathDiscreteSample =
                        sampleDiscreteDistribution1D(m_NoNodeDistributionsArray.get() + distribOffset, getLightPathCountPerNode(), getFloat(threadID));

                if(lightPathDiscreteSample.pdf > 0.f) {
                    PathVertex* lightPath = &m_NoNodeLightPathArray[firstPathOffset + lightPathDiscreteSample.value * getMaxLightPathDepth()];
                    auto totalLength = i + lightPath->length + 1;

                    if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                        Vec3f incidentDirection;
                        float dist;
                        auto G = geometricFactor(eyePath[k].lastVertex, lightPath->lastVertex, incidentDirection, dist);
                        Ray incidentRay(eyePath[k].lastVertex, lightPath->lastVertex, incidentDirection, dist);

                        if(G > 0.f && !getScene().occluded(incidentRay)) {
                            auto weight = computeMISWeight(threadID, pixelID, sampleID, &eyePath[k], lightPath);

                            auto contrib = eyePath[k].power * lightPath->power * lightPath->lastVertexBRDF.eval(lightPath->incidentDirection, -incidentDirection)
                                    * G * eyePath[k].lastVertexBRDF.eval(incidentDirection, eyePath[k].incidentDirection) / lightPathDiscreteSample.pdf;

                            L += weight * contrib;

//                            if(std::isnan(luminance(L))) {
//                                std::cerr << weight << std::endl;
//                                std::cerr << contrib << std::endl;
//                                std::cerr << i << std::endl;
//                                std::cerr << lightPath->length << std::endl;
//                                std::cerr << lightPath->pdf << std::endl;
//                                exit(0);
//                            }

                            values[totalLength] += Vec4f(weight * contrib, 0.f);
                        }
                    }
                }
            } else {
                PathVertex* lightPath;
                Sample1u lightPathDiscreteSample;
                Sample1u sampledNodeChannel;
                int sampledNodeIdx;

                bool useVisible = getFloat(threadID) <= 0.5f ? true : false;

                if(sampleSkeleton(threadID, pixelID, nodes, nodeWeights, lightPathLength, &lightPath, lightPathDiscreteSample,
                                  sampledNodeChannel, sampledNodeIdx, useVisible) && lightPath->pdf > 0.f) {
                    auto totalLength = i + lightPath->length + 1;

                    if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                        Vec3f incidentDirection;
                        float dist;
                        auto G = geometricFactor(eyePath[k].lastVertex, lightPath->lastVertex, incidentDirection, dist);
                        Ray incidentRay(eyePath[k].lastVertex, lightPath->lastVertex, incidentDirection, dist);

                        if(G > 0.f && !getScene().occluded(incidentRay)) {
                            auto weight = computeMISWeight(threadID, pixelID, sampleID, &eyePath[k], lightPath);

                            auto contrib = eyePath[k].power * lightPath->power * lightPath->lastVertexBRDF.eval(lightPath->incidentDirection, -incidentDirection)
                                    * G * eyePath[k].lastVertexBRDF.eval(incidentDirection, eyePath[k].incidentDirection) / (0.5f * lightPathDiscreteSample.pdf);
                            L += weight * contrib;
                            values[totalLength] += Vec4f(weight * contrib, 0.f);
                        }
                    }
                }
            }
        }

        if(i >= 2u) {
            auto totalLength = i + 1;
            if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                Ray shadowRay;
                auto G = lightNode->G(eyePath[k].lastVertex, shadowRay);
                if(G > 0.f && !getScene().occluded(shadowRay)) {
                    auto power = lightNode->getRadiantExitance() / lightNode->pdf();
                    auto contrib = eyePath[k].power * power * lightNode->Le(-shadowRay.dir)
                            * G * eyePath[k].lastVertexBRDF.eval(shadowRay.dir, eyePath[k].incidentDirection);
                    auto weight = computeMISWeight(threadID, pixelID, sampleID, &eyePath[k], lightNode.get(), shadowRay.dir);
                    L += weight * contrib;
                    values[totalLength] += Vec4f(weight * contrib, 0.f);
                }
            }
        }

        if(i < m_nMaxDepth - 1) {
            Sample3f wiSample;
            auto fr = eyePath[k].lastVertexBRDF.sample(eyePath[k].incidentDirection, wiSample, getFloat2(threadID));

            if(wiSample.pdf > 0.f) {
                auto throughput = fr * max(0.f, dot(eyePath[k].lastVertex.Ns, wiSample.value)) / wiSample.pdf;
                auto I = getScene().intersect(Ray(eyePath[k].lastVertex, wiSample.value));
                if(!I) {
                    break;
                }

                eyePath[k + 1].lastVertex = I;
                eyePath[k + 1].lastVertexBRDF = getScene().shade(eyePath[k + 1].lastVertex);
                eyePath[k + 1].incidentDirection = -wiSample.value;
                eyePath[k + 1].length = eyePath[k].length + 1;
                eyePath[k + 1].pdfLastVertex = wiSample.pdf * max(0.f, dot(I.Ns, -wiSample.value)) / sqr(I.distance);
                eyePath[k + 1].pdf = eyePath[k].pdf * eyePath[k + 1].pdfLastVertex;
                eyePath[k + 1].power = eyePath[k].power * throughput;

                if(eyePath[k + 1].pdfLastVertex == 0.f) {
                    break;
                }
            } else {
                break;
            }
        }
    }

    if(std::isnan(luminance(L))) {
        std::cerr << "BUG AGAIN IN SKELBIDIR" << std::endl;

        std::cerr << threadID << ", " << pixelID << ", " << sampleID << ", "  << c
                     << ", " << x << ", " << y << std::endl;
    }

    values[FINAL_RENDER] += Vec4f(L, 0.f);
}

void SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::sampleLightPaths(uint32_t threadID, PathVertex* pBuffer) const {
    auto powerScale = 1.f / getLightPathCountPerNode();
    auto maxLightPathDepth = getMaxLightPathDepth();

    for(auto k = 0u; k < getLightPathCountPerNode(); ++k) {
        auto pathOffset = k * maxLightPathDepth;

        auto pEmissionVertex = m_EmissionSampler.sample(getScene(),
                                                        getFloat(threadID),
                                                        getFloat(threadID),
                                                        getFloat2(threadID));

        RaySample ray;
        auto Le_dir = pEmissionVertex->sampleWi(ray, getFloat2(threadID));
        auto I = getScene().intersect(ray.value);

        auto power = powerScale * pEmissionVertex->getRadiantExitance() * Le_dir / (pEmissionVertex->pdf() * ray.pdf);
        auto pdfLastVertex = pEmissionVertex->pdfWrtArea(I);
        auto pdf = pEmissionVertex->pdf() * pdfLastVertex;
        auto incidentDirection = -ray.value.dir;

        for(auto length = 1u; length <= maxLightPathDepth; ++length) {
            auto idx = pathOffset + length - 1;
            if(I) {
                auto brdf = getScene().shade(I);
                pBuffer[idx].lastVertex = I;
                pBuffer[idx].lastVertexBRDF = brdf;
                pBuffer[idx].incidentDirection = incidentDirection;
                pBuffer[idx].pdf = pdf;
                pBuffer[idx].pdfLastVertex = pdfLastVertex;
                pBuffer[idx].power = power;
                pBuffer[idx].length = length;

                if(length < maxLightPathDepth) {
                    Sample3f woSample;
                    auto fr = brdf.sample(incidentDirection, woSample, getFloat2(threadID));
                    if(woSample.pdf > 0.f && fr != zero<Vec3f>()) {
                        auto Itmp = getScene().intersect(Ray(I, woSample.value));
                        if(Itmp) {
                            incidentDirection = -woSample.value;
                            auto sqrLength = sqr(Itmp.distance);
                            pdfLastVertex = woSample.pdf  * max(0.f, dot(Itmp.Ns, incidentDirection)) / sqrLength;
                            pdf *= pdfLastVertex;
                            power *= max(0.f, dot(I.Ns, woSample.value)) * fr / woSample.pdf;
                        }
                        if(pdfLastVertex > 0.f) {
                            I = Itmp;
                        } else {
                            I = Intersection();
                        }
                    } else {
                        I = Intersection();
                    }
                }
            } else {
                pBuffer[idx].pdf = 0.f;
                pBuffer[idx].pdfLastVertex = 0.f;
                pBuffer[idx].power = Vec3f(0.f);
                pBuffer[idx].length = length;
            }
        }
    }
}

void SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::computeSkelNodeLightPathsDistributions() {
    auto maxLightPathDepth = getMaxLightPathDepth();
    auto nbVertexPerNode = getLightPathVertexPerNode();
    auto pSkel = getScene().getSegmentedCurvSkeleton();
    auto completeSize = nbVertexPerNode * pSkel->size();

    if(!m_NoNodeLightPathArray) {
        m_nDistributionSize = getDistribution1DBufferSize(getLightPathCountPerNode());
        // A distribution contains all paths of the same length
        // There is maxLightPathDepth possible length and getLightPathCountPerNode() paths of a given length
        m_SkelNodeDistributionsArray = makeUniqueArray<float>(2 * maxLightPathDepth * m_nDistributionSize * pSkel->size());
        m_NoNodeLightPathArray = makeUniqueArray<PathVertex>(nbVertexPerNode);
        m_NoNodeDistributionsArray = makeUniqueArray<float>(maxLightPathDepth * m_nDistributionSize);
    }

    sampleLightPaths(0u, m_NoNodeLightPathArray.get());
    for(auto length = 1u; length <= maxLightPathDepth; ++length) {
        auto distribOffset = (length - 1) * m_nDistributionSize;
        auto firstPathOffset = length - 1;

        buildDistribution1D([this, firstPathOffset, maxLightPathDepth](uint32_t i) {
            auto pathIdx = firstPathOffset + i * maxLightPathDepth;
            return m_NoNodeLightPathArray[pathIdx].pdf > 0.f ? 1.f : 0.f;
        }, m_NoNodeDistributionsArray.get() + distribOffset, getLightPathCountPerNode());
    }

    if(m_bUseSkeleton) {
        // For each node, compute intersections, irradiance values and distributions
        launchThreads([pSkel, nbVertexPerNode, maxLightPathDepth, this](uint32_t threadID) {
            uint32_t loopID = 0u;

            while(true) {
                auto nodeIndex = loopID * getThreadCount() + threadID;
                loopID++;

                if(nodeIndex >= pSkel->size()) {
                    break;
                }

                auto nodePos = pSkel->getNode(nodeIndex).P;
                auto nodeRadius = pSkel->getNode(nodeIndex).maxball;
                auto offset = nodeIndex * nbVertexPerNode;

                for(auto length = 1u; length <= maxLightPathDepth; ++length) {
                    auto distribOffset1 = 2 * nodeIndex * m_nDistributionSize * maxLightPathDepth + (length - 1) * 2 * m_nDistributionSize;
                    auto firstPathOffset = length - 1;

                    buildDistribution1D([this, firstPathOffset, maxLightPathDepth, nodePos, nodeRadius, nodeIndex](uint32_t i) {
                        auto pathIdx = firstPathOffset + i * maxLightPathDepth;

                        auto& I = m_NoNodeLightPathArray[pathIdx].lastVertex;
                        auto dir = nodePos - I.P;
                        auto l = BnZ::length(dir);
                        dir /= l;

                        // If unbiased sampling, give less sampling probability to non-visible points
                        if(m_NoNodeLightPathArray[pathIdx].pdf > 0.f && dot(I.Ns, dir) <= 0.f || getScene().occluded(Ray(I, dir, l))) {
                            return 1.f;
//                            return sqr(nodeRadius) * (1.f / sqr(l)) *
//                                                            luminance(m_NoNodeLightPathArray[pathIdx].lastVertexBRDF.diffuseTerm() *
//                                                                m_NoNodeLightPathArray[pathIdx].power);
                        }
                        return 0.f;
//                        return sqr(nodeRadius) * (1.f / sqr(l)) *
//                                luminance(m_NoNodeLightPathArray[pathIdx].lastVertexBRDF.diffuseTerm() *
//                                    m_NoNodeLightPathArray[pathIdx].power);
                    }, m_SkelNodeDistributionsArray.get() + distribOffset1, getLightPathCountPerNode());

                    auto distribOffset2 = distribOffset1 + m_nDistributionSize;

                    buildDistribution1D([this, firstPathOffset, maxLightPathDepth, nodePos, nodeRadius, nodeIndex](uint32_t i) {
                        auto pathIdx = firstPathOffset + i * maxLightPathDepth;

                        auto& I = m_NoNodeLightPathArray[pathIdx].lastVertex;
                        auto dir = nodePos - I.P;
                        auto l = BnZ::length(dir);
                        dir /= l;

                        // If unbiased sampling, give less sampling probability to non-visible points
                        if(m_NoNodeLightPathArray[pathIdx].pdf == 0.f || dot(I.Ns, dir) <= 0.f || getScene().occluded(Ray(I, dir, l))) {
                            return 0.f;
                        }

                        return 1.f;
//                        return sqr(nodeRadius) * (1.f / sqr(l)) *
//                                                        luminance(m_NoNodeLightPathArray[pathIdx].lastVertexBRDF.diffuseTerm() *
//                                                            m_NoNodeLightPathArray[pathIdx].power);
                    }, m_SkelNodeDistributionsArray.get() + distribOffset2, getLightPathCountPerNode());
                }
            }
        });
    }
}

void SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::computeSkeletonMapping() {
    auto framebufferSize = getFramebuffer().getSize();
    m_MappingData.resizeBuffers(framebufferSize.x * framebufferSize.y);

    Vec2u tileCount = framebufferSize / getTileSize() +
            Vec2u(framebufferSize % getTileSize() != zero<Vec2u>());

    auto totalCount = tileCount.x * tileCount.y;

    auto pSkel = getScene().getSegmentedCurvSkeleton();

    auto task = [&](uint32_t threadID) {
        auto loopID = 0u;
        while(true) {
            auto tileID = loopID * getThreadCount() + threadID;
            ++loopID;

            if(tileID >= totalCount) {
                return;
            }

            uint32_t tileX = tileID % tileCount.x;
            uint32_t tileY = tileID / tileCount.x;

            Vec2u tileOrg = Vec2u(tileX, tileY) * getTileSize();
            auto viewport = Vec4u(tileOrg, getTileSize());

            if(viewport.x + viewport.z > framebufferSize.x) {
                viewport.z = framebufferSize.x - viewport.x;
            }

            if(viewport.y + viewport.w > framebufferSize.y) {
                viewport.w = framebufferSize.y - viewport.y;
            }

            auto xEnd = viewport.x + viewport.z;
            auto yEnd = viewport.y + viewport.w;

            for(auto y = viewport.y; y < yEnd; ++y) {
                for(auto x = viewport.x; x < xEnd; ++x) {
                    auto pixelIdx = getPixelIndex(x, y);
                    auto s2D = Vec2f(0.5f, 0.5f);
                    auto ray = getPrimaryRay(x, y, s2D);
                    auto I = getScene().intersect(ray);
                    if(I) {
                        m_MappingData.nodes[pixelIdx] = getNearestNodes(*pSkel, I.P, I.Ns, m_MappingData.weights[pixelIdx],
                                            [](uint32_t nodeIdx, Vec3f dir, float dist) { return false; });
                    } else {
                        m_MappingData.nodes[pixelIdx] = Vec4i(-1);
                    }
                }
            }
        }
    };

    launchThreads(task);
}

void SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::preprocess() {
    m_EmissionSampler.initFrame(getScene());

    if(m_bUseSkeleton && !m_bUseSkelGridMapping) {
        computeSkeletonMapping();
    }
}

void SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::beginFrame() {
    computeSkelNodeLightPathsDistributions();
}

void SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    auto spp = getSppCount();
    TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
        Vec4f L[8];
        std::fill(L, L + 8, Vec4f(0.f));
        auto pixelIdx = getPixelIndex(x, y);
        for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
            Li(threadID, pixelIdx, sampleID, x, y, L);
        }
        for(auto i = 0u; i < 8; ++i) {
            getFramebuffer().accumulate(i, pixelIdx, L[i]);
        }
    });
}

void SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::doExposeIO(TwBar* bar) {
    atb::addVarRW(bar, "Max node count", m_nMaxNodeCount);
    atb::addVarRW(bar, ATB_VAR(m_bUseSkelGridMapping));
    atb::addVarRW(bar, ATB_VAR(m_nNodeLightPathCount));
    atb::addVarRW(bar, ATB_VAR(m_nMaxDepth));
    atb::addVarRW(bar, ATB_VAR(m_bUseSkeleton));
    atb::addButton(bar, "clear", [this]() {
       m_DebugStream.clearObjects();
    });
}

void SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
    getAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
    getAttribute(xml, "useGridSkelMapping", m_bUseSkelGridMapping);
    getAttribute(xml, "nodeLightPathCount", m_nNodeLightPathCount);
    getAttribute(xml, "maxDepth", m_nMaxDepth);
    getAttribute(xml, "useSkeleton", m_bUseSkeleton);
}

void SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
    setAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
    setAttribute(xml, "useGridSkelMapping", m_bUseSkelGridMapping);
    setAttribute(xml, "nodeLightPathCount", m_nNodeLightPathCount);
    setAttribute(xml, "maxDepth", m_nMaxDepth);
    setAttribute(xml, "useSkeleton", m_bUseSkeleton);
}

}
