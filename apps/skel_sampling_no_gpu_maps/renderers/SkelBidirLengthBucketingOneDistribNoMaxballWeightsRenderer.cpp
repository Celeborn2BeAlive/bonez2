#include "SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer.hpp"

namespace BnZ {

SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer::SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer(const Shared<GLDebugRenderer>& debugRenderer):
    m_pDebugRenderer(debugRenderer), m_DebugStream(newGLDebugStream(m_pDebugRenderer, "SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer" + toString(getMicroseconds()))) {
}

std::string SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer::getName() const {
    return "SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer";
}

uint32_t SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer::getLightPathCountPerNode() const {
    return m_nNodeLightPathCount;
}

uint32_t SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer::getMaxLightPathDepth() const {
    return m_nMaxDepth - 2;
}

uint32_t SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer::getLightPathVertexPerNode() const {
    return getLightPathCountPerNode() * getMaxLightPathDepth();
}

void SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer::skeletonMapping(const SurfacePoint& I, uint32_t pixelIdx, Vec4i& nearestNodes, Vec4f& weights) const {
    if(m_bUseSkelGridMapping) {
        nearestNodes = Vec4i(getScene().getSegmentedCurvSkeleton()->getNearestNode(I), -1, -1, -1);
        weights = Vec4f(1, 0, 0, 0);
    } else {
        nearestNodes = m_MappingData.nodes[pixelIdx];
        weights = m_MappingData.weights[pixelIdx];
    }
}

// Sample a light path from a skeleton node
bool SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer::sampleSkeletonNode(uint32_t threadID,
                        uint32_t pixelIdx,
                        uint32_t nodeIdx,
                        uint32_t lightPathLength,
                        PathVertex** lightPath,
                        Sample1u& lightPathDiscreteSample) const {
    auto distribOffset = nodeIdx * m_nDistributionSize * getMaxLightPathDepth() + (lightPathLength - 1) * m_nDistributionSize;
    auto firstPathOffset = /*nodeIdx * getLightPathVertexPerNode() + */lightPathLength - 1;

    // Resampling of a point based on precomputed radiosities
    lightPathDiscreteSample = sampleDiscreteDistribution1D(m_SkelNodeDistributionsArray.get() + distribOffset,
                                                           getLightPathCountPerNode(),
                                                           getFloat(threadID));
    if(lightPathDiscreteSample.pdf > 0.f) {
        *lightPath = &m_NoNodeLightPathArray[firstPathOffset + lightPathDiscreteSample.value * getMaxLightPathDepth()];
        return true;
    }

    return false;
}

void SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer::Li(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx, uint32_t sampleID, Vec4f values[]) const {
    for(auto i = 0u; i < 8; ++i) {
        values[i] += Vec4f(0.f, 0.f, 0.f, 1.f);
    }

    PathVertex eyePath[8];

    {
        auto I = getScene().intersect(primRay);

        if(!I) {
            values[FINAL_RENDER] += Vec4f(badColor(), 1.f);
            return;
        }

        eyePath[0].lastVertex = I;
        eyePath[0].lastVertexBRDF = getScene().shade(eyePath[0].lastVertex);
        eyePath[0].incidentDirection = -primRay.dir.xyz();
        eyePath[0].depth = 1;
        eyePath[0].pdf = 1.f;
        eyePath[0].pdfLastVertex = 1.f;
        eyePath[0].power = Vec3f(1.f);
    }
    auto L = zero<Vec3f>();

    auto lightNode = m_EmissionSampler.sample(getScene(), getFloat(threadID),
                                              getFloat(threadID), getFloat2(threadID));

    // trace an eye patheyePath[0].lastVertex
    for(auto i = 1u; i < m_nMaxDepth; ++i) {
        auto k = i - 1u; // index of vertex in eyePath;

        Vec4i nodes;
        Vec4f nodeWeights;
        DiscreteDistribution4f nodeDist;

        if(m_bUseSkeleton) {
//            nodes = Vec4i(0, -1, -1, -1);
//            nodeWeights = Vec4f(1, 0, 0, 0);
            if(i > 1u) {
                nodes = Vec4i(getScene().getSegmentedCurvSkeleton()->getNearestNode(eyePath[k].lastVertex), -1, -1, -1);
                nodeWeights = Vec4f(1, 0, 0, 0);
            } else {
//                nodes = Vec4i(getScene().getSegmentedCurvSkeleton()->getNearestNode(eyePath[k].lastVertex), -1, -1, -1);
//                nodeWeights = Vec4f(1, 0, 0, 0);
                skeletonMapping(eyePath[k].lastVertex, pixelIdx, nodes, nodeWeights);

                auto size = 0u;
                Vec4f weights = zero<Vec4f>();
                for (size = 0u;
                     size < min(m_nMaxNodeCount, 4u)
                     && nodes[size] >= 0;
                     ++size) {
                    weights[size] = nodeWeights[size];
                }

                nodeDist = DiscreteDistribution4f(weights);
            }
        }

        for(auto lightPathLength = 1u; lightPathLength <= getMaxLightPathDepth(); ++lightPathLength) {
            auto totalLength = i + lightPathLength + 1;

            if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                if(!m_bUseSkeleton || nodes[0] < 0) {
                    auto distribOffset = (lightPathLength - 1) * m_nDistributionSize;
                    auto firstPathOffset = lightPathLength - 1;

                    Sample1u lightPathDiscreteSample =
                            sampleDiscreteDistribution1D(m_NoNodeDistributionsArray.get() + distribOffset, getLightPathCountPerNode(), getFloat(threadID));

                    if(lightPathDiscreteSample.pdf > 0.f) {
                        PathVertex* lightPath = &m_NoNodeLightPathArray[firstPathOffset + lightPathDiscreteSample.value * getMaxLightPathDepth()];

                        Vec3f incidentDirection;
                        float dist;
                        auto G = geometricFactor(eyePath[k].lastVertex, lightPath->lastVertex, incidentDirection, dist);
                        Ray incidentRay(eyePath[k].lastVertex, lightPath->lastVertex, incidentDirection, dist);

                        if(G > 0.f) {
                            if(!getScene().occluded(incidentRay)) {
                                auto weight = computeMISWeight(threadID, pixelIdx, sampleID, &eyePath[k], lightPath, lightNode.get());

                                auto contrib = eyePath[k].power * lightPath->power * lightPath->lastVertexBRDF.eval(lightPath->incidentDirection, -incidentDirection)
                                        * G * eyePath[k].lastVertexBRDF.eval(incidentDirection, eyePath[k].incidentDirection) / lightPathDiscreteSample.pdf;

                                L += weight * contrib;
                                values[totalLength] += Vec4f(weight * contrib, 0.f);
                            }
                        }
                    }
                } else {
                    PathVertex* lightPath;
                    Sample1u lightPathDiscreteSample;
                    Sample1u sampledNodeChannel;
                    int sampledNodeIdx;

                    if(i == 1u) {
                        sampledNodeChannel = nodeDist.sample(getFloat(threadID));
                    } else {
                        sampledNodeChannel = Sample1u(0, 1.f);
                    }

                    if(sampledNodeChannel.pdf > 0.f) {
                        sampledNodeIdx = nodes[sampledNodeChannel.value];

                        if(sampledNodeIdx >= 0 && sampleSkeletonNode(threadID, pixelIdx, sampledNodeIdx, lightPathLength, &lightPath, lightPathDiscreteSample)
                                && lightPath->pdf > 0.f) {
                            Vec3f incidentDirection;
                            float dist;
                            auto G = geometricFactor(eyePath[k].lastVertex, lightPath->lastVertex, incidentDirection, dist);
                            Ray incidentRay(eyePath[k].lastVertex, lightPath->lastVertex, incidentDirection, dist);

                            if(G > 0.f) {
                                if(!getScene().occluded(incidentRay)) {
                                    auto weight = computeMISWeight(threadID, pixelIdx, sampleID, &eyePath[k], lightPath, lightNode.get());

                                    auto contrib = eyePath[k].power * lightPath->power * lightPath->lastVertexBRDF.eval(lightPath->incidentDirection, -incidentDirection)
                                            * G * eyePath[k].lastVertexBRDF.eval(incidentDirection, eyePath[k].incidentDirection) / lightPathDiscreteSample.pdf;
                                    L += weight * contrib;
                                    values[totalLength] += Vec4f(weight * contrib, 0.f);
                                }
                            }
                        }
                    }
                }
            }
        }

        if(i >= 2u) {
            auto totalLength = i + 1;
            if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                Ray shadowRay;
                auto G = lightNode->G(eyePath[k].lastVertex, shadowRay);
                if(G > 0.f && !getScene().occluded(shadowRay)) {
                    auto power = lightNode->getRadiantExitance() / lightNode->pdf();
                    auto contrib = eyePath[k].power * power * lightNode->Le(-shadowRay.dir)
                            * G * eyePath[k].lastVertexBRDF.eval(shadowRay.dir, eyePath[k].incidentDirection);
                    auto weight = computeMISWeight(threadID, pixelIdx, sampleID, &eyePath[k], lightNode.get(), shadowRay.dir);
                    L += weight * contrib;
                    values[totalLength] += Vec4f(weight * contrib, 0.f);
                }
            }
        }

        if(i < m_nMaxDepth - 1) {
            Sample3f wiSample;
            auto fr = eyePath[k].lastVertexBRDF.sample(eyePath[k].incidentDirection, wiSample, getFloat2(threadID));

            if(wiSample.pdf > 0.f) {
                auto throughput = fr * max(0.f, dot(eyePath[k].lastVertex.Ns, wiSample.value)) / wiSample.pdf;
                auto I = getScene().intersect(Ray(eyePath[k].lastVertex, wiSample.value));
                if(!I) {
                    break;
                }

                eyePath[k + 1].lastVertex = I;
                eyePath[k + 1].lastVertexBRDF = getScene().shade(eyePath[k + 1].lastVertex);
                eyePath[k + 1].incidentDirection = -wiSample.value;
                eyePath[k + 1].depth = eyePath[k].depth + 1;
                eyePath[k + 1].pdfLastVertex = wiSample.pdf * max(0.f, dot(I.Ns, -wiSample.value)) / sqr(I.distance);
                eyePath[k + 1].pdf = eyePath[k].pdf * eyePath[k + 1].pdfLastVertex;
                eyePath[k + 1].power = eyePath[k].power * throughput;

                if(eyePath[k + 1].pdfLastVertex == 0.f) {
                    break;
                }
            } else {
                break;
            }
        }
    }

    if(std::isnan(luminance(L))) {
        std::cerr << "BUG AGAIN IN SKELBIDIR" << std::endl;
    }

    values[FINAL_RENDER] += Vec4f(L, 0.f);
}

void SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer::sampleLightPaths(uint32_t threadID, PathVertex* pBuffer) const {
    auto powerScale = 1.f / getLightPathCountPerNode();
    auto maxLightPathDepth = getMaxLightPathDepth();

    for(auto k = 0u; k < getLightPathCountPerNode(); ++k) {
        auto pathOffset = k * maxLightPathDepth;

        auto pEmissionVertex = m_EmissionSampler.sample(getScene(),
                                                        getFloat(threadID),
                                                        getFloat(threadID),
                                                        getFloat2(threadID));

        RaySample ray;
        auto Le_dir = pEmissionVertex->sampleWi(ray, getFloat2(threadID));
        auto I = getScene().intersect(ray.value);

        auto power = powerScale * pEmissionVertex->getRadiantExitance() * Le_dir / (pEmissionVertex->pdf() * ray.pdf);
        auto pdfLastVertex = pEmissionVertex->pdfWrtArea(I);
        auto pdf = pEmissionVertex->pdf() * pdfLastVertex;
        auto incidentDirection = -ray.value.dir;

        for(auto length = 1u; length <= maxLightPathDepth; ++length) {
            auto idx = pathOffset + length - 1;
            if(I) {
                auto brdf = getScene().shade(I);
                pBuffer[idx].lastVertex = I;
                pBuffer[idx].lastVertexBRDF = brdf;
                pBuffer[idx].incidentDirection = incidentDirection;
                pBuffer[idx].pdf = pdf;
                pBuffer[idx].pdfLastVertex = pdfLastVertex;
                pBuffer[idx].power = power;
                pBuffer[idx].depth = length;

                if(length < maxLightPathDepth) {
                    Sample3f woSample;
                    auto fr = brdf.sample(incidentDirection, woSample, getFloat2(threadID));
                    if(woSample.pdf > 0.f && fr != zero<Vec3f>()) {
                        auto Itmp = getScene().intersect(Ray(I, woSample.value));
                        if(Itmp) {
                            incidentDirection = -woSample.value;
                            auto sqrLength = sqr(Itmp.distance);
                            pdfLastVertex = woSample.pdf  * max(0.f, dot(Itmp.Ns, incidentDirection)) / sqrLength;
                            pdf *= pdfLastVertex;
                            power *= max(0.f, dot(I.Ns, woSample.value)) * fr / woSample.pdf;
                        }
                        if(pdfLastVertex > 0.f) {
                            I = Itmp;
                        } else {
                            I = Intersection();
                        }
                    } else {
                        I = Intersection();
                    }
                }
            } else {
                pBuffer[idx].pdf = 0.f;
                pBuffer[idx].pdfLastVertex = 0.f;
                pBuffer[idx].power = Vec3f(0.f);
                pBuffer[idx].depth = length;
            }
        }
    }
}

void SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer::computeSkelNodeLightPathsDistributions() {
    auto maxLightPathDepth = getMaxLightPathDepth();
    auto nbVertexPerNode = getLightPathVertexPerNode();
    auto pSkel = getScene().getSegmentedCurvSkeleton();
    auto completeSize = nbVertexPerNode * pSkel->size();

    if(!m_NoNodeLightPathArray) {
        //m_SkelNodeLightPathArray = makeUniqueArray<PathVertex>(completeSize);
        m_nDistributionSize = getDistribution1DBufferSize(getLightPathCountPerNode());
        // A distribution contains all paths of the same length
        // There is maxLightPathDepth possible length and getLightPathCountPerNode() paths of a given length
        m_SkelNodeDistributionsArray = makeUniqueArray<float>(maxLightPathDepth * m_nDistributionSize * pSkel->size());
        //m_SkelNodePowerArray = makeUniqueArray<float>(pSkel->size());
        m_NoNodeLightPathArray = makeUniqueArray<PathVertex>(nbVertexPerNode);
        m_NoNodeDistributionsArray = makeUniqueArray<float>(maxLightPathDepth * m_nDistributionSize);
    }

    sampleLightPaths(0u, m_NoNodeLightPathArray.get());
    for(auto length = 1u; length <= maxLightPathDepth; ++length) {
        auto distribOffset = (length - 1) * m_nDistributionSize;
        auto firstPathOffset = length - 1;

        buildDistribution1D([this, firstPathOffset, maxLightPathDepth](uint32_t i) {
            auto pathIdx = firstPathOffset + i * maxLightPathDepth;
            return luminance(m_NoNodeLightPathArray[pathIdx].power);
        }, m_NoNodeDistributionsArray.get() + distribOffset, getLightPathCountPerNode());
    }

    if(m_bUseSkeleton) {
        // For each node, compute intersections, irradiance values and distributions
        launchThreads([pSkel, nbVertexPerNode, maxLightPathDepth, this](uint32_t threadID) {
            uint32_t loopID = 0u;

            while(true) {
                auto nodeIndex = loopID * getThreadCount() + threadID;
                loopID++;

                if(nodeIndex >= pSkel->size()) {
                    break;
                }

                auto nodePos = pSkel->getNode(nodeIndex).P;
                auto nodeRadius = pSkel->getNode(nodeIndex).maxball;
                auto offset = nodeIndex * nbVertexPerNode;

                for(auto length = 1u; length <= maxLightPathDepth; ++length) {
                    auto distribOffset = nodeIndex * m_nDistributionSize * maxLightPathDepth + (length - 1) * m_nDistributionSize;
                    auto firstPathOffset = length - 1;

                    buildDistribution1D([this, firstPathOffset, maxLightPathDepth, nodePos, nodeRadius, nodeIndex](uint32_t i) {
                        auto pathIdx = firstPathOffset + i * maxLightPathDepth;

                        auto& I = m_NoNodeLightPathArray[pathIdx].lastVertex;
                        auto dir = nodePos - I.P;
                        auto l = BnZ::length(dir);
                        dir /= l;

                        // If unbiased sampling, give less sampling probability to non-visible points
                        if(dot(I.Ns, dir) <= 0.f || getScene().occluded(Ray(I, dir, l))) {
                            return m_fPowerSkelFactor * luminance(m_NoNodeLightPathArray[pathIdx].power);
                        }

                        return luminance(m_NoNodeLightPathArray[pathIdx].power);
                    }, m_SkelNodeDistributionsArray.get() + distribOffset, getLightPathCountPerNode());
                }
            }
        });
    }
}

void SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer::computeSkeletonMapping() {
    auto framebufferSize = getFramebuffer().getSize();
    m_MappingData.resizeBuffers(framebufferSize.x * framebufferSize.y);

    Vec2u tileCount = framebufferSize / getTileSize() +
            Vec2u(framebufferSize % getTileSize() != zero<Vec2u>());

    auto totalCount = tileCount.x * tileCount.y;

    auto pSkel = getScene().getSegmentedCurvSkeleton();

    auto task = [&](uint32_t threadID) {
        auto loopID = 0u;
        while(true) {
            auto tileID = loopID * getThreadCount() + threadID;
            ++loopID;

            if(tileID >= totalCount) {
                return;
            }

            uint32_t tileX = tileID % tileCount.x;
            uint32_t tileY = tileID / tileCount.x;

            Vec2u tileOrg = Vec2u(tileX, tileY) * getTileSize();
            auto viewport = Vec4u(tileOrg, getTileSize());

            if(viewport.x + viewport.z > framebufferSize.x) {
                viewport.z = framebufferSize.x - viewport.x;
            }

            if(viewport.y + viewport.w > framebufferSize.y) {
                viewport.w = framebufferSize.y - viewport.y;
            }

            auto xEnd = viewport.x + viewport.z;
            auto yEnd = viewport.y + viewport.w;

            for(auto y = viewport.y; y < yEnd; ++y) {
                for(auto x = viewport.x; x < xEnd; ++x) {
                    auto pixelIdx = getPixelIndex(x, y);
                    auto s2D = Vec2f(0.5f, 0.5f);
                    auto ray = getPrimaryRay(x, y, s2D);
                    auto I = getScene().intersect(ray);
                    if(I) {
                        m_MappingData.nodes[pixelIdx] = getNearestNodes(*pSkel, I.P, I.Ns, m_MappingData.weights[pixelIdx],
                                            [](uint32_t nodeIdx, Vec3f dir, float dist) { return false; });
                    } else {
                        m_MappingData.nodes[pixelIdx] = Vec4i(-1);
                    }
                }
            }
        }
    };

    launchThreads(task);
}

void SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer::preprocess() {
    m_fBeginTileTime = 0.f;

    m_EmissionSampler.initFrame(getScene());

    if(m_bUseSkeleton && !m_bUseSkelGridMapping) {
        computeSkeletonMapping();
    }
}

void SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer::beginFrame() {
    Timer t;
    computeSkelNodeLightPathsDistributions();

    m_fBeginTileTime += us2ms(t.getEllapsedTime());
}

void SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    auto spp = getSppCount();
    TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
        Vec4f L[8];
        std::fill(L, L + 8, Vec4f(0.f));
        auto pixelIdx = getPixelIndex(x, y);
        for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
            auto s2D = getPixelSample(threadID, sampleID);
            auto ray = getPrimaryRay(x, y, s2D);
            Li(ray, threadID, pixelIdx, sampleID, L);
        }
        for(auto i = 0u; i < 8; ++i) {
            getFramebuffer().accumulate(i, pixelIdx, L[i]);
        }
    });
}

void SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer::doExposeIO(TwBar* bar) {
    atb::addVarRW(bar, "Max node count", m_nMaxNodeCount);
    atb::addVarRW(bar, ATB_VAR(m_bUseSkelGridMapping));
    atb::addVarRW(bar, ATB_VAR(m_nNodeLightPathCount));
    atb::addVarRW(bar, ATB_VAR(m_fPowerSkelFactor));
    atb::addVarRW(bar, ATB_VAR(m_nMaxDepth));
    atb::addVarRW(bar, ATB_VAR(m_bUseSkeleton));
    atb::addButton(bar, "clear", [this]() {
       m_DebugStream.clearObjects();
    });
}

void SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
    getAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
    getAttribute(xml, "useGridSkelMapping", m_bUseSkelGridMapping);
    getAttribute(xml, "nodeLightPathCount", m_nNodeLightPathCount);
    getAttribute(xml, "powerSkelFactor", m_fPowerSkelFactor);
    getAttribute(xml, "maxDepth", m_nMaxDepth);
    getAttribute(xml, "useSkeleton", m_bUseSkeleton);
}

void SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
    setAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
    setAttribute(xml, "useGridSkelMapping", m_bUseSkelGridMapping);
    setAttribute(xml, "nodeLightPathCount", m_nNodeLightPathCount);
    setAttribute(xml, "powerSkelFactor", m_fPowerSkelFactor);
    setAttribute(xml, "maxDepth", m_nMaxDepth);
    setAttribute(xml, "useSkeleton", m_bUseSkeleton);
}

void SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer::doStoreStatistics() const {
            std::cerr << "mdr" << std::endl;
    if(auto pStats = getStatisticsOutput()) {
        std::cerr << "loulz" << std::endl;
        setAttribute(*pStats, "beginTileTime", m_fBeginTileTime);
    }
}

}
