#pragma once

#include "SceneViewer.hpp"

#include <bonez/rendering/Renderer.hpp>
#include <bonez/sampling/multiple_importance_sampling.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/sampling/distribution1d.h>
#include <bonez/sampling/shapes.hpp>
#include <bonez/sampling/patterns.hpp>
#include <bonez/scene/lights/EmissionSampler.hpp>

#include <bonez/opengl/debug/GLDebugRenderer.hpp>
#include <bonez/opengl/debug/GLDebugOutputStream.hpp>

#include "bdpt.hpp"

namespace BnZ {

class BidirRenderer: public TileProcessingRenderer {
    mutable std::ofstream m_DebugFile;

    UpdateFlag m_Flag;
    uint32_t m_nMaxDepth = 3;
    bool m_bDisplayProgress = false;

    EmissionSampler m_EmissionSampler;

    Shared<GLDebugRenderer> m_pDebugRenderer;
    mutable GLDebugOutputStream m_DebugStream;

    enum RenderTarget {
        FINAL_RENDER = 0
    };

    Unique<PathVertex[]> m_PathArray;

public:
    BidirRenderer(const Shared<GLDebugRenderer>& debugRenderer);

    virtual std::string getName() const;

    uint32_t getMaxLightPathDepth() const;

    uint32_t getMaxEyePathDepth() const;

    Unique<EmissionVertex> sampleLightPath(uint32_t threadID, uint32_t pixelID, PathVertex* pBuffer,
                                           uint32_t& lightPathLength) const;

    void Li(uint32_t threadID, uint32_t pixelID, uint32_t sampleID, uint32_t x, uint32_t y, Vec4f values[8]) const;

    void preprocess() override;

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override;

    virtual void doExposeIO(TwBar* bar);

    virtual void doLoadSettings(const tinyxml2::XMLElement& xml);

    virtual void doStoreSettings(tinyxml2::XMLElement& xml) const;
};

}
