#include "bdpt.hpp"
#include <bonez/scene/lights/AreaLight.hpp>
#include <bonez/sampling/shapes.hpp>

namespace BnZ {

float computeLightPathWeight(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                             PathVertex* eyePathVertex, PathVertex* lightPathVertex, const EmissionVertex* lightNode) {
    auto sum = 0.f;
    auto p = 1.f;
    auto lightPathLength = lightPathVertex->depth;

    auto currentVertex = eyePathVertex;
    auto nextVertex = lightPathVertex;
    auto incidentDir = currentVertex->incidentDirection; // Incident direction on the currentVertex when going toward the light node

    auto exitantDir = nextVertex->lastVertex.P - currentVertex->lastVertex.P; // Exitent direction on the currentVertex when going toward the light node
    auto l = length(exitantDir);
    if(l == 0.f) {
        return 0.f; // avoid NaN bugs
    }
    exitantDir /= l;
    auto sqrDist = sqr(l);

    for(auto i = 0u; i < lightPathLength; ++i) {
        auto pdfWrtSolidAngle = currentVertex->lastVertexBRDF.pdf(incidentDir, exitantDir);
        auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(nextVertex->lastVertex.Ns, -exitantDir)) / sqrDist;

        p *= pdfWrtArea / nextVertex->pdfLastVertex;
        sum += p;

        currentVertex = nextVertex;
        --nextVertex;
        incidentDir = -exitantDir;
        exitantDir = currentVertex->incidentDirection; // The incident direction of the next current vertex goes toward the light node
        sqrDist = sqr(currentVertex->lastVertex.distance);
    }

    p *= currentVertex->lastVertexBRDF.pdf(incidentDir, currentVertex->incidentDirection) * lightNode->g(currentVertex->lastVertex) / lightNode->pdf();
    sum += p;

    return sum;
}

float computeEyePathWeight(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                           PathVertex* eyePathVertex, PathVertex* lightPathVertex) {
    auto sum = 0.f;
    auto p = 1.f;
    auto eyePathLength = eyePathVertex->depth;

    auto currentVertex = lightPathVertex;
    auto nextVertex = eyePathVertex;
    auto incidentDir = currentVertex->incidentDirection; // Incident direction on the currentVertex when going toward the eye node

    auto exitantDir = nextVertex->lastVertex.P - currentVertex->lastVertex.P;  // Exitent direction on the currentVertex when going toward the light node
    auto l = length(exitantDir);
    if(l == 0.f) {
        return 0.f; // avoid NaN bugs
    }
    exitantDir /= l;
    auto sqrDist = sqr(l);

    for(auto i = 0u; i < eyePathLength - 1; ++i) {
        auto pdfWrtSolidAngle = currentVertex->lastVertexBRDF.pdf(incidentDir, exitantDir);
        auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(nextVertex->lastVertex.Ns, -exitantDir)) / sqrDist;

        p *= pdfWrtArea / nextVertex->pdfLastVertex;
        sum += p;

        currentVertex = nextVertex;
        --nextVertex;
        incidentDir = -exitantDir;
        exitantDir = currentVertex->incidentDirection; // The incident direction of the next current vertex goes toward the eye node
        sqrDist = sqr(currentVertex->lastVertex.distance);
    }

    return sum;
}

float computeMISWeight(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       PathVertex* eyePathVertex, PathVertex* lightPathVertex, const EmissionVertex* lightNode) {
    auto sum = 1.f;

    sum += computeLightPathWeight(threadID, pixelID, sampleID, eyePathVertex, lightPathVertex, lightNode);

    sum += computeEyePathWeight(threadID, pixelID, sampleID, eyePathVertex, lightPathVertex);

    return 1.f / sum;
}

float computeMISWeight(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       PathVertex* eyePathVertex,
                       const EmissionVertex* lightNode,
                       const Vec3f& dirToLight) {
    auto sum = 1.f;

    { // Eval p_0
       auto pdfWrtSolidAngle = eyePathVertex->lastVertexBRDF.pdf(eyePathVertex->incidentDirection, dirToLight);
       auto pdfWrtArea = pdfWrtSolidAngle * lightNode->g(eyePathVertex->lastVertex);

       sum += pdfWrtArea / lightNode->pdf();
    }

    if(eyePathVertex->depth > 1u) {
        auto p = 1.f;

        auto pdfWrtArea = lightNode->pdfWrtArea(eyePathVertex->lastVertex);
        p *= pdfWrtArea / eyePathVertex->pdfLastVertex;

        sum += p;

        auto currentVertex = eyePathVertex;
        auto nextVertex = eyePathVertex - 1;
        auto incidentDir = dirToLight;

        auto pEyePathLength = eyePathVertex->depth;

        if(pEyePathLength > 1) {
            for(auto i = 0u; i < pEyePathLength - 2; ++i) {
                auto exitantDir = currentVertex->incidentDirection;
                auto pdfWrtSolidAngle = currentVertex->lastVertexBRDF.pdf(incidentDir, exitantDir);
                auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(nextVertex->lastVertex.Ns, -exitantDir)) / sqr(currentVertex->lastVertex.distance);

                p *= pdfWrtArea / nextVertex->pdfLastVertex;
                sum += p;

                currentVertex = nextVertex;
                --nextVertex;
                incidentDir = -exitantDir;
            }
        }
    }

    return 1.f / sum;
}

float computeMISWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                                PathVertex* eyePathVertex,
                                const EmissionVertex* lightNode,
                                const Vec3f& dirToLight) {
    auto sum = 1.f;

    { // Eval p_0
       auto pdfWrtSolidAngle = eyePathVertex->lastVertexBRDF.pdf(eyePathVertex->incidentDirection, dirToLight);
       auto pdfWrtArea = pdfWrtSolidAngle * lightNode->g(eyePathVertex->lastVertex);

       sum += pdfWrtArea / lightNode->pdf();
    }

    if(eyePathVertex->depth > 1u) {
        auto pdfWrtArea = lightNode->pdfWrtArea(eyePathVertex->lastVertex);

        auto p1 = pdfWrtArea / eyePathVertex->pdfLastVertex;

        auto p2 = eyePathVertex->lastVertexBRDF.pdf(dirToLight, eyePathVertex->incidentDirection);

        sum += p1 * (1 + p2 * eyePathVertex->recursiveMISFactor);
    }

    return 1.f / sum;
}

float computeMISWeight(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       PathVertex* eyePathVertex, const Scene& scene, const EmissionSampler& sampler) {
    if(eyePathVertex->lastVertex.Le == zero<Vec3f>()) {
        return 0.f;
    }

    if(eyePathVertex->depth == 1u) {
        return 1.f;
    }

    if(!eyePathVertex->lastVertex) {
        // Hit on environment map (if present) should be handled here
        return 0.f;
    }

    auto meshID = eyePathVertex->lastVertex.meshID;
    auto lightID = scene.getGeometry().getMesh(meshID).m_nLightID;

    const auto& pLight = scene.getLightContainer().getLight(lightID);
    const auto* pAreaLight = static_cast<const AreaLight*>(pLight.get());

    auto pdfWrtArea = sampler.pdf(lightID) * pAreaLight->pdf(eyePathVertex->lastVertex, scene);

    auto sum = 1.f;
    auto p = 1.f;

    p *= pdfWrtArea / eyePathVertex->pdfLastVertex;

    sum += p;

    if(eyePathVertex->depth > 2u) {
        auto currentVertex = eyePathVertex;
        auto nextVertex = eyePathVertex - 1;

        {
            auto exitantDir = currentVertex->incidentDirection;
            auto pdfWrtSolidAngle = cosineSampleHemispherePDF(eyePathVertex->incidentDirection, eyePathVertex->lastVertex.Ns);
            auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(nextVertex->lastVertex.Ns, -exitantDir)) / sqr(currentVertex->lastVertex.distance);

            p *= pdfWrtArea / eyePathVertex->pdfLastVertex;

            sum += p;
        }

        currentVertex = nextVertex;
        --nextVertex;
        auto incidentDir = -eyePathVertex->incidentDirection;
        while(currentVertex->depth > 2u) {
            auto exitantDir = currentVertex->incidentDirection;
            auto pdfWrtSolidAngle = currentVertex->lastVertexBRDF.pdf(incidentDir, exitantDir);
            auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(nextVertex->lastVertex.Ns, -exitantDir)) / sqr(currentVertex->lastVertex.distance);

            p *= pdfWrtArea / nextVertex->pdfLastVertex;
            sum += p;

            currentVertex = nextVertex;
            --nextVertex;
            incidentDir = -exitantDir;
        }
    }

    return 1.f / sum;
}

float computeMISWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       PathVertex* eyePathVertex, const Scene& scene, const EmissionSampler& sampler) {
    if(eyePathVertex->lastVertex.Le == zero<Vec3f>()) {
        return 0.f;
    }

    if(eyePathVertex->depth == 1u) {
        return 1.f;
    }

    if(!eyePathVertex->lastVertex) {
        // Hit on environment map (if present) should be handled here
        return 0.f;
    }

    auto meshID = eyePathVertex->lastVertex.meshID;
    auto lightID = scene.getGeometry().getMesh(meshID).m_nLightID;

    const auto& pLight = scene.getLightContainer().getLight(lightID);
    const auto* pAreaLight = static_cast<const AreaLight*>(pLight.get());

    auto pdfWrtArea = sampler.pdf(lightID) * pAreaLight->pdf(eyePathVertex->lastVertex, scene);

    auto p1 = pdfWrtArea / eyePathVertex->pdfLastVertex;

    auto p2 = cosineSampleHemispherePDF(eyePathVertex->incidentDirection, eyePathVertex->lastVertex.Ns);

    auto sum = 1 + p1 * (1 + p2 * eyePathVertex->recursiveMISFactor);
    return 1.f / sum;
}

float computeLightPathWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                             PathVertex* eyePathVertex, PathVertex* lightPathVertex) {
    auto exitantDir = lightPathVertex->lastVertex.P - eyePathVertex->lastVertex.P; // Exitent direction on the currentVertex when going toward the light node
    auto l = length(exitantDir);
    if(l == 0.f) {
        return 0.f; // avoid NaN bugs
    }
    exitantDir /= l;
    auto sqrDist = sqr(l);

    auto pdfWrtSolidAngle = eyePathVertex->lastVertexBRDF.pdf(eyePathVertex->incidentDirection, exitantDir);
    auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(lightPathVertex->lastVertex.Ns, -exitantDir)) / sqrDist;

    auto p1 = pdfWrtArea / lightPathVertex->pdfLastVertex;

    auto p2 = lightPathVertex->lastVertexBRDF.pdf(-exitantDir, lightPathVertex->incidentDirection);

    return p1 * (1 + p2 * lightPathVertex->recursiveMISFactor);
}

float computeEyePathWeightRecursive(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                           PathVertex* eyePathVertex, PathVertex* lightPathVertex) {
    if(eyePathVertex->depth == 1u) {
        return 0.f;
    }

    auto exitantDir = eyePathVertex->lastVertex.P - lightPathVertex->lastVertex.P; // Exitent direction on the currentVertex when going toward the light node
    auto l = length(exitantDir);
    if(l == 0.f) {
        return 0.f; // avoid NaN bugs
    }
    exitantDir /= l;
    auto sqrDist = sqr(l);

    auto pdfWrtSolidAngle = lightPathVertex->lastVertexBRDF.pdf(lightPathVertex->incidentDirection, exitantDir);
    auto pdfWrtArea = pdfWrtSolidAngle * max(0.f, dot(eyePathVertex->lastVertex.Ns, -exitantDir)) / sqrDist;

    auto p1 = pdfWrtArea / eyePathVertex->pdfLastVertex;

    auto p2 = eyePathVertex->lastVertexBRDF.pdf(-exitantDir, eyePathVertex->incidentDirection);

    return p1 * (1 + p2 * eyePathVertex->recursiveMISFactor);
}

float computeMISWeightRecursive(uint threadID, uint pixelID, uint32_t sampleID,
                       PathVertex* eyePathVertex, PathVertex* lightPathVertex) {
    auto sum = 1.f;

    sum += computeLightPathWeightRecursive(threadID, pixelID, sampleID, eyePathVertex, lightPathVertex);

    sum += computeEyePathWeightRecursive(threadID, pixelID, sampleID, eyePathVertex, lightPathVertex);

    return 1.f / sum;
}

}
