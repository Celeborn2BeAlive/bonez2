#include "BidirRenderer.hpp"

namespace BnZ {

BidirRenderer::BidirRenderer(const Shared<GLDebugRenderer>& debugRenderer):
    m_DebugFile("BidirRenderer" + toString(getMicroseconds()) + ".debug.txt"),
    m_pDebugRenderer(debugRenderer), m_DebugStream(newGLDebugStream(m_pDebugRenderer, "BidirRenderer" + toString(getMicroseconds()))) {
}

std::string BidirRenderer::getName() const {
    return "BidirRenderer";
}

uint32_t BidirRenderer::getMaxLightPathDepth() const {
    return m_nMaxDepth - 2;
}

uint32_t BidirRenderer::getMaxEyePathDepth() const {
    return m_nMaxDepth - 1;
}

Unique<EmissionVertex> BidirRenderer::sampleLightPath(uint32_t threadID, uint32_t pixelID,
                                                      PathVertex* pBuffer, uint32_t& lightPathLength) const {
    auto maxLightPathDepth = getMaxLightPathDepth();

    auto pEmissionVertex = m_EmissionSampler.sample(getScene(),
                                                    getFloat(threadID),
                                                    getFloat(threadID),
                                                    getFloat2(threadID));

    RaySample ray;
    auto Le_dir = pEmissionVertex->sampleWi(ray, getFloat2(threadID));
    auto I = getScene().intersect(ray.value);

    auto power = pEmissionVertex->getRadiantExitance() * Le_dir / (pEmissionVertex->pdf() * ray.pdf);
    auto pdfLastVertex = pEmissionVertex->pdfWrtArea(I);
    auto pdf = pEmissionVertex->pdf() * pdfLastVertex;
    auto incidentDirection = -ray.value.dir;

    lightPathLength = 0u;

    auto recursiveMISSequence = 0.f;
    auto recursiveMISFactor = 0.f;

    for(auto length = 1u; length <= maxLightPathDepth; ++length) {
        auto idx = length - 1;
        if(I) {
            lightPathLength = length;
            auto brdf = getScene().shade(I);
            pBuffer[idx].lastVertex = I;
            pBuffer[idx].lastVertexBRDF = brdf;
            pBuffer[idx].incidentDirection = incidentDirection;
            pBuffer[idx].pdf = pdf;
            pBuffer[idx].pdfLastVertex = pdfLastVertex;
            pBuffer[idx].power = power;
            pBuffer[idx].depth = length;
//            pBuffer[idx].recursiveMISFactor = recursiveMISFactor;

            if(length < maxLightPathDepth) {
                Sample3f woSample;
                auto fr = brdf.sample(incidentDirection, woSample, getFloat2(threadID));
                if(woSample.pdf > 0.f && fr != zero<Vec3f>()) {
                    auto Itmp = getScene().intersect(Ray(I, woSample.value));

                    if(Itmp) {
                        incidentDirection = -woSample.value;
                        auto sqrLength = sqr(Itmp.distance);
                        pdfLastVertex = woSample.pdf  * max(0.f, dot(Itmp.Ns, incidentDirection)) / sqrLength;
                        pdf *= pdfLastVertex;
                        power *= max(0.f, dot(I.Ns, woSample.value)) * fr / woSample.pdf;

                        // Doesn't handle intersection with the light source
                        if(idx > 0.f) {
                            float reversePdfWrtArea =
                                    brdf.pdf(woSample.value, pBuffer[idx].incidentDirection) * max(0.f, dot(pBuffer[idx - 1].lastVertex.Ns, -pBuffer[idx].incidentDirection)) / sqr(pBuffer[idx].lastVertex.distance);
                            recursiveMISSequence = (reversePdfWrtArea / pBuffer[idx - 1].pdfLastVertex) * (1 + recursiveMISSequence);
                        }

                        recursiveMISFactor = (max(0.f, dot(pBuffer[idx].lastVertex.Ns, woSample.value)) / sqr(Itmp.distance)) * (1.f + recursiveMISSequence) / pBuffer[idx].pdfLastVertex;

                    }
                    if(pdfLastVertex == 0.f) {
                        break;
                    }
                    I = Itmp;
                } else {
                    break;
                }
            }
        } else {
            break;
        }
    }

    return pEmissionVertex;
}

void BidirRenderer::Li(uint32_t threadID, uint32_t pixelID, uint32_t sampleID,
                       uint32_t x, uint32_t y, Vec4f values[8]) const {
    for(auto i = 0u; i < 8; ++i) {
        values[i] += Vec4f(0.f, 0.f, 0.f, 1.f);
    }
    values[1] += Vec4f(0.f, 0.f, 0.f, 0.f);

    auto randomCount = getRandomCallCount(threadID);

    auto s2D = getPixelSample(threadID, sampleID);
    auto primRay = getPrimaryRay(x, y, s2D);

    auto pLightPath = m_PathArray.get() + threadID * (getMaxLightPathDepth() + getMaxEyePathDepth());
    auto pEyePath = pLightPath + getMaxLightPathDepth();

    auto recursiveMISSequence = 0.f;
    auto recursiveMISFactor = 0.f;

    {
        auto I = getScene().intersect(primRay);

        if(!I) {
            values[FINAL_RENDER] += Vec4f(badColor(), 1.f);
            return;
        }

        pEyePath[0].lastVertex = I;
        pEyePath[0].lastVertexBRDF = getScene().shade(pEyePath[0].lastVertex);
        pEyePath[0].incidentDirection = -primRay.dir.xyz();
        pEyePath[0].depth = 1;
        pEyePath[0].pdf = 1.f;
        pEyePath[0].pdfLastVertex = 1.f;
        pEyePath[0].power = Vec3f(1.f);
//        pEyePath[0].recursiveMISFactor = recursiveMISFactor;
    }

    uint32_t lightPathLength;
    auto lightNode = sampleLightPath(threadID, pixelID, pLightPath, lightPathLength);

    auto L = zero<Vec3f>();

    // trace an eye pathpEyePath[0].lastVertex
    for(auto i = 1u; i < m_nMaxDepth; ++i) {
        auto k = i - 1u; // index of vertex in pEyePath;

        for(auto j = 0u; j < lightPathLength; ++j) {
            auto lightPath = pLightPath + j;
            auto totalLength = i + lightPath->depth + 1;

            if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                Vec3f incidentDirection;
                float dist;
                auto G = geometricFactor(pEyePath[k].lastVertex, lightPath->lastVertex, incidentDirection, dist);
                Ray incidentRay(pEyePath[k].lastVertex, lightPath->lastVertex, incidentDirection, dist);

                if(G > 0.f) {
                    bool isVisible = !getScene().occluded(incidentRay);

                    if(isVisible) {
                        auto weight = computeMISWeight(threadID, pixelID, sampleID, &pEyePath[k], lightPath, lightNode.get());
    //                    auto weight2 = computeMISWeightRecursive(threadID, pixelID, sampleID, &pEyePath[k], lightPath);

    //                    if(weight != weight2) {
    //                        auto d = debugLock();
    //                        std::cerr << weight << ", " << weight2 << std::endl;
    //                    }

                        auto contrib = pEyePath[k].power * lightPath->power * lightPath->lastVertexBRDF.eval(lightPath->incidentDirection, -incidentDirection)
                                * G * pEyePath[k].lastVertexBRDF.eval(incidentDirection, pEyePath[k].incidentDirection);

                        L += weight * contrib;

                        values[totalLength] += Vec4f(weight * contrib, 0.f);
                        values[1] += Vec4f(1, 1, 1, 1);
                    } else {
                        values[1] += Vec4f(0, 0, 0, 1);
                    }
                }
            }
        }

        if(i >= 2u) {
            auto totalLength = i + 1;
            if(acceptPathDepth(totalLength) && totalLength <= m_nMaxDepth) {
                Ray shadowRay;
                auto G = lightNode->G(pEyePath[k].lastVertex, shadowRay);
                if(G > 0.f && !getScene().occluded(shadowRay)) {
                    auto power = lightNode->getRadiantExitance() / lightNode->pdf();
                    auto contrib = pEyePath[k].power * power * lightNode->Le(-shadowRay.dir)
                            * G * pEyePath[k].lastVertexBRDF.eval(shadowRay.dir, pEyePath[k].incidentDirection);
                    auto weight = computeMISWeight(threadID, pixelID, sampleID, &pEyePath[k], lightNode.get(), shadowRay.dir);

                    L += weight * contrib;

                    values[totalLength] += Vec4f(weight * contrib, 0.f);
                }
            }
        }

        if(i < m_nMaxDepth - 1) {
            Sample3f wiSample;
            auto fr = pEyePath[k].lastVertexBRDF.sample(pEyePath[k].incidentDirection, wiSample, getFloat2(threadID));

            if(wiSample.pdf > 0.f) {
                auto throughput = fr * max(0.f, dot(pEyePath[k].lastVertex.Ns, wiSample.value)) / wiSample.pdf;
                auto I = getScene().intersect(Ray(pEyePath[k].lastVertex, wiSample.value));
                if(!I) {
                    break;
                }

//                if(k > 1) {
//                    float reversePdfWrtArea =
//                            pEyePath[k].lastVertexBRDF.pdf(wiSample.value, pEyePath[k].incidentDirection) * max(0.f, dot(pEyePath[k - 1].lastVertex.Ns, -pEyePath[k].incidentDirection)) / sqr(pEyePath[k].lastVertex.distance);
//                    recursiveMISSequence = (reversePdfWrtArea / pEyePath[k - 1].pdfLastVertex) * (1 + recursiveMISSequence);
//                }
//                if(k > 0) {
//                    // At least 1 eye vertex
//                    recursiveMISFactor = (max(0.f, dot(pEyePath[k].lastVertex.Ns, wiSample.value)) / sqr(I.distance)) * (1.f + recursiveMISSequence) / pEyePath[k].pdfLastVertex;
//                }

                pEyePath[k + 1].lastVertex = I;
                pEyePath[k + 1].lastVertexBRDF = getScene().shade(pEyePath[k + 1].lastVertex);
                pEyePath[k + 1].incidentDirection = -wiSample.value;
                pEyePath[k + 1].depth = pEyePath[k].depth + 1;
                pEyePath[k + 1].pdfLastVertex = wiSample.pdf * max(0.f, dot(I.Ns, -wiSample.value)) / sqr(I.distance);
                pEyePath[k + 1].pdf = pEyePath[k].pdf * pEyePath[k + 1].pdfLastVertex;
                pEyePath[k + 1].power = pEyePath[k].power * throughput;
//                pEyePath[k + 1].recursiveMISFactor = recursiveMISFactor;

                if(pEyePath[k + 1].pdfLastVertex == 0.f) {
                    break;
                }

            } else {
                break;
            }
        }
    }

    if(std::isnan(luminance(L))) {
        auto sCount = getRandomCallCount(threadID) - randomCount;

        m_DebugFile << "BUG AGAIN IN BIDIR" << std::endl;
        m_DebugFile << "threadID = " << threadID << std::endl;
        m_DebugFile << "sampleID = " << sampleID << std::endl;
        m_DebugFile << "pixelID = " << pixelID << std::endl;
        m_DebugFile << "randomCount = " << randomCount << std::endl;
        m_DebugFile << "L = " << L << std::endl;

        setRandomSeed(threadID, getRandomSeed(threadID));
        discardRandom(threadID, randomCount);

        for(auto i = 0u; i < sCount; ++i) {
            m_DebugFile << getFloat(threadID) << ", ";
        }
        m_DebugFile << std::endl;

        return;
    }

    values[FINAL_RENDER] += Vec4f(L, 0.f);
}

void BidirRenderer::preprocess() {
    m_EmissionSampler.initFrame(getScene());

    m_PathArray = makeUniqueArray<PathVertex>(getThreadCount() * (getMaxLightPathDepth() + getMaxEyePathDepth()));
}

void BidirRenderer::processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
    auto spp = getSppCount();
    Vec4f L[8];

//    threadID = 17
//    sampleID = 0
//    pixelID = 71217
//    randomCount = 499816804
//    L = [     -nan,     -nan,     -nan]

//    if(threadID == 17) {
//        auto pixel = getPixel(71217, getFramebuffer().getSize());
//        discardRandom(threadID, 499816804);
//        Li(threadID, 71217, 0, pixel.x, pixel.y, L);
//    }

//    return;

    TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
        auto pixelID = getPixelIndex(x, y);
        std::fill(L, L + 8, Vec4f(0.f));
        for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
            Li(threadID, pixelID, sampleID, x, y, L);
        }
        for(auto i = 0u; i < 8; ++i) {
            getFramebuffer().accumulate(i, pixelID, L[i]);
        }
    });

    if(m_bDisplayProgress) {
        auto l = debugLock();
        std::cerr << "Tile " << tileID << " / " << getTileCount() << " (" << (tileID * 100.f / getTileCount()) << " %)" << std::endl;
    }
}

void BidirRenderer::doExposeIO(TwBar* bar) {
    atb::addVarRW(bar, ATB_VAR(m_nMaxDepth));
    atb::addVarRW(bar, ATB_VAR(m_bDisplayProgress));
    atb::addButton(bar, "clear", [this]() {
       m_DebugStream.clearObjects();
    });
}

void BidirRenderer::doLoadSettings(const tinyxml2::XMLElement& xml) {
    getAttribute(xml, "maxDepth", m_nMaxDepth);
    getAttribute(xml, "displayProgress", m_bDisplayProgress);
}

void BidirRenderer::doStoreSettings(tinyxml2::XMLElement& xml) const {
    setAttribute(xml, "maxDepth", m_nMaxDepth);
    setAttribute(xml, "displayProgress", m_bDisplayProgress);
}

}
