#pragma once

#include "SceneViewer.hpp"

#include <bonez/rendering/Renderer.hpp>
#include <bonez/sampling/multiple_importance_sampling.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/sampling/distribution1d.h>
#include <bonez/sampling/shapes.hpp>
#include <bonez/sampling/patterns.hpp>
#include <bonez/scene/lights/EmissionSampler.hpp>

namespace BnZ {


/*
 * Stratégie d'échantillonage:
 * Pour chaque noeud on tire N points visible sur lesquels on estime l'irradiance directe.
 * Au moment du rendu on echantillone un point parmis ceux associé au noeud le plus proche.
 */
class SkelRISPTNodeRepDepth3Renderer: public TileProcessingRenderer {
public:
    bool m_bRequireArrayRealloc = true;

    UpdateFlag m_Flag;
    float m_fMaxPSkel = 0.9f;
    float m_fMinPSkel = 0.1f;
    uint32_t m_nMaxNodeCount = 4;
    bool m_bUseSkelGridMapping = false;
    bool m_bUseSkelNodeNeighbours = false;

    bool m_bNeedPreprocess = true;
    EmissionSampler m_EmissionSampler;

    struct MappingData {
        std::vector<Intersection> intersections;
        std::vector<Vec4i> nodes;
        std::vector<Vec4f> weights;
        std::vector<Vec4f> representativities;
        std::vector<float> meanRepresentativities;

        void resizeBuffers(uint32_t size) {
            intersections.resize(size);
            nodes.resize(size);
            weights.resize(size);
            representativities.resize(size);
            meanRepresentativities.resize(size);
        }
    };

    virtual std::string getName() const {
        return "SkelRISPTNodeRepDepth3Renderer";
    }

    MappingData m_MappingData;

    uint32_t m_nNodeSamplingGridWidth = 32;
    Unique<Sample<Intersection>[]> m_SkelNodeIntersectionsArray; // For each node, store its m_nIntersectionCountPerNode intersections

    Unique<Vec3f[]> m_SkelNodeIntersectionsDirectRadianceArray; // For each node, store the irradiance of its intersections
    Unique<Vec3f[]> m_SkelNodeIntersectionsIncidentDirectionArray;
    Unique<Vec3f[]> m_SkelNodeIntersectionsRadiosityArray;
    Unique<float[]> m_RadiosityVarianceArray;

    Unique<float[]> m_SkelNodeDistributionsArray; // For each node, contains a discrete distribution among its intersections
    Unique<float[]> m_SkelNodePowerArray;
    uint32_t m_nDistributionSize;


    uint32_t m_nRISSampleCount = 8u;
    // Thread datas
    Unique<Sample3f[]> m_Wi;
    Unique<float[]> m_WiLength;
    Unique<Intersection[]> m_SampledI;
    Unique<Vec3f[]> m_EstimatedRadiosity;
    Unique<Vec3f[]> m_EstimatedRadiance;
    Unique<Vec3f[]> m_WiSample;
    Unique<Vec3f[]> m_Fr;
    Unique<uint32_t[]> m_SampledNode;
    Unique<Vec3f[]> m_Contributions;
    Unique<float[]> m_Weights;
    Unique<float[]> m_RISDistributions;


    uint32_t m_nRepresentativitySampleCount = 8u;
    bool m_bRequiredRepresentativiesCompute = true;

    uint32_t getPointSampleCountPerNode() const {
        return sqr(m_nNodeSamplingGridWidth);
    }

    void skeletonMapping(const Intersection& I, uint32_t pixelIdx, Vec4i& nearestNodes, Vec4f& weights) const {
        if(m_bUseSkelGridMapping) {
            nearestNodes = Vec4i(getScene().getSegmentedCurvSkeleton()->getNearestNode(I), -1, -1, -1);
            weights = Vec4f(1, 0, 0, 0);
        } else {
            nearestNodes = m_MappingData.nodes[pixelIdx];
            weights = m_MappingData.weights[pixelIdx];
        }
    }

    Vec3f estimateDirectRadiance(
            const SurfacePoint& point,
            float sLightIdx, float s1D, const Vec2f& s2D,
            Sample3f& wiSample) {
        auto lightNode = m_EmissionSampler.sample(getScene(), sLightIdx, s1D, s2D);
        Ray shadowRay;
        auto G = lightNode->G(point, shadowRay);

        if(G > 0.f && !getScene().occluded(shadowRay)) {
            auto L = lightNode->power() * lightNode->Le(-shadowRay.dir);
            return L * G / lightNode->pdf();
        }

        wiSample.pdf = 0.f;
        return zero<Vec3f>();

        //        RaySample r;
//        auto L = m_DirectLightSampler.sample(getScene(), point, r, sLightIdx, s1D, s2D);

//        wiSample.value = r.value.dir;

//        auto d = dot(point.Ns, r.value.dir);

//        if(d <= 0.f) {
//            wiSample.pdf = 0.f;
//        } else {
//            wiSample.pdf = r.pdf / d; // pdf wrt. projected solid angle on the point
//        }

//        if(getScene().occluded(r.value)) {
//            return zero<Vec3f>();
//        }

//        return L;
    }

    Vec3f sampleSkeleton(uint32_t threadID,
                         uint32_t pixelIdx,
                         const Intersection& I,
                         const Vec3f& wo, const BRDF& brdf,
                         Sample3f& wi,
                         float& wiLength,
                         Intersection& sampledI,
                         Vec3f& estimatedRadiosity,
                         Vec3f& estimatedRadiance,
                         Vec3f& wiSample,
                         uint32_t& sampledNode) const {
        Vec4i nearestNodes;
        Vec4f weights;
        skeletonMapping(I, pixelIdx, nearestNodes, weights);

        auto nodeSample = sampleNodeFromWeights(nearestNodes,
                                                weights,
                                                m_nMaxNodeCount,
                                                getFloat(threadID));


        if(nodeSample.pdf > 0.f) {
            auto nodeIdx = nearestNodes[nodeSample.value];

            if(nodeIdx >= 0) {
                // Choosing a neighbour ? let's try => it works better
                if(m_bUseSkelNodeNeighbours) {
                    auto pSkel = getScene().getSegmentedCurvSkeleton();
                    auto nodeCount = 1 + pSkel->neighbours(nodeIdx).size();
                    auto r1D = getFloat(threadID);
                    auto idx = min(uint32_t(nodeCount * r1D), uint32_t(nodeCount - 1));
                    if(idx > 0) {
                        nodeIdx = pSkel->neighbours(nodeIdx)[idx - 1];
                    }
                }

                sampledNode = nodeIdx;

                auto offset = nodeIdx * getPointSampleCountPerNode();
                auto distribOffset = nodeIdx * m_nDistributionSize;


                // Resampling of a point based on precomputed radiosities
                auto sample = sampleDiscreteDistribution1D(m_SkelNodeDistributionsArray.get() + distribOffset,
                                                           getPointSampleCountPerNode(),
                                                           getFloat(threadID));

                if(sample.pdf > 0.f) {
                    auto& tmp = m_SkelNodeIntersectionsArray[offset + sample.value];
                    sampledI = tmp.value;
                    estimatedRadiosity = m_SkelNodeIntersectionsRadiosityArray[offset + sample.value];
                    estimatedRadiance = m_SkelNodeIntersectionsDirectRadianceArray[offset + sample.value];
                    wiSample = m_SkelNodeIntersectionsIncidentDirectionArray[offset + sample.value];
                    wi.value = sampledI.P - I.P;
                    wiLength = length(wi.value);
                    wi.value /= wiLength;

                    auto fr = brdf.eval(wi.value, wo);

                    if(fr != zero<Vec3f>()) {
                        wi.pdf = getPointSampleCountPerNode() *
                                sample.pdf *
                                areaToSolidAnglePDF(tmp.pdf, sqr(wiLength), dot(sampledI.Ns, -wi.value));
                    }

                    return fr;
                }
            }
        }

        wi.pdf = 0.f;
        return zero<Vec3f>();
    }

    float pdfSkeleton(uint32_t threadID,
                      uint32_t pixelIdx,
                      const Intersection& I,
                      const Vec3f& estimatedRadiosity,
                      uint32_t nodeID,
                      bool doVisibilityTest) const {
        if(m_SkelNodePowerArray[nodeID] == 0.f) {
            return 0.f;
        }

        auto pSkel = getScene().getSegmentedCurvSkeleton();
        auto node = pSkel->getNode(nodeID);

        auto dir = node.P - I.P;
        auto l = length(dir);
        if(l == 0.f) {
            return 0.f;
        }
        dir /= l;

        // Perf or correctness
        if(m_nMaxNodeCount > 1 && doVisibilityTest && getScene().occluded(Ray(I, dir, l))) {
            return 0.f;
        }


        return luminance(estimatedRadiosity / m_SkelNodePowerArray[nodeID]) * solidAngleToAreaPDF(uniformSampleSpherePDF(), 1.f / (l * l), dot(dir, I.Ns));
    }

    float pdfSkeletonWithNeighbours(uint32_t threadID,
                                    uint32_t pixelIdx,
                                    const Intersection& I,
                                    const Vec3f& estimatedRadiosity,
                                    uint32_t nodeID,
                                    bool doVisibilityTest) const {
        if(!m_bUseSkelNodeNeighbours) {
            return pdfSkeleton(threadID, pixelIdx, I, estimatedRadiosity, nodeID, doVisibilityTest);
        }

        auto pSkel = getScene().getSegmentedCurvSkeleton();
        auto size = pSkel->neighbours(nodeID).size() + 1;
        auto w = 1.f / size;

        auto p = 0.f;
        p += pdfSkeleton(threadID, pixelIdx, I, estimatedRadiosity, nodeID, doVisibilityTest);
        for(auto& n: pSkel->neighbours(nodeID)) {
            p += pdfSkeleton(threadID, pixelIdx, I, estimatedRadiosity, n, doVisibilityTest);
        }
        return w * p;
    }

    float pdfSkeletonWrtArea(uint32_t threadID,
                             uint32_t pixelIdx,
                             const Intersection& I,
                             const Vec3f& estimatedRadiosity,
                             const Intersection& shadingI,
                             bool doVisibilityTest) const {
        auto start = getMicroseconds();
        Vec4i nearestNodes;
        Vec4f nodeWeight;
        skeletonMapping(shadingI, pixelIdx, nearestNodes, nodeWeight);

        // Here we should do that:
        auto size = 0u;
        Vec4f weights = zero<Vec4f>();
        for (size = 0u;
             size < min(m_nMaxNodeCount, 4u) && nearestNodes[size] >= 0;
             ++size) {
            weights[size] = nodeWeight[size];
        }

        if(size == 0u) {
            return 0.f;
        }

        DiscreteDistribution4f dist(weights);
        float p = 0.f;
        for(auto i = 0u; i < size; ++i) {
            float pdf = pdfSkeletonWithNeighbours(threadID, pixelIdx, I, estimatedRadiosity, nearestNodes[i], doVisibilityTest);
            if(pdf > 0.f) {
                p += dist.pdf(i) * pdf;
            }
        }
        return p;
    }

    // Return the pdf of sampling the direction (shadingI -> I) with the skeleton
    float pdfSkeleton(uint32_t threadID,
                      uint32_t pixelIdx,
                      const Intersection& I,
                      const Vec3f& estimatedRadiosity,
                      const Intersection& shadingI,
                      bool doVisibilityTest) const {
        auto pdfWrtArea = pdfSkeletonWrtArea(threadID, pixelIdx, I, estimatedRadiosity, shadingI, doVisibilityTest);

        auto dir = shadingI.P - I.P;
        auto l = length(dir);

        if(l == 0.f) {
            return 0.f;
        }
        dir /= l;

        return areaToSolidAnglePDF(pdfWrtArea, l * l, dot(dir, I.Ns));
    }

    enum RenderTarget {
        FINAL_RENDER = 0,
        SKEL_CONTRIBUTION = 1,
        BRDF_CONTRIBUTION = 2,
        SKEL_CHOSEN = 3,
        BRDF_CHOSEN = 4,
        RADIOSITY_VARIANCE = 5,
        REPRESENTATIVITY = 6,
        CHOSEN_NODE = 7
    };

    void Li_one_sample_strategy(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx, Vec4f values[]) const {
        auto I = getScene().intersect(primRay);

        if(!I) {
            values[FINAL_RENDER] += Vec4f(badColor(), 1.f);
            return;
        }

        auto brdf = getScene().shade(I);
        auto wo = -primRay.dir.xyz();

        auto L = zero<Vec3f>();

        {
            auto s1D = getFloat(threadID);

            //auto pSkel = m_fPSkel;
            auto pSkel = min(m_fMaxPSkel, max(m_fMinPSkel, m_MappingData.meanRepresentativities[pixelIdx]));

            // Sample the skeleton
            if(s1D <= pSkel)
            {
                values[SKEL_CHOSEN] += Vec4f(1, 1, 1, 1);
                values[BRDF_CHOSEN] += Vec4f(0, 0, 0, 1);

                float weightsMean = 0.f;

                auto offset = threadID * m_nRISSampleCount;

                for(auto i = offset, end = offset + m_nRISSampleCount; i < end; ++i) {
                    m_Fr[i] = sampleSkeleton(threadID, pixelIdx, I, wo, brdf,
                                        m_Wi[i],
                                        m_WiLength[i],
                                        m_SampledI[i],
                                        m_EstimatedRadiosity[i],
                                        m_EstimatedRadiance[i],
                                        m_WiSample[i],
                                        m_SampledNode[i]);
                    bool isValid = (m_Fr[i] != Vec3f(0.f) &&
                            m_Wi[i].pdf != 0.f && dot(I.Ns, m_Wi[i].value) > 0.f);
                    if(isValid) {
                        auto brdfSampleI = getScene().shade(m_SampledI[i]);
                        auto Li = brdfSampleI.eval(m_WiSample[i], -m_Wi[i].value) * m_EstimatedRadiance[i];
                        m_Contributions[i] = m_Fr[i] * Li * max(0.f, dot(I.Ns.xyz(), m_Wi[i].value))
                                / m_Wi[i].pdf;
                        m_Weights[i] = luminance(m_Contributions[i]);
                    } else {
                        m_Contributions[i] = Vec3f(0.f);
                        m_Weights[i] = 0.f;
                    }
                    weightsMean += m_Weights[i];
                }
                weightsMean /= m_nRISSampleCount;

                auto pDistrib = m_RISDistributions.get() + threadID * getDistribution1DBufferSize(m_nRISSampleCount);
                buildDistribution1D([this, offset](uint32_t i) {
                    return m_Weights[offset + i];
                }, pDistrib, m_nRISSampleCount);

                auto sample = sampleDiscreteDistribution1D(pDistrib, m_nRISSampleCount, getFloat(threadID));

                if(weightsMean > 0.f && sample.pdf > 0.f) {
                    auto idx = offset + sample.value;

                    values[RADIOSITY_VARIANCE] += Vec4f(Vec3f(m_RadiosityVarianceArray[m_MappingData.nodes[pixelIdx][0]]), 1);

                    values[CHOSEN_NODE] += Vec4f(getColor(m_SampledNode[idx]), 1.f);
                    values[REPRESENTATIVITY] += Vec4f(Vec3f(m_MappingData.meanRepresentativities[pixelIdx]), 1.f);

                    if(getScene().occluded(Ray(I, m_SampledI[idx], m_Wi[idx].value, m_WiLength[idx]))) {
                        values[FINAL_RENDER] += Vec4f(Vec3f(0.f), 1.f);
                        //values[SKEL_COHERENCY] += Vec4f(0, 0, 0, 1);
                    } else {
                        //values[SKEL_COHERENCY] += Vec4f(1, 1, 1, 1);

                        float pdfWrtSkel = pdfSkeleton(threadID,
                                                 pixelIdx,
                                                 m_SampledI[idx],
                                                 m_EstimatedRadiosity[idx],
                                                 I,
                                                 true);

                        float w = pSkel * pdfWrtSkel / (pSkel * pdfWrtSkel + (1 - pSkel) * brdf.pdf(wo, m_Wi[idx].value));

                        auto Ltmp = m_Contributions[idx] / (m_nRISSampleCount * pSkel * sample.pdf);

                        L += w * Ltmp;
                        values[1] += Vec4f(Ltmp, 1.f);
                        values[FINAL_RENDER] += Vec4f(L, 1.f);
                    }
                } else {
                    values[FINAL_RENDER] += Vec4f(Vec3f(0.f), 1.f);
                }
            } else
            // Sample the brdf
            {
                values[BRDF_CHOSEN] += Vec4f(1, 1, 1, 1);
                values[SKEL_CHOSEN] += Vec4f(0, 0, 0, 1);

                Sample3f wiSample;
                auto fr = brdf.sample(wo, wiSample, getFloat2(threadID));

                if(wiSample.pdf > 0.f) {
                    auto Ireflect = getScene().intersect(Ray(I, wiSample.value));
                    if(Ireflect) {
                        Vec3f irradiance;
                        auto brdfReflectI = getScene().shade(Ireflect);
                        auto Li = estimateDirectLightAndDirectIrradiance(getScene(),
                                                      m_EmissionSampler,
                                                      Ireflect, -wiSample.value,
                                                      brdfReflectI,
                                                      irradiance,
                                                      getFloat(threadID),
                                                      getFloat(threadID),
                                                      getFloat2(threadID));

                        float pdfWrtSkel = pdfSkeleton(threadID,
                                                       pixelIdx,
                                                       Ireflect,
                                                       brdfReflectI.diffuseTerm() * irradiance,
                                                       I,
                                                       true);

                        float w = (1 - pSkel) * wiSample.pdf / ((1 - pSkel) * wiSample.pdf + pSkel * pdfWrtSkel);

                        auto Ltmp = fr * Li * max(0.f, dot(I.Ns.xyz(), wiSample.value))
                                / ((1 - pSkel)  * wiSample.pdf);

                        L += w * Ltmp;
                        values[2] += Vec4f(Ltmp, 1.f);
                        values[FINAL_RENDER] += Vec4f(L, 1.f);
                    }
                }
            }
        }
    }

    void Li(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx, Vec4f values[]) const {
        Li_one_sample_strategy(primRay, threadID, pixelIdx, values);
    }

    void computeSkelNodeIntersections() {
        auto pSkel = getScene().getSegmentedCurvSkeleton();

        // For each node, compute intersections, irradiance values and distributions
        launchThreads([pSkel, this](uint32_t threadID) {
            uint32_t i = 0u;

            while(true) {
                auto nodeIndex = i * getThreadCount() + threadID;
                i++;

                if(nodeIndex >= pSkel->size()) {
                    break;
                }

                JitteredDistribution2D jitter(m_nNodeSamplingGridWidth, m_nNodeSamplingGridWidth);

                auto nodePos = pSkel->getNode(nodeIndex).P;
                auto offset = nodeIndex * getPointSampleCountPerNode();
                auto distribOffset = nodeIndex * m_nDistributionSize;

                m_SkelNodePowerArray[nodeIndex] = 0.f;
                m_RadiosityVarianceArray[nodeIndex] = 0.f;
                float maxRad = 0.f;
                for(auto i = 0u, count = getPointSampleCountPerNode(); i < count; ++i) {
                    auto uv = jitter.getSample(i, getFloat2(threadID));
                    auto dir = uniformSampleSphere(uv.x, uv.y);

                    auto& I = m_SkelNodeIntersectionsArray[offset + i].value = getScene().intersect(Ray(nodePos, dir.value));
                    if(I) {
                        auto V = nodePos - I.P;
                        auto rcpSqrDist = 1.f / dot(V, V);
                        auto normalDotProduct = dot(I.Ns, -dir.value);
                        auto pdfWrtArea = m_SkelNodeIntersectionsArray[offset + i].pdf = solidAngleToAreaPDF(dir.pdf, rcpSqrDist, normalDotProduct);

                        auto brdf = getScene().shade(I);

                        Sample3f wiSample;
                        auto Li = estimateDirectRadiance(I,
                                                         getFloat(threadID),
                                                         getFloat(threadID),
                                                         getFloat2(threadID),
                                                         wiSample);

                        if(wiSample.pdf > 0.f && Li != zero<Vec3f>()) {
                            m_SkelNodeIntersectionsDirectRadianceArray[offset + i] = Li / wiSample.pdf; // Monte-Carlo estimation
                            m_SkelNodeIntersectionsIncidentDirectionArray[offset + i] = wiSample.value;

                            Sample3f woSample;

                            auto fr = brdf.sample(wiSample.value, woSample, getFloat2(threadID));

                            if(woSample.pdf > 0.f && fr != zero<Vec3f>()) {
                                m_SkelNodeIntersectionsRadiosityArray[offset + i] = fr * m_SkelNodeIntersectionsDirectRadianceArray[offset + i] * max(0.f, dot(I.Ns, woSample.value)) / woSample.pdf;
                            } else {
                                m_SkelNodeIntersectionsRadiosityArray[offset + i] = zero<Vec3f>();
                            }
                        } else {
                            m_SkelNodeIntersectionsDirectRadianceArray[offset + i] = zero<Vec3f>();
                            m_SkelNodeIntersectionsRadiosityArray[offset + i] = zero<Vec3f>();
                        }

                        auto radLuminance = luminance(m_SkelNodeIntersectionsRadiosityArray[offset + i]);

                        maxRad = max(maxRad, radLuminance);

                        m_SkelNodePowerArray[nodeIndex] += radLuminance;
                        m_RadiosityVarianceArray[nodeIndex] += radLuminance * radLuminance;
                    } else {
                        m_SkelNodeIntersectionsDirectRadianceArray[offset + i] = zero<Vec3f>();
                        m_SkelNodeIntersectionsRadiosityArray[offset + i] = zero<Vec3f>();
                    }
                }

                m_SkelNodePowerArray[nodeIndex] /= float(getPointSampleCountPerNode());

                // Moyenne des carré - carré de la moyenne
                m_RadiosityVarianceArray[nodeIndex] /= float(getPointSampleCountPerNode());
                m_RadiosityVarianceArray[nodeIndex] -= m_SkelNodePowerArray[nodeIndex] * m_SkelNodePowerArray[nodeIndex];

                m_RadiosityVarianceArray[nodeIndex] /= maxRad * maxRad; // normalization

                buildDistribution1D([this, offset](uint32_t i) {
                    return luminance(m_SkelNodeIntersectionsRadiosityArray[offset + i]/* / m_SkelNodeIntersectionsArray[offset + i].pdf*/);
                }, m_SkelNodeDistributionsArray.get() + distribOffset, getPointSampleCountPerNode());
            }
        });
    }

    float computeRepresentativity(int32_t nodeIdx, const Intersection& I, uint32_t threadID) {
        float sum = 0.f;

        auto count = getPointSampleCountPerNode();
        auto offset = nodeIdx * count;

        for(auto i = 0u; i < m_nRepresentativitySampleCount; ++i) {
            auto s1D = getFloat(threadID);
            uint32_t sample = min(uint32_t(s1D * count), count - 1);

            const auto& Q = m_SkelNodeIntersectionsArray[offset + sample].value;
            auto dir = I.P - Q.P;
            auto l = length(dir);
            dir /= l;
            auto pdfWrtSA = areaToSolidAnglePDF(m_SkelNodeIntersectionsArray[offset + sample].pdf, l * l, dot(dir, Q.Ns));
            auto d = max(0.f, dot(I.Ns, -dir));

            if(d > 0.f && pdfWrtSA > 0.f && m_SkelNodeIntersectionsArray[offset + sample].pdf > 0.f && !getScene().occluded(Ray(Q, I, dir, l))) {
                sum += d / pdfWrtSA;
            }
        }

        sum /= pi<float>() * float(m_nRepresentativitySampleCount);
        return sum;
    }

    void computeRepresentativities() {
        std::cerr << "Compute representativities..." << std::endl;
        computeSkelNodeIntersections();

        auto framebufferSize = getFramebuffer().getSize();
        Vec2u tileCount = framebufferSize / getTileSize() +
                Vec2u(framebufferSize % getTileSize() != zero<Vec2u>());

        auto totalCount = tileCount.x * tileCount.y;

        auto task = [&](uint32_t threadID) {
            auto loopID = 0u;
            while(true) {
                auto tileID = loopID * getThreadCount() + threadID;
                ++loopID;

                if(tileID >= totalCount) {
                    return;
                }

                uint32_t tileX = tileID % tileCount.x;
                uint32_t tileY = tileID / tileCount.x;

                Vec2u tileOrg = Vec2u(tileX, tileY) * getTileSize();
                auto viewport = Vec4u(tileOrg, getTileSize());

                if(viewport.x + viewport.z > framebufferSize.x) {
                    viewport.z = framebufferSize.x - viewport.x;
                }

                if(viewport.y + viewport.w > framebufferSize.y) {
                    viewport.w = framebufferSize.y - viewport.y;
                }

                auto xEnd = viewport.x + viewport.z;
                auto yEnd = viewport.y + viewport.w;

                for(auto y = viewport.y; y < yEnd; ++y) {
                    for(auto x = viewport.x; x < xEnd; ++x) {
                        auto taskID = getPixelIndex(x, y);

                        if(m_MappingData.intersections[taskID]) {
                            auto nodeIdx = m_MappingData.nodes[taskID];
                            m_MappingData.meanRepresentativities[taskID] = 0.f;
                            float sumWeights = 0.f;
                            for(auto i = 0u; i < max(m_nMaxNodeCount, 4u) && nodeIdx[i] > 0; ++i) {
                                auto rep = computeRepresentativity(nodeIdx[i],
                                                                   m_MappingData.intersections[taskID],
                                                                   threadID);

                                m_MappingData.meanRepresentativities[taskID] += rep * m_MappingData.weights[taskID][i];
                                sumWeights += m_MappingData.weights[taskID][i];

                                m_MappingData.weights[taskID][i] = rep * m_MappingData.weights[taskID][i];
                            }
                            m_MappingData.meanRepresentativities[taskID] /= sumWeights;
                        } else {
                            m_MappingData.representativities[taskID] = Vec4f(0.f);
                        }
                    }
                }
            }
        };

        launchThreads(task);

        std::cerr << "Done." << std::endl;
    }

    void computeSkeletonMapping() {
        auto framebufferSize = getFramebuffer().getSize();
        m_MappingData.resizeBuffers(framebufferSize.x * framebufferSize.y);

        Vec2u tileCount = framebufferSize / getTileSize() +
                Vec2u(framebufferSize % getTileSize() != zero<Vec2u>());

        auto totalCount = tileCount.x * tileCount.y;

        auto pSkel = getScene().getSegmentedCurvSkeleton();

        auto task = [&](uint32_t threadID) {
            auto loopID = 0u;
            while(true) {
                auto tileID = loopID * getThreadCount() + threadID;
                ++loopID;

                if(tileID >= totalCount) {
                    return;
                }

                uint32_t tileX = tileID % tileCount.x;
                uint32_t tileY = tileID / tileCount.x;

                Vec2u tileOrg = Vec2u(tileX, tileY) * getTileSize();
                auto viewport = Vec4u(tileOrg, getTileSize());

                if(viewport.x + viewport.z > framebufferSize.x) {
                    viewport.z = framebufferSize.x - viewport.x;
                }

                if(viewport.y + viewport.w > framebufferSize.y) {
                    viewport.w = framebufferSize.y - viewport.y;
                }

                auto xEnd = viewport.x + viewport.z;
                auto yEnd = viewport.y + viewport.w;

                for(auto y = viewport.y; y < yEnd; ++y) {
                    for(auto x = viewport.x; x < xEnd; ++x) {
                        auto pixelIdx = getPixelIndex(x, y);
                        auto s2D = Vec2f(0.5f, 0.5f);
                        auto ray = getPrimaryRay(x, y, s2D);
                        auto I = getScene().intersect(ray);
                        if(I) {
                            m_MappingData.intersections[pixelIdx] = I;
                            m_MappingData.nodes[pixelIdx] = getNearestNodes(*pSkel, I.P, I.Ns, m_MappingData.weights[pixelIdx],
                                                [](uint32_t nodeIdx, Vec3f dir, float dist) { return false; });
                            m_MappingData.representativities[pixelIdx] = Vec4f(0.f);
                        } else {
                            m_MappingData.nodes[pixelIdx] = Vec4i(-1);
                            m_MappingData.weights[pixelIdx] = Vec4f(0.f);
                            m_MappingData.representativities[pixelIdx] = Vec4f(0.f);
                        }
                    }
                }
            }
        };

        launchThreads(task);
    }

    void allocArrays() {
        if(m_bRequireArrayRealloc) {
            // Alloc memory
            auto size = m_nRISSampleCount * getThreadCount();
            m_Wi = makeUniqueArray<Sample3f>(size);
            m_WiLength = makeUniqueArray<float>(size);
            m_SampledI = makeUniqueArray<Intersection>(size);
            m_EstimatedRadiosity = makeUniqueArray<Vec3f>(size);
            m_EstimatedRadiance = makeUniqueArray<Vec3f>(size);
            m_WiSample = makeUniqueArray<Vec3f>(size);
            m_Fr = makeUniqueArray<Vec3f>(size);
            m_SampledNode = makeUniqueArray<uint32_t>(size);
            m_Contributions = makeUniqueArray<Vec3f>(size);
            m_Weights = makeUniqueArray<float>(size);
            m_RISDistributions = makeUniqueArray<float>(getDistribution1DBufferSize(m_nRISSampleCount) * getThreadCount());

            auto pSkel = getScene().getSegmentedCurvSkeleton();
            auto completeSize = getPointSampleCountPerNode() * pSkel->size();
            m_SkelNodeIntersectionsArray = makeUniqueArray<Sample<Intersection>>(completeSize);
            m_SkelNodeIntersectionsDirectRadianceArray = makeUniqueArray<Vec3f>(completeSize);
            m_SkelNodeIntersectionsIncidentDirectionArray = makeUniqueArray<Vec3f>(completeSize);
            m_SkelNodeIntersectionsRadiosityArray = makeUniqueArray<Vec3f>(completeSize);
            m_nDistributionSize = getDistribution1DBufferSize(getPointSampleCountPerNode());
            m_SkelNodeDistributionsArray = makeUniqueArray<float>(m_nDistributionSize * pSkel->size());
            m_SkelNodePowerArray = makeUniqueArray<float>(pSkel->size());
            m_RadiosityVarianceArray = makeUniqueArray<float>(pSkel->size());

            m_bRequireArrayRealloc = false;
        }
    }

    void preprocess() override {
        allocArrays();

        if(getScene().getLightContainer().hasChanged(m_Flag)) {
            m_bNeedPreprocess = true;
        }

        m_EmissionSampler.initFrame(getScene());

        if(!m_bUseSkelGridMapping) {
            computeSkeletonMapping();
        }
    }

    void beginFrame() override {
        allocArrays();

        if(m_bRequiredRepresentativiesCompute) {
            computeRepresentativities();
            m_bRequiredRepresentativiesCompute = false;
        }
        computeSkelNodeIntersections();
    }

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override {
        auto spp = getSppCount();
        TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
            Vec4f L[8];
            std::fill(L, L + 8, Vec4f(0.f));
            auto pixelIdx = getPixelIndex(x, y);
            for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
                auto s2D = getPixelSample(threadID, sampleID);
                auto ray = getPrimaryRay(x, y, s2D);
                Li(ray, threadID, pixelIdx, L);
            }
            for(auto i = 0u; i < 8; ++i) {
                getFramebuffer().accumulate(i, pixelIdx, L[i]);
            }
        });
    }

    virtual void doExposeIO(TwBar* bar) {
        auto allocCallback = [this]() {
            m_bRequireArrayRealloc = true;
        };

        atb::addVarRW(bar, ATB_VAR(m_fMaxPSkel));
        atb::addVarRW(bar, ATB_VAR(m_fMinPSkel));
        atb::addVarRW(bar, ATB_VAR(m_nMaxNodeCount));
        atb::addVarRW(bar, ATB_VAR(m_bUseSkelGridMapping));
        atb::addVarRW(bar, ATB_VAR(m_bUseSkelNodeNeighbours));
        atb::addVarRWCB(bar, ATB_VAR(m_nNodeSamplingGridWidth), allocCallback);
        atb::addVarRWCB(bar, ATB_VAR(m_nRISSampleCount), allocCallback);

        auto repCallback = [this]() {
            m_bRequiredRepresentativiesCompute = true;
        };

        atb::addVarRWCB(bar, ATB_VAR(m_nRepresentativitySampleCount), repCallback);
    }

    virtual void doLoadSettings(const tinyxml2::XMLElement& xml) {
        getAttribute(xml, "maxPSkel", m_fMaxPSkel);
        getAttribute(xml, "minPSkel", m_fMinPSkel);
        getAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
        getAttribute(xml, "useGridSkelMapping", m_bUseSkelGridMapping);
        getAttribute(xml, "useSkelNodeNeighbours", m_bUseSkelNodeNeighbours);
        getAttribute(xml, "nodeSamplingGridWidth", m_nNodeSamplingGridWidth);
        getAttribute(xml, "risSampleCount", m_nRISSampleCount);
        getAttribute(xml, "representativitySampleCount", m_nRepresentativitySampleCount);
    }

    virtual void doStoreSettings(tinyxml2::XMLElement& xml) const {
        setAttribute(xml, "maxPSkel", m_fMaxPSkel);
        setAttribute(xml, "minPSkel", m_fMinPSkel);
        setAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
        setAttribute(xml, "useGridSkelMapping", m_bUseSkelGridMapping);
        setAttribute(xml, "useSkelNodeNeighbours", m_bUseSkelNodeNeighbours);
        setAttribute(xml, "nodeSamplingGridWidth", m_nNodeSamplingGridWidth);
        setAttribute(xml, "representativitySampleCount", m_nRepresentativitySampleCount);
    }
};

}
