#include <iostream>

#include "SceneViewer.hpp"

namespace BnZ {

int main(int argc, char** argv) {
    if(argc != 2) {
        std::cerr << "Usage: " << argv[0] << " < XML configuration filepath >" << std::endl;
        return -1;
    }

    try {
        SceneViewer viewer(argv[1]);
        viewer.run();
    }
    catch (const std::exception& e) {
        std::cerr << "EXCEPTION: " << e.what() << std::endl;
        throw e;
    }

	return 0;
}

}

int main(int argc, char** argv) {
#ifdef _DEBUG
        std::cerr << "DEBUG MODE ACTIVATED" << std::endl;
#endif

#ifdef DEBUG
        std::cerr << "DEBUG MODE ACTIVATED" << std::endl;
#endif
    return BnZ::main(argc, argv);
}
