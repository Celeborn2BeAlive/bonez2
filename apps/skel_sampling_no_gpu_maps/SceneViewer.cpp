#include "SceneViewer.hpp"

#include "bonez/sys/time.hpp"

#include "renderers/NodeSkelMappingRenderer.hpp"
#include "renderers/SkelBidirDepth3Renderer.hpp"
#include "renderers/SkelBidirDepth3NoMISRenderer.hpp"
#include "renderers/SkelNodeRepPTDepth3Renderer.hpp"
#include "renderers/SkelPTDepth3Renderer.hpp"
#include "renderers/SkelRISAllPointsPT.hpp"
#include "renderers/SkelRISPTDepth3Renderer.hpp"
#include "renderers/SkelRISPTNodeRepDepth3Renderer.hpp"
#include "renderers/SkelBidirRenderer.hpp"
#include "renderers/SkelBidirNoMISRenderer.hpp"
#include "renderers/SkelBidirLightnodeRenderer.hpp"
#include "renderers/BidirRenderer.hpp"
#include "renderers/SkelBidirLengthBucketingRenderer.hpp"
#include "renderers/SkelBidirLengthBucketingNoAlphaRenderer.hpp"
#include "renderers/SkelBidirLengthBucketingOneDistribRenderer.hpp"
#include "renderers/SkelBidirLengthBucketingNoAlphaOneDistribRenderer.hpp"
#include "renderers/SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer.hpp"
#include "renderers/SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer.hpp"
#include "renderers/SkelBidirLengthBucketingOneDistribRenderer_WithNodeStats.hpp"
#include "renderers/SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer_WithNodeStats.hpp"

namespace BnZ {

void SceneViewer::exposeIO() {
    TwBar* bar = TwNewBar("SceneViewer");
    TwDefine(" SceneViewer size='240 640' ");

    atb::addVarRW(bar, ATB_VAR(m_bUseGridSkelMapping));
}


void SceneViewer::addRenderers() {
    m_RenderModule.m_RendererManager.addRenderer("NodeSkelMappingRenderer", [this]() {
        return makeUnique<NodeSkelMappingRenderer>([this](const SurfacePoint& point, Vec4f& weights) {
            return getNearestNodes(point, weights);
        });
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelBidirDepth3Renderer", [this]() {
        return makeUnique<SkelBidirDepth3Renderer>(m_pGLDebugRenderer);
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelBidirDepth3NoMISRenderer", [this]() {
        return makeUnique<SkelBidirDepth3NoMISRenderer>(m_pGLDebugRenderer);
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelNodeRepPTDepth3Renderer", [this]() {
        return makeUnique<SkelNodeRepPTDepth3Renderer>();
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelPTDepth3Renderer", [this]() {
        return makeUnique<SkelPTDepth3Renderer>();
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelRISAllPointsPT", [this]() {
        return makeUnique<SkelRISAllPointsPT>();
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelRISPTDepth3Renderer", [this]() {
        return makeUnique<SkelRISPTDepth3Renderer>();
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelRISPTNodeRepDepth3Renderer", [this]() {
        return makeUnique<SkelRISPTNodeRepDepth3Renderer>();
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelBidirRenderer", [this]() {
        return makeUnique<SkelBidirRenderer>(m_pGLDebugRenderer);
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelBidirNoMISRenderer", [this]() {
        return makeUnique<SkelBidirRenderer>(m_pGLDebugRenderer);
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelBidirLightnodeRenderer", [this]() {
        return makeUnique<SkelBidirLightnodeRenderer>(m_pGLDebugRenderer);
    });

    m_RenderModule.m_RendererManager.addRenderer("BidirRenderer", [this]() {
        return makeUnique<BidirRenderer>(m_pGLDebugRenderer);
    });


    m_RenderModule.m_RendererManager.addRenderer("SkelBidirLengthBucketingRenderer", [this]() {
        return makeUnique<SkelBidirLengthBucketingRenderer>(m_pGLDebugRenderer);
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelBidirLengthBucketingNoAlphaRenderer", [this]() {
        return makeUnique<SkelBidirLengthBucketingNoAlphaRenderer>(m_pGLDebugRenderer);
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelBidirLengthBucketingOneDistribRenderer", [this]() {
        return makeUnique<SkelBidirLengthBucketingOneDistribRenderer>(m_pGLDebugRenderer);
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelBidirLengthBucketingNoAlphaOneDistribRenderer", [this]() {
        return makeUnique<SkelBidirLengthBucketingNoAlphaOneDistribRenderer>(m_pGLDebugRenderer);
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer", [this]() {
        return makeUnique<SkelBidirLengthBucketingNoAlphaOneDistribUniformSamplingRenderer>(m_pGLDebugRenderer);
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer", [this]() {
        return makeUnique<SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer>(m_pGLDebugRenderer);
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelBidirLengthBucketingOneDistribRenderer_WithNodeStats", [this]() {
        return makeUnique<SkelBidirLengthBucketingOneDistribRenderer_WithNodeStats>(m_pGLDebugRenderer);
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer_WithNodeStats", [this]() {
        return makeUnique<SkelBidirLengthBucketingOneDistribNoMaxballWeightsRenderer_WithNodeStats>(m_pGLDebugRenderer);
    });
}

void SceneViewer::drawFrame() {
    auto debugStream = newGLDebugStream(m_pGLDebugRenderer, "default");

    m_GBufferRenderPass.render(m_Camera.getProjMatrix(), m_Camera.getViewMatrix(),
                               m_ZNearFar.y, *m_pGLScene, m_GBuffer);

    m_GLFramebuffer.bindForDrawing();
    glClear(GL_COLOR_BUFFER_BIT);

    if(m_RenderModule.m_bCPURendering) {
        m_RenderModule.doCPURendering(m_CameraUpdateFlag, m_Camera, *m_pScene, m_GLImageRenderer);
    } else {
        m_FlatShadingPass.render(m_GBuffer, m_Camera.getRcpProjMatrix(),
                                 m_Camera.getViewMatrix(), m_ScreenTriangle);

        auto objectID = m_SelectedObjectID;
        int selectedNode = -1;
        if(objectID.x == SEG_SKEL_NODE_ID) {
            selectedNode = objectID.y;
        }
    }

    drawCurvSkel(debugStream);
    doPicking(debugStream);

    m_SkelNodeDebugModule.drawMaxball(debugStream);

    if(m_PickedIntersection) {
        Vec4i nodeIndex;
        Vec4f nodeWeights;
        //getNearestNodes(1, &m_PickedIntersection, &nodeIndex, &nodeWeights);
        nodeIndex = getNearestNodes(m_PickedIntersection, nodeWeights);

        Vec3f colors[] = {
            Vec3f(1, 0, 0),
            Vec3f(0, 1, 0),
            Vec3f(0, 0, 1),
            Vec3f(1, 1, 1)
        };

        for(int i = 0; i < 4; ++i) {
            if(nodeIndex[i] >= 0) {
                m_pGLDebugRenderer->addLine(
                            "default",
                            m_PickedIntersection.P.xyz(),
                            m_pSkel->getNode(nodeIndex[i]).P,
                            colors[i],
                            colors[i],
                            5);
            }
        }
    }

    m_pGLDebugRenderer->render(m_GBuffer, m_Camera);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glClear(GL_COLOR_BUFFER_BIT);

    drawGBuffer();
    drawFinalRender();
    drawSideView();

    if(m_bDoScreenshot) {
        doScreenshot();
        m_bDoScreenshot = false;
    }

}

void SceneViewer::onIntersectionPicked() {
    auto& I = m_PickedIntersection;
}

void SceneViewer::onObjectPicked() {
    if(m_SelectedObjectID.x != SEG_SKEL_NODE_ID) {
        m_nSelectedNodeIdx = UNDEFINED_NODE;
        return;
    }

    m_nSelectedNodeIdx = m_SelectedObjectID.y;
    auto node = m_pSkel->getNode(m_nSelectedNodeIdx);
}

void SceneViewer::drawSideView() {

}

}
