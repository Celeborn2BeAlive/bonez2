#pragma once

#include "SkelSampler.hpp"

#include <bonez/common.hpp>
#include <bonez/types.hpp>
#include <bonez/scene/topology/Skeleton.hpp>
#include <bonez/sys/memory.hpp>
#include <bonez/sys/threads.hpp>
#include <bonez/sampling/distribution2d.h>
#include <bonez/sampling/shapes.hpp>

namespace BnZ {

class SceneViewer;

class SkelSphericalMapContainer: public SkelSampler {
public:
    virtual void loadSettings(const tinyxml2::XMLElement& xml) override {
        getAttribute(xml, "mapHeight", m_nMapHeightSetting);
    }

    virtual void storeSettings(tinyxml2::XMLElement& xml) const override {
        setAttribute(xml, "mapHeight", m_MapSize.y);
    }

    virtual void exposeIO(TwBar* bar) override {
        atb::addVarRW(bar, ATB_VAR(m_nMapHeightSetting));
        atb::addButton(bar, "Force Preprocess", [this]() {
           preprocess(nullptr);
        });
    }

    void alloc(uint32_t maxWidth, uint32_t maxHeight, uint32_t maxCount) {
        auto distributionsBufferMaxSize = maxCount * getDistribution2DBufferSize(maxWidth, maxHeight);
        m_DistributionsBuffer.reset(new float[distributionsBufferMaxSize]);

        auto maxMapsBufferMaxSize = maxCount * maxWidth * maxHeight;

        m_PositionMapsBuffer.reset(new Vec3f[maxMapsBufferMaxSize]);
        m_NormalDistMapsBuffer.reset(new Vec4f[maxMapsBufferMaxSize]);
        m_ImportanceMapsBuffer.reset(new Vec4f[maxMapsBufferMaxSize]);
    }

    void clear() {
        m_pSkel = nullptr;
        m_nDistributionSize = 0u;
        m_MapSize = Vec2u(0u);
        m_nMapSize = 0u;
        m_nMapCount = 0u;
    }

    Vec4f* getNormalDistMapsBufferPtr() {
        return m_NormalDistMapsBuffer.get();
    }

    Vec4f* getImportanceMapsBufferPtr() {
        return m_ImportanceMapsBuffer.get();
    }

    SkelSphericalMapContainer(SceneViewer& viewer):
        m_Viewer(viewer) {
    }

    // Preprocess data from the content of the normal-distance maps buffer and the importance maps buffer
    void preprocess(tinyxml2::XMLElement* pStatistics) override;

    // Sample a surface point from the distribution of a node
    // Return the value of the importance function for the sampled point
    Vec3f sampleSurfacePoint(
            uint32_t nodeIdx, // Index of the node to sample
            Vec3f& P, // Return the sampled position
            Vec3f& N, // Return the sampled normal
            float& pdfWrtSolidAngle, // Return the pdf of the sampled surface point wrt. node solid angle
            float& pdfWrtArea, // Return the pdf of the sampled surface point, wrt. area
            const Vec2f& s2D // Random uniform 2D variable which is used to sample
    ) const override {
        auto sample = sampleContinuousDistribution2D(
                    m_DistributionsBuffer.get() + nodeIdx * m_nDistributionSize,
                    m_MapSize.x, m_MapSize.y, s2D);

        auto uv = getUV(sample.value, m_MapSize);
        float sinTheta;
        auto nodeWo = sphericalMapping(uv, sinTheta);

        auto nodePos = m_pSkel->getNode(nodeIdx).P;

        // This one is biased: only points from the map can be sampled
        auto pixel = rasterPositionToPixel(sample.value, m_MapSize);

        auto pixelIdx = getPixelIndex(pixel, m_MapSize);

        P = m_PositionMapsBuffer[nodeIdx * m_nMapSize + pixelIdx];
        N = m_NormalDistMapsBuffer[nodeIdx * m_nMapSize + pixelIdx].xyz();

        auto nodeWoLength = distance(P, nodePos);

        pdfWrtSolidAngle = sphericalAnglesToSolidAnglePDF(uvToSphericalAnglesPDF(sample.pdf), 1.f / sinTheta);
        pdfWrtArea = solidAngleToAreaPDF(pdfWrtSolidAngle, 1.f / sqr(nodeWoLength), dot(N, -nodeWo));

        return m_ImportanceMapsBuffer[nodeIdx * m_nMapSize + pixelIdx].rgb();
    }

    virtual float directionPdfWrtNodeSolidAngle(uint32_t nodeIdx,
                                                const Vec3f& wo) const {
        float sinTheta;
        auto uv = rcpSphericalMapping(wo, sinTheta);

        auto pdf = pdfContinuousDistribution2D(
                        m_DistributionsBuffer.get() + nodeIdx * m_nDistributionSize,
                        m_MapSize.x, m_MapSize.y, uv);

        return sphericalAnglesToSolidAnglePDF(uvToSphericalAnglesPDF(pdf), 1.f / sinTheta);
    }

    // Compute the pdf of sampling a point wrt. the solid angle at the node position
    float pdfWrtNodeSolidAngle(
            uint32_t nodeIdx,
            const Vec3f& P,
            Vec3f& dirNode2Point,
            float& dist) const override {
        auto nodeP = m_pSkel->getNode(nodeIdx).P;
        dirNode2Point = P - nodeP;
        dist = length(dirNode2Point);
        dirNode2Point /= dist;

        if(occluded(nodeIdx, dirNode2Point, dist)) {
            return -1.f;
        }

        float sinTheta;
        auto uv = rcpSphericalMapping(dirNode2Point, sinTheta);

        auto pdf = pdfContinuousDistribution2D(
                        m_DistributionsBuffer.get() + nodeIdx * m_nDistributionSize,
                        m_MapSize.x, m_MapSize.y, uv);

        return sphericalAnglesToSolidAnglePDF(uvToSphericalAnglesPDF(pdf), 1.f / sinTheta);
    }

    bool occluded(uint32_t nodeIdx, const Vec3f& dir, float l) const {
        auto uv = rcpSphericalMapping(dir);
        auto pixel = getPixel(uv, m_MapSize);
        auto pixelIdx = getPixelIndex(pixel, m_MapSize);

        float storedDist = m_NormalDistMapsBuffer[nodeIdx * m_nMapSize + pixelIdx].w;
        return (m_fOcclusionError + storedDist / l) < 1.f;
    }

    const Vec2u& getMapSize() const {
        return m_MapSize;
    }

private:
    SceneViewer& m_Viewer;

    uint32_t m_nMapHeightSetting = 128u;

    const Skeleton* m_pSkel = nullptr;

    // CPU Buffers
    Unique<float[]> m_DistributionsBuffer;
    Unique<Vec3f[]> m_PositionMapsBuffer;
    Unique<Vec4f[]> m_NormalDistMapsBuffer;
    Unique<Vec4f[]> m_ImportanceMapsBuffer;

    uint32_t m_nDistributionSize = 0u;
    Vec2u m_MapSize = Vec2u(0);
    uint32_t m_nMapSize = 0u;
    uint32_t m_nMapCount = 0u;

    float m_fOcclusionError = 0.05f; // 5% error
};

}
