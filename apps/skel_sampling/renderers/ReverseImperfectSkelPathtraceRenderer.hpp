#pragma once

#include "SceneViewer.hpp"
#include "SkelSampler.hpp"

#include <bonez/rendering/Renderer.hpp>
#include <bonez/opengl/debug/GLDebugOutputStream.hpp>
#include <bonez/sampling/multiple_importance_sampling.hpp>
#include <bonez/scene/lights/EmissionSampler.hpp>

namespace BnZ {

class ReverseImperfectSkelPathtraceRenderer: public TileProcessingRenderer {
public:
    UpdateFlag m_Flag;
    uint32_t m_nSphereMapHeight = 128;
    int m_nSelectedNode = 2;

    EmissionSampler m_EmissionSampler;

    struct MappingData {
        std::vector<Intersection> intersections;
        std::vector<Vec4i> nodes;
        std::vector<Vec4f> weights;

        void resizeBuffers(uint32_t size) {
            intersections.resize(size);
            nodes.resize(size);
            weights.resize(size);
        }
    };

    MappingData m_Data;

    float m_fRandom1;
    Vec2f m_fRandom2;

    SceneViewer& m_Viewer;
    const SkelSampler& m_SkelSampler;

    ReverseImperfectSkelPathtraceRenderer(SceneViewer& module, const SkelSampler& skelSampler):
        m_Viewer(module), m_SkelSampler(skelSampler) {
    }

    virtual std::string getName() const {
        return "ReverseImperfectSkelPathtraceRenderer";
    }

    // This version trace a ray
    /*
    Vec4f Li(const FrameData& frameData, const Ray& primRay, uint32_t threadID, uint32_t pixelIdx) const {
        auto I = frameData.m_pScene->intersect(primRay);

        if(!I) {
            return Vec4f(badColor(), 1);
        }

        // If I is emissive and currentDepth == m_nMaxPathDepth, add Le
        auto brdf = frameData.m_pScene->shade(I);

        auto wo = -primRay.dir.xyz();

        auto L = Vec3f(0.f);

        RaySample sample = m_Viewer.sampleRay(m_Data.nodes[pixelIdx],
                                              m_Data.weights[pixelIdx],
                                              getFloat(frameData, threadID),
                                              getFloat2(frameData, threadID));

        if(!sample) {
            return Vec4f(0.f);
        }

        if(auto I2 = frameData.m_pScene->intersect(sample.value)) {
            float pdfWrtArea, pdfWrtSolidAngle;

            {
                // First: compute pdf wrt. to Area
                auto wi = sample.value.org - I2.P;
                auto d = length(wi);
                wi /= d;
                pdfWrtArea = sample.pdf * max(0.f, dot(I2.Ns, wi)) / sqr(d);
            }

            if(pdfWrtArea == 0.f) {
                return Vec4f(0.f);
            }

            // Then: compute pdf wrt. solid angle at I
            auto wi = I2.P - I.P;
            auto d = length(wi);
            wi /= d;
            auto c = max(0.f, dot(I2.Ns, -wi));
            if(c > 0.f) {
                pdfWrtSolidAngle = pdfWrtArea * sqr(d) / c;
            } else {
                return Vec4f(0.f);
            }

            if(pdfWrtSolidAngle == 0.f) {
                return Vec4f(0.f);
            }

            auto Li = estimateDirectLight(*frameData.m_pScene,
                                          m_DirectLightSampler,
                                          I2, -wi,
                                          frameData.m_pScene->shade(I2),
                                          getFloat(frameData, threadID),
                                          getFloat(frameData, threadID),
                                          getFloat2(frameData, threadID));

            L += brdf.eval(wi, wo) * Li * max(0.f, dot(I.Ns.xyz(), wi))
                    / pdfWrtSolidAngle;
        }

        return Vec4f(L, 1.f);
    }*/

    // This version use a precomputed position and illumination
    /*
    Vec4f Li(const FrameData& frameData, const Ray& primRay, uint32_t threadID, uint32_t pixelIdx) const {
        auto I = frameData.m_pScene->intersect(primRay);

        if(!I) {
            return Vec4f(badColor(), 1);
        }

        // If I is emissive and currentDepth == m_nMaxPathDepth, add Le
        auto brdf = frameData.m_pScene->shade(I);

        auto wo = -primRay.dir.xyz();

        auto L = Vec3f(0.f);

        Vec3f sampledPosition, sampledNormal;
        float pdfWrtArea;
        Vec3f Li;

        m_Viewer.sampleLightedPoint(
                    m_Data.nodes[pixelIdx],
                    m_Data.weights[pixelIdx],
                    sampledPosition,
                    sampledNormal,
                    pdfWrtArea,
                    Li,
                    getFloat(frameData, threadID),
                    getFloat2(frameData, threadID));

        if(!pdfWrtArea) {
            return Vec4f(0.f);
        }

        float pdfWrtSolidAngle;


        // Then: compute pdf wrt. solid angle at I
        auto wi = sampledPosition - I.P;
        auto d = length(wi);
        wi /= d;
        auto c = max(0.f, dot(sampledNormal, -wi));
        if(c > 0.f) {
            pdfWrtSolidAngle = pdfWrtArea * sqr(d) / c;
        } else {
            return Vec4f(0.f);
        }

        if(pdfWrtSolidAngle == 0.f) {
            return Vec4f(0.f);
        }

        L += brdf.eval(wi, wo) * Li * max(0.f, dot(I.Ns.xyz(), wi))
                / pdfWrtSolidAngle;

        return Vec4f(L, 1.f);
    }*/

    // This version use a precomputed position and illumination
    // and use the same random numbers for every pixel (no noise ?)
    Vec4f Li(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx) const {
        auto I = getScene().intersect(primRay);

        if(!I) {
            return Vec4f(badColor(), 1);
        }

        auto node = clamp(uint32_t(m_fRandom1 * getScene().getSegmentedCurvSkeleton()->size()), 0u, uint32_t(getScene().getSegmentedCurvSkeleton()->size()));

        if(m_nSelectedNode >= 0) {
            node = m_nSelectedNode;
        }

        // FAUX POUR LE MOMENT: LE POINT CHOISIT SUR LA CARTE N'EST PAS FORCEMENT ASSOCIE AU NOEUD
        auto dir = getScene().getSegmentedCurvSkeleton()->getNode(node).P - I.P;
        auto l = length(dir);
        dir /= l;
        if(getScene().occluded(secondaryRay(I, dir, l))) {
            return Vec4f(0, 0, 0, 1);
        }

        // If I is emissive and currentDepth == m_nMaxPathDepth, add Le
        auto brdf = getScene().shade(I);

        auto wo = -primRay.dir.xyz();

        auto L = Vec3f(0.f);

        Vec3f sampledPosition, sampledNormal;
        float pdfWrtArea;
        Vec3f Li = sampleSurfacePoint(
                    m_SkelSampler,
                    1u,
                    Vec4i(node, -1, -1, -1),
                    Vec4f(1, 0, 0, 0),
                    sampledPosition,
                    sampledNormal,
                    pdfWrtArea,
                    m_fRandom1,
                    m_fRandom2);

        if(!pdfWrtArea) {
            return Vec4f(0.f);
        }

        float pdfWrtSolidAngle;


        // Then: compute pdf wrt. solid angle at I
        auto wi = sampledPosition - I.P;
        auto d = length(wi);
        wi /= d;
        auto c = max(0.f, dot(sampledNormal, -wi));
        if(c > 0.f) {
            pdfWrtSolidAngle = pdfWrtArea * sqr(d) / c;
        } else {
            return Vec4f(0.f);
        }

        if(pdfWrtSolidAngle == 0.f) {
            return Vec4f(0.f);
        }

        L += brdf.eval(wi, wo) * Li * max(0.f, dot(I.Ns.xyz(), wi))
                / pdfWrtSolidAngle;

        return Vec4f(L, 1.f);
    }

    void preprocess() override {
        if(getScene().getLightContainer().hasChanged(m_Flag)) {
            // TODO: update the code
            //m_Viewer.doPreprocess();

        }

        m_EmissionSampler.initFrame(getScene());

        auto size = getFramebuffer().getPixelCount();
        m_Data.resizeBuffers(size);

        auto pCamera = dynamic_cast<const ProjectiveCamera*>(&getCamera());
        if(!pCamera) {
            throw std::runtime_error("SkelPathtraceRenderer only works with projective camera");
        }

        // Skeleton node mapping using GPU
        m_Viewer.getNearestNodesFromGBuffer(
                    *pCamera,
                    m_Data.nodes.data(),
                    m_Data.weights.data());
    }

    void beginFrame() override {
        m_fRandom1 = getFloat(0);
        m_fRandom2 = getFloat2(0);
    }

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override {
        processTileSamples(viewport, [this, threadID](uint32_t x, uint32_t y, uint32_t pixelID, uint32_t sampleID) {
            auto s2D = getPixelSample(threadID, sampleID);
            auto ray = getPrimaryRay(x, y, s2D);
            getFramebuffer().accumulate(pixelID, Li(ray, threadID, pixelID));
        });
    }

    virtual void doExposeIO(TwBar* bar) {
        atb::addVarRW(bar, ATB_VAR(m_nSphereMapHeight));
        atb::addVarRW(bar, ATB_VAR(m_nSelectedNode));
    }

    virtual void doLoadSettings(const tinyxml2::XMLElement& xml) {
        getAttribute(xml, "sphereMapHeight", m_nSphereMapHeight);
    }

    virtual void doStoreSettings(tinyxml2::XMLElement& xml) const {
        setAttribute(xml, "sphereMapHeight", m_nSphereMapHeight);
    }
};

}
