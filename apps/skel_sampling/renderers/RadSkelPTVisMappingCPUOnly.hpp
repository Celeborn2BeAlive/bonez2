#pragma once

#include "SkelSampler.hpp"
#include "SceneViewer.hpp"
#include "opengl/GLSkeletonData.hpp"
#include "opengl/GLSkeletonMappingComputePass.hpp"

#include <bonez/rendering/Renderer.hpp>
#include <bonez/sampling/multiple_importance_sampling.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/sampling/distribution1d.h>
#include <bonez/sampling/shapes.hpp>
#include <bonez/sampling/patterns.hpp>
#include <bonez/scene/lights/EmissionSampler.hpp>

namespace BnZ {

class RadSkelPTVisMappingCPUOnly: public TileProcessingRenderer {
public:
    UpdateFlag m_Flag;
    float m_fPSkel = 0.5f;
    uint32_t m_nMaxNodeCount = 4;
    uint32_t m_nMISHeuristic = 0;
    typedef float (*HeuristicFunction)(float, float, float);
    HeuristicFunction m_pMISHeuristic = balanceHeuristicWeight;
    uint32_t m_nSphereMapHeight = 128;
    bool m_bUseCosinePSkel = false;
    uint32_t m_nComputeNodeWeightSubIndex = 0u;
    float m_fSMBias = 0.001f;
    bool m_bUseSkelGridMapping = false;
    bool m_bUseMISOneSampleModel = true;
    bool m_bUseSkelNodeNeighbours = false;

    bool m_bNeedPreprocess = true;
    EmissionSampler m_EmissionSampler;

    struct MappingData {
        std::vector<Vec4i> nodes;
        std::vector<Vec4f> weights;

        void resizeBuffers(uint32_t size) {
            nodes.resize(size);
            weights.resize(size);
        }
    };

    MappingData m_MappingData;

    //uint32_t m_nNodeSamplingGridWidth = 32;
    //Unique<Sample<Intersection>[]> m_SkelNodeIntersectionsArray; // For each node, store its m_nIntersectionCountPerNode intersections

    uint32_t m_nNodeSampleCount = 1024;
    Unique<SurfacePointSample[]> m_SkelNodeSurfacePointArray;

    Unique<float[]> m_1DMeshBuffer;
    Unique<float[]> m_1DTriangleBuffer;
    Unique<float[]> m_1DTriangleSideBuffer;
    Unique<Vec2f[]> m_2DTriangleBuffer;

    Unique<Vec3f[]> m_SkelNodeIntersectionPowerArray; // For each node, store the irradiance of its intersections
    Unique<Vec3f[]> m_SkelNodeIntersectionsIncidentDirectionArray;
    Unique<Vec3f[]> m_SkelNodeIntersectionsRadiosityArray;

    Unique<float[]> m_SkelNodeDistributionsArray; // For each node, contains a discrete distribution among its intersections
    Unique<float[]> m_SkelNodePowerArray;
    uint32_t m_nDistributionSize;

    uint32_t getPointSampleCountPerNode() const {
        return m_nNodeSampleCount;
    }

    void setHeuristic(uint32_t idx) {
        m_nMISHeuristic = idx < 3 ? idx : 0;
        switch(m_nMISHeuristic) {
        default:
        case 0:
            m_pMISHeuristic = balanceHeuristicWeight;
            break;
        case 1:
            m_pMISHeuristic = powerHeuristicWeight;
            break;
        case 2:
            m_pMISHeuristic = nullptr;
            break;
        }
    }

    void skeletonMapping(const Intersection& I, uint32_t pixelIdx, Vec4i& nearestNodes, Vec4f& weights) const {
        if(m_bUseSkelGridMapping) {
            nearestNodes = Vec4i(getScene().getSegmentedCurvSkeleton()->getNearestNode(I), -1, -1, -1);
            weights = Vec4f(1, 0, 0, 0);
        } else {
            nearestNodes = m_MappingData.nodes[pixelIdx];
            weights = m_MappingData.weights[pixelIdx];
        }
    }

    Vec3f estimateDirectRadiance(
            const SurfacePoint& point,
            float sLightIdx, float s1D, const Vec2f& s2D,
            Sample3f& wiSample,
            float& pdfWrtArea) const {
        auto lightNode = m_EmissionSampler.sample(getScene(), sLightIdx, s1D, s2D);

        Ray shadowRay;
        auto G = lightNode->G(point, shadowRay);

        wiSample.value = shadowRay.dir;
        if(G > 0.f) {
            wiSample.pdf = lightNode->pdf() / G;
        } else {
            wiSample.pdf = 0.f;
        }

        pdfWrtArea = lightNode->pdf() * lightNode->pdfWrtArea(point);

        if(getScene().occluded(shadowRay)) {
            return zero<Vec3f>();
        }

        auto L = lightNode->getRadiantExitance() * lightNode->Le(-shadowRay.dir);

        return L;

        /*
        RaySample r;
        auto L = m_DirectLightSampler.sample(getScene(), point, r, sLightIdx, s1D, s2D);

        wiSample.value = r.value.dir;

        auto d = dot(point.Ns, r.value.dir);

        if(d <= 0.f) {
            wiSample.pdf = 0.f;
        } else {
            wiSample.pdf = r.pdf / d; // pdf wrt. projected solid angle on the point
        }

        if(getScene().occluded(r.value)) {
            return zero<Vec3f>();
        }

        return L;*/
    }

    Vec3f sampleSkeleton(uint32_t threadID,
                         uint32_t pixelIdx,
                         const Intersection& I,
                         const Vec3f& wo, const BRDF& brdf,
                         Sample3f& wi,
                         float& wiLength,
                         SurfacePointSample& sampledI,
                         Vec3f& estimatedRadiosity,
                         Vec3f& estimatedRadiance,
                         Vec3f& wiSample,
                         uint32_t& sampledNode,
                         Vec4f values[]) const {
        Vec4i nearestNodes;
        Vec4f weights;
        skeletonMapping(I, pixelIdx, nearestNodes, weights);

        auto nodeSample = sampleNodeFromWeights(nearestNodes,
                                                weights,
                                                m_nMaxNodeCount,
                                                getFloat(threadID));


        if(nodeSample.pdf > 0.f) {
            auto nodeIdx = nearestNodes[nodeSample.value];

            if(nodeIdx >= 0) {
                // Choosing a neighbour ? let's try => it works better
                if(m_bUseSkelNodeNeighbours) {
                    auto pSkel = getScene().getSegmentedCurvSkeleton();
                    auto nodeCount = 1 + pSkel->neighbours(nodeIdx).size();
                    auto r1D = getFloat(threadID);
                    auto idx = min(uint32_t(nodeCount * r1D), uint32_t(nodeCount - 1));
                    if(idx > 0) {
                        nodeIdx = pSkel->neighbours(nodeIdx)[idx - 1];
                    }
                }

                sampledNode = nodeIdx;

                auto offset = nodeIdx * getPointSampleCountPerNode();
                auto distribOffset = nodeIdx * m_nDistributionSize;


                // Resampling of a point based on precomputed radiosities
                auto sample = sampleDiscreteDistribution1D(m_SkelNodeDistributionsArray.get() + distribOffset,
                                                           getPointSampleCountPerNode(),
                                                           getFloat(threadID));

                if(sample.pdf > 0.f) {
                    auto& tmp = m_SkelNodeSurfacePointArray[offset + sample.value];
                    sampledI = tmp;
                    estimatedRadiosity = m_SkelNodeIntersectionsRadiosityArray[offset + sample.value];
                    estimatedRadiance = m_SkelNodeIntersectionPowerArray[offset + sample.value];
                    wiSample = m_SkelNodeIntersectionsIncidentDirectionArray[offset + sample.value];
                    wi.value = sampledI.value.P - I.P;
                    wiLength = length(wi.value);
                    wi.value /= wiLength;

                    auto fr = brdf.eval(wi.value, wo);

                    wi.pdf = getPointSampleCountPerNode() *
                            sample.pdf *
                            areaToSolidAnglePDF(tmp.pdf, sqr(wiLength), dot(sampledI.value.Ns, -wi.value));


                    return fr;
                }
            }
        }

        wi.pdf = 0.f;
        return zero<Vec3f>();
    }

    float pdfSkeleton(uint32_t threadID,
                      uint32_t pixelIdx,
                      const SurfacePoint& I,
                      float pdfWrtArea,
                      const Vec3f& estimatedRadiosity,
                      uint32_t nodeID,
                      uint32_t sampledNodeID) const {
        if(m_SkelNodePowerArray[nodeID] == 0.f) {
            return 0.f;
        }

        auto pSkel = getScene().getSegmentedCurvSkeleton();
        auto node = pSkel->getNode(nodeID);

        auto dir = node.P - I.P;
        auto l = length(dir);
        if(l == 0.f) {
            return 0.f;
        }
        dir /= l;

        // Perf or correctness
        if(nodeID != sampledNodeID && getScene().occluded(Ray(I, dir, l))) {
            return 0.f;
        }

        return /*luminance(estimatedRadiosity / m_SkelNodePowerArray[nodeID]) * */pdfWrtArea;
    }

    float pdfSkeletonWithNeighbours(uint32_t threadID,
                                    uint32_t pixelIdx,
                                    const SurfacePoint& I,
                                    float pdfWrtArea,
                                    const Vec3f& estimatedRadiosity,
                                    uint32_t nodeID,
                                    uint32_t sampledNodeID) const {
        if(!m_bUseSkelNodeNeighbours) {
            return pdfSkeleton(threadID, pixelIdx, I, pdfWrtArea, estimatedRadiosity, nodeID, sampledNodeID);
        }

        auto pSkel = getScene().getSegmentedCurvSkeleton();
        auto size = pSkel->neighbours(nodeID).size() + 1;
        auto w = 1.f / size;

        auto p = 0.f;
        p += pdfSkeleton(threadID, pixelIdx, I, pdfWrtArea, estimatedRadiosity, nodeID, sampledNodeID);
        for(auto& n: pSkel->neighbours(nodeID)) {
            p += pdfSkeleton(threadID, pixelIdx, I, pdfWrtArea, estimatedRadiosity, n, sampledNodeID);
        }
        return w * p;
    }

    float pdfSkeletonWrtArea(uint32_t threadID,
                             uint32_t pixelIdx,
                             const SurfacePoint& I,
                             float pdfWrtArea,
                             const Vec3f& estimatedRadiosity,
                             const Intersection& shadingI,
                             uint32_t sampledNodeID) const {
        Vec4i nearestNodes;
        Vec4f nodeWeight;
        skeletonMapping(shadingI, pixelIdx, nearestNodes, nodeWeight);

        // Here we should do that:
        auto size = 0u;
        Vec4f weights = zero<Vec4f>();
        for (size = 0u;
             size < min(m_nMaxNodeCount, 4u) && nearestNodes[size] >= 0;
             ++size) {
            weights[size] = nodeWeight[size];
        }

        if(size == 0u) {
            return 0.f;
        }

        DiscreteDistribution4f dist(weights);
        float p = 0.f;
        for(auto i = 0u; i < size; ++i) {
            float pdf = pdfSkeletonWithNeighbours(threadID, pixelIdx, I, pdfWrtArea, estimatedRadiosity, nearestNodes[i], sampledNodeID);
            if(pdf > 0.f) {
                p += dist.pdf(i) * pdf;
            }
        }
        return p;
    }

    // Return the pdf of sampling the direction (shadingI -> I) with the skeleton
    float pdfSkeleton(uint32_t threadID,
                      uint32_t pixelIdx,
                      const SurfacePoint& I,
                      float pdfWrtArea,
                      const Vec3f& estimatedRadiosity,
                      const Intersection& shadingI,
                      uint32_t sampledNodeID) const {
        auto pdfWrtSkel = pdfSkeletonWrtArea(threadID, pixelIdx, I, pdfWrtArea, estimatedRadiosity, shadingI, sampledNodeID);

        auto dir = shadingI.P - I.P;
        auto l = length(dir);

        if(l == 0.f) {
            return 0.f;
        }
        dir /= l;

        return areaToSolidAnglePDF(pdfWrtSkel, l * l, dot(dir, I.Ns));
    }

    enum RenderTarget {
        FINAL_RENDER,
        SKEL_WEIGHT = 3,
        BRDF_WEIGHT = 4,
        SKEL_COHERENCY = 5,
        VARIANCE = 6,
        CHOSEN_SAMPLE = 7,
        //PDF = 8
    };

    void Li_one_sample_strategy(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx, Vec4f values[]) const {
        auto I = getScene().intersect(primRay);

        if(!I) {
            values[FINAL_RENDER] += Vec4f(badColor(), 1.f);
            return;
        }

        auto brdf = getScene().shade(I);
        auto wo = -primRay.dir.xyz();

        auto L = zero<Vec3f>();

        {
            auto s1D = getFloat(threadID);

            Sample3f wi;
            float wiLength;
            SurfacePointSample sampledI;
            Vec3f estimatedRadiosity;
            Vec3f estimatedRadiance;
            Vec3f wiSample;
            Vec3f fr;
            uint32_t sampledNode;

            auto pSkel = m_fPSkel;

            if(m_bUseCosinePSkel) {
                fr = sampleSkeleton(threadID, pixelIdx, I, wo, brdf,
                                    wi,
                                    wiLength,
                                    sampledI,
                                    estimatedRadiosity,
                                    estimatedRadiance,
                                    wiSample,
                                    sampledNode,
                                    values);
                pSkel = min(m_fPSkel, max(0.f, dot(I.Ns, wi.value)));
            }

            // Sample the skeleton
            if(s1D <= pSkel)
            {
                if(!m_bUseCosinePSkel) {
                    fr = sampleSkeleton(threadID, pixelIdx, I, wo, brdf,
                                        wi,
                                        wiLength,
                                        sampledI,
                                        estimatedRadiosity,
                                        estimatedRadiance,
                                        wiSample,
                                        sampledNode,
                                        values);
                }

                bool isValid = (fr != Vec3f(0.f) &&
                        wi.pdf != 0.f && dot(I.Ns, wi.value) > 0.f);

                if(isValid) {
                    values[CHOSEN_SAMPLE] += Vec4f(getColor(sampledNode), 1.f);

                    if(getScene().occluded(Ray(I, sampledI.value, wi.value, wiLength))) {
                        values[SKEL_COHERENCY] += Vec4f(0, 0, 0, 1);
                        values[FINAL_RENDER] += Vec4f(Vec3f(0.f), 1.f);
                        values[1] += Vec4f(Vec3f(0.f), 1.f);
                        values[SKEL_WEIGHT] += Vec4f(Vec3f(0), 1.f);
                    } else {
                        values[SKEL_COHERENCY] += Vec4f(1, 1, 1, 1);

                        auto brdfSampleI = getScene().shade(sampledI.value);

                        auto Li = brdfSampleI.eval(wiSample, -wi.value) * estimatedRadiance;

                        float pdfWrtSkel = pdfSkeleton(threadID,
                                                 pixelIdx,
                                                 sampledI.value,
                                                 sampledI.pdf,
                                                 estimatedRadiosity,
                                                 I,
                                                 sampledNode);

                        float w = pSkel * pdfWrtSkel / (pSkel * pdfWrtSkel + (1 - pSkel) * brdf.pdf(wo, wi.value));

                        /*
                        if(threadID == 0) {
                            std::cerr << wi.pdf << ", " << pdfWrtSkel << std::endl;
                        }*/

                        auto Ltmp = fr * Li * max(0.f, dot(I.Ns.xyz(), wi.value))
                                / (pSkel * wi.pdf);

                        L += Ltmp;
                        values[1] += Vec4f(pSkel * Ltmp, 1.f);
                        values[SKEL_WEIGHT] += Vec4f(Vec3f(w), 1.f);
                        values[FINAL_RENDER] += Vec4f(L, 1.f);
                    }
                } else {
                    values[FINAL_RENDER] += Vec4f(Vec3f(0), 1.f);
                    values[1] += Vec4f(Vec3f(0.f), 1.f);
                    values[SKEL_WEIGHT] += Vec4f(Vec3f(0), 1.f);
                }
            } else
            // Sample the brdf
            {
                Sample3f wiSample;
                auto fr = brdf.sample(wo, wiSample, getFloat2(threadID));

                if(wiSample.pdf > 0.f) {
                    auto Ireflect = getScene().intersect(Ray(I, wiSample.value));
                    if(Ireflect) {
                        auto brdfReflectI = getScene().shade(Ireflect);

                        float pdfWrtArea;
                        Sample3f wiReflectSample;
                        auto Li = estimateDirectRadiance(
                                Ireflect,
                                getFloat(threadID),
                                getFloat(threadID),
                                getFloat2(threadID),
                                wiReflectSample,
                                pdfWrtArea);

                        if(wiReflectSample.pdf > 0.f) {
                            auto fr2 = brdfReflectI.eval(wiReflectSample.value, -wiSample.value) / wiReflectSample.pdf;

                            Li *= fr2;

                            float pdfWrtSkel = pdfSkeleton(threadID,
                                                           pixelIdx,
                                                           Ireflect,
                                                           pdfWrtArea,
                                                           Li * max(0.f, dot(-wiSample.value, Ireflect.Ns)), // TODO: need a dot with sampled ourputdir ?
                                                           I,
                                                           -1);

                            float w = (1 - pSkel) * wiSample.pdf / ((1 - pSkel) * wiSample.pdf + pSkel * pdfWrtSkel);

                            auto Ltmp = fr * Li * max(0.f, dot(I.Ns.xyz(), wiSample.value))
                                    / ((1 - pSkel)  * wiSample.pdf);

                            L += w * Ltmp;
                            values[2] += Vec4f((1 - pSkel)  * Ltmp, 1.f);
                            values[FINAL_RENDER] += Vec4f(L, 1.f);
                            values[BRDF_WEIGHT] += Vec4f(Vec3f(w), 1.f);
                        } else {
                            values[FINAL_RENDER] += Vec4f(Vec3f(0.f), 1.f);
                            values[2] += Vec4f(Vec3f(0.f), 1.f);
                            values[BRDF_WEIGHT] += Vec4f(Vec3f(0), 1.f);
                        }
                    } else {
                        values[FINAL_RENDER] += Vec4f(Vec3f(0.f), 1.f);
                    }
                }
            }
        }
    }

    void Li(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx, Vec4f values[]) const {
        if(m_bUseMISOneSampleModel) {
            Li_one_sample_strategy(primRay, threadID, pixelIdx, values);
        }
    }

    virtual std::string getName() const {
        return "RadSkelPTVisMappingCPUOnly";
    }

    void computeSkelNodeIntersections() {
        auto pSkel = getScene().getSegmentedCurvSkeleton();
        auto completeSize = getPointSampleCountPerNode() * pSkel->size();

        if(!m_SkelNodeSurfacePointArray) {
            m_SkelNodeSurfacePointArray = makeUniqueArray<SurfacePointSample>(completeSize);
            m_SkelNodeIntersectionPowerArray = makeUniqueArray<Vec3f>(completeSize);
            m_SkelNodeIntersectionsIncidentDirectionArray = makeUniqueArray<Vec3f>(completeSize);
            m_SkelNodeIntersectionsRadiosityArray = makeUniqueArray<Vec3f>(completeSize);
            m_nDistributionSize = getDistribution1DBufferSize(getPointSampleCountPerNode());
            m_SkelNodeDistributionsArray = makeUniqueArray<float>(m_nDistributionSize * pSkel->size());
            m_SkelNodePowerArray = makeUniqueArray<float>(pSkel->size());

            m_1DMeshBuffer = makeUniqueArray<float>(completeSize);
            m_1DTriangleBuffer = makeUniqueArray<float>(completeSize);
            m_1DTriangleSideBuffer = makeUniqueArray<float>(completeSize);
            m_2DTriangleBuffer = makeUniqueArray<Vec2f>(completeSize);
        }

        // For each node, compute intersections, irradiance values and distributions
        launchThreads([pSkel, this](uint32_t threadID) {
            uint32_t loopID = 0u;

            while(true) {
                auto nodeIndex = loopID * getThreadCount() + threadID;
                loopID++;

                if(nodeIndex >= pSkel->size()) {
                    break;
                }

                auto nodePos = pSkel->getNode(nodeIndex).P;
                auto offset = nodeIndex * getPointSampleCountPerNode();
                auto distribOffset = nodeIndex * m_nDistributionSize;

                /*
                for(auto k = 0u; k < getPointSampleCountPerNode(); ++k) {
                    m_1DMeshBuffer[offset + k] = getFloat(threadID);
                    m_1DTriangleBuffer[offset + k] = getFloat(threadID);
                    m_1DTriangleSideBuffer[offset + k] = getFloat(threadID);
                    m_2DTriangleBuffer[offset + k] = getFloat2(threadID);
                }

                getScene().uniformSampleSurfacePoints(getPointSampleCountPerNode(),
                                                      m_1DMeshBuffer.get() + offset,
                                                      m_1DTriangleBuffer.get() + offset,
                                                      m_1DTriangleSideBuffer.get() + offset,
                                                      m_2DTriangleBuffer.get() + offset,
                                                      m_SkelNodeSurfacePointArray.get() + offset);
*/

                auto pEmissionVertex = m_EmissionSampler.sample(getScene(),
                                                                getFloat(threadID),
                                                                getFloat(threadID),
                                                                getFloat2(threadID));

                float scaleCoeff = 1.f/* / getPointSampleCountPerNode()*/;

                for(auto k = 0u; k < getPointSampleCountPerNode(); ++k) {
                    RaySample ray;
                    auto Le_dir = pEmissionVertex->sampleWi(ray, getFloat2(threadID));
                    auto power = scaleCoeff * pEmissionVertex->getRadiantExitance() * Le_dir;
                    auto I = getScene().intersect(ray.value);

                    if(I) {
                        m_SkelNodeSurfacePointArray[offset + k].value = I;
                        Ray shadowRay;
                        m_SkelNodeIntersectionPowerArray[offset + k] = power * pEmissionVertex->G(I, shadowRay);
                        m_SkelNodeIntersectionsIncidentDirectionArray[offset + k] = -ray.value.dir;

                        auto brdf = getScene().shade(I);
                        Sample3f woSample;

                        auto fr = brdf.sample(-ray.value.dir, woSample, getFloat2(threadID));

                        if(woSample.pdf > 0.f && fr != zero<Vec3f>()) {
                            m_SkelNodeIntersectionsRadiosityArray[offset + k] =
                                    fr * m_SkelNodeIntersectionPowerArray[offset + k] *
                                    max(0.f, dot(I.Ns, woSample.value)) / (pEmissionVertex->pdf() * ray.pdf * woSample.pdf);
                        } else {
                            m_SkelNodeIntersectionsRadiosityArray[offset + k] = zero<Vec3f>();
                        }

                        m_SkelNodeSurfacePointArray[offset + k].pdf = pEmissionVertex->pdf() * pEmissionVertex->pdfWrtArea(I);
                    } else {
                        m_SkelNodeIntersectionPowerArray[offset + k] = zero<Vec3f>();
                        m_SkelNodeIntersectionsRadiosityArray[offset + k] = zero<Vec3f>();
                        m_SkelNodeSurfacePointArray[offset + k].pdf = 0.f;
                    }
                }

                m_SkelNodePowerArray[nodeIndex] = 0.f;
                for(auto i = 0u, count = getPointSampleCountPerNode(); i < count; ++i) {
                    auto& I = m_SkelNodeSurfacePointArray[offset + i].value;
                    auto dir = nodePos - I.P;
                    auto l = length(dir);
                    dir /= l;
                    if(dot(I.Ns, dir) > 0.f && !getScene().occluded(Ray(I, dir, l))) {
                        m_SkelNodePowerArray[nodeIndex] += luminance(m_SkelNodeIntersectionsRadiosityArray[offset + i]);
                    } else {
                        m_SkelNodeIntersectionPowerArray[offset + i] = zero<Vec3f>();
                        m_SkelNodeIntersectionsRadiosityArray[offset + i] = zero<Vec3f>();
                    }
                }

                m_SkelNodePowerArray[nodeIndex] /= float(getPointSampleCountPerNode());

                buildDistribution1D([this, offset](uint32_t i) {
//                    if(m_SkelNodeIntersectionsArray[offset + i].pdf == 0.f) {
//                        return 0.f;
//                    }
                    return luminance(m_SkelNodeIntersectionsRadiosityArray[offset + i]/* / m_SkelNodeIntersectionsArray[offset + i].pdf*/);
                }, m_SkelNodeDistributionsArray.get() + distribOffset, getPointSampleCountPerNode());
            }
        });
    }

    void computeSkeletonMapping() {
        auto framebufferSize = getFramebuffer().getSize();
        m_MappingData.resizeBuffers(framebufferSize.x * framebufferSize.y);

        Vec2u tileCount = framebufferSize / getTileSize() +
                Vec2u(framebufferSize % getTileSize() != zero<Vec2u>());

        auto totalCount = tileCount.x * tileCount.y;

        auto pSkel = getScene().getSegmentedCurvSkeleton();

        auto task = [&](uint32_t threadID) {
            auto loopID = 0u;
            while(true) {
                auto tileID = loopID * getThreadCount() + threadID;
                ++loopID;

                if(tileID >= totalCount) {
                    return;
                }

                uint32_t tileX = tileID % tileCount.x;
                uint32_t tileY = tileID / tileCount.x;

                Vec2u tileOrg = Vec2u(tileX, tileY) * getTileSize();
                auto viewport = Vec4u(tileOrg, getTileSize());

                if(viewport.x + viewport.z > framebufferSize.x) {
                    viewport.z = framebufferSize.x - viewport.x;
                }

                if(viewport.y + viewport.w > framebufferSize.y) {
                    viewport.w = framebufferSize.y - viewport.y;
                }

                auto xEnd = viewport.x + viewport.z;
                auto yEnd = viewport.y + viewport.w;

                for(auto y = viewport.y; y < yEnd; ++y) {
                    for(auto x = viewport.x; x < xEnd; ++x) {
                        auto pixelIdx = getPixelIndex(x, y);
                        auto s2D = Vec2f(0.5f, 0.5f);
                        auto ray = getPrimaryRay(x, y, s2D);
                        auto I = getScene().intersect(ray);
                        if(I) {
                            m_MappingData.nodes[pixelIdx] = getNearestNodes(*pSkel, I.P, I.Ns, m_MappingData.weights[pixelIdx],
                                                [](uint32_t nodeIdx, Vec3f dir, float dist) { return false; });
                        } else {
                            m_MappingData.nodes[pixelIdx] = Vec4i(-1);
                        }
                    }
                }
            }
        };

        launchThreads(task);
    }

    void preprocess() override {
        if(getScene().getLightContainer().hasChanged(m_Flag)) {
            m_bNeedPreprocess = true;
        }

        m_EmissionSampler.initFrame(getScene());

        if(!m_bUseSkelGridMapping) {
            computeSkeletonMapping();
        }
    }

    void beginFrame() override {
        computeSkelNodeIntersections();
    }

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override {
        auto spp = getSppCount();
        TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
            Vec4f L[8];
            std::fill(L, L + 8, Vec4f(0.f));
            auto pixelIdx = getPixelIndex(x, y);
            for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
                auto s2D = getPixelSample(threadID, sampleID);
                auto ray = getPrimaryRay(x, y, s2D);
                Li(ray, threadID, pixelIdx, L);
            }
            for(auto i = 0u; i < 8; ++i) {
                getFramebuffer().accumulate(i, pixelIdx, L[i]);
            }
        });
    }

    virtual void doExposeIO(TwBar* bar) {
        atb::addVarRW(bar, "pSkel", m_fPSkel);
        atb::addVarRW(bar, "Max node count", m_nMaxNodeCount);
        auto setHeuristicCB = [&](uint32_t idx) {
            setHeuristic(idx);
        };

        auto getHeuristicCB =[&]() -> uint32_t {
            return m_nMISHeuristic;
        };

        atb::addVarCB(bar, "MIS Heuristic", "Balance,Power,Simple",
                      setHeuristicCB, getHeuristicCB);
        atb::addVarRW(bar, ATB_VAR(m_nSphereMapHeight));
        atb::addVarRW(bar, ATB_VAR(m_bUseCosinePSkel));
        atb::addVarRW(bar, ATB_VAR(m_nComputeNodeWeightSubIndex));
        atb::addVarRW(bar, ATB_VAR(m_fSMBias));
        atb::addVarRW(bar, ATB_VAR(m_bUseSkelGridMapping));
        atb::addVarRW(bar, ATB_VAR(m_bUseMISOneSampleModel));
        atb::addVarRW(bar, ATB_VAR(m_bUseSkelNodeNeighbours));
        atb::addVarRW(bar, ATB_VAR(m_nNodeSampleCount));
    }

    virtual void doLoadSettings(const tinyxml2::XMLElement& xml) {
        getAttribute(xml, "pSkel", m_fPSkel);
        getAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
        getAttribute(xml, "misHeuristic", m_nMISHeuristic);
        getAttribute(xml, "sphereMapHeight", m_nSphereMapHeight);
        getAttribute(xml, "useCosinePSkel", m_bUseCosinePSkel);
        getAttribute(xml, "nodeWeightFunctionIndex", m_nComputeNodeWeightSubIndex);
        getAttribute(xml, "smBias", m_fSMBias);
        getAttribute(xml, "useGridSkelMapping", m_bUseSkelGridMapping);
        getAttribute(xml, "useMISOneSampleModel", m_bUseMISOneSampleModel);
        getAttribute(xml, "useSkelNodeNeighbours", m_bUseSkelNodeNeighbours);
        getAttribute(xml, "nodeSampleCount", m_nNodeSampleCount);

        setHeuristic(m_nMISHeuristic);
    }

    virtual void doStoreSettings(tinyxml2::XMLElement& xml) const {
        setAttribute(xml, "pSkel", m_fPSkel);
        setAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
        setAttribute(xml, "misHeuristic", m_nMISHeuristic);
        setAttribute(xml, "sphereMapHeight", m_nSphereMapHeight);
        setAttribute(xml, "useCosinePSkel", m_bUseCosinePSkel);
        setAttribute(xml, "nodeWeightFunctionIndex", m_nComputeNodeWeightSubIndex);
        setAttribute(xml, "smBias", m_fSMBias);
        setAttribute(xml, "useGridSkelMapping", m_bUseSkelGridMapping);
        setAttribute(xml, "useMISOneSampleModel", m_bUseMISOneSampleModel);
        setAttribute(xml, "useSkelNodeNeighbours", m_bUseSkelNodeNeighbours);
        setAttribute(xml, "nodeSampleCount", m_nNodeSampleCount);
    }
};

}
