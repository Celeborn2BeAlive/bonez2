#pragma once

#include "SceneViewer.hpp"
#include "SkelSampler.hpp"

#include <bonez/rendering/Renderer.hpp>
#include <bonez/opengl/debug/GLDebugOutputStream.hpp>
#include <bonez/sampling/multiple_importance_sampling.hpp>
#include <bonez/scene/lights/EmissionSampler.hpp>

namespace BnZ {

class NodeVisibilitySamplingCoherenceRenderer: public TileProcessingRenderer {
public:
    UpdateFlag m_Flag;
    uint32_t m_nMaxNodeCount = 4;
    uint32_t m_nSphereMapHeight = 128;

    EmissionSampler m_EmissionSampler;

    struct MappingData {
        //mutable GLDebugOutputStream debugStream;
        std::vector<Intersection> intersections;
        std::vector<Vec4i> nodes;
        std::vector<Vec4f> weights;

        MappingData(const Shared<GLDebugRenderer>& pDebugRenderer)/*:
            debugStream(newGLDebugStream(pDebugRenderer, "SkelPathtraceRenderer"))*/ {
        }

        void resizeBuffers(uint32_t size) {
            intersections.resize(size);
            nodes.resize(size);
            weights.resize(size);
        }
    };

    MappingData m_Data;

    SceneViewer& m_Viewer;
    const SkelSampler& m_SkelSampler;

    NodeVisibilitySamplingCoherenceRenderer(SceneViewer& module, const SkelSampler& skelSampler):
        m_Data(module.m_pGLDebugRenderer), m_Viewer(module), m_SkelSampler(skelSampler) {
    }

    virtual std::string getName() const {
        return "NodeVisibilitySamplingCoherenceRenderer";
    }

    Vec3f sampleSkeleton(uint32_t threadID,
                         uint32_t pixelIdx,
                         const Intersection& I,
                         const Vec3f& wo, const BRDF& brdf,
                         Sample3f& wi, Vec3f& sampledPos, Vec3f& sampledNormal) const {
        sampleSurfacePoint(m_SkelSampler, m_nMaxNodeCount, m_Data.nodes[pixelIdx],
                m_Data.weights[pixelIdx],
                sampledPos, sampledNormal, wi.pdf, getFloat(threadID),
                getFloat2(threadID));

        if(wi.pdf) {
            wi.value = sampledPos - I.P.xyz();
            auto wiLength = length(wi.value);
            wi.value /= wiLength;
        }

        return zero<Vec3f>();
    }

    Vec3f Li(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx) const {
        auto I = getScene().intersect(primRay);

        if(!I) {
            return badColor();
        }

        // If I is emissive and currentDepth == m_nMaxPathDepth, add Le
        auto brdf = getScene().shade(I);

        auto wo = -primRay.dir.xyz();

        auto L = Vec3f(0.f);

        Intersection I2;
        Sample3f wi;

        Vec3f sampledPos, sampledNormal;
        sampleSkeleton(threadID, pixelIdx, I, wo, brdf, wi, sampledPos, sampledNormal);

        if(wi.pdf > 0.f) {
            auto tfar = distance(I.P, sampledPos);
            if(dot(I.Ns, wi.value) > 0.f
                    && dot(sampledNormal, -wi.value) > 0.f &&
                    !getScene().occluded(secondaryRay(I, wi.value,
                                                          tfar - 0.01f * m_Viewer.getZFar()))) {
                L += Vec3f(1.f);
            }
        }

        return L;
    }

    void preprocess() override {
        if(getScene().getLightContainer().hasChanged(m_Flag)) {
            // TODO: update the code
            //m_Viewer.doPreprocess();
        }

        m_EmissionSampler.initFrame(getScene());

        auto size = getFramebuffer().getPixelCount();
        m_Data.resizeBuffers(size);

        auto pCamera = dynamic_cast<const ProjectiveCamera*>(&getCamera());
        if(!pCamera) {
            throw std::runtime_error("SkelPathtraceRenderer only works with projective camera");
        }

        // Skeleton node mapping using GPU
        m_Viewer.getNearestNodesFromGBuffer(
                    *pCamera,
                    m_Data.nodes.data(),
                    m_Data.weights.data());
    }

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override {
        auto spp = getSppCount();
        TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
            Vec4f L(0.f);
            auto pixelIdx = getPixelIndex(x, y);
            for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
                auto s2D = getPixelSample(threadID, sampleID);
                auto ray = getPrimaryRay(x, y, s2D);
                L += Vec4f(Li(ray, threadID, pixelIdx), 1.f);
            }
            //(*frameData.m_pFramebuffer)[pixelIdx] += L;
            getFramebuffer().accumulate(pixelIdx, L);
        });
    }

    virtual void doExposeIO(TwBar* bar) {
        atb::addVarRW(bar, "Max node count", m_nMaxNodeCount);
        atb::addVarRW(bar, ATB_VAR(m_nSphereMapHeight));
    }

    virtual void doLoadSettings(const tinyxml2::XMLElement& xml) {
        getAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
        getAttribute(xml, "sphereMapHeight", m_nSphereMapHeight);
    }

    virtual void doStoreSettings(tinyxml2::XMLElement& xml) const {
        setAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
        setAttribute(xml, "sphereMapHeight", m_nSphereMapHeight);
    }
};

}
