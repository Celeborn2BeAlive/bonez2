#pragma once

#include "SkelSampler.hpp"
#include "SceneViewer.hpp"
#include "opengl/GLSkeletonData.hpp"
#include "opengl/GLSkeletonMappingComputePass.hpp"

#include <bonez/rendering/Renderer.hpp>
#include <bonez/sampling/multiple_importance_sampling.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/sampling/distribution1d.h>
#include <bonez/sampling/shapes.hpp>
#include <bonez/sampling/patterns.hpp>
#include <bonez/scene/lights/EmissionSampler.hpp>

namespace BnZ {

/*
 * Stratégie d'échantillonage:
 * Pour chaque noeud on tire N points visible sur lesquels on estime l'irradiance directe.
 * Au moment du rendu on echantillone un point parmis ceux associé au noeud le plus proche.
 */
class SkelPathtracePreprocessNodesRenderer: public TileProcessingRenderer {
public:
    UpdateFlag m_Flag;
    float m_fPSkel = 0.5f;
    uint32_t m_nMaxNodeCount = 4;
    uint32_t m_nMISHeuristic = 0;
    typedef float (*HeuristicFunction)(float, float, float);
    HeuristicFunction m_pMISHeuristic = balanceHeuristicWeight;
    uint32_t m_nSphereMapHeight = 128;
    bool m_bUseCosinePSkel = false;
    uint32_t m_nComputeNodeWeightSubIndex = 0u;
    float m_fSMBias = 0.001f;
    bool m_bUseSkelGridMapping = false;
    bool m_bUseResampling = false;
    uint32_t m_nResamplingSampleCount = 1u;

    bool m_bNeedPreprocess = true;
    EmissionSampler m_EmissionSampler;

    GLGBuffer m_GBuffer;
    GLGBufferRenderPass m_GBufferRendererPass;
    GLSkeletonMappingComputePass m_SkelMappingComputePass;

    struct MappingData {
        std::vector<Vec4i> nodes;
        std::vector<Vec4f> weights;
        GLImmutableBuffer<Vec4i> gpuNodesBuffer;
        GLImmutableBuffer<Vec4f> gpuWeightsBuffer;

        void resizeBuffers(uint32_t size) {
            nodes.resize(size);
            weights.resize(size);
            gpuNodesBuffer = genBufferStorage<Vec4i>(size, nullptr, 0, GL_WRITE_ONLY);
            gpuWeightsBuffer =  genBufferStorage<Vec4f>(size, nullptr, 0, GL_WRITE_ONLY);
        }
    };

    MappingData m_Data;
    GLSkeletonData<2>& m_GLSkelData;

    //SkelSampler& m_SkelSampler;

    const GLScene& m_GLScene;

    SceneViewer& m_Viewer;

    uint32_t m_nNodeSamplingGridHeight = 32;
    uint32_t m_nNodeSamplingGridWidth = m_nNodeSamplingGridHeight;
    uint32_t m_nIntersectionCountPerNode = m_nNodeSamplingGridWidth * m_nNodeSamplingGridHeight;
    Unique<Sample<Intersection>[]> m_SkelNodeIntersectionsArray; // For each node, store its m_nIntersectionCountPerNode intersections
    Unique<Vec3f[]> m_SkelNodeIntersectionsIrradianceArray; // For each node, store the irradiance of its intersections
    Unique<float[]> m_SkelNodeDistributionsArray; // For each node, contains a discrete distribution among its intersections
    uint32_t m_nDistributionSize;

    SkelPathtracePreprocessNodesRenderer(//SkelSampler& sampler,
                          const GLShaderManager& shaderManager,
                          const GLScene& glScene,
                          SceneViewer& viewer,
                          GLSkeletonData<2>& glSkelData):
        m_GBufferRendererPass(shaderManager),
        m_SkelMappingComputePass(shaderManager),
        m_GLSkelData(glSkelData),
        m_Viewer(viewer),
        m_GLScene(glScene) {
    }

    void setHeuristic(uint32_t idx) {
        m_nMISHeuristic = idx < 3 ? idx : 0;
        switch(m_nMISHeuristic) {
        default:
        case 0:
            m_pMISHeuristic = balanceHeuristicWeight;
            break;
        case 1:
            m_pMISHeuristic = powerHeuristicWeight;
            break;
        case 2:
            m_pMISHeuristic = nullptr;
            break;
        }
    }

    void skeletonMapping(const Intersection& I, uint32_t pixelIdx, Vec4i& nearestNodes, Vec4f& weights) const {
        if(m_bUseSkelGridMapping) {
            nearestNodes = Vec4i(getScene().getSegmentedCurvSkeleton()->getNearestNode(I), -1, -1, -1);
            weights = Vec4f(1, 0, 0, 0);
        } else {
            nearestNodes = m_Data.nodes[pixelIdx];
            weights = m_Data.weights[pixelIdx];
        }
    }

    Vec3f sampleSkeleton(uint32_t threadID,
                         uint32_t pixelIdx,
                         const Intersection& I,
                         const Vec3f& wo, const BRDF& brdf,
                         Sample3f& wi,
                         float& wiLength,
                         Intersection& sampledI,
                         Vec3f& sampledIrradiance,
                         Vec4f values[]) const {
        auto start = getMicroseconds();
        Vec4i nearestNodes;
        Vec4f weights;
        skeletonMapping(I, pixelIdx, nearestNodes, weights);

        auto nodeSample = sampleNodeFromWeights(nearestNodes,
                                                weights,
                                                m_nMaxNodeCount,
                                                getFloat(threadID));

        //auto nodeSample = Sample1u(0, 1.f);

        if(nodeSample.pdf > 0.f) {
            auto nodeIdx = nearestNodes[nodeSample.value];

            if(nodeIdx >= 0) {
                // Choosing a neighbour ? let's try => it works better
                auto pSkel = getScene().getSegmentedCurvSkeleton();
                auto nodeCount = 1 + pSkel->neighbours(nodeIdx).size();
                auto r1D = getFloat(threadID);
                auto idx = min(uint32_t(nodeCount * r1D), uint32_t(nodeCount - 1));
                if(idx > 0) {
                    nodeIdx = pSkel->neighbours(nodeIdx)[idx - 1];
                }

                auto offset = nodeIdx * m_nIntersectionCountPerNode;
                auto distribOffset = nodeIdx * m_nDistributionSize;

                auto sample = sampleDiscreteDistribution1D(m_SkelNodeDistributionsArray.get() + distribOffset,
                                                           m_nIntersectionCountPerNode,
                                                           getFloat(threadID));

                if(sample.pdf > 0.f) {
                    values[CHOSEN_SAMPLE] += Vec4f(getColor(offset + sample.value), 1.f);


                    auto& tmp = m_SkelNodeIntersectionsArray[offset + sample.value];
                    sampledI = tmp.value;
                    sampledIrradiance = m_SkelNodeIntersectionsIrradianceArray[offset + sample.value];
                    wi.value = sampledI.P - I.P;
                    wiLength = length(wi.value);
                    wi.value /= wiLength;

                    auto fr = brdf.eval(wi.value, wo);

                    if(fr != zero<Vec3f>()) {
                        // sample.pdf instead of m_nIntersectionCountPerNode ??
                        wi.pdf = m_nIntersectionCountPerNode *
                                sample.pdf *
                                areaToSolidAnglePDF(tmp.pdf, sqr(wiLength), dot(sampledI.Ns, -wi.value));
                        values[PDF] += Vec4f(wi.pdf, wi.pdf, wi.pdf, 1.f);
                    }

                    return fr;
                }
            }
        }

        wi.pdf = 0.f;
        return zero<Vec3f>();
    }

    enum RenderTarget {
        FINAL_RENDER,
        SKEL_CHOSEN,
        BRDF_CHOSEN,
        SKEL_COHERENCY,
        VARIANCE,
        CHOSEN_SAMPLE,
        PDF
    };

    void Li_noResampling(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx, Vec4f values[]) const {
        auto I = getScene().intersect(primRay);

        if(!I) {
            values[FINAL_RENDER] += Vec4f(badColor(), 1.f);
            return;
        }

        auto brdf = getScene().shade(I);
        auto wo = -primRay.dir.xyz();

        // Perform 2 reflections to get computational power comparable to bidirectional pathtracing
        for(auto i = 0u; i < 2; ++i) {
            auto L = Vec3f(0.f);

            Intersection I2;
            Sample3f wi;
            float wiLength;
            Vec3f fr = zero<Vec3f>();
            float weight = 0.f;

            float pSkel = m_fPSkel;
            float s1D = getFloat(threadID);

            if(pSkel > 0.f && m_bUseCosinePSkel) {
                //Intersection Ireflect;
                Vec3f irradiance;
                fr = sampleSkeleton(threadID, pixelIdx, I, wo, brdf, wi, wiLength, I2, irradiance, values);
                if(dot(I.Ns, wi.value) <= 0.f) {
                    values[SKEL_COHERENCY] += Vec4f(0, 0, 0, 1);
                }
                if(wi.pdf > 0.f) {
                    pSkel = min(max(0.f, dot(I.Ns, wi.value)), m_fPSkel);
                } else {
                    pSkel = 0.f;
                }
            } else if(s1D < pSkel) {
                Vec3f irradiance;
                fr = sampleSkeleton(threadID, pixelIdx, I, wo, brdf, wi, wiLength, I2, irradiance, values);
            }

            if(s1D < pSkel) {
                values[SKEL_CHOSEN] += Vec4f(1, 1, 1, 1);
                values[BRDF_CHOSEN] += Vec4f(0, 0, 0, 1);

                if(I2) {
                    if(getScene().occluded(Ray(I, I2, wi.value, wiLength))) {
                        values[SKEL_COHERENCY] += Vec4f(0, 0, 0, 1);
                        continue;
                    } else {
                        values[SKEL_COHERENCY] += Vec4f(1, 1, 1, 1);
                    }
                    weight = 1.f;
                    /*if(m_pMISHeuristic) {
                        weight = balanceHeuristicWeight(pSkel, wi.pdf, brdf.pdf(wo, wi.value));
                        wi.pdf *= pSkel;
                    } else {
                        weight = 1.f;
                    }*/
                }
            } else {
                // Sample the brdf
                fr = brdf.sample(wo, wi, getFloat2(threadID));

                if(wi.pdf > 0.f && fr != zero<Vec3f>()) {
                    I2 = getScene().intersect(Ray(I, wi.value));
                }
                weight = 1.f;
                /* HARD
                if(m_pMISHeuristic) {
                    Vec4i nearestNodes;
                    Vec4f weights;
                    skeletonMapping(I, pixelIdx, nearestNodes, weights);
                    auto skelPdf = pdfWrtIntersectionSolidAngle(m_SkelSampler, I, m_nMaxNodeCount,
                                                                nearestNodes,
                                                                weights,
                                                                I2.P,
                                                                I2.Ns);

                    weight = m_pMISHeuristic(1 - pSkel, wi.pdf, skelPdf);
                    wi.pdf *= (1 - pSkel);
                } else {
                    weight = 1.f;
                }*/

                values[BRDF_CHOSEN] += Vec4f(1, 1, 1, 1);
                values[SKEL_CHOSEN] += Vec4f(0, 0, 0, 1);
            }

            if(wi.pdf > 0.f && I2) {
                auto Li = estimateDirectLight(getScene(),
                                              m_EmissionSampler,
                                              I2, -wi.value,
                                              getScene().shade(I2),
                                              getFloat(threadID),
                                              getFloat(threadID),
                                              getFloat2(threadID));
                L += weight * fr * Li * max(0.f, dot(I.Ns.xyz(), wi.value))
                        / wi.pdf;

                if(s1D < pSkel) {
                    auto current = getFramebuffer().getChannel(FINAL_RENDER)[pixelIdx];
                    if(current.w > 0.f) {
                        auto v = L / (current.w + 1) + current.xyz() / (current.w * (current.w + 1));
                        values[VARIANCE] += Vec4f(v, 1.f);
                    }
                }
            }

            values[FINAL_RENDER] += Vec4f(L, 1.f);
        }
    }

    void Li_resampling(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx, Vec4f values[]) const {
        auto I = getScene().intersect(primRay);

        if(!I) {
            values[FINAL_RENDER] += Vec4f(badColor(), 1.f);
            return;
        }

        const auto brdf = getScene().shade(I);
        auto wo = -primRay.dir.xyz();

        // Perform 2 reflections to get computational power comparable to bidirectional pathtracing
        for(auto i = 0u; i < 2; ++i) {
            auto L = Vec3f(0.f);

            const uint32_t MAX_NB_SAMPLED = 32;
            Sample3f wi[MAX_NB_SAMPLED];
            float wiLength[MAX_NB_SAMPLED];
            Intersection sampledI[MAX_NB_SAMPLED];
            Vec3f sampledIrradiance[MAX_NB_SAMPLED];
            Vec3f fr[MAX_NB_SAMPLED];
            float resamplingDistrib[MAX_NB_SAMPLED + 1];
            bool isFromSkel[MAX_NB_SAMPLED];

            Vec4f meanIrradiance = Vec4f(0.f);

            for(auto j = 0u; j < m_nResamplingSampleCount; ++j) {
                fr[j] = sampleSkeleton(threadID, pixelIdx, I, wo, brdf, wi[j], wiLength[j], sampledI[j], sampledIrradiance[j], values);
                if(m_fPSkel > 0.f && (fr[j] == Vec3f(0.f) || wi[j].pdf == 0.f || dot(I.Ns, wi[j].value) <= 0.f)) {
                    fr[j] = brdf.sample(wo, wi[j], getFloat2(threadID));
                    /*if(m_nTotalSpp == 18 && pixelIdx == BnZ::getPixelIndex(m_nDebugPixelX, m_nDebugPixelY, getFramebuffer().getSize())) {
                        std::cerr << "loul" << " - " << wi[j].pdf << " - " << isFromSkel[j] << std::endl;
                    }*/
                    isFromSkel[j] = false;
                } else {
                    meanIrradiance += Vec4f(sampledIrradiance[j], 1.f);
                    isFromSkel[j] = true;
                }

                /*
                if(m_nTotalSpp == 18 && pixelIdx == BnZ::getPixelIndex(m_nDebugPixelX, m_nDebugPixelY, getFramebuffer().getSize())) {
                    std::cerr << j << " - " << wi[j].pdf << " - " << isFromSkel[j] << std::endl;
                }*/
            }

            if(meanIrradiance.w) {
                meanIrradiance /= meanIrradiance.w;
            } else {
                meanIrradiance = Vec4f(1.f);
            }

            for(auto j = 0u; j < m_nResamplingSampleCount; ++j) {
                if(!isFromSkel[i]) {
                    sampledIrradiance[j] = Vec3f(meanIrradiance);
                }
            }

            buildDistribution1D([this, &fr, &sampledIrradiance, &I, &wi](uint32_t j) {
                return luminance(fr[j] * sampledIrradiance[j] * max(0.f, dot(I.Ns.xyz(), wi[j].value))
                        / wi[j].pdf);
            }, resamplingDistrib, m_nResamplingSampleCount);

            float s1D = getFloat(threadID);

            auto sample = sampleDiscreteDistribution1D(resamplingDistrib, m_nResamplingSampleCount, s1D);

            if(sample.pdf > 0.f && wi[sample.value].pdf > 0.f) {
                if(isFromSkel[sample.value]) {
                    values[SKEL_CHOSEN] += Vec4f(1, 1, 1, 1);
                    values[BRDF_CHOSEN] += Vec4f(0, 0, 0, 1);

                    if(getScene().occluded(Ray(I, sampledI[sample.value], wi[sample.value].value, wiLength[sample.value]))) {
                        values[SKEL_COHERENCY] += Vec4f(0, 0, 0, 1);
                        continue;
                    }
                    values[SKEL_COHERENCY] += Vec4f(1, 1, 1, 1);

                } else {
                    values[BRDF_CHOSEN] += Vec4f(1, 1, 1, 1);
                    values[SKEL_CHOSEN] += Vec4f(0, 0, 0, 1);

                    sampledI[sample.value] = getScene().intersect(Ray(I, wi[sample.value].value));
                }
                wi[sample.value].pdf *= sample.pdf * m_nResamplingSampleCount;

                auto Li = estimateDirectLight(getScene(),
                                              m_EmissionSampler,
                                              sampledI[sample.value], -wi[sample.value].value,
                                              getScene().shade(sampledI[sample.value]),
                                              getFloat(threadID),
                                              getFloat(threadID),
                                              getFloat2(threadID));
                L += fr[sample.value] * Li * max(0.f, dot(I.Ns.xyz(), wi[sample.value].value))
                        / wi[sample.value].pdf;
            }

            values[FINAL_RENDER] += Vec4f(L, 1.f);
        }
    }

    void Li(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx, Vec4f values[]) const {
        if(m_bUseResampling) {
            return Li_resampling(primRay, threadID, pixelIdx, values);
        }
        return Li_noResampling(primRay, threadID, pixelIdx, values);
    }

    virtual std::string getName() const {
        return "SkelPathtracePreprocessNodesRenderer";
    }

    void getNearestNodesFromGBuffer() {
        auto size = getFramebuffer().getPixelCount();
        m_Data.resizeBuffers(size);

        auto pCamera = dynamic_cast<const ProjectiveCamera*>(&getCamera());
        if(!pCamera) {
            throw std::runtime_error("SkelPathtraceRenderer only works with projective camera");
        }

        m_GBuffer.init(getFramebuffer().getWidth(), getFramebuffer().getHeight());
        m_GBufferRendererPass.render(
                    pCamera->getProjMatrix(),
                    pCamera->getViewMatrix(),
                    pCamera->getZFar(),
                    m_GLScene,
                    m_GBuffer);

        auto count = m_GBuffer.getWidth() * m_GBuffer.getHeight();

        GLQuery query;
        m_Viewer.computeGPUSkeletonData(query);

        m_GLSkelData.m_NodeCubeMapContainer.makeResident();
        m_SkelMappingComputePass.computeFromGBuffer(
                    m_GBuffer,
                    *pCamera,
                    m_GLSkelData,
                    m_fSMBias,
                    m_nComputeNodeWeightSubIndex,
                    m_Data.gpuNodesBuffer.getGPUAddress(),
                    m_Data.gpuWeightsBuffer.getGPUAddress());
        m_GLSkelData.m_NodeCubeMapContainer.makeNonResident();

        m_Data.gpuNodesBuffer.getData(0, count, m_Data.nodes.data());
        m_Data.gpuWeightsBuffer.getData(0, count, m_Data.weights.data());
    }

    void computeSkelNodeIntersections() {
        auto pSkel = getScene().getSegmentedCurvSkeleton();
        auto completeSize = m_nIntersectionCountPerNode * pSkel->size();
        m_SkelNodeIntersectionsArray = makeUniqueArray<Sample<Intersection>>(completeSize);
        m_SkelNodeIntersectionsIrradianceArray = makeUniqueArray<Vec3f>(completeSize);
        m_nDistributionSize = getDistribution1DBufferSize(m_nIntersectionCountPerNode);
        m_SkelNodeDistributionsArray = makeUniqueArray<float>(m_nDistributionSize * pSkel->size());

        // For each node, compute intersections, irradiance values and distributions
        launchThreads([pSkel, this](uint32_t threadID) {
            uint32_t i = 0u;

            while(true) {
                auto nodeIndex = i * getThreadCount() + threadID;
                i++;

                if(nodeIndex >= pSkel->size()) {
                    break;
                }

                JitteredDistribution2D jitter(m_nNodeSamplingGridWidth, m_nNodeSamplingGridHeight);

                auto nodePos = pSkel->getNode(nodeIndex).P;
                auto offset = nodeIndex * m_nIntersectionCountPerNode;
                auto distribOffset = nodeIndex * m_nDistributionSize;

                for(auto i = 0u; i < m_nIntersectionCountPerNode; ++i) {
                    auto uv = jitter.getSample(i, getFloat2(threadID));
                    auto dir = uniformSampleSphere(uv.x, uv.y);

                    auto& I = m_SkelNodeIntersectionsArray[offset + i].value = getScene().intersect(Ray(nodePos, dir.value));
                    if(I) {
                        auto V = nodePos - I.P;
                        auto rcpSqrDist = 1.f / dot(V, V);
                        auto normalDotProduct = dot(I.Ns, -dir.value);
                        auto pdfWrtArea = m_SkelNodeIntersectionsArray[offset + i].pdf = solidAngleToAreaPDF(dir.pdf, rcpSqrDist, normalDotProduct);

                        m_SkelNodeIntersectionsIrradianceArray[offset + i] =
                                estimateDirectIrradiance(getScene(),
                                                         m_EmissionSampler,
                                                         I,
                                                         getFloat(threadID),
                                                         getFloat(threadID),
                                                         getFloat2(threadID));
                    } else {
                        m_SkelNodeIntersectionsIrradianceArray[offset + i] = Vec3f(0.f);
                    }
                }

                buildDistribution1D([this, offset](uint32_t i) {
                    return luminance(m_SkelNodeIntersectionsIrradianceArray[offset + i]);
                }, m_SkelNodeDistributionsArray.get() + distribOffset, m_nIntersectionCountPerNode);
            }
        });
    }

    void preprocess() override {
        if(getScene().getLightContainer().hasChanged(m_Flag)) {
            m_bNeedPreprocess = true;
        }

        m_EmissionSampler.initFrame(getScene());

        if(!m_bUseSkelGridMapping && (m_fPSkel > 0.f || m_bUseResampling)) {
            getNearestNodesFromGBuffer();
        }
    }

    void beginFrame() override {
        if(m_fPSkel > 0.f || m_bUseResampling) {
            computeSkelNodeIntersections();
        }
    }

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override {
        auto spp = getSppCount();
        TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
            Vec4f L[8];
            std::fill(L, L + 8, Vec4f(0.f));
            auto pixelIdx = getPixelIndex(x, y);
            for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
                auto s2D = getPixelSample(threadID, sampleID);
                auto ray = getPrimaryRay(x, y, s2D);
                Li(ray, threadID, pixelIdx, L);
            }
            for(auto i = 0u; i < 8; ++i) {
                getFramebuffer().accumulate(i, pixelIdx, L[i]);
            }
        });
    }

    virtual void doExposeIO(TwBar* bar) {
        atb::addVarRW(bar, "pSkel", m_fPSkel);
        atb::addVarRW(bar, "Max node count", m_nMaxNodeCount);
        auto setHeuristicCB = [&](uint32_t idx) {
            setHeuristic(idx);
        };

        auto getHeuristicCB =[&]() -> uint32_t {
            return m_nMISHeuristic;
        };

        atb::addVarCB(bar, "MIS Heuristic", "Balance,Power,Simple",
                      setHeuristicCB, getHeuristicCB);
        atb::addVarRW(bar, ATB_VAR(m_nSphereMapHeight));
        atb::addVarRW(bar, ATB_VAR(m_bUseCosinePSkel));
        atb::addVarRW(bar, ATB_VAR(m_nComputeNodeWeightSubIndex));
        atb::addVarRW(bar, ATB_VAR(m_fSMBias));
        atb::addVarRW(bar, ATB_VAR(m_bUseSkelGridMapping));
        atb::addVarRW(bar, ATB_VAR(m_bUseResampling));
        atb::addVarRW(bar, ATB_VAR(m_nResamplingSampleCount));

        //m_SkelSampler.exposeIO(bar);
    }

    virtual void doLoadSettings(const tinyxml2::XMLElement& xml) {
        getAttribute(xml, "pSkel", m_fPSkel);
        getAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
        getAttribute(xml, "misHeuristic", m_nMISHeuristic);
        getAttribute(xml, "sphereMapHeight", m_nSphereMapHeight);
        getAttribute(xml, "useCosinePSkel", m_bUseCosinePSkel);
        getAttribute(xml, "nodeWeightFunctionIndex", m_nComputeNodeWeightSubIndex);
        getAttribute(xml, "smBias", m_fSMBias);
        getAttribute(xml, "useGridSkelMapping", m_bUseSkelGridMapping);
        getAttribute(xml, "useResampling", m_bUseResampling);
        getAttribute(xml, "resamplingSampleCount", m_nResamplingSampleCount);

        setHeuristic(m_nMISHeuristic);
    }

    virtual void doStoreSettings(tinyxml2::XMLElement& xml) const {
        setAttribute(xml, "pSkel", m_fPSkel);
        setAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
        setAttribute(xml, "misHeuristic", m_nMISHeuristic);
        setAttribute(xml, "sphereMapHeight", m_nSphereMapHeight);
        setAttribute(xml, "useCosinePSkel", m_bUseCosinePSkel);
        setAttribute(xml, "nodeWeightFunctionIndex", m_nComputeNodeWeightSubIndex);
        setAttribute(xml, "smBias", m_fSMBias);
        setAttribute(xml, "useGridSkelMapping", m_bUseSkelGridMapping);
        setAttribute(xml, "useResampling", m_bUseResampling);
        setAttribute(xml, "resamplingSampleCount", m_nResamplingSampleCount);
    }
};

}
