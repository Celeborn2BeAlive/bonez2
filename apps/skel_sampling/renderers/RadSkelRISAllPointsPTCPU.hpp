#pragma once

#include "SkelSampler.hpp"
#include "SceneViewer.hpp"
#include "opengl/GLSkeletonData.hpp"
#include "opengl/GLSkeletonMappingComputePass.hpp"

#include <bonez/rendering/Renderer.hpp>
#include <bonez/sampling/multiple_importance_sampling.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/sampling/distribution1d.h>
#include <bonez/sampling/shapes.hpp>
#include <bonez/sampling/patterns.hpp>
#include <bonez/scene/lights/EmissionSampler.hpp>

namespace BnZ {

class RadSkelRISAllPointsPTCPU: public TileProcessingRenderer {
public:
    bool m_bRequireArrayRealloc = true;

    UpdateFlag m_Flag;
    float m_fPSkel = 0.5f;
    uint32_t m_nMaxNodeCount = 4;
    bool m_bUseSkelGridMapping = false;
    bool m_bUseSkelNodeNeighbours = false;

    bool m_bNeedPreprocess = true;
    EmissionSampler m_EmissionSampler;

    struct MappingData {
        std::vector<Vec4i> nodes;
        std::vector<Vec4f> weights;

        void resizeBuffers(uint32_t size) {
            nodes.resize(size);
            weights.resize(size);
        }
    };

    MappingData m_MappingData;

    uint32_t m_nNodeSamplingGridWidth = 32;
    Unique<Sample<Intersection>[]> m_SkelNodeIntersectionsArray; // For each node, store its m_nIntersectionCountPerNode intersections

    Unique<Vec3f[]> m_SkelNodeIntersectionsDirectRadianceArray; // For each node, store the irradiance of its intersections
    Unique<Vec3f[]> m_SkelNodeIntersectionsIncidentDirectionArray;
    Unique<Vec3f[]> m_SkelNodeIntersectionsRadiosityArray;

    Unique<float[]> m_SkelNodeDistributionsArray; // For each node, contains a discrete distribution among its intersections
    Unique<float[]> m_SkelNodePowerArray;
    uint32_t m_nDistributionSize;

    uint32_t m_nRISDistributionSize;

    // Thread datas
    Unique<Sample3f[]> m_Wi;
    Unique<float[]> m_WiLength;
    Unique<Intersection[]> m_SampledI;
    Unique<Vec3f[]> m_EstimatedRadiosity;
    Unique<Vec3f[]> m_EstimatedRadiance;
    Unique<Vec3f[]> m_WiSample;
    Unique<Vec3f[]> m_Fr;
    Unique<uint32_t[]> m_SampledNode;
    Unique<Vec3f[]> m_Contributions;
    Unique<float[]> m_Weights;
    Unique<float[]> m_RISDistributions;

    uint32_t getPointSampleCountPerNode() const {
        return sqr(m_nNodeSamplingGridWidth);
    }

    void skeletonMapping(const Intersection& I, uint32_t pixelIdx, Vec4i& nearestNodes, Vec4f& weights) const {
        if(m_bUseSkelGridMapping) {
            nearestNodes = Vec4i(getScene().getSegmentedCurvSkeleton()->getNearestNode(I), -1, -1, -1);
            weights = Vec4f(1, 0, 0, 0);
        } else {
            nearestNodes = m_MappingData.nodes[pixelIdx];
            weights = m_MappingData.weights[pixelIdx];
        }
    }

    Vec3f estimateDirectRadiance(
            const SurfacePoint& point,
            float sLightIdx, float s1D, const Vec2f& s2D,
            Sample3f& wiSample) {
        auto lightNode = m_EmissionSampler.sample(getScene(), sLightIdx, s1D, s2D);
        Ray shadowRay;
        auto G = lightNode->G(point, shadowRay);

        if(G > 0.f && !getScene().occluded(shadowRay)) {
            auto L = lightNode->power() * lightNode->Le(-shadowRay.dir);
            return L * G / lightNode->pdf();
        }

        wiSample.pdf = 0.f;
        return zero<Vec3f>();

        //        RaySample r;
//        auto L = m_DirectLightSampler.sample(getScene(), point, r, sLightIdx, s1D, s2D);

//        wiSample.value = r.value.dir;

//        auto d = dot(point.Ns, r.value.dir);

//        if(d <= 0.f) {
//            wiSample.pdf = 0.f;
//        } else {
//            wiSample.pdf = r.pdf / d; // pdf wrt. projected solid angle on the point
//        }

//        if(getScene().occluded(r.value)) {
//            return zero<Vec3f>();
//        }

//        return L;
    }

    Vec3f sampleSkeleton(uint32_t threadID,
                         uint32_t pixelIdx,
                         const Intersection& I,
                         const Vec3f& wo, const BRDF& brdf,
                         Sample3f& wi,
                         float& wiLength,
                         Intersection& sampledI,
                         Vec3f& estimatedRadiosity,
                         Vec3f& estimatedRadiance,
                         Vec3f& wiSample,
                         uint32_t& sampledNode) const {
        Vec4i nearestNodes;
        Vec4f weights;
        skeletonMapping(I, pixelIdx, nearestNodes, weights);

        auto nodeSample = sampleNodeFromWeights(nearestNodes,
                                                weights,
                                                m_nMaxNodeCount,
                                                getFloat(threadID));


        if(nodeSample.pdf > 0.f) {
            auto nodeIdx = nearestNodes[nodeSample.value];

            if(nodeIdx >= 0) {
                // Choosing a neighbour ? let's try => it works better
                if(m_bUseSkelNodeNeighbours) {
                    auto pSkel = getScene().getSegmentedCurvSkeleton();
                    auto nodeCount = 1 + pSkel->neighbours(nodeIdx).size();
                    auto r1D = getFloat(threadID);
                    auto idx = min(uint32_t(nodeCount * r1D), uint32_t(nodeCount - 1));
                    if(idx > 0) {
                        nodeIdx = pSkel->neighbours(nodeIdx)[idx - 1];
                    }
                }

                sampledNode = nodeIdx;

                auto offset = nodeIdx * getPointSampleCountPerNode();
                auto distribOffset = nodeIdx * m_nDistributionSize;


                // Resampling of a point based on precomputed radiosities
                auto sample = sampleDiscreteDistribution1D(m_SkelNodeDistributionsArray.get() + distribOffset,
                                                           getPointSampleCountPerNode(),
                                                           getFloat(threadID));

                if(sample.pdf > 0.f) {
                    auto& tmp = m_SkelNodeIntersectionsArray[offset + sample.value];
                    sampledI = tmp.value;
                    estimatedRadiosity = m_SkelNodeIntersectionsRadiosityArray[offset + sample.value];
                    estimatedRadiance = m_SkelNodeIntersectionsDirectRadianceArray[offset + sample.value];
                    wiSample = m_SkelNodeIntersectionsIncidentDirectionArray[offset + sample.value];
                    wi.value = sampledI.P - I.P;
                    wiLength = length(wi.value);
                    wi.value /= wiLength;

                    auto fr = brdf.eval(wi.value, wo);

                    if(fr != zero<Vec3f>()) {
                        wi.pdf = getPointSampleCountPerNode() *
                                sample.pdf *
                                areaToSolidAnglePDF(tmp.pdf, sqr(wiLength), dot(sampledI.Ns, -wi.value));
                    }

                    return fr;
                }
            }
        }

        wi.pdf = 0.f;
        return zero<Vec3f>();
    }

    float pdfSkeleton(uint32_t threadID,
                      uint32_t pixelIdx,
                      const Intersection& I,
                      const Vec3f& estimatedRadiosity,
                      uint32_t nodeID,
                      uint32_t sampledNodeID) const {
        if(m_SkelNodePowerArray[nodeID] == 0.f) {
            return 0.f;
        }

        auto pSkel = getScene().getSegmentedCurvSkeleton();
        auto node = pSkel->getNode(nodeID);

        auto dir = node.P - I.P;
        auto l = length(dir);
        if(l == 0.f) {
            return 0.f;
        }
        dir /= l;

        // Perf or correctness
        if(nodeID != sampledNodeID && getScene().occluded(Ray(I, dir, l))) {
            return 0.f;
        }

        return luminance(estimatedRadiosity / m_SkelNodePowerArray[nodeID]) * solidAngleToAreaPDF(uniformSampleSpherePDF(), 1.f / (l * l), dot(dir, I.Ns));
    }

    float pdfSkeletonWithNeighbours(uint32_t threadID,
                                    uint32_t pixelIdx,
                                    const Intersection& I,
                                    const Vec3f& estimatedRadiosity,
                                    uint32_t nodeID,
                                    uint32_t sampledNodeID) const {
        if(!m_bUseSkelNodeNeighbours) {
            return pdfSkeleton(threadID, pixelIdx, I, estimatedRadiosity, nodeID, sampledNodeID);
        }

        auto pSkel = getScene().getSegmentedCurvSkeleton();
        auto size = pSkel->neighbours(nodeID).size() + 1;
        auto w = 1.f / size;

        auto p = 0.f;
        p += pdfSkeleton(threadID, pixelIdx, I, estimatedRadiosity, nodeID, sampledNodeID);
        for(auto& n: pSkel->neighbours(nodeID)) {
            p += pdfSkeleton(threadID, pixelIdx, I, estimatedRadiosity, n, sampledNodeID);
        }
        return w * p;
    }

    float pdfSkeletonWrtArea(uint32_t threadID,
                             uint32_t pixelIdx,
                             const Intersection& I,
                             const Vec3f& estimatedRadiosity,
                             const Intersection& shadingI,
                             uint32_t sampledNodeID) const {
        Vec4i nearestNodes;
        Vec4f nodeWeight;
        skeletonMapping(shadingI, pixelIdx, nearestNodes, nodeWeight);

        // Here we should do that:
        auto size = 0u;
        Vec4f weights = zero<Vec4f>();
        for (size = 0u;
             size < min(m_nMaxNodeCount, 4u) && nearestNodes[size] >= 0;
             ++size) {
            weights[size] = nodeWeight[size];
        }

        if(size == 0u) {
            return 0.f;
        }

        DiscreteDistribution4f dist(weights);
        float p = 0.f;
        for(auto i = 0u; i < size; ++i) {
            float pdf = pdfSkeletonWithNeighbours(threadID, pixelIdx, I, estimatedRadiosity, nearestNodes[i], sampledNodeID);
            if(pdf > 0.f) {
                p += dist.pdf(i) * pdf;
            }
        }
        return p;
    }

    // Return the pdf of sampling the direction (shadingI -> I) with the skeleton
    float pdfSkeleton(uint32_t threadID,
                      uint32_t pixelIdx,
                      const Intersection& I,
                      const Vec3f& estimatedRadiosity,
                      const Intersection& shadingI,
                      uint32_t sampledNodeID) const {
        auto pdfWrtArea = pdfSkeletonWrtArea(threadID, pixelIdx, I, estimatedRadiosity, shadingI, sampledNodeID);

        auto dir = shadingI.P - I.P;
        auto l = length(dir);

        if(l == 0.f) {
            return 0.f;
        }
        dir /= l;

        return areaToSolidAnglePDF(pdfWrtArea, l * l, dot(dir, I.Ns));
    }

    enum RenderTarget {
        FINAL_RENDER = 0,
        SKEL_CONTRIBUTION = 1,
        BRDF_CONTRIBUTION = 2,
        SKEL_CHOSEN = 3,
        BRDF_CHOSEN = 4,
        SKEL_COHERENCY = 5,
        VARIANCE = 6,
        CHOSEN_NODE = 7
    };

    void Li_one_sample_strategy(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx, Vec4f values[]) const {
        auto I = getScene().intersect(primRay);

        if(!I) {
            values[FINAL_RENDER] += Vec4f(badColor(), 1.f);
            return;
        }

        auto brdf = getScene().shade(I);
        auto wo = -primRay.dir.xyz();

        auto L = zero<Vec3f>();

        {
            auto s1D = getFloat(threadID);

            auto pSkel = m_fPSkel;

            // Sample the skeleton
            if(s1D <= pSkel)
            {
                float weightsMean = 0.f;

                Vec4i nearestNodes;
                Vec4f weights;
                skeletonMapping(I, pixelIdx, nearestNodes, weights);

                auto offset = threadID * m_nRISDistributionSize;
                auto nbPointSample = 0u;
                for(auto i = 0u; i < max(m_nMaxNodeCount, 4u) && nearestNodes[i] >= 0; ++i) {
                    auto nodeIdx = nearestNodes[i];
                    auto nodeOffset = nodeIdx * getPointSampleCountPerNode();

                    for(auto j = 0u; j < getPointSampleCountPerNode(); ++j) {
                        auto k = offset + nbPointSample;

                        m_SampledNode[k] = nodeIdx;
                        auto& tmp = m_SkelNodeIntersectionsArray[nodeOffset + j];
                        m_SampledI[k] = tmp.value;
                        m_EstimatedRadiosity[k] = m_SkelNodeIntersectionsRadiosityArray[nodeOffset + j];
                        m_EstimatedRadiance[k] = m_SkelNodeIntersectionsDirectRadianceArray[nodeOffset + j];
                        m_WiSample[k] = m_SkelNodeIntersectionsIncidentDirectionArray[nodeOffset + j];
                        m_Wi[k].value = m_SampledI[k].P - I.P;
                        m_WiLength[k] = length(m_Wi[k].value);
                        m_Wi[k].value /= m_WiLength[k];

                        m_Fr[k] = brdf.eval(m_Wi[k].value, wo);

                        m_Wi[k].pdf = areaToSolidAnglePDF(tmp.pdf, sqr(m_WiLength[k]), dot(m_SampledI[k].Ns, -m_Wi[k].value));

                        bool isValid = (m_Fr[k] != Vec3f(0.f) &&
                                m_Wi[k].pdf != 0.f && dot(I.Ns, m_Wi[k].value) > 0.f);
                        if(isValid && m_EstimatedRadiance[k] != zero<Vec3f>()) {
                            auto brdfSampleI = getScene().shade(m_SampledI[k]);
                            auto Li = brdfSampleI.eval(m_WiSample[k], -m_Wi[k].value) * m_EstimatedRadiance[k];
                            m_Contributions[k] = m_Fr[k] * Li * max(0.f, dot(I.Ns.xyz(), m_Wi[k].value))
                                    / m_Wi[k].pdf;
                            m_Weights[k] = luminance(m_Contributions[k]);

                            weightsMean += m_Weights[k];

                            ++nbPointSample;
                        } else {
                            m_Contributions[k] = Vec3f(0.f);
                            m_Weights[k] = 0.f;
                        }
                    }
                }

                auto pDistrib = m_RISDistributions.get() + threadID * getDistribution1DBufferSize(m_nRISDistributionSize);
                buildDistribution1D([this, offset](uint32_t i) {
                    return m_Weights[offset + i];
                }, pDistrib, nbPointSample);

                auto sample = sampleDiscreteDistribution1D(pDistrib, nbPointSample, getFloat(threadID));

                if(weightsMean > 0.f && sample.pdf > 0.f) {
                    auto idx = offset + sample.value;

                    values[CHOSEN_NODE] += Vec4f(getColor(m_SampledNode[idx]), 1.f);

                    if(getScene().occluded(Ray(I, m_SampledI[idx], m_Wi[idx].value, m_WiLength[idx]))) {
                        values[SKEL_COHERENCY] += Vec4f(0, 0, 0, 1);
                        values[FINAL_RENDER] += Vec4f(Vec3f(0.f), 1.f);
                    } else {
                        values[SKEL_COHERENCY] += Vec4f(1, 1, 1, 1);

                        /*float pdfWrtSkel = pdfSkeleton(threadID,
                                                 pixelIdx,
                                                 m_SampledI[idx],
                                                 m_EstimatedRadiosity[idx],
                                                 I,
                                                 m_SampledNode[idx]);*/

                        //float w = pSkel * pdfWrtSkel / (pSkel * pdfWrtSkel + (1 - pSkel) * brdf.pdf(wo, m_Wi[idx].value));

                        float w = 1.f;

                        auto Ltmp = m_Contributions[idx] / (m_nRISDistributionSize * pSkel * sample.pdf);

                        L += w * Ltmp;
                        values[1] += Vec4f(Ltmp, 1.f);
                        values[FINAL_RENDER] += Vec4f(L, 1.f);
                    }
                } else {
                    values[FINAL_RENDER] += Vec4f(Vec3f(0.f), 1.f);
                }
            } else
            // Sample the brdf
            {
                Sample3f wiSample;
                auto fr = brdf.sample(wo, wiSample, getFloat2(threadID));

                if(wiSample.pdf > 0.f) {
                    auto Ireflect = getScene().intersect(Ray(I, wiSample.value));
                    if(Ireflect) {
                        Vec3f irradiance;
                        auto brdfReflectI = getScene().shade(Ireflect);
                        auto Li = estimateDirectLightAndDirectIrradiance(getScene(),
                                                      m_EmissionSampler,
                                                      Ireflect, -wiSample.value,
                                                      brdfReflectI,
                                                      irradiance,
                                                      getFloat(threadID),
                                                      getFloat(threadID),
                                                      getFloat2(threadID));

                        float pdfWrtSkel = pdfSkeleton(threadID,
                                                       pixelIdx,
                                                       Ireflect,
                                                       brdfReflectI.diffuseTerm() * irradiance,
                                                       I,
                                                       -1);

                        float w = (1 - pSkel) * wiSample.pdf / ((1 - pSkel) * wiSample.pdf + pSkel * pdfWrtSkel);

                        auto Ltmp = fr * Li * max(0.f, dot(I.Ns.xyz(), wiSample.value))
                                / ((1 - pSkel)  * wiSample.pdf);

                        L += w * Ltmp;
                        values[2] += Vec4f(Ltmp, 1.f);
                        values[FINAL_RENDER] += Vec4f(L, 1.f);
                    }
                }
            }
        }
    }

    void Li(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx, Vec4f values[]) const {
        Li_one_sample_strategy(primRay, threadID, pixelIdx, values);
    }

    virtual std::string getName() const {
        return "RadSkelRISAllPointsPTCPU";
    }

    void computeSkelNodeIntersections() {
        auto pSkel = getScene().getSegmentedCurvSkeleton();

        // For each node, compute intersections, irradiance values and distributions
        launchThreads([pSkel, this](uint32_t threadID) {
            uint32_t i = 0u;

            while(true) {
                auto nodeIndex = i * getThreadCount() + threadID;
                i++;

                if(nodeIndex >= pSkel->size()) {
                    break;
                }

                JitteredDistribution2D jitter(m_nNodeSamplingGridWidth, m_nNodeSamplingGridWidth);

                auto nodePos = pSkel->getNode(nodeIndex).P;
                auto offset = nodeIndex * getPointSampleCountPerNode();
                auto distribOffset = nodeIndex * m_nDistributionSize;

                m_SkelNodePowerArray[nodeIndex] = 0.f;
                for(auto i = 0u, count = getPointSampleCountPerNode(); i < count; ++i) {
                    auto uv = jitter.getSample(i, getFloat2(threadID));
                    auto dir = uniformSampleSphere(uv.x, uv.y);

                    auto& I = m_SkelNodeIntersectionsArray[offset + i].value = getScene().intersect(Ray(nodePos, dir.value));
                    if(I) {
                        auto V = nodePos - I.P;
                        auto rcpSqrDist = 1.f / dot(V, V);
                        auto normalDotProduct = dot(I.Ns, -dir.value);
                        auto pdfWrtArea = m_SkelNodeIntersectionsArray[offset + i].pdf = solidAngleToAreaPDF(dir.pdf, rcpSqrDist, normalDotProduct);

                        auto brdf = getScene().shade(I);

                        Sample3f wiSample;
                        auto Li = estimateDirectRadiance(I,
                                                         getFloat(threadID),
                                                         getFloat(threadID),
                                                         getFloat2(threadID),
                                                         wiSample);

                        if(wiSample.pdf > 0.f && Li != zero<Vec3f>()) {
                            m_SkelNodeIntersectionsDirectRadianceArray[offset + i] = Li / wiSample.pdf; // Monte-Carlo estimation
                            m_SkelNodeIntersectionsIncidentDirectionArray[offset + i] = wiSample.value;

                            Sample3f woSample;

                            auto fr = brdf.sample(wiSample.value, woSample, getFloat2(threadID));

                            if(woSample.pdf > 0.f && fr != zero<Vec3f>()) {
                                m_SkelNodeIntersectionsRadiosityArray[offset + i] = fr * m_SkelNodeIntersectionsDirectRadianceArray[offset + i] * max(0.f, dot(I.Ns, woSample.value)) / woSample.pdf;
                            } else {
                                m_SkelNodeIntersectionsRadiosityArray[offset + i] = zero<Vec3f>();
                            }
                        } else {
                            m_SkelNodeIntersectionsDirectRadianceArray[offset + i] = zero<Vec3f>();
                            m_SkelNodeIntersectionsRadiosityArray[offset + i] = zero<Vec3f>();
                        }

                        m_SkelNodePowerArray[nodeIndex] += luminance(m_SkelNodeIntersectionsRadiosityArray[offset + i]);
                    } else {
                        m_SkelNodeIntersectionsDirectRadianceArray[offset + i] = zero<Vec3f>();
                        m_SkelNodeIntersectionsRadiosityArray[offset + i] = zero<Vec3f>();
                    }
                }

                m_SkelNodePowerArray[nodeIndex] /= float(getPointSampleCountPerNode());


                buildDistribution1D([this, offset](uint32_t i) {
                    return luminance(m_SkelNodeIntersectionsRadiosityArray[offset + i]/* / m_SkelNodeIntersectionsArray[offset + i].pdf*/);
                }, m_SkelNodeDistributionsArray.get() + distribOffset, getPointSampleCountPerNode());
            }
        });
    }

    void computeSkeletonMapping() {
        auto framebufferSize = getFramebuffer().getSize();
        m_MappingData.resizeBuffers(framebufferSize.x * framebufferSize.y);

        Vec2u tileCount = framebufferSize / getTileSize() +
                Vec2u(framebufferSize % getTileSize() != zero<Vec2u>());

        auto totalCount = tileCount.x * tileCount.y;

        auto pSkel = getScene().getSegmentedCurvSkeleton();

        auto task = [&](uint32_t threadID) {
            auto loopID = 0u;
            while(true) {
                auto tileID = loopID * getThreadCount() + threadID;
                ++loopID;

                if(tileID >= totalCount) {
                    return;
                }

                uint32_t tileX = tileID % tileCount.x;
                uint32_t tileY = tileID / tileCount.x;

                Vec2u tileOrg = Vec2u(tileX, tileY) * getTileSize();
                auto viewport = Vec4u(tileOrg, getTileSize());

                if(viewport.x + viewport.z > framebufferSize.x) {
                    viewport.z = framebufferSize.x - viewport.x;
                }

                if(viewport.y + viewport.w > framebufferSize.y) {
                    viewport.w = framebufferSize.y - viewport.y;
                }

                auto xEnd = viewport.x + viewport.z;
                auto yEnd = viewport.y + viewport.w;

                for(auto y = viewport.y; y < yEnd; ++y) {
                    for(auto x = viewport.x; x < xEnd; ++x) {
                        auto pixelIdx = getPixelIndex(x, y);
                        auto s2D = Vec2f(0.5f, 0.5f);
                        auto ray = getPrimaryRay(x, y, s2D);
                        auto I = getScene().intersect(ray);
                        if(I) {
                            m_MappingData.nodes[pixelIdx] = getNearestNodes(*pSkel, I.P, I.Ns, m_MappingData.weights[pixelIdx],
                                                [](uint32_t nodeIdx, Vec3f dir, float dist) { return false; });
                        } else {
                            m_MappingData.nodes[pixelIdx] = Vec4i(-1);
                        }
                    }
                }
            }
        };

        launchThreads(task);
    }

    void allocArrays() {
        if(m_bRequireArrayRealloc) {
            m_nRISDistributionSize = getPointSampleCountPerNode() * m_nMaxNodeCount;

            auto size = m_nRISDistributionSize * getThreadCount();
            // Alloc memory
            m_Wi = makeUniqueArray<Sample3f>(size);
            m_WiLength = makeUniqueArray<float>(size);
            m_SampledI = makeUniqueArray<Intersection>(size);
            m_EstimatedRadiosity = makeUniqueArray<Vec3f>(size);
            m_EstimatedRadiance = makeUniqueArray<Vec3f>(size);
            m_WiSample = makeUniqueArray<Vec3f>(size);
            m_Fr = makeUniqueArray<Vec3f>(size);
            m_SampledNode = makeUniqueArray<uint32_t>(size);
            m_Contributions = makeUniqueArray<Vec3f>(size);
            m_Weights = makeUniqueArray<float>(size);
            m_RISDistributions = makeUniqueArray<float>(getDistribution1DBufferSize(m_nRISDistributionSize) * getThreadCount());

            auto pSkel = getScene().getSegmentedCurvSkeleton();
            auto completeSize = getPointSampleCountPerNode() * pSkel->size();
            m_SkelNodeIntersectionsArray = makeUniqueArray<Sample<Intersection>>(completeSize);
            m_SkelNodeIntersectionsDirectRadianceArray = makeUniqueArray<Vec3f>(completeSize);
            m_SkelNodeIntersectionsIncidentDirectionArray = makeUniqueArray<Vec3f>(completeSize);
            m_SkelNodeIntersectionsRadiosityArray = makeUniqueArray<Vec3f>(completeSize);
            m_nDistributionSize = getDistribution1DBufferSize(getPointSampleCountPerNode());
            m_SkelNodeDistributionsArray = makeUniqueArray<float>(m_nDistributionSize * pSkel->size());
            m_SkelNodePowerArray = makeUniqueArray<float>(pSkel->size());

            m_bRequireArrayRealloc = false;
        }
    }

    void preprocess() override {
        allocArrays();

        if(getScene().getLightContainer().hasChanged(m_Flag)) {
            m_bNeedPreprocess = true;
        }

        m_EmissionSampler.initFrame(getScene());

        if(!m_bUseSkelGridMapping) {
            computeSkeletonMapping();
        }
    }

    void beginFrame() override {
        allocArrays();
        computeSkelNodeIntersections();
    }

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override {
        auto spp = getSppCount();
        TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
            Vec4f L[8];
            std::fill(L, L + 8, Vec4f(0.f));
            auto pixelIdx = getPixelIndex(x, y);
            for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
                auto s2D = getPixelSample(threadID, sampleID);
                auto ray = getPrimaryRay(x, y, s2D);
                Li(ray, threadID, pixelIdx, L);
            }
            for(auto i = 0u; i < 8; ++i) {
                getFramebuffer().accumulate(i, pixelIdx, L[i]);
            }
        });
    }

    virtual void doExposeIO(TwBar* bar) {
        auto allocCallback = [this]() {
            m_bRequireArrayRealloc = true;
        };

        atb::addVarRW(bar, "pSkel", m_fPSkel);
        atb::addVarRW(bar, "Max node count", m_nMaxNodeCount);
        atb::addVarRW(bar, ATB_VAR(m_bUseSkelGridMapping));
        atb::addVarRW(bar, ATB_VAR(m_bUseSkelNodeNeighbours));
        atb::addVarRWCB(bar, ATB_VAR(m_nNodeSamplingGridWidth), allocCallback);
    }

    virtual void doLoadSettings(const tinyxml2::XMLElement& xml) {
        getAttribute(xml, "pSkel", m_fPSkel);
        getAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
        getAttribute(xml, "useGridSkelMapping", m_bUseSkelGridMapping);
        getAttribute(xml, "useSkelNodeNeighbours", m_bUseSkelNodeNeighbours);
        getAttribute(xml, "nodeSamplingGridWidth", m_nNodeSamplingGridWidth);
    }

    virtual void doStoreSettings(tinyxml2::XMLElement& xml) const {
        setAttribute(xml, "pSkel", m_fPSkel);
        setAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
        setAttribute(xml, "useGridSkelMapping", m_bUseSkelGridMapping);
        setAttribute(xml, "useSkelNodeNeighbours", m_bUseSkelNodeNeighbours);
        setAttribute(xml, "nodeSamplingGridWidth", m_nNodeSamplingGridWidth);
    }
};

}
