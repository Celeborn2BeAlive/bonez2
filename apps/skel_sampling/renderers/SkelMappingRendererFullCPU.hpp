#pragma once

#include <bonez/rendering/Renderer.hpp>

namespace BnZ {

class SkelMappingRendererFullCPU: public TileProcessingRenderer {
public:
    using GetNearestNodeFunction = std::function<int(const SurfacePoint& point)>;
    GetNearestNodeFunction m_GetNearestNode;

    SkelMappingRendererFullCPU(const GetNearestNodeFunction& getNearestNode):
        m_GetNearestNode(getNearestNode) {
    }

    virtual std::string getName() const {
        return "SkelMappingRendererFullCPU";
    }

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override {
        auto spp = getSppCount();
        TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
            auto pixelIdx = getPixelIndex(x, y);
            Vec4f L(0.f);
            for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
                auto s2D = getPixelSample(threadID, sampleID);
                auto ray = getPrimaryRay(x, y, s2D);
                auto I = getScene().intersect(ray);

                auto node = m_GetNearestNode(I);
                L += Vec4f(getColor(node), 1.f);
            }
            getFramebuffer().accumulate(pixelIdx, L);
        });
    }
};

}
