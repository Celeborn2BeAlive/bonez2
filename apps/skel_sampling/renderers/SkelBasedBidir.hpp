#pragma once

#include "SkelSampler.hpp"
#include "SceneViewer.hpp"
#include "opengl/GLSkeletonData.hpp"
#include "opengl/GLSkeletonMappingComputePass.hpp"

#include <bonez/rendering/Renderer.hpp>
#include <bonez/sampling/multiple_importance_sampling.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/sampling/distribution1d.h>
#include <bonez/sampling/shapes.hpp>
#include <bonez/sampling/patterns.hpp>
#include <bonez/scene/lights/EmissionSampler.hpp>

#include <bonez/opengl/debug/GLDebugRenderer.hpp>
#include <bonez/opengl/debug/GLDebugOutputStream.hpp>

namespace BnZ {

class SkelBasedBidir: public TileProcessingRenderer {
public:
    UpdateFlag m_Flag;
    float m_fPSkel = 0.5f;
    uint32_t m_nMaxNodeCount = 4;
    bool m_bUseSkelGridMapping = false;

    EmissionSampler m_EmissionSampler;

    struct MappingData {
        std::vector<Vec4i> nodes;
        std::vector<Vec4f> weights;

        void resizeBuffers(uint32_t size) {
            nodes.resize(size);
            weights.resize(size);
        }
    };

    MappingData m_MappingData;

    uint32_t m_nNodeLightPathCount = 1024;

    struct LightPath {
        SurfacePoint lastVertex;
        BRDF lastVertexBRDF;
        Vec3f incidentDirection;
        float pdf; // pdf of the complete path wrt. area product
        float pdfLastVertex; // pdf of the last vertex, conditional to the previous
        Vec3f power;
    };

    Unique<LightPath[]> m_SkelNodeLightPathArray;
    Unique<float[]> m_SkelNodeDistributionsArray; // For each node, contains a discrete distribution among its light paths
    Unique<float[]> m_SkelNodePowerArray;
    uint32_t m_nDistributionSize;

    Shared<GLDebugRenderer> m_pDebugRenderer;
    mutable GLDebugOutputStream m_DebugStream;

    SkelBasedBidir(const Shared<GLDebugRenderer>& debugRenderer):
        m_pDebugRenderer(debugRenderer), m_DebugStream(newGLDebugStream(m_pDebugRenderer, "SkelBasedBidir" + toString(getMicroseconds()))) {
    }

    uint32_t getLightPathCountPerNode() const {
        return m_nNodeLightPathCount;
    }

    void skeletonMapping(const Intersection& I, uint32_t pixelIdx, Vec4i& nearestNodes, Vec4f& weights) const {
        if(m_bUseSkelGridMapping) {
            nearestNodes = Vec4i(getScene().getSegmentedCurvSkeleton()->getNearestNode(I), -1, -1, -1);
            weights = Vec4f(1, 0, 0, 0);
        } else {
            nearestNodes = m_MappingData.nodes[pixelIdx];
            weights = m_MappingData.weights[pixelIdx];
            /*for(auto i = 0u; i < m_nMaxNodeCount && nearestNodes[i] >= 0; ++i) {
                weights[i] *= m_SkelNodePowerArray[nearestNodes[i]];
            }*/
        }
    }

    // Sample a light path from a skeleton node
    bool sampleSkeletonNode(uint32_t threadID,
                            uint32_t pixelIdx,
                            uint32_t nodeIdx,
                            LightPath& lightPath,
                            Sample1u& lightPathDiscreteSample) const {
        auto offset = nodeIdx * getLightPathCountPerNode();
        auto distribOffset = nodeIdx * m_nDistributionSize;

        // Resampling of a point based on precomputed radiosities
        lightPathDiscreteSample = sampleDiscreteDistribution1D(m_SkelNodeDistributionsArray.get() + distribOffset,
                                                               getLightPathCountPerNode(),
                                                               getFloat(threadID));
        if(lightPathDiscreteSample.pdf > 0.f) {
            lightPath = m_SkelNodeLightPathArray[offset + lightPathDiscreteSample.value];
            return true;
        }

        return false;
    }

    bool sampleSkeleton(uint32_t threadID,
                        uint32_t pixelIdx,
                        Vec4i pixelNodes,
                        Vec4f pixelNodeWeights,
                        LightPath& lightPath,
                        Sample1u& lightPathDiscreteSample,
                        Sample1u& sampledNodeChannel,
                        int& sampledNodeIdx) const {
        Vec4f weights = pixelNodeWeights;
        for(auto i = 0u; i < m_nMaxNodeCount && pixelNodes[i] >= 0; ++i) {
            weights[i] *= m_SkelNodePowerArray[pixelNodes[i]];
        }

        sampledNodeChannel = sampleNodeFromWeights(pixelNodes,
                                                weights,
                                                m_nMaxNodeCount,
                                                getFloat(threadID));

        if(sampledNodeChannel.pdf > 0.f) {
            sampledNodeIdx = pixelNodes[sampledNodeChannel.value];

            if(sampledNodeIdx >= 0) {
                return sampleSkeletonNode(threadID, pixelIdx, sampledNodeIdx, lightPath, lightPathDiscreteSample);
            }
        }

        return false;
    }

    bool sampleLightPathFromLastVertex(uint32_t threadID,
                                       uint32_t pixelIdx,
                                       const SurfacePoint& lastVertex,
                                       LightPath& lightPath) const {
        lightPath.lastVertex = lastVertex;
        lightPath.lastVertexBRDF = getScene().shade(lightPath.lastVertex);

        auto lightNode = m_EmissionSampler.sample(getScene(), getFloat(threadID),
                                                  getFloat(threadID), getFloat2(threadID));

        Ray shadowRay;
        auto G = lightNode->G(lastVertex, shadowRay);

        lightPath.incidentDirection = shadowRay.dir;

        if(G > 0.f && !getScene().occluded(shadowRay)) {
            auto contrib = lightNode->getRadiantExitance() *
                lightNode->Le(-shadowRay.dir) * G;
            lightPath.power = contrib / lightNode->pdf();
            lightPath.pdfLastVertex = lightNode->pdfWrtArea(lightPath.lastVertex);
            lightPath.pdf = lightNode->pdf() * lightPath.pdfLastVertex;

            return true;
        }

        lightPath.power = Vec3f(0.f);
        lightPath.pdf = 0.f;
        lightPath.pdfLastVertex = 0.f;

        return false;
    }

    float pdfSkeleton(uint32_t threadID,
                      uint32_t pixelIdx,
                      Vec4i pixelNodes,
                      Vec4f pixelNodeWeights,
                      const LightPath& lightPath,
                      uint32_t sampledNodeID = -1) const {
        return lightPath.pdf;

        auto size = 0u;
        Vec4f weights = zero<Vec4f>();
        for (size = 0u;
             size < min(m_nMaxNodeCount, 4u) && pixelNodes[size] >= 0;
             ++size) {
            weights[size] = pixelNodeWeights[size];
        }

        if(size == 0u) {
            return 0.f;
        }

        auto pSkel = getScene().getSegmentedCurvSkeleton();

        DiscreteDistribution4f dist(weights);
        float p = 0.f;
        for(auto i = 0u; i < size; ++i) {
            auto node = pSkel->getNode(pixelNodes[i]);

            auto dir = node.P - lightPath.lastVertex.P;
            auto l = length(dir);
            if(l > 0.f) {
                dir /= l;
                if(pixelNodes[i] == sampledNodeID || (dot(lightPath.lastVertex.Ns, dir) > 0.f &&
                                                      !getScene().occluded(Ray(lightPath.lastVertex, dir, l)))) {
                    p += dist.pdf(i);
                }
            }
        }

        return p * lightPath.pdf;
    }

    enum RenderTarget {
        FINAL_RENDER = 0,
        SKEL_CONTRIB = 1,
        BRDF_CONTRIB = 2,
        SKEL_WEIGHT = 3,
        BRDF_WEIGHT = 4,
        SKEL_COHERENCY = 5,
        VARIANCE = 6,
        SAMPLED_NODE = 7
    };

    void Li_one_sample_strategy(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx, Vec4f values[]) const {
        auto I = getScene().intersect(primRay);

        if(!I) {
            values[FINAL_RENDER] += Vec4f(badColor(), 1.f);
            return;
        }

        auto brdf = getScene().shade(I);
        auto wo = -primRay.dir.xyz();

        auto L = zero<Vec3f>();

        {
            Vec4i pixelNodes;
            Vec4f pixelNodeWeights;
            skeletonMapping(I, pixelIdx, pixelNodes, pixelNodeWeights);

            auto s1D = getFloat(threadID);
            auto pSkel = m_fPSkel;

            // Sample the skeleton
            if(s1D <= pSkel)
            {
                LightPath lightPath;
                Sample1u lightPathDiscreteSample;
                Sample1u sampledNodeChannel;
                int sampledNodeIdx;

                if(sampleSkeleton(threadID, pixelIdx, pixelNodes, pixelNodeWeights, lightPath, lightPathDiscreteSample,
                                  sampledNodeChannel, sampledNodeIdx)) {
                    values[SAMPLED_NODE] += Vec4f(getColor(sampledNodeIdx), 1.f);


                    Vec3f incidentDirection;
                    float dist;
                    auto G = geometricFactor(I, lightPath.lastVertex, incidentDirection, dist);
                    Ray incidentRay(I, lightPath.lastVertex, incidentDirection, dist);

                    if(G > 0.f && !getScene().occluded(incidentRay)) {
                        values[SKEL_COHERENCY] += Vec4f(Vec3f(1.f), 1);

                        auto pdfSkel = pdfSkeleton(threadID, pixelIdx, pixelNodes, pixelNodeWeights, lightPath, sampledNodeIdx);
                        auto d = distance(I.P, lightPath.lastVertex.P);
                        auto pdfBRDF = (lightPath.pdf / lightPath.pdfLastVertex) * brdf.pdf(wo, incidentDirection) *
                                max(0.f, dot(lightPath.lastVertex.Ns, -incidentDirection)) / (d * d);
                        auto misWeight = 1.f / (1.f + (1 - pSkel) * pdfBRDF / (pSkel * pdfSkel));

                        auto contrib = lightPath.power * lightPath.lastVertexBRDF.eval(lightPath.incidentDirection, -incidentDirection)
                                * G * brdf.eval(incidentDirection, wo) / lightPathDiscreteSample.pdf;

                        auto weightedContrib = misWeight * contrib / pSkel;

                        L += weightedContrib;

                        values[SKEL_CONTRIB] += Vec4f(contrib, 1.f);
                        values[SKEL_WEIGHT] += Vec4f(weightedContrib, 1.f);

                    } else {
                        values[SKEL_COHERENCY] += Vec4f(Vec3f(0.f), 1);
                        values[SKEL_CONTRIB] += Vec4f(Vec3f(0.f), 1.f);
                        values[SKEL_WEIGHT] += Vec4f(Vec3f(0), 1.f);
                    }

                } else {
                    values[SKEL_CONTRIB] += Vec4f(Vec3f(0.f), 1.f);
                    values[SKEL_WEIGHT] += Vec4f(Vec3f(0), 1.f);
                }
            } else
            // Sample the brdf
            {
                Sample3f wiSample;
                auto fr = brdf.sample(wo, wiSample, getFloat2(threadID));

                if(wiSample.pdf > 0.f) {
                    auto lastVertex = getScene().intersect(Ray(I, wiSample.value));
                    if(lastVertex) {
                        LightPath lightPath;
                        if(sampleLightPathFromLastVertex(threadID, pixelIdx, lastVertex, lightPath) && lightPath.pdf > 0.f) {
                            auto d = distance(I.P, lightPath.lastVertex.P);
                            if(d > 0.f) {
                                auto pdfSkel = pdfSkeleton(threadID, pixelIdx, pixelNodes, pixelNodeWeights, lightPath);
                                auto pdfBRDF = (lightPath.pdf / lightPath.pdfLastVertex) * wiSample.pdf *
                                        max(0.f, dot(lightPath.lastVertex.Ns, -wiSample.value)) / (d * d);
                                auto misWeight = 1.f / (1.f + pSkel * pdfSkel / ((1 - pSkel) * pdfBRDF));

                                auto contrib = lightPath.power * lightPath.lastVertexBRDF.eval(lightPath.incidentDirection, -wiSample.value)
                                        * fr * max(0.f, dot(I.Ns, wiSample.value)) / wiSample.pdf;

                                auto weightedContrib = misWeight * contrib / (1 - pSkel);

                                L += weightedContrib;

                                values[BRDF_CONTRIB] += Vec4f(contrib, 1.f);
                                values[BRDF_WEIGHT] += Vec4f(weightedContrib, 1.f);
                            } else {
                                values[BRDF_CONTRIB] += Vec4f(Vec3f(0.f), 1.f);
                                values[BRDF_WEIGHT] += Vec4f(Vec3f(0), 1.f);
                            }
                        } else {
                            values[BRDF_CONTRIB] += Vec4f(Vec3f(0.f), 1.f);
                            values[BRDF_WEIGHT] += Vec4f(Vec3f(0), 1.f);
                        }
                    } else {
                        values[BRDF_CONTRIB] += Vec4f(Vec3f(0.f), 1.f);
                        values[BRDF_WEIGHT] += Vec4f(Vec3f(0), 1.f);
                    }
                } else {
                    values[BRDF_CONTRIB] += Vec4f(Vec3f(0.f), 1.f);
                    values[BRDF_WEIGHT] += Vec4f(Vec3f(0), 1.f);
                }
            }
        }

        values[FINAL_RENDER] += Vec4f(L, 1.f);
    }

    void Li_multi_sample_strategy(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx, Vec4f values[]) const {
        auto I = getScene().intersect(primRay);

        if(!I) {
            values[FINAL_RENDER] += Vec4f(badColor(), 1.f);
            return;
        }

        auto brdf = getScene().shade(I);
        auto wo = -primRay.dir.xyz();

        auto L = zero<Vec3f>();

        {
            Vec4i pixelNodes;
            Vec4f pixelNodeWeights;
            skeletonMapping(I, pixelIdx, pixelNodes, pixelNodeWeights);

            // Sample the skeleton
            {
                LightPath lightPath;
                Sample1u lightPathDiscreteSample;
                Sample1u sampledNodeChannel;
                int sampledNodeIdx;

                if(sampleSkeleton(threadID, pixelIdx, pixelNodes, pixelNodeWeights, lightPath, lightPathDiscreteSample,
                                  sampledNodeChannel, sampledNodeIdx)) {
                    values[SAMPLED_NODE] += Vec4f(getColor(sampledNodeIdx), 1.f);

                    Vec3f incidentDirection;
                    float dist;
                    auto G = geometricFactor(I, lightPath.lastVertex, incidentDirection, dist);
                    Ray incidentRay(I, lightPath.lastVertex, incidentDirection, dist);

                    if(G > 0.f && !getScene().occluded(incidentRay)) {
                        values[SKEL_COHERENCY] += Vec4f(Vec3f(1.f), 1);

                        auto pdfSkel = pdfSkeleton(threadID, pixelIdx, pixelNodes, pixelNodeWeights, lightPath, sampledNodeIdx);
                        auto d = distance(I.P, lightPath.lastVertex.P);
                        if(d > 0.f) {
                            auto pdfBRDF = (lightPath.pdf / lightPath.pdfLastVertex) * brdf.pdf(wo, incidentDirection) *
                                    max(0.f, dot(lightPath.lastVertex.Ns, -incidentDirection)) / (d * d);
                            auto misWeight = 1.f / (1.f + pdfBRDF / pdfSkel);

                            auto contrib = lightPath.power * lightPath.lastVertexBRDF.eval(lightPath.incidentDirection, -incidentDirection)
                                    * G * brdf.eval(incidentDirection, wo) / lightPathDiscreteSample.pdf;

                            auto weightedContrib = misWeight * contrib;

                            L += weightedContrib;

                            values[SKEL_CONTRIB] += Vec4f(contrib, 1.f);
                            values[SKEL_WEIGHT] += Vec4f(weightedContrib, 1.f);
                        } else {
                            values[SKEL_CONTRIB] += Vec4f(Vec3f(0.f), 1.f);
                            values[SKEL_WEIGHT] += Vec4f(Vec3f(0), 1.f);
                        }

                    } else {
                        values[SKEL_COHERENCY] += Vec4f(Vec3f(0.f), 1);
                        values[SKEL_CONTRIB] += Vec4f(Vec3f(0.f), 1.f);
                        values[SKEL_WEIGHT] += Vec4f(Vec3f(0), 1.f);
                    }

                } else {
                    values[SKEL_CONTRIB] += Vec4f(Vec3f(0.f), 1.f);
                    values[SKEL_WEIGHT] += Vec4f(Vec3f(0), 1.f);
                }
            }

            // Sample the brdf
            {
                Sample3f wiSample;
                auto fr = brdf.sample(wo, wiSample, getFloat2(threadID));

                if(wiSample.pdf > 0.f) {
                    auto lastVertex = getScene().intersect(Ray(I, wiSample.value));
                    if(lastVertex) {
                        LightPath lightPath;
                        if(sampleLightPathFromLastVertex(threadID, pixelIdx, lastVertex, lightPath) && lightPath.pdf > 0.f) {
                            auto d = distance(I.P, lightPath.lastVertex.P);
                            if(d > 0.f) {
                                auto pdfSkel = pdfSkeleton(threadID, pixelIdx, pixelNodes, pixelNodeWeights, lightPath);
                                auto pdfBRDF = (lightPath.pdf / lightPath.pdfLastVertex) * wiSample.pdf *
                                        max(0.f, dot(lightPath.lastVertex.Ns, -wiSample.value)) / (d * d);
                                auto misWeight = 1.f / (1.f + pdfSkel / pdfBRDF);

                                auto contrib = lightPath.power * lightPath.lastVertexBRDF.eval(lightPath.incidentDirection, -wiSample.value)
                                        * fr * max(0.f, dot(I.Ns, wiSample.value)) / wiSample.pdf;

                                auto weightedContrib = misWeight * contrib;

                                L += weightedContrib;

                                values[BRDF_CONTRIB] += Vec4f(contrib, 1.f);
                                values[BRDF_WEIGHT] += Vec4f(weightedContrib, 1.f);
                            } else {
                                values[BRDF_CONTRIB] += Vec4f(Vec3f(0.f), 1.f);
                                values[BRDF_WEIGHT] += Vec4f(Vec3f(0), 1.f);
                            }
                        } else {
                            values[BRDF_CONTRIB] += Vec4f(Vec3f(0.f), 1.f);
                            values[BRDF_WEIGHT] += Vec4f(Vec3f(0), 1.f);
                        }
                    } else {
                        values[BRDF_CONTRIB] += Vec4f(Vec3f(0.f), 1.f);
                        values[BRDF_WEIGHT] += Vec4f(Vec3f(0), 1.f);
                    }
                } else {
                    values[BRDF_CONTRIB] += Vec4f(Vec3f(0.f), 1.f);
                    values[BRDF_WEIGHT] += Vec4f(Vec3f(0), 1.f);
                }
            }
        }

        values[FINAL_RENDER] += Vec4f(L, 1.f);
    }

    void Li(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx, Vec4f values[]) const {
        Li_one_sample_strategy(primRay, threadID, pixelIdx, values);
    }

    virtual std::string getName() const {
        return "SkelBasedBidir";
    }

    void computeSkelNodeIntersections() {
        auto pSkel = getScene().getSegmentedCurvSkeleton();
        auto completeSize = getLightPathCountPerNode() * pSkel->size();

        if(!m_SkelNodeLightPathArray) {
            m_SkelNodeLightPathArray = makeUniqueArray<LightPath>(completeSize);
            m_nDistributionSize = getDistribution1DBufferSize(getLightPathCountPerNode());
            m_SkelNodeDistributionsArray = makeUniqueArray<float>(m_nDistributionSize * pSkel->size());
            m_SkelNodePowerArray = makeUniqueArray<float>(pSkel->size());
        }

        // For each node, compute intersections, irradiance values and distributions
        launchThreads([pSkel, this](uint32_t threadID) {
            uint32_t loopID = 0u;

            while(true) {
                auto nodeIndex = loopID * getThreadCount() + threadID;
                loopID++;

                if(nodeIndex >= pSkel->size()) {
                    break;
                }

                auto nodePos = pSkel->getNode(nodeIndex).P;
                auto offset = nodeIndex * getLightPathCountPerNode();
                auto distribOffset = nodeIndex * m_nDistributionSize;

                auto powerScale = 2.f / getLightPathCountPerNode();

                for(auto k = 0u; k < getLightPathCountPerNode() / 2; ++k) {
                    auto idx = 2 * k;

                    auto pEmissionVertex = m_EmissionSampler.sample(getScene(),
                                                                    getFloat(threadID),
                                                                    getFloat(threadID),
                                                                    getFloat2(threadID));

                    RaySample ray;
                    auto Le_dir = pEmissionVertex->sampleWi(ray, getFloat2(threadID));
                    auto I = getScene().intersect(ray.value);

                    if(I) {
                        m_SkelNodeLightPathArray[offset + idx].lastVertex = I;
                        m_SkelNodeLightPathArray[offset + idx].lastVertexBRDF = getScene().shade(I);
                        m_SkelNodeLightPathArray[offset + idx].incidentDirection = -ray.value.dir;
                        m_SkelNodeLightPathArray[offset + idx].pdf = pEmissionVertex->pdf() * pEmissionVertex->pdfWrtArea(I);
                        m_SkelNodeLightPathArray[offset + idx].pdfLastVertex = pEmissionVertex->pdfWrtArea(I);
                        m_SkelNodeLightPathArray[offset + idx].power = powerScale * pEmissionVertex->getRadiantExitance() * Le_dir / (pEmissionVertex->pdf() * ray.pdf);

                        Sample3f woSample;
                        auto fr = m_SkelNodeLightPathArray[offset + idx].lastVertexBRDF.sample(-ray.value.dir, woSample, getFloat2(threadID));

                        if(woSample.pdf > 0.f && fr != zero<Vec3f>()) {
                            Ray ray(I, woSample.value);
                            auto I2 = getScene().intersect(ray);
                            if(I2) {
                                m_SkelNodeLightPathArray[offset + idx + 1].lastVertex = I2;
                                m_SkelNodeLightPathArray[offset + idx + 1].lastVertexBRDF = getScene().shade(I2);
                                m_SkelNodeLightPathArray[offset + idx + 1].incidentDirection = -woSample.value;
                                auto d = distance(I.P, I2.P);
                                m_SkelNodeLightPathArray[offset + idx + 1].pdfLastVertex = woSample.pdf * max(0.f, dot(I2.Ns, -woSample.value)) / sqr(d);
                                m_SkelNodeLightPathArray[offset + idx + 1].pdf = m_SkelNodeLightPathArray[offset + idx].pdf * m_SkelNodeLightPathArray[offset + idx + 1].pdfLastVertex;
                                m_SkelNodeLightPathArray[offset + idx + 1].power = m_SkelNodeLightPathArray[offset + idx].power * fr * max(0.f, dot(woSample.value, I.Ns)) / woSample.pdf;
                            } else {
                                m_SkelNodeLightPathArray[offset + idx + 1].pdf = 0.f;
                                m_SkelNodeLightPathArray[offset + idx + 1].pdfLastVertex = 0.f;
                                m_SkelNodeLightPathArray[offset + idx + 1].power = Vec3f(0.f);
                            }
                        } else {
                            m_SkelNodeLightPathArray[offset + idx + 1].pdf = 0.f;
                            m_SkelNodeLightPathArray[offset + idx + 1].pdfLastVertex = 0.f;
                            m_SkelNodeLightPathArray[offset + idx + 1].power = Vec3f(0.f);
                        }

                    } else {
                        m_SkelNodeLightPathArray[offset + idx].pdf = 0.f;
                        m_SkelNodeLightPathArray[offset + idx].pdfLastVertex = 0.f;
                        m_SkelNodeLightPathArray[offset + idx].power = Vec3f(0.f);
                    }
                }

                m_SkelNodePowerArray[nodeIndex] = 0.f;
                for(auto i = 0u, count = getLightPathCountPerNode(); i < count; ++i) {
                    m_SkelNodePowerArray[nodeIndex] += luminance(m_SkelNodeLightPathArray[offset + i].power);

                    auto& I = m_SkelNodeLightPathArray[offset + i].lastVertex;
                    auto dir = nodePos - I.P;
                    auto l = length(dir);
                    dir /= l;
                    if(dot(I.Ns, dir) > 0.f && !getScene().occluded(Ray(I, dir, l))) {
                        m_SkelNodePowerArray[nodeIndex] += luminance(m_SkelNodeLightPathArray[offset + i].power);
                    } else {
                        m_SkelNodeLightPathArray[offset + i].pdf = 0.f;
                        m_SkelNodeLightPathArray[offset + i].pdfLastVertex = 0.f;
                        m_SkelNodeLightPathArray[offset + i].power = Vec3f(0.f);
                    }
                }

                m_SkelNodePowerArray[nodeIndex] /= float(getLightPathCountPerNode());

                buildDistribution1D([this, offset, nodePos](uint32_t i) {
                    return luminance(m_SkelNodeLightPathArray[offset + i].lastVertexBRDF.diffuseTerm() * m_SkelNodeLightPathArray[offset + i].power);
                }, m_SkelNodeDistributionsArray.get() + distribOffset, getLightPathCountPerNode());
            }
        });
    }

    void computeSkeletonMapping() {
        auto framebufferSize = getFramebuffer().getSize();
        m_MappingData.resizeBuffers(framebufferSize.x * framebufferSize.y);

        Vec2u tileCount = framebufferSize / getTileSize() +
                Vec2u(framebufferSize % getTileSize() != zero<Vec2u>());

        auto totalCount = tileCount.x * tileCount.y;

        auto pSkel = getScene().getSegmentedCurvSkeleton();

        auto task = [&](uint32_t threadID) {
            auto loopID = 0u;
            while(true) {
                auto tileID = loopID * getThreadCount() + threadID;
                ++loopID;

                if(tileID >= totalCount) {
                    return;
                }

                uint32_t tileX = tileID % tileCount.x;
                uint32_t tileY = tileID / tileCount.x;

                Vec2u tileOrg = Vec2u(tileX, tileY) * getTileSize();
                auto viewport = Vec4u(tileOrg, getTileSize());

                if(viewport.x + viewport.z > framebufferSize.x) {
                    viewport.z = framebufferSize.x - viewport.x;
                }

                if(viewport.y + viewport.w > framebufferSize.y) {
                    viewport.w = framebufferSize.y - viewport.y;
                }

                auto xEnd = viewport.x + viewport.z;
                auto yEnd = viewport.y + viewport.w;

                for(auto y = viewport.y; y < yEnd; ++y) {
                    for(auto x = viewport.x; x < xEnd; ++x) {
                        auto pixelIdx = getPixelIndex(x, y);
                        auto s2D = Vec2f(0.5f, 0.5f);
                        auto ray = getPrimaryRay(x, y, s2D);
                        auto I = getScene().intersect(ray);
                        if(I) {
                            m_MappingData.nodes[pixelIdx] = getNearestNodes(*pSkel, I.P, I.Ns, m_MappingData.weights[pixelIdx],
                                                [](uint32_t nodeIdx, Vec3f dir, float dist) { return false; });
                        } else {
                            m_MappingData.nodes[pixelIdx] = Vec4i(-1);
                        }
                    }
                }
            }
        };

        launchThreads(task);
    }

    void preprocess() override {
        m_EmissionSampler.initFrame(getScene());

        if(!m_bUseSkelGridMapping) {
            computeSkeletonMapping();
        }
    }

    void beginFrame() override {
        computeSkelNodeIntersections();
    }

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override {
        auto spp = getSppCount();
        TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
            Vec4f L[8];
            std::fill(L, L + 8, Vec4f(0.f));
            auto pixelIdx = getPixelIndex(x, y);
            for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
                auto s2D = getPixelSample(threadID, sampleID);
                auto ray = getPrimaryRay(x, y, s2D);
                Li(ray, threadID, pixelIdx, L);
            }
            for(auto i = 0u; i < 8; ++i) {
                getFramebuffer().accumulate(i, pixelIdx, L[i]);
            }
        });
    }

    virtual void doExposeIO(TwBar* bar) {
        atb::addVarRW(bar, "pSkel", m_fPSkel);
        atb::addVarRW(bar, "Max node count", m_nMaxNodeCount);
        atb::addVarRW(bar, ATB_VAR(m_bUseSkelGridMapping));
        atb::addVarRW(bar, ATB_VAR(m_nNodeLightPathCount));
        atb::addButton(bar, "clear", [this]() {
           m_DebugStream.clearObjects();
        });
    }

    virtual void doLoadSettings(const tinyxml2::XMLElement& xml) {
        getAttribute(xml, "pSkel", m_fPSkel);
        getAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
        getAttribute(xml, "useGridSkelMapping", m_bUseSkelGridMapping);
        getAttribute(xml, "nodeLightPathCount", m_nNodeLightPathCount);

    }

    virtual void doStoreSettings(tinyxml2::XMLElement& xml) const {
        setAttribute(xml, "pSkel", m_fPSkel);
        setAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
        setAttribute(xml, "useGridSkelMapping", m_bUseSkelGridMapping);
        setAttribute(xml, "nodeLightPathCount", m_nNodeLightPathCount);
    }
};

}
