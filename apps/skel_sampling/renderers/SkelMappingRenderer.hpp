#pragma once

#include <bonez/rendering/Renderer.hpp>
#include "SceneViewer.hpp"

namespace BnZ {

class SkelMappingRenderer: public TileProcessingRenderer {
public:
    SceneViewer& m_Viewer;

    struct MappingData {
        mutable std::vector<Intersection> intersections;
        std::vector<Vec4i> nodes;
//        GLImmutableBuffer<Vec4i> nodes;
//        const Vec4i* pNode;

        void resizeBuffers(uint32_t size) {
            intersections.resize(size);

            nodes.resize(size);
//            if(nodes.size() < size) {
//                nodes = genBufferStorage<Vec4i>(size, nullptr, GL_MAP_READ_BIT | GL_MAP_PERSISTENT_BIT, GL_WRITE_ONLY);
//                pNode = nodes.map(GL_MAP_READ_BIT);
//            }


        }
    };

    MappingData m_Data;

    // Multi pass rendering
    SkelMappingRenderer(SceneViewer& module):
        m_Viewer(module) {
    }

    virtual std::string getName() const {
        return "SkelMappingRenderer";
    }

    void preprocess() override {
        m_Data.resizeBuffers(getFramebuffer().getPixelCount());

        /*auto imageSize = frameData.m_nFramebufferSize;

        auto pixelTask = [&](uint32_t x, uint32_t y, uint32_t threadID) {
            if(x >= imageSize.x || y >= imageSize.y) {
                return;
            }

            auto ndc = getNDC(Vec2f(x + 0.5f, y + 0.5f), Vec2f(imageSize));
            auto ray = frameData.m_pCamera->getRay(ndc);
            auto idx = getPixelIndex(x, y, imageSize);
            m_Data.intersections[idx] = frameData.m_pScene->intersect(ray);
        };

        auto tileSize = Vec2u(32);
        auto tileCount = Vec2u(1) + imageSize / tileSize;

        processTiles(tileSize.x, tileSize.y, tileCount.x, tileCount.y, pixelTask);


        m_Module.getNearestNodes(
                    m_Data.intersections.size(),
                    m_Data.intersections.data(),
                    m_Data.nodes.data(),
                    nullptr);*/

/*
        m_Viewer.getNearestNodesFromGBuffer(m_Data.nodes.data(),
                                            nullptr);*/

        m_Viewer.getNearestNodes(
                    m_Data.intersections.size(),
                    m_Data.intersections.data(),
                    m_Data.nodes.data(),
                    nullptr);

        TileProcessingRenderer::processTiles([&](uint32_t threadID, uint32_t tileID, const Vec4u& viewport) {
            TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
                auto pixelIdx = getPixelIndex(x, y);
                auto s2D = getPixelSample(threadID, 0);
                auto ray = getPrimaryRay(x, y, s2D);
                m_Data.intersections[pixelIdx] = getScene().intersect(ray);
            });
        });
    }

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override {
        TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
            auto pixelIdx = getPixelIndex(x, y);

                getFramebuffer().accumulate(pixelIdx, Vec4f(getColor(m_Data.nodes[pixelIdx].x), 1.f));
                //(*frameData.m_pFramebuffer)[pixelIdx] += Vec4f(getColor(m_Data.nodes[pixelIdx].x), 1.f);
//                (*frameData.m_pFramebuffer)[pixelIdx] += Vec4f(getColor(m_Data.pNode[pixelIdx].x), 1.f);

        });

//        auto spp = frameData.m_nSppToProcess;
//        auto lastSample = frameData.m_nCurrentSample + spp;
//        Renderer::processTile(viewport, [&](uint32_t x, uint32_t y) {
//            Vec4f L(0.f);
//            auto pixelIdx = getPixelIndex(x, y, frameData.m_nFramebufferSize);
//            for(auto sampleID = frameData.m_nCurrentSample; sampleID < lastSample; ++sampleID) {
//                L += Vec4f(getColor(m_Data.nodes[pixelIdx].x), 1.f);
//            }
//            (*frameData.m_pFramebuffer)[pixelIdx] += L;
//        });
    }

};

}
