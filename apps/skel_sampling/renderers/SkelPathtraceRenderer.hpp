#pragma once

#include "SkelSampler.hpp"
#include "SceneViewer.hpp"
#include "opengl/GLSkeletonData.hpp"
#include "opengl/GLSkeletonMappingComputePass.hpp"

#include <bonez/rendering/Renderer.hpp>
#include <bonez/sampling/multiple_importance_sampling.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/scene/lights/EmissionSampler.hpp>

namespace BnZ {

class SkelPathtraceRenderer: public TileProcessingRenderer {
public:
    UpdateFlag m_Flag;
    float m_fPSkel = 0.5f;
    uint32_t m_nMaxNodeCount = 4;
    uint32_t m_nMISHeuristic = 0;
    typedef float (*HeuristicFunction)(float, float, float);
    HeuristicFunction m_pMISHeuristic = balanceHeuristicWeight;
    bool m_bFallbackBRDF = true;
    uint32_t m_nSphereMapHeight = 128;
    bool m_bUseCosinePSkel = false;
    uint32_t m_nComputeNodeWeightSubIndex = 0u;
    float m_fSMBias = 0.001f;
    bool m_bUseSkelGridMapping = false;

    bool m_bNeedPreprocess = true;
    EmissionSampler m_EmissionSampler;

    mutable DistributedStatistic<uint64_t, 32> m_TracingTime;
    uint32_t m_PassCount = 0u;
    float m_MeanTracingTime = 0.f;

    GLGBuffer m_GBuffer;
    GLGBufferRenderPass m_GBufferRendererPass;
    GLSkeletonMappingComputePass m_SkelMappingComputePass;

    struct MappingData {
        std::vector<Vec4i> nodes;
        std::vector<Vec4f> weights;
        GLImmutableBuffer<Vec4i> gpuNodesBuffer;
        GLImmutableBuffer<Vec4f> gpuWeightsBuffer;

        void resizeBuffers(uint32_t size) {
            nodes.resize(size);
            weights.resize(size);
            gpuNodesBuffer = genBufferStorage<Vec4i>(size, nullptr, 0, GL_WRITE_ONLY);
            gpuWeightsBuffer =  genBufferStorage<Vec4f>(size, nullptr, 0, GL_WRITE_ONLY);
        }
    };

    MappingData m_Data;
    GLSkeletonData<2>& m_GLSkelData;

    SkelSampler& m_SkelSampler;

    const GLScene& m_GLScene;

    SkelPathtraceRenderer(SkelSampler& sampler,
                          const GLShaderManager& shaderManager,
                          const GLScene& glScene,
                          GLSkeletonData<2>& glSkelData):
        m_GBufferRendererPass(shaderManager),
        m_SkelMappingComputePass(shaderManager),
        m_SkelSampler(sampler),
        m_GLSkelData(glSkelData),
        m_GLScene(glScene) {
    }

    void setHeuristic(uint32_t idx) {
        m_nMISHeuristic = idx < 3 ? idx : 0;
        switch(m_nMISHeuristic) {
        default:
        case 0:
            m_pMISHeuristic = balanceHeuristicWeight;
            break;
        case 1:
            m_pMISHeuristic = powerHeuristicWeight;
            break;
        case 2:
            m_pMISHeuristic = nullptr;
            break;
        }
    }

    virtual std::string getName() const {
        return "SkelPathtraceRenderer2";
    }

    Vec3f sampleBRDF(uint32_t threadID,
                     uint32_t pixelIdx,
                     const Intersection& I,
                     const Vec3f& wo, const BRDF& brdf,
                     Sample3f& wi, Intersection& I2) const {
        auto fr = brdf.sample(wo, wi, getFloat2(threadID));

        if(wi.pdf > 0.f && fr != zero<Vec3f>()) {
            Timer t;
            I2 = getScene().intersect(secondaryRay(I, wi.value));
            m_TracingTime[threadID] += t.getEllapsedTime();
        }
        return fr;
    }

    void skeletonMapping(const Intersection& I, uint32_t pixelIdx, Vec4i& nearestNodes, Vec4f& weights) const {
        if(m_bUseSkelGridMapping) {
            nearestNodes = Vec4i(getScene().getSegmentedCurvSkeleton()->getNearestNode(I), -1, -1, -1);
            weights = Vec4f(1, 0, 0, 0);
        } else {
            nearestNodes = m_Data.nodes[pixelIdx];
            weights = m_Data.weights[pixelIdx];
        }
    }

    Vec3f sampleSkeleton(uint32_t threadID,
                         uint32_t pixelIdx,
                         const Intersection& I,
                         const Vec3f& wo, const BRDF& brdf,
                         Sample3f& wi,
                         float& wiLength,
                         Vec3f& sampledPos, Vec3f& sampledNormal) const {
        Vec4i nearestNodes;
        Vec4f weights;
        skeletonMapping(I, pixelIdx, nearestNodes, weights);

        sampleSurfacePoint(m_SkelSampler, m_nMaxNodeCount, nearestNodes,
                           weights,
                           sampledPos, sampledNormal, wi.pdf, getFloat(threadID),
                           getFloat2(threadID));

        if(wi.pdf) {
            wi.value = sampledPos - I.P.xyz();
            wiLength = length(wi.value);
            wi.value /= wiLength;

            auto fr = brdf.eval(wi.value, wo);

            if(fr != zero<Vec3f>()) {
                wi.pdf = areaToSolidAnglePDF(wi.pdf, sqr(wiLength), dot(sampledNormal, -wi.value));
            }

            return fr;
        }

        return zero<Vec3f>();
    }

    void Li(const Ray& primRay, uint32_t threadID, uint32_t pixelIdx, Vec4f values[]) const {
        auto t = getMicroseconds();
        auto I = getScene().intersect(primRay);
        m_TracingTime[threadID] += getMicroseconds() - t;

        if(!I) {
            values[0] += Vec4f(badColor(), 1.f);
            return;
        }

        auto brdf = getScene().shade(I);
        auto wo = -primRay.dir.xyz();

        // Perform 2 reflections to get computational power comparable to bidirectional pathtracing
        for(auto i = 0u; i < 2; ++i) {
            auto L = Vec3f(0.f);

            Intersection I2;
            Sample3f wi;
            float wiLength;
            Vec3f fr = zero<Vec3f>();
            float weight = 0.f;

            float pSkel = m_fPSkel;
            float s1D = getFloat(threadID);

            if(pSkel > 0.f && m_bUseCosinePSkel) {
                Vec3f sampledPos, sampledNormal;
                fr = sampleSkeleton(threadID, pixelIdx, I, wo, brdf, wi, wiLength, sampledPos, sampledNormal);
                if(wi.pdf > 0.f) {
                    pSkel = min(max(0.f, dot(I.Ns, wi.value)), m_fPSkel);
                } else {
                    pSkel = 0.f;
                }
            } else if(s1D < pSkel) {
                Vec3f sampledPos, sampledNormal;
                fr = sampleSkeleton(threadID, pixelIdx, I, wo, brdf, wi, wiLength, sampledPos, sampledNormal);
            }

            if(s1D < pSkel) {
                if(fr != zero<Vec3f>()) {
                    auto t = getMicroseconds();
                    I2 = getScene().intersect(secondaryRay(I, wi.value));
                    m_TracingTime[threadID] += getMicroseconds() - t;
                }
                if(I2) {
                    //float l = distance(I.P.xyz(), I2.P.xyz());
                    // Check that the intersection is the sampled point
                    /*if(abs(1.f - l / wiLength) >= m_Viewer.m_fOcclusionError) {
                        values[4] += Vec4f(1, 0, 0, 1);
                        continue; // Cancel the sample
                    } else {
                        values[4] += Vec4f(0, 0, 0, 1);
                    }*/
                    // Use the sampled ray
                    if(m_pMISHeuristic) {
                        weight = balanceHeuristicWeight(pSkel, wi.pdf, brdf.pdf(wo, wi.value));
                        wi.pdf *= pSkel;
                    } else {
                        weight = 1.f;
                    }
                }


                values[1] += Vec4f(1, 0, 0, 1);
                values[2] += Vec4f(1, 1, 1, 1);
                values[3] += Vec4f(0, 0, 0, 1);

            } else if(pSkel > 0.f) {
                // Sample the brdf
                fr = sampleBRDF(threadID, pixelIdx, I, wo, brdf, wi, I2);
                if(I2) {
                    if(m_pMISHeuristic) {
                        Vec4i nearestNodes;
                        Vec4f weights;
                        skeletonMapping(I, pixelIdx, nearestNodes, weights);
                        auto skelPdf = pdfWrtIntersectionSolidAngle(m_SkelSampler, I, m_nMaxNodeCount,
                                                                    nearestNodes,
                                                                    weights,
                                                                    I2.P,
                                                                    I2.Ns);

                        weight = m_pMISHeuristic(1 - pSkel, wi.pdf, skelPdf);
                        wi.pdf *= (1 - pSkel);
                    } else {
                        weight = 1.f;
                    }
                }

                values[1] += Vec4f(0, 1, 0, 1);
                values[3] += Vec4f(1, 1, 1, 1);
                values[2] += Vec4f(0, 0, 0, 1);
            }

            if(pSkel == 0.f || (m_bFallbackBRDF && s1D < pSkel && wi.pdf == 0.f)) {
                fr = sampleBRDF(threadID, pixelIdx, I, wo, brdf, wi, I2);
                weight = 1.f;
            }

            if(wi.pdf == 0.f && s1D < pSkel && !m_bFallbackBRDF) {
                // Cancel the sample
                continue;
            }

            if(wi.pdf > 0.f && I2) {
                auto t = getMicroseconds();
                auto Li = estimateDirectLight(getScene(),
                                              m_EmissionSampler,
                                              I2, -wi.value,
                                              getScene().shade(I2),
                                              getFloat(threadID),
                                              getFloat(threadID),
                                              getFloat2(threadID));
                m_TracingTime[threadID] += getMicroseconds() - t;
                L += weight * fr * Li * max(0.f, dot(I.Ns.xyz(), wi.value))
                        / wi.pdf;
            }

            values[0] += Vec4f(L, 1.f);
        }
    }

    void initStatCounterArrays() {
        m_TracingTime.init(0u);
        m_PassCount = 0u;
    }

    void getNearestNodesFromGBuffer() {
        auto size = getFramebuffer().getPixelCount();
        m_Data.resizeBuffers(size);

        auto pCamera = dynamic_cast<const ProjectiveCamera*>(&getCamera());
        if(!pCamera) {
            throw std::runtime_error("SkelPathtraceRenderer only works with projective camera");
        }

        m_GBuffer.init(getFramebuffer().getWidth(), getFramebuffer().getHeight());
        m_GBufferRendererPass.render(
                    pCamera->getProjMatrix(),
                    pCamera->getViewMatrix(),
                    pCamera->getZFar(),
                    m_GLScene,
                    m_GBuffer);

        auto count = m_GBuffer.getWidth() * m_GBuffer.getHeight();

        m_GLSkelData.m_NodeCubeMapContainer.makeResident();
        m_SkelMappingComputePass.computeFromGBuffer(
                    m_GBuffer,
                    *pCamera,
                    m_GLSkelData,
                    m_fSMBias,
                    m_nComputeNodeWeightSubIndex,
                    m_Data.gpuNodesBuffer.getGPUAddress(),
                    m_Data.gpuWeightsBuffer.getGPUAddress());
        m_GLSkelData.m_NodeCubeMapContainer.makeNonResident();

        m_Data.gpuNodesBuffer.getData(0, count, m_Data.nodes.data());
        m_Data.gpuWeightsBuffer.getData(0, count, m_Data.weights.data());
    }

    void preprocess() override {
        initStatCounterArrays();

        if(getScene().getLightContainer().hasChanged(m_Flag)) {
            m_bNeedPreprocess = true;
        }

        m_EmissionSampler.initFrame(getScene());

        if(m_fPSkel > 0.f) {
            if(m_bNeedPreprocess) {
                m_SkelSampler.preprocess(getStatisticsOutput());
                m_bNeedPreprocess = false;
            }
        }

        if(!m_bUseSkelGridMapping) {
            getNearestNodesFromGBuffer();
        }
    }

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override {
        auto spp = getSppCount();
        TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
            Vec4f L[8];
            std::fill(L, L + 8, Vec4f(0.f));
            auto pixelIdx = getPixelIndex(x, y);
            for(auto sampleID = 0u; sampleID < spp; ++sampleID) {
                auto s2D = getPixelSample(threadID, sampleID);
                auto ray = getPrimaryRay(x, y, s2D);
                Li(ray, threadID, pixelIdx, L);
            }
            for(auto i = 0u; i < 8; ++i) {
                getFramebuffer().accumulate(i, pixelIdx, L[i]);
            }
        });
    }

//    void endTiles() override {
//        auto sum = m_TracingTime.sum() / getThreadCount();

//        ++m_PassCount;
//        m_MeanTracingTime = us2ms(sum) / m_PassCount;
//    }

    virtual void doExposeIO(TwBar* bar) {
        atb::addVarRO(bar, ATB_VAR(m_MeanTracingTime));
        atb::addVarRW(bar, "pSkel", m_fPSkel);
        atb::addVarRW(bar, "Max node count", m_nMaxNodeCount);
        auto setHeuristicCB = [&](uint32_t idx) {
            setHeuristic(idx);
        };

        auto getHeuristicCB =[&]() -> uint32_t {
            return m_nMISHeuristic;
        };

        atb::addVarCB(bar, "MIS Heuristic", "Balance,Power,Simple",
                      setHeuristicCB, getHeuristicCB);
        atb::addVarRW(bar, "fallback BRDF", m_bFallbackBRDF);
        atb::addVarRW(bar, ATB_VAR(m_nSphereMapHeight));
        atb::addVarRW(bar, ATB_VAR(m_bUseCosinePSkel));
        atb::addVarRW(bar, ATB_VAR(m_nComputeNodeWeightSubIndex));
        atb::addVarRW(bar, ATB_VAR(m_fSMBias));
        atb::addVarRW(bar, ATB_VAR(m_bUseSkelGridMapping));

        m_SkelSampler.exposeIO(bar);
    }

    virtual void doLoadSettings(const tinyxml2::XMLElement& xml) {
        getAttribute(xml, "pSkel", m_fPSkel);
        getAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
        getAttribute(xml, "misHeuristic", m_nMISHeuristic);
        getAttribute(xml, "fallbackBRDF", m_bFallbackBRDF);
        getAttribute(xml, "sphereMapHeight", m_nSphereMapHeight);
        getAttribute(xml, "useCosinePSkel", m_bUseCosinePSkel);
        getAttribute(xml, "nodeWeightFunctionIndex", m_nComputeNodeWeightSubIndex);
        getAttribute(xml, "smBias", m_fSMBias);
        getAttribute(xml, "useGridSkelMapping", m_bUseSkelGridMapping);

        setHeuristic(m_nMISHeuristic);

        m_SkelSampler.loadSettings(xml);
    }

    virtual void doStoreSettings(tinyxml2::XMLElement& xml) const {
        setAttribute(xml, "pSkel", m_fPSkel);
        setAttribute(xml, "maxNodeCount", m_nMaxNodeCount);
        setAttribute(xml, "misHeuristic", m_nMISHeuristic);
        setAttribute(xml, "fallbackBRDF", m_bFallbackBRDF);
        setAttribute(xml, "sphereMapHeight", m_nSphereMapHeight);
        setAttribute(xml, "useCosinePSkel", m_bUseCosinePSkel);
        setAttribute(xml, "nodeWeightFunctionIndex", m_nComputeNodeWeightSubIndex);
        setAttribute(xml, "smBias", m_fSMBias);
        setAttribute(xml, "useGridSkelMapping", m_bUseSkelGridMapping);

        m_SkelSampler.storeSettings(xml);
    }
};

}
