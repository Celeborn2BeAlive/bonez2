#version 430 core
#extension GL_NV_gpu_shader5 : enable
#extension GL_NV_bindless_texture : enable

layout(local_size_x = 1024) in;

struct Point {
    vec4 P;
    vec4 N;
};

// Input buffer
uniform const Point* uPointBuffer;
uniform uint uPointCount;

coherent uniform ivec4* uNodeBuffer; // Output buffer
coherent uniform vec4* uWeightsBuffer; // Output buffer

ivec4 computeNearestNodes(vec4 P_ws, vec3 N_ws, out vec4 weights);

void main() {
    uint threadID = gl_GlobalInvocationID.x;

    if(threadID >= uPointCount) {
        return;
    }

    Point point = uPointBuffer[threadID];

    vec4 weights;
    uNodeBuffer[threadID] = computeNearestNodes(point.P, point.N.xyz, weights);
    uWeightsBuffer[threadID] = weights;
}
