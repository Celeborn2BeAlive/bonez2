#version 430 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 18) out;

uniform mat4 uFaceMVPMatrices[6];
uniform int uLayer;

in vec3 vPosition[];
in vec3 vNormal[];
in vec2 vTexCoords[];

out vec3 gPosition;
out vec3 gNormal;
out vec2 gTexCoords;

void main() {
    for(int face = 0; face < 6; ++face) {
        gl_ViewportIndex = face;
        for(int i = 0; i < 3; ++i) {
            gl_Layer = uLayer;
            gl_Position = uFaceMVPMatrices[face] * gl_in[i].gl_Position;

            gPosition = vPosition[i];
            gNormal = vNormal[i];
            gTexCoords = vTexCoords[i];

            EmitVertex();
        }
        EndPrimitive();
    }
}
