#version 330 core

uniform samplerCube uIndirectionTexture;
uniform sampler2DArray uTexture;
uniform float uLayer;
uniform float uZNear;
uniform float uZFar;
uniform bool uDrawDepth;
uniform bool uSphereView;

in vec2 vTexCoords;

out vec3 fColor;

vec3 texCoordsToDirection1(vec2 texCoords);
float linearDepth(float, float, float);

void main() {
    vec3 uv;

    if(uSphereView) {
        //uv = vTexCoords;
        //fColor = vec3(1);
    } else {
        vec3 d = texCoordsToDirection1(vTexCoords);
        uv = vec3(texture(uIndirectionTexture, d).rg, uLayer);
    }

    if(uDrawDepth) {
        float depth = texture(uTexture, uv).r;
        fColor = vec3(linearDepth(depth, uZNear, uZFar));
    } else {
        fColor = texture(uTexture, uv).rgb;
    }
}
