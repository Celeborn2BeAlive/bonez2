#version 430 core
#extension GL_NV_gpu_shader5 : enable
#extension GL_NV_bindless_texture : enable

struct GBuffer {
    sampler2D normalDepthSampler;
    sampler2D diffuseSampler;
    sampler2D glossyShininessSampler;
};

uniform GBuffer uGBuffer;
uniform mat4 uRcpViewMatrix;

uniform uint uSMCount;
uniform samplerCube uIndirectionTexture;
uniform sampler2D* uShadowMapBuffer;
uniform mat4* uNodeViewMatrixBuffer;
uniform float* uNodeRadiusBuffer;
uniform vec4* uNodeColorBuffer;

uniform mat4 uFaceProjectionMatrices[6];

uniform float uSMBias;
uniform int uSelectedNode;

uniform bool uRequestNode;
uniform vec3 uRequestedP;
uniform vec3 uRequestedN;
coherent uniform uint* uRequestedNode;

subroutine float
computeNodeWeightSubroutine(uint nodeIdx, vec3 P_ns, float dist, vec3 wi, vec3 N_ws);

layout(location = 0) subroutine uniform computeNodeWeightSubroutine uComputeNodeWeight;

in vec3 vFarPoint_vs;

out vec3 fColor;

int getFaceIdx(vec3 wi);

bool occluded(vec3 P_ns, uint shadowMap, out vec3 wi, out float dist) {
    dist = length(P_ns);
    vec3 shadowRay = P_ns / dist;
    wi = -shadowRay;
    int face = getFaceIdx(shadowRay);
    vec4 faceClipSpacePosition = uFaceProjectionMatrices[face] *
        vec4(P_ns, 1);
    float depthRef = faceClipSpacePosition.z / faceClipSpacePosition.w;
    depthRef = 0.5 * (depthRef + 1) - uSMBias;

    vec2 uv = texture(uIndirectionTexture, shadowRay).rg;

    float depth = texture(uShadowMapBuffer[shadowMap], uv);
    return depthRef >= depth;
}

layout(index = 0) subroutine(computeNodeWeightSubroutine)
float computeNodeWeightDistCosMaxball(uint nodeIdx, vec3 P_ns, float dist, vec3 wi, vec3 N_ws) {
    float c = dot(wi, N_ws); // Works because node view matrix are only translations
    float r = uNodeRadiusBuffer[nodeIdx];
    return r * sqrt(c) / (dist * dist);
}

layout(index = 1) subroutine(computeNodeWeightSubroutine)
float computeNodeWeightDist(uint nodeIdx, vec3 P_ns, float dist, vec3 wi, vec3 N_ws) {
    return 100000 - dist;
}

int computeNearestNode(vec4 P_ws, vec3 N_ws) {
    int nearestNode = -1;
    float currentWeight = 0;

    for(int i = 0; i < uSMCount; ++i) {
        // Position in node space
        vec3 P_ns = vec3(uNodeViewMatrixBuffer[i] * P_ws);
        float dist;
        vec3 wi;

        if(!occluded(P_ns, i, wi, dist)) {
            float weight = uComputeNodeWeight(i, P_ns, dist, wi, N_ws);
            if(weight > currentWeight) {
                currentWeight = weight;
                nearestNode = i;
            }
        }
    }

    return nearestNode;
}

void main() {
    ivec2 pixelCoords = ivec2(gl_FragCoord.xy);

    // Reconstruct fragment position from depth
    vec4 normalDepth = texelFetch(uGBuffer.normalDepthSampler, pixelCoords, 0);
    vec3 P_vs = normalDepth.w * vFarPoint_vs;

    vec4 P_ws = uRcpViewMatrix * vec4(P_vs, 1);

    if(uRequestNode) {
        *uRequestedNode = computeNearestNode(vec4(uRequestedP, 1), uRequestedN);
    }

    int bestMatch = computeNearestNode(P_ws, normalDepth.xyz);

    if(uSelectedNode < 0) {
        if(bestMatch > 0) {
            fColor = uNodeColorBuffer[bestMatch].rgb;
        }
    } else {
        if(bestMatch != uSelectedNode) {
            discard;
        } else {
            fColor = uNodeColorBuffer[bestMatch].rgb;
        }
    }
}
