#version 430 core
#extension GL_NV_gpu_shader5 : enable
#extension GL_NV_bindless_texture : enable

in vec3 gPosition;
in vec3 gNormal;
in vec2 gTexCoords;

struct Material {
    vec3 Kd;
    vec3 Ks;
    float shininess;
    sampler2D KdSampler;
    sampler2D KsSampler;
    sampler2D shininessSampler;
};

uniform Material uMaterial;

vec3 normal;

struct Light {
    vec4 P; // P.w = 0 => directional light
    vec4 L;
    uint smIndex;
};

uniform Light* uLightBuffer;
uniform uint uLightCount;
uniform samplerCube* uPointLightSMBuffer;
uniform mat4 uFaceProjectionMatrices[6];
uniform float uSMBias;

out vec3 fIrradiance;
out vec3 fPosition;
out vec3 fNormal;

int getFaceIdx(vec3 wi);

bool pointLightOccluded(vec3 P_ls, uint shadowMap, out vec3 wi, out float dist) {
    dist = length(P_ls);
    vec3 shadowRay = P_ls / dist;
    wi = -shadowRay;
    int face = getFaceIdx(shadowRay);
    vec4 faceClipSpacePosition = uFaceProjectionMatrices[face] *
        vec4(P_ls, 1);
    float depthRef = faceClipSpacePosition.z / faceClipSpacePosition.w;
    depthRef = 0.5 * (depthRef + 1) - uSMBias;

    float depth = texture(uPointLightSMBuffer[shadowMap], shadowRay);
    return depthRef >= depth;
}

vec3 pointLightIrradiance(Light light) {
    vec3 P_ls = gPosition - light.P.xyz;

    vec3 wi;
    float d;

    if(pointLightOccluded(P_ls, light.smIndex, wi, d)) {
        return vec3(0);
    }

    return light.L.rgb * max(0, dot(wi, normal)) / (d * d);
}

vec3 directionalLightIrradiance(Light light) {
    vec3 wi = light.P.xyz;
    return light.L.xyz * max(0, dot(wi, normal));
}

void main() {
    normal = normalize(gNormal);
    fPosition = gPosition;
    fNormal = normal;
    fIrradiance = vec3(0);
    for(uint i = 0; i < uLightCount; ++i) {
        Light light = uLightBuffer[i];
        if(light.P.w == 0.f) {
            fIrradiance += directionalLightIrradiance(light);
        } else {
            fIrradiance += pointLightIrradiance(light);
        }
    }
    fIrradiance *= uMaterial.Kd * texture(uMaterial.KdSampler, gTexCoords).rgb;
}
