#version 330 core

uniform samplerCube uIndirectionTexture;
uniform sampler2DArray uDepthTexture;
uniform sampler2DArray uIrradianceTexture;
uniform sampler2DArray uPositionTexture;
uniform sampler2DArray uNormalTexture;
uniform float uLayer;
uniform float uZNear;
uniform float uZFar;

in vec2 vTexCoords;

layout(location = 0) out float fDepth;
layout(location = 1) out vec3 fIrradiance;
layout(location = 2) out vec3 fPosition;
layout(location = 3) out vec3 fNormal;

vec3 sphericalToCartesian(vec2 texCoords) {
    float PI = 3.14;

    float phi = texCoords.x * 2 * PI;
    float theta = texCoords.y * PI;

    return vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta));
}

float linearDepth(float, float, float);

void main() {
    vec3 d = sphericalToCartesian(vTexCoords);
    vec3 uv = vec3(texture(uIndirectionTexture, d).rg, uLayer);

    fDepth = texture(uDepthTexture, uv).r;
    fIrradiance = texture(uIrradianceTexture, uv).rgb;
    fPosition = texture(uPositionTexture, uv).rgb;
    fNormal = texture(uNormalTexture, uv).rgb;
}
