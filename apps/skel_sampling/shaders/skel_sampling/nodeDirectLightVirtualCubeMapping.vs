#version 330 core

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec2 aTexCoords;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCoords;

void main() {
    vPosition = aPosition;
    vNormal = aNormal;
    vTexCoords = aTexCoords;

    gl_Position = vec4(aPosition, 1);
}
