#version 430 core
#extension GL_NV_gpu_shader5 : enable
#extension GL_NV_bindless_texture : enable

layout(local_size_x = 32, local_size_y = 32) in;

uniform uint uSMCount;
uniform samplerCube uIndirectionTexture;
uniform sampler2D* uShadowMapBuffer;
uniform mat4* uNodeViewMatrixBuffer;
uniform float* uNodeRadiusBuffer;

struct GBuffer {
    sampler2D normalDepthSampler;
    sampler2D diffuseSampler;
    sampler2D glossyShininessSampler;
};

uniform GBuffer uGBuffer;
uniform mat4 uRcpProjMatrix;
uniform mat4 uRcpViewMatrix;

uniform mat4 uFaceProjectionMatrices[6];

uniform float uSMBias;

coherent uniform ivec4* uNodeBuffer; // Output buffer
coherent uniform vec4* uWeightsBuffer; // Output buffer

subroutine float
computeNodeWeightSubroutine(uint nodeIdx, vec3 P_ns, float dist, vec3 wi, vec3 N_ws);

layout(location = 0) subroutine uniform computeNodeWeightSubroutine uComputeNodeWeight;

int getFaceIdx(vec3 wi);

bool occluded(vec3 P_ns, uint shadowMap, out vec3 wi, out float dist) {
    dist = length(P_ns);
    vec3 shadowRay = P_ns / dist;
    wi = -shadowRay;
    int face = getFaceIdx(shadowRay);
    vec4 faceClipSpacePosition = uFaceProjectionMatrices[face] *
        vec4(P_ns, 1);
    float depthRef = faceClipSpacePosition.z / faceClipSpacePosition.w;
    depthRef = 0.5 * (depthRef + 1) - uSMBias;

    vec2 uv = texture(uIndirectionTexture, shadowRay).rg;

    float depth = texture(uShadowMapBuffer[shadowMap], uv);
    return depthRef >= depth;
}

layout(index = 0) subroutine(computeNodeWeightSubroutine)
float computeNodeWeightDistCosMaxball(uint nodeIdx, vec3 P_ns, float dist, vec3 wi, vec3 N_ws) {
    float c = dot(wi, N_ws); // Works because node view matrix are only translations
    float r = uNodeRadiusBuffer[nodeIdx];
    return r * sqrt(c) / (dist * dist);
}

layout(index = 1) subroutine(computeNodeWeightSubroutine)
float computeNodeWeightDist(uint nodeIdx, vec3 P_ns, float dist, vec3 wi, vec3 N_ws) {
    return 100000 - dist;
}

int computeNearestNode(vec4 P_ws, vec3 N_ws) {
    int nearestNode = -1;
    float currentWeight = 0;

    for(int i = 0; i < uSMCount; ++i) {
        // Position in node space
        vec3 P_ns = vec3(uNodeViewMatrixBuffer[i] * P_ws);
        float dist;
        vec3 wi;

        if(!occluded(P_ns, i, wi, dist)) {
            float weight = uComputeNodeWeight(i, P_ns, dist, wi, N_ws);
            if(weight > currentWeight) {
                currentWeight = weight;
                nearestNode = i;
            }
        }
    }

    return nearestNode;
}

ivec4 computeNearestNodes(vec4 P_ws, vec3 N_ws, out vec4 weights) {
    ivec4 nearestNodes = ivec4(-1);
    vec4 currentWeights = vec4(0);

    for(int i = 0; i < uSMCount; ++i) {
        // Position in node space
        vec3 P_ns = vec3(uNodeViewMatrixBuffer[i] * P_ws);
        float dist;
        vec3 wi;

        if(!occluded(P_ns, i, wi, dist)) {
            float weight = uComputeNodeWeight(i, P_ns, dist, wi, N_ws);
            if(weight > currentWeights.x) {
                currentWeights.x = weight;
                nearestNodes.x = i;
            } else if(weight > currentWeights.y) {
                currentWeights.y = weight;
                nearestNodes.y = i;
            } else if(weight > currentWeights.z) {
                currentWeights.z = weight;
                nearestNodes.z = i;
            } else if(weight > currentWeights.w) {
                currentWeights.w = weight;
                nearestNodes.w = i;
            }
        }
    }

    weights = currentWeights;
    return nearestNodes;
}

void main() {
    ivec2 threadID = ivec2(gl_GlobalInvocationID.xy);
    ivec2 size = textureSize(uGBuffer.normalDepthSampler, 0);

    int idx = threadID.x + threadID.y * size.x;

    if(threadID.x >= size.x || threadID.y >= size.y) {
        return;
    }

    vec2 ndcPosition = vec2(-1) + 2.f * (vec2(threadID) + vec2(0.5f)) / vec2(size);

    vec4 farPointViewSpaceCoords = uRcpProjMatrix * vec4(ndcPosition, 1.f, 1.f);
    vec3 vFarPoint_vs = farPointViewSpaceCoords.xyz / farPointViewSpaceCoords.w;

    vec4 normalDepth = texelFetch(uGBuffer.normalDepthSampler, threadID, 0);
    vec3 P_vs = normalDepth.w * vFarPoint_vs;

    vec4 P_ws = uRcpViewMatrix * vec4(P_vs, 1);

    vec4 weights;
    uNodeBuffer[idx] = computeNearestNodes(P_ws, normalDepth.xyz, weights);
    uWeightsBuffer[idx] = weights;
}
