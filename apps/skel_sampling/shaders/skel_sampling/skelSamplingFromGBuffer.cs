#version 430 core
#extension GL_NV_gpu_shader5 : enable
#extension GL_NV_bindless_texture : enable

layout(local_size_x = 32, local_size_y = 32) in;

struct GBuffer {
    sampler2D normalDepthSampler;
    sampler2D diffuseSampler;
    sampler2D glossyShininessSampler;
};

uniform GBuffer uGBuffer;
uniform mat4 uRcpProjMatrix;
uniform mat4 uRcpViewMatrix;

coherent uniform ivec4* uNodeBuffer; // Output buffer
coherent uniform vec4* uWeightsBuffer; // Output buffer

ivec4 computeNearestNodes(vec4 P_ws, vec3 N_ws, out vec4 weights);

void main() {
    ivec2 threadID = ivec2(gl_GlobalInvocationID.xy);
    ivec2 size = textureSize(uGBuffer.normalDepthSampler, 0);

    int idx = threadID.x + threadID.y * size.x;

    if(threadID.x >= size.x || threadID.y >= size.y) {
        return;
    }

    vec2 ndcPosition = vec2(-1) + 2.f * (vec2(threadID) + vec2(0.5f)) / vec2(size);

    vec4 farPointViewSpaceCoords = uRcpProjMatrix * vec4(ndcPosition, 1.f, 1.f);
    vec3 vFarPoint_vs = farPointViewSpaceCoords.xyz / farPointViewSpaceCoords.w;

    vec4 normalDepth = texelFetch(uGBuffer.normalDepthSampler, threadID, 0);
    vec3 P_vs = normalDepth.w * vFarPoint_vs;

    vec4 P_ws = uRcpViewMatrix * vec4(P_vs, 1);

    vec4 weights;
    uNodeBuffer[idx] = computeNearestNodes(P_ws, normalDepth.xyz, weights);
    uWeightsBuffer[idx] = weights;
}
