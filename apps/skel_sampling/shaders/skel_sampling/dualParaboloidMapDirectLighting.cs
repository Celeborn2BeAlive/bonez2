#version 430 core
#extension GL_NV_gpu_shader5 : enable
#extension GL_NV_bindless_texture : enable

layout(local_size_x = 16, local_size_y = 16, local_size_z = 4) in;

struct PointLightData {
    vec4 position;
    vec4 intensity;
};

uniform uint uPointLightCount;
uniform PointLightData* uPointLightBuffer;
uniform samplerCubeArray uPointLightDepthMaps;
uniform mat4 uPointLightFaceProjMatrices[6];
uniform float uPointLightDepthBias;

int getFaceIdx(vec3 wi);

bool pointLightOccluded(vec3 P_ls, uint lightIdx, vec3 shadowRay) {
    int face = getFaceIdx(shadowRay);
    vec4 faceClipSpacePosition = uPointLightFaceProjMatrices[face] *
        vec4(P_ls, 1);
    float depthRef = faceClipSpacePosition.z / faceClipSpacePosition.w;
    depthRef = 0.5 * (depthRef + 1) - uPointLightDepthBias;

    float depth = texture(uPointLightDepthMaps, vec4(shadowRay, lightIdx));
    return depthRef >= depth;
}

uniform uint uSphereMapWidth;
uniform uint uSphereMapHeight;
uniform uint uSphereMapCount;

uniform vec4* uNodePositionBuffer;
uniform vec4* uNormalDistSphereMaps;

uniform vec4* uDirectLightSphereMaps; // Output

vec2 getUV(uvec2 pixel, uvec2 imageSize) {
    return (vec2(pixel) + vec2(0.5)) / vec2(imageSize);
}

vec2 getNDC(vec2 uv) {
    return vec2(-1) + 2 * uv;
}

vec3 dualParaboloidMapping(vec2 uv) {
    if(uv.x < 0.5) {
        vec2 ndc = getNDC(uv / vec2(0.5, 1));
        vec3 N = vec3(ndc.x, ndc.y, 1.f);
        float scale = 2.f / dot(N, N);
        return scale * N - vec3(0.f, 0.f, 1.f);
    }
    vec2 ndc = getNDC((uv - vec2(0.5, 0)) / vec2(0.5, 1));
    ndc.x = -ndc.x; // Reverse x to get continuity along the edge
    vec3 N = vec3(ndc.x, ndc.y, -1.f);
    float scale = 2.f / dot(N, N);
    return scale * N - vec3(0.f, 0.f, -1.f);
}

void main() {
    uvec3 size = uvec3(uSphereMapWidth, uSphereMapHeight, uSphereMapCount);

    if(any(greaterThanEqual(gl_GlobalInvocationID, size))) {
        return;
    }

    uvec2 pixel = gl_GlobalInvocationID.xy;

    uint idx = pixel.x + uSphereMapWidth * pixel.y +
    uSphereMapWidth * uSphereMapHeight * gl_GlobalInvocationID.z;

    vec3 wi = dualParaboloidMapping(getUV(pixel, size.xy));
    vec4 normalDist = uNormalDistSphereMaps[idx];

    vec4 position = vec4(uNodePositionBuffer[gl_GlobalInvocationID.z].xyz
        + wi * normalDist.w, 1);

    vec3 L = vec3(0);

    for(uint i = 0u; i < uPointLightCount; ++i) {
        PointLightData pointLight = uPointLightBuffer[i];
        vec3 shadowRay = vec3(position - pointLight.position);
        vec3 P_ls = shadowRay;
        float d = length(shadowRay);
        shadowRay /= d;
        if(!pointLightOccluded(P_ls, i, shadowRay)) {
            L += pointLight.intensity.rgb * max(0, dot(-shadowRay, normalDist.xyz)) / (d * d);
        }
    }

    uDirectLightSphereMaps[idx] = vec4(L, 0);
}
