#version 430 core
#extension GL_NV_gpu_shader5 : enable
#extension GL_NV_bindless_texture : enable

layout(local_size_x = 16, local_size_y = 16, local_size_z = 4) in;

struct PointLightData {
    vec4 position;
    vec4 intensity;
};

uniform uint uPointLightCount;
uniform PointLightData* uPointLightBuffer;
uniform samplerCubeArray uPointLightDepthMaps;
uniform mat4 uPointLightFaceProjMatrices[6];
uniform float uPointLightDepthBias;

int getFaceIdx(vec3 wi);

bool pointLightOccluded(vec3 P_ls, uint lightIdx, vec3 shadowRay) {
    int face = getFaceIdx(shadowRay);
    vec4 faceClipSpacePosition = uPointLightFaceProjMatrices[face] *
        vec4(P_ls, 1);
    float depthRef = faceClipSpacePosition.z / faceClipSpacePosition.w;
    depthRef = 0.5 * (depthRef + 1) - uPointLightDepthBias;

    float depth = texture(uPointLightDepthMaps, vec4(shadowRay, lightIdx));
    return depthRef >= depth;
}

uniform uint uSphereMapWidth;
uniform uint uSphereMapHeight;
uniform uint uSphereMapCount;

uniform vec4* uNodePositionBuffer;
uniform vec4* uNormalDistSphereMaps;

uniform vec4* uDirectLightSphereMaps; // Output

vec3 sphericalMapping(vec2 texCoords);

void main() {
    uvec3 size = uvec3(uSphereMapWidth, uSphereMapHeight, uSphereMapCount);

    if(any(greaterThanEqual(gl_GlobalInvocationID, size))) {
        return;
    }

    uvec2 pixel = gl_GlobalInvocationID.xy;

    uint idx = pixel.x + uSphereMapWidth * pixel.y +
    uSphereMapWidth * uSphereMapHeight * gl_GlobalInvocationID.z;

    vec3 wi = sphericalMapping((vec2(pixel) + vec2(0.5)) / vec2(size.xy));
    vec4 normalDist = uNormalDistSphereMaps[idx];

    vec4 position = vec4(uNodePositionBuffer[gl_GlobalInvocationID.z].xyz
        + wi * normalDist.w, 1);

    vec3 L = vec3(0);
    for(uint i = 0u; i < uPointLightCount; ++i) {
        PointLightData pointLight = uPointLightBuffer[i];
        vec3 shadowRay = vec3(position - pointLight.position);
        vec3 P_ls = shadowRay;
        float d = length(shadowRay);
        shadowRay /= d;
        if(!pointLightOccluded(P_ls, i, shadowRay)) {
            L += pointLight.intensity.rgb * max(0, dot(-shadowRay, normalDist.xyz)) / (d * d);
        }
    }

    uDirectLightSphereMaps[idx] = vec4(L, 0);
}
