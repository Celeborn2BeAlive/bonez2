#include "SceneViewer.hpp"

#include "bonez/sys/time.hpp"

#include "renderers/SkelMappingRenderer.hpp"
#include "renderers/SkelPathtraceRenderer.hpp"
#include "renderers/ImperfectSkelPathtraceRenderer.hpp"
#include "renderers/ReverseImperfectSkelPathtraceRenderer.hpp"
#include "renderers/SkelMappingRendererFullCPU.hpp"
#include "renderers/NodeVisibilitySamplingCoherenceRenderer.hpp"
#include "renderers/SkelPathtraceImportanceResamplingRenderer.hpp"
#include "renderers/SkelPathtracePreprocessNodesRenderer.hpp"
#include "renderers/SkelPathtracePreprocessNodesWithResamplingRenderer.hpp"
#include "renderers/RadiosityBasedSkelSamplingPathtracer.hpp"
#include "renderers/RadiosityBasedSkelSamplingPathtracerCPUOnly.hpp"
#include "renderers/RadiosityBasedSkelRISPathtracerCPUOnly.hpp"
#include "renderers/RadSkelRISPTNodeRepCPUOnly.hpp"
#include "renderers/RadSkelPTVisMappingCPUOnly.hpp"
#include "renderers/RadSkelSamplingPTCPU.hpp"
#include "renderers/RadSkelRISAllPointsPTCPU.hpp"
#include "renderers/RadSkelSamplingNodeRepPTCPU.hpp"
#include "renderers/RadSkelSamplingIntegralCutPTCPU.hpp"
#include "renderers/RadSkelPTVisMappingPathSpaceCPUOnly.hpp"
#include "renderers/SkelBasedBidir.hpp"
#include <bonez/scene/lights/EmissionSampler.hpp>

namespace BnZ {

void SceneViewer::initDebugMaps(const Vec2u& size) {
    m_DebugSphereMap = GLTexture2DArray();
    m_DebugSphereMap.setStorage(1, GL_RGBA32F,
                               size.x, size.y,
                               m_pSkel->size());
    m_DebugSphereMap.setMinFilter(GL_NEAREST);
    m_DebugSphereMap.setMagFilter(GL_NEAREST);
    m_nCurrentDebugSphereChannel = uint32_t(-1);

    m_DebugDualParaboloidMap = GLTexture2DArray();
    m_DebugDualParaboloidMap.setStorage(1, GL_RGBA32F,
                                        size.x, size.y,
                                        m_pSkel->size());
    m_DebugDualParaboloidMap.setMinFilter(GL_NEAREST);
    m_DebugDualParaboloidMap.setMagFilter(GL_NEAREST);
    m_nCurrentDebugDualParaboloidChannel = uint32_t(-1);
}

void SceneViewer::initGPUBuffers(GLQuery& timeQuery) {
    GLTimerScope timer(timeQuery);

    auto totalSize = MAX_BUFFERS_SIZE;

    m_SphereMapsBuffer = genBufferStorage<Vec4f>(
                ChannelCount * totalSize,
                nullptr,
                0,
                GL_READ_WRITE);

    for (auto i = 0u; i < ChannelCount; ++i) {
        m_SphereMapAddresses[i] = m_SphereMapsBuffer.getGPUAddress() + totalSize * i;
    }
    std::vector<Vec4f> test(totalSize, Vec4f(0.3, 0, 1, 0));
    m_NodeDirectLightSphereMaps = genBufferStorage<Vec4f>(
                totalSize,
                test.data(),
                0,
                GL_READ_WRITE);

    m_GLSkelData.init(*m_pSkel, m_nNodeCubeMapRes);
    m_PointLightsData.init(PointLightCubeMapRes, MAX_LIGHT_COUNT);

    m_DualParaboloidMapBuffers = genBufferStorage<Vec4f>(
                ChannelCount * totalSize,
                nullptr,
                0,
                GL_READ_WRITE);

    for (auto i = 0u; i < ChannelCount; ++i) {
        m_DualParaboloidMapAddresses[i] = m_DualParaboloidMapBuffers.getGPUAddress() + totalSize * i;
    }
    m_NodeDirectLightDualParaboloidMaps = genBufferStorage<Vec4f>(
                totalSize,
                test.data(),
                0,
                GL_READ_WRITE);
}

void SceneViewer::initCPUBuffers(float& time) {
    auto start = getMicroseconds();

    auto totalSize = MAX_BUFFERS_SIZE;

    m_SkelSphericalMapContainer.alloc(MAX_SPHERE_MAP_SIZE.x, MAX_SPHERE_MAP_SIZE.y, MAX_NODE_COUNT);
    m_SkelDualParaboloidMapContainer.alloc(MAX_SPHERE_MAP_SIZE.x, MAX_SPHERE_MAP_SIZE.y, MAX_NODE_COUNT);

    time = us2ms(getMicroseconds() - start);
}

struct PointLightExtractor: public LightVisitor {
    std::vector<PointLight> m_PointLights;

    void visit(const PointLight& light) override {
        m_PointLights.emplace_back(light);
    }
};

void SceneViewer::computeGPUPointLightsData(GLQuery& timeQuery) {
    GLTimerScope timer(timeQuery);
    const auto& scene = *m_pScene;

    PointLightExtractor visitor;
    scene.getLightContainer().accept(visitor);

    const auto& pointLights = visitor.m_PointLights;

    m_PointLightsData.compute(pointLights.size(),
                           pointLights.data(),
                           *m_pGLScene,
                           m_CubeMapRenderPass,
                           m_ZNearFar.x, m_ZNearFar.y);
}

void SceneViewer::computeGPUSkeletonData(GLQuery& timeQuery) {
    GLTimerScope timer(timeQuery);
    m_GLSkelData.renderCubeMaps(*m_pGLScene, m_ZNearFar.x, m_ZNearFar.y, m_CubeMapRenderPass);
}

void SceneViewer::convertCubeMapsToSphericalMaps(GLQuery& timeQuery, const Vec2u& mapSize) {
    GLTimerScope timer(timeQuery);
    m_GLSkelData.m_NodeCubeMapContainer.makeResident();
    m_CubeMapRenderPass.convertToSphericalMaps(
                m_GLSkelData.m_NodeCubeMapContainer,
                m_SphereMapAddresses,
                mapSize.x, mapSize.y);
    m_GLSkelData.m_NodeCubeMapContainer.makeNonResident();
}

void SceneViewer::convertCubeMapsToDualParaboloidMaps(GLQuery& timeQuery, const Vec2u& mapSize) {
    GLTimerScope timer(timeQuery);
    m_GLSkelData.m_NodeCubeMapContainer.makeResident();
    m_CubeMapRenderPass.convertToDualParaboloidMaps(
                m_GLSkelData.m_NodeCubeMapContainer,
                m_DualParaboloidMapAddresses,
                0.5f * mapSize.x, mapSize.y);
    m_GLSkelData.m_NodeCubeMapContainer.makeNonResident();
}

void SceneViewer::computeSphericalSkelMapsDirectLighting(GLQuery& timeQuery, const Vec2u& mapSize) {
    GLTimerScope timer(timeQuery);
    m_PointLightsData.m_CubeMapContainer.makeResident();
    m_NodeSphericalMapDirectLightingComputePass.compute(
                mapSize.x, mapSize.y, m_pSkel->size(),
                m_PointLightsData,
                m_fSMBias,
                m_GLSkelData.m_NodePositionBuffer.getGPUAddress(),
                m_SphereMapAddresses[0],
                m_NodeDirectLightSphereMaps.getGPUAddress());
    m_PointLightsData.m_CubeMapContainer.makeNonResident();
}

void SceneViewer::computeDualParaboloidSkelMapsDirectLighting(GLQuery& timeQuery, const Vec2u& mapSize) {
    GLTimerScope timer(timeQuery);
    m_PointLightsData.m_CubeMapContainer.makeResident();
    m_NodeDualParaboloidMapDirectLightingComputePass.compute(
                mapSize.x, mapSize.y, m_pSkel->size(),
                m_PointLightsData,
                m_fSMBias,
                m_GLSkelData.m_NodePositionBuffer.getGPUAddress(),
                m_DualParaboloidMapAddresses[0],
                m_NodeDirectLightDualParaboloidMaps.getGPUAddress());
    m_PointLightsData.m_CubeMapContainer.makeNonResident();
}

void SceneViewer::getSphericalMapsFromGPU(GLQuery& timeQuery, const Vec2u& mapSize) {
    GLTimerScope timer(timeQuery);

    m_NodeDirectLightSphereMaps.getData(
                0, mapSize.x * mapSize.y * m_pSkel->size(),
                m_SkelSphericalMapContainer.getImportanceMapsBufferPtr());

    m_SphereMapsBuffer.getData(
                0, mapSize.x * mapSize.y * m_pSkel->size(),
                m_SkelSphericalMapContainer.getNormalDistMapsBufferPtr());

}

void SceneViewer::getDualParaboloidMapsFromGPU(GLQuery& timeQuery, const Vec2u& mapSize) {
    GLTimerScope timer(timeQuery);

    m_NodeDirectLightDualParaboloidMaps.getData(
                0, mapSize.x * mapSize.y * m_pSkel->size(),
                m_SkelDualParaboloidMapContainer.getImportanceMapsBufferPtr());

    m_DualParaboloidMapBuffers.getData(
                0, mapSize.x * mapSize.y * m_pSkel->size(),
                m_SkelDualParaboloidMapContainer.getNormalDistMapsBufferPtr());

}

void SceneViewer::computeDualParaboloidMaps(uint32_t sphereMapHeight,
                               tinyxml2::XMLElement* stats) {
    if(!m_pSkel) {
        return;
    }

    GLQuery pointLightCubeMapQuery;
    GLQuery skelCubeMapQuery;
    GLQuery conversionDualParaboloidsQuery;
    GLQuery directLightingQuery;
    GLQuery dataTransfertTimeQuery;

    std::cerr << "SceneViewer::computeDualParaboloidMaps" << std::endl;

//    setSphereMapsHeight(sphereMapHeight);

//    auto totalSize = m_nSphereMapSize * m_pSkel->size();
//    if(totalSize > MAX_BUFFERS_SIZE) {
//        throw std::runtime_error("SceneViewer::computeDualParaboloidMaps OUT OF MEMORY");
//    }

    auto mapSize = Vec2u(sphereMapHeight * 2, sphereMapHeight);

    computeGPUPointLightsData(pointLightCubeMapQuery);
    computeGPUSkeletonData(skelCubeMapQuery);

    convertCubeMapsToDualParaboloidMaps(conversionDualParaboloidsQuery, mapSize);

    computeDualParaboloidSkelMapsDirectLighting(directLightingQuery, mapSize);

    getDualParaboloidMapsFromGPU(dataTransfertTimeQuery, mapSize);

    initDebugMaps(mapSize);

    m_fLightCubeMapRenderTime = ns2ms(pointLightCubeMapQuery.waitResult());
    m_fSkelCubeMapRenderTime = ns2ms(skelCubeMapQuery.waitResult());
    m_fConversionTime = ns2ms(conversionDualParaboloidsQuery.waitResult());
    m_fDirectLightingTime = ns2ms(directLightingQuery.waitResult());
    m_fDataTransfertTime = ns2ms(dataTransfertTimeQuery.waitResult());

    if(stats) {
        setAttribute(*stats, "initGPUBuffersTime", m_fInitGPUBuffersTime);
        setAttribute(*stats, "initCPUBuffersTime", m_fInitCPUBuffersTime);
        setAttribute(*stats, "lightCubeMapsRenderTime", m_fLightCubeMapRenderTime);
        setAttribute(*stats, "skelCubeMapsRenderTime", m_fSkelCubeMapRenderTime);
        setAttribute(*stats, "convertToDualParaboloidMapsTime", m_fConversionTime);
        setAttribute(*stats, "directLightingTime", m_fDirectLightingTime);
        setAttribute(*stats, "dataTransfertTime", m_fDataTransfertTime);
        setAttribute(*stats, "computeSkelDistribTime", m_fComputeSkelDistribTime);
    }
}

void SceneViewer::computeSphericalMaps(uint32_t sphereMapHeight,
                               tinyxml2::XMLElement* stats) {
    if(!m_pSkel) {
        return;
    }

    GLQuery pointLightCubeMapQuery;
    GLQuery skelCubeMapQuery;
    GLQuery conversionQuery;
    GLQuery directLightingQuery;
    GLQuery dataTransfertTimeQuery;

    std::cerr << "SceneViewer::computeSphericalMaps" << std::endl;

    /*setSphereMapsHeight(sphereMapHeight);

    auto totalSize = m_nSphereMapSize * m_pSkel->size();
    if(totalSize > MAX_BUFFERS_SIZE) {
        throw std::runtime_error("SceneViewer::computeSphericalMaps OUT OF MEMORY");
    }*/

    auto mapSize = Vec2u(sphereMapHeight * 2, sphereMapHeight);

    computeGPUPointLightsData(pointLightCubeMapQuery);
    computeGPUSkeletonData(skelCubeMapQuery);

    convertCubeMapsToSphericalMaps(conversionQuery, mapSize);

    computeSphericalSkelMapsDirectLighting(directLightingQuery, mapSize);

    getSphericalMapsFromGPU(dataTransfertTimeQuery, mapSize);

    initDebugMaps(mapSize);

    m_fLightCubeMapRenderTime = ns2ms(pointLightCubeMapQuery.waitResult());
    m_fSkelCubeMapRenderTime = ns2ms(skelCubeMapQuery.waitResult());
    m_fConversionTime = ns2ms(conversionQuery.waitResult());
    m_fDirectLightingTime = ns2ms(directLightingQuery.waitResult());
    m_fDataTransfertTime = ns2ms(dataTransfertTimeQuery.waitResult());

    if(stats) {
        setAttribute(*stats, "initGPUBuffersTime", m_fInitGPUBuffersTime);
        setAttribute(*stats, "initCPUBuffersTime", m_fInitCPUBuffersTime);
        setAttribute(*stats, "lightCubeMapsRenderTime", m_fLightCubeMapRenderTime);
        setAttribute(*stats, "skelCubeMapsRenderTime", m_fSkelCubeMapRenderTime);
        setAttribute(*stats, "convertToSphericalMapsTime", m_fConversionTime);
        setAttribute(*stats, "directLightingTime", m_fDirectLightingTime);
        setAttribute(*stats, "dataTransfertTime", m_fDataTransfertTime);
        setAttribute(*stats, "computeSkelDistribTime", m_fComputeSkelDistribTime);
    }
}

void SceneViewer::exposeIO() {
    TwBar* bar = TwNewBar("SceneViewer");
    TwDefine(" SceneViewer size='240 640' ");

    atb::addVarRO(bar, ATB_VAR(m_fInitGPUBuffersTime));
    atb::addVarRO(bar, ATB_VAR(m_fInitCPUBuffersTime));
    atb::addVarRO(bar, ATB_VAR(m_fLightCubeMapRenderTime));
    atb::addVarRO(bar, ATB_VAR(m_fSkelCubeMapRenderTime));
    atb::addVarRO(bar, ATB_VAR(m_fConversionTime));
    atb::addVarRO(bar, ATB_VAR(m_fDirectLightingTime));
    atb::addVarRO(bar, ATB_VAR(m_fDataTransfertTime));
    atb::addVarRO(bar, ATB_VAR(m_fComputeSkelDistribTime));

    atb::addSeparator(bar);

    atb::addVarRO(bar, ATB_VAR(m_fMappingFromGBufferTime));

    atb::addSeparator(bar);

    atb::addVarRW(bar, ATB_VAR(m_fOcclusionError));
    atb::addVarRW(bar, ATB_VAR(m_bUseOcclusionSphereMap));
    atb::addVarRW(bar, ATB_VAR(m_bUseRaytracedOcclusion));
    atb::addVarRW(bar, ATB_VAR(m_bUseGridSkelMapping));
    atb::addVarRW(bar, "SM Bias", m_fSMBias);

    atb::addVarRW(bar, ATB_VAR(m_nNodeCubeMapRes));
    atb::addVarRO(bar, "Requested Node", m_nRequestedNode);
    atb::addVarRW(bar, "Compute Node Weight", m_nComputeNodeWeightSubIndex);

    atb::addSeparator(bar);

    atb::addVarRWCB(bar, "Display Data", m_DisplayData, m_nDisplayDataIndex, [this]() {
        // Clear side framebuffers
        m_PTNodeImage.clear();
        m_PDFNodeImage.clear();
        m_PerfectPDFNodeImage.fill(Vec4f(0.f));

        m_PTPickedViewPoint.clear();
        m_PDFPickedViewPoint.clear();
        m_PerfectPDFPickedPointImage.fill(Vec4f(0.f));

        if(m_nDisplayDataIndex == DD_NODE_SPHERICAL_SAMPLING) {
            m_DirectIrradianceRenderer->init(*m_pScene, *m_SphericalNodeCamera, m_PTNodeImage);
        } else if(m_nDisplayDataIndex == DD_NODE_DUAL_PARABOLOID_SAMPLING) {
            m_DirectIrradianceRenderer->init(*m_pScene, *m_DualParaboloidNodeCamera, m_PTNodeImage);
        } else if(m_nDisplayDataIndex == DD_PICKED_POINT_DUAL_PARABOLOID_SAMPLING) {
            m_DirectIrradianceRenderer->init(*m_pScene, *m_DualParaboloidPickedPointCamera, m_PTPickedViewPoint);
        } else if(m_nDisplayDataIndex == DD_PICKED_POINT_SPHERICAL_SAMPLING) {
            m_DirectIrradianceRenderer->init(*m_pScene, *m_SphericalPickedPointCamera, m_PTPickedViewPoint);
        }

        m_nCurrentDebugDualParaboloidChannel = -1;
        m_nCurrentDebugSphereChannel = -1;
    });
    atb::addVarRW(bar, ATB_VAR(m_ChannelIdx));
    atb::addVarRW(bar, ATB_VAR(m_fGamma));
    atb::addVarRW(bar, ATB_VAR(m_fIntegralPDFPerfect));
    atb::addVarRW(bar, ATB_VAR(m_fIntegralPDFApprox));

    atb::addSeparator(bar);

    bar = TwNewBar("SkelSphericalMapContainer");
    m_SkelSphericalMapContainer.exposeIO(bar);

    bar = TwNewBar("SkelDualParaboloidMapContainer");
    m_SkelDualParaboloidMapContainer.exposeIO(bar);
}

class SkelSamplingPDFRenderer: public TileProcessingRenderer {
public:
    SceneViewer& m_Module;
    const SkelSampler& m_SkelSampler;

    Vec4i m_PickedNodes;
    Vec4f m_PickedWeights;

    uint32_t m_nMaxNodeCountToUse = 4;

    SkelSamplingPDFRenderer(SceneViewer& module, const SkelSampler& skelSampler):
        m_Module(module), m_SkelSampler(skelSampler) {
    }

    virtual std::string getName() const {
        return "SkelSamplingPDFRenderer";
    }

    uint32_t m_nCurrentSample = 0u;

    void beginFrame() override {
        m_nCurrentSample = 0u;

        m_Module.getNearestNodes(
                    1,
                    &m_Module.m_PickedIntersection,
                    &m_PickedNodes,
                    &m_PickedWeights);
    }

//    bool beginTiles() override {
//        if(m_nCurrentSample < getSppCount()) {
//            m_Module.getNearestNodes(
//                        1,
//                        &m_Module.m_PickedIntersection,
//                        &m_PickedNodes,
//                        &m_PickedWeights);
//            return true;
//        }
//        return false;
//    }

//    void endTiles() override {
//        ++m_nCurrentSample;
//    }

    void endFrame() {
        ++m_nCurrentSample;
    }

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const override {
        TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
            auto pixelIdx = getPixelIndex(x, y);
            auto s2D = getPixelSample(threadID, m_nCurrentSample);
            auto ray = getPrimaryRay(x, y, s2D);
            if(ray) {
                if(dot(ray.dir, m_Module.m_PickedIntersection.Ns) < 0.f) {
                    getFramebuffer().accumulate(pixelIdx, Vec4f(1, 0, 1, 1));
                } else {
                    auto I = getScene().intersect(ray);
                    float pdf = pdfWrtIntersectionSolidAngle(m_SkelSampler, m_Module.m_PickedIntersection,
                                                 m_nMaxNodeCountToUse,
                                                 m_PickedNodes, m_PickedWeights, I.P, I.Ns);

                    if(pdf < 0.f) {
                        getFramebuffer().accumulate(pixelIdx, Vec4f(0, 0, 1, 1.f));
                    } else {
                        getFramebuffer().accumulate(pixelIdx, Vec4f(pdf, pdf, pdf, 1.f));
                    }
                }
            }
        });
    }

    virtual void doExposeIO(TwBar* bar) override {
        atb::addVarRW(bar, ATB_VAR(m_nMaxNodeCountToUse), "min=1 max =4");
    }
};

class PickedPointSkelSamplesRenderer: public Renderer {
    SceneViewer& m_Module;
    const SkelSampler& m_SkelSampler;

    Intersection m_PickedPoint;
    Vec4i m_PickedNodes;
    Vec4f m_PickedWeights;

    uint32_t m_nMaxNodeCountToUse = 4;

    RandomGenerator m_Rng;
    EmissionSampler m_EmissionSampler;

    TwBar* m_pBar = nullptr;

    float m_MCEstimation = 0.f;
    float m_fSum = 0.f;
    uint32_t m_nSamples = 0u;

public:
    PickedPointSkelSamplesRenderer(SceneViewer& module, const SkelSampler& skelSampler):
        m_Module(module), m_SkelSampler(skelSampler) {
        m_pBar = TwNewBar(toString(getMicroseconds()).c_str());
        atb::addVarRO(m_pBar, ATB_VAR(m_MCEstimation));
    }

    void doInit() override {
        m_PickedPoint = m_Module.m_PickedIntersection;
        m_Module.getNearestNodes(
                    1,
                    &m_Module.m_PickedIntersection,
                    &m_PickedNodes,
                    &m_PickedWeights);

        m_nSamples = 0.f;
        m_fSum = 0.f;
        m_MCEstimation = 0.f;
    }

    void doRender() override {
        m_EmissionSampler.initFrame(getScene());

        Vec3f P, N;
        float pdfWrtArea, pdfWrtSolidAngle;

        //auto s1D = m_Rng.getFloat();
        auto s2D = m_Rng.getFloat2();

        /*sampleSurfacePoint(m_SkelSampler, 1u,
                           m_PickedNodes, m_PickedWeights,
                           P, N,
                           pdfWrtArea,
                           s1D, s2D);*/

        m_SkelSampler.sampleSurfacePoint(m_PickedNodes[0], P, N, pdfWrtSolidAngle, pdfWrtArea, s2D);

        ++m_nSamples;

        Vec2f ndc;
        if(getCamera().getNDC(P, ndc)) {
            auto pixel = getPixel(ndcToUV(ndc),
                                  getFramebuffer().getSize());

            auto wi = m_PickedPoint.P - P;
            auto dist = length(wi);
            wi /= dist;
            pdfWrtSolidAngle = areaToSolidAnglePDF(pdfWrtArea, dist * dist, dot(N, wi));

            if(dot(m_PickedPoint.Ns, -wi) > 0.f) {
                m_fSum += 1.f;
            }
            m_MCEstimation = m_fSum / float(m_nSamples);

            /*
            auto L = estimateDirectIrradiance(getScene(),
                                     m_DirectLightSampler,
                                     Intersection(P, N),
                                     m_Rng.getFloat(),
                                     m_Rng.getFloat(),
                                     m_Rng.getFloat2());*/

            getFramebuffer().accumulate(getPixelIndex(pixel.x, pixel.y), Vec4f(Vec3f(max(0.f, dot(m_PickedPoint.Ns, -wi)) * pdfWrtSolidAngle), 1));
        }
    }
};

class NodeSkelSamplesRenderer: public Renderer {
    SceneViewer& m_Module;
    const SkelSampler& m_SkelSampler;

    RandomGenerator m_Rng;
public:
    NodeSkelSamplesRenderer(SceneViewer& module, const SkelSampler& skelSampler):
        m_Module(module), m_SkelSampler(skelSampler) {
    }

    void doInit() override {
    }

    void doRender() override {
        Vec3f P, N;
        float pdfWrtSolidAngle, pdfWrtArea;

        auto s2D = m_Rng.getFloat2();

        m_SkelSampler.sampleSurfacePoint(m_Module.m_nSelectedNodeIdx, P, N, pdfWrtSolidAngle, pdfWrtArea, s2D);

        Vec2f ndc;
        if(getCamera().getNDC(P, ndc)) {
            auto pixel = getPixel(ndcToUV(ndc),
                                  getFramebuffer().getSize());
            getFramebuffer().accumulate(getPixelIndex(pixel.x, pixel.y), Vec4f(pdfWrtSolidAngle, pdfWrtSolidAngle, pdfWrtSolidAngle, 1));
        }
    }
};

class SkelSamplingPDFWrtNodeRenderer: public TileProcessingRenderer {
public:
    SceneViewer& m_Module;
    const SkelSampler& m_SkelSampler;

    SkelSamplingPDFWrtNodeRenderer(SceneViewer& module, const SkelSampler& skelSampler):
        m_Module(module), m_SkelSampler(skelSampler) {
    }

    virtual std::string getName() const {
        return "SkelSamplingPDFWrtNodeRenderer";
    }

    uint32_t m_nCurrentSample = 0u;

    void beginFrame() override {
        m_nCurrentSample = 0u;
    }

//    bool beginTiles() override {
//        return m_nCurrentSample < getSppCount();
//    }

    void endFrame() override {
        ++m_nCurrentSample;
    }

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u& viewport) const {
        TileProcessingRenderer::processTilePixels(viewport, [&](uint32_t x, uint32_t y) {
            auto pixelIdx = getPixelIndex(x, y);
            auto s2D = getPixelSample(threadID, m_nCurrentSample);
            auto ray = getPrimaryRay(x, y, s2D);
            if(ray) {
                auto I = getScene().intersect(ray);
                if(I) {
                    float pdf = m_SkelSampler.pdfWrtNodeSolidAngle(m_Module.m_nSelectedNodeIdx, I.P);

                    if(pdf < 0.f) {
                        getFramebuffer().accumulate(pixelIdx, Vec4f(0, 0, 1, 1.f));
                    } else {
                        getFramebuffer().accumulate(pixelIdx, Vec4f(pdf, pdf, pdf, 1.f));
                    }
                }

                /*
                float pdf = m_SkelSampler.directionPdfWrtNodeSolidAngle(m_Module.m_nSelectedNodeIdx, ray.dir);
                getFramebuffer().accumulate(pixelIdx, Vec4f(pdf, pdf, pdf, 1.f));*/
            }
        });
    }
};

class DirectIrradianceRenderer: public TileProcessingRenderer {
public:
    EmissionSampler m_EmissionSampler;
    SceneViewer& m_Module;

    DirectIrradianceRenderer(SceneViewer& module):
        m_Module(module) {
    }

    std::string getName() const override {
        return "DirectLightingRenderer";
    }

    void preprocess() override {
        m_EmissionSampler.initFrame(getScene());
    }

    Vec4f Li(const Ray& ray, uint32_t threadID) const {
        Vec3f L = Vec3f(0.f);

        if(m_Module.m_PickedIntersection && dot(m_Module.m_PickedIntersection.Ns, ray.dir) <= 0.f) {
            return Vec4f(L, 1.f);
        }

        auto I = getScene().intersect(ray);
        if(!I) {
            return Vec4f(0, 0, 0, 1);
        }

        L += estimateDirectIrradiance(getScene(),
                                 m_EmissionSampler,
                                 I,
                                 getFloat(threadID),
                                 getFloat(threadID),
                                 getFloat2(threadID));

        return Vec4f(L, 1.f);
    }

    void processTile(uint32_t threadID, uint32_t tileID, const Vec4u &viewport) const override {
        processTileSamples(viewport, [this, threadID](uint32_t x, uint32_t y, uint32_t pixelID, uint32_t sampleID) {
            auto ray = getPrimaryRay(x, y, getPixelSample(threadID, sampleID));
            getFramebuffer().accumulate(pixelID, Li(ray, threadID));
        });
    }
};

void SceneViewer::addRenderers() {
    m_RenderModule.m_RendererManager.addRenderer("SkelMappingRenderer", [this]() {
        return makeUnique<SkelMappingRenderer>(*this);
    });
    m_RenderModule.m_RendererManager.addRenderer("SkelPathtraceRenderer", [this]() {
        return makeUnique<SkelPathtraceRenderer>(m_SkelSphericalMapContainer, m_ShaderManager, *m_pGLScene, m_GLSkelData);
    });
    m_RenderModule.m_RendererManager.addRenderer("SkelPathtracePreprocessNodesRenderer", [this]() {
        return makeUnique<SkelPathtracePreprocessNodesRenderer>(m_ShaderManager, *m_pGLScene, *this, m_GLSkelData);
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelPathtracePreprocessNodesWithResamplingRenderer", [this]() {
        return makeUnique<SkelPathtracePreprocessNodesWithResamplingRenderer>(m_ShaderManager, *m_pGLScene, *this, m_GLSkelData);
    });

    m_RenderModule.m_RendererManager.addRenderer("RadiosityBasedSkelSamplingPathtracer", [this]() {
        return makeUnique<RadiosityBasedSkelSamplingPathtracer>(m_ShaderManager, *m_pGLScene, *this, m_GLSkelData);
    });

    m_RenderModule.m_RendererManager.addRenderer("RadiosityBasedSkelSamplingPathtracerCPUOnly", [this]() {
        return makeUnique<RadiosityBasedSkelSamplingPathtracerCPUOnly>();
    });

    m_RenderModule.m_RendererManager.addRenderer("RadiosityBasedSkelRISPathtracerCPUOnly", [this]() {
        return makeUnique<RadiosityBasedSkelRISPathtracerCPUOnly>();
    });

    m_RenderModule.m_RendererManager.addRenderer("RadSkelRISPTNodeRepCPUOnly", [this]() {
        return makeUnique<RadSkelRISPTNodeRepCPUOnly>();
    });

    m_RenderModule.m_RendererManager.addRenderer("RadSkelPTVisMappingCPUOnly", [this]() {
        return makeUnique<RadSkelPTVisMappingCPUOnly>();
    });

    m_RenderModule.m_RendererManager.addRenderer("RadSkelSamplingPTCPU", [this]() {
        return makeUnique<RadSkelSamplingPTCPU>();
    });

    m_RenderModule.m_RendererManager.addRenderer("RadSkelSamplingNodeRepPTCPU", [this]() {
        return makeUnique<RadSkelSamplingNodeRepPTCPU>();
    });

    m_RenderModule.m_RendererManager.addRenderer("RadSkelRISAllPointsPTCPU", [this]() {
        return makeUnique<RadSkelRISAllPointsPTCPU>();
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelBasedBidir", [this]() {
        return makeUnique<SkelBasedBidir>(m_pGLDebugRenderer);
    });

    m_RenderModule.m_RendererManager.addRenderer("RadSkelPTVisMappingPathSpaceCPUOnly", [this]() {
        return makeUnique<RadSkelPTVisMappingPathSpaceCPUOnly>(m_pGLDebugRenderer);
    });

    m_RenderModule.m_RendererManager.addRenderer("RadSkelSamplingIntegralCutPTCPU", [this]() {
        return makeUnique<RadSkelSamplingIntegralCutPTCPU>();
    });

    m_RenderModule.m_RendererManager.addRenderer("SkelPathtraceImportanceResamplingRenderer", [this]() {
        return makeUnique<SkelPathtraceImportanceResamplingRenderer>(m_SkelSphericalMapContainer, m_ShaderManager, *m_pGLScene, m_GLSkelData);
    });
    m_RenderModule.m_RendererManager.addRenderer("SkelPathtraceRenderer_DualParaboloid", [this]() {
        return makeUnique<SkelPathtraceRenderer>(m_SkelDualParaboloidMapContainer, m_ShaderManager, *m_pGLScene, m_GLSkelData);
    });

    m_RenderModule.m_RendererManager.addRenderer("ImperfectSkelPathtraceRenderer", [this]() {
        return makeUnique<ImperfectSkelPathtraceRenderer>(*this, m_SkelSphericalMapContainer);
    });
    m_RenderModule.m_RendererManager.addRenderer("ReverseImperfectSkelPathtraceRenderer", [this]() {
        return makeUnique<ReverseImperfectSkelPathtraceRenderer>(*this, m_SkelSphericalMapContainer);
    });
    m_RenderModule.m_RendererManager.addRenderer("SkelMappingRendererFullCPU", [this]() {
        return makeUnique<SkelMappingRendererFullCPU>([this](const SurfacePoint& point) {
            return getNearestNode(point);
        });
    });
    m_RenderModule.m_RendererManager.addRenderer("NodeVisibilitySamplingCoherenceRenderer", [this]() {
        return makeUnique<NodeVisibilitySamplingCoherenceRenderer>(*this, m_SkelSphericalMapContainer);
    });

    m_DirectIrradianceRenderer = makeShared<DirectIrradianceRenderer>(*this);
    m_SkelSphericalSamplingPDFRenderer = makeShared<SkelSamplingPDFRenderer>(*this, m_SkelSphericalMapContainer);
    m_SkelSphericalSamplingPDFWrtNodeRenderer = makeShared<SkelSamplingPDFWrtNodeRenderer>(*this, m_SkelSphericalMapContainer);
    m_NodeSkelSphericalSamplesRenderer = makeShared<NodeSkelSamplesRenderer>(*this, m_SkelSphericalMapContainer);
    m_PickedPointSkelSphericalSamplesRenderer = makeShared<PickedPointSkelSamplesRenderer>(*this, m_SkelSphericalMapContainer);

    m_SkelDualParaboloidSamplingPDFRenderer = makeShared<SkelSamplingPDFRenderer>(*this, m_SkelDualParaboloidMapContainer);
    m_SkelDualParaboloidSamplingPDFWrtNodeRenderer = makeShared<SkelSamplingPDFWrtNodeRenderer>(*this, m_SkelDualParaboloidMapContainer);
    m_NodeSkelDualParaboloidSamplesRenderer = makeShared<NodeSkelSamplesRenderer>(*this, m_SkelDualParaboloidMapContainer);
    m_PickedPointSkelDualParaboloidSamplesRenderer = makeShared<PickedPointSkelSamplesRenderer>(*this, m_SkelDualParaboloidMapContainer);
}

void SceneViewer::drawFrame() {
    auto debugStream = newGLDebugStream(m_pGLDebugRenderer, "default");

    m_GBufferRenderPass.render(m_Camera.getProjMatrix(), m_Camera.getViewMatrix(),
                               m_ZNearFar.y, *m_pGLScene, m_GBuffer);

    m_GLFramebuffer.bindForDrawing();
    glClear(GL_COLOR_BUFFER_BIT);

    if(m_RenderModule.m_bCPURendering) {
        m_RenderModule.doCPURendering(m_CameraUpdateFlag, m_Camera, *m_pScene, m_GLImageRenderer);
    } else {
        m_FlatShadingPass.render(m_GBuffer, m_Camera.getRcpProjMatrix(),
                                 m_Camera.getViewMatrix(), m_ScreenTriangle);

        auto objectID = m_SelectedObjectID;
        int selectedNode = -1;
        if(objectID.x == SEG_SKEL_NODE_ID) {
            selectedNode = objectID.y;
        }

        glEnable(GL_BLEND);

        glBlendEquation(GL_FUNC_ADD);
        glBlendFunc(GL_ZERO, GL_SRC_COLOR);

        m_SkelMappingRenderPass.render(m_GBuffer, m_Camera, m_ScreenTriangle,
                                       m_GLSkelData,
                                       m_fSMBias, selectedNode, m_PickedIntersection,
                                       m_nRequestedNode,
                                       m_nComputeNodeWeightSubIndex);

        glDisable(GL_BLEND);
    }

    drawCurvSkel(debugStream);
    doPicking(debugStream);

    m_SkelNodeDebugModule.drawMaxball(debugStream);



    // Random sampling of points
//    RandomGenerator rng;

//    auto N = 4096u;
//    std::vector<float> s1DMeshBuffer(N);
//    std::vector<float> s1DTriangleBuffer(N);
//    std::vector<float> s1DTriangleSideBuffer(N);
//    std::vector<Vec2f> s2DTriangleBuffer(N);

//    for(auto i = 0u; i < N; ++i) {
//        s1DMeshBuffer[i] = rng.getFloat();
//        s1DTriangleBuffer[i] = rng.getFloat();
//        s1DTriangleSideBuffer[i] = rng.getFloat();
//        s2DTriangleBuffer[i] = rng.getFloat2();
//    }

//    std::vector<SurfacePointSample> points(N);
//    m_pScene->uniformSampleSurfacePoints(N,
//                                         s1DMeshBuffer.data(),
//                                         s1DTriangleBuffer.data(),
//                                         s1DTriangleSideBuffer.data(),
//                                         s2DTriangleBuffer.data(),
//                                         points.data());

//    for(auto i = 0u; i < N; ++i) {
//        m_pGLDebugRenderer->addArrow("default",
//                                     points[i].value.P,
//                                     points[i].value.Ns,
//                                     getArrowLength(),
//                                     0.3f * getArrowLength(),
//                                     Vec3f(1.f));
//    }

    //    // Test visibility of node points with spherical map
    //    if(m_nSelectedNodeIdx !=  UNDEFINED_NODE) {
    //        auto count = 0u;
    //        auto offset = m_nSelectedNodeIdx * m_nSphereMapSize;
    //        for(auto y = 0u; y < m_SphereMapSize.y; ++y) {
    //            for(auto x = 0u; x < m_SphereMapSize.x; ++x) {
    //                auto j = x + y * m_SphereMapSize.x;
    //                Vec2f uv = Vec2f(x + 0.5f, y + 0.5f) / Vec2f(m_SphereMapSize);
    //                //auto pos = m_NodePositionBuffers[offset + j];
    //                auto pos = m_pSkel->getNode(m_nSelectedNodeIdx).P +
    //                            m_NodeNormalDistBuffers[offset + j].w * sphericalMapping(uv);
    ////                m_pGLDebugRenderer->addLine(
    ////                            "default",
    ////                            pos,
    ////                            m_pSkel->getNode(m_nSelectedNodeIdx).P,
    ////                            Vec3f(1),
    ////                            Vec3f(1),
    ////                            5);

    //                if(m_PickedIntersection) {
    //                    auto wi = pos - m_PickedIntersection.P;
    //                    auto l = length(wi);
    //                    wi /= l;
    //                    auto shadowRay = secondaryRay(m_PickedIntersection, wi, l - 0.01 * m_ZNearFar.y);
    //                    if(dot(wi, m_PickedIntersection.Ns) > 0.f && !m_pScene->occluded(shadowRay)) {
    //                        ++count;
    //                    }
    ////                    else {
    ////                        m_pGLDebugRenderer->addLine(
    ////                                                    "default",
    ////                                                    pos,
    ////                                                    m_PickedIntersection.P,
    ////                                                    Vec3f(1),
    ////                                                    Vec3f(1),
    ////                                                    5);
    ////                    }
    //                }
    //            }
    //        }


            // Test visibility of node points with dual paraboloid map
    //        if(m_nSelectedNodeIdx !=  UNDEFINED_NODE) {
    //            auto count = 0u;
    //            auto offset = m_nSelectedNodeIdx * m_nSphereMapSize;
    //            for(auto y = 0u; y < m_SphereMapSize.y; ++y) {
    //                for(auto x = 0u; x < m_SphereMapSize.x; ++x) {
    //                    auto j = x + y * m_SphereMapSize.x;
    //                    Vec2f uv = Vec2f(x + 0.5f, y + 0.5f) / Vec2f(m_SphereMapSize);
    //                    auto wi = dualParaboloidMapping(uv);
    //                    if(wi.x || wi.y || wi.z) {
    //                        auto pos = m_pSkel->getNode(m_nSelectedNodeIdx).P +
    //                                                m_NodeNormalDualParaboloidsDistBuffers[offset + j].w * wi;
    //                        m_pGLDebugRenderer->addLine(
    //                                    "default",
    //                                    pos,
    //                                    m_pSkel->getNode(m_nSelectedNodeIdx).P,
    //                                    Vec3f(1),
    //                                    Vec3f(1),
    //                                    5);

    //                        if(m_PickedIntersection) {
    //                            auto wi = pos - m_PickedIntersection.P;
    //                            auto l = length(wi);
    //                            wi /= l;
    //                            auto shadowRay = secondaryRay(m_PickedIntersection, wi, l - 0.01 * m_ZNearFar.y);
    //                            if(dot(wi, m_PickedIntersection.Ns) > 0.f && !m_pScene->occluded(shadowRay)) {
    //                                ++count;
    //                            }
    //        //                    else {
    //        //                        m_pGLDebugRenderer->addLine(
    //        //                                                    "default",
    //        //                                                    pos,
    //        //                                                    m_PickedIntersection.P,
    //        //                                                    Vec3f(1),
    //        //                                                    Vec3f(1),
    //        //                                                    5);
    //        //                    }
    //                        }
    //                    }
    //                }
    //            }

    //        std::cerr << "Nb of visibles " << count << " / " << m_nSphereMapSize << std::endl;
    //    }

    if(m_nSelectedNodeIdx !=  UNDEFINED_NODE) {
        // Viewport of the dual paraboloid view
        auto offset = m_nSelectedNodeIdx * m_SkelSphericalMapContainer.getMapSize().x * m_SkelSphericalMapContainer.getMapSize().y;
        auto viewport = m_SideViewport;
        viewport.w = viewport.z * 0.5f;
        if((m_nDisplayDataIndex == DD_NODE_SPHERICAL_SAMPLING  || m_nDisplayDataIndex == DD_NODE_SPHERICAL_MAP) &&
                viewportContains(m_SelectedPixel, viewport)) {
            Vec2f uv = (Vec2f(m_SelectedPixel.x + 0.5f, m_SelectedPixel.y + 0.5f) - viewport.xy()) / viewport.zw();
            float sinTheta;
            auto wi = sphericalMapping(uv, sinTheta);
            if(wi.x || wi.y || wi.z) {
                auto pixel = Vec2u(uv * Vec2f(m_SkelSphericalMapContainer.getMapSize()));
                auto j = getPixelIndex(pixel, m_SkelSphericalMapContainer.getMapSize());
                auto normalDist = m_SkelSphericalMapContainer.getNormalDistMapsBufferPtr()[offset + j];
                auto pos = m_pSkel->getNode(m_nSelectedNodeIdx).P + normalDist.w * wi;

                m_pGLDebugRenderer->addLine(
                            "default",
                            pos,
                            m_pSkel->getNode(m_nSelectedNodeIdx).P,
                            Vec3f(1),
                            Vec3f(1),
                            5);

                float pixelArea = 1.f / (m_SkelSphericalMapContainer.getMapSize().x * m_SkelSphericalMapContainer.getMapSize().y);
                float surfaceArea = solidAngleToAreaPDF(
                            sphericalAnglesToSolidAnglePDF(
                                uvToSphericalAnglesPDF(pixelArea),
                                1.f / sinTheta),
                            sqr(normalDist.w), dot(-wi, normalDist.xyz()));
                float radius = sqrt(surfaceArea * one_over_pi<float>());

                m_pGLDebugRenderer->addArrow("default",
                                             pos,
                                             normalDist.xyz(),
                                             3.f * radius,
                                             radius,
                                             normalDist.xyz());
            }
        }
    }


    if(m_PickedIntersection) {
        Vec4i nodeIndex;
        Vec4f nodeWeights;
        //getNearestNodes(1, &m_PickedIntersection, &nodeIndex, &nodeWeights);
        nodeIndex = getNearestNodes(m_PickedIntersection, nodeWeights);

        Vec3f colors[] = {
            Vec3f(1, 0, 0),
            Vec3f(0, 1, 0),
            Vec3f(0, 0, 1),
            Vec3f(1, 1, 1)
        };

        for(int i = 0; i < 4; ++i) {
            if(nodeIndex[i] >= 0) {
                m_pGLDebugRenderer->addLine(
                            "default",
                            m_PickedIntersection.P.xyz(),
                            m_pSkel->getNode(nodeIndex[i]).P,
                            colors[i],
                            colors[i],
                            5);
            }
        }
    }




    m_pGLDebugRenderer->render(m_GBuffer, m_Camera);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glClear(GL_COLOR_BUFFER_BIT);

    drawGBuffer();
    drawFinalRender();
    drawSideView();

    if(m_bDoScreenshot) {
        doScreenshot();
        m_bDoScreenshot = false;
    }

}

void SceneViewer::onIntersectionPicked() {
    auto& I = m_PickedIntersection;

    if(I) {
        if(m_nDisplayDataIndex == DD_NODE_SPHERICAL_SAMPLING) {
            m_nDisplayDataIndex = DD_PICKED_POINT_SPHERICAL_SAMPLING;
        } else if(m_nDisplayDataIndex == DD_NODE_DUAL_PARABOLOID_SAMPLING) {
            m_nDisplayDataIndex = DD_PICKED_POINT_DUAL_PARABOLOID_SAMPLING;
        }

        m_SphericalPickedPointCamera = makeUnique<EnvironmentCamera>(lookAt(I.P + 4.f * I.Ns, I.P + 6.f * I.Ns, getOrthogonalUnitVector(I.Ns)));
        m_DualParaboloidPickedPointCamera = makeUnique<DualParaboloidCamera>(lookAt(I.P + 4.f * I.Ns, I.P + 6.f * I.Ns, getOrthogonalUnitVector(I.Ns)));
        m_PTPickedViewPoint.clear();
        m_PDFPickedViewPoint.clear();
        m_PerfectPDFPickedPointImage.fill(Vec4f(0.f));
        m_SamplesPickedViewPointFB.clear();

        if(m_nDisplayDataIndex == DD_PICKED_POINT_SPHERICAL_SAMPLING) {
           m_DirectIrradianceRenderer->init(*m_pScene, *m_SphericalPickedPointCamera, m_PTPickedViewPoint);
        } else if(m_nDisplayDataIndex == DD_PICKED_POINT_DUAL_PARABOLOID_SAMPLING) {
            m_DirectIrradianceRenderer->init(*m_pScene, *m_DualParaboloidPickedPointCamera, m_PTPickedViewPoint);
        }

        m_SkelSphericalSamplingPDFRenderer->init(*m_pScene, *m_SphericalPickedPointCamera, m_PDFPickedViewPoint);
        m_SkelDualParaboloidSamplingPDFRenderer->init(*m_pScene, *m_DualParaboloidPickedPointCamera, m_PDFPickedViewPoint);
        m_PickedPointSkelSphericalSamplesRenderer->init(*m_pScene, *m_SphericalPickedPointCamera, m_SamplesPickedViewPointFB);
        m_PickedPointSkelDualParaboloidSamplesRenderer->init(*m_pScene, *m_DualParaboloidPickedPointCamera, m_SamplesPickedViewPointFB);
    }
}

void SceneViewer::onObjectPicked() {
    if(m_SelectedObjectID.x != SEG_SKEL_NODE_ID) {
        m_nSelectedNodeIdx = UNDEFINED_NODE;
        return;
    }

    if(m_nDisplayDataIndex == DD_PICKED_POINT_SPHERICAL_SAMPLING) {
        m_nDisplayDataIndex = DD_NODE_SPHERICAL_SAMPLING;
    } else if(m_nDisplayDataIndex == DD_PICKED_POINT_DUAL_PARABOLOID_SAMPLING) {
        m_nDisplayDataIndex = DD_NODE_DUAL_PARABOLOID_SAMPLING;
    }

    m_nSelectedNodeIdx = m_SelectedObjectID.y;
    auto node = m_pSkel->getNode(m_nSelectedNodeIdx);

    m_DualParaboloidNodeCamera = makeUnique<DualParaboloidCamera>(lookAt(node.P, node.P + Vec3f(0, 0, -1), Vec3f(0, 1, 0)));
    m_SphericalNodeCamera = makeUnique<EnvironmentCamera>(lookAt(node.P, node.P + Vec3f(0, 0, -1), Vec3f(0, 1, 0)));

    m_PTNodeImage.clear();
    m_PDFNodeImage.clear();
    m_PerfectPDFNodeImage.fill(Vec4f(0.f));
    m_SamplesNodeFB.clear();

    if(m_nDisplayDataIndex == DD_NODE_SPHERICAL_SAMPLING) {
        m_DirectIrradianceRenderer->init(*m_pScene, *m_SphericalNodeCamera, m_PTNodeImage);
    } else if(m_nDisplayDataIndex == DD_NODE_DUAL_PARABOLOID_SAMPLING) {
        m_DirectIrradianceRenderer->init(*m_pScene, *m_DualParaboloidNodeCamera, m_PTNodeImage);
    }

    m_SkelSphericalSamplingPDFWrtNodeRenderer->init(*m_pScene, *m_SphericalNodeCamera, m_PDFNodeImage);
    m_SkelDualParaboloidSamplingPDFWrtNodeRenderer->init(*m_pScene, *m_DualParaboloidNodeCamera, m_PDFNodeImage);
    m_NodeSkelSphericalSamplesRenderer->init(*m_pScene, *m_SphericalNodeCamera, m_SamplesNodeFB);
    m_NodeSkelDualParaboloidSamplesRenderer->init(*m_pScene, *m_DualParaboloidNodeCamera, m_SamplesNodeFB);
}

void SceneViewer::drawNodeCubeMap() {
    auto viewport = m_SideViewport;
    auto objectID = m_SelectedObjectID;

    if(objectID.x != SEG_SKEL_NODE_ID) {
        return;
    }
    glViewport(viewport.x, viewport.y, viewport.z, 0.5 * viewport.z);
    if(m_ChannelIdx < ChannelCount) {
        GLResidentTextureScope scope(m_GLSkelData.m_NodeCubeMapContainer.getMapArray(m_ChannelIdx));
        m_CubeMapRenderPass.drawMap(m_GLSkelData.m_NodeCubeMapContainer, objectID.y, m_ChannelIdx, m_ZNearFar.x, m_ZNearFar.y, m_ScreenTriangle);
    }
}

void SceneViewer::drawNodeSphericalMap() {
    auto viewport = m_SideViewport;
    auto objectID = m_SelectedObjectID;

    if(objectID.x != SEG_SKEL_NODE_ID) {
        return;
    }

    glViewport(viewport.x, viewport.y, viewport.z, 0.5 * viewport.z);
    if(m_ChannelIdx != m_nCurrentDebugSphereChannel) {
        m_nCurrentDebugSphereChannel = m_ChannelIdx;

        // Send image to the GPU
        if(m_ChannelIdx < ChannelCount) {
            m_SphereMapsBuffer.bind(GL_PIXEL_UNPACK_BUFFER);
            m_DebugSphereMap.setSubImage(0, GL_RGBA, GL_FLOAT,
                                         (const GLvoid*) (MAX_BUFFERS_SIZE * m_ChannelIdx * sizeof(Vec4f)));
        } else {
            m_NodeDirectLightSphereMaps.bind(GL_PIXEL_UNPACK_BUFFER);
            m_DebugSphereMap.setSubImage(0, GL_RGBA, GL_FLOAT, 0);
        }

        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    }

    {
        GLResidentTextureScope scope(m_DebugSphereMap);
        if (m_ChannelIdx == m_GLSkelData.m_NodeCubeMapContainer.getDepthMapChannelIndex()) {
            m_GLImageRenderer.drawDepthTexture(objectID.y, m_DebugSphereMap,
                                                              m_ZNearFar.x, m_ZNearFar.y);
        }
        else {
            m_GLImageRenderer.drawTexture(objectID.y, m_DebugSphereMap);
        }
    }
}

void SceneViewer::drawLightDepthMap() {
    auto viewport = m_SideViewport;
    auto objectID = m_SelectedObjectID;

    glViewport(viewport.x, viewport.y, viewport.z, 0.5 * viewport.z);
    GLResidentTextureScope scope(m_PointLightsData.m_CubeMapContainer.getMapArray(0));
    m_CubeMapRenderPass.drawMap(m_PointLightsData.m_CubeMapContainer, 0, 0, m_ZNearFar.x, m_ZNearFar.y, m_ScreenTriangle);
}

void SceneViewer::drawSkelSphericalSamplingPickedPointData() {
    auto viewport = m_SideViewport;

    if(m_PickedIntersection) {
        m_DirectIrradianceRenderer->render();
        m_SkelSphericalSamplingPDFRenderer->render();
        m_PickedPointSkelSphericalSamplesRenderer->render();

        // Draw direct irradiance computed with pathtracing
        glViewport(viewport.x, viewport.y, viewport.z, 0.5 * viewport.z);
        m_GLImageRenderer.drawFramebuffer(m_fGamma, m_PTPickedViewPoint);

        // Compute perfect pdf and draw it
        glViewport(viewport.x, viewport.y + 0.5 * viewport.z, viewport.z, 0.5 * viewport.z);
        {
            float sum = 0.f;
            for(auto y = 0u; y < m_PTPickedViewPoint.getHeight(); ++y) {
                for(auto x = 0u; x < m_PTPickedViewPoint.getWidth(); ++x) {
                    auto v = m_PTPickedViewPoint.getChannel(0)(x, y);

                    if(v.w > 0.f) {
                        auto l = luminance(v.xyz() / v.w);

                        float theta = pi<float>() * (y + 0.5f) / float(m_PTPickedViewPoint.getHeight());
                        auto sinTheta = sin(theta);

                        sum += l * sinTheta;

                        m_PerfectPDFPickedPointImage(x, y) = Vec4f(l, l, l, 1.f);
                    }
                }
            }

            sum /= (m_PTPickedViewPoint.getWidth() * m_PTPickedViewPoint.getHeight());
            sum *= two_pi<float>() * pi<float>();
            for(auto& p: m_PerfectPDFPickedPointImage) {
                p /= sum;
                p.a = 1.f;
            }


            float sum2 = 0.f;
            for(auto y = 0u; y < m_PerfectPDFPickedPointImage.getHeight(); ++y) {
                for(auto x = 0u; x < m_PerfectPDFPickedPointImage.getWidth(); ++x) {
                    auto v = m_PerfectPDFPickedPointImage(x, y);

                    float theta = pi<float>() * (y + 0.5f) / float(m_PerfectPDFPickedPointImage.getHeight());
                    auto sinTheta = sin(theta);

                    sum2 += v.x * sinTheta;
                }
            }
            sum2 /= (m_PerfectPDFPickedPointImage.getWidth() * m_PerfectPDFPickedPointImage.getHeight());
            sum2 *= two_pi<float>() * pi<float>();

            m_fIntegralPDFPerfect = sum2;

            m_GLImageRenderer.drawImage(1.f, m_PerfectPDFPickedPointImage);
        }

        // Draw skel based pdf
        glViewport(viewport.x, viewport.y + viewport.z, viewport.z, 0.5 * viewport.z);
        {
            float sum2 = 0.f;
            for(auto y = 0u; y < m_PDFPickedViewPoint.getHeight(); ++y) {
                for(auto x = 0u; x < m_PDFPickedViewPoint.getWidth(); ++x) {
                    auto v = m_PDFPickedViewPoint.getChannel(0)(x, y);

                    float theta = pi<float>() * (y + 0.5f) / float(m_PDFNodeImage.getHeight());
                    auto sinTheta = sin(theta);

                    if(v.w > 0.f)  {
                        sum2 += (v.x / v.w) * sinTheta;
                    }
                }
            }
            sum2 /= (m_PDFNodeImage.getWidth() * m_PDFNodeImage.getHeight());
            sum2 *= two_pi<float>() * pi<float>();

            m_fIntegralPDFApprox = sum2;

            m_GLImageRenderer.drawFramebuffer(1.f, m_PDFPickedViewPoint);
        }

        glViewport(viewport.x, viewport.y + 1.5f * viewport.z, viewport.z, 0.5 * viewport.z);
        m_GLImageRenderer.drawFramebuffer(1.f, m_SamplesPickedViewPointFB);
    }
}

void SceneViewer::drawSkelDualParaboloidSamplingPickedPointData() {
    auto viewport = m_SideViewport;

    if(m_PickedIntersection) {
        m_DirectIrradianceRenderer->render();
        m_SkelDualParaboloidSamplingPDFRenderer->render();
        m_PickedPointSkelDualParaboloidSamplesRenderer->render();

        // Draw direct irradiance computed with pathtracing
        glViewport(viewport.x, viewport.y, viewport.z, 0.5 * viewport.z);
        m_GLImageRenderer.drawFramebuffer(m_fGamma, m_PTPickedViewPoint);

        // Compute perfect pdf and draw it
        glViewport(viewport.x, viewport.y + 0.5 * viewport.z, viewport.z, 0.5 * viewport.z);
        {
            float sum = 0.f;
            for(auto y = 0u; y < m_PTPickedViewPoint.getHeight(); ++y) {
                for(auto x = 0u; x < m_PTPickedViewPoint.getWidth(); ++x) {
                    auto v = m_PTPickedViewPoint.getChannel(0)(x, y);

                    if(v.w > 0.f) {
                        auto l = luminance(v.xyz() / v.w);
                        sum += l * dualParaboloidJacobian(getUV(x, y, m_PTPickedViewPoint.getSize()));
                        m_PerfectPDFPickedPointImage(x, y) = Vec4f(l, l, l, 1.f);
                    }
                }
            }

            sum /= m_PTPickedViewPoint.getPixelCount();
            for(auto& p: m_PerfectPDFPickedPointImage) {
                p /= sum;
                p.a = 1.f;
            }

            float sum2 = 0.f;
            for(auto y = 0u; y < m_PerfectPDFPickedPointImage.getHeight(); ++y) {
                for(auto x = 0u; x < m_PerfectPDFPickedPointImage.getWidth(); ++x) {
                    auto v = m_PerfectPDFPickedPointImage(x, y);
                    sum2 += v.x * dualParaboloidJacobian(getUV(x, y, m_PTPickedViewPoint.getSize()));
                }
            }
            sum2 /= m_PerfectPDFPickedPointImage.getPixelCount();

            m_fIntegralPDFPerfect = sum2;

            m_GLImageRenderer.drawImage(1.f, m_PerfectPDFPickedPointImage);
        }

        // Draw skel based pdf
        glViewport(viewport.x, viewport.y + viewport.z, viewport.z, 0.5 * viewport.z);
        {
            float sum2 = 0.f;
            for(auto y = 0u; y < m_PDFPickedViewPoint.getHeight(); ++y) {
                for(auto x = 0u; x < m_PDFPickedViewPoint.getWidth(); ++x) {
                    auto v = m_PDFPickedViewPoint.getChannel(0)(x, y);
                    if(v.w > 0.f)  {
                        sum2 += (v.x / v.w) * dualParaboloidJacobian(getUV(x, y, m_PDFPickedViewPoint.getSize()));
                    }
                }
            }
            sum2 /= m_PTPickedViewPoint.getPixelCount();

            m_fIntegralPDFApprox = sum2;

            m_GLImageRenderer.drawFramebuffer(1.f, m_PDFPickedViewPoint);
        }

        glViewport(viewport.x, viewport.y + 1.5f * viewport.z, viewport.z, 0.5 * viewport.z);
        m_GLImageRenderer.drawFramebuffer(1.f, m_SamplesPickedViewPointFB);
    }
}

void SceneViewer::drawSphericalSampling() {
    auto viewport = m_SideViewport;
    auto objectID = m_SelectedObjectID;

    if(m_nSelectedNodeIdx >= m_pSkel->size()) {
        return;
    }

    m_DirectIrradianceRenderer->render();
    m_SkelSphericalSamplingPDFWrtNodeRenderer->render();
    m_NodeSkelSphericalSamplesRenderer->render();

    // Draw the direct lighting image
    glViewport(viewport.x, viewport.y, viewport.z, 0.5 * viewport.z);
    m_GLImageRenderer.drawFramebuffer(m_fGamma, m_PTNodeImage);

    // Draw the perfect pdf image computed by dividing the direct lighting image by it's integral
    glViewport(viewport.x, viewport.y + 0.5 * viewport.z, viewport.z, 0.5 * viewport.z);
    {
        float sum = 0.f;
        for(auto y = 0u; y < m_PTNodeImage.getHeight(); ++y) {
            for(auto x = 0u; x < m_PTNodeImage.getWidth(); ++x) {
                auto v = m_PTNodeImage.getChannel(0)(x, y);
                auto l = luminance(v.xyz() / v.w);

                float theta = pi<float>() * (y + 0.5f) / float(m_PTNodeImage.getHeight());
                auto sinTheta = sin(theta);

                sum += l * sinTheta;
                m_PerfectPDFNodeImage(x, y) = Vec4f(l, l, l, 1);
            }
        }
        sum /= (m_PTNodeImage.getWidth() * m_PTNodeImage.getHeight());
        sum *= two_pi<float>() * pi<float>();

        for(auto& p: m_PerfectPDFNodeImage) {
            p /= sum;
            p.a = 1.f;
        }

        float sum2 = 0.f;
        for(auto y = 0u; y < m_PerfectPDFNodeImage.getHeight(); ++y) {
            for(auto x = 0u; x < m_PerfectPDFNodeImage.getWidth(); ++x) {
                auto v = m_PerfectPDFNodeImage(x, y);

                float theta = pi<float>() * (y + 0.5f) / float(m_PerfectPDFNodeImage.getHeight());
                auto sinTheta = sin(theta);

                sum2 += v.x * sinTheta;
            }
        }
        sum2 /= (m_PerfectPDFNodeImage.getWidth() * m_PerfectPDFNodeImage.getHeight());
        sum2 *= two_pi<float>() * pi<float>();

        m_fIntegralPDFPerfect = sum2;

        m_GLImageRenderer.drawImage(1.f, m_PerfectPDFNodeImage);
    }

    // Draw the pdf image computed with the renderer
    glViewport(viewport.x, viewport.y + viewport.z, viewport.z, 0.5 * viewport.z);
    {
        float sum2 = 0.f;
        auto count = 0u;
        for(auto y = 0u; y < m_PDFNodeImage.getHeight(); ++y) {
            for(auto x = 0u; x < m_PDFNodeImage.getWidth(); ++x) {
                auto v = m_PDFNodeImage.getChannel(0)(x, y);

                float theta = pi<float>() * (y + 0.5f) / float(m_PDFNodeImage.getHeight());
                auto sinTheta = sin(theta);

                if(v.w > 0.f) {
                    sum2 += (v.x / v.w) * sinTheta;
                    ++count;
                }
            }
        }
        sum2 /= count;
        sum2 *= two_pi<float>() * pi<float>();

        m_fIntegralPDFApprox = sum2;

        m_GLImageRenderer.drawFramebuffer(1.f, m_PDFNodeImage);
    }

    // Draw the GPU direct lighting map
    glViewport(viewport.x, viewport.y + 1.5f * viewport.z, viewport.z, 0.5 * viewport.z);
    if(ChannelCount != m_nCurrentDebugSphereChannel) {
        m_nCurrentDebugSphereChannel = ChannelCount;

        m_NodeDirectLightSphereMaps.bind(GL_PIXEL_UNPACK_BUFFER);
        m_DebugSphereMap.setSubImage(0, GL_RGBA, GL_FLOAT, 0);

        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    }

    {
        GLResidentTextureScope scope(m_DebugSphereMap);
        m_GLImageRenderer.drawTexture(objectID.y, m_DebugSphereMap);
    }

    glViewport(viewport.x, viewport.y + 1.5f * viewport.z, viewport.z, 0.5 * viewport.z);
    m_GLImageRenderer.drawFramebuffer(1.f, m_SamplesNodeFB);
}

void SceneViewer::drawDualParaSampling() {
    auto viewport = m_SideViewport;
    auto objectID = m_SelectedObjectID;

    if(m_nSelectedNodeIdx >= m_pSkel->size()) {
        return;
    }

    m_DirectIrradianceRenderer->render();
    m_SkelDualParaboloidSamplingPDFWrtNodeRenderer->render();
    m_NodeSkelDualParaboloidSamplesRenderer->render();

    glViewport(viewport.x, viewport.y, viewport.z, 0.5 * viewport.z);
    m_GLImageRenderer.drawFramebuffer(m_fGamma, m_PTNodeImage);

    glViewport(viewport.x, viewport.y + 0.5 * viewport.z, viewport.z, 0.5 * viewport.z);
    {
        float sum = 0.f;
        for(auto y = 0u; y < m_PTNodeImage.getHeight(); ++y) {
            for(auto x = 0u; x < m_PTNodeImage.getWidth(); ++x) {
                auto v = m_PTNodeImage.getChannel(0)(x, y);
                auto l = luminance(v.xyz() / v.w);

                sum += l * dualParaboloidJacobian(getUV(x, y, m_PTNodeImage.getSize()));
                m_PerfectPDFNodeImage(x, y) = Vec4f(l, l, l, 1);
            }
        }
        sum /= m_PTNodeImage.getPixelCount();

        for(auto& p: m_PerfectPDFNodeImage) {
            p /= sum;
            p.a = 1.f;
        }

        float sum2 = 0.f;
        for(auto y = 0u; y < m_PerfectPDFNodeImage.getHeight(); ++y) {
            for(auto x = 0u; x < m_PerfectPDFNodeImage.getWidth(); ++x) {
                auto v = m_PerfectPDFNodeImage(x, y);

                sum2 += v.x * dualParaboloidJacobian(getUV(x, y, m_PTNodeImage.getSize()));
            }
        }
        sum2 /= m_PTNodeImage.getPixelCount();

        m_fIntegralPDFPerfect = sum2;




//                for(auto y = 0u; y < m_PerfectPDFNodeImage.getHeight(); ++y) {
//                    for(auto x = 0u; x < m_PerfectPDFNodeImage.getWidth(); ++x) {
//                        auto v1 = m_PerfectPDFNodeImage(x, y);
//                        auto v2 = m_PDFNodeImage(x, y);

//                        if(v2.w) {
//                            v2 /= v2.w;
//                        }

//                        auto v = v1.x - v2.x;
//                        v = v * v;

//                        m_PerfectPDFNodeImage(x, y) = Vec4f(v, v, v, 1.f);
//                    }
//                }


        m_GLImageRenderer.drawImage(1.f, m_PerfectPDFNodeImage);
    }

    glViewport(viewport.x, viewport.y + viewport.z, viewport.z, 0.5 * viewport.z);
    {
        float sum2 = 0.f;
        auto count = 0u;
        for(auto y = 0u; y < m_PDFNodeImage.getHeight(); ++y) {
            for(auto x = 0u; x < m_PDFNodeImage.getWidth(); ++x) {
                auto v = m_PDFNodeImage.getChannel(0)(x, y);

                auto jacobian = dualParaboloidJacobian(getUV(x, y, m_PTNodeImage.getSize()));
                if(v.w > 0.f && jacobian > 0.f) {
                    sum2 += (v.x / v.w) * jacobian;
                    ++count;
                }
            }
        }
        sum2 /= count;

        m_fIntegralPDFApprox = sum2;

        m_GLImageRenderer.drawFramebuffer(1.f, m_PDFNodeImage);
    }

    glViewport(viewport.x, viewport.y + 1.5f * viewport.z, viewport.z, 0.5 * viewport.z);
    if(ChannelCount != m_nCurrentDebugDualParaboloidChannel) {
        m_nCurrentDebugDualParaboloidChannel = ChannelCount;

        m_NodeDirectLightDualParaboloidMaps.bind(GL_PIXEL_UNPACK_BUFFER);
        m_DebugSphereMap.setSubImage(0, GL_RGBA, GL_FLOAT, 0);

        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    }

    {
        GLResidentTextureScope scope(m_DebugSphereMap);
        m_GLImageRenderer.drawTexture(objectID.y, m_DebugSphereMap);
    }

    glViewport(viewport.x, viewport.y + 1.5f * viewport.z, viewport.z, 0.5 * viewport.z);
    m_GLImageRenderer.drawFramebuffer(1.f, m_SamplesNodeFB);
}

void SceneViewer::drawNodeDualParaboloidMap() {
    auto viewport = m_SideViewport;
    auto objectID = m_SelectedObjectID;

    if(objectID.x != SEG_SKEL_NODE_ID) {
        return;
    }

    glViewport(viewport.x, viewport.y, viewport.z, 0.5 * viewport.z);
    if(m_ChannelIdx != m_nCurrentDebugDualParaboloidChannel) {
        m_nCurrentDebugDualParaboloidChannel = m_ChannelIdx;

        // Send image to the GPU
        if(m_ChannelIdx < ChannelCount) {
            m_DualParaboloidMapBuffers.bind(GL_PIXEL_UNPACK_BUFFER);
            m_DebugDualParaboloidMap.setSubImage(0, GL_RGBA, GL_FLOAT,
                                         (const GLvoid*) (MAX_BUFFERS_SIZE * m_ChannelIdx * sizeof(Vec4f)));
        } else {
            m_NodeDirectLightDualParaboloidMaps.bind(GL_PIXEL_UNPACK_BUFFER);
            m_DebugDualParaboloidMap.setSubImage(0, GL_RGBA, GL_FLOAT, 0);
        }

        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    }

    {
        GLResidentTextureScope scope(m_DebugDualParaboloidMap);
        if (m_ChannelIdx == m_GLSkelData.m_NodeCubeMapContainer.getDepthMapChannelIndex()) {
            m_GLImageRenderer.drawDepthTexture(objectID.y, m_DebugDualParaboloidMap,
                                               m_ZNearFar.x, m_ZNearFar.y);
        }
        else {
            m_GLImageRenderer.drawTexture(objectID.y, m_DebugDualParaboloidMap);
        }
    }
}

void SceneViewer::drawSideView() {
    if(!m_pSkel) {
        return;
    }

    switch(m_nDisplayDataIndex) {
        case DD_NODE_CUBE_MAP:
            drawNodeCubeMap();
            break;
        case DD_NODE_SPHERICAL_MAP:
            drawNodeSphericalMap();
            break;
        case DD_LIGHT_DEPTH_MAP:
            drawLightDepthMap();
            break;
        case DD_PICKED_POINT_SPHERICAL_SAMPLING:
            drawSkelSphericalSamplingPickedPointData();
            break;
        case DD_NODE_SPHERICAL_SAMPLING:
            drawSphericalSampling();
            break;
        case DD_NODE_DUAL_PARABOLOID_MAP:
            drawNodeDualParaboloidMap();
            break;
        case DD_NODE_DUAL_PARABOLOID_SAMPLING:
            drawDualParaSampling();
            break;
        case DD_PICKED_POINT_DUAL_PARABOLOID_SAMPLING:
            drawSkelDualParaboloidSamplingPickedPointData();
            break;
    }
}

void SceneViewer::getNearestNodesFromGBuffer(
        const ProjectiveCamera& camera,
        Vec4i* nodeIndices,
        Vec4f* nodeWeights) {
    auto start = getMicroseconds();

    m_GBufferRenderPass.render(camera.getProjMatrix(), camera.getViewMatrix(),
                               m_ZNearFar.y, *m_pGLScene, m_GBuffer);

    auto count = m_GBuffer.getWidth() * m_GBuffer.getHeight();

    m_SamplingData.reserve(count);

//    m_GLGBuffer2PointBufferComputePass.compute(*m_pContext, m_SamplingData.m_PointBuffer.getGPUAddress());

//    m_GLSkeletonMappingComputePass.compute(*m_pContext, m_SMContainer.getIndirectionTexture().getTextureHandle(),
//                                           m_NodeSMBuffer,
//                                           m_NodeViewMatrixBuffer, m_NodeRadiusBuffer,
//                                           m_SMRenderPass.getFaceProjectionMatrices(),
//                                           m_fSMBias, m_nComputeNodeWeightSubIndex,
//                                           m_SamplingData.m_PointBuffer,
//                                           m_SamplingData.m_NodeBuffer,
//                                           m_SamplingData.m_WeightsBuffer);

    m_GLSkelData.m_NodeCubeMapContainer.makeResident();
    m_SkelMappingComputePass.computeFromGBuffer(
                m_GBuffer,
                camera,
                m_GLSkelData,
                m_fSMBias, m_nComputeNodeWeightSubIndex,
                m_SamplingData.m_NodeBuffer.getGPUAddress(),
                m_SamplingData.m_WeightBuffer.getGPUAddress());
    m_GLSkelData.m_NodeCubeMapContainer.makeNonResident();

    if(nodeIndices) {
        m_SamplingData.m_NodeBuffer.getData(0, count, nodeIndices);
    }
    if(nodeWeights) {
        m_SamplingData.m_WeightBuffer.getData(0, count, nodeWeights);
    }

    auto end = getMicroseconds();

    m_fMappingFromGBufferTime = us2ms(end - start);
}

void SceneViewer::getNearestNodes(
                     uint32_t count,
                     const Intersection* intersections,
                     Vec4i* nodeIndices,
                     Vec4f* nodeWeights) {
    auto start = getMicroseconds();

    m_SamplingData.reserve(count);

    auto pPoint = m_SamplingData.m_PointBuffer.map(GL_MAP_WRITE_BIT);
    for(auto i = 0u; i < count; ++i) {
        pPoint[i].P = Vec4f(intersections[i].P, 1);
        pPoint[i].N = Vec4f(intersections[i].Ns, 0);
    }
    m_SamplingData.m_PointBuffer.unmap();

    m_GLSkelData.m_NodeCubeMapContainer.makeResident();
    m_SkelMappingComputePass.compute(
                count,
                m_SamplingData.m_PointBuffer.getGPUAddress(),
                m_GLSkelData,
                m_fSMBias, m_nComputeNodeWeightSubIndex,
                m_SamplingData.m_NodeBuffer.getGPUAddress(),
                m_SamplingData.m_WeightBuffer.getGPUAddress());
    m_GLSkelData.m_NodeCubeMapContainer.makeNonResident();

    if(nodeIndices) {
        m_SamplingData.m_NodeBuffer.getData(0, count, nodeIndices);
    }
    if(nodeWeights) {
        m_SamplingData.m_WeightBuffer.getData(0, count, nodeWeights);
    }



    // ON CPU test:
//    processTasks(count, [this, nodeIndices, nodeWeights, intersections](uint32_t taskID, uint32_t threadID) {
//        Vec4f weights;
//        if(nodeIndices) {
//            nodeIndices[taskID] = getNearestNodes(intersections[taskID], weights);
//        }
//        if(nodeWeights) {
//            nodeWeights[taskID] = weights;
//        }
//    });

    auto end = getMicroseconds();

    m_fMappingFromGBufferTime = us2ms(end - start);
}

void SceneViewer::getNearestNodes(uint32_t count,
                     const Intersection* intersections,
                     GLBufferAddress<Vec4i> nodeIndices) {
    auto start = getMicroseconds();

    m_SamplingData.reserve(count);

    auto pPoint = m_SamplingData.m_PointBuffer.map(GL_MAP_WRITE_BIT);
    for(auto i = 0u; i < count; ++i) {
        pPoint[i].P = Vec4f(intersections[i].P, 1);
        pPoint[i].N = Vec4f(intersections[i].Ns, 0);
    }
    m_SamplingData.m_PointBuffer.unmap();

    m_GLSkelData.m_NodeCubeMapContainer.makeResident();
    m_SkelMappingComputePass.compute(
                count,
                m_SamplingData.m_PointBuffer.getGPUAddress(),
                m_GLSkelData,
                m_fSMBias, m_nComputeNodeWeightSubIndex,
                nodeIndices,
                m_SamplingData.m_WeightBuffer.getGPUAddress());
    m_GLSkelData.m_NodeCubeMapContainer.makeNonResident();

    auto end = getMicroseconds();
}

bool SceneViewer::occludedSphereMap(uint32_t nodeIdx, const Vec3f& dir, float l) {
    return m_SkelSphericalMapContainer.occluded(nodeIdx, dir, l);
}

bool SceneViewer::occludedDualParaboloidMap(uint32_t nodeIdx, const Vec3f& dir, float l) {
    return m_SkelDualParaboloidMapContainer.occluded(nodeIdx, dir, l);
}

bool SceneViewer::occluded(uint32_t nodeIdx, const Vec3f& dir, float l, const SurfacePoint& point) {
    return false;

    if(m_bUseRaytracedOcclusion) {
       return m_pScene->occluded(secondaryRay(point, -dir, l));
    } else if(m_bUseOcclusionSphereMap) {
        return occludedSphereMap(nodeIdx, dir, l);
    } else {
        return occludedDualParaboloidMap(nodeIdx, dir, l);
    }
}

}
