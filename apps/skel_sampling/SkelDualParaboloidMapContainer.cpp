#include "SkelDualParaboloidMapContainer.hpp"
#include "SceneViewer.hpp"

namespace BnZ {

void SkelDualParaboloidMapContainer::preprocess(tinyxml2::XMLElement* pStatistics) {
    m_pSkel = &(*m_Viewer.getScene().getSegmentedCurvSkeleton());
    m_MapSize.x = 2 * m_nMapHeightSetting;
    m_MapSize.y = m_nMapHeightSetting;
    m_nMapCount = m_pSkel->size();
    m_nMapSize = m_MapSize.x * m_MapSize.y;
    m_nDistributionSize = getDistribution2DBufferSize(m_MapSize.x, m_MapSize.y);

    m_Viewer.computeDualParaboloidMaps(m_nMapHeightSetting, pStatistics);

    processTasks(m_nMapCount, [this](uint32_t i, uint32_t threadID) {
        uint32_t offset = i * m_nMapSize;

        buildDistribution2D([this, offset, i](uint32_t x, uint32_t y) {
            auto uv = getUV(Vec2u(x, y), m_MapSize);
            auto pixelIdx = getPixelIndex(x, y, m_MapSize);
            auto lum = luminance(m_ImportanceMapsBuffer[offset + pixelIdx].rgb());

            auto jacobian = dualParaboloidJacobian(uv);
            return lum * jacobian;
        }, m_DistributionsBuffer.get() + i * m_nDistributionSize,
           m_MapSize.x, m_MapSize.y);

        for(auto y = 0u; y < m_MapSize.y; ++y) {
            for(auto x = 0u; x < m_MapSize.x; ++x) {
                auto pixelIdx = getPixelIndex(x, y, m_MapSize);
                auto uv = getUV(Vec2u(x, y), m_MapSize);
                m_PositionMapsBuffer[offset + pixelIdx] = m_pSkel->getNode(i).P +
                        m_NormalDistMapsBuffer[offset + pixelIdx].w * dualParaboloidMapping(uv);
            }
        }
    });
}

}
