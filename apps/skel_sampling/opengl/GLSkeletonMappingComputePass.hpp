#pragma once

#include "GLSkeletonData.hpp"
#include <bonez/opengl/cube_mapping/GLCubeMapContainer.hpp>
#include <bonez/opengl/GLGBuffer.hpp>
#include <bonez/scene/cameras/ProjectiveCamera.hpp>

namespace BnZ {

class GLSkeletonMappingComputePass {
public:
    struct Point {
        Vec4f P, N;
    };

    struct FromBufferComputePass {
        GLProgram m_Program;

        BNZ_GLUNIFORM(m_Program, GLBufferAddress<Point>, uPointBuffer);
        BNZ_GLUNIFORM(m_Program, GLuint, uPointCount);
        BNZ_GLUNIFORM(m_Program, GLBufferAddress<Vec4i>, uNodeBuffer);
        BNZ_GLUNIFORM(m_Program, GLBufferAddress<Vec4f>, uWeightsBuffer);

        GLSkelDataUniforms uSkelData { m_Program };

        FromBufferComputePass(const GLShaderManager& shaderManager):
            m_Program(shaderManager.buildProgram({
                                                     "cube_mapping/cubeMapUtils.cs",
                                                     "skel_sampling/skelMappingUtils.cs",
                                                     "skel_sampling/skelSampling1.cs"
                                                 })) {
        }
    };

    FromBufferComputePass m_FromBufferComputePass;

    struct FromGBufferComputePass {
        GLProgram m_Program;

        GLGBufferUniform uGBuffer { m_Program };
        BNZ_GLUNIFORM(m_Program, Mat4f, uRcpProjMatrix);
        BNZ_GLUNIFORM(m_Program, Mat4f, uRcpViewMatrix);
        BNZ_GLUNIFORM(m_Program, GLBufferAddress<Vec4i>, uNodeBuffer);
        BNZ_GLUNIFORM(m_Program, GLBufferAddress<Vec4f>, uWeightsBuffer);

        GLSkelDataUniforms uSkelData { m_Program };

        FromGBufferComputePass(const GLShaderManager& shaderManager):
            m_Program(shaderManager.buildProgram({
                                                     "cube_mapping/cubeMapUtils.cs",
                                                     "skel_sampling/skelMappingUtils.cs",
                                                     "skel_sampling/skelSamplingFromGBuffer.cs"
                                                 })) {
        }
    };

    FromGBufferComputePass m_FromGBufferComputePass;

    GLSkeletonMappingComputePass(const GLShaderManager& shaderManager):
        m_FromBufferComputePass(shaderManager),
        m_FromGBufferComputePass(shaderManager) {
    }

    template<uint32_t ChannelCount>
    void compute(
            uint32_t pointCount,
            GLBufferAddress<Point> pointBuffer,
            const GLSkeletonData<ChannelCount>& skelData,
            float smBias,
            uint32_t computeNodeWeightSubroutineIndex,
            GLBufferAddress<Vec4i> nodeBuffer,
            GLBufferAddress<Vec4f> weightsBuffer) {
        m_FromBufferComputePass.m_Program.use();

        m_FromBufferComputePass.uPointCount.set(pointCount);
        m_FromBufferComputePass.uSkelData.set(skelData, smBias);
        m_FromBufferComputePass.uPointBuffer.set(pointBuffer);
        m_FromBufferComputePass.uNodeBuffer.set(nodeBuffer);
        m_FromBufferComputePass.uWeightsBuffer.set(weightsBuffer);

        glUniformSubroutinesuiv(GL_COMPUTE_SHADER, 1, &computeNodeWeightSubroutineIndex);

        const uint32_t workGroupSize = 1024;
        const uint32_t numWorkGroup = 1 + pointCount / workGroupSize;

        glDispatchCompute(numWorkGroup, 1, 1);

        glMemoryBarrier(GL_ALL_BARRIER_BITS);
    }

    template<uint32_t ChannelCount>
    void computeFromGBuffer(
            const GLGBuffer& gBuffer,
            const ProjectiveCamera& camera,
            const GLSkeletonData<ChannelCount>& skelData,
            float smBias,
            uint32_t computeNodeWeightSubroutineIndex,
            GLBufferAddress<Vec4i> nodeBuffer,
            GLBufferAddress<Vec4f> weightsBuffer) {
        m_FromGBufferComputePass.m_Program.use();

        m_FromGBufferComputePass.uSkelData.set(skelData, smBias);
        m_FromGBufferComputePass.uGBuffer.set(gBuffer);
        m_FromGBufferComputePass.uNodeBuffer.set(nodeBuffer);
        m_FromGBufferComputePass.uWeightsBuffer.set(weightsBuffer);
        m_FromGBufferComputePass.uRcpProjMatrix.set(camera.getRcpProjMatrix());
        m_FromGBufferComputePass.uRcpViewMatrix.set(camera.getRcpViewMatrix());

        glUniformSubroutinesuiv(GL_COMPUTE_SHADER, 1, &computeNodeWeightSubroutineIndex);

        const uint32_t workGroupSizeX = 32;
        const uint32_t workGroupSizeY = 32;
        const uint32_t numWorkGroupX = 1 + gBuffer.getWidth() / workGroupSizeX;
        const uint32_t numWorkGroupY = 1 + gBuffer.getHeight() / workGroupSizeY;

        glDispatchCompute(numWorkGroupX, numWorkGroupY, 1);

        glMemoryBarrier(GL_ALL_BARRIER_BITS);
    }
};

}
