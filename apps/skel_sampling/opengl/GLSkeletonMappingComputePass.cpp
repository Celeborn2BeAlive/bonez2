#include "GLSkeletonMappingComputePass.hpp"

namespace BnZ {

/*
void GLSkeletonMappingComputePass::compute(Context& context,
                                           GLTextureCubeMapHandle indirectionTexture,
                                           const GLBuffer<GLuint64>& shadowMapBuffer,
                                           const GLBuffer<Mat4f>& viewMatrixBuffer,
                                           const GLBuffer<float>& nodeRadiusBuffer,
                                           const Mat4f* faceProjMatrices,
                                           float smBias,
                                           uint32_t computeNodeWeightSubroutineIndex,
                                           const GLBuffer<Point>& pointBuffer,
                                           GLBuffer<Vec4i>& nodeBuffer,
                                           GLBuffer<Vec4f>& weightsBuffer) {
    m_FromBufferComputePass.m_Program.use();

    m_FromBufferComputePass.uSMCount.set(shadowMapBuffer.size());
    m_FromBufferComputePass.uShadowMapBuffer.set(shadowMapBuffer.getGPUAddress());
    m_FromBufferComputePass.uNodeViewMatrixBuffer.set(viewMatrixBuffer.getGPUAddress());
    m_FromBufferComputePass.uNodeRadiusBuffer.set(nodeRadiusBuffer.getGPUAddress());
    m_FromBufferComputePass.uFaceProjectionMatrices.set(6, faceProjMatrices);
    m_FromBufferComputePass.uSMBias.set(smBias);
    m_FromBufferComputePass.uPointBuffer.set(pointBuffer.getGPUAddress());
    m_FromBufferComputePass.uPointCount.set(pointBuffer.size());
    m_FromBufferComputePass.uNodeBuffer.set(nodeBuffer.getGPUAddress());
    m_FromBufferComputePass.uWeightsBuffer.set(weightsBuffer.getGPUAddress());
    m_FromBufferComputePass.uIndirectionTexture.set(indirectionTexture);

    glUniformSubroutinesuiv(GL_COMPUTE_SHADER, 1, &computeNodeWeightSubroutineIndex);

    const uint32_t workGroupSize = 1024;
    const uint32_t numWorkGroup = 1 + pointBuffer.size() / workGroupSize;

    glDispatchCompute(numWorkGroup, 1, 1);

    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}*/

}



//#include "GLSkeletonMappingComputePass.hpp"

//namespace BnZ {

//void GLSkeletonMappingComputePass::compute(Context& context,
//                                           GLTextureCubeMapHandle indirectionTexture,
//                                           const GLBuffer<GLuint64>& shadowMapBuffer,
//                                           const GLBuffer<Mat4f>& viewMatrixBuffer,
//                                           const GLBuffer<float>& nodeRadiusBuffer,
//                                           const Mat4f* faceProjMatrices,
//                                           float smBias,
//                                           uint32_t computeNodeWeightSubroutineIndex,
//                                           const GLBuffer<Point>& pointBuffer,
//                                           GLBuffer<Vec4i>& nodeBuffer,
//                                           GLBuffer<Vec4f>& weightsBuffer) {
//    m_FromBufferComputePass.m_Program.use();

//    m_FromBufferComputePass.uSMCount.set(shadowMapBuffer.size());
//    m_FromBufferComputePass.uShadowMapBuffer.set(shadowMapBuffer.getGPUAddress());
//    m_FromBufferComputePass.uNodeViewMatrixBuffer.set(viewMatrixBuffer.getGPUAddress());
//    m_FromBufferComputePass.uNodeRadiusBuffer.set(nodeRadiusBuffer.getGPUAddress());
//    m_FromBufferComputePass.uFaceProjectionMatrices.set(6, faceProjMatrices);
//    m_FromBufferComputePass.uSMBias.set(smBias);
//    m_FromBufferComputePass.uPointBuffer.set(pointBuffer.getGPUAddress());
//    m_FromBufferComputePass.uPointCount.set(pointBuffer.size());
//    m_FromBufferComputePass.uNodeBuffer.set(nodeBuffer.getGPUAddress());
//    m_FromBufferComputePass.uWeightsBuffer.set(weightsBuffer.getGPUAddress());
//    m_FromBufferComputePass.uIndirectionTexture.set(indirectionTexture);

//    glUniformSubroutinesuiv(GL_COMPUTE_SHADER, 1, &computeNodeWeightSubroutineIndex);

//    const uint32_t workGroupSize = 1024;
//    const uint32_t numWorkGroup = 1 + pointBuffer.size() / workGroupSize;

//    glDispatchCompute(numWorkGroup, 1, 1);

//    glMemoryBarrier(GL_ALL_BARRIER_BITS);
//}

//void GLSkeletonMappingComputePass::computeFromGBuffer(Context& context,
//                                           GLTextureCubeMapHandle indirectionTexture,
//                                           const GLBuffer<GLuint64>& shadowMapBuffer,
//                                           const GLBuffer<Mat4f>& viewMatrixBuffer,
//                                           const GLBuffer<float>& nodeRadiusBuffer,
//                                           const Mat4f* faceProjMatrices,
//                                           float smBias,
//                                           uint32_t computeNodeWeightSubroutineIndex,
//                                           GLBuffer<Vec4i>& nodeBuffer,
//                                           GLBuffer<Vec4f>& weightsBuffer) {
//    m_FromGBufferComputePass.m_Program.use();

//    m_FromGBufferComputePass.uSMCount.set(shadowMapBuffer.size());
//    m_FromGBufferComputePass.uShadowMapBuffer.set(shadowMapBuffer.getGPUAddress());
//    m_FromGBufferComputePass.uNodeViewMatrixBuffer.set(viewMatrixBuffer.getGPUAddress());
//    m_FromGBufferComputePass.uNodeRadiusBuffer.set(nodeRadiusBuffer.getGPUAddress());
//    m_FromGBufferComputePass.uFaceProjectionMatrices.set(6, faceProjMatrices);
//    m_FromGBufferComputePass.uSMBias.set(smBias);
//    m_FromGBufferComputePass.uGBuffer.set(context.getGLGBuffer());
//    m_FromGBufferComputePass.uNodeBuffer.set(nodeBuffer.getGPUAddress());
//    m_FromGBufferComputePass.uWeightsBuffer.set(weightsBuffer.getGPUAddress());
//    m_FromGBufferComputePass.uIndirectionTexture.set(indirectionTexture);
//    m_FromGBufferComputePass.uRcpProjMatrix.set(context.getCamera().getRcpProjMatrix());
//    m_FromGBufferComputePass.uRcpViewMatrix.set(context.getCamera().getRcpViewMatrix());

//    glUniformSubroutinesuiv(GL_COMPUTE_SHADER, 1, &computeNodeWeightSubroutineIndex);

//    const uint32_t workGroupSizeX = 32;
//    const uint32_t workGroupSizeY = 32;
//    const uint32_t numWorkGroupX = 1 + context.getGLGBuffer().getWidth() / workGroupSizeX;
//    const uint32_t numWorkGroupY = 1 + context.getGLGBuffer().getHeight() / workGroupSizeY;

//    glDispatchCompute(numWorkGroupX, numWorkGroupY, 1);

//    glMemoryBarrier(GL_ALL_BARRIER_BITS);
//}

//}
