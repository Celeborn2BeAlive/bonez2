#pragma once

#include "GLSkeletonData.hpp"
#include <bonez/opengl/cube_mapping/GLCubeMapContainer.hpp>
#include <bonez/opengl/lighting/GLPointLightsData.hpp>

namespace BnZ {

class GLNodeDualParaboloidMapDirectLightingComputePass {
public:
    using PointLightData = GLPointLightsData<1>::PointLightData;

    GLNodeDualParaboloidMapDirectLightingComputePass(const GLShaderManager& shaderManager):
        m_Program(shaderManager.buildProgram({ "cube_mapping/cubeMapUtils.cs", "skel_sampling/dualParaboloidMapDirectLighting.cs" })) {
    }

    void compute(uint32_t mapWidth, uint32_t mapHeight,
                 uint32_t mapCount,
                 const GLPointLightsData<1>& pointLightsData,
                 float pointLightDepthBias,
                 GLBufferAddress<Vec4f> nodePositionBuffer,
                 GLBufferAddress<Vec4f> normalDistMaps,
                 GLBufferAddress<Vec4f> directLightMaps) {
        m_Program.use();

        uPointLightCount.set(pointLightsData.size());
        uPointLightBuffer.set(pointLightsData.m_PointLightBuffer.getGPUAddress());
        uPointLightDepthMaps.set(pointLightsData.m_CubeMapContainer.getDepthMapArray().getTextureHandle());
        uPointLightFaceProjMatrices.set(6, pointLightsData.m_CubeMapContainer.getFaceProjMatrices());
        uPointLightDepthBias.set(pointLightDepthBias);

        uSphereMapWidth.set(mapWidth);
        uSphereMapHeight.set(mapHeight);
        uSphereMapCount.set(mapCount);
        uNodePositionBuffer.set(nodePositionBuffer);
        uNormalDistSphereMaps.set(normalDistMaps);
        uDirectLightSphereMaps.set(directLightMaps);

        Vec3u groupSize(16, 16, 4);

        glDispatchCompute(1 + mapWidth / groupSize.x,
                          1 + mapHeight / groupSize.y,
                          1 + mapCount / groupSize.z);

        glMemoryBarrier(GL_ALL_BARRIER_BITS);
    }

private:
    GLProgram m_Program;

    BNZ_GLUNIFORM(m_Program, GLuint, uPointLightCount);
    BNZ_GLUNIFORM(m_Program, GLBufferAddress<PointLightData>, uPointLightBuffer);
    BNZ_GLUNIFORM(m_Program, GLTextureCubeMapArrayHandle, uPointLightDepthMaps);
    BNZ_GLUNIFORM(m_Program, Mat4f[], uPointLightFaceProjMatrices);
    BNZ_GLUNIFORM(m_Program, float, uPointLightDepthBias);

    BNZ_GLUNIFORM(m_Program, GLuint, uSphereMapWidth);
    BNZ_GLUNIFORM(m_Program, GLuint, uSphereMapHeight);
    BNZ_GLUNIFORM(m_Program, GLuint, uSphereMapCount);

    BNZ_GLUNIFORM(m_Program, GLBufferAddress<Vec4f>, uNodePositionBuffer);
    BNZ_GLUNIFORM(m_Program, GLBufferAddress<Vec4f>, uNormalDistSphereMaps);
    BNZ_GLUNIFORM(m_Program, GLBufferAddress<Vec4f>, uDirectLightSphereMaps);
};

}
