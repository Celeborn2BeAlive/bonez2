//#include "GLNodeDirectLightVirtualCubeMapRenderPass.hpp"
//#include <bonez/utils/CubeMapUtils.hpp>

//namespace BnZ {

//GLNodeDirectLightVirtualCubeMapRenderPass::RenderPass::RenderPass(const GLShaderManager &shaderManager):
//    m_Program(shaderManager.buildProgram({
//                                             "cubeMapUtils.fs",
//                                             "nodeDirectLightVirtualCubeMapping.vs",
//                                             "nodeDirectLightVirtualCubeMapping.gs",
//                                             "nodeDirectLightVirtualCubeMapping.fs"
//                                         })) {
//}

//GLNodeDirectLightVirtualCubeMapRenderPass::DrawPass::DrawPass(const GLShaderManager &shaderManager):
//    m_Program(shaderManager.buildProgram({
//                                             "utils.fs",
//                                             "image.vs",
//                                             "cubeMapUtils.fs",
//                                             "drawNodeVirtualCubeMap.fs"
//                                         })) {
//}

//GLNodeDirectLightVirtualCubeMapRenderPass::RenderSphereViewPass::RenderSphereViewPass(const GLShaderManager &shaderManager):
//    m_Program(shaderManager.buildProgram({
//                                             "utils.fs",
//                                             "image.vs",
//                                             "cubeMapUtils.fs",
//                                             "renderSphereViewPass.fs"
//                                         })) {
//}

//GLNodeDirectLightVirtualCubeMapRenderPass::GLNodeDirectLightVirtualCubeMapRenderPass(const GLShaderManager& shaderManager):
//    m_RenderPass(shaderManager), m_DrawPass(shaderManager), m_RenderSphereViewPass(shaderManager) {
//}

//void GLNodeDirectLightVirtualCubeMapRenderPass::render(const GLScene& scene, GLVirtualCubeMapContainer<3>& container,
//                                 const Mat4f* viewMatrices, uint32_t count, float zNear, float zFar,
//                                 const LightsData& lightsData) {
//    CubeMapUtils::computeFaceProjMatrices(zNear, zFar, m_FaceProjectionMatrices);

//    auto depthTest = pushGLState<GL_DEPTH_TEST>();
//    depthTest.set(true);

//    auto viewport = pushGLState<GL_VIEWPORT>();
//    auto fb = pushGLState<GL_DRAW_FRAMEBUFFER_BINDING>();

//    m_RenderPass.m_Program.use();

//    m_RenderPass.uLightCount.set(lightsData.lightBuffer.size());
//    m_RenderPass.uLightBuffer.set(lightsData.lightBuffer.getGPUAddress());
//    m_RenderPass.uPointLightSMBuffer.set(lightsData.pointLightSMBuffer.getGPUAddress());
//    m_RenderPass.uSMBias.set(lightsData.smBias);
//    m_RenderPass.uFaceProjectionMatrices.set(6, m_FaceProjectionMatrices);
//    m_RenderPass.uLayer.set(0);

//    Mat4f faceMVPMatrices[6];

//    glViewportArrayv(0, 6, (const GLfloat*) container.getFaceViewports());

//    container.bindForDrawing();
//    container.setDrawBuffers();

//    for(uint32_t idx = 0; idx < count; ++idx) {
//        //auto& fb = container.getFramebuffer(idx);
//        //fb.bindForDrawing();

//        //container.setActiveMap(idx);
//        // TODO: CHANGE
//        assert(false);

//        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

//        for(int i = 0; i < 6; ++i) {
//            faceMVPMatrices[i] = m_FaceProjectionMatrices[i] * viewMatrices[idx];
//        }
//        m_RenderPass.uFaceMVPMatrices.set(6, faceMVPMatrices);

//        scene.render(m_RenderPass.m_MaterialUniforms);
//    }
//}

//void GLNodeDirectLightVirtualCubeMapRenderPass::renderSphereView(
//        const Context& context,
//        const GLVirtualCubeMapContainer<3>& container,
//        std::vector<GLTexture2D>& depthMaps,
//        std::vector<GLTexture2D>& lightMaps,
//        std::vector<GLTexture2D>& positionMaps,
//        std::vector<GLTexture2D>& normalMaps) {
//    auto depthTest = pushGLState<GL_DEPTH_TEST>();
//    depthTest.set(false);

//    auto viewport = pushGLState<GL_VIEWPORT>();
//    auto fb = pushGLState<GL_DRAW_FRAMEBUFFER_BINDING>();

//    m_RenderSphereViewPass.m_Program.use();

//    m_RenderSphereViewPass.uIndirectionTexture.set(container.getIndirectionTexture().getTextureHandle());

//    m_RenderSphereViewPass.uZNear.set(context.getZNear());
//    m_RenderSphereViewPass.uZFar.set(context.getZFar());

//    GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1,
//                           GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3 };

//    for(auto i = 0u; i < container.size(); ++i) {
//        //m_RenderSphereViewPass.uDepthTexture.set(container.getFramebuffer(i).getDepthBuffer().getTextureHandle());
//        //m_RenderSphereViewPass.uIrradianceTexture.set(container.getFramebuffer(i).getColorBuffer(0).getTextureHandle());
//        //m_RenderSphereViewPass.uPositionTexture.set(container.getFramebuffer(i).getColorBuffer(1).getTextureHandle());
//        //m_RenderSphereViewPass.uNormalTexture.set(container.getFramebuffer(i).getColorBuffer(2).getTextureHandle());

//        m_RenderSphereViewPass.uDepthTexture.set(container.getDepthMapArray().getTextureHandle());
//        m_RenderSphereViewPass.uIrradianceTexture.set(container.getMapArray(0).getTextureHandle());
//        m_RenderSphereViewPass.uPositionTexture.set(container.getMapArray(1).getTextureHandle());
//        m_RenderSphereViewPass.uNormalTexture.set(container.getMapArray(2).getTextureHandle());
//        m_RenderSphereViewPass.uLayer.set(GLfloat(i));

//        glNamedFramebufferTexture2DEXT(m_RenderSphereViewPass.m_FBO.glId(), GL_COLOR_ATTACHMENT0,
//                                     GL_TEXTURE_2D, depthMaps[i].glId(), 0);
//        glNamedFramebufferTexture2DEXT(m_RenderSphereViewPass.m_FBO.glId(), GL_COLOR_ATTACHMENT1,
//                                     GL_TEXTURE_2D, lightMaps[i].glId(), 0);
//        glNamedFramebufferTexture2DEXT(m_RenderSphereViewPass.m_FBO.glId(), GL_COLOR_ATTACHMENT2,
//                                     GL_TEXTURE_2D, positionMaps[i].glId(), 0);
//        glNamedFramebufferTexture2DEXT(m_RenderSphereViewPass.m_FBO.glId(), GL_COLOR_ATTACHMENT3,
//                                     GL_TEXTURE_2D, normalMaps[i].glId(), 0);

//        // check that the FBO is complete
//        GLenum status = glCheckNamedFramebufferStatusEXT(m_RenderSphereViewPass.m_FBO.glId(), GL_DRAW_FRAMEBUFFER);
//        if(GL_FRAMEBUFFER_COMPLETE != status) {
//            throw std::runtime_error(GLFramebufferErrorString(status));
//        }

//        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_RenderSphereViewPass.m_FBO.glId());

//        // Specify the outputs of the fragment shader
//        glDrawBuffers(4, drawBuffers);

//        glViewport(0, 0, depthMaps[i].getWidth(), depthMaps[i].getHeight());
//        glClear(GL_COLOR_BUFFER_BIT);

//        context.drawScreenTriangle();
//    }
//}

//void GLNodeDirectLightVirtualCubeMapRenderPass::renderSphereView(const Context& context,
//                      const GLVirtualCubeMapContainer<3>& container,
//                      uint32_t index,
//                      GLTexture2D& depthMap,
//                      GLTexture2D& lightMap,
//                      GLTexture2D& positionMap,
//                      GLTexture2D& normalMap) {
//    auto depthTest = pushGLState<GL_DEPTH_TEST>();
//    depthTest.set(false);

//    auto viewport = pushGLState<GL_VIEWPORT>();
//    auto fb = pushGLState<GL_DRAW_FRAMEBUFFER_BINDING>();

//    m_RenderSphereViewPass.m_Program.use();

//    m_RenderSphereViewPass.uIndirectionTexture.set(container.getIndirectionTexture().getTextureHandle());

//    m_RenderSphereViewPass.uZNear.set(context.getZNear());
//    m_RenderSphereViewPass.uZFar.set(context.getZFar());

//    GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1,
//                           GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3 };

//    //m_RenderSphereViewPass.uDepthTexture.set(cubeView.getDepthBuffer().getTextureHandle());
//    ///m_RenderSphereViewPass.uIrradianceTexture.set(cubeView.getColorBuffer(0).getTextureHandle());
//    //m_RenderSphereViewPass.uPositionTexture.set(cubeView.getColorBuffer(1).getTextureHandle());
//    //m_RenderSphereViewPass.uNormalTexture.set(cubeView.getColorBuffer(2).getTextureHandle());

//    m_RenderSphereViewPass.uDepthTexture.set(container.getDepthMapArray().getTextureHandle());
//    m_RenderSphereViewPass.uIrradianceTexture.set(container.getMapArray(0).getTextureHandle());
//    m_RenderSphereViewPass.uPositionTexture.set(container.getMapArray(1).getTextureHandle());
//    m_RenderSphereViewPass.uNormalTexture.set(container.getMapArray(2).getTextureHandle());
//    m_RenderSphereViewPass.uLayer.set(GLfloat(index));

//    glNamedFramebufferTexture2DEXT(m_RenderSphereViewPass.m_FBO.glId(), GL_COLOR_ATTACHMENT0,
//                                 GL_TEXTURE_2D, depthMap.glId(), 0);
//    glNamedFramebufferTexture2DEXT(m_RenderSphereViewPass.m_FBO.glId(), GL_COLOR_ATTACHMENT1,
//                                 GL_TEXTURE_2D, lightMap.glId(), 0);
//    glNamedFramebufferTexture2DEXT(m_RenderSphereViewPass.m_FBO.glId(), GL_COLOR_ATTACHMENT2,
//                                 GL_TEXTURE_2D, positionMap.glId(), 0);
//    glNamedFramebufferTexture2DEXT(m_RenderSphereViewPass.m_FBO.glId(), GL_COLOR_ATTACHMENT3,
//                                 GL_TEXTURE_2D, normalMap.glId(), 0);

//    // check that the FBO is complete
//    GLenum status = glCheckNamedFramebufferStatusEXT(m_RenderSphereViewPass.m_FBO.glId(), GL_DRAW_FRAMEBUFFER);
//    if(GL_FRAMEBUFFER_COMPLETE != status) {
//        throw std::runtime_error(GLFramebufferErrorString(status));
//    }

//    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_RenderSphereViewPass.m_FBO.glId());

//    // Specify the outputs of the fragment shader
//    glDrawBuffers(4, drawBuffers);

//    glViewport(0, 0, depthMap.getWidth(), depthMap.getHeight());
//    glClear(GL_COLOR_BUFFER_BIT);

//    context.drawScreenTriangle();
//}

//void GLNodeDirectLightVirtualCubeMapRenderPass::drawShadowMap(const GLVirtualCubeMapContainer<3>& container,
//                                               uint32_t index,
//                                               const Context& context,
//                                               bool drawDepth) {
//    auto depthTest = pushGLState<GL_DEPTH_TEST>();
//    depthTest.set(false);

//    m_DrawPass.m_Program.use();

//    m_DrawPass.uSphereView.set(false);
//    m_DrawPass.uDrawDepth.set(drawDepth);
//    m_DrawPass.uIndirectionTexture.set(container.getIndirectionTexture().getTextureHandle());

//    if(drawDepth) {
//        m_DrawPass.uTexture.set(container.getDepthMapArray().getTextureHandle());
//    } else {
//        m_DrawPass.uTexture.set(container.getMapArray(1).getTextureHandle());
//    }

//    m_DrawPass.uLayer.set(GLfloat(index));
//    m_DrawPass.uZNear.set(context.getZNear());
//    m_DrawPass.uZFar.set(context.getZFar());

//    context.drawScreenTriangle();
//}

//void GLNodeDirectLightVirtualCubeMapRenderPass::drawSphereView(const GLTexture2D& texture,
//                    const Context& context,
//                    bool drawDepth) {
//    auto depthTest = pushGLState<GL_DEPTH_TEST>();
//    depthTest.set(false);

//    m_DrawPass.m_Program.use();

//    m_DrawPass.uSphereView.set(true);
//    m_DrawPass.uDrawDepth.set(drawDepth);

//    m_DrawPass.uTexture.set(texture.getTextureHandle());

//    m_DrawPass.uZNear.set(context.getZNear());
//    m_DrawPass.uZFar.set(context.getZFar());

//    context.drawScreenTriangle();
//}

//}
