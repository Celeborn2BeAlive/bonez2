//#pragma once

//#include <bonez/opengl/shadow_mapping/GLCubeShadowMapContainer.hpp>
//#include <bonez/opengl/cube_mapping/GLVirtualCubeMapContainer.hpp>
//#include <bonez/opengl/GLShaderManager.hpp>
//#include <bonez/opengl/GLScene.hpp>
//#include <bonez/Context.hpp>

//#include "GLNodeDirectLightVirtualCubeMapRenderPass.hpp"

//namespace BnZ {

//class GLNodeDirectLightVirtualCubeMapRenderPass {
//public:
//    GLNodeDirectLightVirtualCubeMapRenderPass(const GLShaderManager& shaderManager);

//    struct Light {
//        Vec4f P;
//        Vec4f L;
//        GLuint smIndex;
//    };

//    struct LightsData {
//        GLBuffer<Light> lightBuffer;
//        GLCubeShadowMapContainer pointLightSMContainer;
//        GLBuffer<GLuint64> pointLightSMBuffer;
//        GLfloat smBias;
//    };

//    void render(const GLScene& scene, GLVirtualCubeMapContainer<3>& container, const Mat4f* viewMatrices,
//                uint32_t count, float zNear, float zFar, const LightsData& lightsData);

//    void render(const Context& context, GLVirtualCubeMapContainer<3>& container, const Mat4f* viewMatrices,
//                uint32_t count, const LightsData& lightsData) {
//        render(context.getGLGeometry(), container, viewMatrices, count,
//               context.getZNear(), context.getZFar(), lightsData);
//    }

//    void renderSphereView(const Context& context,
//                          const GLVirtualCubeMapContainer<3>& container,
//                          std::vector<GLTexture2D>& depthMaps,
//                          std::vector<GLTexture2D>& lightMaps,
//                          std::vector<GLTexture2D>& positionMaps,
//                          std::vector<GLTexture2D>& normalMaps);

//    void renderSphereView(const Context& context,
//                          const GLVirtualCubeMapContainer<3>& container,
//                          uint32_t index,
//                          GLTexture2D& depthMap,
//                          GLTexture2D& lightMap,
//                          GLTexture2D& positionMap,
//                          GLTexture2D& normalMap);

//    const Mat4f* getFaceProjectionMatrices() const {
//        return m_FaceProjectionMatrices;
//    }

//    const Mat4f& getFaceProjectionMatrix(uint32_t face) const {
//        return m_FaceProjectionMatrices[face];
//    }

//    void drawShadowMap(const GLVirtualCubeMapContainer<3>& container,
//                       uint32_t index,
//                       const Context& context,
//                       bool drawDepth);

//    void drawSphereView(const GLTexture2D& texture,
//                        const Context& context,
//                        bool drawDepth);

//private:
//    // Contains the projection matrix of each face
//    Mat4f m_FaceProjectionMatrices[6];

//    struct RenderPass {
//        GLProgram m_Program;
//        BNZ_GLUNIFORM(m_Program, Mat4f[], uFaceMVPMatrices);

//        BNZ_GLUNIFORM(m_Program, GLBufferAddress<Light>, uLightBuffer);
//        BNZ_GLUNIFORM(m_Program, GLuint, uLightCount);
//        BNZ_GLUNIFORM(m_Program, GLBufferAddress<GLuint64>, uPointLightSMBuffer);
//        BNZ_GLUNIFORM(m_Program, Mat4f[], uFaceProjectionMatrices);
//        BNZ_GLUNIFORM(m_Program, GLfloat, uSMBias);
//        BNZ_GLUNIFORM(m_Program, GLint, uLayer);
//        GLMaterialUniforms m_MaterialUniforms { m_Program };

//        RenderPass(const GLShaderManager& shaderManager);
//    };

//    RenderPass m_RenderPass;

//    struct RenderSphereViewPass {
//        GLProgram m_Program;

//        BNZ_GLUNIFORM(m_Program, GLTextureCubeMapHandle, uIndirectionTexture);
//        BNZ_GLUNIFORM(m_Program, GLuint64, uDepthTexture);
//        BNZ_GLUNIFORM(m_Program, GLuint64, uIrradianceTexture);
//        BNZ_GLUNIFORM(m_Program, GLuint64, uPositionTexture);
//        BNZ_GLUNIFORM(m_Program, GLuint64, uNormalTexture);
//        BNZ_GLUNIFORM(m_Program, float, uZNear);
//        BNZ_GLUNIFORM(m_Program, float, uZFar);
//        BNZ_GLUNIFORM(m_Program, float, uLayer);

//        RenderSphereViewPass(const GLShaderManager& shaderManager);

//        GLFramebufferObject m_FBO;
//    };

//    RenderSphereViewPass m_RenderSphereViewPass;

//    struct DrawPass {
//        GLProgram m_Program;

//        BNZ_GLUNIFORM(m_Program, bool, uDrawDepth);
//        BNZ_GLUNIFORM(m_Program, GLuint64, uIndirectionTexture);
//        BNZ_GLUNIFORM(m_Program, GLuint64, uTexture);
//        BNZ_GLUNIFORM(m_Program, float, uZNear);
//        BNZ_GLUNIFORM(m_Program, float, uZFar);
//        BNZ_GLUNIFORM(m_Program, float, uLayer);
//        BNZ_GLUNIFORM(m_Program, bool, uSphereView);

//        DrawPass(const GLShaderManager& shaderManager);
//    };

//    DrawPass m_DrawPass;
//};

//}
