#pragma once

#include "GLSkeletonData.hpp"
#include <bonez/opengl/cube_mapping/GLCubeMapContainer.hpp>
#include <bonez/opengl/lighting/GLPointLightsData.hpp>

namespace BnZ {

class GLNodeSphericalMapDirectLightingComputePass {
public:
    using PointLightData = GLPointLightsData<1>::PointLightData;

    GLNodeSphericalMapDirectLightingComputePass(const GLShaderManager& shaderManager):
        m_Program(shaderManager.buildProgram({ "cube_mapping/cubeMapUtils.cs", "skel_sampling/sphereMapDirectLighting.cs" })) {
    }

    void compute(uint32_t sphereMapWidth, uint32_t sphereMapHeight,
                 uint32_t sphereMapCount,
                 const GLPointLightsData<1>& pointLightsData,
                 float pointLightDepthBias,
                 GLBufferAddress<Vec4f> nodePositionBuffer,
                 GLBufferAddress<Vec4f> normalDistSphereMaps,
                 GLBufferAddress<Vec4f> directLightSphereMaps) {
        m_Program.use();

        uPointLightCount.set(pointLightsData.size());
        uPointLightBuffer.set(pointLightsData.m_PointLightBuffer.getGPUAddress());
        uPointLightDepthMaps.set(pointLightsData.m_CubeMapContainer.getDepthMapArray().getTextureHandle());
        uPointLightFaceProjMatrices.set(6, pointLightsData.m_CubeMapContainer.getFaceProjMatrices());
        uPointLightDepthBias.set(pointLightDepthBias);

        uSphereMapWidth.set(sphereMapWidth);
        uSphereMapHeight.set(sphereMapHeight);
        uSphereMapCount.set(sphereMapCount);
        uNodePositionBuffer.set(nodePositionBuffer);
        uNormalDistSphereMaps.set(normalDistSphereMaps);
        uDirectLightSphereMaps.set(directLightSphereMaps);

        Vec3u groupSize(16, 16, 4);

        glDispatchCompute(1 + sphereMapWidth / groupSize.x,
                          1 + sphereMapHeight / groupSize.y,
                          1 + sphereMapCount / groupSize.z);

        glMemoryBarrier(GL_ALL_BARRIER_BITS);
    }

private:
    GLProgram m_Program;

    BNZ_GLUNIFORM(m_Program, GLuint, uPointLightCount);
    BNZ_GLUNIFORM(m_Program, GLBufferAddress<PointLightData>, uPointLightBuffer);
    BNZ_GLUNIFORM(m_Program, GLTextureCubeMapArrayHandle, uPointLightDepthMaps);
    BNZ_GLUNIFORM(m_Program, Mat4f[], uPointLightFaceProjMatrices);
    BNZ_GLUNIFORM(m_Program, float, uPointLightDepthBias);

    BNZ_GLUNIFORM(m_Program, GLuint, uSphereMapWidth);
    BNZ_GLUNIFORM(m_Program, GLuint, uSphereMapHeight);
    BNZ_GLUNIFORM(m_Program, GLuint, uSphereMapCount);

    BNZ_GLUNIFORM(m_Program, GLBufferAddress<Vec4f>, uNodePositionBuffer);
    BNZ_GLUNIFORM(m_Program, GLBufferAddress<Vec4f>, uNormalDistSphereMaps);
    BNZ_GLUNIFORM(m_Program, GLBufferAddress<Vec4f>, uDirectLightSphereMaps);
};

}
