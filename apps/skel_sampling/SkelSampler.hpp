#pragma once

#include <bonez/common.hpp>
#include <bonez/types.hpp>
#include <bonez/maths/maths.hpp>
#include <bonez/sampling/shapes.hpp>
#include <bonez/sampling/DiscreteDistribution.hpp>
#include <bonez/scene/Intersection.hpp>
#include <bonez/atb.hpp>
#include <bonez/parsing/parsing.hpp>

namespace BnZ {

class SkelSampler {
public:
    virtual ~SkelSampler() {
    }

    virtual void preprocess(tinyxml2::XMLElement* pStatistics) = 0;

    virtual void loadSettings(const tinyxml2::XMLElement& xml) = 0;

    virtual void storeSettings(tinyxml2::XMLElement& xml) const = 0;

    virtual void exposeIO(TwBar* bar) = 0;

    // Sample a surface point from the distribution of a node
    // Return the value of the importance function for the sampled point
    virtual Vec3f sampleSurfacePoint(
            uint32_t nodeIdx, // Index of the node to sample
            Vec3f& P, // Return the sampled position
            Vec3f& N, // Return the sampled normal
            float& pdfWrtSolidAngle, // Return the pdf of the sampled surface point wrt. node solid angle
            float& pdfWrtArea, // Return the pdf of the sampled surface point, wrt. area
            const Vec2f& s2D // Random uniform 2D variable which is used to sample
    ) const = 0;

    virtual float directionPdfWrtNodeSolidAngle(uint32_t nodeIdx,
                                                const Vec3f& wo) const = 0;

    // Compute the pdf of sampling a point wrt. the solid angle at the node position
    virtual float pdfWrtNodeSolidAngle(
            uint32_t nodeIdx,
            const Vec3f& P,
            Vec3f& dirNode2Point,
            float& dist) const = 0;

    float pdfWrtNodeSolidAngle(
            uint32_t nodeIdx,
            const Vec3f& P) const {
        Vec3f dir;
        float dist;
        return pdfWrtNodeSolidAngle(nodeIdx, P, dir, dist);
    }

    // Compute the pdf of sampling a point wrt. surface area
    float pdfWrtArea(
            uint32_t nodeIndex,
            const Vec3f& P,
            const Vec3f& N) const {
        Vec3f dir;
        float dist;
        auto pdf = pdfWrtNodeSolidAngle(nodeIndex, P, dir, dist);
        if(pdf <= 0.f) {
            return pdf;
        }
        return solidAngleToAreaPDF(pdf, 1.f / sqr(dist), dot(N, -dir));
    }
};

inline Sample1u sampleNodeFromWeights(
        const Vec4i& nodeIndices,
        const Vec4f& nodeWeights,
        uint32_t maxNodeCount,
        float s1D) {
    auto size = 0u;
    Vec4f weights = zero<Vec4f>();
    for (size = 0u;
         size < min(maxNodeCount, 4u)
         && nodeIndices[size] >= 0;
         ++size) {
        weights[size] = nodeWeights[size];
    }

    if(size > 0) {
        DiscreteDistribution4f dist(weights);
        return dist.sample(s1D);
    }

    return Sample1u();
}

inline Vec3f sampleSurfacePoint(
        const SkelSampler& sampler,
        uint32_t maxNodeCount,
        const Vec4i& nodeIndex,
        const Vec4f& nodeWeight,
        Vec3f& P,
        Vec3f& N,
        float& pdfWrtArea,
        float s1D,
        const Vec2f& s2D) {
    auto nodeSample = sampleNodeFromWeights(nodeIndex, nodeWeight, maxNodeCount, s1D);
    if(nodeSample.pdf == 0.f) {
        pdfWrtArea = 0.f;
        return Vec3f(0.f);
    }

    auto nodeIdx = nodeIndex[nodeSample.value];

    float pdfWrtSolidAngle;
    return sampler.sampleSurfacePoint(nodeIdx, P, N, pdfWrtSolidAngle, pdfWrtArea, s2D);
}

inline float pdfWrtIntersectionSolidAngle(
        const SkelSampler& sampler,
        const Intersection& I,
        uint32_t nodeIdx,
        const Vec3f& P,
        const Vec3f& N) {
    Vec3f dir;
    float dist;
    auto pdf = sampler.pdfWrtNodeSolidAngle(nodeIdx, P, dir, dist);
    if(pdf <= 0.f) {
        return pdf;
    }

    pdf = solidAngleToAreaPDF(pdf, 1.f / sqr(dist), dot(N, -dir));

    auto wo = I.P.xyz() - P;
    auto d = length(wo);
    wo /= d;

    return areaToSolidAnglePDF(pdf, sqr(d), dot(N, wo));
}

inline float pdfWrtIntersectionSolidAngle(
        const SkelSampler& sampler,
        const Intersection& I,
        uint32_t maxNodeCount,
        const Vec4i& nodeIndex,
        const Vec4f& nodeWeight,
        const Vec3f& P,
        const Vec3f& N) {
    auto size = 0u;
    Vec4f weights = zero<Vec4f>();
    for (size = 0u;
         size < min(maxNodeCount, 4u) && nodeIndex[size] >= 0;
         ++size) {
        weights[size] = nodeWeight[size];
    }

    if(size == 0u) {
        return 0.f;
    }

    DiscreteDistribution4f dist(weights);
    float p = 0.f;
    for(auto i = 0u; i < size; ++i) {
        float pdf = pdfWrtIntersectionSolidAngle(sampler, I, nodeIndex[i], P, N);
        if(pdf > 0.f) {
            p += dist.pdf(i) * pdf;
        }
    }
    return p;
}

}
