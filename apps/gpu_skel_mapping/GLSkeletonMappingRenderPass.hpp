#pragma once

#include "GLSkeletonData.hpp"

namespace BnZ {

class GLSkeletonMappingRenderPass {
public:
    GLProgram m_Program;
    GLGBufferUniform uGBuffer { m_Program };
    BNZ_GLUNIFORM(m_Program, Mat4f, uRcpProjMatrix);
    BNZ_GLUNIFORM(m_Program, Mat4f, uRcpViewMatrix);
    BNZ_GLUNIFORM(m_Program, GLBufferAddress<Vec4f>, uNodeColorBuffer);
    GLSkelDataUniforms uSkelData { m_Program };
    GLCubeMapContainerUniform uCubeMapContainer { m_Program };
    BNZ_GLUNIFORM(m_Program, int, uSelectedNode);
    BNZ_GLUNIFORM(m_Program, bool, uRequestNode);
    BNZ_GLUNIFORM(m_Program, Vec3f, uRequestedP);
    BNZ_GLUNIFORM(m_Program, Vec3f, uRequestedN);
    BNZ_GLUNIFORM(m_Program, GLBufferAddress<uint32_t>, uRequestedNode);
    BNZ_GLUNIFORM(m_Program, bool, uDisplayWeight);

    GLSkeletonMappingRenderPass(const GLShaderManager& shaderManager):
        m_Program(shaderManager.buildProgram({
            "deferredShadingPass.vs",
            "cube_mapping/cubeMapUtils.fs",
            "gpu_skel_mapping/skelMappingUtils.fs",
            "gpu_skel_mapping/skelMapping.fs",
            getCubeMapSphereTextureShader(".fs")
        })) {
    }

    template<uint32_t ChannelCount>
    void render(const GLGBuffer& gbuffer,
                const ProjectiveCamera& camera,
                const GLScreenTriangle& triangle,
                GLSkeletonData<ChannelCount>& skelData,
                float nodeDepthBias,
                int selectedNode,
                const Intersection& requestI,
                uint32_t& requestedNode,
                uint32_t computeNodeWeightSubroutineIndex,
                bool displayWeight) {
        auto depthTest = pushGLState<GL_DEPTH_TEST>();
        depthTest.set(false);

        GLResidentTextureScope scope(skelData.m_NodeCubeMapContainer.getMapArray(
                                         skelData.m_NodeCubeMapContainer.getDepthMapChannelIndex()));

        glViewport(0, 0, gbuffer.getWidth(), gbuffer.getHeight());

        m_Program.use();

        uCubeMapContainer.set(skelData.m_NodeCubeMapContainer.getDepthMapChannelIndex(),
                              skelData.m_NodeCubeMapContainer);

        uGBuffer.set(gbuffer);
        uRcpProjMatrix.set(camera.getRcpProjMatrix());
        uRcpViewMatrix.set(camera.getRcpViewMatrix());

        uNodeColorBuffer.set(skelData.m_NodeColorBuffer.getGPUAddress());
        uSkelData.set(skelData, nodeDepthBias);
        uSelectedNode.set(selectedNode);

        uDisplayWeight.set(displayWeight);

        GLBuffer<uint32_t> requestedNodeBuffer;
        requestedNodeBuffer.setStorage(1, GL_STATIC_DRAW);
        requestedNodeBuffer.makeResident(GL_WRITE_ONLY);
        if(requestI) {
            uRequestNode.set(true);
            uRequestedP.set(Vec3f(requestI.P));
            uRequestedN.set(Vec3f(requestI.Ns));
            uRequestedNode.set(requestedNodeBuffer.getGPUAddress());
        } else {
            uRequestNode.set(false);
        }

        glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &computeNodeWeightSubroutineIndex);

        triangle.render();

        glMemoryBarrier(GL_ALL_BARRIER_BITS);

        if(requestI) {
            requestedNodeBuffer.getData(1, &requestedNode);
        }
    }
};

}
