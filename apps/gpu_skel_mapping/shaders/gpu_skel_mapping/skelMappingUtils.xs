#version 430 core
#extension GL_NV_gpu_shader5 : enable
#extension GL_NV_bindless_texture : enable

subroutine float
computeNodeWeightSubroutine(uint nodeIdx, vec3 P_ns, float dist, vec3 wi, vec3 N_ws);

layout(location = 0) subroutine
uniform computeNodeWeightSubroutine uComputeNodeWeight;

struct SkelDataUniforms {
    uint nodeCount;
    mat4* nodeViewMatrixBuffer;
    float* nodeRadiusBuffer;
    mat4 faceProjMatrices[6];
    float depthBias;
    samplerCubeArray nodeDepthBuffer;
};

uniform SkelDataUniforms uSkelData;

int getFaceIdx(vec3 wi);

layout(index = 0) subroutine(computeNodeWeightSubroutine)
float computeNodeWeightDistCosMaxball(uint nodeIdx, vec3 P_ns, float dist, vec3 wi, vec3 N_ws) {
    float c = max(0, dot(wi, N_ws)); // Works because node view matrix are only translations
    float r = uSkelData.nodeRadiusBuffer[nodeIdx];
    return r * c / (dist * dist);
}

layout(index = 1) subroutine(computeNodeWeightSubroutine)
float computeNodeWeightDist(uint nodeIdx, vec3 P_ns, float dist, vec3 wi, vec3 N_ws) {
    return 1.f / dist;
}

layout(index = 2) subroutine(computeNodeWeightSubroutine)
float computeNodeWeightDistSqrtCosMaxball(uint nodeIdx, vec3 P_ns, float dist, vec3 wi, vec3 N_ws) {
    float c = max(0, dot(wi, N_ws)); // Works because node view matrix are only translations
    float r = uSkelData.nodeRadiusBuffer[nodeIdx];
    return r * sqrt(c) / (dist * dist);
}

layout(index = 3) subroutine(computeNodeWeightSubroutine)
float computeNodeWeightDistCosSqrMaxball(uint nodeIdx, vec3 P_ns, float dist, vec3 wi, vec3 N_ws) {
    float c = max(0, dot(wi, N_ws)); // Works because node view matrix are only translations
    float r = uSkelData.nodeRadiusBuffer[nodeIdx];
    return r * r * c / (dist * dist);
}

layout(index = 4) subroutine(computeNodeWeightSubroutine)
float computeNodeWeightDistMaxball(uint nodeIdx, vec3 P_ns, float dist, vec3 wi, vec3 N_ws) {
    if(dot(wi, N_ws) <= 0.f) {
        return 0.f;
    }
    float r = uSkelData.nodeRadiusBuffer[nodeIdx];
    return r / (dist * dist);
}

layout(index = 5) subroutine(computeNodeWeightSubroutine)
float computeNodeWeightDistCos(uint nodeIdx, vec3 P_ns, float dist, vec3 wi, vec3 N_ws) {
    float c = max(0, dot(wi, N_ws)); // Works because node view matrix are only translations
    return c / (dist * dist);
}

layout(index = 6) subroutine(computeNodeWeightSubroutine)
float computeNodeWeightDist2CosMaxball(uint nodeIdx, vec3 P_ns, float dist, vec3 wi, vec3 N_ws) {
    float c = max(0, dot(wi, N_ws)); // Works because node view matrix are only translations
    float r = uSkelData.nodeRadiusBuffer[nodeIdx];
    return r * c / dist;
}

bool occluded(vec3 P_ns, uint mapIndex, vec3 shadowRay) {
    int face = getFaceIdx(shadowRay);
    vec4 faceClipSpacePosition = uSkelData.faceProjMatrices[face] *
        vec4(P_ns, 1);
    float depthRef = faceClipSpacePosition.z / faceClipSpacePosition.w;
    depthRef = 0.5 * (depthRef + 1) - uSkelData.depthBias;

    float depth = texture(uSkelData.nodeDepthBuffer, vec4(shadowRay, mapIndex)).r;
    return depthRef >= depth;
}

int computeNearestNode(vec4 P_ws, vec3 N_ws) {
    int nearestNode = -1;
    float currentWeight = 0;

    for(int i = 0; i < uSkelData.nodeCount; ++i) {
        // Position in node space
        vec3 P_ns = vec3(uSkelData.nodeViewMatrixBuffer[i] * P_ws);

        float dist = length(P_ns);
        vec3 wi = -P_ns / dist;

        if(!occluded(P_ns, i, -wi)) {
            float weight = uComputeNodeWeight(i, P_ns, dist, wi, N_ws);
            if(weight > currentWeight) {
                currentWeight = weight;
                nearestNode = i;
            }
        }
    }

    return nearestNode;
}

int computeNearestNode(vec4 P_ws, vec3 N_ws, out float currentWeight) {
    int nearestNode = -1;
    currentWeight = 0;

    for(int i = 0; i < uSkelData.nodeCount; ++i) {
        // Position in node space
        vec3 P_ns = vec3(uSkelData.nodeViewMatrixBuffer[i] * P_ws);

        float dist = length(P_ns);
        vec3 wi = -P_ns / dist;

        if(!occluded(P_ns, i, -wi)) {
            float weight = uComputeNodeWeight(i, P_ns, dist, wi, N_ws);
            if(weight > currentWeight) {
                currentWeight = weight;
                nearestNode = i;
            }
        }
    }

    return nearestNode;
}

ivec4 computeNearestNodes(vec4 P_ws, vec3 N_ws, out vec4 weights) {
    ivec4 nearestNodes = ivec4(-1);
    vec4 currentWeights = vec4(0);

    for(int i = 0; i < uSkelData.nodeCount; ++i) {
        // Position in node space
        vec3 P_ns = vec3(uSkelData.nodeViewMatrixBuffer[i] * P_ws);

        float dist = length(P_ns);
        vec3 wi = -P_ns / dist;

        if(!occluded(P_ns, i, -wi)) {
            float weight = uComputeNodeWeight(i, P_ns, dist, wi, N_ws);
            if(weight > currentWeights.x) {
                for(int i = 3; i > 0; --i) {
                    currentWeights[i] = currentWeights[i - 1];
                    nearestNodes[i] = nearestNodes[i - 1];
                }
                currentWeights.x = weight;
                nearestNodes.x = i;
            } else if(weight > currentWeights.y) {
                for(int i = 3; i > 1; --i) {
                    currentWeights[i] = currentWeights[i - 1];
                    nearestNodes[i] = nearestNodes[i - 1];
                }
                currentWeights.y = weight;
                nearestNodes.y = i;
            } else if(weight > currentWeights.z) {
                for(int i = 3; i > 2; --i) {
                    currentWeights[i] = currentWeights[i - 1];
                    nearestNodes[i] = nearestNodes[i - 1];
                }
                currentWeights.z = weight;
                nearestNodes.z = i;
            } else if(weight > currentWeights.w) {
                currentWeights.w = weight;
                nearestNodes.w = i;
            }
        }
    }

    weights = currentWeights;
    return nearestNodes;
}

vec4 getNodesColor(ivec4 nodes) {
    return vec4(nodes) / float(uSkelData.nodeCount);
}

float computeWeight(int i, vec4 P_ws, vec3 N_ws) {
    float bestWeight;
    int bestMatch = computeNearestNode(P_ws, N_ws, bestWeight);

    vec3 P_ns = vec3(uSkelData.nodeViewMatrixBuffer[i] * P_ws);

    float dist = length(P_ns);
    vec3 wi = -P_ns / dist;

    if(!occluded(P_ns, i, -wi)) {
        float weight = uComputeNodeWeight(i, P_ns, dist, wi, N_ws);
        return weight / bestWeight;
    }
    return 0.f;

/*
    int nearestNode = -1;
    float currentWeight = 0;

    for(int i = 0; i < uSkelData.nodeCount; ++i) {
        // Position in node space
        vec3 P_ns = vec3(uSkelData.nodeViewMatrixBuffer[i] * P_ws);

        float dist = length(P_ns);
        vec3 wi = -P_ns / dist;

        if(!occluded(P_ns, i, -wi)) {
            float weight = uComputeNodeWeight(i, P_ns, dist, wi, N_ws);
            if(weight > currentWeight) {
                currentWeight = weight;
                nearestNode = i;
            }
        }
    }

    return nearestNode;*/
}
