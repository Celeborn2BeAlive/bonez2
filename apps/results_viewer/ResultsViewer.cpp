#include "ResultsViewer.hpp"

namespace BnZ {

void ResultsViewer::initShaderManager(const char* shadersFilePath) {
    auto shadersPath = FilePath(shadersFilePath);

    tinyxml2::XMLDocument shadersDoc;
    if(tinyxml2::XML_NO_ERROR != shadersDoc.LoadFile(shadersPath.c_str())) {
        throw std::runtime_error("Unable to load shaders file");
    }
    auto pRoot = shadersDoc.RootElement();
    if(!pRoot) {
        throw std::runtime_error("No root element in shaders' XML file");
    }
    auto path = shadersPath.path();
    for(auto pDir = pRoot->FirstChildElement("Dir"); pDir; pDir = pDir->NextSiblingElement("Dir")) {
        m_ShaderManager.addDirectory(path + pDir->Attribute("path"));
    }
}

Shared<Image> ResultsViewer::loadImage(const char* rawFilePath) {
    Shared<Image> pImg = loadRawImage(rawFilePath);
    if(!pImg) {
            throw std::runtime_error("Unable to load image " + std::string(rawFilePath));
    }
    pImg->flipY();
    return pImg;
}

ResultsViewer::ResultsViewer(const char* shadersFilePath, const char* rawFilePath):
    m_ImagePath(rawFilePath),
    m_pImage(loadImage(rawFilePath)),
    m_WindowManager(m_pImage->getWidth(), m_pImage->getHeight(), "ResultsViewer") {

    initShaderManager(shadersFilePath);
    initImageIO();

    m_GLImageRenderer = makeUnique<GLImageRenderer>(m_ShaderManager);
}

void ResultsViewer::exposeIO() {
    TwBar* pBar = TwNewBar("Params");
    atb::addVarRW(pBar, ATB_VAR(m_fGamma), " min=0 ");
    atb::addButton(pBar, "Export PNG", [this]() {
        Image copy = *m_pImage;
        copy.flipY();
        copy.divideByAlpha();
        copy.applyGamma(m_fGamma);
        std::clog << "Store " << (m_ImagePath.path() + m_ImagePath.file()).
                     addExt(".gamma" + toString(m_fGamma) + ".png") << std::endl;
        storeImage((m_ImagePath.path() + m_ImagePath.file()).
                   addExt(".gamma" + toString(m_fGamma) + ".png"), copy);
    });
}

void ResultsViewer::drawImage() {
    m_GLImageRenderer->drawImage(m_fGamma, *m_pImage);
}

void ResultsViewer::run() {
    exposeIO();

    while (m_WindowManager.isOpen()) {
        glClear(GL_COLOR_BUFFER_BIT);

        drawImage();

        m_bGUIHasFocus = m_WindowManager.drawGUI();

        m_WindowManager.handleEvents();

        m_WindowManager.swapBuffers();
    }
}

}
