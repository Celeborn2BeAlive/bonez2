#include <iostream>

#include "ResultsViewer.hpp"

namespace BnZ {

int main(int argc, char** argv) {
    if(argc != 3) {
        std::cerr << "Usage: " << argv[0] << " < shaders file path > < raw file path >" << std::endl;
        return -1;
    }

    ResultsViewer viewer(argv[1], argv[2]);
    viewer.run();

	return 0;
}

}

int main(int argc, char** argv) {
#ifdef _DEBUG
        std::cerr << "DEBUG MODE ACTIVATED" << std::endl;
#endif

#ifdef DEBUG
        std::cerr << "DEBUG MODE ACTIVATED" << std::endl;
#endif
    return BnZ::main(argc, argv);
}
