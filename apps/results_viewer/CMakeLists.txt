set(EXECUTABLE_FILE results_viewer)

find_package(OpenGL REQUIRED)

include_directories(${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_SOURCE_DIR}/bonez2/
    ${CMAKE_SOURCE_DIR}/apps)

file(GLOB_RECURSE SRC_FILES ${CMAKE_SOURCE_DIR}/apps/viewer/*.cpp ${CMAKE_SOURCE_DIR}/apps/viewer/*.hpp *.cpp *.hpp shaders/*.xs shaders/*.vs shaders/*.fs shaders/*.gs shaders/*.cs)
add_executable(${EXECUTABLE_FILE} ${SRC_FILES})
target_link_libraries(${EXECUTABLE_FILE} GLEW
    ${OPENGL_LIBRARIES} bonez2 ${GLFW_LIBRARIES} glfw embree assimp freeimage AntTweakBar)

file(GLOB_RECURSE LIB_FILES ${CMAKE_SOURCE_DIR}/third-party/lib/linux-x64/*.so*)
file(COPY ${LIB_FILES} DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
