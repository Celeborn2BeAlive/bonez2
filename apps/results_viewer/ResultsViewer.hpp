#pragma once

#include <bonez/viewer/WindowManager.hpp>
#include <bonez/opengl/GLShaderManager.hpp>
#include <bonez/opengl/GLImageRenderer.hpp>
#include <bonez/parsing/parsing.hpp>

namespace BnZ {

class ResultsViewer {
    void initShaderManager(const char* shadersFilePath);

    Shared<Image> loadImage(const char* rawFilePath);

    void exposeIO();

    void drawImage();

protected:
    FilePath m_ImagePath;

    Shared<Image> m_pImage;

    WindowManager m_WindowManager;
    GLShaderManager m_ShaderManager;
    Unique<GLImageRenderer> m_GLImageRenderer;

    bool m_bGUIHasFocus = false;

    float m_fGamma = 1.f;

public:
    ResultsViewer(const char* shadersFilePath, const char* rawFilePath);

    void run();
};

}
